% Pre- an Processing upGPR Data
% to check how raw data look like



%% Tabula Rasa
clear all
close all
clc
%% initialize project


% add  automaticProcessing and sub folders to path 
% addpath(genpath('N:\lawprae\LBI\Projects\101_upGPR\postprocessing-scripts\Matlab\automaticProcessing'))

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%GPRproject_prjpath = 'N:\lawprae\LBI\Projects\101_upGPR\data\Scans\2016-17\upGPR WFJ\missions\161223.MIS\161223_1500.ZON\';

% open file list and make it readable for windows
% load('fls.mat')
% teil1 = 'N:\lawprae\LBI\Projects\101_upGPR\data\Scans\2016-17\upGPR WFJ\';
  teil1 = 'X:\Radar\Radardata\2018-19\upGPR WFJ\missions';
% teil2_beta = char(strrep(fls,'/','\')); % change slash to backslash
% teil2 = teil2_beta(:,3:end);
% 
% %%
% stepsize = 100;
% n = 1; % counting pics
% i = 2;%1:stepsize:length(teil2(:,1))

teil2='\190115.MIS\190115_0900.ZON\LID10001.dt';

% GPRproject_prjpath = [teil1 teil2(i,:)]  % file path
GPRproject_prjpath = [teil1 teil2];  % file path

%%
clear gprdat GPRproject_datfile gprflw procp

%filedate = GPRproject_prjpath(end-15:end-5);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% raw data file
GPRproject_datfile = dir([GPRproject_prjpath '*.dt']);

%% import gpr-data

gprflw(1).para.path     = GPRproject_prjpath;
gprflw(1).para.datafile = GPRproject_datfile;
gprflw(1).name = 'import_IDS';
% gprdat(1).data = ReadIDS([gprflw(1).para.path gprflw(1).para.datafile.name]);
gprdat(1).data = ReadIDS(gprflw(1).para.path);
%% -------------------------------------------------------------------------
% ------------------------- INPUT PARAMETERS ------------------------------
% -------------------------------------------------------------------------

meastime             = 40;   % Measurement time for nsamp [ns]
vlen                 = 40;   % Displaying time (max 40 ns)
picksurf             = 0;    % Use static correction picks (1) or not (0)

%% -------------------------------------------------------------------------
% ------------------- DEFINE PROCESSING PARAMETERS ------------------------
% -------------------------------------------------------------------------

load zcm.mat                % blue-red colormap
load blkwhtred.mat          % black-white-red colormap
procp.cmap = blkwhtred;     % Colormap for displaying sections
procp.cax = [-1.0 1.0];     % Lower and upper bounds of colorbar
procp.vair = 0.299;         % Air velocity
procp.vice = 0.1689;        % Ice velocity
procp.vsnow = 0.23;         % Snow velocity

procp.fl = 300;             % Low cut frequency for bandpass filter %290
procp.fh = 2500;            % High cut frequency for bandpass filter %2000
procp.threshold_amp = 0.07; % Threshold for the amplitude
procp.ford = 1;             % Order of the Butterworth bandpass filter
procp.startTwt  = 5.6;%5.7; % Phase follower starting time [ns], staticauto  must be set 1. % 5.8 for svd3
procp.startTwt_high = 3.5; % Phase follower starting time upper side [ns], staticauto  must be set 1. % 5.8 for svd33. Only if background = 33
startDate = 1;             % starting trace (set to 1)
%procp.twin = 3000;         % Length of trace time window
%procp.maxdepth = 250;      % Maximum depth [m] for displaying data
procp.svd_len = 4000;%2500;        % Length of SVD filter % 51
%procp.decon_len = 50;      % fx-deconvolution filter length
procp.agclen = 10;          % Length of AGC filter (0 if no agc) (max 40ns)
procp.tord = 0;             % y-axis time (0) or depth (1)
procp.tax = 10;             % Lower limitation of the time axis for picking
bound = [1 263 539 876 1723 1970 2236 2604 3427 3640]; % Boundaries

%% -------------------------------------------------------------------------
% ------------------- Calculated Input Variables --------------------------
% -------------------------------------------------------------------------

gprdat.nr_of_traces  = length(gprdat.data(1,:)); % Number of traces (around 3640)
gprdat.nr_of_samples = length(gprdat.data(:,1)); % Number of samples (around 1024)

gprdat.sampling_rate = meastime / gprdat.nr_of_samples; % Sampling Interval [ns]
f = linspace(0,1/(2*gprdat.sampling_rate),gprdat.nr_of_samples/2)* 1000;
nf = length(f);
n = 1; % counting pics
k = 1; % counting data
viewlen = vlen/gprdat.sampling_rate;
stepsize = gprdat.nr_of_samples/4; % for time: 4  %%for depth: 92 for vsnow, 59.8 for v air

if procp.tord == 1 % y-axis: depth
    yend = procp.vsnow*meastime/2;
    deltat = yend*stepsize/gprdat.nr_of_samples;
    ylab = 'Depth [m]';
else % y-axis: time
    yend = meastime;
    deltat = yend*stepsize/gprdat.nr_of_samples;
    ylab = 'Time [ns]';
end

gprdat(k).fdata =1; % Initialize
gprdat(k).normfdata =1;

%% -------------------------------------------------------------------------
% ------------------- PLOT RAW DATA ---------------------------------------
% -------------------------------------------------------------------------

fig = figure(n);clf;
subplot(211)
imagesc(gprdat(k).data(1:viewlen,:))
    set(gca,'YTick',0:stepsize:gprdat(k).nr_of_samples)
    set(gca,'YTickLabel',0:deltat:yend)
    colormap(procp.cmap)
    xlabel('Trace','fontsize',12);
    ylabel(ylab,'fontsize',12);
    set(gca,'fontsize',12);
    %title({['Raw Data'];['Filename ',gprflw.para.datafile.name];['File date: ',filedate]})

k= k+1;
n= n+1;

        
subplot(212)
gprdat(k-1).fdata = sum(abs(fft(gprdat(k-1).data))'); % calculate for further steps

plot(f,gprdat(k-1).fdata(1:nf),'b')
hold on

% Butterwoth filter
nyq = 0.5 / gprdat(k-1).sampling_rate * 1000;
fl = procp.fl/nyq;
fh = procp.fh/nyq;
[bb,aa] = butter(procp.ford,[fl fh]);  
gprdat(k).data = filter(bb,aa,gprdat(k-1).data);
gprdat = renamedata(gprdat,k);
gprdat(k).fdata = sum(abs(fft(gprdat(k).data))');


plot(f,gprdat(k).fdata(1:nf),'r');
xlabel('Frequency [MHz]','fontsize',12);
ylabel('Amplitude [1]','fontsize',12);
[~,I] = max(gprdat(k).fdata(1:nf));
title({['Central Freq.: ',num2str(f(I)),' MHz'];['Lowcut: ',num2str(procp.fl)];['Highcut: ',num2str(procp.fh)]});
legend('Before Butterworth Bandpass Filter','After Butterworth Bandpass Filter');
xlim([0 nyq/4])
hold off
n = n+1;
k = k+1;

     
