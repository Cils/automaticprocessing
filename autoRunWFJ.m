% Run evaluation of the current hydrological Year 2020
% Find the evaluation folder by reading the INI file upGPR.ini and then calls the scripts  PreproSeason.m and SeasonEval.m. 
% If an error occurs, it send a warning email.
% 
% 
% Author: Achille Capelli, SLF
% % email: capelli@slf.ch 
% November 2019; Last revision:

clear

% Read upGPR.ini for information about season and path
try 
    inifile='C:\batches\upGPR.ini';
    I = INI('File',inifile);
    % INI file reading
    I.read();
    errorcode=0;
catch ME
    errorcode=1;
    disp('!Error!')
    disp(['The .ini file was not found in path=' inifile ])
    return
end

% folderpath
folderpath=['D:\scans\' char(I.general.season) '\Evaluation'];
cd (folderpath)

% Load Eval*.ini for getting settings. If not found display message and exit evaluation.
try 
    inifile=dir([folderpath '\Eval*.ini']);
    I2 = INI('File',[folderpath '\' inifile.name]);
    % INI file reading
    I2.read();
    logToFile(['path Eval*.ini: ',folderpath '\' inifile.name],4)
catch ME
    errorcode=2;
    disp('!Error!')
    disp(['The Eval*.ini file was not found in path=' [folderpath '\' inifile.name] ])
    return
end




% process single scans, 1st step
errorcode1 = PreproSeason(folderpath,'justnew',I2.AutoRun.justnew);

% process season, 2nd step
errorcode2 = seasonEval(folderpath,'autoEval',1,'plStatCorTr',I2.AutoRun.plStatCorTr,'fitDrift',I2.AutoRun.fitDrift,'plotHS',I2.AutoRun.plotHS);

close('all')

%% handle errors

% errors of seasonEval
switch errorcode1
    case 0
        disp('Successful preprocessing')
    case 1
        command= ['C:\batches\SendMail.exe ' ...
                  '"Matlab preprocessing failed (PreproSeason.m).  The INI file was not found." ' ...
                  num2str(0)];
        [status,cmdout] = system(command);
    case 2
         command= ['C:\batches\SendMail.exe ' ...
                  '"Matlab preprocessing failed (PreproSeason.m).  Was not possible to load the static correction file (staticCorShift.mat)." ' ...
                  num2str(0)];
        [status,cmdout] = system(command);
    otherwise
        command= ['C:\batches\SendMail.exe ' ...
                  '"Unknown error in Matlab preprocessing (PreproSeason.m)." ' ...
                  num2str(1)];
        [status,cmdout] = system(command);
end

% errors of seasonEval
switch errorcode2
    case 0
        disp('Successful evaluation of season')
    case 1
        command= ['C:\batches\SendMail.exe ' ...
                  '"Matlab season evaluation failed (seasonEval.m).  The INI file was not found." ' ...
                  num2str(0)];
        [status,cmdout] = system(command);
    otherwise
        command= ['C:\batches\SendMail.exe ' ...
                  '"Unknown error in Matlab season evaluation (seasonEval.m)." ' ...
                  num2str(1)];
        [status,cmdout] = system(command);
end


% main errors
switch errorcode
    case 0
        disp('Evrithing working well')
    case 1
        command= ['C:\batches\SendMail.exe ' ...
                  '"The file with path= c:\batches\upGPR.ini could not be loaded." ' ...
                  num2str(1)];
        [status,cmdout] = system(command);
   case 2
        command= ['C:\batches\SendMail.exe ' ...
                  '"The INI file for the MAtlab evaluation could not be loaded. E.g. path: D:\scans\*season*\Evaluation\Eval*.ini" ' ...
                  num2str(1)];
        [status,cmdout] = system(command);
    otherwise
        command= ['C:\batches\SendMail.exe ' ...
                  '"Unknown error in Matlab season evaluation (seasonEval.m)." ' ...
                  num2str(1)];
        [status,cmdout] = system(command);
end