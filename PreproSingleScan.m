function [ errorcode,gprdat,I ] = PreproSingleScan( folderpath, scan_file, varargin )
% PreproSingleScan - Processing upGPR Data of a single scan and check the settings.
% Workflow to read in all raw data, process and stack the traces and save settings in .ini if desired.
% 
% Syntax:  [ errorcode ] = PreproSingleScan( evalfolder, scan_file )
%          [ errorcode ] = PreproSingleScan( evalfolder, scan_file, 'SaveSettings', '1'  )
%
% Inputs:
%   path            -   Path of folder containig .ini file with settings and
%                       where outputs are saved. 
%	scan_file		-	Scan to be processed. e.g. '191001_1500' 
%                       or complete path e.g.: path\*.ZON
% Optional inputs:
%   SaveSettings    -   Save changed settings to .ini?  
%                       Default=0
%   plotraw         -   plot Raw data. Default=0
%   staticauto      -   Automatic static correction. default =1
%   importStatic    -   Import static correction shift instead of computing
%                       it. Default=0. staticauto must be =1 for this option
%                       to work.
%
% Outputs:
%    errorcode  - Errorcode.    
%                 0: successful evaluation.
%                 1: .ini file was not found. It must be in path and named Eval*.ini 
%                 2: the raw data file could not be loaded! 
%                 3: The file with static correction line could not be imported.
%                 ... to be set!
%
% Example: 
%    errorcode = PreproSingleScan('N:\lawprae\LBI\Projects\101_upGPR\results\EvaluationHydYear2020','191001_1500')
%
%    
%
% Other m-files required: files in 'N:\lawprae\LBI\Projects\101_upGPR\postprocessing-scripts\Matlab\automaticProcessing'
% Subfunctions: 
% MAT-files required: none
%
% See also: seasonManEval,  PreproSeason
%
% Author: Achille Capelli, SLF
% % email: capelli@slf.ch 
% November 2019; Last revision: 

   
    %--------------------------------------------------------------------------------------------
    %-------------- Check inputs and set defaults if not defined --------------------------------
    %--------------------------------------------------------------------------------------------
    i_p = inputParser;
    validpartOfSeason = @(x) (isvector(x) && isnumeric(x) && length(x)==2) || (isscalar(x) &&  x == 0);
    i_p.FunctionName = 'PreproSingleScan';
    i_p.addRequired('folderpath',@ischar);
	i_p.addRequired('scan_file',@ischar);
    i_p.addParameter('SaveSettings',0,@(x) isnumeric(x) && isscalar(x));  
    i_p.addParameter('plotraw',0,@(x) isnumeric(x) && isscalar(x));
    i_p.addParameter('staticauto',1,@(x) isnumeric(x) && isscalar(x));
    i_p.addParameter('importStatic',0,@(x) isnumeric(x) && isscalar(x));
    
    i_p.parse(folderpath,scan_file, varargin{:});   
    
    saveSettings=i_p.Results.SaveSettings;   % Save setting to .ini if true
    
    global f_path;
    f_path = folderpath;
    

    % -------------------------------------------------------------------------
    %% ----------------- define initial settings and load stuff ----------------
    % -------------------------------------------------------------------------
    
    % Create new entry in logfile
    logToFile(['New evaluation of single scan.'],3)
%     logToFile(['folderpath: ' folderpath],4)
%     logToFile(['File to process: ' scan_file],4)
	% Add settings to Logfile
    logToFile('Inputs:',4)
    logToFile('---------',4)
    inputs=fieldnames(i_p.Results);
    for k=1:length(inputs)
        logToFile(join([inputs{k} ': '  string( i_p.Results.(inputs{k}))]),4)
    end
          
    
    
    
    % delete shit
    clear -regexp 'I' 'errorcode' 'inifile' 'procp';

    
    % colorbar
    load zcm.mat                % blue-red colormap
    load blkwhtred.mat          % black-white-red colormap
    
    % set output errorcode to no error
    errorcode=0;
    I=[];
    gprdat=[];
    
    % Load .ini. If not found display message and exit evaluation.
    try 
        inifile=dir([folderpath '\Eval*.ini']);
        inifile=[folderpath ,'\',inifile.name ];
        I = INI('File',inifile);
        % INI file reading
        I.read();
        logToFile(['path Eval*.ini: ',inifile],4)

    catch ME
        errorcode=1;
        %disp(ME.identifier)
        disp('!Error!')
        %disp(['The .ini file was not found in path=' folderpath '. It must be in path and named Eval*.ini'])
        msg=['The .ini file was not found in path=' folderpath '. It must be named Eval*.ini. The folder should not contain more than one Eval*.ini file.'];
        logToFile(msg,1,'dispMsg',1)
        return
    end
    
    
    % ----------------- Extract setting from Eval*.ini ----------------
    general=I.General;  % general settings valid for all steps
    
    procp=I.OneScan;    % setting for evaluating the single scans
    procp.cmap = eval(procp.cmap); 
    procp.n_samples=general.n_samples;
    % extract or pack variables
    vlen=I.OneScan.vlen;
    
    
    % check input and extract rigth path and file name
    if exist(scan_file)
        filepath =scan_file;
        filedate=scan_file(end-15:end-5);
    else
        daypath = fullfile(general.rawdatapath, [scan_file(1:6),'.Mis\']);
        filepath = fullfile(daypath, [scan_file '.ZON\']);
        filedate = scan_file;
    end
    logToFile(['filedate : ', filedate],4)
    logToFile(['filepath: ', filepath],4)
    
    
    
    % build output folder path and create it if not existing 
    outPath=fullfile(folderpath,'SingleScans\',filedate,'\');
    if ~exist(outPath, 'dir')  % make dir if doesn't exist
       mkdir(outPath)
    end
 
    
    % plots to be saves
    figures=[];
    fignames={};
    
    %% -------------------------------------------
    %%-------------- import gpr-data -------------
    %%--------------------------------------------
           
    gprflw(1).para.path     = filepath;
    gprflw(1).para.datafile = dir(fullfile(filepath, '*.dt'));
    gprflw(1).name = 'import_IDS';
    try
        gprdat(1).data = ReadIDS(fullfile(gprflw(1).para.path, gprflw(1).para.datafile.name),general.n_samples);
    catch
        logToFile(['The file: '  filedate ' could not be loaded!'],2,'dispMsg',1);
        errorcode=2;
        return 
    end
    
    n = 1; % counting pics
    k = 1; % counting data

    % cut data if to long
    if length(gprdat(k).data(1,:))>procp.nmax  % if too many samples are recorded, kill those traces
        l=length(gprdat(k).data(1,:));
        redtraces = gprdat(k).data(:,1:procp.nmax);
        gprdat(k).data = redtraces;
        disp(['Mission ' filedate sprintf(' has %d traces. Traces higher than %d were killed.', l, procp.nmax)])
    end
    
    %% -------------------------------------------------------------------------
    % ------------------- Calculate Variables for further processing--------------------------
    % -------------------------------------------------------------------------
    [gprdat,yend,deltat,ylab,f,nf,viewlen,stepsize]=calsInpVar(gprdat,1,procp,general.meastime,procp.vlen);    
    gprdat(k).date = filedate;
 
 
    % -------------------------------------------------------------------------
    % ------------------- PLOT RAW DATA ---------------------------------------
    % -------------------------------------------------------------------------

    if i_p.Results.plotraw == 1
        fig = figure(n);clf;
        imagesc(gprdat(k).data(1:viewlen,:))
        set(gca,'YTick',0:stepsize:gprdat(k).nr_of_samples)
        set(gca,'YTickLabel',0:deltat:yend)
        colormap(procp.cmap)
        xlabel('Trace','fontsize',12);
        ylabel(ylab,'fontsize',12);
        set(gca,'fontsize',12);
        title('Raw Data')
 
        hold on
        for i =1:length( procp.bound_SVD)
            plot( [procp.bound_SVD(i) procp.bound_SVD(i) ],[0 viewlen], 'color','g' )
        end
        startTwt  = round(procp.startTwt/gprdat(k).sampling_rate);
        plot([0;length(gprdat(k).data(1,:))],[startTwt ;startTwt],'--k');
        
        %add figure to saveing list
        figures(end+1)=n;
        fignames{end+1}='raw.png';
            
        n = n+1;
        k = k+1;
        
    else
        k = k+1;
    end
    
    
    
    %% ------------------------------------------------------------------
    % call function plotting raw data and change settings if necessary
    %------------------------------------------------------------------

    [bound_SVD,exc,startTwt]=find_bounds(gprdat,procp,procp.bound_SVD,1,1,1,'keep_plot',1);
    procp.bound_SVD=bound_SVD;
    procp.startTwt=startTwt;

    % add exception to exception list if necessary
    if exc
       I.General.exceptions(end+1)={[scan_file '.ZON\']}; 
    end
    
    %add figure to saveing list
    fig_findbounds=gcf;
    figures(end+1)=fig_findbounds.Number;
    fignames{end+1}='check_settings.png';
    
    
    %% -------------------------------------------------------------------------
    % ------------------- BACKGROUND REMOVAL ----------------------------------
    % -------------------------------------------------------------------------
    

    [ gprdat, k ] = backgroundrem( gprdat, k, procp.background, procp );
    
    figBackground = figure(n); clf;
    subplot(1,2,1)
    imagesc(gprdat(k-2).data(1:viewlen,:))
    set(gca,'YTick',0:stepsize:gprdat(k-2).nr_of_samples)
    set(gca,'YTickLabel',0:deltat:yend)
    colormap(procp.cmap)
    xlabel('Trace','fontsize',12);
    ylabel(ylab,'fontsize',12);
    set(gca,'fontsize',12);
    title('Before Background Removal')
    hold on
    for i =1:length( procp.bound_SVD)
        plot( [procp.bound_SVD(i) procp.bound_SVD(i) ],[0 viewlen], 'color','g' )
    end
    startTwt_s  = round(procp.startTwt/gprdat(k-2).sampling_rate);
    plot([0;length(gprdat(k-2).data(1,:))],[startTwt_s ;startTwt_s],'--k');
    
    
    subplot(1,2,2)
    imagesc(gprdat(k-1).data(1:viewlen,:))
    set(gca,'YTick',0:stepsize:gprdat(k-1).nr_of_samples)
    set(gca,'YTickLabel',0:deltat:yend)
    colormap(procp.cmap)
    xlabel('Trace','fontsize',12);
    ylabel(ylab,'fontsize',12);
    set(gca,'fontsize',12);
    title('After Background Removal')
    hold on
    plot([0;length(gprdat(k-1).data(1,:))],[startTwt_s ;startTwt_s],'--k');
    
    
    %add figure to saveing list
    figures(end+1)=n;
    fignames{end+1}='BackgroundRem.png';

    n = n+1;


    gprdat(k-1).fdata = sum(abs(fft(gprdat(k-1).data))'); % calculate for further steps

    

    %% -------------------------------------------------------------------------
    % ------------------- BUTTERWORTH BANDPASS FILTER -------------------------
    % -------------------------------------------------------------------------  
    if procp.butterbp1 == 1
        if ((procp.fl > 0) & (procp.fh > 0))

            % apply filter
            nyq = 0.5 / gprdat(k-1).sampling_rate * 1000;
            fl = procp.fl/nyq;
            fh = procp.fh/nyq;
            [bb,aa] = butter(procp.ford,[fl fh]);
            gprdat(k).data = filter(bb,aa,gprdat(k-1).data);
            gprdat = renamedata(gprdat,k);
            gprdat(k).fdata = sum(abs(fft(gprdat(k).data))');

            % plot data
            figBandpass = figure(n); clf;
            plot(f,gprdat(k-1).fdata(1:nf),'b')
            hold on
            plot(f,gprdat(k).fdata(1:nf),'r');
            xlabel('Frequency [MHz]','fontsize',12);
            ylabel('Amplitude [1]','fontsize',12);
            title('Amplitude Spectrum');
            legend('Before Butterworth Bandpass Filter','After Butterworth Bandpass Filter');
            xlim([0 6000])
            hold off
            %add figure to saveing list
            figures(end+1)=n;
            fignames{end+1}='FreqSpectr.png';

            n = n+1;

            fig = figure(n); clf;
            imagesc(gprdat(k).data(1:viewlen,:))
            set(gca,'YTick',0:stepsize:gprdat(k).nr_of_samples)
            set(gca,'YTickLabel',0:deltat:yend)
            colormap(procp.cmap)
            xlabel('Trace','fontsize',12);
            ylabel(ylab,'fontsize',12);
            set(gca,'fontsize',12);
            title('After Butterworth Bandpass Filter');
            startTwt_s  = round(procp.startTwt/gprdat(k).sampling_rate);
            hold on         
            plot([0;length(gprdat(k).data(1,:))],[startTwt_s ;startTwt_s],'--k');

            %add figure to saveing list
            figures(end+1)=n;
            fignames{end+1}='Bandpass.png';
            n = n+1;
            k = k+1;
        end
    end
    
    %% -------------------------------------------------------------------------
    % ------------------- AUTOMATIC STATIC CORRECTION -------------------------
    % -------------------------------------------------------------------------

    if i_p.Results.staticauto == 1 % follow the phase after a pick %
        
        if ~i_p.Results.importStatic
            dates     = linspace(1,gprdat(k-1).nr_of_traces,gprdat(k-1).nr_of_traces);
            samples   = linspace(1,gprdat(k-1).nr_of_samples,gprdat(k-1).nr_of_samples);

            % convert startTw from time units to samples
            startTwt_s  = round(procp.startTwt/gprdat(k-1).sampling_rate); 

            % follow phase
            [newPickTwt, newPickTwt_zero] = manualPick_FollowPhase2(gprdat(k-1).data, dates, samples, procp.startDate, startTwt_s);

            newPickTwt_zero = newPickTwt_zero';
            newPickTwt = newPickTwt_zero; % use follow zero crossing 
            lenNewPick=size(newPickTwt,1); 

            % fill up missing 
            if procp.startDate > 1
                newPickTwt2 = zeros(gprdat(k-1).nr_of_traces ,1);
                % fill up < procp.startDate
                newPickTwt2(1:procp.startDate-1) = newPickTwt(1);

                % fill up < startDate
                newPickTwt2(lenNewPick+procp.startDate:end) = newPickTwt(lenNewPick);

                newPickTwt2(procp.startDate:lenNewPick+procp.startDate) = newPickTwt(:);
                newPickTwt = newPickTwt2;
            end

            % transform froms samples to time units
            [tti] = (newPickTwt*gprdat(k-1).sampling_rate)';
        
        else    % import static correction
                try 
                    loadfile = fullfile(folderpath,'staticCorShift.mat');
                    load(loadfile,'tti','newPickTwt');
                    dates     = linspace(1,gprdat(k-1).nr_of_traces,gprdat(k-1).nr_of_traces);
                catch ME
                    logToFile(['The File: ',loadfile,' could not be loaded! Continuing with 0 shift for static correction.'],2,'dispMsg',1)
                    newPickTwt= zeros(gprdat(k-1).nr_of_traces,1);
                    tti=newPickTwt;
                    dates     = linspace(1,gprdat(k-1).nr_of_traces,gprdat(k-1).nr_of_traces);
                    errorcode=2;
                end
                
        end

        
        
        % Align traces  
        for a = 1:gprdat(k-1).nr_of_traces
            if (newPickTwt(a) > 0)
                i1 = round(newPickTwt(a)-min(newPickTwt)+1);
                gprdat(k).data(1:gprdat(k-1).nr_of_samples-i1+1,a) = gprdat(k-1).data(i1:end,a);
            else
                gprdat(k).data(:,a) = gprdat(k-1).data(:,a);
            end
        end
        
        
        % add new data to data container 
        gprdat = renamedata(gprdat,k);
        gprdat(k).nr_of_samples = length(gprdat(k).data(:,1));
%         viewlen = length(gprdat(k).data(:,1));
        gprdat(k).fdata = sum(abs(fft(gprdat(k).data))');

        % add automatic static correction curve to data to be saved
        gprdat(k).newPickTwt=newPickTwt;

        
        
        % figure plots
         
        fig = figure(n);clf;
        hold on
        plot([0;length(newPickTwt)],[startTwt_s ;startTwt_s],'--k');
        plot(newPickTwt)
        legend('Static Correction line')
        %add figure to saveing list
        figures(end+1)=n;
        fignames{end+1}='statCorrLine.png';
        n = n+1;
        
     
        fig = figure(n); clf;
        subplot(2,1,1)
        imagesc(gprdat(k-1).data(1:viewlen,:))
        hold on
        plot(dates,newPickTwt,'LineWidth',2,'Color','m')
        set(gca,'YTick',0:stepsize:gprdat(k-1).nr_of_samples)
        set(gca,'YTickLabel',0:deltat:yend)
        colormap(procp.cmap)
        xlabel('Trace','fontsize',12);
        ylabel(ylab,'fontsize',12);
        set(gca,'fontsize',12);
        title('Static correction line')
        startTwt_s  = round(procp.startTwt/gprdat(k).sampling_rate);
        plot([0;length(gprdat(k).data(1,:))],[startTwt_s ;startTwt_s],'--k');

        subplot(2,1,2)
        imagesc(gprdat(k).data(1:viewlen,:))
        set(gca,'YTick',0:stepsize:gprdat(k).nr_of_samples)
        set(gca,'YTickLabel',0:deltat:yend)
        colormap(procp.cmap)
        xlabel('Trace','fontsize',12);
        ylabel(ylab,'fontsize',12);
        set(gca,'fontsize',12);
        title('Static correction')
        
        %add figure to saveing list
        figures(end+1)=n;
        fignames{end+1}='staticCorr.png';
        n=n+1;
        k = k+1;
    end

    %% -------------------------------------------------------------------------
    % ---------------------- STACK TRACES -------------------------------------
    % -------------------------------------------------------------------------
    gprdat(k).data = sum(gprdat(k-1).data,2);
    gprdat = renamedata(gprdat,k);
    gprdat(k).date = filedate;
    
    
    % plot stacked 
    stacked=gprdat(k).data;
    fig = figure(n); clf;
    plot(stacked,[length(stacked):-1:1])
    set(gca,'YTick',0:stepsize:gprdat(k).nr_of_samples)
    set(gca,'YTickLabel',yend:-deltat:0)
    ylabel(ylab,'fontsize',12);
    %add figure to saveing list
    figures(end+1)=n;
    fignames{end+1}='stacked.png';
    
    k = k+1;
    
    %% -------------------------------------------------------------------------
    % -------- save data, parametres in INI file, and plost --------------------
    % --------------------------------------------------------------------------
    
    % save data of static correction shift
    savefile = fullfile(outPath,'staticCorShift.mat');
    save(savefile,'tti','newPickTwt');
    
%     savefile = fullfile(outPath,'aligned.mat');
%     aligned=gprdat(k-2).data;
%     save(savefile,'aligned');
    
    savefile = fullfile(outPath,'stacked.mat');
    stacked=gprdat(k-1).data;
    save(savefile,'stacked');
    
    % save changed settings to .ini
    if saveSettings
        [err(1),msg(1)]=writeToINI(inifile,'startTwt', procp.startTwt,'OneScan');
        [err(2),msg(2)]=writeToINI(inifile,'bound_SVD', ['[', char(strjoin(string(procp.bound_SVD),', ')),']'],'OneScan');
        [err(3),msg(3)]=writeToINI(inifile,'exceptions', ['{''' strjoin(I.General.exceptions,''', ''') '''}'],'General');
        
        for i=1:length(err)
            if err(i)
                logToFile(msg(i),2);
            end
        end
    end

    
    % save plots
    for i=1:length(figures)
        try
            figure(figures(i))
            plotfile = fullfile(outPath,fignames{i});
            print(plotfile,'-dpng','-r300');
            print('-dpng','-r300',plotfile);
        catch
            logToFile(['Figure: ',fignames{i},' could not be saved!'],2);
            logToFile(plotfile,4);
        end
    end

    % write to logfile
    logToFile(' ',4,'dispMsg',0);
    logToFile('Evaluation successful',0,'dispMsg',1);
    % save setting in .INI in a logfile
    INItoLog(inifile,fullfile(outPath,'INIlog.txt'));
    disp(['Errorcode: ',num2str(errorcode)])
end


