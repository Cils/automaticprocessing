function [ errorcode] = PreproSeason( folderpath, varargin )
%PreproSeason - Processing upGPR Data of one season.
% Workflow to read in all raw data, process and save it.
% 
% Syntax:  [ errorcode ] = PreproSeason( evalfolder )
%          [ errorcode ] = PreproSeason( evalfolder,'justnew',justnew,'partOfSeason' )
%
% Inputs:
%   path            -   Path of folder containig .ini file with settings and
%                       where outputs are saved. 
% Optional inputs:
%   justnew         -   Check just new scans (=1) or process entire season (=0). 
%                       Default=1
%   partOfSeason    -   Boundaries of part of season to process or equal to 0. 
%                       E.g.:  partOfSeason=[190505, 190615];
%
% Input files:
%   Scans raw data
% 	Eval*.ini.
% 	staticCorShift.mat (optional. Used just, when a fix static correction shift is used instead of computing it. )
% 
% Outputs:
%    errorcode  - Errorcode.    
%                 0: successful evaluation.
%                 1: .ini file was not found. It must be in path and named Eval*.ini
%                 2: Static correction file was not found.
%                 ... to be set!
%
% Output files:
%   allScans.mat  (data file)
% 	scan_info.mat (processing settings for single scans)
% 	evalLog.txt
% 	INIlog.txt

% Example: 
%    errorcode = PreproSeason('N:\lawprae\LBI\Projects\101_upGPR\results\EvaluationHydYear2020')
%    or
%    errorcode = PreproSeason(path,'justnew',1,'partOfSeason',[190505, 190615])
%    
%
% Other m-files required: files in 'N:\lawprae\LBI\Projects\101_upGPR\postprocessing-scripts\Matlab\automaticProcessing'
% Subfunctions: none
% MAT-files required: none
%
% See also: seasonManEval,  PreprocSingleScan
%
% Author: Achille Capelli, SLF
% % email: capelli@slf.ch 
% November 2019; Last revision: 

%------------- BEGIN CODE --------------
   
    %--------------------------------------------------------------------------------------------
    %-------------- Check inputs and set defaults if not defined --------------------------------
    %--------------------------------------------------------------------------------------------
    i_p = inputParser;
    validpartOfSeason = @(x) (isvector(x) && isnumeric(x) && length(x)==2) || (isscalar(x) &&  x == 0);
    i_p.FunctionName = 'PreproSeason';
    i_p.addRequired('folderpath',@ischar);
    i_p.addParameter('justnew',1,@(x) isnumeric(x) && isscalar(x));    
    i_p.addParameter('partOfSeason',0,validpartOfSeason);
       
    i_p.parse(folderpath, varargin{:});   
    
    if length(i_p.Results.partOfSeason)>1
        partOfSeason_bounds=i_p.Results.partOfSeason;
        partOfSeason=1;
    else
        partOfSeason=0;
    end
    justnew=i_p.Results.justnew;   % check just new data 
    
    global f_path;
    f_path = folderpath;
    

    % -------------------------------------------------------------------------
    %% ----------------- define initial settings and load stuff ----------------
    % -------------------------------------------------------------------------
    
    % Create new entry in logfile
    logToFile(['New evaluation.'],3)
%     logToFile(['folderpath: ' folderpath],4)
    % Add settings to Logfile
    logToFile('Inputs:',4)
    logToFile('---------',4)
    inputs=fieldnames(i_p.Results);
    for k=1:length(inputs)
        logToFile(join([inputs{k} ': '  string( i_p.Results.(inputs{k}))]),4)
    end
          
    
    
    
    % delete shit
    clear 'I' 'errorcode' 'inifile' 'procp';
    clear -regexp ^Mon ^Tue ^Wed;
    
    % colorbars
    load zcm.mat                % blue-red colormap
    load blkwhtred.mat          % black-white-red colormap
    
    % set output errorcode to no error
    errorcode=0;

    % Load .ini. If not found display message and exit evaluation.
    try 
        inifile=dir([folderpath '\Eval*.ini']);
        inifile=[folderpath ,'\',inifile.name ];
        I = INI('File',inifile);
        % INI file reading
        I.read();
        logToFile(['path Eval*.ini: ',inifile],4)

    catch ME
        errorcode=1;
        %disp(ME.identifier)
        disp('!Error!')
        %disp(['The .ini file was not found in path=' folderpath '. It must be in path and named Eval*.ini'])
        msg=['The .ini file was not found in path=' folderpath '. It must be named Eval*.ini. The folder should not contain more than one Eval*.ini file.'];
        logToFile(msg,1,'dispMsg',1)
        return
    end
    
    
    
    % ----------------- Extract setting from Eval*.ini ----------------
    general=I.General;  % general settings valid for all steps
    
    procp=I.OneScan;    % setting for evaluating the single scans
    procp.n_samples=general.n_samples;
    
    % create other variables
    dataFile=fullfile(folderpath,  'allScans.mat');
    
    % extract or pack variables
    vlen=I.OneScan.vlen;
    
    
    
    
    %% -------------------------------------------
    %%---- process data single scans--------------
    %%--------------------------------------------
    
    % get list of missions
    disp(fullfile(general.rawdatapath ,'*.MIS'))
    listingmissions = dir(fullfile(general.rawdatapath ,'*.MIS'));
%     logToFile(['Listingmissions: ' , listingmissions.name],4,'dispMsg',1)

    %load cell arrays containing information for single scan_info or create
    %empty array
    try 
        load([folderpath '\scan_info.mat'],'scan_info')
        r=length(scan_info);
    catch ME
        disp(ME.identifier)
%         disp('Creating new scan_info!!')
%         disp('Preprocessing without scan_info!!!!!!!!!')
        logToFile('Creating new scan_info! Preprocessing without scan_info.',0,'dispMsg',1)
        logToFile(ME.identifier,4)
        scan_info.names=[]; % scan name
        r=0;  % index for scan_info
    end
    times=zeros(1,length(listingmissions));

    
    % load previusly processed data if intend to reprocess just part of data
    if partOfSeason || justnew
        try
            load(dataFile,'dataScans');
        catch ME
            logToFile('File: wholeSeason.mat could not be found! Continuing without it!',2,'dispMsg',1)
            logToFile(ME.identifier,4)
            disp(ME.identifier)
        end   
    end


    for aaa = 1:length(listingmissions)  % loop through days
%         disp(aaa)
        tStart = tic;

        daypath = fullfile(general.rawdatapath, listingmissions(aaa).name,'\');
%         disp(daypath)
        listingzon = dir(fullfile(daypath, '*.ZON'));
        
        dayIsException=0;
        % check if mission is in exceptionslist and skip if the case
        if ismember(listingmissions(aaa).name,general.exceptions)
            %disp(['       ' listingzon(bbb).name 'was skipped!'])
            logToFile([listingmissions(aaa).name ' was skipped!'],0,'dispMsg',1)
            dayIsException=1;
        end
        
        
        if length(listingzon) > 48 % measurement on day 121 and 128 (year 15/16) have more than 48 measurements. Kill too many traces.
            ccc = 48;
            logToFile([daypath,' had more than 48 measurements. The ones too many were killed.'],2,'dispMsg',1)
        else
            ccc = length(listingzon);
        end

        % loop through missions of one day 
        for bbb = 1:ccc  
%             disp(listingzon(bbb).name(1:11))
            
            message=''; % Message to be printed in logfile
            
            % if intend to process just part of season check if mission is in period to process
            if partOfSeason
                dati=str2double([listingzon(bbb).name(1:6) '.' listingzon(bbb).name(8:11)]); %
                if not( (partOfSeason_bounds(1)<= dati) & (dati<= partOfSeason_bounds(2)) )
                    disp(['       ' listingzon(bbb).name ' was skipped because not in part of season to be processed!'])
%                     logToFile([num2str(dati) ' was skipped!'],4,'dispMsg',1)
                    continue
                end
            end

%             disp(['       ' listingzon(bbb).name])
            
            filedate = listingzon(bbb).name(1:11);
            sessionpath = fullfile(daypath, listingzon(bbb).name, '\');
            upGPRsession = dir(fullfile(sessionpath, '*.dt'));

            %------------------------------------------------------------------
            % load information from scan_info and make a new one if not found
            %------------------------------------------------------------------
            try
                idx = find(ismember({scan_info.names}, listingzon(bbb).name(1:end-4)));
            catch
                idx=[];
            end

            if length(idx)==1   % scan is already in scan_info
                r=idx;

                % old code
%                 bound_SVD=scan_info(r).bound_SVD;
%                 procp.bound_SVD=bound_SVD;
%                 procp.startTwt = scan_info(r).startTwt;
                scan_info(r).startTwt=procp.startTwt;
                scan_info(r).bound_SVD=procp.bound_SVD;
                exc=scan_info(r).exception;
                scan_info(r).new=0;
                

            else  % scan isn't in scan_info. Creating new entry.
                if r>0
                    r=length(scan_info)+1;
                else
                    r=1;
                end
                scan_info(r).names=listingzon(bbb).name(1:end-4);
                scan_info(r).bound_SVD= procp.bound_SVD;
                scan_info(r).exception=0;
                scan_info(r).startTwt=procp.startTwt;
                scan_info(r).new=1;
                
                message=[message ' New scan.'];
            end
            

            
            % check if mission is in exceptionslist and skip if the case
            if ismember(listingzon(bbb).name,general.exceptions) | scan_info(r).exception | dayIsException
                scan_info(r).exception=1;
                disp(['       ' listingzon(bbb).name ' is in exceptionlist and was skipped!'])
                %logToFile([listingzon(bbb).name 'was skipped!'],0,'dispMsg',1)
                
                %----------  delete exceptions from data file (dataScans) ---------------- 
                if  exist('dataScans')
                    % find entry to substitute otherwise append at end of data array
                    j = find(ismember({dataScans.date}, filedate));
                    switch length(j)
                        case 0
                            disp(['                          No entry to delete!'])
                        otherwise  % one or more entries. Delete.
                            dataScans(j) = [];
                            logToFile([filedate ' is in exceptionlist. Deleted from datafile'],2,'dispMsg',1); 
                    end
                end
                
                continue;
            end
            
            % Files already processed can be skipped]
            if (justnew && ~scan_info(r).new)  % skip files not new
                disp(['       ' listingzon(bbb).name ' was skipped because just the new file are beeing processsed!'])
%                 logToFile(['skip:', scan_info(r).names],0,'dispMsg',1)
                continue;
            end
                        


            %% import gpr-data

            gprflw(1).para.path     = sessionpath;
            gprflw(1).para.datafile = upGPRsession;
            gprflw(1).name = 'import_IDS';
            try
                gprdat(1).data = ReadIDS(fullfile(gprflw(1).para.path, gprflw(1).para.datafile.name),general.n_samples);
            catch
                logToFile(['The file: ' listingzon(bbb).name ' could not be loaded!'],2,'dispMsg',1);
                continue 
            end
            n = 1; % counting pics
            k = 1; % counting data

            % cut data if to long
            if length(gprdat(k).data(1,:))>procp.nmax  % if too many samples are recorded, kill those traces
                l=length(gprdat(k).data(1,:));
                redtraces = gprdat(k).data(:,1:procp.nmax);
                gprdat(k).data = redtraces;
                disp(['Mission ' listingzon(bbb).name sprintf(' has %d traces. Traces higher than %d were killed.', l, procp.nmax)])
            end



            % -------------------------------------------------------------------------
            % ------------------- Calculate Variables for further processing--------------------------
            % -------------------------------------------------------------------------
            [gprdat,yend,deltat,ylab,f,nf,viewlen,stepsize]=calsInpVar(gprdat,1,procp,general.meastime,procp.vlen);    

            k = k+1;

            %%
            % -------------------------------------------------------------------------
            % ------------------- BACKGROUND REMOVAL ----------------------------------
            % -------------------------------------------------------------------------
            if procp.background > 0
                [ gprdat, k ] = backgroundrem( gprdat, k, procp.background, procp );
            end

            %%
            % -------------------------------------------------------------------------
            % ------------------- BUTTERWORTH BANDPASS FILTER -------------------------
            % -------------------------------------------------------------------------

            if procp.butterbp1 == 1
                if (procp.fl & procp.fh > 0)
                    nyq = 0.5 / gprdat(k-1).sampling_rate * 1000;
                    fl = procp.fl/nyq;
                    fh = procp.fh/nyq;
                    [bb,aa] = butter(procp.ford,[fl fh]);
                    gprdat(k).data = filter(bb,aa,gprdat(k-1).data);
                    gprdat = renamedata(gprdat,k);
                    gprdat(k).fdata = sum(abs(fft(gprdat(k).data))');

                    k = k+1;
                end
            end

            
            % -------------------------------------------------------------------------
            % ------------------- AUTOMATIC STATIC CORRECTION -------------------------
            % -------------------------------------------------------------------------

            if procp.staticauto == 1 % follow the phase after a pick %
                
                if ~procp.importStatic % find static correction line
                    
                    dates     = linspace(1,gprdat(k-1).nr_of_traces,gprdat(k-1).nr_of_traces);
                    samples   = linspace(1,gprdat(k-1).nr_of_samples,gprdat(k-1).nr_of_samples);

                    % convert startTw from time units to samples
                    startTwt_s  = round(procp.startTwt/gprdat(k-1).sampling_rate); 

                    % follow phase
                    [newPickTwt, newPickTwt_zero] = manualPick_FollowPhase2(gprdat(k-1).data, dates, samples, procp.startDate, startTwt_s);

                    newPickTwt_zero = newPickTwt_zero';
                    newPickTwt = newPickTwt_zero;
                    lenNewPick=size(newPickTwt,1); % use follow zero crossing 

                    % fill up missing 
                    if procp.startDate > 1
                        newPickTwt2 = zeros(gprdat(k-1).nr_of_traces ,1);
                        % fill up < procp.startDate
                        newPickTwt2(1:procp.startDate-1) = newPickTwt(1);

                        % fill up < startDate
                        newPickTwt2(lenNewPick+procp.startDate:end) = newPickTwt(lenNewPick);

                        newPickTwt2(procp.startDate:lenNewPick+procp.startDate) = newPickTwt(:);
                        newPickTwt = newPickTwt2;
                    end

                    % transform froms samples to time units
                    [tti] = (newPickTwt*gprdat(k-1).sampling_rate)';
               
                else    % import static correction line
                        try 
                            savefile = fullfile(folderpath,'staticCorShift.mat');
                            load(savefile,'tti','newPickTwt');
                            dates     = linspace(1,gprdat(k-1).nr_of_traces,gprdat(k-1).nr_of_traces);
                        
                        catch ME
                            logToFile(['The File: ',savefile,' could not be loaded!'],1,'dispMsg',1)
                            errorcode=2;
                            return
                        end

                end
                
                
                                
                % Align traces  
                for a = 1:gprdat(k-1).nr_of_traces
                    if (tti(a) > 0)
                        i1 = round(newPickTwt(a)-min(newPickTwt)+1);
                        gprdat(k).data(1:gprdat(k-1).nr_of_samples-i1+1,a) = gprdat(k-1).data(i1:end,a);
                    else
                        gprdat(k).data(:,a) = gprdat(k-1).data(:,a);
                    end
                end
                
                %  
                gprdat = renamedata(gprdat,k);
                gprdat(k).nr_of_samples = length(gprdat(k).data(:,1));
                viewlen = length(gprdat(k).data(:,1));
                gprdat(k).fdata = sum(abs(fft(gprdat(k).data))');
                
                % add automatic static correction curve to data to be saved
                gprdat(k).newPickTwt=newPickTwt;
                
                
                k = k+1;
            end


            % -------------------------------------------------------------------------
            % ------------------- BUTTERWORTH BANDPASS FILTER -------------------------
            % -------------------------------------------------------------------------

            if procp.butterbp2 == 1
                if (procp.fl & procp.fh > 0)
                    nyq = 0.5 / gprdat(k-1).sampling_rate * 1000;
                    fl = procp.fl/nyq;
                    fh = procp.fh/nyq;
                    [bb,aa] = butter(procp.ford,[fl fh]);
                    gprdat(k).data = filter(bb,aa,gprdat(k-1).data);
                    gprdat = renamedata(gprdat,k);
                    gprdat(k).fdata = sum(abs(fft(gprdat(k).data))');


                    k = k+1;
                end
            end

            % -------------------------------------------------------------------------
            % ---------------------- STACK TRACES -------------------------------------
            % -------------------------------------------------------------------------

            if procp.stacktraces == 1
                for i = 1:gprdat(k-1).nr_of_samples
                    stack(i) = sum(gprdat(k-1).data(i,:));
                end
                gprdat(k).data = stack';
                gprdat = renamedata(gprdat,k);
                gprdat(k).date = filedate;
                
                k = k+1;
            end 


            %% -------------------------------------------------------------------------
            % ---------------------- pack data for exporting----------------------------
            % -------------------------------------------------------------------------
            
            % 
            try
                logToFile([gprdat(k-1).date ' was processed.', message],0,'dispMsg',1);
            catch ME
                disp('Unable to write to logfile!!!')
                disp([gprdat(k-1).date ' was processed.', message]);
                disp(ME)
            end
            
            %gprdatsession(aaa,bbb) = gprdat(k-1);
            if  exist('dataScans')
                % find entry to substitute otherwise append at end of data array
                j = find(ismember({dataScans.date}, gprdat(k-1).date));
                switch length(j)
                    case 0
                        dataScans(end+1)= gprdat(k-1);
                    case 1   % scan is already in scan_info
                        dataScans(j)= gprdat(k-1);
                    otherwise  % more than one entry. Keep first and delete rest.
                        dataScans(j(1)) = gprdat(k-1);
                        dataScans(j(2:end)) = [];
                        logToFile([gprdat(k-1).date ' had more than one entry in datafile. Just one was kept.'],2,'dispMsg',1); 
                end
            else
                dataScans(1)= gprdat(k-1);
            end
         

        end

        time=toc(tStart);
        disp(['Time for processing ',listingmissions(aaa).name,': ', num2mstr(time), ' s'])
        times(aaa)=time;
    end
      
    
    %% -------------------------------------------------------------------------
    % ---------------------- save and sort data ----------------------------
    % -------------------------------------------------------------------------
    
    % sort data
    [~,idx]=sort({dataScans.date});
    dataScans=dataScans(idx);
    
    
    save(dataFile, 'dataScans');
    %save(dataFile, 'gprdatsession');   
    save([folderpath '\scan_info.mat'], 'scan_info')
    
    % write to logfile
    logToFile(' ',4,'dispMsg',0)
    logToFile(['End of evaluation. Total processing time: ' num2mstr(sum(times)) ' s' ],0,'dispMsg',1)
    % ToDo: Add more informations?
    
    % save setting in .INI in a logfile
    INItoLog(inifile,fullfile(f_path,'INIlog.txt'))

    
    disp(['Errorcode: ',num2str(errorcode)])
end
  
    
    
