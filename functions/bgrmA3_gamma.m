function [ gprdat, k ] = bgrmA3_gamma( gprdat, k, background, procp )
% BACKGROUND REMOVAL either median filtering or SVD.
% background = 1 -> median filtering and so on..
% median = 1, svd gerade gerade = 2, svd steigung steigung = 3, svd gerade steigung = 4, svd steigung gerade = 5, svd steigung alles = 6, svd gerade alles = 7

% median
if background == 1
    %disp('Time picks have to be made on the whole line of traces (1444). If this is not the case, an error will show up because of tti(a).')
    if gprdat(k-1).nr_of_traces == 1444
        %%% filter calculations
        sumtr1 = median(gprdat(k-1).data(:,178:285),2);
        sumtr2 = median(gprdat(k-1).data(:,500:615),2);
        sumtr3 = median(gprdat(k-1).data(:,820:935),2);
        sumtr4 = median(gprdat(k-1).data(:,1144:1257),2);
        sumtr = median([sumtr1,sumtr2,sumtr3,sumtr4],2);
    elseif gprdat(k-1).nr_of_traces > 615
        sumtr1 = median(gprdat(k-1).data(:,178:285),2);
        sumtr2 = median(gprdat(k-1).data(:,500:615),2);
        sumtr = median([sumtr1,sumtr2],2);
    elseif gprdat(k-1).nr_of_traces > 285
        sumtr1 = median(gprdat(k-1).data(:,178:285),2);
    else
        sumtr = median(gprdat(k-1).data,2);
    end
    %% filter implementation for all trace
    
    %sumtr = median(gprdat(k-1).data,2); % horizontal filtering.   svd .. Fabian singular value decomp
    for b = 1:gprdat(k-1).nr_of_traces
        gprdat(k).data(:,b) = gprdat(k-1).data(:,b) - sumtr;
    end;
    gprdat = renamedata(gprdat,k);
    
    
    k = k+1;
end

%% svd gerade gerade
if background == 2
    d = gprdat(k-1).data;
    [m, n]=size(d);
    
    % Build filter
    bound = [1 177 286 499 616 819 936 1143 1258 n]; % Boundaries, where the upGPR moves up and down
    
    if n >= 1444
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp3 = d(:,bound(5):bound(6)); % gerade
        dtmp4 = d(:,bound(7):bound(8)); % gerade
        dtmp5 = d(:,bound(9):bound(10)); % gerade
        dtmp = [dtmp1 dtmp2 dtmp3 dtmp4 dtmp5];
    elseif n >= bound(8) && n < 1444
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp3 = d(:,bound(5):bound(6)); % gerade
        dtmp4 = d(:,bound(7):bound(8)); % gerade
        dtmp = [dtmp1 dtmp2 dtmp3 dtmp4];
    elseif n >= bound(6) && n < bound(8)
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp3 = d(:,bound(5):bound(6)); % gerade
        dtmp = [dtmp1 dtmp2 dtmp3];
    elseif n >= bound(4) && n < bound(6)
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp = [dtmp1 dtmp2];
    elseif n >= bound(2) && n < bound(4)
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp = [dtmp1];
    else
        dtmp = d;
    end
    
    % Actual Filter
    [~,s_filter,~] = svd(dtmp);
    s_filter(1,1) = 0;
    % s_filter(2,2) = 0;
    % s_filter(3,3) = 0;
    [m_f, n_f] = size(s_filter);
    done = 1;
    b = 1;
    while (done == 1)
        a = 1;
        done = 0;
        m1 = b;
        m2 = b + m_f -1;
        if (m2 >= m), m2 = m; m1 = m - m_f + 1; done = 0; end;
        mmeffidx = m2 - m1;
        while(done == 0)
            n1 = a;
            n2 = a+n_f-1;
            if (n2 >= n), n2 = n; n1 = n - n_f + 1; done = 1; end;
            nneffidx = n2 - n1;
            dtmp = d(m1:m2,n1:n2);
            [u,~,v] = svd(dtmp);
            dtmp = u*s_filter*v';
            d(m1:m2,n1:n2) = dtmp;
            a = a + n_f;
        end
        b = b + m_f;
        if (m2 >= m), m2 = m; done = 2; end;
    end
    
    % Apply filter to
    
    if n >= 1444
        d1 = d(:,bound(1):bound(2)); % gerade
        d2 = d(:,bound(3):bound(4)); % gerade
        d3 = d(:,bound(5):bound(6)); % gerade
        d4 = d(:,bound(7):bound(8)); % gerade
        d5 = d(:,bound(9):bound(10)); % gerade
        d = [d1 d2 d3 d4 d5];
    elseif n >= bound(8) && n < 1444
        d1 = d(:,bound(1):bound(2)); % gerade
        d2 = d(:,bound(3):bound(4)); % gerade
        d3 = d(:,bound(5):bound(6)); % gerade
        d4 = d(:,bound(7):bound(8)); % gerade
        d = [d1 d2 d3 d4];
    elseif n >= bound(6) && n < bound(8)
        d1 = d(:,bound(1):bound(2)); % gerade
        d2 = d(:,bound(3):bound(4)); % gerade
        d3 = d(:,bound(5):bound(6)); % gerade
        d = [d1 d2 d3];
    elseif n >= bound(4) && n < bound(6)
        d1 = d(:,bound(1):bound(2)); % gerade
        d2 = d(:,bound(3):bound(4)); % gerade
        d = [d1 d2];
    elseif n >= bound(2) && n < bound(4)
        d1 = d(:,bound(1):bound(2)); % gerade
        d = [d1];
    else
        d = d;
    end
    
    
    gprdat(k).data = d;
    gprdat = renamedata(gprdat,k);
    
    k = k+1;
end
%% svd steigung steigung
if background == 3
    d = gprdat(k-1).data;
    [m, n]=size(d);
    
    % Build filter
    bound = [1 177 286 499 616 819 936 1143 1258 n]; % Boundaries, where the upGPR moves up and down
    
    if n >= bound(9)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d3 = d(:,bound(6):bound(7)); % Steigung
        d4 = d(:,bound(8):bound(9)); % Steigung
        d = [d1 d2 d3 d4];
    elseif n >= bound(7) && n < bound(9)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d3 = d(:,bound(6):bound(7)); % Steigung
        d = [d1 d2 d3];
    elseif n >= bound(5) && n < bound(7)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d = [d1 d2];
    elseif n >= bound(3) && n < bound(5)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d = [d1];
    else
        d = d;
    end
    
    n = length(d(1,:));
    %dv = zeros(size(d));
    a = 1;
    done = 0;
    while(done == 0)
        n2 = a;
        n3 = a+procp.svd_len-1;
        n1 = n2 - procp.svd_len;
        n4 = n3 + procp.svd_len;
        if (n1 < 1), n1 = 1; end;
        if (n3 >= n), n3 = n; done = 1; end;
        if (n4 > n), n4 = n; end;
        nneffidx = n3 - n2;
        dtmp = d(:,n1:n4);
        [u,s,v] = svd(dtmp);
        s(1,1) = 0;
        %   s(2,2) = 0;
        dtmp = u*s*v';
        d(:,n2:n3) = dtmp(:,(n2-n1+1):(n2-n1+1)+nneffidx);
        a = a + procp.svd_len;
    end;
    
    gprdat(k).data = d;
    gprdat = renamedata(gprdat,k);
    
    k = k+1;
    
    
end
%% svd gerade steigung
if background == 4
    
    d = gprdat(k-1).data;
    [m, n]=size(d);
    
    % Build filter
    bound = [1 177 286 499 616 819 936 1143 1258 n]; % Boundaries, where the upGPR moves up and down
    
    if n >= 1444
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp3 = d(:,bound(5):bound(6)); % gerade
        dtmp4 = d(:,bound(7):bound(8)); % gerade
        dtmp5 = d(:,bound(9):bound(10)); % gerade
        dtmp = [dtmp1 dtmp2 dtmp3 dtmp4 dtmp5];
    elseif n >= bound(8) && n < 1444
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp3 = d(:,bound(5):bound(6)); % gerade
        dtmp4 = d(:,bound(7):bound(8)); % gerade
        dtmp = [dtmp1 dtmp2 dtmp3 dtmp4];
    elseif n >= bound(6) && n < bound(8)
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp3 = d(:,bound(5):bound(6)); % gerade
        dtmp = [dtmp1 dtmp2 dtmp3];
    elseif n >= bound(4) && n < bound(6)
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp = [dtmp1 dtmp2];
    elseif n >= bound(2) && n < bound(4)
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp = [dtmp1];
    else
        dtmp = d;
    end
    
    % Actual Filter
    [~,s_filter,~] = svd(dtmp);
    s_filter(1,1) = 0;
    % s_filter(2,2) = 0;
    % s_filter(3,3) = 0;
    [m_f, n_f] = size(s_filter);
    done = 1;
    b = 1;
    while (done == 1)
        a = 1;
        done = 0;
        m1 = b;
        m2 = b + m_f -1;
        if (m2 >= m), m2 = m; m1 = m - m_f + 1; done = 0; end;
        mmeffidx = m2 - m1;
        while(done == 0)
            n1 = a;
            n2 = a+n_f-1;
            if (n2 >= n), n2 = n; n1 = n - n_f + 1; done = 1; end;
            nneffidx = n2 - n1;
            dtmp = d(m1:m2,n1:n2);
            [u,~,v] = svd(dtmp);
            dtmp = u*s_filter*v';
            d(m1:m2,n1:n2) = dtmp;
            a = a + n_f;
        end
        b = b + m_f;
        if (m2 >= m), m2 = m; done = 2; end;
    end
    
    % Apply filter to
    if n >= bound(9)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d3 = d(:,bound(6):bound(7)); % Steigung
        d4 = d(:,bound(8):bound(9)); % Steigung
        d = [d1 d2 d3 d4];
    elseif n >= bound(7) && n < bound(9)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d3 = d(:,bound(6):bound(7)); % Steigung
        d = [d1 d2 d3];
    elseif n >= bound(5) && n < bound(7)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d = [d1 d2];
    elseif n >= bound(3) && n < bound(5)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d = [d1];
    else
        d = d;
    end
    
    gprdat(k).data = d;
    gprdat = renamedata(gprdat,k);
    
    k = k+1;
end

%% svd steigung gerade
if background == 5
    d = gprdat(k-1).data;
    [m, n]=size(d);
    
    % Build filter
    bound = [1 177 286 499 616 819 936 1143 1258 n]; % Boundaries, where the upGPR moves up and down
    
    if n >= bound(9)
        dtmp1 = d(:,bound(2):bound(3)); % Steigung
        dtmp2 = d(:,bound(4):bound(5)); % Steigung
        dtmp3 = d(:,bound(6):bound(7)); % Steigung
        dtmp4 = d(:,bound(8):bound(9)); % Steigung
        dtmp = [dtmp1 dtmp2 dtmp3 dtmp4];
    elseif n >= bound(7) && n < bound(9)
        dtmp1 = d(:,bound(2):bound(3)); % Steigung
        dtmp2 = d(:,bound(4):bound(5)); % Steigung
        dtmp3 = d(:,bound(6):bound(7)); % Steigung
        dtmp = [dtmp1 dtmp2 dtmp3];
    elseif n >= bound(5) && n < bound(7)
        dtmp1 = d(:,bound(2):bound(3)); % Steigung
        dtmp2 = d(:,bound(4):bound(5)); % Steigung
        dtmp = [dtmp1 dtmp2];
    elseif n >= bound(3) && n < bound(5)
        dtmp1 = d(:,bound(2):bound(3)); % Steigung
        dtmp = [dtmp1];
    else
        dtmp = d;
    end
    
    dtmp = d;
    
    % Actual Filter
    [~,s_filter,~] = svd(dtmp);
    s_filter(1,1) = 0;
    % s_filter(2,2) = 0;
    % s_filter(3,3) = 0;
    [m_f, n_f] = size(s_filter);
    done = 1;
    b = 1;
    while (done == 1)
        a = 1;
        done = 0;
        m1 = b;
        m2 = b + m_f -1;
        if (m2 >= m), m2 = m; m1 = m - m_f + 1; done = 0; end;
        mmeffidx = m2 - m1;
        while(done == 0)
            n1 = a;
            n2 = a+n_f-1;
            if (n2 >= n), n2 = n; n1 = n - n_f + 1; done = 1; end;
            nneffidx = n2 - n1;
            dtmp = d(m1:m2,n1:n2);
            [u,~,v] = svd(dtmp);
            dtmp = u*s_filter*v';
            d(m1:m2,n1:n2) = dtmp;
            a = a + n_f;
        end
        b = b + m_f;
        if (m2 >= m), m2 = m; done = 2; end;
    end
    
    % Apply filter to
    
    if n >= 1444
        d1 = d(:,bound(1):bound(2)); % gerade
        d2 = d(:,bound(3):bound(4)); % gerade
        d3 = d(:,bound(5):bound(6)); % gerade
        d4 = d(:,bound(7):bound(8)); % gerade
        d5 = d(:,bound(9):bound(10)); % gerade
        d = [d1 d2 d3 d4 d5];
    elseif n >= bound(8) && n < 1444
        d1 = d(:,bound(1):bound(2)); % gerade
        d2 = d(:,bound(3):bound(4)); % gerade
        d3 = d(:,bound(5):bound(6)); % gerade
        d4 = d(:,bound(7):bound(8)); % gerade
        d = [d1 d2 d3 d4];
    elseif n >= bound(6) && n < bound(8)
        d1 = d(:,bound(1):bound(2)); % gerade
        d2 = d(:,bound(3):bound(4)); % gerade
        d3 = d(:,bound(5):bound(6)); % gerade
        d = [d1 d2 d3];
    elseif n >= bound(4) && n < bound(6)
        d1 = d(:,bound(1):bound(2)); % gerade
        d2 = d(:,bound(3):bound(4)); % gerade
        d = [d1 d2];
    elseif n >= bound(2) && n < bound(4)
        d1 = d(:,bound(1):bound(2)); % gerade
        d = [d1];
    else
        d = d;
    end
    
    gprdat(k).data = d;
    gprdat = renamedata(gprdat,k);
    
    
    
    k = k+1;
    
end


%% svd steigung alles
if background == 6
    d = gprdat(k-1).data;
    [m, n]=size(d);
    
    % Build filter
    bound = [1 177 286 499 616 819 936 1143 1258 n]; % Boundaries, where the upGPR moves up and down
    
    if n >= bound(9)
        dtmp1 = d(:,bound(2):bound(3)); % Steigung
        dtmp2 = d(:,bound(4):bound(5)); % Steigung
        dtmp3 = d(:,bound(6):bound(7)); % Steigung
        dtmp4 = d(:,bound(8):bound(9)); % Steigung
        dtmp = [dtmp1 dtmp2 dtmp3 dtmp4];
    elseif n >= bound(7) && n < bound(9)
        dtmp1 = d(:,bound(2):bound(3)); % Steigung
        dtmp2 = d(:,bound(4):bound(5)); % Steigung
        dtmp3 = d(:,bound(6):bound(7)); % Steigung
        dtmp = [dtmp1 dtmp2 dtmp3];
    elseif n >= bound(5) && n < bound(7)
        dtmp1 = d(:,bound(2):bound(3)); % Steigung
        dtmp2 = d(:,bound(4):bound(5)); % Steigung
        dtmp = [dtmp1 dtmp2];
    elseif n >= bound(3) && n < bound(5)
        dtmp1 = d(:,bound(2):bound(3)); % Steigung
        dtmp = [dtmp1];
    else
        dtmp = d;
    end
    
    dtmp = d;
    
    % Actual Filter
    [~,s_filter,~] = svd(dtmp);
    s_filter(1,1) = 0;
    % s_filter(2,2) = 0;
    % s_filter(3,3) = 0;
    [m_f, n_f] = size(s_filter);
    done = 1;
    b = 1;
    while (done == 1)
        a = 1;
        done = 0;
        m1 = b;
        m2 = b + m_f -1;
        if (m2 >= m), m2 = m; m1 = m - m_f + 1; done = 0; end;
        mmeffidx = m2 - m1;
        while(done == 0)
            n1 = a;
            n2 = a+n_f-1;
            if (n2 >= n), n2 = n; n1 = n - n_f + 1; done = 1; end;
            nneffidx = n2 - n1;
            dtmp = d(m1:m2,n1:n2);
            [u,~,v] = svd(dtmp);
            dtmp = u*s_filter*v';
            d(m1:m2,n1:n2) = dtmp;
            a = a + n_f;
        end
        b = b + m_f;
        if (m2 >= m), m2 = m; done = 2; end;
    end
    
    
    gprdat(k).data = d;
    gprdat = renamedata(gprdat,k);
    
    k=k+1;
end

%% svd gerade steigung
if background == 7
    
    d = gprdat(k-1).data;
    [m, n]=size(d);
    
    % Build filter
    bound = [1 177 286 499 616 819 936 1143 1258 n]; % Boundaries, where the upGPR moves up and down
    
    if n >= 1444
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp3 = d(:,bound(5):bound(6)); % gerade
        dtmp4 = d(:,bound(7):bound(8)); % gerade
        dtmp5 = d(:,bound(9):bound(10)); % gerade
        dtmp = [dtmp1 dtmp2 dtmp3 dtmp4 dtmp5];
    elseif n >= bound(8) && n < 1444
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp3 = d(:,bound(5):bound(6)); % gerade
        dtmp4 = d(:,bound(7):bound(8)); % gerade
        dtmp = [dtmp1 dtmp2 dtmp3 dtmp4];
    elseif n >= bound(6) && n < bound(8)
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp3 = d(:,bound(5):bound(6)); % gerade
        dtmp = [dtmp1 dtmp2 dtmp3];
    elseif n >= bound(4) && n < bound(6)
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp = [dtmp1 dtmp2];
    elseif n >= bound(2) && n < bound(4)
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp = [dtmp1];
    else
        dtmp = d;
    end
    
    % Actual Filter
    [~,s_filter,~] = svd(dtmp);
    s_filter(1,1) = 0;
    % s_filter(2,2) = 0;
    % s_filter(3,3) = 0;
    [m_f, n_f] = size(s_filter);
    done = 1;
    b = 1;
    while (done == 1)
        a = 1;
        done = 0;
        m1 = b;
        m2 = b + m_f -1;
        if (m2 >= m), m2 = m; m1 = m - m_f + 1; done = 0; end;
        mmeffidx = m2 - m1;
        while(done == 0)
            n1 = a;
            n2 = a+n_f-1;
            if (n2 >= n), n2 = n; n1 = n - n_f + 1; done = 1; end;
            nneffidx = n2 - n1;
            dtmp = d(m1:m2,n1:n2);
            [u,~,v] = svd(dtmp);
            dtmp = u*s_filter*v';
            d(m1:m2,n1:n2) = dtmp;
            a = a + n_f;
        end
        b = b + m_f;
        if (m2 >= m), m2 = m; done = 2; end;
    end
    
    gprdat(k).data = d;
    gprdat = renamedata(gprdat,k);
    
    k = k+1;
    
end

%median filter cut section
if background == 8
    %disp('Time picks have to be made on the whole line of traces (1444). If this is not the case, an error will show up because of tti(a).')
        d = gprdat(k-1).data;
    [m, n]=size(d);
        % Build filter
    bound = [1 177 286 499 616 819 936 1143 1258 n]; % Boundaries, where the upGPR moves up and down
    
    if n >= bound(9)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d3 = d(:,bound(6):bound(7)); % Steigung
        d4 = d(:,bound(8):bound(9)); % Steigung
        d = [d1 d2 d3 d4];
    elseif n >= bound(7) && n < bound(9)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d3 = d(:,bound(6):bound(7)); % Steigung
        d = [d1 d2 d3];
    elseif n >= bound(5) && n < bound(7)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d = [d1 d2];
    elseif n >= bound(3) && n < bound(5)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d = [d1];
    else
        d = d;
    end
%% filter implementation for all trace
    sumtr = median(d,2);
    
    %sumtr = median(gprdat(k-1).data,2); % horizontal filtering.   svd .. Fabian singular value decomp
    for b = 1:size(d,2)
        gprdat(k).data(:,b) = d(:,b) - sumtr;
    end;
    gprdat = renamedata(gprdat,k);
    
    
    k = k+1;
end

%median and svd beside each other for thesis
%median section
if background == 9
    %disp('Time picks have to be made on the whole line of traces (1444). If this is not the case, an error will show up because of tti(a).')
        d = gprdat(k-1).data;
    [m, n]=size(d);
        % Build filter
   
     gprdat(k).data =d;
     k =k+1;
    
    dfm = d;
    %%%SVD steigung steigung%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    n = length(d(1,:));
    %dv = zeros(size(d));
    a = 1;
    done = 0;
    while(done == 0)
        n2 = a;
        n3 = a+procp.svd_len-1;
        n1 = n2 - procp.svd_len;
        n4 = n3 + procp.svd_len;
        if (n1 < 1), n1 = 1; end;
        if (n3 >= n), n3 = n; done = 1; end;
        if (n4 > n), n4 = n; end;
        nneffidx = n3 - n2;
        dtmp = d(:,n1:n4);
        [u,s,v] = svd(dtmp);
        s(1,1) = 0;
          % s(2,2) = 0;
        dtmp = u*s*v';
        d(:,n2:n3) = dtmp(:,(n2-n1+1):(n2-n1+1)+nneffidx);
        a = a + 1; %procp.svd_len;
        display([num2str(a/n*100),'% complete'])
    end;
    
    gprdat(k).data = d;

    
    k = k+1;
    
    %%%%MEDIAN%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% filter implementation for all trace
    sumtr = median(dfm,2);
    
    %sumtr = median(gprdat(k-1).data,2); % horizontal filtering.   svd .. Fabian singular value decomp
    for b = 1:size(dfm,2)
        gprdat(k).data(:,b) = dfm(:,b) - sumtr;
    end;

    
    
    k = k+1;
end








end

