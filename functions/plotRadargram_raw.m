function [ax,fig] = plotRadargram_raw(data,date,procp,task,sra,Title)
%plotRadargram: plot a radargram before final treatment
%   data:       radagram data vector
%   date:       time vector with dates
%   task:       Either Time (s)(task=0) or use Depth (m) (task=1) as y-axis.
%   procp:      setting container
%   sra:        sampling rate
%   title:      figure title
%



% plotting options
%-----------------
load blkwhtred.mat
load zcm.mat
cmap = blkwhtred;
cmap2 = zcm;
cax = [-1.0 1.0];     % Lower and upper bounds of colorbar


sam = length(data(:,1));
t = (0:sam-1)*sra;
date = date'; %datestr(date);

if (task == 0) % use time as y-axis
    xr = date;
    yr = t;
    xlab = 'Date';
    ylab = 'Time [ns]';
    yd = 'normal';
end

if (task == 1) % use Depth as y-axis
    xr = date;
    yr = t*procp.vsnow/2;
    xlab = 'Date';
    ylab = 'Depth [m]';
    yd = 'normal';
end




fig = figure();
imagesc(xr,yr,data);
xlabel(xlab,'fontsize',12);
ylabel(ylab,'fontsize',12);
set(gca,'fontsize',12);
title(Title,'fontsize', 12,'interpreter','none');
colormap(cmap);    
caxis(cax); 
freezeColors;
set(gca,'YDir',yd);
%colorbar
datetick('x','dd.mm.yy')

ax=gca;
