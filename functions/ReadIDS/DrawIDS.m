function DrawIDS(fname)
%

headersize = 30780;
nsamp = 1024;

fid = fopen(fname,'rb');
fseek(fid,0,1);
nbytes = ftell(fid);
fseek(fid,0,-1);
ntraces = (nbytes - headersize)/(2*nsamp + 4);
data = zeros(nsamp,ntraces);
for a = 1:ntraces
    ss = fread(fid,4,'char');
    data(:,a) = fread(fid,nsamp,'int16');    
end;
fclose(fid);
clf;
imagesc(data);
title(sprintf('No of traces: %f',ntraces));
