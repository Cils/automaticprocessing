function data = ReadIDS(fname,varargin)
% Read IDS import IDS data files in matlab. GPR kjdata (e.g.: LID10001.dt)
% fname:        Path of file to read
% varargin:     nsamp:          Number of samples in file.  Default:1024
%               headersize:     Size of header. Default: 30780
%
% Modified by A.Cap, 2019


if nargin==1
    headersize = 30780;
    nsamp = 1024;
    
elseif nargin==2
    headersize = 30780;
    nsamp = varargin{1};
%     disp(varargin{1})
else
    headersize = varargin{2};
    nsamp = varargin{1};
%     disp(varargin{1})
%     disp(varargin{2})
end

fid = fopen(fname,'rb');
fseek(fid,0,1);
nbytes = ftell(fid);
fseek(fid,0,-1);
ntraces = round((nbytes - headersize)/(2*nsamp + 4));
data = zeros(nsamp,ntraces);
for a = 1:ntraces
    ss = fread(fid,4,'char');
    data(:,a) = fread(fid,nsamp,'int16');    
end
fclose(fid);
