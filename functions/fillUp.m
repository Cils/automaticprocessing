% Creates a matrix containing a radargram with constant stepsize
%
% from: start date (first date in radargram)
% to:   end date (last date in radargram)
% step: step in radargram
% interpolTime: interpolate, if the next measurement is temporally closer than interpolTime (in hours). Fill up with zeros if not.
% A:    matrix containing radargram
% x:    vector of size length(A) containing time for each trace in A
% x_new:vector containing time for each trace in B
% B:    radargram with constant stepsize
% ex.: [x_new,B] = fillUp('27.11.2010 18:00:00','22.05.2011 18:30:00',1/24/2,radargram_radargram22,radargram_dates_exact)
function [x_new,B] = fillUp(from, to, step, interpolTime, A, x)
x_new=datenum(from,'dd.mm.yyyy HH:MM:SS'):step:datenum(to,'dd.mm.yyyy HH:MM:SS');
B=ones(length(A(:,1)), length(x_new));

for trace = 1:size(B,2)
    %[C,I]=min(abs(x-x_new(trace)));
    [diff,I,value]=findNearest(x, x_new(trace));
    
    % {
    if abs(x(I)-x_new(trace)) <= interpolTime/24
        B(:,trace)=A(:,I);
    else
        B(:,trace)=zeros(size(A(:,I)));
    end
    %}
    
    %{
    %interpolieren
    if x(I) < x_new(trace)
        Imin=I;
        Imax=I+1;
    else
        Imin=I-1;
        Imax=I;
    end
    
    if Imin<1 || Imax>length(x)
        B(:,trace)=zeros(size(A(:,I)));
    elseif x(Imax)-x(Imin) > interpolTime/24
        B(:,trace)=zeros(size(A(:,I)));
    else
        xMin=x(Imin);
        xMax=x(Imax);
        traceMin=A(:,Imin);
        traceMax=A(:,Imax);
        B(:,trace)=traceMin+(traceMax-traceMin)/(xMax-xMin)*(x_new(trace)-xMin);
    end
    %}
end