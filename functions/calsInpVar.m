function [gprdat,yend,deltat,ylab,f,nf,viewlen,stepsize]=calsInpVar(gprdat,k,procp,meastime,vlen)
% Calculate some input variables necessary for data processing and plotting
% directly from the data.
% 
% Inputs:
% -------
% gprdat:       data container
% procp:        Contains all setting for GPR processing. e.g. procp.bound_SVD, procp.svd_len
% k:            index of gprdat
% 
% By A.Capelli 2019, moved repeated code from evaluation scripts of Dylan to independent funtion  


gprdat(k).nr_of_traces  = length(gprdat(k).data(1,:)); % Number of traces (around procp.nmax)
gprdat(k).nr_of_samples = length(gprdat(k).data(:,1)); % Number of samples (around 1024)

try
    gprdat(k).sampling_rate = meastime / procp.n_samples; % Sampling Interval [ns]
catch
    gprdat(k).sampling_rate = meastime / gprdat(k).nr_of_samples; % Sampling Interval [ns]
end

f = linspace(0,1/(2*gprdat(k).sampling_rate),gprdat(k).nr_of_samples/2)* 1000;
nf = length(f);
viewlen = vlen/gprdat(k).sampling_rate;
stepsize = gprdat(k).nr_of_samples/8;

if procp.tord == 1 % y-axis: depth
    yend = procp.vsnow*meastime/2;
    deltat = yend*stepsize/gprdat(k).nr_of_samples;
    ylab = 'Depth [m]';
else % y-axis: time
    yend = meastime;
    deltat = yend*stepsize/gprdat(k).nr_of_samples;
    ylab = 'Time [ns]';
end

%saqve values in gprdat
gprdat(k).yend =yend;
gprdat(k).deltat =deltat;
gprdat(k).ylab =ylab;
gprdat(k).f =f;
gprdat(k).nf =nf;
gprdat(k).viewlen =viewlen;
gprdat(k).stepsize =stepsize;

gprdat(k).fdata =1; % Initialize
gprdat(k).normfdata =1;