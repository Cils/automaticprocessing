% function that mainly call other function "manualPick_PickReflector_...
% Description must be inproved + eventually get rid of function in proessing flow! 
% Function no longer used in UpGPR processing flow 

function [dates,twt]=pickOberflaecheManuell_AC(Radar, colRamp, vMean, varargin)
Radar.radargram = Radar.radargramC1;
Radar.TWT = 1;
twt = manualPick_PickReflector_AC(Radar,colRamp,vMean,varargin);
dates = Radar.dates;
end
