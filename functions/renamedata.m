function [ gprdat ] = renamedata( gprdat,k )
%renamedata updates the values of the nb of samples, the nb of traces and
%the sampling rate

gprdat(k).nr_of_samples = length(gprdat(k).data(:,1));
gprdat(k).nr_of_traces  = length(gprdat(k).data(1,:));
gprdat(k).sampling_rate = gprdat(k-1).sampling_rate;
gprdat(k).fdata         = gprdat(k-1).fdata;
gprdat(k).normfdata     = gprdat(k-1).normfdata;

namelist=["yend","deltat","ylab","f","nf","viewlen","stepsize","date","tti","newPickTwt"];
for i=1:length(namelist)
    try
        gprdat(k).(namelist(i))=gprdat(k-1).(namelist(i));
    catch
        % do nothing
    end
end    
% gprdat(k).yend      =gprdat(k-1).yend;
% gprdat(k).deltat    =gprdat(k-1).deltat;
% gprdat(k).ylab      =gprdat(k-1).ylab;
% gprdat(k).f         =gprdat(k-1).f;
% gprdat(k).nf        =gprdat(k-1).nf;
% gprdat(k).viewlen   =gprdat(k-1).viewlen;
% gprdat(k).stepsize  =gprdat(k-1).stepsize;
% gprdat(k).date      =gprdat(k-1).date;
end

