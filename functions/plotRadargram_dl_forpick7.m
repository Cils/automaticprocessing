function [ax, h1, h2] = plotRadargram_dl_forpick7(propSpeed,from, to, step, interpolTime, radargram, traceTimes, twtTimes, language, colRamp, clims, varargin)
%plotRadargram: plot a radargram and the snowheights
%
%   from:       start time of Radargram (dd.mm.yyyy HH.MM.SS)
%   to:         end time of Radargram (dd.mm.yyyy HH.MM.SS)
%   step:       samplerate in days
%   interpolTime: interpolate, if the next measurement is temporally closer than interpolTime (in hours). Fill up with zeros if not.
%   radargram:  matrix containing radargram (m x n)
%   traceTimes: vector containing times of measurement for each trace in radargram (m x 1)
%   twtTimes:   vector containing twt times for each line in radargram (n x 1)
%   language:   'e' for english, 'd' for german labelling of the axes
%   colRamp:    A colormap is an m-by-3 matrix of real numbers between 0.0 and 1.0. Each row is an RGB vector that defines one color. The kth row of the colormap defines the kth color, where map(k,:) = [r(k) g(k) b(k)]) specifies the intensity of red, green, and blue.
%   clims:      normalizes the values in radargram to the range specified by clims and displays radargram as an image. clims is a two-element vector that limits the range of data values in radargram. These values map to the full range of values in the current colormap.
%   varargin:
%       X1,Y1,LS1,L1,... snowheights to plot in radargram. Plots all lines defined by Xn versus Yn pairs with LSn line specification that determines line type and legend title Ln
%
%   example:    plotRadargram('27.11.2010 18:00:00','22.05.2011 18:30:00',1/24/2,radargram_radargram22, radargram_dates_exact,Radar.samples, radargram_colRamp, [-750,750], Radar.dates_rounded,radar_HS_22,'Radar22',meteo_dates,meteo_HS_Laser,'Laser',meteo_dates, meteo_HS_Imis,'Imis')

if twtTimes(end)>10
    corrected=false;
else
    corrected=true;
end

if corrected, ymax = 3; else ymax = 30; end % max ns in twt
if corrected, propSpeed = 2; else propSpeed = propSpeed; end % max ns in twt
%propSpeed=eps2v(densLwc2eps_Denoth(323.297297,0))/1e9;%densLwc2v(323.297297,0); %Lino bitte entfernen, danke!!!!
fontSize = 15;%22/52*43.9;%22/52*43.4;%15
linewidth = 1;%4/52*43.9; %4/52*43.4;%3
markersize= 11/52*43.9;%11/52*43.4;%

[x_new,B] = fillUp(from, to, step, interpolTime, radargram, traceTimes);

% hfig = figure()%same size of plots Dylan
% set(hfig, 'Position', [500 200 800 600])
% unbundle x and y values for snow height plot, and their legend title
plots = cell(1,length(varargin)/4*3);
legends = cell(1,length(varargin)/4);
if isempty(varargin)
    plots{1,1}=0;
    plots{1,2}=0;
    plots{1,3}='b';
    plots{1,4}=0;
    plots{1,5}=0;
    plots{1,6}='b';
else
    for i = 0:length(varargin)/4-1
       plots{1,i*3+1} = varargin{i*4+1};
       plots{1,i*3+2} = varargin{i*4+2};
       plots{1,i*3+3} = varargin{i*4+3};
       legends{i+1} = varargin{i*4+4};
    end
end

% 2-D line plots with y-axes on both left and right side
%set(gcf,'DefaultAxesColorOrder',[0 0 0;0 1 0;1 0 0;1 0 1;0 1 1;1 1 1])
%set(gcf,'DefaultAxesColorOrder',[0 1 0;1 0 1;1 0 0;0 0 0;0 1 1;1 1 1])
[ax, h1, h2] = plotyy([],[],[],[], ...
  @(x,y) imagesc(x_new,twtTimes,B,clims), ...
  @(x,y) plot(plots{:},'LineWidth',linewidth,'Markersize',markersize));

% set proporties of radargram



mx = max(max(colRamp)); % dylan, because of different color schemes
set(ax(1),'YDir','normal') 
colormap(ax(1),colRamp/mx)
set(ax(1),'box','off')
set(ax(1),'YLim',[0,ymax]) 
set(ax(1),'YTickMode','auto')
if language=='d'
    if corrected
        set(get(ax(1),'YLabel'),'FontSize',fontSize,'String','Schneehöhe (m)')
    else
        set(get(ax(1),'YLabel'),'FontSize',fontSize,'String','Zweiweglaufzeit (ns)')
    end
else
    if corrected
        set(get(ax(1),'YLabel'),'FontSize',fontSize,'String','Snow height [m]')
    else
        set(get(ax(1),'YLabel'),'FontSize',fontSize,'String','Two-way travel time [ns]')
    end
end

% set proporties of snow height
%set(ax(2),'YLim',[0,propSpeed*ymax/2]) 
set(ax(2),'YTickMode','auto') 
if language=='d'
    set(get(ax(2),'YLabel'),'FontSize',fontSize,'String','Schneehöhe (m)')
else
    set(get(ax(2),'YLabel'),'FontSize',fontSize,'String','Snow height [m]')
end

lgnd=legend(ax(2),legends{:});
set(lgnd,'color','white');

%set proporties of both
% linkprop(ax,{'Xlim','XTickLabel','Xtick'}); % set in comments, Dylan 30.6
% set(ax(1),'XLim',[x_new(1),x_new(length(x_new))]); % set in comments, Dylan 30.6
dynamicDateTicks() % Dylan
if language=='d'
    set(get(ax(1),'XLabel'),'FontSize',fontSize,'String','Datum')
else
    set(get(ax(1),'XLabel'),'FontSize',fontSize,'String','Date')
end

set(ax,'fontsize',fontSize)
mypostcallback([],[],ax,propSpeed)

%set callbacks for zooming and pan
h = zoom(gcf);
set(h,'ActionPostCallback',{@mypostcallback,ax,propSpeed});
set(h,'Enable','on');
h = pan(gcf);
set(h,'ActionPostCallback',{@mypostcallback,ax,propSpeed});
set(h,'Enable','on');