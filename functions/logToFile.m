function errCode=logToFile(msg,code,varargin)
    %logToFile - Add message to logfile.
    %
    % Syntax:  [errCode] = function_name('Message',Code)
    %                      function_name('Message',Code,'path')
    %                      function_name('Message',Code,'newlog',1)    
    %                      function_name('Message',Code,'dispMsg',1)
    %
    % Inputs:
    %    msg        - Message to display
    %    code       - Message code: 
    %                1= Error causing process failure;  
    %                0= No error;  
    %                2= Error but process can be continued. 
    %                3= Add header for new evaluation run
    %                4= Message without date and time
    %    'path'     - Optioneal path of logfile otherwise the global variable "f_path" is used. 
    %    'dispMsg'  - Show "Message" in console
    %
    % Outputs:
    %    errCode - 0: no error, 1: error
    %
    % Example: 
    %    Line 1 of example
    %    Line 2 of example
    %    Line 3 of example
    %
    % Other m-files required: none
    % Subfunctions: none
    % MAT-files required: none
    %
    
    % Author: Achille Capelli, SLF
    % email: capelli@slf.ch 
    % November 2019; Last revision: 18.11.2019
    
    
    %------------- BEGIN CODE --------------
    %--------------------------------------------------------------------------------------------
    %-------------- Check inputs and set defaults if not defined --------------------------------
    %--------------------------------------------------------------------------------------------
    global f_path;
    i_p = inputParser;
    validpartOfSeason = @(x) (isvector(x) && isnumeric(x) && length(x)==2) || (isscalar(x) &&  x == 0);
    i_p.FunctionName = 'logToFile';
    i_p.addRequired('msg',@(x) ischar(x) || isstring(x));
    i_p.addRequired('code',@(x) isnumeric(x) && isscalar(x));
    i_p.addParameter('pathLog',fullfile(f_path, 'evalLog.txt' ),@ischar);
%     i_p.addParameter('newlog',0,@(x) isnumeric(x) && isscalar(x));
    i_p.addParameter('dispMsg',0,@(x) isnumeric(x) && isscalar(x)); 
    i_p.parse(msg,code,varargin{:});   
    
%    disp(i_p.Results.pathLog)
    
    % open file
    fid = fopen(i_p.Results.pathLog, 'a');
    if fid == -1
      disp('Cannot open log file.');
      errCode=1;
      return
    end
    
    

    
    % Disply message
    if i_p.Results.dispMsg
      disp(msg);
    end
    
    
    switch code
        case 0
            fprintf(fid, '%s:\t %s\n', datestr(now, 0), msg);
        case 1
            fprintf(fid, '%s:\t %s:\n\t %s\n', datestr(now, 0),'!!!!!!! Error, evaluation failed !!!!!!!!', msg);
        case 2
            fprintf(fid, '%s:\t %s:\n\t %s\n', datestr(now, 0),'Error, but evaluation could be continued.', msg);
        case 3
            % add header for new run
            fprintf(fid,'\n\n\n%s\n%s\n%s\n', ...
                        '----------------------------------------------------',...
                        ['--------------- ', datestr(now, 0),' ---------------'],...
                        '----------------------------------------------------');
            fprintf(fid,'%s\n',msg);
        case 4 
            fprintf(fid, '\t%s\n', msg);            
        otherwise
            fprintf(fid, '%s: %s:\n\t %s\n', datestr(now, 0),'Unknown entry type:', msg);
    end
    

    fclose(fid);