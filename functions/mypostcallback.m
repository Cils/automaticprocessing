function mypostcallback(obj,evd,ax,propSpeed)

yRadargram = get(ax(1),'YLim');
xRadargram = get(ax(1),'XLim');

set(ax(2),'YLim',yRadargram*propSpeed/2)

%linkprop(ax,{'Xlim','XTickLabel','Xtick'});
set(ax(2),'XLim',xRadargram);

set(ax(1),'XTickMode','auto') 
set(ax(2),'XTickMode','auto') 

ticks = get(ax(1),'XTick');
if length(ticks)>1 && ticks(2)-ticks(1)>=1
    %datetick(ax(1),'x',1,'keeplimits','keepticks');
    %datetick(ax(2),'x',1,'keeplimits','keepticks');
    datetick(ax(1),'x','dd mmm yyyy','keeplimits');
    datetick(ax(2),'x','dd mmm yyyy','keeplimits');
else
    datetick(ax(1),'x','dd mmm HH:MM','keeplimits');
    datetick(ax(2),'x','dd mmm HH:MM','keeplimits');
end