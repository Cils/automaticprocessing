% MatlabR2018b
% Created: Dyllan Longidge (Lino Schmid)
% Modified: Achille Capelli, 2019
function SurfaceTWT = manualPick_PickReflector_AC(Radar, colRamp, vMean,varargin)
%load_WFJ: import HS data from WFJ
%
%   Radar:      Structure containing preprocessed UpGPR data
%   colRamp:    A colormap is an m-by-3 matrix of real numbers between 0.0 and 1.0. Each row is an RGB vector that defines one color. The kth row of the colormap defines the kth color, where map(k,:) = [r(k) g(k) b(k)]) specifies the intensity of red, green, and blue.
%   vMean:      Mean electromagnetic speed in snow ( Used to get HS from travel time)
%   Varargin:   Supplementary HS datasets to plot. e.g: surface.dates, surface.HS, surface.label, surface.LS (linestyle) 
%   
% 
% example: "Surface.twt =%   manualPick_PickReflector_AC(Radar,colRamp,waveSpeed,WFJ_surface,Surface_old)"




from = datestr(Radar.dates(1),'dd.mm.yyyy HH:MM:SS');
to = datestr(Radar.dates(end),'dd.mm.yyyy HH:MM:SS');


% settings for radargram interpolation
step=1/8;
interpolTime=3;


% Adding Nan if not long enough
if length(Radar.dates)~=length(Radar.TWT)
    Radar.TWT=[Radar.TWT ones(length(Radar.dates)-length(Radar.TWT),1)'*NaN].*1e9;
end
SurfaceTWT = Radar.TWT;

% reset plot parameters to first klick
lastKlick = [0 0];

% prepare other lines to plot
surfacelist={};
j=1;
length(varargin)
for i=1:length(varargin)
   i
   a=varargin{i}
   if not(isempty(a))
       surfacelist(j)={a.dates};
       surfacelist(j+1)={a.HS};
       surfacelist(j+2)={a.LS};
       surfacelist(j+3)={a.label};
       j=j+4
   end
end

% as long as user wants to set another pick
fprintf('For help press h\n')   
fig1 = figure();
notFirst=0;
climpIntens = [-1 1]; % dylan
while(1)
%     if notFirst
%        
%        xl=xlim;
%        yl=ylim;
%     else
%        xl=[datenum(from,'dd.mm.yyyy HH:MM:SS') datenum(to,'dd.mm.yyyy HH:MM:SS')];
%        yl=[0,Radar.samples(end)];
%     end
%     notFirst=1; 
    
    lx1 = subplot(4,1,1:3);
    set(fig1,'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    
    [ax, h1, h2] = plotRadargram_dl_forpick7(vMean,from, to, step, interpolTime,...
        Radar.radargram, Radar.dates, Radar.samples, 'e', colRamp, climpIntens,...
        Radar.dates, SurfaceTWT * vMean / 2,'.g','Semiautomatic Phasefollower Pick',...
        surfacelist{:},...
        lastKlick(1),lastKlick(2),'ro','lastClick'); 
        
        %Radar.dates, GPRv1.TWT * vMean / 2,'b','HS GPR Dylan',...
        %GPRva.date,GPRva.HS/10,'m','HS GPR Achim',...
        
    title('Radargram Surface Picking','fontsize',12)
    
    gcf1 = gcf();
    gca1 = gca();
    
    lx2 = subplot(4,1,4);
    axisOben=axis(ax(1));
   
    %createfigtogether2(WAN6, WFJ, lx2)
    %plot(TA.dates,TA.values,'b',TSS.dates,TSS.values,'c', axisOben(1:2) ,[0 0],'k')
    
    xlim([datenum(from,'dd.mm.yyyy HH:MM:SS') datenum(to,'dd.mm.yyyy HH:MM:SS')])
    gca12 = gca();
    xlim(gca12,axisOben(1:2)); %%%%%%%%%%%%%
%     linkprop([lx1,lx2],{'XTickLabel','XTick', 'XLim'});

    
    % set callback s.t. radargram right shows always data of position of mouse in the left radargram
    global mouseMoveAlreadyExecuting;
    mouseMoveAlreadyExecuting=false;
    set (gcf1, 'WindowButtonMotionFcn', {@manualPick_MouseMove_DL_reduced gca1});% gca1
  
    
    % wait until key pressed (ignore mouse klicks)
    w=0;
    while w==0
        w = waitforbuttonpress; % Returns 0 when terminated by a mouse buttonpress, or 1 when terminate by a keypress.
    end
    % switch for pressed key
    key=get(fig1, 'CurrentKey'); % dylan hfig2 
    switch lower(key)
        case 's'
            C = get (gca1, 'CurrentPoint');
            % get position of mouse
            if iscell(C)
                x = C{1,1}(1,1);
                y = C{1,1}(1,2);
            else
                x = C(1,1);
                y = C(1,2);
            end
            
            % follow phase after klick
            %newPickTwt = manualPick_FollowPhase_DL(Radar.radargram, Radar.dates, Radar.samples, x, y); % on highest/lowest amplitude

            [newPickTwt, newPickTwt_zero] = manualPick_FollowPhase3(Radar.radargram, Radar.dates, Radar.samples, x, y); % on zerocrossing
            % merge old and new pick
            SurfaceTWT(end-length(newPickTwt_zero)+1:end) = newPickTwt_zero;
            % store positon of last klick
            lastKlick = [x,y*vMean/2];
        case 'c'
            C = get (gca1, 'CurrentPoint');
            % get position of mouse
            if iscell(C)
                x = C{1,1}(1,1);
                y = C{1,1}(1,2);
            else
                x = C(1,1);
                y = C(1,2);
            end
            [~,Ix,~]=findNearest(Radar.dates, x);
            
            % clear WettingFront after klick
            SurfaceTWT(Ix:end) = NaN;
            % store positon of last klick
            lastKlick = [x,y*vMean/2];
        case 'f'
            C = get (gca1, 'CurrentPoint');
            % get position of mouse
            if iscell(C)
                x = C{1,1}(1,1);
                y = C{1,1}(1,2);
            else
                x = C(1,1);
                y = C(1,2);
            end
            [~,Ix,~]=findNearest(Radar.dates, x);
            
            % force pick to be here
            SurfaceTWT(Ix) =y;
            % store positon of last klick
            lastKlick = [x,y*vMean/2];
        case 'n'
            C = get (gca1, 'CurrentPoint');
            % get position of mouse
            if iscell(C)
                x = C{1,1}(1,1);
                y = C{1,1}(1,2);
            else
                x = C(1,1);
                y = C(1,2);
            end
            [~,Ix,~]=findNearest(Radar.dates, x);
            
            % clear WettingFront after klick
            SurfaceTWT(Ix:end) = 0;
            % store positon of last klick
            lastKlick = [x,0];
        case 'r'
            clear ax axis1_vor axis2_vor;
        case 'z'
            notFirst=0;
        case 'add'
            climpIntens=climpIntens*.7;
        case 'subtract'
            climpIntens=climpIntens*1.3;
        case 'escape'
            break;
        case 'f5'
            error('Interrupted by user');
        case 'h'
            fprintf('===== Help for PickReflector =======\ns: set pick\nc: clear pic\nf: force pick\nn: force pick to be 0\nr: reset plot\nz: zoom out to original\n+: more contrast\n-: less contrast\nESC: escape\n')
        otherwise
            disp('Press "h" for help')
    end
end