function [WFJ_surface]=load_WFJ(WFJ_path,sourcetype)
%load_WFJ: import HS data from WFJ
%
%   WFJ_path:       path of file
%   sourcetype:     type of file to get HS.  sourcetype='MST_flux' or TP. 


if  strcmp(sourcetype,'MST_flux')
    A = importdata(WFJ_path,',',3);
    WFJ_surface.HS=A.data(:,7)'/100;
    WFJ_surface.dates=datenum(A.textdata(4:end), 'dd.mm.yyyy  HH:MM')';

elseif strcmp(sourcetype,'TP')
    A = importdata(WFJ_path,',',3);
    WFJ_surface.HS=A.data(:,26)'/100;
    fid = fopen(WFJ_path);
    for i=1:3
        tline = fgetl(fid);
    end
    dat=strings(1,length(A.data));
    i=1;
    for j=1:length(A.data)
        tline = fgetl(fid);
        b=strfind(tline,',');
        dat(i) =tline(1:b-1);

    %     disp(tline)
        i=i+1;
    end
    fclose(fid);
    WFJ_surface.dates=datenum(dat, 'dd.mm.yyyy  HH:MM')';
    
    
elseif strcmp(sourcetype,'V_tp')
    A = importdata(WFJ_path,' ',35);
    WFJ_surface.HS=A.data(:,25)';
    WFJ_surface.dates=datenum(datenum(A.textdata(36:end), 'yyyy-mm-ddTHH:MM:SS'));
end


