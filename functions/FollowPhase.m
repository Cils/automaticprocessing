
function [newPickTwt,newPickTwt_i] = FollowPhase(radargram, times, startTwt)
    % Follows zero-crossing of radargramm 
    %
    % radargram:    radargram
    % times:        vector with positions in y-directions (e.g. travel times or distances)
    % startTwt:     twt of from where you want to follow the phase. Same units as "times"
    %
    
    % find zerocrossings  
    phaseChanges = sign(radargram(2:end,:)) - sign(radargram(1:end-1,:));
            
        
    % find indices in radargram of klick
%     [~,Ix,~]=findNearest(dates, startDate);
    Ix=1;
    [~,Iy,~]=findNearest(times, startTwt);
    
    iy=Iy;
    ixNewpick=1;
    
    signum = 0;
    newPickTwt = zeros(size(radargram,2)-Ix+1,1);
    newPickTwt_zero = zeros(size(radargram,2)-Ix+1,1);
    
    for ix = Ix:size(radargram,2) % Dylan
        
        % if no phase change can be found
        if phaseChanges(:,ix) == zeros(length(phaseChanges(:,1)))
            phaseChanges(:,ix) = phaseChanges(:,ix-1);
        end

                
        if signum ==0  % for first point find nearest zero-crossing
            
            % find indices zerocrossings      
            zeroCrossings = find(phaseChanges(:,ix));
            
            % find nearest zerocrossings
            [~,~,iy]=findNearest(zeroCrossings, iy);
            
            signum=phaseChanges(iy,ix);
        
        else
            zeroCrossings2= find(phaseChanges(:,ix)==signum);
            [~,~,iy]=findNearest(zeroCrossings2, iy);
        end
        
        newPickTwt(ixNewpick)=times(iy);
        newPickTwt_i(ixNewpick) = iy;
        
        ixNewpick=ixNewpick+1;
    end
end

