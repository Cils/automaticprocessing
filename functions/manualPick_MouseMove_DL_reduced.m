function manualPick_MouseMove_DL_reduced(plotMatrix, dates, samples, pick, twt, gca1, ax1)  
   
% if already executing mouseMove, just return
    global mouseMoveAlreadyExecuting;
    if mouseMoveAlreadyExecuting
        return
    else
        mouseMoveAlreadyExecuting=true;
    end

    C = get (gca1, 'CurrentPoint');
    if iscell(C)
        x = C{1,1}(1,1);
        y = C{1,1}(1,2);
    else
        x = C(1,1);
        y = C(1,2);
    end
%     title(gca1, [datestr(x,'dd.mmm HH:MM'), ': TWT = ',num2str(y)]);
% 
%     [~,Ix,~]=findNearest(dates, x);
%     [~,Iy,~]=findNearest(samples, y);
% 
% %     figure(fig2)
% %     hold off
%     plot(gca1, plotMatrix(:,Ix), samples);
%     hold on
%     %plot(gca1, [0,0], samples(1,end),'k');
%     plot(gca1, xlim, [1,1] * samples(Iy),'r');
%     plot(gca1, xlim, [1,1] * twt(Ix),'g');
%     plot(gca1, xlim, [1,1] * pick(Ix),'k');
%     ylim(gca1,[0,30]);
%     title(gca1, [datestr(x,'dd.mmm HH:MM'), ': TWT = ',num2str(y)]);
%     mouseMoveAlreadyExecuting=false;
end