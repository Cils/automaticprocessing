function [ Radar_new ] = datacutter( Radar,samstart,samend )
%datacutter cuts the data's trace length to the required length

Radar_new.dates = Radar.dates;
Radar_new.radargramC1 = Radar.radargramC1(samstart:samend,:);
Radar_new.samples = Radar.samples(1:samend-samstart,:);

end

