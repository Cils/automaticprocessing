function [ data ] = bckgamma( data, svd_len,sra,decon_len,fl,fh, varargin )
%BCKGAMMA: Background removal for radargram.
% Syntax:  [ errorcode ] = seasonEval( evalfolder )
%          [ errorcode ] = seasonEval( evalfolder,'justnew',justnew,'partOfSeason' )
%
% Inputs:
%   data, svd_len,sra,decon_len,fl,fh       
%
%
% Optional inputs:
%   'bckgamma','median','SVD','deconv','butther':  -- 1 if the filter is to be applied otherwise 0. Default: SVD=1, the rest =0
%
% 
% Outputs:
%    data      -   data with background removed 
%

%--------------------------------------------------------------------------------------------
%-------------- Check inputs and set defaults if not defined --------------------------------
%--------------------------------------------------------------------------------------------
i_p = inputParser;
i_p.FunctionName = 'bckgamma';  
i_p.addParameter('median',0,@(x) isnumeric(x) && isscalar(x));
i_p.addParameter('SVD',1,@(x) isnumeric(x) && isscalar(x));
i_p.addParameter('deconv',0,@(x) isnumeric(x) && isscalar(x)); 
i_p.addParameter('butther',0,@(x) isnumeric(x) && isscalar(x));  
i_p.parse(varargin{:}); 



h = waitbar(0,'Background removal (SVD filter)') ;

% ---------------------------------------------------------------
% ------------------- Median ------------------------------------
% ---------------------------------------------------------------
if i_p.Results.median==1
    sumtr = median(data,2);
    d=zeros(size(data),'like',data);

    for b = 1:size(data,2)
        d(:,b) = data(:,b) - sumtr;
    end
    
    data=d;
end

% -------------------------------------------------------------------------
% ------------------- SVD Filtering    ------------------------------------
% -------------------------------------------------------------------------
if i_p.Results.SVD==1
    d = data;
    n = length(d(1,:));

    a = 1;
    done = 0;
    while(done == 0)
        n2 = a;
        n3 = a+svd_len-1;
        n1 = n2 - svd_len;
        n4 = n3 + svd_len;
        if (n1 < 1), n1 = 1; end;
        if (n3 >= n), n3 = n; done = 1; end;
        if (n4 > n), n4 = n; end;
        nneffidx = n3 - n2;
        
        % SVD filter
        [u,s,v] = svd(data(:,n1:n4));
        s(1,1) = 0;
          % s(2,2) = 0;
        dtmp = u*s*v';

        d(:,n2:n3) = dtmp(:,(n2-n1+1):(n2-n1+1)+nneffidx);
        a = a + svd_len;
    %     display([num2str(a/n*100),'% complete'])
        waitbar(a/n)
    end

    data= d;
end



   
% -------------------------------------------------------------------------
% ------------------- Deconvolution   ------------------------------------
% -------------------------------------------------------------------------

% To be implemented.

% -------------------------------------------------------------------------
% ------------------- Buttherworth     ------------------------------------
% -------------------------------------------------------------------------

% To be implemented.


close(h) %close progress bar
end

