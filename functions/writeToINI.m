function [errcode,msg] = writeToINI(inifile,key, value,section,varargin)
% writeToINI: Function for adding entry to .ini file. 
% It uses the class iniconfig by Iroln. See: iniconfig for more
% 
% Syntax:  writeToINI(inifile,key, value, section,v_format )
%
% Inputs:
%   inifile     -   Path of .ini file 
%	key         -	Key name.  
%   section     -   Section name. Default: no section
% Optional inputs:
%   v_format      -   Format of value to be saved. E.g: '%.2f'
%
% Outputs:
%   errorcode   -   Errorcode.    
%                   0: successful evaluation.
%                   1: .ini file was not found. 
%                   2: Entry not found! Create new one
%                   3: Fail to save entry in ini file!
%   msg         -   Message to be returned. If no error msg=''
%
% Example: 
%    writeToINI(inifile,key, value, section )
%    writeToINI(inifile,key, value, section,v_format )
%
% Other m-files required: IniConfig.m
% Subfunctions: 
% MAT-files required: none
%
% See also: iniconfig
%
% Author: Achille Capelli, SLF
% email: capelli@slf.ch 
% November 2019; Last revision: 
   


    %--------------------------------------------------------------------------------------------
    %-------------- Check inputs and set defaults if not defined --------------------------------
    %--------------------------------------------------------------------------------------------
    i_p = inputParser;
    validpartOfSeason = @(x) (isvector(x) && isnumeric(x) && length(x)==2) || (isscalar(x) &&  x == 0);
    i_p.FunctionName = 'writeToINI';
    i_p.addRequired('inifile',@ischar);
	i_p.addRequired('key',@ischar);
    i_p.addRequired('value',@(x) ischar(x) || isnumeric(x)|| isstring(x) );
    i_p.addRequired('section',@ischar);
    i_p.addOptional('v_format','',@ischar);
    
    i_p.parse(inifile,key,value,section,varargin{:}); 
    v_format=i_p.Results.v_format;

    msg=' ';
    errcode=0;
    
    % open file
    ini = IniConfig();
    er = ini.ReadFile(inifile);
    if ~er
       msg=['The .ini could not be open! Path: ' inifile];
       disp(msg)
       errcode=1;
       return 
    end
    
    % add [] to section name if necessary
    if ~(section(1)=='[')
        section=['[' section ']'];
    end
        
    % add new section if necessary section
    section_list=ini.GetSections();
    if ~ismember(section,section_list)
        ini.AddSections(section)
        msg=['The .ini could not be open! Path: ' inifile];
        disp(msg)
        errcode=er;
    end
    
    % add new section if necessary
    section_list=ini.GetSections();
    if ~ismember(section,section_list)
        ini.AddSections(section)
        msg=['A new section "' section '" was added to the .ini file'];
        disp(msg)
        errcode=2;
    end
    
    % add value to key
    key_list=ini.GetKeys(section);
    if ~ismember(key,key_list)
        er=ini.AddKeys(section,key,value,v_format );
        msg=['A new key "' key '" was added to the .ini file. Section: ' section ];
        disp(msg)
        errcode=2;
    else
        er=ini.SetValues(section,key,value,v_format);
    end
    
    if ~er
        msg=['Failed to add value to key: ' key ' Value:' string(value) ];
        disp(msg)
        errcode=3;
    end

    %write to file
    ini.WriteFile(inifile);
    
end