% backgroundrem
% Remove background noise from upGPR data
% Original fuction by Dylan Longridge 2018
% Modified by A.Capelli 2019 


function [ gprdat, k ] = backgroundrem( gprdat, k, background, procp )
% BACKGROUND REMOVAL either median filtering or SVD.
% gprdat: data container
% k:      index for gprdat
% background = 1 -> median filtering and so on..
%           median = 1, svd gerade gerade = 2, svd steigung steigung = 3, svd gerade steigung = 4, svd steigung gerade = 5, svd steigung alles = 6, svd gerade alles = 7
%           Use backgoud=10 for automatic processing. Similar to backgoud=3
%           but flexible length of ramps. With procp.bound_SVD the position
%           of the bounds are passed. procp.svd_WinNumb: used to determine
%           length of SVD windows based on length of ramp.
% procp:    Contains all setting for GPR processing. e.g. procp.bound_SVD, procp.svd_len

% median
if background == 1
    %disp('Time picks have to be made on the whole line of traces (1444). If this is not the case, an error will show up because of tti(a).')
    
    d = gprdat(k-1).data;
    [m, n]=size(d);
    
    % Build filter
    if isfield(procp,'bound_SVD')
        bound = procp.bound_SVD;
        bound(10)=n;
    else    
        bound = [1 177 286 499 616 819 936 1143 1258 n]; % Boundaries, where the upGPR moves up and down
    end
    
    for i=1:length(bound)
        if bound(i)>n
            bound=bound(1:i);
            bound(i)=n;
            disp('Array shorter than bound. New bound: ')
            disp(bound)
            break
        end
    end
    
    
    j=0;
    s= ones(floor((length(bound)-1)/2),length(gprdat(k-1).data(:,1)),'double');
    for i=2:2:length(bound)-1
        j=j+1;
        s(j,:)=median(gprdat(k-1).data(:,bound(i):bound(i+1)),2);
    end
    sumtr = median(s,1)';

    
    
    % old code
%     if gprdat(k-1).nr_of_traces == 1444
%         %%% filter calculations
%         sumtr1 = median(gprdat(k-1).data(:,178:285),2);
%         sumtr2 = median(gprdat(k-1).data(:,500:615),2);
%         sumtr3 = median(gprdat(k-1).data(:,820:935),2);
%         sumtr4 = median(gprdat(k-1).data(:,1144:1257),2);
%         sumtr = median([sumtr1,sumtr2,sumtr3,sumtr4],2);
%     elseif gprdat(k-1).nr_of_traces > 615
%         sumtr1 = median(gprdat(k-1).data(:,178:285),2);
%         sumtr2 = median(gprdat(k-1).data(:,500:615),2);
%         sumtr = median([sumtr1,sumtr2],2);
%     elseif gprdat(k-1).nr_of_traces > 285
%         sumtr1 = median(gprdat(k-1).data(:,178:285),2);
%     else
%         sumtr = median(gprdat(k-1).data,2);
%     end
    %% filter implementation for all trace
    
    %sumtr = median(gprdat(k-1).data,2); % horizontal filtering.   svd .. Fabian singular value decomp
    for b = 1:gprdat(k-1).nr_of_traces
        gprdat(k).data(:,b) = gprdat(k-1).data(:,b) - sumtr;
    end
    gprdat = renamedata(gprdat,k);
    
    
    k = k+1;
end

%% svd gerade gerade
if background == 2
    d = gprdat(k-1).data;
    [m, n]=size(d);
    
    % Build filter
    if isfield(procp,'bound_SVD')
        bound = procp.bound_SVD;
    else    
        bound = [1 177 286 499 616 819 936 1143 1258 n]; % Boundaries, where the upGPR moves up and down
    end
    bound(10)=n;
    
    
    if n >= 1444
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp3 = d(:,bound(5):bound(6)); % gerade
        dtmp4 = d(:,bound(7):bound(8)); % gerade
        dtmp5 = d(:,bound(9):bound(10)); % gerade
        dtmp = [dtmp1 dtmp2 dtmp3 dtmp4 dtmp5];
    elseif n >= bound(8) && n < 1444
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp3 = d(:,bound(5):bound(6)); % gerade
        dtmp4 = d(:,bound(7):bound(8)); % gerade
        dtmp = [dtmp1 dtmp2 dtmp3 dtmp4];
    elseif n >= bound(6) && n < bound(8)
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp3 = d(:,bound(5):bound(6)); % gerade
        dtmp = [dtmp1 dtmp2 dtmp3];
    elseif n >= bound(4) && n < bound(6)
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp = [dtmp1 dtmp2];
    elseif n >= bound(2) && n < bound(4)
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp = [dtmp1];
    else
        dtmp = d;
    end
    
    % Actual Filter
    [~,s_filter,~] = svd(dtmp);
    s_filter(1,1) = 0;
    % s_filter(2,2) = 0;
    % s_filter(3,3) = 0;
    [m_f, n_f] = size(s_filter);
    done = 1;
    b = 1;
    while (done == 1)
        a = 1;
        done = 0;
        m1 = b;
        m2 = b + m_f -1;
        if (m2 >= m), m2 = m; m1 = m - m_f + 1; done = 0; end;
        mmeffidx = m2 - m1;
        while(done == 0)
            n1 = a;
            n2 = a+n_f-1;
            if (n2 >= n), n2 = n; n1 = n - n_f + 1; done = 1; end;
            nneffidx = n2 - n1;
            dtmp = d(m1:m2,n1:n2);
            [u,~,v] = svd(dtmp);
            dtmp = u*s_filter*v';
            d(m1:m2,n1:n2) = dtmp;
            a = a + n_f;
        end
        b = b + m_f;
        if (m2 >= m), m2 = m; done = 2; end;
    end
    
    % Apply filter to
    
    if n >= 1444
        d1 = d(:,bound(1):bound(2)); % gerade
        d2 = d(:,bound(3):bound(4)); % gerade
        d3 = d(:,bound(5):bound(6)); % gerade
        d4 = d(:,bound(7):bound(8)); % gerade
        d5 = d(:,bound(9):bound(10)); % gerade
        d = [d1 d2 d3 d4 d5];
    elseif n >= bound(8) && n < 1444
        d1 = d(:,bound(1):bound(2)); % gerade
        d2 = d(:,bound(3):bound(4)); % gerade
        d3 = d(:,bound(5):bound(6)); % gerade
        d4 = d(:,bound(7):bound(8)); % gerade
        d = [d1 d2 d3 d4];
    elseif n >= bound(6) && n < bound(8)
        d1 = d(:,bound(1):bound(2)); % gerade
        d2 = d(:,bound(3):bound(4)); % gerade
        d3 = d(:,bound(5):bound(6)); % gerade
        d = [d1 d2 d3];
    elseif n >= bound(4) && n < bound(6)
        d1 = d(:,bound(1):bound(2)); % gerade
        d2 = d(:,bound(3):bound(4)); % gerade
        d = [d1 d2];
    elseif n >= bound(2) && n < bound(4)
        d1 = d(:,bound(1):bound(2)); % gerade
        d = [d1];
    else
        d = d;
    end
    
    
    gprdat(k).data = d;
    gprdat = renamedata(gprdat,k);
    
    k = k+1;
end


%% svd steigung steigung
if background == 3
    d = gprdat(k-1).data;
    [m, n]=size(d);
    
    % Build filter
    if isfield(procp,'bound_SVD')
        bound = procp.bound_SVD;
    else    
        bound = [1 177 286 499 616 819 936 1143 1258 n]; % Boundaries, where the upGPR moves up and down
    end
    bound(10)=n;
    
    
    if n >= bound(9)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d3 = d(:,bound(6):bound(7)); % Steigung
        d4 = d(:,bound(8):bound(9)); % Steigung
        d = [d1 d2 d3 d4];
    elseif n >= bound(7) && n < bound(9)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d3 = d(:,bound(6):bound(7)); % Steigung
        d = [d1 d2 d3];
    elseif n >= bound(5) && n < bound(7)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d = [d1 d2];
    elseif n >= bound(3) && n < bound(5)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d = [d1];
    else
        d = d;
    end
    
    n = length(d(1,:));
    %dv = zeros(size(d));
    a = 1;
    done = 0;
    while(done == 0)
        n2 = a;
        n3 = a+procp.svd_len-1;
        n1 = n2 - procp.svd_len;
        n4 = n3 + procp.svd_len;
        if (n1 < 1), n1 = 1; end;
        if (n3 >= n), n3 = n; done = 1; end;
        if (n4 > n), n4 = n; end;
        nneffidx = n3 - n2;
        dtmp = d(:,n1:n4);
        [u,s,v] = svd(dtmp);
        s(1,1) = 0;
        %   s(2,2) = 0;
        dtmp = u*s*v';
        d(:,n2:n3) = dtmp(:,(n2-n1+1):(n2-n1+1)+nneffidx);
        a = a + procp.svd_len;
    end;
    
    gprdat(k).data = d;
    gprdat = renamedata(gprdat,k);
    
    k = k+1;
    
    
end


%% svd steigung steigung; version for automaticProcessing (A. Cap.)
if background == 10
    d = gprdat(k-1).data;
    [m, n]=size(d);
    
    % Build filter
    if isfield(procp,'bound_SVD')
        bound = procp.bound_SVD;
    else    
        bound = [1 177 286 499 616 819 936 1143 1258 n]; % Boundaries, where the upGPR moves up and down
    end
    bound(10)=n;
    
    % get data of ramps (steigungen)
    nn=[0,0,0,0];    
    if n >= bound(9)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d3 = d(:,bound(6):bound(7)); % Steigung
        d4 = d(:,bound(8):bound(9)); % Steigung
        d =[d1 d2 d3 d4];
        nn(1)=bound(3)-bound(2);
        nn(2)=bound(5)-bound(4);
        nn(3)=bound(7)-bound(6);
        nn(4)=bound(9)-bound(8);
        nd = 4;
    elseif n >= bound(7) && n < bound(9)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d3 = d(:,bound(6):bound(7)); % Steigung
        d =[d1 d2 d3];
        nn(1)=bound(3)-bound(2);
        nn(2)=bound(5)-bound(4);
        nn(3)=bound(7)-bound(6);
        nd=3;
    elseif n >= bound(5) && n < bound(7)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d =[d1 d2];
        nn(1)=bound(3)-bound(2);
        nn(2)=bound(5)-bound(4);
        nd=2;
    elseif n >= bound(3) && n < bound(5)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d =d1;
        nn(1)=bound(3)-bound(2);
        nd=1;
    else
                nn(1)=n;
        nd=1;
    end
    
    dd=d;  %copy of data
    nn=nn+1;
%     sum(nn)
%     size(d)

    for i= 1:nd
        
        l = nn(i); % length of ramp
        
        if (i>1) 
            a1=sum(nn(1:i-1)); 
        else 
            a1=1;
        end
        
        a2=a1+l;
        
        w=round(l/procp.SVD_WinNumb); % windows size
        dw=procp.svd_dWin; % windows spacing 
        if (mod((w-dw),2)==1), w=w+1; end
        c=(w-dw)/2;
       
        done = 0;
        a = a1;
        
        while(done == 0)
            n2 = a;
            n3 = a+dw-1;
            n1 = n2 - c;
            n4 = n3 + c;
            if (n1 < a1), n1 = a1; end
            if (n3 >= a2), n3 = a2; done = 1; end
            if (n4 > a2), n4 = a2; end
%             [n1 n2 n3 n4]
            try
                [u,s,v] = svd(dd(:,n1:n4));
            catch e      % show infos before throwing error!
                disp([' n1:', num2str(n1),', n2:', num2str(n2),', c:', num2str(c),', w:',num2str(w),', dw:', num2str(dw),' l:', num2str(l), 'n:' num2str(n) ]);
                disp('bound:');
                disp(bound);
                disp('nn:');
                disp(nn);
                disp('n1:');
                disp(n1);
                disp('n4:');
                disp(n4);
                disp('length dd:');
                disp(size(dd));
                rethrow(e)
            end
            s(1,1) = 0;
            %   s(2,2) = 0;
            dtmp = u*s*v';
            d(:,n2:n3) = dtmp(:,(n2-n1+1):(n3-n1+1));
            
            a = a + dw; % move window
        end
%     [n1 n2 n3 n4]
    end
%     size(d)
    gprdat(k).data = d;
    gprdat = renamedata(gprdat,k);
    
    k = k+1;
    
    
end



%% svd gerade steigung
if background == 4
    
    d = gprdat(k-1).data;
    [m, n]=size(d);
    
    % Build filter
    bound = [1 177 286 499 616 819 936 1143 1258 n]; % Boundaries, where the upGPR moves up and down
    
    if n >= 1444
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp3 = d(:,bound(5):bound(6)); % gerade
        dtmp4 = d(:,bound(7):bound(8)); % gerade
        dtmp5 = d(:,bound(9):bound(10)); % gerade
        dtmp = [dtmp1 dtmp2 dtmp3 dtmp4 dtmp5];
    elseif n >= bound(8) && n < 1444
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp3 = d(:,bound(5):bound(6)); % gerade
        dtmp4 = d(:,bound(7):bound(8)); % gerade
        dtmp = [dtmp1 dtmp2 dtmp3 dtmp4];
    elseif n >= bound(6) && n < bound(8)
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp3 = d(:,bound(5):bound(6)); % gerade
        dtmp = [dtmp1 dtmp2 dtmp3];
    elseif n >= bound(4) && n < bound(6)
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp = [dtmp1 dtmp2];
    elseif n >= bound(2) && n < bound(4)
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp = [dtmp1];
    else
        dtmp = d;
    end
    
    % Actual Filter
    [~,s_filter,~] = svd(dtmp);
    s_filter(1,1) = 0;
    % s_filter(2,2) = 0;
    % s_filter(3,3) = 0;
    [m_f, n_f] = size(s_filter);
    done = 1;
    b = 1;
    while (done == 1)
        a = 1;
        done = 0;
        m1 = b;
        m2 = b + m_f -1;
        if (m2 >= m), m2 = m; m1 = m - m_f + 1; done = 0; end;
        mmeffidx = m2 - m1;
        while(done == 0)
            n1 = a;
            n2 = a+n_f-1;
            if (n2 >= n), n2 = n; n1 = n - n_f + 1; done = 1; end;
            nneffidx = n2 - n1;
            dtmp = d(m1:m2,n1:n2);
            [u,~,v] = svd(dtmp);
            dtmp = u*s_filter*v';
            d(m1:m2,n1:n2) = dtmp;
            a = a + n_f;
        end
        b = b + m_f;
        if (m2 >= m), m2 = m; done = 2; end;
    end
    
    % Apply filter to
    if n >= bound(9)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d3 = d(:,bound(6):bound(7)); % Steigung
        d4 = d(:,bound(8):bound(9)); % Steigung
        d = [d1 d2 d3 d4];
    elseif n >= bound(7) && n < bound(9)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d3 = d(:,bound(6):bound(7)); % Steigung
        d = [d1 d2 d3];
    elseif n >= bound(5) && n < bound(7)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d = [d1 d2];
    elseif n >= bound(3) && n < bound(5)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d = [d1];
    else
        d = d;
    end
    
    gprdat(k).data = d;
    gprdat = renamedata(gprdat,k);
    
    k = k+1;
end

%% svd steigung gerade
if background == 5
    d = gprdat(k-1).data;
    [m, n]=size(d);
    
    % Build filter
    bound = [1 177 286 499 616 819 936 1143 1258 n]; % Boundaries, where the upGPR moves up and down
    
    if n >= bound(9)
        dtmp1 = d(:,bound(2):bound(3)); % Steigung
        dtmp2 = d(:,bound(4):bound(5)); % Steigung
        dtmp3 = d(:,bound(6):bound(7)); % Steigung
        dtmp4 = d(:,bound(8):bound(9)); % Steigung
        dtmp = [dtmp1 dtmp2 dtmp3 dtmp4];
    elseif n >= bound(7) && n < bound(9)
        dtmp1 = d(:,bound(2):bound(3)); % Steigung
        dtmp2 = d(:,bound(4):bound(5)); % Steigung
        dtmp3 = d(:,bound(6):bound(7)); % Steigung
        dtmp = [dtmp1 dtmp2 dtmp3];
    elseif n >= bound(5) && n < bound(7)
        dtmp1 = d(:,bound(2):bound(3)); % Steigung
        dtmp2 = d(:,bound(4):bound(5)); % Steigung
        dtmp = [dtmp1 dtmp2];
    elseif n >= bound(3) && n < bound(5)
        dtmp1 = d(:,bound(2):bound(3)); % Steigung
        dtmp = [dtmp1];
    else
        dtmp = d;
    end
    
    dtmp = d;
    
    % Actual Filter
    [~,s_filter,~] = svd(dtmp);
    s_filter(1,1) = 0;
    % s_filter(2,2) = 0;
    % s_filter(3,3) = 0;
    [m_f, n_f] = size(s_filter);
    done = 1;
    b = 1;
    while (done == 1)
        a = 1;
        done = 0;
        m1 = b;
        m2 = b + m_f -1;
        if (m2 >= m), m2 = m; m1 = m - m_f + 1; done = 0; end;
        mmeffidx = m2 - m1;
        while(done == 0)
            n1 = a;
            n2 = a+n_f-1;
            if (n2 >= n), n2 = n; n1 = n - n_f + 1; done = 1; end;
            nneffidx = n2 - n1;
            dtmp = d(m1:m2,n1:n2);
            [u,~,v] = svd(dtmp);
            dtmp = u*s_filter*v';
            d(m1:m2,n1:n2) = dtmp;
            a = a + n_f;
        end
        b = b + m_f;
        if (m2 >= m), m2 = m; done = 2; end;
    end
    
    % Apply filter to
    
    if n >= 1444
        d1 = d(:,bound(1):bound(2)); % gerade
        d2 = d(:,bound(3):bound(4)); % gerade
        d3 = d(:,bound(5):bound(6)); % gerade
        d4 = d(:,bound(7):bound(8)); % gerade
        d5 = d(:,bound(9):bound(10)); % gerade
        d = [d1 d2 d3 d4 d5];
    elseif n >= bound(8) && n < 1444
        d1 = d(:,bound(1):bound(2)); % gerade
        d2 = d(:,bound(3):bound(4)); % gerade
        d3 = d(:,bound(5):bound(6)); % gerade
        d4 = d(:,bound(7):bound(8)); % gerade
        d = [d1 d2 d3 d4];
    elseif n >= bound(6) && n < bound(8)
        d1 = d(:,bound(1):bound(2)); % gerade
        d2 = d(:,bound(3):bound(4)); % gerade
        d3 = d(:,bound(5):bound(6)); % gerade
        d = [d1 d2 d3];
    elseif n >= bound(4) && n < bound(6)
        d1 = d(:,bound(1):bound(2)); % gerade
        d2 = d(:,bound(3):bound(4)); % gerade
        d = [d1 d2];
    elseif n >= bound(2) && n < bound(4)
        d1 = d(:,bound(1):bound(2)); % gerade
        d = [d1];
    else
        d = d;
    end
    
    gprdat(k).data = d;
    gprdat = renamedata(gprdat,k);
    
    
    
    k = k+1;
    
end


%% svd steigung alles
if background == 6
    d = gprdat(k-1).data;
    [m, n]=size(d);
    
    % Build filter
    bound = [1 177 286 499 616 819 936 1143 1258 n]; % Boundaries, where the upGPR moves up and down
    
    if n >= bound(9)
        dtmp1 = d(:,bound(2):bound(3)); % Steigung
        dtmp2 = d(:,bound(4):bound(5)); % Steigung
        dtmp3 = d(:,bound(6):bound(7)); % Steigung
        dtmp4 = d(:,bound(8):bound(9)); % Steigung
        dtmp = [dtmp1 dtmp2 dtmp3 dtmp4];
    elseif n >= bound(7) && n < bound(9)
        dtmp1 = d(:,bound(2):bound(3)); % Steigung
        dtmp2 = d(:,bound(4):bound(5)); % Steigung
        dtmp3 = d(:,bound(6):bound(7)); % Steigung
        dtmp = [dtmp1 dtmp2 dtmp3];
    elseif n >= bound(5) && n < bound(7)
        dtmp1 = d(:,bound(2):bound(3)); % Steigung
        dtmp2 = d(:,bound(4):bound(5)); % Steigung
        dtmp = [dtmp1 dtmp2];
    elseif n >= bound(3) && n < bound(5)
        dtmp1 = d(:,bound(2):bound(3)); % Steigung
        dtmp = [dtmp1];
    else
        dtmp = d;
    end
    
    dtmp = d;
    
    % Actual Filter
    [~,s_filter,~] = svd(dtmp);
    s_filter(1,1) = 0;
    % s_filter(2,2) = 0;
    % s_filter(3,3) = 0;
    [m_f, n_f] = size(s_filter);
    done = 1;
    b = 1;
    while (done == 1)
        a = 1;
        done = 0;
        m1 = b;
        m2 = b + m_f -1;
        if (m2 >= m), m2 = m; m1 = m - m_f + 1; done = 0; end;
        mmeffidx = m2 - m1;
        while(done == 0)
            n1 = a;
            n2 = a+n_f-1;
            if (n2 >= n), n2 = n; n1 = n - n_f + 1; done = 1; end;
            nneffidx = n2 - n1;
            dtmp = d(m1:m2,n1:n2);
            [u,~,v] = svd(dtmp);
            dtmp = u*s_filter*v';
            d(m1:m2,n1:n2) = dtmp;
            a = a + n_f;
        end
        b = b + m_f;
        if (m2 >= m), m2 = m; done = 2; end;
    end
    
    
    gprdat(k).data = d;
    gprdat = renamedata(gprdat,k);
    
    k=k+1;
end

%% svd gerade steigung
if background == 7
    
    d = gprdat(k-1).data;
    [m, n]=size(d);
    
    % Build filter
    bound = [1 177 286 499 616 819 936 1143 1258 n]; % Boundaries, where the upGPR moves up and down
    
    if n >= 1444
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp3 = d(:,bound(5):bound(6)); % gerade
        dtmp4 = d(:,bound(7):bound(8)); % gerade
        dtmp5 = d(:,bound(9):bound(10)); % gerade
        dtmp = [dtmp1 dtmp2 dtmp3 dtmp4 dtmp5];
    elseif n >= bound(8) && n < 1444
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp3 = d(:,bound(5):bound(6)); % gerade
        dtmp4 = d(:,bound(7):bound(8)); % gerade
        dtmp = [dtmp1 dtmp2 dtmp3 dtmp4];
    elseif n >= bound(6) && n < bound(8)
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp3 = d(:,bound(5):bound(6)); % gerade
        dtmp = [dtmp1 dtmp2 dtmp3];
    elseif n >= bound(4) && n < bound(6)
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp2 = d(:,bound(3):bound(4)); % gerade
        dtmp = [dtmp1 dtmp2];
    elseif n >= bound(2) && n < bound(4)
        dtmp1 = d(:,bound(1):bound(2)); % gerade
        dtmp = [dtmp1];
    else
        dtmp = d;
    end
    
    % Actual Filter
    [~,s_filter,~] = svd(dtmp);
    s_filter(1,1) = 0;
    % s_filter(2,2) = 0;
    % s_filter(3,3) = 0;
    [m_f, n_f] = size(s_filter);
    done = 1;
    b = 1;
    while (done == 1)
        a = 1;
        done = 0;
        m1 = b;
        m2 = b + m_f -1;
        if (m2 >= m), m2 = m; m1 = m - m_f + 1; done = 0; end;
        mmeffidx = m2 - m1;
        while(done == 0)
            n1 = a;
            n2 = a+n_f-1;
            if (n2 >= n), n2 = n; n1 = n - n_f + 1; done = 1; end;
            nneffidx = n2 - n1;
            dtmp = d(m1:m2,n1:n2);
            [u,~,v] = svd(dtmp);
            dtmp = u*s_filter*v';
            d(m1:m2,n1:n2) = dtmp;
            a = a + n_f;
        end
        b = b + m_f;
        if (m2 >= m), m2 = m; done = 2; end;
    end
    
    gprdat(k).data = d;
    gprdat = renamedata(gprdat,k);
    
    k = k+1;
    
end

%median filter cut section
if background == 8
    %disp('Time picks have to be made on the whole line of traces (1444). If this is not the case, an error will show up because of tti(a).')
        d = gprdat(k-1).data;
    [m, n]=size(d);
        % Build filter
    bound = [1 177 286 499 616 819 936 1143 1258 n]; % Boundaries, where the upGPR moves up and down
    
    if n >= bound(9)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d3 = d(:,bound(6):bound(7)); % Steigung
        d4 = d(:,bound(8):bound(9)); % Steigung
        d = [d1 d2 d3 d4];
    elseif n >= bound(7) && n < bound(9)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d3 = d(:,bound(6):bound(7)); % Steigung
        d = [d1 d2 d3];
    elseif n >= bound(5) && n < bound(7)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d = [d1 d2];
    elseif n >= bound(3) && n < bound(5)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d = [d1];
    else
        d = d;
    end
%% filter implementation for all trace
    sumtr = median(d,2);
    
    %sumtr = median(gprdat(k-1).data,2); % horizontal filtering.   svd .. Fabian singular value decomp
    for b = 1:size(d,2)
        gprdat(k).data(:,b) = d(:,b) - sumtr;
    end;
    gprdat = renamedata(gprdat,k);
    
    
    k = k+1;
end

%median and svd beside each other for thesis
%median section
if background == 9
    %disp('Time picks have to be made on the whole line of traces (1444). If this is not the case, an error will show up because of tti(a).')
        d = gprdat(k-1).data;
    [m, n]=size(d);
        % Build filter
    bound = [1 177 286 499 616 819 936 1143 1258 n]; % Boundaries, where the upGPR moves up and down
    
    if n >= bound(9)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d3 = d(:,bound(6):bound(7)); % Steigung
        d4 = d(:,bound(8):bound(9)); % Steigung
        d = [d1 d2 d3 d4];
    elseif n >= bound(7) && n < bound(9)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d3 = d(:,bound(6):bound(7)); % Steigung
        d = [d1 d2 d3];
    elseif n >= bound(5) && n < bound(7)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d2 = d(:,bound(4):bound(5)); % Steigung
        d = [d1 d2];
    elseif n >= bound(3) && n < bound(5)
        d1 = d(:,bound(2):bound(3)); % Steigung
        d = [d1];
    else
        d = d;
    end
     gprdat(k).data =d;
     k =k+1;
    
    dfm = d;
    %%%SVD steigung steigung%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    n = length(d(1,:));
    %dv = zeros(size(d));
    a = 1;
    done = 0;
    while(done == 0)
        n2 = a;
        n3 = a+procp.svd_len-1;
        n1 = n2 - procp.svd_len;
        n4 = n3 + procp.svd_len;
        if (n1 < 1), n1 = 1; end;
        if (n3 >= n), n3 = n; done = 1; end;
        if (n4 > n), n4 = n; end;
        nneffidx = n3 - n2;
        dtmp = d(:,n1:n4);
        [u,s,v] = svd(dtmp);
        s(1,1) = 0;
        %   s(2,2) = 0;
        dtmp = u*s*v';
        d(:,n2:n3) = dtmp(:,(n2-n1+1):(n2-n1+1)+nneffidx);
        a = a + procp.svd_len;
    end;
    
    gprdat(k).data = d;
    gprdat = renamedata(gprdat,k);
    
    k = k+1;
    
    %%%%MEDIAN%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% filter implementation for all trace
    sumtr = median(dfm,2);
    
    %sumtr = median(gprdat(k-1).data,2); % horizontal filtering.   svd .. Fabian singular value decomp
    for b = 1:size(dfm,2)
        gprdat(k).data(:,b) = dfm(:,b) - sumtr;
    end;
    gprdat = renamedata(gprdat,k);
    
    
    k = k+1;
end








end

