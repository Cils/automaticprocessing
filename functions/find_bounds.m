
function [bound_SVD,exc,startTwt2]=find_bounds(gprdat,procp,bound_SVD,bkgr_filter,static_check,warn_sound,varargin)
% Plot raw data and current bounds. Bounds can be corrected and an
% exception can be set for skipping the scan in the evaluation.
% 
% Inputs:
% -------
% gprdat:       data container
% bound_SVD:    bounds
% procp:        Contains all setting for GPR processing
% bkgr_filter:  apply background filter, 
% static_check: Check static correction settings
% warnsound:    Make sound when plot is ready for inspection
% keep_plot:    If =1 the plot will not be closed at the end. Default =0. 
%
% Outputs:
% --------
% exc:          If =1, set this scan as exception preventing further use in
%               the data processing.
%
% By A.Capelli 2019 

%Settings
%--------
k=1;
% plot_fig=1;
exc=0;
% startTwt2 = procp.startTwt;

%--------------------------------------------------------------------------------------------
%-------------- Check inputs and set defaults if not defined --------------------------------
%--------------------------------------------------------------------------------------------
i_p = inputParser;
i_p.FunctionName = 'find_bounds';
i_p.addParameter('keep_plot',0,@(x) isnumeric(x) && isscalar(x));    
i_p.parse(varargin{:});


% -------------------------------------------------------------------------
% ------------------- remove background median filter ---------------------------------------
% -------------------------------------------------------------------------
if bkgr_filter
%     sumtr=median(gprdat(k).data,2);
    sumtr=median(gprdat(k).data(:,bound_SVD(1):end),2);
    
    for b = 1:gprdat(k).nr_of_traces
        gprdat(k).data(:,b) = gprdat(k).data(:,b) - sumtr;
    end
%     gprdat = renamedata(gprdat,k+1);
end

% -------------------------------------------------------------------------
% ------------------- BUTTERWORTH BANDPASS FILTER -------------------------
% -------------------------------------------------------------------------
if procp.butterbp1 == 1
    if ((procp.fl > 0) & (procp.fh > 0))

        % apply filter
        nyq = 0.5 / gprdat(k).sampling_rate * 1000;
        fl = procp.fl/nyq;
        fh = procp.fh/nyq;
        [bb,aa] = butter(procp.ford,[fl fh]);
        gprdat(k).data = filter(bb,aa,gprdat(k).data);
        gprdat(k).fdata = sum(abs(fft(gprdat(k).data))');    
    end
end
% -------------------------------------------------------------------------
% ------------------- Find variations ---------------------------------------
% -------------------------------------------------------------------------
% m=mean(diff(gprdat(k).data(),1,2),1);


% -------------------------------------------------------------------------
% ------------------- check static correction setting ---------------------------------------
% -------------------------------------------------------------------------

if static_check
        dates     = linspace(1,gprdat(k).nr_of_traces,gprdat(k).nr_of_traces);
        samples   = linspace(1,gprdat(k).nr_of_samples,gprdat(k).nr_of_samples);
        startTwt_s  = round(procp.startTwt/gprdat(k).sampling_rate);
        %newPickTwt = manualPick_FollowPhase2(gprdat(k-1).data, dates, samples, startDate, startTwt);
        [newPickTwt, newPickTwt_zero] = manualPick_FollowPhase2(gprdat(k).data, dates, samples, bound_SVD(2), startTwt_s);
end


% -------------------------------------------------------------------------
% ------------------- PLOT RAW DATA + bounds ---------------------------------------
% -------------------------------------------------------------------------
% get limits colormap
try
    c_min=min(gprdat(k).data(1:gprdat(k).viewlen,bound_SVD(1):bound_SVD(end)),[],'all');
    c_max=max(gprdat(k).data(1:gprdat(k).viewlen,bound_SVD(1):bound_SVD(end)),[],'all');
catch
    try
        c_min=min(gprdat(k).data(1:gprdat(k).viewlen,bound_SVD(1):end),[],'all');
        c_max=max(gprdat(k).data(1:gprdat(k).viewlen,bound_SVD(1):end),[],'all');
    catch
        c_min=min(gprdat(k).data(1:gprdat(k).viewlen,:),[],'all');
        c_max=max(gprdat(k).data(1:gprdat(k).viewlen,:),[],'all');
    end
end
clims=[c_min c_max];  % limits of colormap

fig = figure();clf;
imagesc(gprdat(k).data(1:gprdat(k).viewlen,:),clims)
set(gca,'YTick',0:gprdat(k).stepsize:gprdat(k).nr_of_samples)
set(gca,'YTickLabel',0:gprdat(k).deltat:gprdat(k).yend)
colormap(procp.cmap)
xlabel('Trace','fontsize',12);
ylabel(gprdat(k).ylab,'fontsize',12);
set(gca,'fontsize',12);
try % try to use the date as title 
    title( gprdat(k).date, 'Interpreter', 'none')
catch 
    try % use name as alternative or let title empty
        title( name)
    catch
        title('')
    end
end
hold on
% plot bounds
for i =1:length( bound_SVD)
    L_bounds(i)=plot( [bound_SVD(i) bound_SVD(i) ],[0 gprdat(k).viewlen], 'color','g' );
end

% plot maximum number of traces
plot( [procp.nmax procp.nmax],[0 gprdat(k).viewlen], 'color','m' )

if static_check
    try
        L_pick=plot(dates(bound_SVD(2):end),newPickTwt);
        L_pick_zero=plot(dates(bound_SVD(2):end),newPickTwt_zero);
        L_twt=plot([dates(1);dates(end)],[startTwt_s ;startTwt_s],'--k');
    catch
        disp('Can not plot picked line')
    end
else
    dates = linspace(1,gprdat(k).nr_of_traces,gprdat(k).nr_of_traces);
    startTwt_s  = round(procp.startTwt/gprdat(k).sampling_rate);
    L_twt=plot([dates(1);dates(end)],[startTwt_s ;startTwt_s],'--k');
end



% display instructions
disp('Press "space" to continue with next scan')
disp('Press "s" for choosing new starting point for automatic static correction ("startTwt") ')
disp([9, 'Use mouse to select starting point (y-axis)'])
disp('Press "b" for choosing new bonds for SVD filter!')
disp([9, 'Use mouse to select bound'])
disp([9, 'Press "n" for going to new bond)'])    
disp([9,'Press "v" for ending bond setting'])
disp('Press "e" for setting scan as exception that will not be used for further evaluation!!!')

% make warning sound 
if warn_sound
    WarnWave = [sin(1:.6:400), sin(1:.7:400), sin(1:.4:400)];
    sound(WarnWave)
end
 
% inputs of new setting
w=1;
while w
    [xx,yy,button] = ginput(1);
    
    if (button == 32) % "space" go to next scan
        w=0 ;
        
    elseif (button == 115) % "s" set new statrTWT
        try L_twt.delete; catch; end
        L_twt=plot([dates(1);dates(end)],[startTwt_s ;startTwt_s],'--r');
        [xx,yy,button] = ginput(1);
%         disp('Setting new starting point for automatic static correction ("startTwt") ')
%         disp('Use mouse to select starting point (y-axis)')
        startTwt_s  = round(yy);
        procp.startTwt = startTwt_s*gprdat(k).sampling_rate;

        
        % recompute follow phase
        [newPickTwt, newPickTwt_zero] = manualPick_FollowPhase2(gprdat(k).data, dates, samples, bound_SVD(2), startTwt_s);
        
        % delete old lines
        L_twt.delete
        L_pick.delete
        L_pick_zero.delete
        
        % plot new lines
        try
            L_pick=plot(dates(bound_SVD(2):end),newPickTwt);
            L_pick_zero=plot(dates(bound_SVD(2):end),newPickTwt_zero);
            L_twt=plot([dates(1);dates(end)],[startTwt_s ;startTwt_s],'--k');
        catch
            disp('Can not plot picked line')
        end
        
        
    elseif (button == 101) % set exception
        disp([gprdat(k).date,' was set as exception and will not be used for further evaluation!!!'])
        title( [gprdat(k).date,' was set as exception and will not be used for further evaluation!!!'], 'Interpreter', 'none')
        exc=1;
    elseif (button == 98)  % "b": change SVD filter bonds 
%         disp('Setting new starting point for automatic static correction ("startTwt") ')
%         disp('Press "n" for going to new bond')
%         disp('Use mouse to select starting point (y-axis)' )        
%         disp('Press "v" for ending bond setting')
        i=1;
        
        while i <(length( bound_SVD)+1) % wait for bond setting            
            L_bounds(i).delete
            L_bounds(i)=plot( [bound_SVD(i) bound_SVD(i) ],[0 gprdat(k).viewlen], 'color','r' );
            [xx,yy,button] = ginput(1);
            
            if button == 1 % mouse click, chouse bound
                bound_SVD(i)=round(xx);
%                 L_bounds(i).delete
%                 L_bounds(i)=plot( [bound_SVD(i) bound_SVD(i) ],[0 gprdat(k).viewlen], 'color','r' );
            elseif button == 110 % "n" go to next bound
               L_bounds(i).delete
               L_bounds(i)=plot( [bound_SVD(i) bound_SVD(i) ],[0 gprdat(k).viewlen], 'color','g' );
               i=i+1 ;
            elseif button == 118 % "v" exit bounds settings
               L_bounds(i).delete
               L_bounds(i)=plot( [bound_SVD(i) bound_SVD(i) ],[0 gprdat(k).viewlen], 'color','g' );
               i=length( bound_SVD)+1;
            end
        end
    end
end

% keep plot?
if ~i_p.Results.keep_plot, close(fig), end

startTwt2 = procp.startTwt;
end

