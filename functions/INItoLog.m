function [errCode,str] = INItoLog(inifile,varargin)
% writeToINI: Function for adding entry to .ini file. 
% It uses the class iniconfig by Iroln. See: iniconfig for more
% 
% Syntax:  writeToINI(inifile,key, value, section,v_format )
%
% Inputs:
%   inifile     -   Path of .ini file 
% Optional inputs:
%   logfile     -   path of logfile
%   remCom      -   remove comments if =1, Default
%
% Outputs:
%   msg         -   Content of .ini file
%
% Example: 
%    INItoLog(inifile,logpath, 0 )
%    
%
% Other m-files required: IniConfig.m
% Subfunctions: 
% MAT-files required: none
%
% See also: iniconfig
%
% Author: Achille Capelli, SLF
% email: capelli@slf.ch 
% November 2019; Last revision: 
   


    %--------------------------------------------------------------------------------------------
    %-------------- Check inputs and set defaults if not defined --------------------------------
    %--------------------------------------------------------------------------------------------
    i_p = inputParser;
    validpartOfSeason = @(x) (isvector(x) && isnumeric(x) && length(x)==2) || (isscalar(x) &&  x == 0);
    i_p.FunctionName = 'writeToINI';
    i_p.addRequired('inifile',@ischar);
    i_p.addOptional('logfile','',@ischar);
    i_p.addOptional('remCom',1,@(x) isnumeric(x) && isscalar(x));
    
    i_p.parse(inifile,varargin{:}); 
    
    % check if a path for logfile was given 
    if isempty(i_p.Results.logfile)
        Log=0;
    else
        Log=1;
    end
    errCode=0;
    str=' ';
        
    % open file
    ini = IniConfig();
    er = ini.ReadFile(inifile);
    str=ini.ToString();
    if ~er
       str=['The .ini could not be open! Path: ' inifile];
       disp(str)
       errCode=1;
       return 
    end
    
    % add [] to section name if necessary
    if i_p.Results.remCom
        s=splitlines(str);
        s2={};
        for i=1:length(s)
            if  ~(isempty( s{i}) || s{i}(1)== ';') 
                s2{end+1}=s{i};
                
            end
        end
        str = strjoin(s2,'\n');
    else
        
    end
        
   if Log
       fid = fopen(i_p.Results.logfile, 'w');
       if fid == -1
            disp('Cannot open log file.');
            errCode=1;
       end
       fprintf(fid,'%s', str);
       fclose(fid);
   end
    
end