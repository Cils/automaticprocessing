
% follows the phase
%
% radargramWithPhase: radargram with phase information (usually FMCW.ant1.radargram)
% dates: dates of measurements (usually FMCW.ant1.dates). Hast to be same length as size(radargramWithPhase,2)
% samples: samples of measurements (usually FMCW.ant1.samples-tv). Hast to be same length as size(radargramWithPhase,1)
% startDate: date of from where you want to follow the phase
% startTwt: twt of from where you want to follow the phase
%
% example pickTwt = followPhase(FMCW.ant1.radargram, FMCW.ant1.dates, FMCW.ant1.samples-tv, startDate, startTwt)
function [newPickTwt, newPickTwt_zero] = manualPick_FollowPhase2(radargram, dates, samples, startDate, startTwt)
    phaseChanges = sign(radargram(2:end,:)) - sign(radargram(1:end-1,:));

    % find indices in radargram of klick
    [~,Ix,~]=findNearest(dates, startDate);
    [~,Iy,~]=findNearest(samples, startTwt);
    
    iy=Iy;
    ixNewpick=1;
    signum=0;
    newPickTwt = zeros(size(radargram,2)-Ix+1,1);
    newPickTwt_zero = zeros(size(radargram,2)-Ix+1,1);
    for ix = Ix:size(radargram,2) % Dylan
        % if no phase change can be found
        if phaseChanges(:,ix) == zeros(length(phaseChanges(:,1)))
            phaseChanges(:,ix) = phaseChanges(:,ix-1);
        end
        
        % find zerocrossing below and above klick
        [IyminVor,IymaxVor]=findNextZeroCrossings(phaseChanges(:,ix), iy);
        
        % find min/max
        [~,IyMaxAmpl] = max(abs(radargram(IyminVor+1:IymaxVor,ix)));
        CMaxAmpl = radargram(IyMaxAmpl+IyminVor,ix);
        newPickTwt(ixNewpick)=samples(IyMaxAmpl+IyminVor);
        
        newPickTwt_zero(ixNewpick) = samples(IyminVor);% Dylan
       
        % if phase change occured
%         if signum~=sign(CMaxAmpl) && signum~=0
%             disp(['Phase changed on ' datestr(dates(ix)) ' at ' num2str(samples(iy))]);
%         end
        signum=sign(CMaxAmpl);
        
        iy=IyMaxAmpl+IyminVor;
        ixNewpick=ixNewpick+1;
    end
end

function [Iymin, Iymax] = findNextZeroCrossings(phaseChangeTrace, Iy)
    zeroCrossings = find(phaseChangeTrace);
    [~,IzeroC,Iynew]=findNearest(zeroCrossings, Iy);
    if Iynew>Iy
        if IzeroC==1
            Iymin=zeroCrossings(1);
            Iymax=zeroCrossings(2);
        else
            Iymin=zeroCrossings(IzeroC-1);
            Iymax=zeroCrossings(IzeroC);
        end
    else
        if IzeroC==length(zeroCrossings)
            Iymin=zeroCrossings(end-1);
            Iymax=zeroCrossings(end);
        else
            Iymin=zeroCrossings(IzeroC);
            Iymax=zeroCrossings(IzeroC+1);
        end
    end
end
