% find nearest value in a serie
% usage: [diff,I,value]=findNearest(vector, wanted)
%
function [diff,I,value]=findNearest(vector, wanted)
[C,I] = min(abs(vector - wanted));
diff=vector(I) - wanted;
value=vector(I);

