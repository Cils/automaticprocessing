function [ errorcode ] = seasonEval( folderpath, varargin )
%seasonEval - Manually check stacked data of all season and adjust filter parameters.
% 
% Syntax:  [ errorcode ] = seasonEval( evalfolder )
%          [ errorcode ] = seasonEval( evalfolder,'justnew',justnew,'partOfSeason' )
%
% Inputs:
%   path            -   Path of folder containig .ini file with settings and
%                       where outputs are saved. 
% Optional inputs:
%   partOfSeason    -   Boundaries of part of season to process or equal to 0. E.g.:  partOfSeason=[190505, 190615];
%   plStatCorTr     -   Prodce plots with the static corrections of all scans. To be used for quality control.
%   autoEval        -   Automatic evaluation without adjusting gain and saturation
%   fitDrift        -   Fit ground level drift along season.    
%   plotHS          -   Plot HS from external data in the final Radargram.
%
% Input files:
%   ?	allScans.mat  (data file)
% 	Eval*.ini.
% 
% Outputs:
%    errorcode      -   Errorcode.    
%                       0: successful evaluation.
%                       1: .ini file was not found. It must be in path and named Eval*.ini 
%
% Output files:
%   seasonProc.mat  (data file)
% 	various plots
% 	evalLog.txt
% 	INIlog.txt
% 	staticCorShift.mat
% 
% 
% Example: 
%    errorcode = seasonEval('N:\lawprae\LBI\Projects\101_upGPR\results\EvaluationHydYear2020')
%    or
%    errorcode = seasonEval(path,...,'partOfSeason',[190505, 190615])
%    
%
% Other m-files required: files in 'N:\lawprae\LBI\Projects\101_upGPR\postprocessing-scripts\Matlab\autoEvalProcessing'
% Subfunctions: none
% MAT-files required: none
%
% See also: PreproSeason
%
% Author: Achille Capelli, SLF
% % email: capelli@slf.ch 
% November 2019; Last revision: 

%------------- BEGIN CODE --------------

    %--------------------------------------------------------------------------------------------
    %-------------- Check inputs and set defaults if not defined --------------------------------
    %--------------------------------------------------------------------------------------------
    i_p = inputParser;
    validpartOfSeason = @(x) (isvector(x) && isnumeric(x) && length(x)==2) || (isscalar(x) &&  x == 0);
    i_p.FunctionName = 'seasonEval';
    i_p.addRequired('folderpath',@ischar);    
    i_p.addParameter('partOfSeason',0,validpartOfSeason);
    i_p.addParameter('autoEval',0,@(x) isnumeric(x) && isscalar(x));
    i_p.addParameter('plStatCorTr',0,@(x) isnumeric(x) && isscalar(x)); 
    i_p.addParameter('fitDrift',0,@(x) isnumeric(x) && isscalar(x));  
    i_p.addParameter('plotHS',0,@(x) isnumeric(x) && isscalar(x));
    i_p.parse(folderpath, varargin{:});   
    
    if length(i_p.Results.partOfSeason)>1
        partOfSeason_bounds=i_p.Results.partOfSeason;
        partOfSeason=1;
    else
        partOfSeason=0;
    end
    
    plStatCorTr=i_p.Results.plStatCorTr;   % check just new data
    fitDrift=i_p.Results.fitDrift;
    global f_path;
    f_path = folderpath;
    

    % -------------------------------------------------------------------------
    %% ----------------- define initial settings and load stuff ----------------
    % -------------------------------------------------------------------------
    
    % Create new entry in logfile
    logToFile(['New evaluation of stacked data.'],3)
%     logToFile(['folderpath: ' folderpath],4)
    % Add settings to Logfile
    logToFile('Inputs:',4)
    logToFile('---------',4)
    inputs=fieldnames(i_p.Results);
    for k=1:length(inputs)
        logToFile(join([inputs{k} ': '  string( i_p.Results.(inputs{k}))]),4)
    end
    
    
    % delete shit
    clear -regexp 'I'  'inifile' 'season';
    close 'all'
    
    
    % colorbar
    load zcm.mat                % blue-red colormap
    load blkwhtred.mat          % black-white-red colormap

    
    
    % set output errorcode to no error
    errorcode=0;

    % Load .ini. If not found display message and exit evaluation.
    try 
        inifile=dir([folderpath '\Eval*.ini']);
        I = INI('File',[folderpath '\' inifile.name]);
        % INI file reading
        I.read();
        logToFile(['path Eval*.ini: ',folderpath '\' inifile.name],4)
    catch ME
        errorcode=1;
        %disp(ME.identifier)
        disp('!Error!')
        %disp(['The .ini file was not found in path=' folderpath '. It must be in path and named Eval*.ini'])
        msg=['The .ini file was not found in path=' folderpath '. It must be named Eval*.ini. The folder should not contain more than one Eval*.ini file.'];
        logToFile(msg,1,'dispMsg',1)
        return
    end
    
    
    
    % ----------------- Extract setting from Eval*.ini ----------------
    general=I.General;  % general settings valid for all steps
    season=I.Season;  % general settings valid for all steps
    
    season.vsnow=I.OneScan.vsnow;
    samplingRate = I.General.meastime/I.General.n_samples;  % sampling time (ms)
    
    % Pack settings for plotRadargramm function
    setsRadargramm.propSpeed = season.vsnow;
    toPass= [ "yHS","ymax","fontSize","linewidth","markersize"];
    for i=1:length(toPass)
       try setsRadargramm.(toPass(i)) = season.(toPass(i));
       catch
       end
    end
    
    
    dataFile=fullfile(folderpath,  'allScans.mat'); % input data file
    
    % build output folder path and create it if not existing 
    if i_p.Results.autoEval
        outPath=fullfile(folderpath,'output_season_auto\');
    else
        outPath=fullfile(folderpath,'output_season_man\');
    end    
    if partOfSeason % change path if just part of season 
       outPath=fullfile(outPath,[num2str( partOfSeason_bounds(1)),'-',num2str( partOfSeason_bounds(2))],'\');
    end
    % make dir if doesn't exist
    if ~exist(outPath, 'dir')
       mkdir(outPath)
    end
 
    % plots to be saves
    figures=[];
    fignames={};
    
    % load HS data 
    if i_p.Results.plotHS == 1
        
        % set right year in HSdata path
        b=split(general.season,'-');
        WFJ_path=strrep(general.HSdataPath,'*year*',b(2));
        
        
        try
        %     WFJ_surface=load_WFJ(WFJ_path,'MST_flux');
            WFJ_surface=load_WFJ(WFJ_path,general.HSdataType);
            WFJ_surface.label='HS WFJ';
            WFJ_surface.twt=WFJ_surface.HS/season.vsnow*2;
            WFJ_surface.LS='g';
            
            % filter data
            WFJ_surface.HS(WFJ_surface.HS>7)=nan;
            
        catch ME
            WFJ_surface=[];
            
            msg=['The HSdata file was not found in path=' WFJ_path '. HS will not be plotted!'];
            logToFile(msg,2,'dispMsg',1)
        end
    end

    %% Load and join season data to single matrix
    load(dataFile,'dataScans');
   
    tra = length(dataScans);  % traces
    
    % process just part of season?
    indis=[];
    if partOfSeason
        for i=1:tra
            d=str2double([dataScans(i).date(1:6) '.' dataScans(i).date(8:11)]); %
%             datenum(dataScans(i).date, 'yymmdd_HHMM');
            if not( (partOfSeason_bounds(1)<= d) & (d<= partOfSeason_bounds(2)) )
                indis(end+1)=i;
            end
        end
        dataScans(indis)=[];
        tra = length(dataScans);
    end
    
    % define cutting length of traces
    if I.Season.cutlength > 0
        cutlength=I.Season.cutlength;
    else
        cutlength=max([dataScans.nr_of_samples]);
    end

    % Create data array
    data=zeros(cutlength,tra);
    date=zeros(tra,1);
    for i = 1:tra
        le=min([cutlength,length(dataScans(i).data)]);
        data(1:le,i) = dataScans(i).data(1:le); % cut data to a certain trace length
        date(i) = datenum(dataScans(i).date, 'yymmdd_HHMM');
    end
%     size(data)
    nr_samples = length(data(:,1)); % samples

    
    
    %% plot raw data
%   [ax,fig] = plotRadargram_raw(agc(data,season.agclen,samplingRate),date,season,season.task,samplingRate,'Raw data');
    [ax,fig] = plotRadargram_raw(data/max(data,[],'all'),date,season,season.task,samplingRate,'Raw data');
    data_raw = data;
    
    %add figure to saveing list
    a=gcf;
    figures(end+1)=a.Number;
    fignames{end+1}='raw.png';

    
    %% -------------------------------------------------------------------------
    % -------------- fit drift and apply correcture drift  ---------------------
    % --------------------------------------------------------------------------
    if fitDrift == 1
        
        
        
        if season.useSections == 0
            
            t = (0:nr_samples-1)*samplingRate*season.vsnow/2;

            [d,iy] = FollowPhase(data,t,season.pick);

            % plot picked drifts on radargram
            figure(fig)
            set(fig,'CurrentAxes',ax)
            hold on
            plot(date,d,...
                 'Color','g',...
                 'LineWidth',2);
            
            % Plot figure with correct date sincronisation
            startdate=datestr(date(1),'dd.mm.yyyy HH:MM:SS');
            stopdate=datestr(date(end),'dd.mm.yyyy HH:MM:SS');
            setsRadargramm2=setsRadargramm;
            setsRadargramm2.ymax=1.5;
            setsRadargramm2.yHS=1;
            Radar.radargramC1 = data;
            fig_drift_nice=figure();
            plotRadargram(startdate,stopdate,season.step,season.interpolTime,...
                          data/max(data,[],'all'),date,linspace(0,nr_samples*samplingRate,nr_samples)' /2*season.vsnow, season.language, blkwhtred, [-1,1],setsRadargramm2, ...
                          date,d/2*season.vsnow,'-g','fitted drift')
            
            % shift data
            data2=zeros(size(data));
            for j=1:tra
                % Set zero at season.zeroPoint , delete samples below zero   
                dy=iy(j)-iy(1)+round(season.zeroPoint/(samplingRate*season.vsnow/2));  
                if dy > 0
                    data2(1:nr_samples-dy ,j)= data(1+dy:nr_samples ,j);
                elseif dy < 0
                    data2(-dy+1:nr_samples ,j)= data(1:nr_samples+dy ,j);
                end
            end
        
            
            
        % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
        % use more than one picking section. Not clean jet!!!!  May not work!!!!  
        else  
            
            sections = datenum(season.sectionssections, 'yymmdd_HHMM');

            Picks(1).str= season.picks;

            colors=['g';'m';'b';'c';'y'];

            sect_ind(1)=1;
            for i=1:length(sections)
                sect_ind(i+1) = find(date> sections(i),1);
            end
            sect_ind(i+2)= length(date); 


            for i=1:length(Picks)
                t = (0:nr_samples-1)*samplingRate*season.vsnow/2;
            %   samples   = linspace(1,length(data(:,1)),length(data(:,1)));
            %   Xs=linspace(1,sect_ind(i+1)-sect_ind(i),sect_ind(i+1)-sect_ind(i));
                Picks(i).d=zeros(sect_ind(i+1)-sect_ind(i)+1,length(Picks));

                % create figure
                fig_drift=figure();
                hold on
                tt=[datestr(date(sect_ind(i)), 'dd/mm/YYYY'),'-',datestr(date(sect_ind(i+1)),'dd/mm/YYYY')];
                title(tt)
                legend()

                % loop through starting points
                for j=1:length(Picks(i).str)

                    [Picks(i).d(:,j),Picks(i).iy(:,j)] = FollowPhase(data(:,sect_ind(i):sect_ind(i+1)),...
                                                          t,...
                                                          Picks(i).str(j) );

            %         [~,Picks(i).d(:,j)] = manualPick_FollowPhase3(data(:,sect_ind(i):sect_ind(i+1)),...
            %                                       Xs,...
            %                                       t,...
            %                                       1,...
            %                                       Picks(i).str(j) );
                    % plot picked drifts

                    % on radargram
                    figure(fig)
                    set(fig,'CurrentAxes',ax)
                    hold on
                    plot(date(sect_ind(i):sect_ind(i+1)),...
                         Picks(i).d(:,j),...
                         'Color',colors(j),...
                         'LineWidth',2);

                    % difference between picking lines
                    figure(fig_drift)
                    plot(date(sect_ind(i):sect_ind(i+1)),Picks(i).d(:,j)-Picks(i).d(1,j),...
                         'Color',colors(j),...
                         'DisplayName',string( Picks(i).str(j) ) )
                end
            end

            
            % shift data
            data2=data;
            for i=1:length(Picks)

                for j=1:length(Picks(i).iy(:,1))
                    k=sect_ind(i)+j-1;
                    iy=Picks(i).iy(j,1)-Picks(i).iy(1,1);

                    if iy > 0
                        data2(1:nr_samples-iy ,k)= data(1+iy:nr_samples ,k);
                        if j>1
                            data2(nr_samples-iy+1:nr_samples ,k)= data2(nr_samples-iy+1:nr_samples ,k-1);
                        end

                    elseif iy < 0
                        data2(-iy+1:nr_samples ,k)= data(1:nr_samples+iy ,k);
                        if j>1
                            data2(1:-iy ,k)= data2(1:-iy ,k-1);
                        end
                    end
                end 
            end
        end
        % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
        

        
        
        [ax1,fig1] = plotRadargram_raw(data2/max(data2,[],'all'),date,season,season.task,samplingRate,'After shifting drift');
        fig2=figure();
        [ax2,fig2] = plotRadargram_raw(agc(data2,season.agclen,samplingRate),date,season,season.task,samplingRate,'After shifting drift + AGC');

        data=data2;
        
        %add figure to saveing list
        figures(end+1)=fig_drift_nice.Number;
        fignames{end+1}='fitDrift.png';
        figures(end+1)=fig1.Number;
        fignames{end+1}='DriftCorr.png';
        figures(end+1)=fig2.Number;
        fignames{end+1}='DriftCorrAGC.png';
    else 
        dy=round(season.zeroPoint/(samplingRate*season.vsnow/2));
        
        data(1:end-dy+1,:) = data(dy:end,:);        % shift data to zero Point
        data(end-dy+2:end,:) =0;
    end
    
    %% -----------------------------------------------------------------------------
    % ------------------- Plot PreproSeason static corrections for all Scans ----------
    % ------------------------------------------------------------------------------
    
    if plStatCorTr == 1
        
        fig_StatCorTr=figure();
        subplot(1,2,1)
        hold on
        for i = 1:length(dataScans)
            plot(dataScans(i).newPickTwt,...%                  'Color','k',...
                 'DisplayName',string(dataScans(i).date) )
            Twts(i,:)=dataScans(i).newPickTwt;
        end
        meanTwt=mean(Twts,1);    %       
        plot(meanTwt,'k--','LineWidth',2,'DisplayName','mean')
        

        subplot(1,2,2)
        hold on
        plot(date,max(Twts,[],2),'xg','DisplayName','max')
        plot(date,min(Twts,[],2),'xb','DisplayName','min')
        plot([date(1),date(end)],[min(meanTwt),min(meanTwt)],'b--','DisplayName','min(mean)')
        plot([date(1),date(end)],[max(meanTwt),max(meanTwt)],'g--','DisplayName','max(mean)')

        datetick('x','dd.mm.yy')
        legend
        
        
        % save mean startic correction line
        newPickTwt=meanTwt;
        tti=meanTwt/samplingRate;
        savefile = fullfile(outPath,'staticCorShift.mat');
        save(savefile,'tti','newPickTwt');
        
        %add figure to saveing list
        figures(end+1)=fig_StatCorTr.Number;
        fignames{end+1}='StatCorLines.png';
        
    end
    
    %% -------------------------------------------------------------------------
    % ------------------- Geometeric spreading compensation -----------------------------------
    % -------------------------------------------------------------------------
    if season.geomspread == 1
        % compute gain
        yr = (1:nr_samples)*samplingRate*season.vsnow/2/season.zeroPoint;

        % apply gain
        data_geospread = data.*yr';

        % normalization
        data_geospread =data_geospread /median(max(data_geospread));

        
        % plot data
        [ax2,fig2] = plotRadargram_raw(data_geospread,date,season,season.task,samplingRate,'Geometeric spreading compensation');
        %add figure to saveing list
        figures(end+1)=fig2.Number;
        fignames{end+1}='GeomSpreadComp.png';
        
        data=data_geospread;
    else
        data_geospread=0;
    end


    
   %% -----------------------------------------------------------------------------------------------------------
    % ------------- Apply background filter (moving window SVD Filtering, or other ...) -------------------------
    % -----------------------------------------------------------------------------------------------------------
     
    if season.svd_periods == 0
        gprdat = bckgamma( data,season.svd_len,samplingRate,season.decon_len,season.fl,season.fh,...
                           'median',season.median,'SVD',season.SVD,'deconv',season.deconv,'butther',season.butther);
        data = gprdat;
    else
        data2=zeros(size(data),'like',data);
        j=1;
        for i=1:length(season.svd_endDates)
%             disp(['Start period SVD nr.',string(i)])
            
            d=datenum(string(season.svd_endDates(i)), 'dd.mm');
            
            % assign rigth year in hydrological year.
            if month(d)>10 
                d=datenum(min(year(date)),month(d),day(d));
            else
                d=datenum(max(year(date)),month(d),day(d));
            end
            
            % find limit
            j2=find(date> d,1);
            if isempty(j2), j2=length(date); end
            
%             disp(fprintf('Start: %d.%d.%d',day(date(j)),month(date(j)),year(date(j))));
%             disp(fprintf('End: %d.%d.%d',day(d),month(d),year(d)));
            
            gprdat = bckgamma( data(:,j:j2),season.svd_len_multy(i),samplingRate,season.decon_len,season.fl,season.fh ,...
                               'median',season.median,'SVD',season.SVD,'deconv',season.deconv,'butther',season.butther);
            data2(:,j:j2)=gprdat;
            
            j=j2;
        end
        data = data2;
        disp('End SVD')
    end
    
    fig3=figure();
        % plot data
    [ax3,fig3]= plotRadargram_raw(data/max(data,[],'all'),date,season,season.task,samplingRate,'SVD');
    
    %add figure to saveing list
    figures(end+1)=fig3.Number;
    fignames{end+1}='SVD.png';
    
        
   %% -------------------------------------------------------------------------
    % ------------------- AGC GAIN FUNCTION -----------------------------------
    % -------------------------------------------------------------------------
    if season.agcgain == 1
        if season.agclen > 0 && season.agclen < samplingRate*nr_samples
            data = agc(data,season.agclen,samplingRate); % f�r Plots zur AGC function muss hier folgendes eingef�gt werden: data = agc2(data,season.agclen,samplingRate);
        elseif season.agclen >=   samplingRate*nr_samples
            disp(['AGC length (agclen) is too long. Type in value lower than ', num2str(nr_samples)])
        elseif season.agclen == 0
            disp('No AGC gain function applied (agclen = 0)')
        else
            disp(['AGC length (agclen) has to be a positive number smaller than ',num2str(nr_samples)])
        end
        
        % plot data
        [ax2,fig2] = plotRadargram_raw(data,date,season,season.task,samplingRate,'AGC');
        %add figure to saveing list
        figures(end+1)=fig2.Number;
        fignames{end+1}='AGC.png'; 
    end


    
   
   %% -------------------------------------------------------------------------
    % ---------------------- Plot and ADJUST COLORAXIS ------------------------
    % -------------------------------------------------------------------------
    fig_fin=figure();
    Title = 'Season';
    
    t = (0:nr_samples-1)*samplingRate;
    date2 = date'; %datestr(date);
    button = 0;
    datao = data;
    season.cmap = blkwhtred;

    % set y axis to time or depth
    if (season.task == 0)
        xr = date2;
        yr = t;
        xlab = 'Date';
        ylab = 'Time [ns]';
        yd = 'normal';
    end

    if (season.task == 1)
        xr = date2;
        yr = t*season.vsnow/2;
        xlab = 'Date';
        ylab = 'Depth [m]';
        yd = 'normal';
    end

    tfun = 0;  % exponential scaling factor for vertical axis
    cax = [-1.0 1.0];     % Lower and upper bounds of colorbar
    
    if (i_p.Results.autoEval == 1)
        try % apply settings from .INI file
            cax=cax.*season.cax;
            tfun=season.tfun;
        catch
             logToFile(['Plot adjustment settings not found in .INI. (cax or tfun)'],2,'dispMsg',1);
        end
    end

    if (i_p.Results.autoEval == 0)
        disp('Choose colormap with 1/2')
        disp('Choose caxis with left/right')
        disp('Choose vertical scaling with up/down')
        disp('Enter for finish selection')
    end;     

        
    

    while(button ~= 27) 
        if (tfun ~= 0)
            data = datao.* exp(-(1:length(yr))*tfun)';
        end;
        
        
        
        imagesc(xr,yr,data);
        xlabel(xlab,'fontsize',12);
        ylabel(ylab,'fontsize',12);
        set(gca,'fontsize',12);
        title(Title,'fontsize', 12,'interpreter','none');
        colormap(season.cmap);    
        caxis(cax); %caxis('auto');% caxis(season.cax); 
        freezeColors;
        set(gca,'YDir',yd);
        %colorbar
        datetick('x','dd.mm.yy');
        
        % set xlimits
        xlim([xr(1),xr(end)]);
            
        if (i_p.Results.autoEval == 1), break; end       % with flag autoeval skip manual adjusting step

        [xx,yy,button] = ginput(1);
        if (button == 28), cax = cax/season.dcax; end;
        if (button == 29), cax = cax*season.dcax; end;
        if (button == 49), season.cmap = zcm; end;
        if (button == 50), season.cmap = blkwhtred; end;                
        if (button == 30), tfun = tfun + season.dta; end;
        if (button == 31), tfun = tfun - season.dta; end;
    end;        
    
    Radar.dates = date2;
    Radar.radargramC1 = data;
    Radar.samples = linspace(0,nr_samples*samplingRate,nr_samples)';
    
    %add figure to saveing list
    figures(end+1)=fig_fin.Number;
    fignames{end+1}='seasonProc.png';
    
    
    %% ---------------------------------------------------------------------------------------
    % ---------------------- Plot Radargramm with right dates and HS  ------------------------
    % ----------------------------------------------------------------------------------------

    startdate=datestr(Radar.dates(1),'dd.mm.yyyy HH:MM:SS');
    stopdate=datestr(Radar.dates(end),'dd.mm.yyyy HH:MM:SS');

    fig_nice=figure();
    if isempty(WFJ_surface) == 0 % plot HS
        plotRadargram(startdate,stopdate,season.step,season.interpolTime,...
            Radar.radargramC1,Radar.dates,Radar.samples, season.language, blkwhtred, cax,setsRadargramm, ...
            WFJ_surface.dates,WFJ_surface.HS,'-g','HS WFJ');
    else
        plotRadargram(startdate,stopdate,season.step,season.interpolTime,...
            Radar.radargramC1,Radar.dates,Radar.samples, season.language, blkwhtred, cax,setsRadargramm );
    end
    set(fig_nice,'units','centimeters','position',[10 8  season.figsize_w   season.figsize_h]);
    
    %add figure to saveing list
    figures(end+1)=fig_nice.Number;
    fignames{end+1}='radargrammSeason.png';
    
    
    
   %% ---------------------------------------------------------------------
    % ---------------------- save data and plots ---------------------------
    % ---------------------------------------------------------------------
    % save data
    save(fullfile(outPath,'seasonProc.mat'),'Radar');

    % save .jpg figure
    % save plots
    for i=1:length(figures)
        try
            figure(figures(i));
            plotfile = fullfile(outPath,fignames{i});
            print(plotfile,'-dpng','-r300');
            print('-dpng','-r300',plotfile);
        catch
            logToFile(['Figure: ',fignames{i},' could not be saved!'],2);
            logToFile(plotfile,4);
        end
    end
    
    % save as matlab figures
    saveas(fig_fin,'seasonProc.fig');
    saveas(fig_nice,'radargrammSeason.fig');
    
    % save setting in .INI in a logfile
    INItoLog(fullfile(folderpath,inifile.name),fullfile(outPath,'INIlog.txt'));
    

    logToFile(' ',4,'dispMsg',1)
    logToFile('Manual coloraxis settings',4,'dispMsg',1)
    logToFile('-------------------------',4,'dispMsg',1)
    logToFile(['tfun: ',num2str(tfun)],4,'dispMsg',1)
    logToFile(['cax: ',num2str(cax)],4,'dispMsg',1)
    logToFile([' ';' '],4,'dispMsg',1)
    logToFile(['Errorcode: ',num2str(errorcode)],4,'dispMsg',1);
end


