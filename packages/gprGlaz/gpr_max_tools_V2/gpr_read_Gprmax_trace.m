% read in gprMax trace data and plot 

function [gprdat n] = gpr_read_Gprmax_trace(para, n)

% para.input = 'DLINE44_migT_x.geo'
% automatisch figure sav!!!!
% addieren von allen Feldern!! dann output SWITCH!!!


% cd para.path
files = dir('*.out');
ifiles = numel(files);

partstr = '(\d*).out';
searchname = sprintf('%s%s',para.input,partstr);
TID = zeros(ifiles,1);

dummy = dir (files(1).name);
dummysize = dummy.bytes;

for i = 1:ifiles
    
    testfile = dir(files(i).name);
    testsize = testfile.bytes;
    
    strdigits = regexp(files(i).name,searchname,'tokens');
    TID(i,1) = str2double(strdigits{1});
    
    if testsize == dummysize
        
        [Header,Fields]=gprmax(files(i).name);    
    else  
        
        [Header,Fields]=gprmax_resize(files(i).name); 
    end
    
    Tracefields.ex(:,i) = squeeze(Fields.ex);
    Tracefields.ey(:,i) = squeeze(Fields.ey);
    Tracefields.ez(:,i) = squeeze(Fields.ez);
    Tracefields.hx(:,i) = squeeze(Fields.hx);
    Tracefields.hy(:,i) = squeeze(Fields.hy);
    Tracefields.hz(:,i) = squeeze(Fields.hz);
    Tracefields.ix(:,i) = squeeze(Fields.ix);
    Tracefields.iy(:,i) = squeeze(Fields.iy);
    Tracefields.iz(:,i) = squeeze(Fields.iz);
    
    gprdat.x(1,i) = (Header.tx+(abs(Header.tx-Header.rx)/2))*Header.dx;                     %coordinates of middle between Tx and Rx
    gprdat.y(1,i) = (Header.ty+(abs(Header.ty-Header.ry)/2))*Header.dy;
    gprdat.z(1,i) = (Header.tz+(abs(Header.tz-Header.rz)/2))*Header.dz;
end

%%
%use gprdat.z somehow to plot it correctly with the altitude, no everything
%is references to a flat surface

it = Header.iterations;
l = -0.01;
u = 0.01;
x = (para.traceinterval*TID)';
y = 1:1:Header.iterations;

figure
subplot(311)
imagesc(Tracefields.ex);
colormap('gray');
caxis([l u])
colorbar
ylim([0 it])
title('Ex')

subplot(312)
imagesc(x,y,Tracefields.ey);
colormap('gray');
caxis([l u])
colorbar
ylim([0 it])
title('Ey')
xlabel('x [m]')
ylabel('t [ns]')

subplot(313)
imagesc(Tracefields.ez); 
colormap('gray');
caxis([l u])
colorbar
ylim([0 it])
title('Ez')

% put all fields together, no ez component because antennas are not
% sensitive in this direction
gprdat.data = Tracefields.ex+Tracefields.ey;


% % always gives the strongest electromagnetic field component for the specific antenna orientation
% if Header.polarization == 120           %transverse
%     gprdat.data = Fields.ex;                                                            
% elseif Header.polarization == 121       %axial
%     gprdat.data = Fields.ey;   
% else 
%     gprdat.data = Fields.ez;
% end

[a b] = size(Tracefields.ex);
gprdat.nr_of_traces = b;
gprdat.nr_of_samples = Header.iterations;
gprdat.t0 = [];
gprdat.time_window = Header.removed/1e-9;

if Header.TxStepX ~= 0
    gprdat.separation = (Header.rx - Header.tx)*Header.dx;
elseif Header.TxStepY ~= 0
    gprdat.separation = (Header.ry - Header.ty)*Header.dy;   
else %Header.TxStepZ ~= 0
    gprdat.separation = (Header.rz - Header.tz)*Header.dz;
end

gprdat.frequency = (str2num(Header.title))/1e6;                                                     
gprdat.sampling_rate = Header.dt*1e9;
gprdat.number_stacks = [];
gprdat.dataz   = [];

gprdat.pos0    = [];


if nargin > 1
    n = n +1;
end



    
    