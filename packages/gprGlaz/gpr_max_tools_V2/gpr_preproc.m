%% Preprocessing I
% workflow to read in raw data, assing geometry (georefernce coordinates) and save for further
% processing

%% Add GPRmatlab path
clear all
close all
clc

% add project path
addpath(genpath('D:\GPR\Projects'))                                         
%% initialize project

% project name
GPRproject_prjname = 'DLINE_33_34_39_ax_migT';                                         
% set path/file manually, comment out for gui
% raw data path
GPRproject_prjpath = 'D:\GPR\Projects\gprMax_glacier_modeling\Results\DLINE_33_34_39_pick\axial\';
% raw data file
GPRproject_datfile = ''; %'line11Ch01Tx1Rx2.DT1';
% gprMAX files 
GPRproject_gprmaxfile = 'DLINE_33_34_39_migT_';
% topography 
% GPRproject_topo    = 1;
% GPS file name
GPRproject_gpsfile = '';%'DLINE30.GPS'; %'FILE____006_swiss.txt';
% GPS file opath
GPRproject_pospath = '';%'D:\GPR\Projects\Otemma_20141017\Otemma_20141017_GPS\';
% Refernced position file
GPRproject_posfile = '';%'Otemma_october_radar.txt';
% Save path
GPRproject_save    = 'D:\GPR\Projects\gprMax_glacier_modeling\Results\DLINE_33_34_39_pick\axial\';

cd (GPRproject_prjpath)
n = 1;

%% plotting parameters

% plot.comap = 'blkwhtred';
% plot.comap = 'cmap';
% plot.comap = 'bgr';               %blue,gray,red
pfig.comap = 'ber';                 %blue,egg,red
pfig.mode = 2;                      % 1 = ns, 2 = m
pfig.velo = 0.167;                 % depth in m ice velocity = 0.1689 m/ns
pfig.min = -0.01;
pfig.max = 0.01;

%% import gpr-data
% read in function depends on data type

gprflw(1).para.path     = GPRproject_prjpath;
gprflw(1).para.datafile = GPRproject_datfile;
% % 
% gprflw(1).name = 'import_PulseEkko';
% [gprdat(1) n] = gpr_readPulseEkko(gprflw(1).para, 1);

% gprflw(1).name = 'import_MalaRamac';
% [gprdat(1) n] = gpr_readMalaRamac(gprflw(1).para, 1);

% gprflw(1).name = 'import_GSSI';
% [gprdat(1) n] = gpr_readGSSI(gprflw(1).para, 1);

% gprflw(1).name = 'import_GPRmax';
% [gprdat(1) n] = gpr_readGPRmax(gprflw(1).para, 1);

% gprflw(1).name = 'import_GPRmax';
% gprflw(1).para.input1 = 'gla_air_y1.out';
% gprflw(1).para.input2 = 'gla_air_y2.out';
% gprflw(1).para.input3 = 'gla_air_y3.out';
% gprflw(1).para.input4 = 'gla_air_y4.out';
% gprflw(1).para.input5 = 'gla_air_y5.out';
% [gprdat(1) n] = gpr_read_joinedGprmax(gprflw(1).para, 1);

gprflw(1).name = 'import_GPRmax';
gprflw(1).para.input = GPRproject_gprmaxfile;
gprflw(1).para.traceinterval = 3.0;
[gprdat(1) n] = gpr_read_Gprmax_trace(gprflw(1).para, 1);

gpr_plot2D(gprdat(n-1),pfig)
%% get position of each trace                                               
% go into gpr_getTracePosition to change swiss/ utm84 (lat/long) coordinates input requirements
% 1: PE swiss / utm84 (lat/long) coordinates from .GPS file
% 2: PE swiss / utm84 (lat/long) coordinates - interpolation on reference gps-data
% 3: GSSI swiss coordinates from GEOSAT file
% 4: GPRmax coordinates
% 5: Ramac rtk_positions in .cor file, 
% 6: Ramac interpolation
% 7 :get position from header

gprflw(1).name         = 'gpr_getTracePosition';
gprflw(1).para.path    = GPRproject_prjpath;
gprflw(1).para.gpsfile = GPRproject_gpsfile;
gprflw(1).para.pospath = GPRproject_pospath;
gprflw(1).para.posfile = GPRproject_posfile;
gprflw(1).para.timesft = 1;
gprflw(1).para.gpstype = 4;

[gprdat(1) n] = gpr_getTracePosition_vari(gprdat(1), gprflw(1).para, 1);      %ramac not tried yet!

gpr_plot2D(gprdat(n-1),pfig)
%% save project to save path
% flow and data files

save([GPRproject_save GPRproject_prjname '.gprf'], 'gprflw');
save([GPRproject_save GPRproject_prjname '.gprd'], 'gprdat');