function Map_Aletsch

% ------------------ BGR ------------------
pp = '/Volumes/ANJA_1GB/MScThesis/GPRData/';
matdata1 = '/Volumes/ANJA_1GB/MScThesis/GPRData/BGR/matdata/';
matdata2 = matdata1;
matdata3 = matdata1;
matdata4 = matdata1;
matdata5 = matdata1;


% filedate1 = 'ALETSCH_1_20090331';
% filedate2 = 'ALETSCH_2_20090331';
% filedate3 = 'ALETSCH_3_20090331';
% filedate4 = 'ALETSCH_4_20090401';

filedate1 = 'ALETSCH_1_20110322';
filedate2 = 'ALETSCH_2_20110322';
filedate3 = 'ALETSCH_3_20110322';
filedate4 = 'ALETSCH_4_20110322';
filedate5 = 'FIESCH_20110321';

segyfolder1 = sprintf('%s%s_Segy/',matdata1,filedate1);
segyfolder2 = sprintf('%s%s_Segy/',matdata2,filedate2);
segyfolder3 = sprintf('%s%s_Segy/',matdata3,filedate3);
segyfolder4 = sprintf('%s%s_Segy/',matdata4,filedate4);
segyfolder5 = sprintf('%s%s_Segy/',matdata5,filedate5);



dd1 = dir([segyfolder1 sprintf('%s_mig_PROFILE_*.sgy',filedate1)]);
dd2 = dir([segyfolder2 sprintf('%s_mig_PROFILE_*.sgy',filedate2)]);
dd3 = dir([segyfolder3 sprintf('%s_mig_PROFILE_*.sgy',filedate3)]);
dd4 = dir([segyfolder4 sprintf('%s_mig_PROFILE_*.sgy',filedate4)]);
dd5 = dir([segyfolder5 sprintf('%s_mig_PROFILE_*.sgy',filedate5)]);

dd2 = 0;
dd4 = 0;

procp.dri = 1;
%year = '2009';
year = '2011';

for aa = 3;
    if aa == 1; mapname = 'Aletsch_Map';  end
    if aa == 2; mapname = 'Aletsch_Map_1'; end    
    if aa == 3; mapname = 'Aletsch_Map_2'; end 
    if aa == 4; mapname = 'Aletsch_Map_3'; end
    if aa == 5; mapname = 'Aletsch_Map_4'; end


    mapfile = [pp sprintf('%s.mat',mapname)];
    fig = figure(7); clf
    showmap(mapfile); hold on
    load(mapfile);
    xmin = x1;
    xmax = x2;
    ymin = y1;
    ymax = y2;

    % ------------ FILEDATE 1 ------------
    for a = [10 23]       
    %for a = 1:length(dd1);
        segyname = sprintf('%s%s_mig_PROFILE_%03d',segyfolder1,filedate1,a);
        [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
        
        % Check if points are within Map
        if px(1) < xmin; continue; end
        if px(end) > xmax; continue; end
        if py(1) < ymin; continue; end
        if py(end) > ymax; continue; end
        
        L1 = plot(px/1000,py/1000,'k','LineWidth', 1);
        plot(px(1)/1000,py(1)/1000,'k.','markersize',10); % Start Point
        % Marker every 200 m
        clear iitic
        xx = (0:ntr-1)*procp.dri;
        ticm = 0:200:max(xx);
        for b = 1:length(ticm)
            [~,iitic(b)] = min(abs(xx-ticm(b))); end
        for bb = 1:length(iitic)
            plot(px(iitic(bb))/1000,py(iitic(bb))/1000,'b.','markersize',5); end
        %Name the profiles
        text(px(1)/1000,py(1)/1000,sprintf('%d',a),'color','k','fontsize',10,'fontweight','bold','HorizontalAlignment','right');
    end

    % ------------ FILEDATE 2 ------------
    for a = 1:length(dd2); continue
        segyname = sprintf('%s%s_mig_PROFILE_%03d',segyfolder2,filedate2,a);
        [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
                
        % Check if points are within Map
        if px(1) < xmin; continue; end
        if px(end) > xmax; continue; end
        if py(1) < ymin; continue; end
        if py(end) > ymax; continue; end
        
        L2 = plot(px/1000,py/1000,'b','LineWidth', 1);
        plot(px(1)/1000,py(1)/1000,'b.','markersize',10); % Start Point
        % Marker every 100 m
        clear iitic
        xx = (0:ntr-1)*procp.dri;
        ticm = 0:200:max(xx);
        for b = 1:length(ticm)
            [~,iitic(b)] = min(abs(xx-ticm(b))); end
        for bb = 1:length(iitic)
            plot(px(iitic(bb))/1000,py(iitic(bb))/1000,'k.','markersize',5); end
        % Name the profiles
        text(px(1)/1000,py(1)/1000,sprintf('%d',a),'color','b','fontsize',10,'fontweight','bold','HorizontalAlignment','right');
    end
    
     % ------------ FILEDATE 3 ------------
     for a = 17
    %for a = 1:length(dd3);
        segyname = sprintf('%s%s_mig_PROFILE_%03d',segyfolder3,filedate3,a);
        [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
                
        % Check if points are within Map
        if px(1) < xmin; continue; end
        if px(end) > xmax; continue; end
        if py(1) < ymin; continue; end
        if py(end) > ymax; continue; end
        
        L3 = plot(px/1000,py/1000,'k','LineWidth', 1);
        plot(px(1)/1000,py(1)/1000,'k.','markersize',10); % Start Point
        % Marker every 200 m
        clear iitic
        xx = (0:ntr-1)*procp.dri;
        ticm = 0:200:max(xx);
        for b = 1:length(ticm)
            [~,iitic(b)] = min(abs(xx-ticm(b))); end
        for bb = 1:length(iitic)
            plot(px(iitic(bb))/1000,py(iitic(bb))/1000,'b.','markersize',5); end
        % Name the profiles
        text(px(1)/1000,py(1)/1000,sprintf('%d',a),'color','k','fontsize',10,'fontweight','bold','HorizontalAlignment','right');
    end
    
     % ------------ FILEDATE 4 ------------
    for a = 1:length(dd4); continue
        segyname = sprintf('%s%s_mig_PROFILE_%03d',segyfolder4,filedate4,a);
        [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
                
        % Check if points are within Map
        if px(1) < xmin; continue; end
        if px(end) > xmax; continue; end
        if py(1) < ymin; continue; end
        if py(end) > ymax; continue; end
        
        L4 = plot(px/1000,py/1000,'r','LineWidth', 1);
        plot(px(1)/1000,py(1)/1000,'r.','markersize',10); % Start Point
        % Marker every 200 m
        clear iitic
        xx = (0:ntr-1)*procp.dri;
        ticm = 0:200:max(xx);
        for b = 1:length(ticm)
            [~,iitic(b)] = min(abs(xx-ticm(b))); end
        for bb = 1:length(iitic)
            plot(px(iitic(bb))/1000,py(iitic(bb))/1000,'b.','markersize',5); end
        % Name the profiles
        text(px(1)/1000,py(1)/1000,sprintf('%d',a),'color','r','fontsize',10,'fontweight','bold','HorizontalAlignment','right');
    end
    
     % ------------ FILEDATE 5 ------------
     for a = 4;
        segyname = sprintf('%s%s_mig_PROFILE_%03d',segyfolder5,filedate5,a);
        [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
                
        % Check if points are within Map
        if px(1) < xmin; continue; end
        if px(end) > xmax; continue; end
        if py(1) < ymin; continue; end
        if py(end) > ymax; continue; end
        
        L5 = plot(px/1000,py/1000,'b','LineWidth', 1);
        plot(px(1)/1000,py(1)/1000,'b.','markersize',10); % Start Point
        % Marker every 200 m
        clear iitic
        xx = (0:ntr-1)*procp.dri;
        ticm = 0:200:max(xx);
        for b = 1:length(ticm)
            [~,iitic(b)] = min(abs(xx-ticm(b))); end
        for bb = 1:length(iitic)
            plot(px(iitic(bb))/1000,py(iitic(bb))/1000,'k.','markersize',5); end
        % Name the profiles
        text(px(1)/1000,py(1)/1000,sprintf('%d',a),'color','b','fontsize',10,'fontweight','bold','HorizontalAlignment','right');
    end
        

    %title(sprintf('%s %s',mapname, year),'interpreter','none');
    title(sprintf('%s Repeatabiltity BGR-P30',mapname),'interpreter','none');

    xlabel('West-East [km]');
    ylabel('North-South [km]');
    set(gca,'dataaspectratio',[1 1 1]);
    
    if exist('L1') == 0; L1 = []; disp('No filedate1 in Legend'); end
    if exist('L2') == 0; L2 = []; disp('No filedate2 in Legend'); end
    if exist('L3') == 0; L3 = []; disp('No filedate3 in Legend'); end
    if exist('L4') == 0; L4 = []; disp('No filedate4 in Legend'); end
    if exist('L5') == 0; L5 = []; disp('No filedate5 in Legend'); end

    %hleg = legend([L1 L2 L3 L4],{filedate1 filedate2 filedate3 filedate4});
    hleg = legend([L1 L5],{filedate1 filedate5});

    set(hleg,'Interpreter','none','Location','NorthEast');
    clear L1 L2 L3 L4 L5
    
    %print('-djpeg99','-r600', sprintf('%s %s',mapname,year));
    print('-djpeg99','-r600', sprintf('%s_RepeatabilityBGR',mapname));
    

end