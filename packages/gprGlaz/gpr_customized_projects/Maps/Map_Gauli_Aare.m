function Map_Gauli_Aare

close all

% ------------------ RST ------------------
matdata1 = 'D:\ranja\DataBase\GPRDataBase\SegyFiles\BGR\'; %path to folder matdata
%filedate1 = 'GAULI_20120326';
%filedate1 = 'OBERAAR_1_20120326';
%filedate1 = 'OBERAAR_2_20120326';
filedate1 = 'OBERAAR_20120327';
%filedate1 = 'FINSTERAAR_20120326';


segyfolder1 = sprintf('%s%s_Segy\\',matdata1,filedate1);

dd1 = dir([segyfolder1 sprintf('%s_mig_PROFILE_*.sgy',filedate1)])
%dd1 = dir([segyfolder1 sprintf('%s_proc1_PROFILE_*.sgy',filedate1)]);
%dd1 = dir([segyfolder1 sprintf('%s_RAW_PROFILE_*.sgy',filedate1)]);


procp.dri = 1;

%mapname = 'GAULI_20120326_Map';
%mapname = 'OBERAAR_1_20120326_Map';
%mapname = 'OBERAAR_2_20120326_Map';
mapname = 'OBERAAR_20120327_Map';
%mapname = 'FINSTERAAR_20120326_Map';


mapfile = 'D:\ranja\MScThesis\GPRData\BGR\matdata\Gauli_Aare_Map.tif';

fig = figure(1); clf
mapshow(mapfile);
axis tight
hold on


% ------------ FILEDATE 1 ------------
for a = 1:length(dd1);
    segyname = sprintf('%s%s_mig_PROFILE_%03d',segyfolder1,filedate1,a);
    %segyname = sprintf('%s%s_RAW_PROFILE_%03d',segyfolder1,filedate1,a);
    [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
    
    
    L1 = plot(px,py,'k','LineWidth', 1);
    plot(px(1),py(1),'k.','markersize',10); % Start Point
    % --- Marker every 100 m ---
    clear iitic
    xx = (0:ntr-1)*procp.dri;
    ticm = 0:200:max(xx);
    for b = 1:length(ticm)
        [~,iitic(b)] = min(abs(xx-ticm(b))); end
    for bb = 1:length(iitic)
        plot(px(iitic(bb)),py(iitic(bb)),'b.','markersize',5); end

    % Name the profiles
    text(px(1),py(1),sprintf('%d',a),'color','k','fontsize',8,'fontweight','bold','HorizontalAlignment','right');
end



title(sprintf('%s',mapname),'interpreter','none');
xlabel('LK west-east [m]');
ylabel('LK north-south [m]');
set(gca,'dataaspectratio',[1 1 1]);

% % Gauli
% xmin = 6.535*10^5;
% xmax = 6.590*10^5;
% ymin = 1.610*10^5;
% ymax = 1.650*10^5;
% axis([xmin xmax ymin ymax])


% Oberaar
xmin = 6.560*10^5;
xmax = 6.615*10^5;
ymin = 1.520*10^5;
ymax = 1.560*10^5;
axis([xmin xmax ymin ymax])

% hleg = legend([L1 L2],{filedate1, filedate2});
% set(hleg,'Interpreter','none','Location','SouthEast');
% clear L1 L2

print('-djpeg99','-r600', sprintf('%s',mapname));

