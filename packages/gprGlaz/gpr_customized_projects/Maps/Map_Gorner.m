function Map_Gorner

close all

% ------------------ RST ------------------
pp = '/Volumes/ANJA_1GB/MScThesis/GPRData/'; %path to matlab files
matdata1 = '/Volumes/ANJA_1GB/MScThesis/GPRData/RST/matdata/'; %path to folder matdata
matdata2 = matdata1;
filedate1 = 'GORNER_20080225';
filedate2 = 'GORNER_20080226';

segyfolder1 = sprintf('%s%s_Segy/',matdata1,filedate1);
segyfolder2 = sprintf('%s%s_Segy/',matdata2,filedate2);


dd1 = dir([segyfolder1 sprintf('%s_mig_PROFILE_*.sgy',filedate1)]);
dd2 = dir([segyfolder2 sprintf('%s_mig_PROFILE_*.sgy',filedate2)]);


procp.dri = 1;


for aa = 1;
    if aa == 1; mapname = 'Gorner_Map'; end
    
    disp(mapname)
    
    mapfile = [pp sprintf('%s.mat',mapname)];
    load(mapfile);
    xmin = x1;
    xmax = x2;
    ymin = y1;
    ymax = y2;
    fig = figure(7); clf
    showmap(mapfile); hold on
  

    % ------------ FILEDATE 1 ------------
    for a = 1:length(dd1);
        segyname = sprintf('%s%s_mig_PROFILE_%03d',segyfolder1,filedate1,a);
        [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
        
        % Check if points are within Map
        if px(1) < xmin; continue; disp(sprintf('%s Profile %d not on Map',filedate1,a)); end
        if px(end) > xmax; continue; disp(sprintf('%s Profile %d not on Map',filedate1,a)); end
        if py(1) < ymin; continue; disp(sprintf('%s Profile %d not on Map',filedate1,a)); end
        if py(end) > ymax; continue; disp(sprintf('%s Profile %d not on Map',filedate1,a)); end
        
        L1 = plot(px/1000,py/1000,'k','LineWidth', 1);
        plot(px(1)/1000,py(1)/1000,'k.','markersize',10); % Start Point
        % Marker every 100 m
        clear iitic
        xx = (0:ntr-1)*procp.dri;
        ticm = 0:200:max(xx);
        for b = 1:length(ticm)
            [~,iitic(b)] = min(abs(xx-ticm(b))); end
        for bb = 1:length(iitic)
            plot(px(iitic(bb))/1000,py(iitic(bb))/1000,'b.','markersize',5); end
        % Name the profiles
        text(px(1)/1000,py(1)/1000,sprintf('%d',a),'color','k','fontsize',8,'fontweight','bold','HorizontalAlignment','right');
    end
    
    for a = 1:length(dd2);
        segyname = sprintf('%s%s_mig_PROFILE_%03d',segyfolder2,filedate2,a);
        [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
        
        % Check if points are within Map
        if px(1) < xmin; disp(sprintf('%s Profile %d not on Map',filedate2,a)); continue; end
        if px(end) > xmax; disp(sprintf('%s Profile %d not on Map',filedate2,a)); continue; end
        if py(1) < ymin; disp(sprintf('%s Profile %d not on Map',filedate2,a)); continue; end
        if py(end) > ymax; disp(sprintf('%s Profile %d not on Map',filedate2,a)); continue; end
        
        L2 = plot(px/1000,py/1000,'b','LineWidth', 1);
        plot(px(1)/1000,py(1)/1000,'b.','markersize',10); % Start Point
        % Marker every 100 m
        clear iitic
        xx = (0:ntr-1)*procp.dri;
        ticm = 0:200:max(xx);
        for b = 1:length(ticm)
            [~,iitic(b)] = min(abs(xx-ticm(b))); end
        for bb = 1:length(iitic)
            plot(px(iitic(bb))/1000,py(iitic(bb))/1000,'k.','markersize',5); end
        % Name the profiles
        text(px(1)/1000,py(1)/1000,sprintf('%d',a),'color','b','fontsize',8,'fontweight','bold','HorizontalAlignment','right');
    end

   

    title(sprintf('%s',mapname),'interpreter','none');
    xlabel('LK west-east [km]');
    ylabel('LK north-south [km]');
    set(gca,'dataaspectratio',[1 1 1]);
   
    hleg = legend([L1 L2],{filedate1, filedate2}); 
    set(hleg,'Interpreter','none','Location','SouthEast');
    clear L1 L2
    
    print('-djpeg99','-r600', sprintf('%s',mapname));

end