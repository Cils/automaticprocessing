function Map_GSSI2012

pp = '/Volumes/ANJA_1GB/MScThesis/GPRData/GSSI/'; %path to dewowed files

filedate1 = '20120417';
filedate2 = '20120322';
filedate3 = '20120315';

mapname = 'wallis_sued';
mapfile = [pp sprintf('%s.mat',mapname)];

segy1 = '/Volumes/ANJA_1GB/MScThesis/GPRData/GSSI/matdata/20120417_Segy/';
segy2 = '/Volumes/ANJA_1GB/MScThesis/GPRData/GSSI/matdata/20120322_Segy/';
segy3 = '/Volumes/ANJA_1GB/MScThesis/GPRData/GSSI/matdata/';

dd1 = dir([segy1 sprintf('%s_proc2_PROFILE_*.sgy',filedate1)]);
dd2 = dir([segy2 sprintf('%s_proc2_PROFILE_*.sgy',filedate2)]);
dd3 = dir([segy3 sprintf('%s_proc2_PROFILE_*.sgy',filedate3)]);



fig = figure(1); clf;
mapfile = [pp sprintf('%s.mat',mapname)];
showmap(mapfile); hold on


px1 = [];
py1 = [];
px2 = [];
py2 = [];
px3 = [];
py3 = [];

% for a = 1:length(dd1)
%     segyname = sprintf('%s%s_proc2_PROFILE_%03d',segy1,filedate1,a);
%     [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
%     px1 = [px1;px']; % write px from each profile into px1
%     py1 = [py1;py']; % write py from each profile into py1
% end
% save('dd1','px1','py1')
% 
% for a = 1:length(dd2)
%     segyname = [sprintf('%s%s_proc2_PROFILE_%03d',segy2,filedate2,a)];
%     [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
%     px2 = [px2;px']; % write px from each profile into px1
%     py2 = [py2;py']; % write py from each profile into py1
% end
% save('dd2','px2','py2')
% % 
% for a = 1:length(dd3)
%     segyname = [sprintf('%s%s_proc2_PROFILE_%03d',segy3,filedate3,a)];
%     [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
%     px3 = [px3;px']; % write px from each profile into px1
%     py3 = [py3;py']; % write py from each profile into py1
% end
% save('dd3','px3','py3')

load 'dd1.mat';
load 'dd2.mat';
load 'dd3.mat';

plot(px1/1000, py1/1000, 'k.','Markersize',5.7); hold on;
plot(px2/1000, py2/1000, 'r.','Markersize',5.7); hold on;
plot(px3/1000, py3/1000, 'g.','Markersize',5.7); hold off;

legend(sprintf('%s Data',filedate1),sprintf('%s Data',filedate2),sprintf('%s Data',filedate3),'Location','SouthEastOutside');
title(sprintf('Map %s / %s / %s',filedate1,filedate2,filedate3));
xlabel('LK west-east [km]');
ylabel('LK north-south [km]');
set(gca,'dataaspectratio',[1 1 1]);

print(fig,'-djpeg99','-r600', sprintf('Map_%s_%s_%s',filedate1,filedate2,filedate3));