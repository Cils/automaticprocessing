function Map_Fiesch
close all
clear all

pp = '/Volumes/ANJA_1GB/MScThesis/GPRData/';

% ------------------ BGR ------------------
matdata1 = '/Volumes/ANJA_1GB/MScThesis/GPRData/BGR/matdata/';
matdata4 = matdata1;

% ------------------ RST ------------------
matdata2 = '/Volumes/ANJA_1GB/MScThesis/GPRData/RST/matdata/';
matdata3 = matdata2;

filedate1 = 'FIESCH_20110321';
filedate2 = 'FIESCH_RST_20110321';
filedate3 = 'FIESCH_RST_20110322';
filedate4 = 'ALETSCH_1_20110322';

segyfolder1 = sprintf('%s%s_Segy/',matdata1,filedate1);
segyfolder2 = sprintf('%s%s_Segy/',matdata2,filedate2);
segyfolder3 = sprintf('%s%s_Segy/',matdata3,filedate3);
segyfolder4 = sprintf('%s%s_Segy/',matdata4,filedate4);



dd1 = dir([segyfolder1 sprintf('%s_mig_PROFILE_*.sgy',filedate1)]);
dd2 = dir([segyfolder2 sprintf('%s_mig_PROFILE_*.sgy',filedate2)]);
dd3 = dir([segyfolder3 sprintf('%s_mig_PROFILE_*.sgy',filedate3)]);
dd4 = dir([segyfolder4 sprintf('%s_mig_PROFILE_*.sgy',filedate4)]);


filedate = filedate3;
%analyseProf1 = [1 2 10 12 13 15 16 19 21 27 28 29 34 38 39 44 48]; % parallel to flow
%analyseProf2 = [1:5 9:11 13 14 16 18 23 28 29 30 34 36 40 46 47 52 54]; % parallel to flow
%analyseProf1 = [4 14 18 20 24 26 30 33 35 36 37 43 44 45:47] % perpendicular to flow
%analyseProf2 = [8 15 17 19 21 27 32 41 42 43 45 49:51 52 53] % perpendicular to flow
analyseProf1 = [];
analyseProf2 = [];
analyseProf3 = [28];
analyseProf4 = [];


procp.dri = 1;
year = '2011';

for aa = 1;
    if aa == 1; mapname = 'Fiesch_Map';  end

    mapfile = [pp sprintf('%s.mat',mapname)];
    fig = figure(7); clf
    showmap(mapfile); hold on
    load(mapfile);
    xmin = x1;
    xmax = x2;
    ymin = y1;
    ymax = y2;

    % ------------ FILEDATE 1 ------------
    for a = analyseProf1
    %for a = 1:length(dd1);
        segyname = sprintf('%s%s_mig_PROFILE_%03d',segyfolder1,filedate1,a);
        [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
        
        % Check if points are within Map
        if px(1) < xmin; continue; end
        if px(end) > xmax; continue; end
        if py(1) < ymin; continue; end
        if py(end) > ymax; continue; end
        
        L1 = plot(px/1000,py/1000,'k','LineWidth', 1);
        plot(px(1)/1000,py(1)/1000,'k.','markersize',10); % Start Point
        % Marker every 200 m
        clear iitic
        xx = (0:ntr-1)*procp.dri;
        ticm = 0:200:max(xx);
        for b = 1:length(ticm)
            [~,iitic(b)] = min(abs(xx-ticm(b))); end
        for bb = 1:length(iitic)
            plot(px(iitic(bb))/1000,py(iitic(bb))/1000,'b.','markersize',5); end
        %Name the profiles
        text(px(1)/1000,py(1)/1000,sprintf('%d',a),'color','k','fontsize',8,'fontweight','bold','HorizontalAlignment','right');
    end

    % ------------ FILEDATE 2 ------------
    for a = analyseProf2
    %for a = 1:length(dd2);
        segyname = sprintf('%s%s_mig_PROFILE_%03d',segyfolder2,filedate2,a);
        [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
                
        % Check if points are within Map
        if px(1) < xmin; continue; end
        if px(end) > xmax; continue; end
        if py(1) < ymin; continue; end
        if py(end) > ymax; continue; end
        
        L2 = plot(px/1000,py/1000,'b','LineWidth', 2);
        %L2 = plot(px/1000,py/1000,'b','LineWidth', 1);
        plot(px(1)/1000,py(1)/1000,'b.','markersize',10); % Start Point
        % Marker every 100 m
        clear iitic
        xx = (0:ntr-1)*procp.dri;
        ticm = 0:200:max(xx);
        for b = 1:length(ticm)
            [~,iitic(b)] = min(abs(xx-ticm(b))); end
        for bb = 1:length(iitic)
            plot(px(iitic(bb))/1000,py(iitic(bb))/1000,'k.','markersize',5); end
        % Name the profiles
        %text(px(1)/1000,py(1)/1000,sprintf('%d',a),'color','b','fontsize',8,'fontweight','bold','HorizontalAlignment','right');
    end
    
     % ------------ FILEDATE 3 ------------
    %for a = 1:length(dd3);
    for a = analyseProf3
        segyname = sprintf('%s%s_mig_PROFILE_%03d',segyfolder3,filedate3,a);
        [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
                
        % Check if points are within Map
        if px(1) < xmin; continue; end
        if px(end) > xmax; continue; end
        if py(1) < ymin; continue; end
        if py(end) > ymax; continue; end
        
        L3 = plot(px/1000,py/1000,'k','LineWidth', 1);
        plot(px(1)/1000,py(1)/1000,'k.','markersize',10); % Start Point
        % Marker every 200 m
        clear iitic
        xx = (0:ntr-1)*procp.dri;
        ticm = 0:200:max(xx);
        for b = 1:length(ticm)
            [~,iitic(b)] = min(abs(xx-ticm(b))); end
        for bb = 1:length(iitic)
            plot(px(iitic(bb))/1000,py(iitic(bb))/1000,'b.','markersize',5); end
        % Name the profiles
        %text(px(1)/1000,py(1)/1000,sprintf('%d',a),'color','k','fontsize',10,'fontweight','bold','HorizontalAlignment','right');
    end
    
     % ------------ FILEDATE 3 ------------
    %for a = 1:length(dd3);
    for a = analyseProf4
        segyname = sprintf('%s%s_mig_PROFILE_%03d',segyfolder4,filedate4,a);
        [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
                
        % Check if points are within Map
        if px(1) < xmin; continue; end
        if px(end) > xmax; continue; end
        if py(1) < ymin; continue; end
        if py(end) > ymax; continue; end
        
        L4 = plot(px/1000,py/1000,'r','LineWidth', 1);
        plot(px(1)/1000,py(1)/1000,'r.','markersize',10); % Start Point
        % Marker every 200 m
        clear iitic
        xx = (0:ntr-1)*procp.dri;
        ticm = 0:200:max(xx);
        for b = 1:length(ticm)
            [~,iitic(b)] = min(abs(xx-ticm(b))); end
        for bb = 1:length(iitic)
            plot(px(iitic(bb))/1000,py(iitic(bb))/1000,'k.','markersize',5); end
        % Name the profiles
        text(px(1)/1000,py(1)/1000,sprintf('%d',a),'color','r','fontsize',10,'fontweight','bold','HorizontalAlignment','right');
    end

    %title(sprintf('%s %s',mapname, year),'interpreter','none');
    %title(sprintf('%s %s',mapname, filedate),'interpreter','none');
    %title('Aletsch CompareSystem1','interpreter','none');
    title('Fiesch RST Repeatability','interpreter','none');

    xlabel('LK west-east [km]');
    ylabel('LK north-south [km]');
    set(gca,'dataaspectratio',[1 1 1]);
    
    if exist('L1') == 0; L1 = []; disp('No filedate1 in Legend'); end
    if exist('L2') == 0; L2 = []; disp('No filedate2 in Legend'); end
    if exist('L3') == 0; L3 = []; disp('No filedate3 in Legend'); end
    hleg = legend([L1 L2 L3 L4],{filedate1 filedate2 filedate3 filedate4});
    %hleg = legend([L1 L2],{filedate1, filedate2});


    set(hleg,'Interpreter','none','Location','SouthEast');
    clear L1 L2 L3
    
    print('-djpeg99','-r600', 'Aletsch CompareSystem1');
    %print('-djpeg99','-r600', sprintf('%s CompareSystem2',mapname));
    %print('-djpeg99','-r600', sprintf('%s %s',mapname,year));
    %print('-djpeg99','-r600', sprintf('%s %s',mapname,filedate));
end
end