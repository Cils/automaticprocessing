function makemap

pp = '/Volumes/ANJA_1GB/MScThesis/SwissMapTifFiles/'; %input map files (georef. tif files)
ppout = '/Volumes/ANJA_1GB/MScThesis/GPRData/';

%% -------------- ALLALIN GLETSCHER GROUND DATA -------------- 
% [a1,cm] = imread([pp 'komb1328.tif']);
% [a2,cm] = imread([pp 'komb1329.tif']);
% [a3,cm] = imread([pp 'komb1348.tif']);
% [a4,cm] = imread([pp 'komb1349.tif']);
% b = load([pp 'komb1328.tfw']);
% h = b(1);
% xo = b(5); % lower left x coordinate
% yo = b(6); % lower left y coordinate
% 
% a = [a1 a2;a3 a4]; % combine the maps
% 
% 
% % Define coordinates of the desired map
% x1 = 636000;
% x2 = 640000;
% y1 = 97000;
% y2 = 100500;
% ix1 = round((x1-xo)/h);
% ix2 = round((x2-xo)/h);
% iy1 = round((yo-y1)/h);
% iy2 = round((yo-y2)/h);
% mapdata = a(iy2:iy1,ix1:ix2);
% save([ppout '/Allalin_Map.mat'],'h','x1','x2','y1','y2','cm','mapdata');


%% -------------- SUEDWALLIS -------------- 
% [a1,cm] = imread([pp 'komb1307.tif']);
% [a2,cm] = imread([pp 'komb1308.tif']);
% [a3,cm] = imread([pp 'komb1327.tif']);
% [a4,cm] = imread([pp 'komb1328.tif']);
% [a5,cm] = imread([pp 'komb1347.tif']);
% [a6,cm] = imread([pp 'komb1348.tif']);
% b = load([pp 'komb1307.tfw']);
% 
% h = b(1);
% xo = b(5);
% yo = b(6);
% a = [a1 a2;a3 a4;a5 a6];
% 
% for aa = 3;
%     aa
%     if aa == 1;
%         % ------------ WallisSued_Map ------------
%         x1 = 607000;
%         x2 = 624000;
%         y1 = 91000;
%         y2 = 117000;
%         mapname = sprintf('%sWallisSued_Map',ppout)
%     end
%     
%     if aa == 2;
%         % ------------ WallisSued_Map_1 ------------
%         x1 = 607000;
%         x2 = 618000;
%         y1 = 91000;
%         y2 = 99000;
%         mapname = sprintf('%sWallisSued_Map_1',ppout)
%     end
%     
%     if aa == 3;
%         % ------------ WallisSued_Map_2 ------------
%         x1 = 610000;
%         x2 = 614500;
%         y1 = 100000;
%         y2 = 107000;
%         mapname = sprintf('%sWallisSued_Map_2',ppout)
%     end
%     
%     if aa == 4;
%         % ------------ WallisSued_Map_3 ------------
%         x1 = 617000;
%         x2 = 624000;
%         y1 = 106000;
%         y2 = 113000;
%         mapname = sprintf('%sWallisSued_Map_3',ppout)
%     end
%     
%     iy1 = round((yo-y1)/h);
%     iy2 = round((yo-y2)/h);
%     ix1 = round((x1-xo)/h);
%     ix2 = round((x2-xo)/h);
%     mapdata = a(iy2:iy1,ix1:ix2);
%     save(mapname,'h','x1','x2','y1','y2','cm','mapdata');
%     figure(1); clf
%     showmap(mapname)
% end

%% -------------- LOETSCHENTAL -------------- 
% [a1,cm] = imread([pp 'komb1248.tif']);
% [a2,cm] = imread([pp 'komb1249.tif']);
% [a3,cm] = imread([pp 'komb1268.tif']);
% [a4,cm] = imread([pp 'komb1269.tif']);
% b = load([pp 'komb1248.tfw']);
% 
% h = b(1);
% xo = b(5);
% yo = b(6);
% a = [a1 a2;a3 a4];
% 
% for aa = 1:4;
%     if aa == 1; 
%     % ------------ Loetschental_Map ------------
%     x1 = 625000;
%     x2 = 642000;
%     y1 = 137000;
%     y2 = 150000;
%     mapname = sprintf('%sLoetschental_Map',ppout);
%     end
% 
%     if aa == 2;
%     % ------------ Loetschental_Map_1 ------------
%     x1 = 625000;
%     x2 = 634000;
%     y1 = 142000;
%     y2 = 150000;
%     mapname = sprintf('%sLoetschental_Map_1',ppout);
%     end
%     
%     if aa == 3;
%     % ------------ Loetschental_Map_2 ------------
%     x1 = 635000;
%     x2 = 642000;
%     y1 = 143000;
%     y2 = 150000;
%     mapname = sprintf('%sLoetschental_Map_2',ppout);
%     end
% 
%     if aa == 4;
%     % ------------ Loetschental_Map_3 ------------
%     x1 = 631000;
%     x2 = 637000;
%     y1 = 137000;
%     y2 = 141000;
%     mapname = sprintf('%sLoetschental_Map_3',ppout);
%     end
% 
%     iy1 = round((yo-y1)/h);
%     iy2 = round((yo-y2)/h);
%     ix1 = round((x1-xo)/h);
%     ix2 = round((x2-xo)/h);
%     mapdata = a(iy2:iy1,ix1:ix2);
%     save(mapname,'h','x1','x2','y1','y2','cm','mapdata');
%     figure(1); clf
%     showmap(mapname)
% end


%% -------------- ALETSCH -------------- 
% [a1,cm] = imread([pp 'komb1248.tif']);
% [a2,cm] = imread([pp 'komb1249.tif']);
% [a3,cm] = imread([pp 'komb1268.tif']);
% [a4,cm] = imread([pp 'komb1269.tif']);
% b = load([pp 'komb1248.tfw']);
% 
% h = b(1);
% xo = b(5);
% yo = b(6);
% a = [a1 a2;a3 a4];
% 
% for aa = 2:5;
%     if aa == 1; 
%     % ------------ Aletsch_Map_Overwiew ------------
%     x1 = 634000;
%     x2 = 651000;
%     y1 = 138000;
%     y2 = 157500;
%     mapname = sprintf('%sAletsch_Map',ppout);
%     end
% 
%     if aa == 2;
%     % ------------ Aletsch_Map_1 ------------
%     x1 = 635000;
%     x2 = 644000;
%     y1 = 139000;
%     y2 = 147000;
%     mapname = sprintf('%sAletsch_Map_1',ppout);
%     end
%     
%     if aa == 3;
%     % ------------ Aletsch_Map_2 ------------
%     x1 = 643000;
%     x2 = 651000;
%     y1 = 139000;
%     y2 = 153000;
%     mapname = sprintf('%sAletsch_Map_2',ppout);
%     end
%     
%     if aa == 4;
%     % ------------ Aletsch_Map_3 ------------
%     x1 = 637000;
%     x2 = 649000;
%     y1 = 146000;
%     y2 = 153000;
%     mapname = sprintf('%sAletsch_Map_3',ppout);
%     end
%     
%     if aa == 5;
%     % ------------ Aletsch_Map_4 ------------
%     x1 = 641000;
%     x2 = 649000;
%     y1 = 148000;
%     y2 = 157000;
%     mapname = sprintf('%sAletsch_Map_4',ppout);
%     end
% 
%     iy1 = round((yo-y1)/h);
%     iy2 = round((yo-y2)/h);
%     ix1 = round((x1-xo)/h);
%     ix2 = round((x2-xo)/h);
%     mapdata = a(iy2:iy1,ix1:ix2);
%     save(mapname,'h','x1','x2','y1','y2','cm','mapdata');
%     figure(1); clf
%     showmap(mapname)
%     pause;
% end


%% -------------- FIESCH -------------- 
% [a1,cm] = imread([pp 'komb1249.tif']);
% [a2,cm] = imread([pp 'komb1250.tif']);
% [a3,cm] = imread([pp 'komb1269.tif']);
% [a4,cm] = imread([pp 'komb1270.tif']);
% b = load([pp 'komb1249.tfw']);
% 
% h = b(1);
% xo = b(5);
% yo = b(6);
% a = [a1 a2;a3 a4];
% 
% for aa = 1;
%     if aa == 1; 
%     % ------------ Fiesch_Map_Overwiew ------------
%     x1 = 643000;
%     x2 = 658000;
%     y1 = 138000;
%     y2 = 156000;
%     mapname = sprintf('%sFiesch_Map',ppout);
%     end
%     
%     iy1 = round((yo-y1)/h);
%     iy2 = round((yo-y2)/h);
%     ix1 = round((x1-xo)/h);
%     ix2 = round((x2-xo)/h);
%     mapdata = a(iy2:iy1,ix1:ix2);
%     save(mapname,'h','x1','x2','y1','y2','cm','mapdata');
%     figure(1); clf
%     showmap(mapname)
%     pause;
% end


%% -------------- MAUVOISIN -------------- 
% [a1,cm] = imread([pp 'komb1346.tif']);
% [a2,cm] = imread([pp 'komb1347.tif']);
% [a3,cm] = imread([pp 'komb1366.tif']);
% [a4,cm] = imread([pp 'komb1368.tif']);
% b = load([pp 'komb1346.tfw']);
% h = b(1);
% xo = b(5);
% yo = b(6);
% a = [a1 a2;a3 a4];
% 
% 
% for aa = 4;
%     
%     if aa == 1;
%     % ------------ Mauvoisin_Map ------------
%     x1 = 585100;
%     x2 = 605000;
%     y1 = 85000;
%     y2 = 95500;
%     mapname = sprintf('%sMauvoisin_Map',ppout);
%     end
%     
%     if aa == 2;
%     % ------------ Mauvoisin_Map_1 ------------
%     x1 = 585100;
%     x2 = 594000;
%     y1 = 85000;
%     y2 = 95500;
%     mapname = sprintf('%sMauvoisin_Map_1',ppout);
%     end
%     
%     if aa == 3;
%     % ------------ Mauvoisin_Map_2 ------------
%     x1 = 594000;
%     x2 = 605000;
%     y1 = 86000;
%     y2 = 95500;
%     mapname = sprintf('%sMauvoisin_Map_2',ppout);
%     end
%     
%     if aa == 4;
%     % ------------ Mauvoisin_Map_3 ------------
%     x1 = 587000;
%     x2 = 591000;
%     y1 = 92000;
%     y2 = 95500;
%     mapname = sprintf('%sMauvoisin_Map_3',ppout);
%     end
%     
%     iy1 = round((yo-y1)/h);
%     iy2 = round((yo-y2)/h);
%     ix1 = round((x1-xo)/h);
%     ix2 = round((x2-xo)/h);
%     mapdata = a(iy2:iy1,ix1:ix2);
%     save(mapname,'h','x1','x2','y1','y2','cm','mapdata');
%     figure(1); clf
%     showmap(mapname)
% end

%% -------------- TRIENT -------------- 
% [a1,cm] = imread([pp 'komb1344.tif']);
% [a2,cm] = imread([pp 'komb1345.tif']);
% [a4,cm] = imread([pp 'komb1365.tif']);
% a3 = zeros(size(a4));
% b = load([pp 'komb1344.tfw']);
% h = b(1);
% xo = b(5);
% yo = b(6);
% 
% a = [a1 a2;a3 a4];
% 
% 
% for aa = 1;
%     
%     if aa == 1;
%     % ------------ Trient_Map ------------
%     x1 = 566000;
%     x2 = 574000;
%     y1 = 85000;
%     y2 = 97000;
%     mapname = sprintf('%sTrient_Map',ppout);
%     end
%     
%     iy1 = round((yo-y1)/h);
%     iy2 = round((yo-y2)/h);
%     ix1 = round((x1-xo)/h);
%     ix2 = round((x2-xo)/h);
%     mapdata = a(iy2:iy1,ix1:ix2);
%     save(mapname,'h','x1','x2','y1','y2','cm','mapdata');
%     figure(1); clf
%     showmap(mapname)
% end


%% -------------- GRONER -------------- 
% [a1,cm] = imread([pp 'komb1348.tif']);
% b = load([pp 'komb1348.tfw']);
% h = b(1);
% xo = b(5);
% yo = b(6);
% 
% a = [a1];
% 
% 
% for aa = 1;
%     
%     if aa == 1;
%     % ------------ Gorner_Map ------------
%     x1 = 621000;
%     x2 = 637000;
%     y1 = 87000;
%     y2 = 97000;
%     mapname = sprintf('%sGorner_Map',ppout);
%     end
%     
%     iy1 = round((yo-y1)/h);
%     iy2 = round((yo-y2)/h);
%     ix1 = round((x1-xo)/h);
%     ix2 = round((x2-xo)/h);
%     mapdata = a(iy2:iy1,ix1:ix2);
%     save(mapname,'h','x1','x2','y1','y2','cm','mapdata');
%     figure(1); clf
%     showmap(mapname)
% end


%% MOIRYs
% [a,cm] = imread([pp 'komb1327.tif']);
% b = load([pp 'komb1327.tfw']);
% h = b(1);
% xo = b(5);
% yo = b(6);
% x1 = 610000;
% x2 = 613000;
% y1 = 103000;
% y2 = 106000;
% iy1 = round((yo-y1)/h);
% iy2 = round((yo-y2)/h);
% ix1 = round((x1-xo)/h);
% ix2 = round((x2-xo)/h);
% mapdata = a(iy2:iy1,ix1:ix2);
% save([pp 'moiry_map'],'h','x1','x2','y1','y2','cm','mapdata');


%% Combi1 - L�tsch/Aletsch/Fiesch
[a1,cm] = imread([pp 'komb1248.tif']);
[a2,cm] = imread([pp 'komb1249.tif']);
[a3,cm] = imread([pp 'komb1250.tif']);
[a4,cm] = imread([pp 'komb1268.tif']);
[a5,cm] = imread([pp 'komb1269.tif']);
[a6,cm] = imread([pp 'komb1270.tif']);

b = load([pp 'komb1248.tfw']); %load upper left coordinate
h = b(1);
xo = b(5); % upper left x coordinate
yo = b(6); % upper left y coordinate

a = [a1 a2 a3; a4 a5 a6]; % combine the maps


% Define coordinates of the desired map
x1 = 624500;
x2 = 658000;
y1 = 137000;
y2 = 157800;
ix1 = round((x1-xo)/h);
ix2 = round((x2-xo)/h);

iy1 = round((yo-y1)/h);
iy2 = round((yo-y2)/h);
mapdata = a(iy2:iy1,ix1:ix2);
save([ppout '/Combi1_Map.mat'],'h','x1','x2','y1','y2','cm','mapdata');
figure(1); clf
showmap('Combi1_Map.mat')


