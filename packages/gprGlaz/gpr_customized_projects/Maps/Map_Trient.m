function Map_Trient

% ------------------ GSSI ------------------
pp = '/Volumes/ANJA_1GB/MScThesis/GPRData/'; %path to matlab files
matdata1 = '/Volumes/ANJA_1GB/MScThesis/GPRData/GSSI/matdata/'; %path to folder matdata
filedate1 = 'TRIENT_20130222';
segyfolder1 = sprintf('%s%s_Segy/',matdata1,filedate1);

dd1 = dir([segyfolder1 sprintf('%s_mig_PROFILE_*.sgy',filedate1)]);

procp.dri = 0.5;


for aa = 1;
    if aa == 1; mapname = 'Trient_Map'; end
    
    disp(mapname)
    
    mapfile = [pp sprintf('%s.mat',mapname)];
    fig = figure(7); clf
    showmap(mapfile); hold on


    % ------------ FILEDATE 1 ------------
    for a = 1:length(dd1);
        segyname = sprintf('%s%s_mig_PROFILE_%03d',segyfolder1,filedate1,a);
        [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
        L1 = plot(px/1000,py/1000,'k','LineWidth', 1);
        plot(px(1)/1000,py(1)/1000,'k.','markersize',10); % Start Point
        % Marker every 100 m
        clear iitic
        xx = (0:ntr-1)*procp.dri;
        ticm = 0:200:max(xx);
        for b = 1:length(ticm)
            [~,iitic(b)] = min(abs(xx-ticm(b))); end
        for bb = 1:length(iitic)
            plot(px(iitic(bb))/1000,py(iitic(bb))/1000,'b.','markersize',5); end
        % Name the profiles
        text(px(1)/1000,py(1)/1000,sprintf('%d',a),'color','k','fontsize',8,'fontweight','bold','HorizontalAlignment','right');
    end

   

    title(sprintf('%s',mapname),'interpreter','none');
    xlabel('LK west-east [km]');
    ylabel('LK north-south [km]');
    set(gca,'dataaspectratio',[1 1 1]);
   
    hleg = legend([L1],{filedate1});
%     hleg = legend([L2],{filedate2});
% 
    set(hleg,'Interpreter','none','Location','NorthEast');
    clear L1 L2
    
    print('-djpeg99','-r600', sprintf('%s',mapname));

end