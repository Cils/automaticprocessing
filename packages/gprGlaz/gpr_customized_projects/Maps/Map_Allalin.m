function Map_Allalin

close all

pp = 'D:\ranja\MScThesis\GPRData\'; %path to matlab files

% ------------------ GSSI ------------------
matdata1 = 'D:\ranja\DataBase\GPRDataBase\SegyFiles\GSSI\';
matdata2 = matdata1;
% ------------------ PULSE EKKO GROUND DATA ------------------
matdata3 = 'D:\ranja\DataBase\GPRDataBase\SegyFiles\PulseEKKO\'
matdata4 = matdata3;


filedate1 = 'ALLALIN_20130209';
filedate2 = 'ALLALIN_20130222';
filedate3 = 'ALLALIN_20130210';
filedate4 = 'ALLALIN_20130507';



segyfolder1 = sprintf('%s%s_Segy/',matdata1,filedate1);
segyfolder2 = sprintf('%s%s_Segy/',matdata2,filedate2);
segyfolder3 = sprintf('%s%s_Segy/',matdata3,filedate3);
segyfolder4 = sprintf('%s%s_Segy/',matdata4,filedate4);


dd1 = dir([segyfolder1 sprintf('%s_mig_PROFILE_*.sgy',filedate1)]);
dd2 = dir([segyfolder2 sprintf('%s_mig_PROFILE_*.sgy',filedate2)]);
dd3 = dir([segyfolder3 sprintf('%s_mig_PROFILE_*.sgy',filedate3)]);
dd4 = dir([segyfolder4 sprintf('%s_mig_PROFILE_*.sgy',filedate4)]);


procp.dri = 0.5;


for aa = 1;
    if aa == 1; mapname = 'Allalin_Map'; end
    if aa == 2; mapname = 'AllalinGround_Map'; end
    
    disp(mapname)
    
    mapfile = [pp sprintf('%s.mat',mapname)];
    load(mapfile);
    xmin = x1;
    xmax = x2;
    ymin = y1;
    ymax = y2;
    fig = figure(7); clf
    showmap(mapfile); hold on
  

    % ------------ FILEDATE 1 ------------
    for a = 1:length(dd1);
        segyname = sprintf('%s%s_mig_PROFILE_%03d',segyfolder1,filedate1,a);
        [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
        
        % Check if points are within Map
        if px(1) < xmin;  continue; end
        if px(end) > xmax; continue; end
        if py(1) < ymin; continue; end
        if py(end) > ymax; continue; end
        
        L1 = plot(px/1000,py/1000,'k','LineWidth', 1);
        plot(px(1)/1000,py(1)/1000,'k.','markersize',10); % Start Point
        % Marker every 200 m
        clear iitic
        xx = (0:ntr-1)*procp.dri;
        ticm = 0:200:max(xx);
        for b = 1:length(ticm)
            [~,iitic(b)] = min(abs(xx-ticm(b))); end
        for bb = 1:length(iitic)
            plot(px(iitic(bb))/1000,py(iitic(bb))/1000,'b.','markersize',5); end
        % Name the profiles
        text(px(1)/1000,py(1)/1000,sprintf('%d',a),'color','k','fontsize',8,'fontweight','bold','HorizontalAlignment','right');
    end
    
    for a = 1:length(dd2);
        segyname = sprintf('%s%s_mig_PROFILE_%03d',segyfolder2,filedate2,a);
        [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
        
        % Check if points are within Map
        if px(1) < xmin;  continue; end
        if px(end) > xmax; continue; end
        if py(1) < ymin; continue; end
        if py(end) > ymax; continue; end
        
        L2 = plot(px/1000,py/1000,'b','LineWidth', 1);
        plot(px(1)/1000,py(1)/1000,'b.','markersize',10); % Start Point
        % Marker every 200 m
        clear iitic
        xx = (0:ntr-1)*procp.dri;
        ticm = 0:200:max(xx);
        for b = 1:length(ticm)
            [~,iitic(b)] = min(abs(xx-ticm(b))); end
        for bb = 1:length(iitic)
            plot(px(iitic(bb))/1000,py(iitic(bb))/1000,'k.','markersize',5); end
        % Name the profiles
        text(px(1)/1000,py(1)/1000,sprintf('%d',a),'color','b','fontsize',8,'fontweight','bold','HorizontalAlignment','right');
    end

   for a = 1:length(dd3);
        segyname = sprintf('%s%s_mig_PROFILE_%03d',segyfolder3,filedate3,a);
        [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
        
        % Check if points are within Map
        if px(1) < xmin;  continue; end
        if px(end) > xmax; continue; end
        if py(1) < ymin; continue; end
        if py(end) > ymax; continue; end
        
        L3 = plot(px/1000,py/1000,'g','LineWidth', 1);
        plot(px(1)/1000,py(1)/1000,'g.','markersize',10); % Start Point
        % Marker every 200 m
        clear iitic
        xx = (0:ntr-1)*procp.dri;
        ticm = 0:200:max(xx);
        for b = 1:length(ticm)
            [~,iitic(b)] = min(abs(xx-ticm(b))); end
        for bb = 1:length(iitic)
            plot(px(iitic(bb))/1000,py(iitic(bb))/1000,'k.','markersize',5); end
        % Name the profiles
        text(px(1)/1000,py(1)/1000,sprintf('%d',a),'color','g','fontsize',8,'fontweight','bold','HorizontalAlignment','right');
   end

   for a = 1:length(dd4);
        segyname = sprintf('%s%s_mig_PROFILE_%03d',segyfolder4,filedate4,a);
        [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
        
        % Check if points are within Map
        if px(1) < xmin;  continue; end
        if px(end) > xmax; continue; end
        if py(1) < ymin; continue; end
        if py(end) > ymax; continue; end
        
        L4 = plot(px/1000,py/1000,'r','LineWidth', 1);
        plot(px(1)/1000,py(1)/1000,'r.','markersize',10); % Start Point
        % Marker every 200 m
        clear iitic
        xx = (0:ntr-1)*procp.dri;
        ticm = 0:200:max(xx);
        for b = 1:length(ticm)
            [~,iitic(b)] = min(abs(xx-ticm(b))); end
        for bb = 1:length(iitic)
            plot(px(iitic(bb))/1000,py(iitic(bb))/1000,'k.','markersize',5); end
        % Name the profiles
        text(px(1)/1000,py(1)/1000,sprintf('%d',a),'color','r','fontsize',8,'fontweight','bold','HorizontalAlignment','right');
    end
 
   
    title(sprintf('%s',mapname),'interpreter','none');
    xlabel('LK west-east [km]');
    ylabel('LK north-south [km]');
    set(gca,'dataaspectratio',[1 1 1]);
    
    if exist('L1') == 0; L1 = []; disp('No filedate1 in Legend'); end
    if exist('L2') == 0; L2 = []; disp('No filedate2 in Legend'); end
    if exist('L3') == 0; L3 = []; disp('No filedate3 in Legend'); end
    if exist('L4') == 0; L4 = []; disp('No filedate4 in Legend'); end


    hleg = legend([L1 L2 L3 L4],{filedate1, filedate2, filedate3, filedate4}); 
    set(hleg,'Interpreter','none','Location','NorthEast');
    clear L1 L2 L3 L4
    
    print('-djpeg99','-r600', sprintf('%s',mapname));

end