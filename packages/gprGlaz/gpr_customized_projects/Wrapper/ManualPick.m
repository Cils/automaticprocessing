% x2t2_upFMCW
% calculate RMS and interval velocites

% {
clear all;
%year='2010-2011';
%year='2011-2012';
year='2012-2013';
%year='2013-2014';

%% load and merge data

load(['..\data\' year '\WFJ_Radar22']);
load(['..\data\' year '\WFJ_TA']);
load(['..\data\' year '\WFJ_TSS']);
%}

if exist(['..\data\' year '\WFJ_WettingfrontManualPicked.mat'],'file')
    load(['..\data\' year '\WFJ_WettingfrontManualPicked']);
else
    WettingfrontManualPicked.TWT=[];
end

WettingfrontManualPicked.dates = Radar.dates;

%% Parameters
vMean=0.23;                             % estimated mean velocity

%% pick layers and calculate RMS velocity
close all;
WettingfrontManualPicked.TWT = manualPick_PickReflector(Radar, TA, TSS, WettingfrontManualPicked.TWT) / 1e9;
WettingfrontManualPicked.HS = WettingfrontManualPicked.TWT / 2 * vMean * 1e9;

%save(['..\data\' year '\WFJ_WettingfrontManualPicked'],'WettingfrontManualPicked');

% calc LWC
load(['..\data\' year '\WFJ_LaserCorrected']);
laserResampled = resampleVect(Radar.dates,Laser.dates,Laser.values,1); 
v=laserResampled*2./Radar.TWT*1e9;
eps=v2eps(v);
lwc=epsDens2lwc(eps,370)*100;
lwc(lwc<0)=0;

% write csv 
datafile = fopen(['C:\Users\schmidl\Desktop\zum l�schen\temp\wettingFront\Wetting_' year '.csv'],'w+');
if datafile == -1
    error('Error opening data file!');
end
header = 'Date (dd.mm.yyyy HH:MM:SS), Datenum (days since 00.January.0000 00:00:00), Snow Height (m), Height Wettingfront (m), Bulk LWC (%)';
fprintf(datafile, '%s', header ); % header
fprintf(datafile, '\n'); 
for i = 1:length(Radar.dates)
    fprintf(datafile, '%s, ', datestr(Radar.dates(i),'dd.mm.yyyy HH:MM:SS') ); % current date (string)
    fprintf(datafile, '%6.5f, ', Radar.dates(i) );   % current date (num)
    fprintf(datafile, '%1.3f, ', laserResampled(i));           % snow height (m)
    fprintf(datafile, '%1.3f, ', WettingfrontManualPicked.HS(i));           % height Wettingfront (m)
    fprintf(datafile, '%2.2f', lwc(i));           % lwc (%)
    
    fprintf(datafile, '\n'); 
end
fclose(datafile);

%%
figure(10);
if  strcmp(year,'2010-2011')
	xLim=datenum(['01.10.2010';'15.06.2011'],'dd.mm.yyyy');
elseif strcmp(year,'2011-2012')
    xLim=datenum(['01.10.2011';'01.07.2012'],'dd.mm.yyyy');
elseif strcmp(year,'2012-2013')
    xLim=datenum(['01.10.2012';'01.07.2013'],'dd.mm.yyyy');
elseif strcmp(year,'2013-2014')
    xLim=datenum(['01.10.2013';'01.07.2014'],'dd.mm.yyyy');
end

subplot(2,1,1)
plot(Laser.dates, Laser.values, 'r', WettingfrontManualPicked.dates, WettingfrontManualPicked.HS, 'k')
legend('Snow height','Wettingfront')
ylabel('Height (m)')
xlabel('Date')
xlim(xLim)
datetick('x','dd.mm.yyyy','keeplimits')
subplot(2,1,2)
plot(Radar.dates, lwc)
ylabel('LWC (%)')
xlabel('Date')
xlim(xLim)
ylim([0,10])
datetick('x','dd.mm.yyyy','keeplimits')


figure(11);
%TODO vergleich mit nander
subplot(2,1,1)
plot(Laser.dates, Laser.values, 'r', WettingfrontManualPicked.dates, WettingfrontManualPicked.HS, 'k')
legend('Snow height','Wettingfront')
ylabel('Height (m)')
xlabel('Date')
xlim(xLim)
datetick('x','dd.mm.yyyy','keeplimits')
subplot(2,1,2)
plot(Radar.dates, lwc)
ylabel('LWC (%)')
xlabel('Date')
xlim(xLim)
ylim([0,10])
datetick('x','dd.mm.yyyy','keeplimits')