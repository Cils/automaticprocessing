
%% load

clear all;
%{
%import from Nander
SnowpackBucket.dates=SnowpackBucketMatr(:,1);
SnowpackBucket.HSRadar=SnowpackBucketMatr(:,2)/100;
SnowpackBucket.HSWettingfrontRadar=SnowpackBucketMatr(:,3)/100;
SnowpackBucket.LWCRadar=SnowpackBucketMatr(:,4);
SnowpackBucket.Lysimeter=SnowpackBucketMatr(:,5);
SnowpackBucket.HS=SnowpackBucketMatr(:,6)/100;
SnowpackBucket.HSWettingfrontSnowpack=SnowpackBucketMatr(:,7)/100;
SnowpackBucket.LWCSnowpack=SnowpackBucketMatr(:,8);

SnowpackRichard.dates=SnowpackRichardMatr(:,1);
SnowpackRichard.HSRadar=SnowpackRichardMatr(:,2)/100;
SnowpackRichard.HSWettingfrontRadar=SnowpackRichardMatr(:,3)/100;
SnowpackRichard.LWCRadar=SnowpackRichardMatr(:,4);
SnowpackRichard.Lysimeter=SnowpackRichardMatr(:,5);
SnowpackRichard.HS=SnowpackRichardMatr(:,6)/100;
SnowpackRichard.HSWettingfrontSnowpack=SnowpackRichardMatr(:,7)/100;
SnowpackRichard.LWCSnowpack=SnowpackRichardMatr(:,8);

save '..\data\2013-2014\WFJ_SnowpackRichard' 'SnowpackRichard'
save '..\data\2013-2014\WFJ_SnowpackBucket' 'SnowpackBucket'
%}

load('..\data\2013-2014\WFJ_Radar22');
load('..\data\2013-2014\WFJ_WettingfrontManualPicked')
load('..\data\2013-2014\WFJ_LaserCorrectedResampledInterpolated');
load('..\data\2013-2014\WFJ_Lysi');
load('..\data\2013-2014\WFJ_SnowpackRichard');
load('..\data\2013-2014\WFJ_SnowpackBucket');

%% compare lwc snowpack and lwc radar
figure(1)
clf
subplot(2,1,1)
plot(SnowpackBucket.dates,SnowpackBucket.LWCRadar,'g',SnowpackBucket.dates,SnowpackBucket.LWCSnowpack,'b')
legend('LWC Radar','LWC Snowpack')
title('Bucket')
datetick('x','dd.mm.yyyy','keeplimits')

subplot(2,1,2)
plot(SnowpackRichard.dates,SnowpackRichard.LWCRadar,'g',SnowpackRichard.dates,SnowpackRichard.LWCSnowpack,'b')
legend('LWC Radar','LWC Snowpack')
title('Richard equations')
datetick('x','dd.mm.yyyy','keeplimits')


%% compare wettingfront snowpack and lwc radar
load('..\data\general\colRamp_BlueYellow')
path('..\radargramm zeichnen',path); 

climpIntens=[-300,300];
vMean=0.23;
from = datestr(Radar.dates(1),'dd.mm.yyyy HH:MM:SS');
to = datestr(Radar.dates(end),'dd.mm.yyyy HH:MM:SS');


hfig2=figure(2);
set(hfig2,'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
clf;
subplot(4,1,1:3)
% plot radargram on the left side
[ax, h1, h2] = plotRadargram(from, to, 1/24/2, 24,...
    Radar.radargram, Radar.dates, Radar.samples, 'e', colRamp, climpIntens,...
    Radar.dates, Radar.HS,'g','HS Radar',...
    Laser.dates, Laser.values, 'r','HS Laser',...
    WettingfrontManualPicked.dates, WettingfrontManualPicked.HS, 'k','Wettingfront Radar',...
    SnowpackBucket.dates, SnowpackBucket.HSWettingfrontSnowpack, 'b','Wettingfront Bucket',...
    SnowpackRichard.dates, SnowpackRichard.HSWettingfrontSnowpack, 'c','Wettingfront Richard');%,...
xLim=xlim;

subplot(4,1,4)
plot(Lysi.dates,Lysi.dischargeRateAvg)
ylabel('Water discharge L h^{-1} m^{-2}')
xlabel('Date')
xlim(xLim)
ylim([0 5])
datetick('x','dd.mmm.yyyy','keeplimits')