function mouseMove (object, eventdata, plotMatrix, dates, samples, pick, twt, TA, TSS, gca11, gca12, ax1, fig2, gca2)
    % if already executing mouseMove, just return
    global mouseMoveAlreadyExecuting;
    if mouseMoveAlreadyExecuting
        return
    else
        mouseMoveAlreadyExecuting=true;
    end

    C = get (gca11, 'CurrentPoint');
    if iscell(C)
        x = C{1,1}(1,1);
        y = C{1,1}(1,2);
    else
        x = C(1,1);
        y = C(1,2);
    end
    title(gca11, [datestr(x,'dd.mmm HH:MM'), ': TWT = ',num2str(y)]);

    [diff,Ix,value]=findNearest(dates, x);
    [diff,Iy,value]=findNearest(samples, y);

    figure(fig2)
    hold off
    plot(gca2, plotMatrix(:,Ix), samples);
    hold on
    %plot(gca2, [0,0], samples(1,end),'k');
    plot(gca2, xlim, [1,1] * samples(Iy),'r');
    plot(gca2, xlim, [1,1] * twt(Ix),'g');
    plot(gca2, xlim, [1,1] * pick(Ix),'k');
    ylim(gca2,[0,30]);
    title(gca2, [datestr(x,'dd.mmm HH:MM'), ': TWT = ',num2str(y)]);
    %abs(FMCW.ant1.radargram)
    mouseMoveAlreadyExecuting=false;
    
    
    %figure1 unten
    axisOben=axis(ax1);
    plot(gca12,TA.dates,TA.values,'b',TSS.dates,TSS.values,'c', axisOben(1:2) ,[0 0],'k',[x x],ylim(gca12),'r');
    xlim(gca12,axisOben(1:2));
    %datetick(a(1),'x','dd.mm.yyyy','keeplimits')
end

%}