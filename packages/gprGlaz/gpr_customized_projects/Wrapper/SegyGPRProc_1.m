function [nsamp,data] = SegyGPRProc_1(procp,ntr,sra,data)


% -------------------------------------------------------------------------
% ------------------- REDUCE TIME WINDOW TO TWIN --------------------------
% -------------------------------------------------------------------------



% -------------------------------------------------------------------------
% ------------------- PLOT RAW DATA ---------------------------------------
% -------------------------------------------------------------------------
t = (0:nsamp-1)*sra;
for a = 1:ntr
    data(:,a) = data(:,a) - median(data(:,a));
end;

fig = figure(1); clf;
imagesc(1:ntr,t,data); 
        colormap(procp.cmap);
        xlabel('Trace number','fontsize',12);
        ylabel('Time [ns]','fontsize',12);
        set(gca,'fontsize',12);
        title('Raw Data')

        
% -------------------------------------------------------------------------
% ------------------- BACKGROUND REMOVAL ----------------------------------
% -------------------------------------------------------------------------
% sumtr = median(data,2);
% for b = 1:ntr
%     data(:,b) = data(:,b) - sumtr; 
%     data(:,b) = data(:,b) - median(data(:,b));
% end;
% 
% fig = figure(2); clf;
% imagesc(1:ntr,t,data);
%         colormap(procp.cmap);
%         xlabel('Trace number','fontsize',12);
%         ylabel('Time [ns]','fontsize',12);
%         set(gca,'fontsize',12);
%         title('After Background Removal')


% -------------------------------------------------------------------------
% ------------------- SVD FILTER ------------------------------------------
% -------------------------------------------------------------------------
if (procp.svd_len > 0)
   data = msvdfilt(data,procp.svd_len);
end;

fig = figure(3); clf;
      imagesc(1:ntr,t,data);
      colormap(procp.cmap);
      xlabel('Trace number','fontsize',12);
      ylabel('Time [ns]','fontsize',12);
      set(gca,'fontsize',12);
      title('After SVD Filter');
    
            
% -------------------------------------------------------------------------
% ------------------- PLOT FREQUENCY SPECTRA ------------------------------
% -------------------------------------------------------------------------
fdata = sum(abs(fft(data))');
f = linspace(0,1/(2*sra),nsamp/2) * 1000;
nf = length(f);

fig = figure(4); clf;
      plot(f,fdata(1:nf),'b');
      hold on;
      xlabel('Frequency [MHz]','fontsize',12);
      ylabel('Amplitude','fontsize',12);
      title('Amplitude Spectrum','fontsize',12);
        
%       % ---------- NORMALIZED LOG PLOT ----------
%       MaxAmp = max(fdata);
%       for a = 1:length(fdata);
%           normfdata(a) = fdata(a)/MaxAmp;
%       end
%       fig = figure(5);
%             semilogy(f,normfdata(1:nf),'k');
%             xlabel('Frequency [MHz]','fontsize',12);
%             ylabel('Normalized Log Amplitude','fontsize',12);
%             title('Normalized Amplitude Spectrum','fontsize',12);
            

% -------------------------------------------------------------------------
% ------------------- BUTTERWORTH BANDPASS FILTER -------------------------
% -------------------------------------------------------------------------
%-------------- BUTTERWORTH BANDPASS FILTER --------------
if (procp.fl & procp.fh > 0)
    nyq = 0.5 / sra * 1000;
    fl = procp.fl/nyq;
    fh = procp.fh/nyq;
    [bb,aa] = butter(procp.ford,[fl fh]);
    data = filter(bb,aa,data);
    fdata = sum(abs(fft(data))');

    plot(f,fdata(1:nf),'r');
    legend('Before Butterworth Bandpass Filter','After Butterworth Bandpass Filter');
    hold off

fig = figure(6); clf;
      imagesc(1:ntr,t,data);
      colormap(procp.cmap);
      xlabel('Trace number','fontsize',12);
      ylabel('Time [ns]','fontsize',12);
      set(gca,'fontsize',12);
      title('After Butterworth Bandpass Filter');
end     

