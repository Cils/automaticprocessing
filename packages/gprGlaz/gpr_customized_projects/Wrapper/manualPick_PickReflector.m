%{
% example of usage 

clear all;
load('..\data\2011-2012\WFJ_Radar22');
load('..\data\2011-2012\WFJ_TA');
load('..\data\2011-2012\WFJ_TSS');
Radar.samples=0:Radar.samples(end)/(length(Radar.samples)-1):Radar.samples(end);
%}

function WettingFrontTwt = manualPick_PickReflector(Radar, TA, TSS, WettingFrontTwt)

load('..\data\general\colRamp_BlueYellow')
path('..\radargramm zeichnen',path); 

WettingFrontTwt=WettingFrontTwt*1e9;
if length(Radar.dates)~=length(WettingFrontTwt)
    WettingFrontTwt=[WettingFrontTwt ones(length(Radar.dates)-length(WettingFrontTwt),1)*NaN];
end

%load('R:\postprocessing-scripts\Matlab\data\2012-2013\WFJ_WettingfrontManualPicked.mat')
%WettingFrontTwt=WettingfrontManualPicked.TWT*1e9;

figure(1); clf;

climpIntens=[-250,250];
vMean=0.23;

from = datestr(Radar.dates(1),'dd.mm.yyyy HH:MM:SS');
to = datestr(Radar.dates(end),'dd.mm.yyyy HH:MM:SS');

% reset plot parameters to first klick
lastKlick = [0 0];

% as long as user wants to set another pick
fprintf('For help press h\n')   
while(1)
    hfig1=figure(1);
    set(hfig1,'Units', 'Normalized', 'OuterPosition', [0 0 .7 1]);
    % buffer zoom and pan
    if exist('ax','var')
        axis1_vor=axis(ax(1));
        axis2_vor=axis(ax(2));
    end
    clf;
    subplot(4,1,1:3)
    % plot radargram on the left side
    
    [ax, h1, h2] = plotRadargram(from, to, 1/24/2, 24,...
        Radar.radargram, Radar.dates, Radar.samples, 'd', colRamp, climpIntens,...
        Radar.dates, Radar.HS,'g','HS',...
        Radar.dates, WettingFrontTwt * vMean / 2,'k','Pick',...
        lastKlick(1),lastKlick(2),'mo','lastKlick');%,...
    % reset pam and zoom to the value buffered bevore
    if exist('axis1_vor','var')
        axis(ax(1),axis1_vor);
        axis(ax(2),axis2_vor);
    end
    gcf1 = gcf();
    gca11 = gca();
    
    subplot(4,1,4)
    axisOben=axis(ax(1));
    plot(TA.dates,TA.values,'b',TSS.dates,TSS.values,'c', axisOben(1:2) ,[0 0],'k')
    gca12 = gca();
    xlim(gca12,axisOben(1:2));

    % plot radargram on the left side
    hfig2=figure(2);
    set(hfig2,'Units', 'Normalized', 'OuterPosition', [.7 0 .3 1]);
    gca2 = gca();

    % set callback s.t. radargram right schows alwas data of position of mouse in the left radargram
    global mouseMoveAlreadyExecuting;
    mouseMoveAlreadyExecuting=false;
    set (gcf1, 'WindowButtonMotionFcn', {@manualPick_MouseMove, Radar.radargram, Radar.dates, Radar.samples, WettingFrontTwt, Radar.TWT, TA, TSS, gca11, gca12, ax(1), hfig2, gca2});

    % wait until key pressed (ignore mouse klicks)
    w=0;
    while w==0
        w = waitforbuttonpress;
    end

    %reset callback
    set (gcf1, 'WindowButtonMotionFcn', []);

    % switch for pressed key
    key=get(hfig2, 'CurrentKey');
    switch lower(key)
        case 's'
            C = get (gca11, 'CurrentPoint');
            % get position of mouse
            if iscell(C)
                x = C{1,1}(1,1);
                y = C{1,1}(1,2);
            else
                x = C(1,1);
                y = C(1,2);
            end
            
            % follow phase after klick
            newPickTwt = manualPick_FollowPhase(Radar.radargram, Radar.dates, Radar.samples, x, y);
            % merge old and new pick
            WettingFrontTwt(end-length(newPickTwt)+1:end) = newPickTwt;
            % store positon of last klick
            lastKlick = [x,y*vMean/2];
        case 'c'
            C = get (gca11, 'CurrentPoint');
            % get position of mouse
            if iscell(C)
                x = C{1,1}(1,1);
                y = C{1,1}(1,2);
            else
                x = C(1,1);
                y = C(1,2);
            end
            [~,Ix,~]=findNearest(Radar.dates, x);
            
            % clear WettingFront after klick
            WettingFrontTwt(Ix:end) = NaN;
            % store positon of last klick
            lastKlick = [x,y*vMean/2]; 
        case 'f'
            C = get (gca11, 'CurrentPoint');
            % get position of mouse
            if iscell(C)
                x = C{1,1}(1,1);
                y = C{1,1}(1,2);
            else
                x = C(1,1);
                y = C(1,2);
            end
            [~,Ix,~]=findNearest(Radar.dates, x);
            
            % force pick to be here
            WettingFrontTwt(Ix) =y;
            % store positon of last klick
            lastKlick = [x,y*vMean/2];
        case 'n'
            C = get (gca11, 'CurrentPoint');
            % get position of mouse
            if iscell(C)
                x = C{1,1}(1,1);
                y = C{1,1}(1,2);
            else
                x = C(1,1);
                y = C(1,2);
            end
            [~,Ix,~]=findNearest(Radar.dates, x);
            
            % clear WettingFront after klick
            WettingFrontTwt(Ix:end) = 0;
            % store positon of last klick
            lastKlick = [x,0]; 
        case 'r'      
            clear ax axis1_vor axis2_vor;
        case 'add'      
            climpIntens=climpIntens*.7;
        case 'subtract'      
            climpIntens=climpIntens*1.3;
        case 'escape'
            break;
        case 'f5'
            error('Interrupted by user');
        case 'h'
            fprintf('===== Help for PickReflector =======\ns: set pick\nc: clear pic\nf: force pick\nn: force pick to be 0\nr: reset plot\n+: more contrast\n-: less contrast\nESC: escape\n')
        otherwise
            disp('Press "h" for help')
    end
end