function [ntr,nsamp,px,py,pz,origpx,origpy,tti,zdem,zeff,data,procp] = SegyGPRProc_2(ntr,sra,nsamp,data,px,py,pz,origpx,origpy,tti,zdem,zeff,profinfo,procp)

% all new variables with t: include topography
% all new variables with i: include binning
% xi, yi: Binning centers
% xxp, yyp: px, py projected on the profile line


%%%%%% Topography correction should be performed after migration. It is now
%%%%%% included in the SegyPlotData routine
% % -------------------------------------------------------------------------
% % ------------------- CALCULATE TOPOGRAPHY --------------------------------
% % -------------------------------------------------------------------------
% ma = max(pz);               % maximum altitude
% mi = min(pz);               % minimum altitude
% dz = sra/2.0*procp.vair;    % calculate sampling distance from sampling rate (distance = v*TWT/2)
% addz = round((ma-mi)/dz);   %addz: additional samples for topography
% 
% nsampt = nsamp + addz;
% t2 = (0:nsampt-1)*sra;
% datat = zeros(nsampt,ntr);
% fs = zeros(1,ntr);          %fs: first sample
% 
% for a = 1:ntr
%     fs(a) = round((ma-pz(a))/dz);
%     datat(fs(a)+1:fs(a)+nsamp,a) = data(1:nsamp,a);
%     tti(a) = tti(a) + fs(a)*sra;
% end;

datat = data;
nsampt = nsamp;
t2 = (0:nsampt-1)*sra;

% -------------------------------------------------------------------------
% ------------------- DETERMINE BINNING CENTERS ---------------------------
% -------------------------------------------------------------------------
maxp = 1000000;
xi = zeros(maxp,1);
yi = zeros(maxp,1);
pseg = zeros(maxp,1);
n = 0;
rest = 0;

% dist: distance of segment
% azi:  azimuth of segment
% dri:  distance between binning centres
for a = 1:profinfo.n-1
    dist = sqrt((profinfo.x(a+1)-profinfo.x(a))^2 + ...
        (profinfo.y(a+1)-profinfo.y(a))^2);
    azi = atan2((profinfo.y(a+1)-profinfo.y(a)),(profinfo.x(a+1)-profinfo.x(a)));
    n = n + 1;
    xi(n) = profinfo.x(a) + rest*cos(azi);
    yi(n) = profinfo.y(a) + rest*sin(azi);
    pseg(n) = a;
    dist2 = 0.0;
    
    while(1)
        dist2 = dist2 + procp.dri;
        if (dist2 >= dist)
            rest = dist2 - dist;
            break;
        else
            n = n + 1;
            xi(n) = xi(n-1) + procp.dri*cos(azi);
            yi(n) = yi(n-1) + procp.dri*sin(azi);
            pseg(n) = a;
        end;
    end;
end;

xi = xi(1:n);
yi = yi(1:n);
pseg = pseg(1:n);


% -------------------------------------------------------------------------
% ------------------- MAKE SURE PROFILE RUNS FROM WEST TO EAST ------------
% -------------------------------------------------------------------------
% if profile does not run from west to east: flip profile
if (xi(n) < xi(1))
    xi = flipud(xi);
    yi = flipud(yi);
    pseg = flipud(pseg);
end;


% -------------------------------------------------------------------------
% ------------------- PROJECT PX AND PY ON THE INDIVIDUAL SEGMENTS --------
% -------------------------------------------------------------------------
xxp = zeros(length(px),profinfo.n-1);
yyp = zeros(length(py),profinfo.n-1);

for a = 1:(profinfo.n-1);
    x = profinfo.x(a:a+1); % Start and end points of segments
    y = profinfo.y(a:a+1); % Start and end points of segments
    dx = abs(diff(x));     % dx length of sement
    dy = abs(diff(y));     % dy length of segment
    
    % Find a straight line through the start and end points of the segments
    % Project xp, yp on this line
    if (dx > dy)
        cc = polyfit(x,y,1);
        [xxp(:,a),yyp(:,a)] = project_p(px,py,cc(1),cc(2));
    else
        cc = polyfit(y,x,1);
        [yyp(:,a),xxp(:,a)] = project_p(py,px,cc(1),cc(2));
    end;
end;

% -------------------------------------------------------------------------
% ----------IDENTIFY FIRST AND LAST INTERPOLATION POINTS ON SEGMENT -------
% -------------------------------------------------------------------------

% Check start point
for a = 1:length(xi)
    dist = sqrt((xxp(:,pseg(a))-xi(a)).^2 + (yyp(:,pseg(a))-yi(a)).^2);
    if (isempty(find(dist < procp.dri)) == 0)
        break;
    end;
end;

first = a;          % First point projected on line
last = length(xi);  % last point projected on line

% Check last point
while(1)
    dist = sqrt((xxp(:,pseg(last))-xi(last)).^2 + (yyp(:,pseg(last))-yi(last)).^2);
    if (isempty(find(dist < procp.dri)) == 0)
        break;
    else
        last = last - 1;
    end;
end;

xi = xi(first:last);
yi = yi(first:last);


% figure(1); clf;
% mix = min(px);
% miy = min(py);
% plot(profinfo.x-mix,profinfo.y-miy, 'b-',xi-mix,yi-miy,'r.',px-mix,py-miy,'m.', xxp-mix,yyp-miy,'k.');
% xlabel('Easting');
% ylabel('Northing');
% legend('picked segment','binning centres','measured points', 'projected points')
% title('Projected Points Profile');
% axis equal



% % % -------------------------------------------------------------------------
% % % -------------- BINNING - WITH MULTIPLICATION AND STACKING ---------------
% % % -------------------------------------------------------------------------
% % ntri = length(xi); % lenght of projected points on profile line
% % datait = zeros(nsampt,ntri);
% % zi = zeros(ntri,1);
% % zeffi = zeros(ntri,1);
% % zdemi = zeros(ntri,1);
% % ttii = zeros(ntri,1);
% % fsi = zeros(ntri,1);
% % 
% % origpxi = origpx;
% % origpyi = origpy;
% % 
% % % w: weighting factor for every trace
% % % ii: Index of all points which are in the radius 2*dri
% % for a = 1:ntri
% %     w = zeros(ntr,1);
% %     dist = sqrt((xxp(:,pseg(a))-xi(a)).^2 + (yyp(:,pseg(a))-yi(a)).^2);
% %     ii = find(dist < 2*procp.dri);
% %     w(ii) = (2*procp.dri - dist(ii))/(2*procp.dri);
% %     w = w/sum(w);
% %     
% %     
% %     for b = 1:length(ii)
% %         datait(:,a) = datait(:,a) + w(ii(b))*datat(:,ii(b));
% %         zi(a) = zi(a) + w(ii(b))*pz(ii(b));
% %         zeffi(a) = zeffi(a) + w(ii(b))*zeff(ii(b));
% %         zdemi(a) = zdemi(a) + w(ii(b))*zdem(ii(b));
% %         ttii(a) = ttii(a) + w(ii(b))*tti(ii(b));
% %         fsi(a) = fsi(a) + w(ii(b))*fs(ii(b));
% %     end;
% %     
% %     % -------------- EXTRAPOLATE EMPTY TRACES --------------
% %     % If no data in trace, take data from trace before and write in this empty trace
% %     % !! First trace must have data!!
% %     if (a > 1)
% %         if (zi(a) == 0), zi(a) = zi(a-1); end;
% %         if (zeffi(a) == 0), zeffi(a) = zeffi(a-1); end;
% %         if (zdemi(a) == 0), zdemi(a) = zdemi(a-1); end;
% %         if (ttii(a) == 0), ttii(a) = ttii(a-1); end;
% %         if ((fsi(a) == 0) & (a > 1)), fsi(a) = fsi(a-1); end;
% %         fsi = round(fsi);
% %         if (fsi(a) == 0), fsi(a) = 1; end;
% %     end
% %     
% %     if ((sum(abs(datait(:,a))) == 0) & (a ~=1))
% %         datait(:,a) = datait(:,a-1);     end;
% % end;




% -------------------------------------------------------------------------
% ------------------- BINNING - TAKE ONLY CLOSEST TRACE -------------------
% -------------------------------------------------------------------------
ntri = length(xi);
datait = zeros(nsampt,ntri);
zi = zeros(ntri,1);
zeffi = zeros(ntri,1);
zdemi = zeros(ntri,1);
ttii = zeros(ntri,1);
fsi = zeros(ntri,1);
origpxi = zeros(ntri,1);
origpyi = zeros(ntri,1);
for a = 1:ntri
    dist = sqrt((xxp(:,pseg(a))-xi(a)).^2 + (yyp(:,pseg(a))-yi(a)).^2);
    ii = find(dist == min(dist));

    ii = ii(1);
    datait(:,a) = datat(:,ii);
    zi(a) =  pz(ii);
    zeffi(a) = zeff(ii);
    zdemi(a) = zdem(ii);
    ttii(a) = tti(ii);
    origpxi(a) = origpx(ii);
    origpyi(a) = origpy(ii);

    % -------------- EXTRAPOLATE EMPTY TRACES --------------
    % If no data in trace, take data from trace before and write in this empty trace
    % With if (a > 1) allow the first trace to be empty (tti is zero for all traces in Ground based data)
    if (a > 1)
        if (zi(a) == 0), zi(a) = zi(a-1); end;
        if (zeffi(a) == 0), zeffi(a) = zeffi(a-1); end;
        if (zdemi(a) == 0), zdemi(a) = zdemi(a-1); end;
        if (ttii(a) == 0), ttii(a) = ttii(a-1); end;
        if ((fsi(a) == 0) & (a > 1)), fsi(a) = fsi(a-1); end;
        fsi = round(fsi);
        if (fsi(a) == 0), fsi(a) = 1; end;
    end

    if ((sum(abs(datait(:,a))) == 0) & (a ~=1))
        datait(:,a) = datait(:,a-1);     end;
end;



% -------------------------------------------------------------------------
% ------------------- SMOOTING OF TOPOGRAPY -------------------------------
% -------------------------------------------------------------------------
zi = medfilt1(zi,3); % medfilt1: 1D median filter
zeffi = medfilt1(zeffi,3);
zdemi = medfilt1(zdemi,3);
ttii = medfilt1(ttii,3);




% -------------------------------------------------------------------------
% ------------------- HILBERT ENVELOPE GAIN FUNCTION ----------------------
% -------------------------------------------------------------------------
if procp.hilbwin > 0 & procp.hilbtask == 2;
    [datait,procp] = HilbertEnvelopeGain(datait,ntri,ttii,t2,sra,nsampt,procp);
end

if procp.hilbwin > 0 & procp.hilbtask ~= 2;
    nsamph = round(procp.hilbwin/sra);
    ifirst = round(procp.hilbfirst/sra);
    itti = round(ttii/sra);
    env2 = zeros(nsampt,1);
    
    for a = 1:ntri
        hd2 = hilbert(datait(:,a));
        env2 = env2 + sqrt(hd2.*conj(hd2));
    end
    
    if  procp.hilbtask == 1; % Give the Hilbert Envelope parameters via input
        figure(2); clf;
        tt = (0:nsampt-1)*sra;
        plot(tt,log(env2)','b'); hold on
        plot(tt(itti(a)+1+ifirst:itti(a)+nsamph+ifirst),log(env2(itti(a)+1+ifirst:itti(a)+nsamph+ifirst)),'r'); hold off;
        grid on;
        title('Hilbert envelope gain function')
        
        prompt = {'Enter Start time:','Enter window length:'};
        def = {num2str(procp.hilbfirst),num2str(procp.hilbwin)};
        hilbparam = inputdlg(prompt,'Parameters to compute Hilbert Envelope',2,def);
        procp.hilbfirst = str2num(hilbparam{1});
        procp.hilbwin = str2num(hilbparam{2});
    end
    
    nsamph = round(procp.hilbwin/sra);
    ifirst = round(procp.hilbfirst/sra);
    env = zeros(nsamph,1);
    
    for a = 1:ntri
        hd = hilbert(datait(itti(a)+1+ifirst:itti(a)+nsamph+ifirst,a));
        env = env + sqrt(hd.*conj(hd));
    end
    
    env = env/ntri;
    th = (0:nsamph-1)*sra;
    cc = polyfit(th,log(env)',1);
    
%     fig = figure(2); clf;
%     tt = (0:nsampt-1)*sra;
%     plot(tt,log(env2)','b');
%     hold on;
%     plot(tt(itti(a)+1+ifirst:itti(a)+nsamph+ifirst),log(env2(itti(a)+1+ifirst:itti(a)+nsamph+ifirst)),'r');
%     hold off;
%     xlabel('Travel Time (ns)','fontsize',12);
%     ylabel('Log(envelope)','fontsize',12);
    
    for a = 1:nsampt
        datait(a,:) = datait(a,:)/exp(polyval(cc,t2(a)));
    end
    
end



% -------------------------------------------------------------------------
% ------------------- AGC GAIN FUNCTION -----------------------------------
% -------------------------------------------------------------------------
if (procp.agclen > 0)
    datait = agc(datait,procp.agclen,sra);
end;



% -------------------------------------------------------------------------
% ------------------- FX-DECONVOLUTION ------------------------------------
% -------------------------------------------------------------------------
if (procp.decon_len > 0)
    datait = fx_decon(datait,sra/1000,procp.decon_len,0.02,procp.fl,procp.fh);
end;



% -------------------------------------------------------------------------
% ------------------- OUTPUT DATA -----------------------------------------
% -------------------------------------------------------------------------
ntr = ntri;
nsamp = nsampt;
px = xi;
py = yi;
pz = zi;
data = datait;
zeff = zeffi;
zdem = zdemi;
tti = ttii;
origpx = origpxi;
origpy = origpyi;


