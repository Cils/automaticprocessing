function Segy_GPR_Wrapper(proc1,picksurf,proc2,mig,dispmap,split,pickhorizon)

% proc1 = 1:        Do initial processing
% picksurf = 1:     Pick surface reflection
% proc2 = 1:        Do second processing
% mig = 1:          Do migration
% dispmap = 1:      Display GPR section and the profile location on a map
% split = 1:        Split too long profiles
% dispmap = 1:      Display profiles on map
% pickhorizon = 1:  Pick bedrock reflection

%Example: Segy_GPR_Wrapper(1,1,0,0,0,0,0)

% 1. Set pp: Path to GPR files
% 2. Set matdata path to matdata folder

if nargin < 7;
    error('Not enough input arguments, check input variables')
    return;
end;

% -------------------------------------------------------------------------
% ------------------- SET PATHS & IDs -------------------------------------
% -------------------------------------------------------------------------

matdata = '/.../matdata/';  %Path to matlab folder
filedate = 'AREA_yyyymmdd';
mapname = '...';            %Name of map.mat 

% ---- Path to crewes folder with kirk_mig.m file ----
crewes_path = '/Users/ranja/Documents/Matlab/crewes'; %Windows
%crewes_path = '/apps/matlab2011b_64/toolbox/crewes'; %Linux


segyfolder = [matdata filedate '_Segy/'];
plotfolder = [matdata filedate '_Plots/'];

% -------------------------------------------------------------------------
% ------------------- DEFINE PROCESSING PARAMETERS ------------------------
% -------------------------------------------------------------------------

load zcm.mat                % blue-red colormap
load blkwhtred.mat          % black-white-red colormap
procp.cmap = blkwhtred;     % Colormap for displaying sections
procp.cax = [-1.0 1.0];     % Lower and upper bounds of colorbar
procp.vair = 0.299;         % Air velocity
procp.vice = 0.1689;        % Ice velocity

procp.fl = 40;              % Low cut frequency for bandpass filter
procp.fh = 150;             % High cut frequency for bandpass filter
procp.ford = 4;             % Order of the Butterworth bandpass filter
procp.twin = 3000;          % Length of trace time window
procp.maxdepth = 250;       % Maximum depth [m] for displaying data
procp.hilbfirst = 700;      % Start time of Hilbert envelope window
procp.hilbwin = 500;        % Length of time window of Hilbert envelope [ns]
procp.dri = 1;              % Trace interpolation distance, used for binning
procp.svd_len = 50;         % Length of SVD filter
procp.decon_len = 50;       % fx-deconvolution filter length
procp.agclen = 0;           % Length of AGC filter (0 if no agc)
procp.GlobalZshift = 0;   % z shift over all profiles
procp.t0_corr = 0;          % time-zero correction for PulseEkko Ground data



% -------------------------------------------------------------------------
% ------------------- LIST PROFILES FOR WHICH PROCESSING IS DONE ----------
% -------------------------------------------------------------------------
% dd1: list all profiles
% dd: vector containing all proifles -> run processing over all dd
dd1 = dir(sprintf('%s%s_RAW_PROFILE_*',segyfolder,filedate)); clear dd;
for C = 1:length(dd1);
    dd(C) = sscanf(dd1(C,1).name,[filedate '_RAW_PROFILE_%d']); end

if dispmap == 1
    dd1 = dir(sprintf('%s%s_mig_PROFILE_*',segyfolder,filedate)); clear dd;
    for C = 1:length(dd1);
        dd(C) = sscanf(dd1(C,1).name,[filedate '_mig_PROFILE_%d']); end
end

if mig == 1 | pickhorizon == 1 | split == 1;
    dd1 = dir(sprintf('%s%s_proc2_PROFILE_*',segyfolder,filedate)); clear dd;
    for C = 1:length(dd1);
        dd(C) = sscanf(dd1(C,1).name,[filedate '_proc2_PROFILE_%d']); end
end


% -------------------------------------------------------------------------
% ------------------- START PROCESSING ------------------------------------
% -------------------------------------------------------------------------
for a = dd
    
%     % ---- File to store adjusted hilbert function and colormap info ----
%     caxis_file = sprintf('%s%s_CAX_HILB_PROFILE_%03d.mat',matdata,filedate,a);
%     if exist(caxis_file) == 2;
%         load(caxis_file);
%         if exist('cmap_adj') == 1;
%             if cmap_adj == 1; procp.cmap = zcm; end
%             if cmap_adj == 2; procp.cmap = blkwhtred; end
%         end
%         if exist('cax_adj') == 1; procp.cax = cax_adj; end
%         procp.hilbfirst = hilbfirst_adj;
%         procp.hilbwin = hilbwin_adj;
%     end
    
    
    % ---------------------------------------------------------------------
    % ------------------- INITIAL PROCESSING ------------------------------
    % ---------------------------------------------------------------------
    if  proc1 == 1
        % ------------------- Read .sgy file ------------------------------
        segyname = sprintf('%s%s_RAW_PROFILE_%03d',segyfolder,filedate,a);
        [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
        disp(comments);
        
        % ------------------- Apply processing ----------------------------
        disp(sprintf('Start proc1 Profile %d',a));
        [nsamp,data] = SegyGPRProc_1(procp,ntr,sra,data);
        
        Title = sprintf('After Initial Processing: %s Profile %d',filedate,a);
        [fig,procp] = SegyPlotData(Title,nsamp,sra,ntr,tti,data,zdem,zeff,procp,0,0);
        plotfile = sprintf('%s%s_Proc1_PROFILE_%03d.jpg',plotfolder,filedate,a);
        %print(fig,'-djpeg99','-r600',plotfile); %print figure
        
        % ------------------- Write .sgy file proc1_PROFILE.sgy -----------
        profnr = a;
        segyname = sprintf('%s%s_proc1_PROFILE_%03d.sgy',segyfolder,filedate,a);
        comments = make_comments(info.area,info.instr,profnr,info.freq,info.offset,info.dewsoft,date,ntr,sra,nsamp,procp,profinfo,1,0,0,0);
        GPR_write_segy(segyname,comments,offset,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date)
        sprintf('--- Write SEG Y file proc1_PROFILE_%03d.sgy done ---',a); 
    end
    
    
    % ---------------------------------------------------------------------
    % ------------------- PICK SURFACE REFLECTION -------------------------
    % ---------------------------------------------------------------------
    if picksurf == 1
        if proc1 == 0
            % ------------------- Read .sgy file --------------------------
            segyname = sprintf('%s%s_proc1_PROFILE_%03d',segyfolder,filedate,a);
            [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
            disp(comments);
        end
        
        % ------------------- Pick Surface Reflection ---------------------
        tax = 1200; % Lower limitation of the time axis for picking
        [tti] = SegyPickSurface(ntr,sra,nsamp,data,procp,tax);
        zeff = pz - (tti/2 * procp.vair);
        
        % ------------------- Save Picks into .mat file -------------------
        pick_file = sprintf('%s%s_SURFPICK_PROFILE_%03d.mat',matdata,filedate,a);
        save(pick_file,'tti','px','py','pz');
        
        % ------------------- If picks  already exist as .mat file --------
        pick_file = sprintf('%s%s_SURFPICK_PROFILE_%03d.mat',matdata,filedate,a);
        load(pick_file,'tti');
        zeff = pz - (tti/2*procp.vair);
        
        % -----------------------------------------------------------------
        % ------ For PulseEkko data: tti is the time-zero correction ------
        % -----------------------------------------------------------------
        % tti = ones(1,ntr)*procp.t0_corr; zeff = pz - (tti/2*procp.vair);
        
        
        % ------------------- Write .sgy file proc1_PROFILE.sgy -----------
        profnr = a;
        segyname = sprintf('%s%s_proc1_PROFILE_%03d.sgy',segyfolder,filedate,a);
        comments = make_comments(info.area,info.instr,profnr,info.freq,info.offset,info.dewsoft,date,ntr,sra,nsamp,procp,profinfo,1,1,0,0);
        GPR_write_segy(segyname,comments,offset,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date)
        sprintf('--- Write SEG Y file proc1_PROFILE_%03d.sgy done ---',a);
    end
    
    
    
    % ---------------------------------------------------------------------
    % ------------------- SECONDARY PROCESSING ----------------------------
    % ---------------------------------------------------------------------
    if proc2 == 1
        if proc1 == 0 | picksurf == 0
            % ------------------- Read .sgy file --------------------------
            segyname = [sprintf('%s%s_proc1_PROFILE_%03d',segyfolder,filedate,a)];
            [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
            disp(comments);
        end
        
        disp(sprintf('Start proc2 Profile %d',a));
        %task = 0;   % No adjustment of Hilbert parameters
        %task = 1;  % Hilbert parameters can be adjusted
        task = 2;   % Hilbert parameters can be adjusted interactively
        [ntr,nsamp,px,py,pz,origpx,origpy,tti,zdem,zeff,data,procp] = SegyGPRProc_2(ntr,sra,nsamp,data,px,py,pz,origpx,origpy,tti,zdem,zeff,profinfo,procp,task);
       
        Title = sprintf('After Second Processing: %s Profile %d',filedate,a);
        [fig,procp] = SegyPlotData(Title,nsamp,sra,ntr,tti,data,zdem,zeff,procp,2,0);
        plotfile = sprintf('%s%s_Proc2_PROFILE_%03d.jpg',plotfolder,filedate,a);
        %print(fig,'-djpeg99','-r600',plotfile);
        
        %---------- Save Hilbert parameters and Colormap parameters -------
        cax_adj = procp.cax_adj;
        cmap_adj = procp.cmap_adj;
        hilbfirst_adj = procp.hilbfirst_adj;
        hilbwin_adj = procp.hilbwin_adj;
        save(caxis_file,'cax_adj','cmap_adj','hilbfirst_adj','hilbwin_adj');
        
        % ------------------- Write .sgy file proc1_PROFILE.sgy -----------
        profnr = a;
        segyname = sprintf('%s%s_proc2_PROFILE_%03d.sgy',segyfolder,filedate,a);
        comments = make_comments(info.area,info.instr,profnr,info.freq,info.offset,info.dewsoft,date,ntr,sra,nsamp,procp,profinfo,1,1,1,0);
        GPR_write_segy(segyname,comments,offset,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date);
        sprintf('--- Write SEG Y file proc2_PROFILE_%03d.sgy done ---',a)
    end
    
    
    
    % ---------------------------------------------------------------------
    % ------------------- MIGRATION ---------------------------------------
    % ---------------------------------------------------------------------    
    if mig == 1;
        % ------------------- Read .sgy file ------------------------------
        segyname = sprintf('%s%s_proc2_PROFILE_%03d',segyfolder,filedate,a);
        [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
        disp(comments);
        
        % ------------------- Run Migration -------------------------------
        if (isempty(which('kirk_mig')) == 1)
            addpath(genpath(crewes_path));
        end;
        
        disp(sprintf('Start mig Profile %d',a));
        [data] = SegyGPRMig(ntr,sra,data,tti,procp); %Run migration
        
        % ------------------- Save migration .mat file --------------------
        mig_file = sprintf('%s%s_mig_PROFILE_%03d.mat',matdata,filedate,a);
        save(mig_file,'data');
        
        % ------------------- If migration .mat file already exists -------
        load(mig_file);
        
        
        Title = sprintf('After Migration: %s Profile %d',filedate,a);
        [fig,procp] = SegyPlotData(Title,nsamp,sra,ntr,tti,data,zdem,zeff,procp,2,1);
        plotfile = sprintf('%s%s_Mig_PROFILE_%03d.jpg',plotfolder,filedate,a);
        print(fig,'-djpeg99','-r600',plotfile);
        
        %---------- Save Hilbert parameters and Colormap parameters -------
        cax_adj = procp.cax_adj;
        cmap_adj = procp.cmap_adj;
        hilbfirst_adj = procp.hilbfirst;
        hilbwin_adj = procp.hilbwin;
        
        save(caxis_file,'cax_adj','cmap_adj','hilbfirst_adj','hilbwin_adj');
        
        
        % -----------------------------------------------------------------
        % ------------------- TRACE NORMALIZATION -------------------------
        % -----------------------------------------------------------------  
        data = tracenormalize(data);
        
        % ------------------- Write .sgy file mig_PROFILE.sgy -------------------
        profnr = a;
        segyname = sprintf('%s%s_mig_PROFILE_%03d.sgy',segyfolder,filedate,a);
        comments = make_comments(info.area,info.instr,profnr,info.freq,info.offset,info.dewsoft,date,ntr,sra,nsamp,procp,profinfo,1,1,1,1);
        GPR_write_segy(segyname,comments,offset,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date)
        sprintf('--- Write SEG Y file mig_PROFILE_%03d.sgy done ---',a)
    end 
    
    
    
    
    % ---------------------------------------------------------------------
    % ------------------- DISPLAY MAP & GPR SECTION -----------------------
    % ---------------------------------------------------------------------   
    if dispmap == 1;
        segyname = sprintf('%s%s_mig_PROFILE_%03d',segyfolder,filedate,a);
        Title = sprintf('After Migration: %s Profile %d',filedate,a);
        task = 0; % .mat file with the map data (from makemap)
        %task = 1 % .tif file (georef. tif file)
        mapfile = sprintf('%s.mat',mapname);
        
        fig = SegyDisplayResults(Title,segyname,procp,mapname,0);
        
        plotfile = sprintf('%s%s_Map_PROFILE_%03d.jpg',plotfolder,filedate,a);
        print(fig,'-djpeg99','-r600',plotfile);
    end
    
    
    % ---------------------------------------------------------------------
    % ------------------- PICK HORIZON / BEDROCK --------------------------
    % ---------------------------------------------------------------------
    if pickhorizon == 1;
        
        datafile = sprintf('%s%s_proc2_PROFILE_%03d',segyfolder,filedate,a);
        migfile = sprintf('%s%s_mig_PROFILE_%03d',segyfolder,filedate,a);
        outputfile = sprintf('%s%s_PROFILE_%d_BEDROCK.txt',segyfolder,filedate,a);
        
        Title = sprintf('Bedrock Picks: %s Profile %d',filedate,a);
        fig = SegyPickHorizon(datafile,migfile,procp,outputfile);
        title(sprintf('Bedrock Picks: %s Profile %d',filedate,a));
        plotfile = sprintf('%s%s_Bedr_PROFILE_%03d.jpg',plotfolder,filedate,a);
        print(fig,'-djpeg99','-r600',plotfile);
    end
end


% -------------------------------------------------------------------------
% ------------------- SPLIT LONG PROFILES ---------------------------------
% -------------------------------------------------------------------------
% !!! Split profiles AFTER second processing (due to profinfo)
% profno: Profile number / Line number of profiles to split
% nsplit: Segments in which profiles get splitted, e.g. if profile should
%         be split in two profiles: nsplit = 2

if  split == 1;
    disp('Start split Profiles')
    
    nprof = length(dd1);
    if (strcmp(filedate,'...') == 1)
        profno = ['..'];
        nsplit = [2]; % integer number of how many sub-profiles
    end
    
    SegySplitProfiles(profno,nsplit,nprof,segyfolder,filedate,procp);
end

