
%% Process GPR data
% order of functions can be changed according to the data
% parameters have to be tuned to the data
%% 
clear all 
close all
clc

addpath(genpath('D:\GPR\Projects'));

%% set projectpath and name
GPRproject_prjname = 'DLINE_23_24';
GPRproject_prjpath = 'D:\GPR\Projects\Otemma_20140307\Otemma_20140307_proc\GROUND_EKKO\preproc\';
GPRproject_save    = 'D:\GPR\Projects\Otemma_20140307\Otemma_20140307_proc\GROUND_EKKO\proc\';

%% load data
% load from GPRmatlab file saved after preprocessing I + II
cd(GPRproject_prjpath);
load('DLINE_23_24.gprd','-mat')

n = 2;

gpr_plot2D(gprdat(n-1))

% % export segy 
%  cd(GPRproject_save);
% 
% params.sgyname = 'DLINE09.segy';
% gpr_exportPromSEGY(gprdat(n-1), params)

% %% remove mean trace/background removal
% % not needed when data was dewowed
% n = 2;
% gprflw(n).name = 'remove_mean';
% gprflw(n).para   = [];
% [gprdat(n) n] = gpr_removeMeanTrace(gprdat(n-1), gprflw(n).para, n);
% 
% gpr_plot2D(gprdat(n-1))

%% mute direct wave
% user can select time window that is muted 
gprflw(n).name = 'mute_directwave';
gprflw(n).para   = [];
[gprdat(n) n] = gpr_muteDirectWave(gprdat(n-1), gprflw(n).para, n);

%% move zero-time
% either use manual value or value from trace header
gprflw(n).name = 'move_t0';
gprflw(n).para.timeshift   = gprdat.t0; % use time from header

[gprdat(n) n] = gpr_moveZeroTime(gprdat(n-1), gprflw(n).para, n);

gpr_plot2D(gprdat(n-1))

%% eigenvalue filter (SVD filter )
gprflw(n).name         = 'SVD filter';
gprflw(n).para.n    = 2; % the first 2 eigenvalues are set to 0
gprflw(n).para.output = 0; % one for eigenvalue spectrum
gprflw(n).para.trwin  = 0; % number of traces in moving svd window

[gprdat(n) n] = gpr_svdfilt(gprdat(n-1), gprflw(n).para, n);

gpr_plot2D(gprdat(n-1))

% %% cut data 
% % reduce length of record
% gprflw(n).name = 'cut_at_3750';
% gprflw(n).para.timecut = 3750;
% 
% [gprdat(n) n] = gpr_cutTime(gprdat(n-1), gprflw(n).para, n);
% 
% gpr_plot2D(gprdat(n-1))

% %% butterworth filter
% gprflw(n).name = 'frequency_bandpass';
% gprflw(n).para.fl = 20;
% gprflw(n).para.fh = 80;
% gprflw(n).para.ford = 4; 
% 
% [gprdat(n) n] = gpr_butterworth(gprdat(n-1), gprflw(n).para, n);
% 
% gpr_plot2D(gprdat(n-1))

%% frequency bandpass filter

gprflw(n).name = 'frequency_bandpass';
gprflw(n).para.corner_frequencies = [20 70];
gprflw(n).para.ntaper = 11;
gprflw(n).para.output = 1;

[gprdat(n) n] = gpr_frequency_bandpass(gprdat(n-1), gprflw(n).para, n);

gpr_plot2D(gprdat(n-1))

%% Surface picking

gprflw(n).name = 'pick_surface';
gprflw(n).para.tax  = 1200;          %Lower limitation of the time axis for picking
        
[tti] = gpr_SegyPickSurface(gprdat(n-1), gprflw(n).para, n);
%     
% %         % -----------------------------------------------------------------
% %         % ------ For PulseEkko data: tti is the time-zero correction ------
% %         % -----------------------------------------------------------------
% %         % tti = ones(1,ntr)*procp.t0_corr; zeff = pz - (tti/2*procp.vair);
% %         
%% binning
% projects traces on a straight line and assigns them to bins of desired
% size
gprflw(n).name         = 'gpr_bin2D';
gprflw(n).para.binsize = 0.5;

[gprdat(n) n] = gpr_bin2D(gprdat(n-1), gprflw(n).para, n);


gpr_plot2D(gprdat(n-1))



% % Hilbert transform
% 
% gprflw(n).name = 'Hilbert transform'; 
% gprflw(n).para.dri = 1;              % Trace interpolation distance, used for binning
% gprflw(n).para.ntri = length(gprdat(n-1).nr_of_traces);
% gprflw(n).para.ttii = gprdat(n-1).nr_of_traces;
% gprflw(n).para.t2 = (0:gprdat(n-1).nr_of_samples-1)*(gprdat(n-1).sampling_rate);
% gprflw(n).para.hilbfirst = 700;      % Start time of Hilbert envelope window
% gprflw(n).para.hilbwin = 500;        % Length of time window of Hilbert envelope [ns]
% 
% [gprdat(n) n] = gpr_HilbertEnvelopeGain(gprdat(n-1), gprflw(n).para,n);

% %% gain
% % # inverse envelope gain
% % # linear window
% % # gaussian window
% 
% switch 3
%     case 1
%         gprflw(n).name = 'gpr_gainInvEnv2D'; 
%         gprflw(n).para.timewindow  = [10 600];
%         gprflw(n).para.tracewindow = 1000;
%         
%         [gprdat(n) ,~] = gpr_gainInvEnv2D(gprdat(n-1), gprflw(n).para,n);
%         
%     case 2
%         gprflw(n).name = 'gpr_gainAGC1D';
%         gprflw(n).para.timewindow = [10 gprdat(n-1).nr_of_samples];
%         gprflw(n).para.agcwindow  = 50;
%         
%         [gprdat(n) n] = gpr_gainAGC1D(gprdat(n-1), gprflw(n).para, n);
%     case 3
%         gprflw(n).name = 'gpr_gaingagc';
%         gprflw(n).para.agcwindow  = 500;
%         [gprdat(n) n] = gpr_gaingagc(gprdat(n-1), gprflw(n).para, n);
%         
%        
% end
% 
% gpr_plot2D(gprdat(n-1))

% %% trace normalize
% 
% gprflw(n).name = 'trace normalize';
% [gprdat(n) n] = gpr_tracenormalize(gprdat(n-1), n);
% 
% gpr_plot2D(gprdat(n-1))

% %% histogram equalisation of data
% gprflw(n).name         = 'gpr_histeq';
% gprflw(n).para.n      = 64; %colour levels 64 / 256
% [gprdat(n) n] = gpr_histeq(gprdat(n-1), gprflw(n).para, n);
% 
% gpr_plot2D(gprdat(n-1))

% %% deconvolution 
% gprflw(n).name         = 'deconvolution';
% gprflw(n).para.decon   = 20;      % fx-deconvolution filter length
% gprflw(n).para.flow    = 20;
% gprflw(n).para.fhigh   = 80;
% gprflw(n).para.sampling = gprdat.sampling_rate;
% gprflw(n).para.mu      = 0.02;
% 
% [gprdat(n) n] = gpr_fx_decon(gprdat(n-1), gprflw(n).para, n);
% 
% gpr_plot2D(gprdat(n-1))

% % %% export to SEGY
% % % SEGY data can be read for exemple in PROMAX
% % 
% % %cd(GPRproject_save);
% % gprflw(n).name         = 'export_SEGY';
% % gprflw(n).para.fname   = [GPRproject_prjname '.segy'];
% % gprflw(n).para.cflag   = 2; % 1 real coordinates, 2 relative along profile
% % gprflw(n).para.pprec   = 2; % position precision (decimals)
% % [gprdat(n) n] =  gpr_exportSEGY(gprdat(n-1),gprflw(n).para, n);
% 
 %% migration

gprflw(n).name      = 'migration';
gprflw(n).para.vair      = 0.299;         % Air velocity
gprflw(n).para.vice      = 0.1689;        % Ice velocity
gprflw(n).para.dri       = 3;             % Trace interpolation distance in migration
gprflw(n).para.tti       = tti;
 
[gprdat(n) n] = gpr_SegyGPRMig(gprdat(n-1), gprflw(n).para, n);

gpr_plot2D(gprdat(n-1))

%% save and quit
save([GPRproject_save GPRproject_prjname '.gprf'], 'gprflw');
save([GPRproject_save GPRproject_prjname '.gprd'], 'gprdat');

%% Plotting

% %% plot data including topo
% gprflw(n).name = 'gpr_plot2DTopo';
% gprflw(n).para.v        = 1.2 / 1e-9; % wavespeed for depth conversion
% gprflw(n).para.hightfct = .2;          % hight multiplier
% gprflw(n).para.thrshld = 0.02;
% [gprdat(n), n] = gpr_plot2DTopo(gprdat(n-1), gprflw(n).para, n);
% 
% %% plot in 3d
% gprflw(n).name = 'gpr_plot3D';
% gprflw(n).para.cfac = [-1 1];
% gprflw(n).para.az = 0;
% gprflw(n).para.el = 5;
% 
% 
% [gprdat(n), n] = gpr_plot3D(gprdat(n-1), gprflw(n).para, n);

% %% coherency filter filter
% % replaces trace by mean of neighbouring traces
% gprflw(n).name         = 'coherency filter';
% gprflw(n).para.n    = []; % no params at the moment
% 
% [gprdat(n) n] = gpr_cohfilt(gprdat(n-1), gprflw(n).para, n);
% 
% gpr_plot2D(gprdat(n-1))


%%
gprflw(n).name = 'Plotting'; 
gprflw(n).para.title = '';
gprflw(n).para.tti = tti;
gprflw(n).para.zdem = 0;   %if DEM is available, please add
gprflw(n).para.zff = 0;
gprflw(n).para.task = 0;   % just 0-2
gprflw(n).para.adj = 1;     % adj = 0/1:  Without/with adjustment of colormap
gprflw(n).para.vice = 0.1689;        % Ice velocity
gprflw(n).para.dri       = 3;             % Trace interpolation distance in migration
gprflw(n).para.cax = [-1.0 1.0];        %Lower and upper bounds of colorbar
%gprflw(n).para.t0_corr   = 330; % moving time in [ns]!
gprflw(n).para.t0_corr   = 300;
%gprflw(n).para.t0_corr   = gprdat.t0; % use time from header
gprflw(n).para.maxdepth = 400;       % Maximum depth [m] for displaying data
gprflw(n).para.tfun = zeros(100,1);  % exponential scaling factor for vertical axis

[gprdat(n) gprflw(n).para n] = gpr_SegyPlotData(gprdat(n-1), gprflw(n).para,n); 


