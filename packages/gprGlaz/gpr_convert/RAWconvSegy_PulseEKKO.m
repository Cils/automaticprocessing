function RAWconvSegy_PulseEKKO(area,instr,freq,offset,dewsoft)

% Input parameters are:
% area: code of suvey area
% instr: Type of instrument
% offset: Antennae separation
% Dewowsoft: Sofware used for Dewow filter

% Example: RAWconvSegy(7,'EKKO','50',4,'EkkoView')

% Organize the data as follows:
% Name the files with the survey area and date of data acquisition
% (area_yearmmdd) -> filedate; eg. ALLALIN_20120322
%   Folder 'matdata' contains the matlab files with the GPR raw data
%       Subfolder 'filedate_Segy' contains the produced SEG-Y files
%       Subfolder 'filedate_Plots' contains the produces jpg files


% -------------------------------------------------------------------------
% ----------------- PATH NAMES AND IDs ------------------------------------
% -------------------------------------------------------------------------
matdata = '/.../';  % path to folder matdata
ppRaw = '/.../';    % path to raw files (in REFLEX Format .00R or .DTZ)
GPSCoordFile = '/.../gpsfile.txt'; % processed GPS file 
filedate = '...';   % name of the SEG-Y file: area_yearmmdd
segyfolder = sprintf('%s%s_Segy/',matdata,filedate);

% -------------------------------------------------------------------------
% ----------------- START CONVERSION --------------------------------------
% -------------------------------------------------------------------------
c = 0;
for a = 1;  % for all raw files
    
    % ----------------- READ IN REFLEX RAW DATA ---------------------------
    ekko_file = sprintf('%sDLINE%d',ppRaw,a); % Dewowed files
    gps_file = sprintf('%sLINE%d',ppRaw,a);   % Original file
    [ntr,nsamp,sra,px,py,pz,freq,offset,data,tt] = ReadPulsEkko(ekko_file,gps_file);
    
    % ----------------- READ PROCESSED GPS DATA ---------------------------
    load(GPSCoordFile);
    tti = linspace(min(tt),max(tt),ntr);
    dt = (max(tt) - min(tt))/(ntr-1);
    xx = interp1(ttref,ee,tti); 
    yy = interp1(ttref,nn,tti); 
    zz = interp1(ttref,hh,tti);
    px = xx.';
    py = yy.';
    pz = zz.';
    
    % ----------------- SAVE AS .mat FILE ---------------------------------
    data_file = sprintf('%s%s_PROFILE_%03d.mat',matdata,filedate,a);
    save(data_file,'ntr','nsamp','sra','px','py','pz','data');
    
    % ----------------- DEFINE PROFILES ------------------
    data_file = sprintf('%s%s_PROFILE_%03d.mat',matdata,filedate,a);
    profile_file = sprintf('%s%s_%03d_RAW_PROF.mat',matdata,filedate,a);
    assign_file = sprintf('%s%s_%03d_RAW_ASSIGN.mat',matdata,filedate,a);

    % Produce a RAW_PROF.mat file and a RAW_ASSIGN.mat file
    SelectProfiles(data_file,profile_file);
    sprintf('Select Profiles for Profile_%03d is done',a)
    AssignProfiles(data_file,profile_file,assign_file);
    sprintf('Assign Profiles for Profile_%03d is done',a)

    % ----------------- SELECT DATA FROM DEFINED PROFILES -----------------
    load(profile_file);
    load(assign_file);
    
    for b = 1:nprof
        load(data_file);
        
        c = c + 1;  
        ii = find(profidx == b);
        data = data(:,ii);
        px = px(ii);
        py = py(ii);
        pz = pz(ii);
               
        zdem = pz(ii)
        profnr = c;          % Profile Number
        ntr = length(ii);    % Number of traces
        profinfo = prof(b);  % Segment points
        origpx = px;         % Original x points on profile line
        origpy = py;         % Original y points on profile line
        tti = zeros(ntr,1);  % Picked air traveltime
        zeff = zeros(ntr,1); % Ground elevation zeff = pz-(tti/2)*vair)
        
        
        % ----------------- Define date -----------------------------------
        date.y = 2013;                               % Year
        date.m = 2;                                  % Month
        date.d = 10;                                 % Day
        date.dj = juliandate(date.y,date.m,date.d);  % Day in Julian date
        date.hr = 0;                                 % Hour
        date.min = 0;                                % Minute
    
        
        % ----------------- PREPARE COMMENTS FOR TEXTUAL HEADER -----------
        comments = make_comments(area,instr,c,freq,offset,dewsoft,date,...
                                 ntr,sra,nsamp,0,profinfo,0,0,0,0);
                                 
        % ----------------- WRITE SEGY FILE -------------------------------
        segyname = sprintf('%s%s_RAW_PROFILE_%03d.sgy',segyfolder,filedate,c);
        GPR_write_segy(segyname,comments,offset,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date)
        sprintf('--- Write %s%s_RAW_PROFILE_%03d.sgy done ---',matdata,filedate,c)
        clear zdem
    end  
end

sprintf('Number of Profiles: %d',c)