function convertGPS_txt_mat(fname_in, fname_out);
% Read GPS coordinates from processed GPS.txt file

% Example: convertGPS('Allalin_20130210.txt', 'Allalin_20130210_GPS.mat')
% fname_in: filename of GPS file after processing the GPS data
% fname_out: filename of file containing parameters

% mm: month
% dd: day
% yy: year
% ee: easting
% nn: northing
% hh: height [m]
% acc: Accuracy of GPS data [m]

[mm,dd,yy,hour,min,sec,ee,nn,hh,acc] = textread(fname_in,'%*d_%*d %d/%d/%d %d:%d:%d %f %f %f %f %*s %*s %*s %*s %*s','headerlines',1,'commentstyle','matlab');
ttref = 3600 * hour + 60 * min + sec; % convert time to seconds
% Leica GPS measures in GMT, PulsEkko measures local time

% If wintertime: GPS time + -2h
ttref = ttref - 2*3600
% If sommertime: GPS time - 1h
% ttref = ttref - 3600;

[ttref,ii] = sort(ttref);
yy = yy(ii);
mm = mm(ii);
dd = dd(ii);
nn = nn(ii);
ee = ee(ii);
hh = hh(ii);
save(fname_out,'mm','dd','yy', 'ttref','ee','nn','hh', 'acc');
