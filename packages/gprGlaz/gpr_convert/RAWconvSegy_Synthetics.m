function RAWconvSegy_Synthetics

% Input parameters are:
% area: code of suvey area
% instr: Type of instrument
% freq: Central frequency (as a string parameter)
% offset: Antennae separation
% Dewowsoft: Sofware used for Dewow filter (string parameter, only one word!)
% firstprof: first profile number to be used (e.g., if the main loop does
%            not run over all raw data files) (optional parameter)

% Example: RAWconvSegy_GSSI(2,'GSSI','65',1.8,'NoDewow')


area = 0;
instr = 'GSSI';
freq = '65';
offset = 1.8;
dewsoft = 'NoDewow';

% firstprof = 1;


% -------------------------------------------------------------------------
% ----------------- PATH NAMES AND IDs ------------------------------------
% -------------------------------------------------------------------------
pp = '/Users/celialucas/Desktop/Masterthesis/Heli_radar_modelle/Glacier Models/';
ppRaw = '/Users/celialucas/Desktop/Masterthesis/Heli_radar_modelle/Glacier Models/RAW_synt/';
segyfolder = [pp 'SEGY_synt/'];  % path to folder for storing SEGY files
matdata = [pp 'matdata_synt/'];  % path to folder for storing *.mat files
tag = 'picked_model_20140304';

% -------------------------------------------------------------------------
% ----------------- START CONVERSION --------------------------------------
% -------------------------------------------------------------------------
if (exist('firstprof','var') == 1)
    c = firstprof - 1;
else
    c = 0;
end;
for a = 1  % for all (or selected) raw files
    
    % ----------------- READ IN Synthetical RAW DATA (.mat) ----------------------
    fname = sprintf('%s%s',ppRaw,tag);
    [ntr,nsamp,sra,px,py,pz,data,Create]=ReadData([fname '.mat']);
    
    % ----------------- Read coordinates ----------------------------------
%     fname_GPS = [fname '_CH03_NF02.csv'];
%     [x,y,z] = textread(fname_GPS,'%*d,%f,%f,%f','headerlines',1); %CH03_NF02.txt contains GPS positions
%     ntr = length(x);
%     data = data(:,1:ntr);
%     px = x;
%     py = y;
%     pz = z;
% 
%     % ----------------- OFFSET REMOVAL --------------------------------
%     datamedian = median(data,1);
%     data = data- repmat(datamedian,nsamp,1); 

%     % ----------------- DE-GAIN DATA ----------------------------------
%     [t0 HP LP GainPoints] = ReadGssiGain([fname '.TXT']);
%     if (length(GainPoints) > 1)
%         gloc = linspace(0,nsamp,length(GainPoints));
%         gainfkt = interp1(gloc,10.^(GainPoints./20),1:nsamp,'linear','extrap');
%         gainmat = repmat(gainfkt',1,size(data,2));
%         data = data./gainmat;
%     end;
%         
%     % ----------------- TIME-ZERO CORRECTION --------------------------
%     ns0 = round(-t0/sra);
%     data = data(ns0+1:end,:);
%                 
%     % ----------------- Include DEM values if existing ----------------
%     % Read here zdem information or initialize zdem
%     zdem = zeros(ntr,1);
        
    % ----------------- Initialize variables to be used later -------------
    tti = zeros(ntr,1);  % Picked air traveltime
    zeff = zeros(ntr,1); % Ground elevation zeff = pz-(tti/2)*vair)

    % ----------------- Raw file creation date -----------------------------
    ddate.y = Create.year;                                   % Year
    ddate.m = Create.month;                                  % Month
    ddate.d = Create.day;                                    % Day
    ddate.dj = date2jd(ddate.y,ddate.m,ddate.d);            % Julian day
%    ddate.dj = juliandate(ddate.y,Create.month,Create.day);  % Day in Julian date
    ddate.hr = Create.hour;                                  % Hour
    ddate.min = Create.min;                                  % Minute         
        
    % ----------------- DEFINE PROFILES -----------------------------------
%     [p,n,e] = fileparts(fname);
%     assign_file = [matdata n '_ASSIGN.mat'];
%     if (exist(assign_file,'file') ~= 2)
%         [profidx,profinfo,nprof] = SelectAssignProfiles(n,px,py);
%         save(assign_file,'profidx','profinfo','nprof');
%     else
%         load(assign_file);
%     end;
%             
%     % ----------------- Write SEGY file with profile data -----------------
%     for b = 1:nprof
%         c = c + 1;  
%         profinfo(b).profno = c;
%         ii = find(profidx == b);
%                 
        % ----------------- PREPARE COMMENTS FOR TEXTUAL HEADER -----------
        comments = make_comments(area,instr,c,freq,offset,dewsoft,ddate,...
                                 length(ii),sra,nsamp,0,~,0,0,0,0);

        % ----------------- WRITE SEGY FILE -------------------------------
        segyname = sprintf('%s%s_RAW_PROFILE_%03d.sgy',segyfolder,tag,c);
        GPR_write_segy(segyname, ...
                       comments, ...
                       offset, ...
                       sra, ...
                       px(ii),py(ii),pz(ii), ...
                       data(:,ii), ...
                       zdem(ii), ...
                       px(ii),py(ii), ...
                       tti(ii),zeff(ii),...
                       c,ddate);
        sprintf('--- %s created ---',segyname)
    end  
    save(assign_file,'profidx','profinfo','nprof');
end

sprintf('Total number of Profiles created: %d',c)