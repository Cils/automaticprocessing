function [dataz,dist,zz,zeff] = TimeToDepthConversion(data,sra,zdem,zeff,tti,procp);
%dataz: Depth converted data
%dist:  Distance along the profile line
%zz:    z-axis (altitude)
%zeff:  glacier surface elevation [m]
%tti:   picked TWT from glacier surface [ns]
%procp: processing parameters
%sra:   sampling rate

[nsamp ntr] = size(data);
dist = (0:ntr-1)*procp.dri;
dz = sra/2.0*procp.vice;
if sum(isnan(zdem)) == 0
    zeff = zeff - median(zeff-zdem);
else
    zeff = zeff - procp.GlobalZshift;
end;
maxalt = max(zeff) + 20;
minalt = min(zeff) - procp.maxdepth;
nz = round((maxalt - minalt)/dz);
zz = linspace(-maxalt,-minalt,nz);
dataz = zeros(nz,ntr);
itopo = round(tti/sra);

maxsamp = round(procp.maxdepth/dz);

for a = 1:ntr;
    [mv,mi] = min(abs(zz+zeff(a)));
    nins = nsamp - itopo(a);
    if (itopo(a) < 1), itopo(a) = 1; end;
    i3 = itopo(a);
    i4 = itopo(a)+maxsamp;
    i1 = mi;
    i2 = mi + maxsamp;
    if (i4 > nsamp)
        i4 = nsamp;
        i2 = mi+length(i3:i4)-1;
    end
    dataz(i1:i2,a) = data(i3:i4,a);
end
rsum = sum(abs(dataz),2);
ii = find(rsum > 1.0e-3);
zz = zz(ii(1):ii(end)-1);
dataz = dataz(ii(1):ii(end)-1,:);

end