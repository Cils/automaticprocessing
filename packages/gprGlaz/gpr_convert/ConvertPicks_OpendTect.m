function ConvertPicks_OpendTect

% -------------------------------------------------------------------------
% ---------------------- SET PATHS & FILEDATE -----------------------------
% -------------------------------------------------------------------------
pp_in = '...\';                %path to SEGY-Files to convert
pp_out = '...\';               %path to OpendTect (OdT) Project folder
pp_pickIn = '...\';            %path to existing picks

pp_pickOut = '...\';           %path to pick folder where bedrock picks are
%stored after picking in OdT
pickfileOdt = '...\xxx.txt';   %If picked in OpendTect choose the exported
%pickfile

filedate = 'AREA_yyyymmdd'; procp.GlobalZshift = -57; procp.dri = 1;

dd1 = dir(sprintf('%s%s_mig_PROFILE_*',pp_in,filedate)); clear DD
for C = 1:length(dd1);
    DD(C) = sscanf(dd1(C,1).name,[filedate '_RAW_PROFILE_%d']); end

% -------------------------------------------------------------------------
% ---------------------- DEFINE CONVERSION AND MAXALT ---------------------
% -------------------------------------------------------------------------
convertInput = 'Mat';   %Convert from Matlab picks to OdT
%convertInput = 'OdT';  %Convert from OdT to Matlab
maxAlt = 3400;          %Defined previously when converted SEG-Y files
%!!! Use same value here !!!
procp.maxdepth = 250;   %Maximum depth the data is displayed

procp.vair = 0.299;     %Air velocity
procp.vice = 0.1689;    %Ice velocity



% -------------------------------------------------------------------------
% ---------------------- START CONVERSION ---------------------------------
% -------------------------------------------------------------------------

xbed = [];
ybed = [];
zbed = [];
Lname = [];
tr_Nr = [];

for A = DD;
    disp(sprintf('Profile %d',A))
    segyfile = sprintf('%s%s_mig_PROFILE_%03d',pp_in,filedate,A);
    [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyfile);
    if sum(isnan(zdem)) == 0
        zeff = zeff - median(zeff-zdem);
    else
        zeff = zeff - procp.GlobalZshift;
    end;
    
    % ---------------------------------------------------------------------
    % ------------------- CONVERSTION: OPENDTECT TO MATLAB ----------------
    % ---------------------------------------------------------------------
    if strcmp(convertInput,'OdT') == 1;
        fid = fopen(pickfileOdt);
        formatSpec = '%s %f %f %d %f';
        CC = textscan(fid,formatSpec);
        profilenr = CC{1,1};
        xpick = CC{1,2};
        ypick = CC{1,3};
        trNr = CC{1,4};
        zbedpicki = CC{1,5};
        
        clear I xbed ybed zbed i
        
        I = find(strcmp(profilenr,sprintf('PROFILE_%d',profnr)) == 1);
        
        if isempty(I) == 0;
            ftrace = I(1);
            ltrace = I(1)+ntr-1;
            
            if length(ftrace:ltrace) ~= ntr; error('Check Picks'); end
            zbedi = zbedpicki(ftrace:ltrace);
            i3 = find(zbedi == 5000); %Undefined value = 5000
            zbedi(i3) = NaN;
            zbed = maxAlt-zbedi;
            ii = find(isnan(zbed)==0);
            zbed = zbed(ii);
            xbed = px(ii);
            ybed = py(ii);
            zsurf = zeff(ii);
            iweight = ones(size(zbed))*2;
            
        else
            zbed = [];
            disp('No Picks')
        end
        
        outputfile = sprintf('%s%s_PROFILE_%d_BEDROCK.txt',pp_pickOut,filedate,A);
        
        if isempty(zbed) == 0;
            fid = fopen(outputfile,'wt');
            for a = 1:length(xbed)
                fprintf(fid,'%10.3f %10.3f %10.3f %10.3f %10.3f %d\n',xbed(a),ybed(a),zbed(a),zsurf(a),zsurf(a)-zbed(a),iweight(a));
            end;
            fclose(fid);
        end
    end
    
    
    % ---------------------------------------------------------------------
    % ------------------- CONVERSTION: MATLAB TO OPENDTECT ----------------
    % ---------------------------------------------------------------------
    if strcmp(convertInput,'Mat') == 1;
        Input_pickfile = sprintf('%s%s_PROFILE_%d_BEDROCK.txt',pp_pickIn,filedate,A);
        if exist(Input_pickfile) == 0; continue; end
        C = load(Input_pickfile);
        xpick = C(:,1);
        ypick = C(:,2);
        zbedpicki = C(:,3);
        
        xypickCoord = sqrt(xpick.^2 + ypick.^2);
        pxpyCoord = sqrt(px.^2+py.^2);
        
        clear zbedi2 zbedi i2 ii
        for a = 1:ntr;
            i2 = find(xypickCoord == pxpyCoord(a));
            if length(i2) > 1; i2 = []; end
            if isempty(i2) == 1;
                zbedi2(a) = NaN;
            else
                zbedi2(a) = zbedpicki(i2);
            end
        end
        
        zbedi = maxAlt-zbedi2;
        ii = find(isnan(zbedi) == 1);
        zbedi(ii) = 5000;
        
        zbed = [zbed; zbedi'];
        xbed = [xbed; px'];
        ybed = [ybed; py'];
        tr_Nr = [tr_Nr; (1:ntr)'];
        Lname = [Lname; ones(ntr,1)*A];
    end
end

% -------------------------------------------------------------------------
% ------------------- SAVE PICKS IN .txt FILE -----------------------------
% -------------------------------------------------------------------------
if strcmp(convertInput,'Mat') == 1;
    outputfile = [pp_out filedate '_MatPicks.txt'];
    if isempty(zbed) == 0;
        fid = fopen(outputfile,'wt');
        for a = 1:length(xbed)
            fprintf(fid,'PROFILE_%d %10.3f %10.3f %d %10.3f\n',Lname(a),xbed(a),ybed(a),tr_Nr(a),zbed(a));
        end;
        fclose(fid);
    end
end
end

