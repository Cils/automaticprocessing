function data = tracenormalize(data)
%

nsamp = length(data(:,1));
ntr = length(data(1,:));

iquart = round(0.25*nsamp);
dtmp = sort(abs(data),1,'descend');
scfac = median(dtmp(1:iquart,:));
scfac(find(scfac == 0)) = 1;
for a = 1:ntr
    data(:,a) = data(:,a)/scfac(a);
end;
