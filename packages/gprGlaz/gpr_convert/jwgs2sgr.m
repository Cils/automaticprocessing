function [x,y,h] = jwgs2sgr(PHI,L,H)
%
% function [x,y,h] = jwgs2sgr(PHI,L,H)
% Transformation according to: 
% Bundesamt f�r Landestopographie, p.11 (accuracy ~1m)
% Bern: y = 600000, x = 200000
%
% Input ( -> output from jreadgga):
%   PHI latitude/northing  [decimal degrees]
%   L   longitude/easting  [decimal degrees]
%   H   orthometric height [m]
% Ouput:
%   x, y, z [m] swissgrid coordinates (x North, y East)
%
% Jan Walbrecker, 25.07.07
% ed. 08.10.07
% =========================================================================

% testparameter
% PHI = 46 + 2/60 + 38.87/3600;
% L = 8 + 43/60 + 49.79/3600;
% H = 650.6;

PHI = ((PHI*3600) - 169028.66)/10000;
L =   ((L*3600)   - 26782.5)  /10000;

y =   600072.37              ...
    + 211455.93 * L          ...
    -  10938.51 * L .* PHI    ...
    -     0.36  * L .* PHI.^2  ...
    -    44.54  * L.^3;
    
x =   200147.07                ...
    + 308807.95        * PHI   ...
    +   3745.25 * L.^2         ...
    +     76.63        .* PHI.^2 ...
    -    194.56 * L.^2 .* PHI    ...
    +    119.79        .* PHI.^3;
   
h =          H              ...
    -    49.55              ...
    +     2.73 * L          ...
    +     6.94     * PHI;

