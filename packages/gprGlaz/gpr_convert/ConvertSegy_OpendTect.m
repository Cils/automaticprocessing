function ConvertSegy_OpendTect

% -------------------------------------------------------------------------
% ---------------------- SET PATHS & FILEDATE -----------------------------
% -------------------------------------------------------------------------
SegyMat_path = '...\SegyMAT\';  %path to SegyMAT folder
pp_in = '...\';                %path to SEGY-Files to convert
pp_out = '...\';               %path to OpendTect Project folder

filedate = 'AREA_yyyymmdd'; procp.GlobalZshift = -57; procp.dri = 1;

dd1 = dir(sprintf('%s%s_mig_PROFILE_*',pp_in,filedate)); clear dd
for C = 1:length(dd1);
    dd(C) = sscanf(dd1(C,1).name,[filedate '_RAW_PROFILE_%d']); end

procp.vice = 0.1689;

% -------------------------------------------------------------------------
% ---------------------- DEFINE MAX/MIN ALTITUDE --------------------------
% -------------------------------------------------------------------------
maxdepth = 250; %Maximum depth the data is displayed
maxAlt = 3400;  %Maximum altitude of all profiles in this area
minAlt = 1800;  %Minimum depth o the displayed data (= minimum Altitude
                %of al profiles in this area - masdepth)

% -------------------------------------------------------------------------
% ---------------------- CONVERT DATA TO NEW SEG-Y FILES ------------------
% -------------------------------------------------------------------------
for a = dd
    disp(sprintf('Profile %d',a))
    
    inputsegy = sprintf('%s%s_mig_PROFILE_%03d',pp_in,filedate,a);
    outname = sprintf('%sPROFILE_%d.sgy',pp_out,a);
    [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(inputsegy);
    
    % ---------------------- COMPUTE NEW DATA MATRIX ----------------------
    if sum(isnan(zdem)) == 0
        zeff = zeff - median(zeff-zdem);
    else
        zeff = zeff - procp.GlobalZshift;
    end;
    
    dz = sra/2.0*procp.vice;
    nsamp_n = round((maxAlt-minAlt)/dz);
    data_n = ones(nsamp_n,ntr)*1e30;
    
    for b = 1:ntr;
        fs(b) = round(tti(b)/sra)+1;
        ls(b) = round(tti(b)/sra + maxdepth/dz)+1;
        
        if ls(b) > nsamp;
            ls(b) = nsamp;
        end
        
        fs_n(b) = round((maxAlt-zeff(b))/dz);
        ls_n(b) = fs_n(b) + (ls(b)-fs(b));
        
        data_n(fs_n(b):ls_n(b),b) = data(fs(b):ls(b),b);
    end
    
    data = data_n;
    
    
    % ------------------------ WRITE NEW SEGY FILE ------------------------
    addpath(genpath(SegyMat_path));
    
    WriteSegy(outname,data,'dt',dz/1000,'dsf',1,'revision',1); % IBM FLOATING POINT / SEG-Y Revision 1
    SCALAR = 1000;
    WriteSegyTraceHeaderValue(outname,1:ntr,'key','cdp');
    WriteSegyTraceHeaderValue(outname,px*SCALAR,'key','SourceX');
    WriteSegyTraceHeaderValue(outname,py*SCALAR,'key','SourceY');
    WriteSegyTraceHeaderValue(outname,px*SCALAR,'key','GroupX');
    WriteSegyTraceHeaderValue(outname,py*SCALAR,'key','GroupY');
    WriteSegyTraceHeaderValue(outname,px*SCALAR,'key','cdpX');
    WriteSegyTraceHeaderValue(outname,py*SCALAR,'key','cdpY');
    WriteSegyTraceHeaderValue(outname,zeff*SCALAR,'key','ReceiverDatumElevation');
    WriteSegyTraceHeaderValue(outname,zeff*SCALAR,'key','SourceDatumElevation');
    WriteSegyTraceHeaderValue(outname,-SCALAR,'key','ElevationScalar');
    WriteSegyTraceHeaderValue(outname,-SCALAR,'key','SourceGroupScalar');
    WriteSegyTraceHeaderValue(outname,1,'key','CoordinateUnits'); % meters
    
end
