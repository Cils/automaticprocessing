function RAWconvSegy_GSSI(area,instr,freq,offset,dewsoft,firstprof)

% Input parameters are:
% area: code of suvey area
% instr: Type of instrument
% freq: Central frequency (as a string parameter)
% offset: Antennae separation
% Dewowsoft: Sofware used for Dewow filter (string parameter, only one word!)
% firstprof: first profile number to be used (e.g., if the main loop does
%            not run over all raw data files) (optional parameter)

% Example: RAWconvSegy_GSSI(2,'GSSI','65',1.8,'NoDewow')


% -------------------------------------------------------------------------
% ----------------- PATH NAMES AND IDs ------------------------------------
% -------------------------------------------------------------------------
pp = 'd:/maurer/work/GSSI_Zermatt_20131203/';
ppRaw = 'D:/DATA/GlacierGPR/DataBase/GPR_Data_and_Results/RawGPRData/GSSI_Zermatt_20131203/';
segyfolder = [pp 'SEGY/'];  % path to folder for storing SEGY files
tag = 'ZERMATT_20131203';

% -------------------------------------------------------------------------
% ----------------- START CONVERSION --------------------------------------
% -------------------------------------------------------------------------
if (nargin >= 6)
    c = firstprof - 1;
else
    c = 0;
end;
for a = 1:27  % for all (or selected) raw files
    
    % ----------------- READ IN GSSI RAW DATA (.DZT) ----------------------
    fname = sprintf('%sFILE____%03d',ppRaw,a);
    [ntr,nsamp,sra,px,py,pz,data,Create]=ReadGssi([fname '.DZT']);
    
    % ----------------- Read coordinates ----------------------------------
    fname_GPS = [fname '_CH03_NF02.txt'];
    [x,y,z] = textread(fname_GPS,'%*d,%f,%f,%f','headerlines',1); %CH03_NF02.txt contains GPS positions
    ntr = length(x);
    data = data(:,1:ntr);
    px = x;
    py = y;
    pz = z;

    % ----------------- OFFSET REMOVAL --------------------------------
    datamedian = median(data,1);
    data = data- repmat(datamedian,nsamp,1); 

    % ----------------- DE-GAIN DATA ----------------------------------
    [t0 HP LP GainPoints] = ReadGssiGain([fname '.TXT']);
    gloc = linspace(0,nsamp,length(GainPoints));
    gainfkt = interp1(gloc,10.^(GainPoints./20),1:nsamp,'linear','extrap');
    gainmat = repmat(gainfkt',1,size(data,2));
    data = data./gainmat;
        
    % ----------------- TIME-ZERO CORRECTION --------------------------
    ns0 = round(-t0/sra);
    data = data(ns0+1:end,:);
                
    % ----------------- Include DEM values if existing ----------------
    % Read here zdem information
    if (exist('zdem','var') == 0), zdem = zeros(ntr,1); end                
        
    % ----------------- Initialize variables to be used later -------------
    tti = zeros(ntr,1);  % Picked air traveltime
    zeff = zeros(ntr,1); % Ground elevation zeff = pz-(tti/2)*vair)

    % ----------------- Raw file creation date -----------------------------
    ddate.y = Create.year;                                   % Year
    ddate.m = Create.month;                                  % Month
    ddate.d = Create.day;                                    % Day
    ddate.dj = juliandate(ddate.y,Create.month,Create.day);  % Day in Julian date
    ddate.hr = Create.hour;                                  % Hour
    ddate.min = Create.min;                                  % Minute         
        
    % ----------------- DEFINE PROFILES -----------------------------------
    assign_file = [pp fname '.ASI'];
    if (exist(assign_file,'file') ~= 2)
        [profidx,profinfo,nprof] = SelectAssignProfiles(px,py);
        save(assign_file,'profidx','prof','nprof');
    else
        load(assign_file);
    end;
            
    % ----------------- Write SEGY file with profile data -----------------
    for b = 1:nprof
        c = c + 1;  
        ii = find(profidx == b);
                
        % ----------------- PREPARE COMMENTS FOR TEXTUAL HEADER -----------
        comments = make_comments(area,instr,c,freq,offset,dewsoft,date,...
                                 length(ii),sra,nsamp,0,profinfo,0,0,0,0);

        % ----------------- WRITE SEGY FILE -------------------------------
        segyname = sprintf('%s%s_RAW_PROFILE_%03d.sgy',segyfolder,tag,c);
        GPR_write_segy(segyname, ...
                       comments, ...
                       offset, ...
                       sra, ...
                       px(ii),py(ii),pz(ii), ...
                       data(:,ii), ...
                       zdem(ii), ...
                       px(ii),py(ii), ...
                       tti(ii),zeff(ii),...
                       c,ddate);
        sprintf('--- %s created ---',segyname)
    end  
end

sprintf('Total number of Profiles created: %d',c)

