function [gprdat nn] = gpr_writevtk( indata,para, nn )
%GPR_WRITEVTK writes GPR data as paraview VTK file
%   call:
filename = [para.vtkname '.vtk'];

X = linspace(indata.pos0(1,1),indata.pos0(2,1),indata.nr_of_traces);
Y = linspace(indata.pos0(1,2),indata.pos0(2,2),indata.nr_of_traces);
nx = indata.nr_of_traces;
nz = indata.nr_of_samples;

xx = repmat(X,nz,1);
yy = repmat(Y,nz,1);
zz = repmat(indata.z',1,nx);
IND = find(isfinite(indata.data));

fid = fopen(filename, 'wt');
fprintf(fid, '# vtk DataFile Version 2.0\n');
fprintf(fid, 'Comment goes here\n');
fprintf(fid, 'ASCII\n');
fprintf(fid, '\n');
fprintf(fid, 'DATASET POLYDATA\n');
% fprintf(fid, '\n');
% fprintf(fid, 'ORIGIN    0.000   0.000   0.000\n');
% fprintf(fid, 'SPACING    1.000   1.000   1.000\n');
fprintf(fid, '\n');
fprintf(fid, 'POINTS   %d float\n', length(IND));
fprintf(fid, '%6.2f %5.2f %6.2f\n',[xx(IND) yy(IND) zz(IND)]');
fprintf(fid, 'POINT_DATA %i\n',length(IND));
fprintf(fid, 'SCALARS scalars double\n');
fprintf(fid, 'LOOKUP_TABLE default\n');
fprintf(fid, '\n');
fprintf(fid, '%d\n',indata.data(IND));
fclose(fid);

gprdat = indata;
if nargin > 2
    nn = nn +1;
end

end

