function data = datanormalize(data)

[nsamp ntr] = size(data);
data = reshape(data,ntr*nsamp,1);
ii = find(isnan(data) == 1);
data(ii) = 0;
dtmp = sort(abs(data));
scfac = dtmp(round(0.9*nsamp*ntr));
data = data/scfac;
data = reshape(data,nsamp,ntr);

