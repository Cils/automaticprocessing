function [xp,yp] = project_p(x,y,a,b);
% Project points xp, yp on the straight line of the segments.
% input for x and y are px and py
% input for a, b are cc(1) and cc(2)

% syms x y a b xp yp
% solve('(x-xp)*(0-xp)+(y-(a*xp+b))*(b-(a*xp+b))=0','xp')
xp = (x - a*b + a*y)/(a^2 + 1);
yp = a*xp + b;