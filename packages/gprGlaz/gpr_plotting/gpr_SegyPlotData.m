function [data, procp, n] = gpr_SegyPlotData(indata, para, n)

% task = 0: Plot traces on x-axis, time on y-axis
% task = 1: Plot distance on x-axis, time on y-axis
% task = 2: Plot distance on x-axis, (tti) adjusted time on y axis
% task = 3: Plot distance on x-axis, depth below surface on y axis
% task = 4: Plot distance on x-axis, altitude on y axis
% adj = 0/1:  Without/with adjustment of colormap

data = indata.data;
sra = indata.sampling_rate;
nsamp = indata.nr_of_samples;
procp = para;
ntr = indata.nr_of_traces;
tti = para.tti;
task = procp.task; 
adj = procp.adj;
load blkwhtred
Title = procp.title;

figure,

% -------------------------------------------------------------------------
% ------------------- TRACE NORMALIZATION ---------------------------------
% -------------------------------------------------------------------------
%data = tracenormalize(data);
data = gpr_datanormalize(data);

% -------------------------------------------------------------------------
% ------------------- PLOT DATA WITH TIME AXIS ----------------------------
% -------------------------------------------------------------------------
t = (0:nsamp-1)*sra;
t = t - procp.t0_corr;
dist = (0:ntr-1)*procp.dri;
load zcm.mat
load blkwhtred.mat

if (task == 0)
    xr = 1:ntr;
    yr = t;
    xlab = 'Trace number';
    ylab = 'Time [ns]';
    yd = 'reverse';
end

if (task == 1)
    xr = dist;
    yr = t;
    xlab = 'Distance [m]';
    ylab = 'Time [ns]';
    yd = 'reverse';
end

if (task == 2)
    data2 = zeros(size(data));
    for a = 1:ntr
        if (tti(a) > 0)
            i1 = round(tti(a)/sra);
            data2(1:nsamp-i1+1,a) = data(i1:end,a);
        else
            data2(:,a) = data(:,a);
        end;
    end;
    data = data2;
    xr = dist;
    yr = t;
    xlab = 'Distance [m]';
    ylab = 'Time [ns]';
    yd = 'reverse';
end

if (task == 3)
    data2 = zeros(size(data));
    for a = 1:ntr
        if (tti(a) > 0)
            i1 = round(tti(a)/sra);
            data2(1:nsamp-i1+1,a) = data(i1:end,a);
        else
            data2(:,a) = data(:,a);
        end;
    end;
    z = t/2.0*procp.vice;
    data = data2;
    xr = dist;
    yr = z;
    yd = 'reverse';
    xlab = 'Distance [m]';
    ylab = 'Depth [m]';
end

if (task == 4) % altitude plot
    % Correct zeff using zdem or procp.GlobalZshift
    if (sum(isnan(zdem)) == 0)
        zeff = zeff + median(zdem - zeff);
    else
        zeff = zeff + procp.GlobalZshift;
    end;
    
    % Define altitude range
    dz = sra/2.0*procp.vice;    
    maxalt = max(zeff) + 20;
    minalt = min(zeff) - procp.maxdepth;
    nz = round((maxalt - minalt)/dz);
    zz = linspace(-maxalt,-minalt,nz);
    
    dataz = zeros(nz,ntr);
    itopo = round(tti/sra);
    itopo(itopo < 1) = 1;
    
    for a = 1:ntr
        [~,mi] = min(abs(zz+zeff(a))); % index of surface altitude
        nins = nsamp - itopo(a); % No. of samples to be transferred from data
        i3 = itopo(a);
        i4 = itopo(a)+nins - 1;
        i1 = mi;
        i2 = mi + nins - 1;
        
        if (i2 > nz)
            i4 = i4 - (i2-nz);
            i2 = nz;
        end;
        dataz(i1:i2,a) = data(i3:i4,a);
    end
    
    % Trim z range of data
    rsum = sum(abs(dataz),2);
    ii = find(rsum > 1.0e-3);
    zz = zz(ii(1):ii(length(ii)));
    dataz = dataz(ii(1):ii(length(ii)),:);
    data = dataz;
    xr = dist;
    yr = abs(zz);
    xlab = 'Distance [m]';
    ylab = 'Altitude [m.a.s.l]';
    yd = 'normal';
end;    

% -------------------------------------------------------------------------
% ------------------- Plot and ADJUST COLORAXIS ------------------------------------
% -------------------------------------------------------------------------
button = 0;
dta = 5.0e-4;
dcax = 1.1;
datao = data;
if (adj == 1)
    disp('Choose colormap with 1/2')
    disp('Choose caxis with left/right')
    disp('Choose vertical scaling with up/down')
end;

while(button ~= 27)
    if (procp.tfun(task+1) ~= 0)
        for a = 1:length(dist)
            data(:,a) = datao(:,a) .* exp(-(1:length(yr))*procp.tfun(task+1))';
        end;
    end;
    imagesc(xr,yr,data);
    xlabel(xlab,'fontsize',12);
    ylabel(ylab,'fontsize',12);
    set(gca,'fontsize',12);
    title(Title,'fontsize', 12,'interpreter','none');
    colormap(blkwhtred);    
    caxis(procp.cax);
    freezeColors;
    set(gca,'YDir',yd);
    if (task == 3)
        axis([0 max(dist) 0 procp.maxdepth]);
    end;
    if ((task == 3) | (task == 4))
        set(gca,'dataaspectratio',[procp.zexag 1 1]);
    end;
    if (task == 4)
        hold on;
        plot(dist,zeff,'k-','linewidth',1.5); 
        hold off;
    end;
    
    if (adj == 0), break; end;
        
    [xx,yy,button] = ginput(1);
    if (button == 28), procp.cax = procp.cax/dcax; end;
    if (button == 29), procp.cax = procp.cax*dcax; end;
    if (button == 49), procp.cmap = zcm; end;
    if (button == 50), procp.cmap = blkwhtred; end;                
    if (button == 30), procp.tfun(task+1) = procp.tfun(task+1) + dta; end;
    if (button == 31), procp.tfun(task+1) = procp.tfun(task+1) - dta; end;

end;        


if nargin > 2
    n = n +1;
end
