function SegyPlotRaw(nsamp,sra,ntr,data,hh,mm,ss)


% -------------------------------------------------------------------------
% ------------------- TRACE NORMALIZATION ---------------------------------
% -------------------------------------------------------------------------
%data = tracenormalize(data);
data = datanormalize(data);

%determine if time or trace plot
time=0
if exist('hh'),
    time = 1;
    interval = 1000; %tickinterval in samples
    sod = hh*3600+mm*60+ss; %sod = seconds of day
    range = sod(length(sod))-sod(1) % in seconds
    nr_ticks = floor(length(sod)/interval); % determine number of ticks in timeplot
    times = num2str([hh,mm,ss],'%02d'); a = repmat(':',length(times),1); %create string array with times
    times = [times(:,1:2),a,times(:,3:4),a,times(:,5:6)];   %create string array with times
    times = cellstr(times); %create string array with times
    timeticks = cell(1,nr_ticks);
    for i=1:length(timeticks)
        timeticks(1,i) = times(i*interval);
    end    
end    


% -------------------------------------------------------------------------
% ------------------- PLOT DATA WITH TIME AXIS ----------------------------
% -------------------------------------------------------------------------
t = (0:nsamp-1)*sra;
load zcm.mat
load blkwhtred.mat

xr = 1:ntr;
yr = t;
xlab = 'Trace number';
ylab = 'Time [ns]';
yd = 'reverse';
%tfun = zeros(100,1);
tfun = 0;

    
  

% -------------------------------------------------------------------------
% ------------------- Plot and ADJUST COLORAXIS ------------------------------------
% -------------------------------------------------------------------------
button = 0;
dta = 5.0e-4;
dcax = 1.1;
cax = [-1,1];
datao = data;
Title = 'Choose colormap with 1/2, caxis with left/right, vertical scaling with up/down'; 

while(button ~= 27)
    if (tfun ~= 0)
        for a = 1:ntr
            data(:,a) = datao(:,a) .* exp(-(1:length(yr))*tfun)';
        end;
    end;
    imagesc(xr,yr,data);
    if (time == 1)
        L = get(gca,'XLim'); % determine limits on x axis
        set(gca,'Xtick',linspace(L(1),L(2),nr_ticks)); %determine new ticks
        set(gca,'Xticklabel',timeticks); %label ticks with hh:mm
        xlab = 'Time hh:mm:ss';
    end    
    xlabel(xlab,'fontsize',12);
    ylabel(ylab,'fontsize',12);
    set(gca,'fontsize',12);
    title(Title,'fontsize', 12,'interpreter','none');
    colormap(blkwhtred);    
    caxis(cax);
    freezeColors;
    set(gca,'YDir',yd);
   
    
    
    [xx,yy,button] = ginput(1);
    if (button == 28), cax = cax/dcax; end;
    if (button == 29), cax = cax*dcax; end;
    if (button == 49), cmap = zcm; end;
    if (button == 50), cmap = blkwhtred; end;                
    if (button == 30), tfun = tfun + dta; end;
    if (button == 31), tfun = tfun - dta; end;
    if (button == 27), continue; end;
end;        


