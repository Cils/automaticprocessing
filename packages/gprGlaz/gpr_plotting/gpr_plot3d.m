function [ gprdat nn ] = gpr_plot3d(indat,para, nn)
% plots data in corect geospatial position

gprdat = indat;




x = linspace(indat.pos0(1,1),indat.pos0(2,1),indat.nr_of_traces);
y = linspace(indat.pos0(2,1),indat.pos0(2,1),indat.nr_of_traces);
% z = indat.z;

zr = repmat(indat.dataz',1,indat.nr_of_traces);
xx = repmat(x,indat.nr_of_samples,1);
yy = repmat(y,indat.nr_of_samples,1);
figure
load zcm;
colormap (zcm);
surf(xx,yy,zr,indat.data,'edgecolor','none');hold on;

load zcm;
colormap (zcm);

caxis(para.cfac);
view(para.az,para.el)
set(gca,'DataAspectRatio',[1 1 1]);

if nargin > 2
    nn = nn +1;
end

end

