function [gprdat nn] = gpr_plot2DTopo(indata, para, nn)

if ~isfield(para, 'flipdir')
    para.flipdir = 0;
end
if ~isfield(para, 'shpname')
    para.shpname = 'myshape';
end
if ~isfield(para, 'thrshld')
    para.thrshld = 0;
end

gprdat = indata;
dt     = indata.sampling_rate * 1e-9;
dz     = dt * para.v / 2 * para.hightfct; % !! should be 2
Dz     = dz * indata.nr_of_samples;
z      = dz:dz:Dz;
ztopo  = max(indata.z)-min(indata.z);
Z      = Dz + ztopo;
gprdat.dataz  = 0:dz:Z;

gprdat.data = zeros(length(gprdat.dataz), length(indata.x));
gprdat.data = gprdat.data/0;
for n = 1:length(indata.x)
    z1  = max(indata.z) - indata.z(n);
    dz1 = round(z1/dz)+1;
    dz2 = dz1 + indata.nr_of_samples - 1;
    gprdat.data(dz1:dz2,n) = indata.data(:,n);
end

gprdat.dataz = fliplr(gprdat.dataz + min(indata.z)-Dz);
gprdat.nr_of_samples = length(gprdat.dataz);

if para.flipdir
    gprdat.data = fliplr(gprdat.data);
    gprdat.flip = 1;
end

if para.thrshld
    gprdat.data(abs(gprdat.data) < para.thrshld) = NaN;
end

figure
load rdwhitblu
pcolor(indata.x, gprdat.dataz, gprdat.data);  shading interp;
caxis(para.cfac)
% axis ij
xlabel('X Distance (m)')
ylabel('Elevation (m)')
axis  tight
% if isfield(para,'xlim')
%     xlim(para.xlim)
% end
% if isfield(para,'ylim')
%     ylim(para.ylim)
% end

 axis tight

colormap(red_white_blue)


if nargin> 2 && nargout == 2
    nn = nn +1;
end