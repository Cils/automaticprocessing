function [gprdat nn] = gpr_plot2Dshp(indata, para, nn)
%
% if ~isfield(para, 'flipdir')
%     para.flipdir = 0;
% end
if ~isfield(para, 'shpname')
    para.shpname = 'myshape';
end

gprdat = indata;

if gprdat.flip
    x0 = gprdat.pos0(2,1);
    y0 = gprdat.pos0(2,2);
else
    x0 = gprdat.pos0(1,1);
    y0 = gprdat.pos0(1,2);
end

gprdat = indata;
dx = gprdat.x(2)-gprdat.x(1);
nitem = 1;
h = waitbar(0,'Writing shapefile...');
for ntrace = 1:gprdat.nr_of_traces-1;
    for nsample = 1:gprdat.nr_of_samples-1;
        if ~isnan(gprdat.data(nsample, ntrace)) && abs(gprdat.data(nsample, ntrace)) > para.thrshld
            gstruct(nitem).Geometry = 'PolygonZ';
            gstruct(nitem).X = [...
                x0+gprdat.x(ntrace)*cos(gprdat.profdir(1))...
                x0+(gprdat.x(ntrace)+dx)*cos(gprdat.profdir(1))...
                x0+(gprdat.x(ntrace)+dx)*cos(gprdat.profdir(1))...
                x0+gprdat.x(ntrace)*cos(gprdat.profdir(1))...
                x0+gprdat.x(ntrace)*cos(gprdat.profdir(1))];
            gstruct(nitem).Y = [...
                y0+gprdat.x(ntrace)*sin(gprdat.profdir(1))...
                y0+(gprdat.x(ntrace)+dx)*sin(gprdat.profdir(1))...
                y0+(gprdat.x(ntrace)+dx)*sin(gprdat.profdir(1))...
                y0+gprdat.x(ntrace)*sin(gprdat.profdir(1))...
                y0+gprdat.x(ntrace)*sin(gprdat.profdir(1))];
            gstruct(nitem).Z = [...
                gprdat.z(nsample) ...
                gprdat.z(nsample) ...
                gprdat.z(nsample+1) ...
                gprdat.z(nsample+1) ...
                gprdat.z(nsample)];
            gstruct(nitem).OBJECTID = 'patch';
            gstruct(nitem).OBJECTVAL = gprdat.data(nsample, ntrace);
            %         gprdat.data(nsample, ntrace)
            nitem = nitem + 1;
        end
    end
    waitbar(ntrace/(gprdat.nr_of_traces-1))
end
close(h)
shapewritez(gstruct, strcat(para.shpname, '.shp'))

% figure
% load rdwhitblu
% pcolor(indata.x, fliplr(gprdat.z), gprdat.data); shading flat; shading interp;
% caxis([-1 1])
%  %axis ij
% xlabel('Distance (m)')
% ylabel('Elevation (m)')
% axis equal
% axis tight
% colormap(red_white_blue)


if nargin > 2
    nn = nn +1;
end