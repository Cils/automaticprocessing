function [  ] = gpr_plot2D(indata,pfig)
%GPR_PLOT2D plots 2D profile 

%% use the following comments in the wrapper file to run gpr_plot2D
% pfig.comap = 'ber';                 %blue,egg,red
% pfig.mode = 2;                      % 1 = ns, 2 = m
% pfig.velo = 0.1689;                 % depth in m ice velocity = 0.1689 m/ns
% pfig.min = -0.025;
% pfig.max = 0.025;
% gpr_plot2D(gprdat(n-1),pfig)
%%

dt    = indata.sampling_rate ;
ntr = indata.nr_of_traces;
ns = indata.nr_of_samples;
tt = (0:ns-1)*dt;           % in ns
depth = tt/2*pfig.velo;        % depth in m ice velocity = 0.1689 m/ns
xx = 1:ntr;

% colormap
color = load (pfig.comap);
color = struct2cell(color);
color = cell2mat(color);

% axis
figmin = pfig.min;
figmax = pfig.max;

%% y axis = time in ns
if pfig.mode == 1
    figure
    pdist = sqrt((indata.x-indata.x(1)).^2+(indata.y-indata.y(1)).^2);
    if isempty(indata.x) || isempty(indata.z)
        pcolor(xx,tt,indata.data)
        shading flat; shading interp;
        colormap(color)
        caxis([figmin figmax])
        xlabel('trace nr')
        ylabel('t [ns]')                             
        axis tight ij
    elseif isempty(indata.dataz) || isempty(indata.z)
        pcolor(indata.x-min(indata.x),tt,indata.data)
        shading flat; shading interp;
        colormap(color)
        axis tight ij
        caxis([figmin figmax])
        axis tight ij
        xlabel('x [m]')
        ylabel('t [ns]')                             
    else
        pcolor(pdist,indata.dataz,indata.data)
        shading flat; shading interp;
        colormap(color)
        caxis([figmin figmax])
        axis tight equal
        xlabel('x [m]')
        ylabel('t [ns]')
    end
end

%% y axis = depth in m
if pfig.mode == 2
   figure
    pdist = sqrt((indata.x-indata.x(1)).^2+(indata.y-indata.y(1)).^2);
    if isempty(indata.x) || isempty(indata.z)
        pcolor(xx,depth,indata.data)
        shading flat; shading interp;
        colormap(color)
        caxis([figmin figmax])
        xlabel('trace nr')
        ylabel('depth [m]')                             %'t [ns]')
        axis tight ij
    elseif isempty(indata.dataz) || isempty(indata.z)
        pcolor(indata.x-min(indata.x),depth,indata.data)
        shading flat; shading interp;
        colormap(color)
        caxis([figmin figmax])
        axis tight ij
                caxis([figmin figmax])
        xlabel('x [m]')
        ylabel('depth [m]')                             %'t [ns]')
    else
%         pcolor(pdist,indata.dataz,indata.data)                
%         shading flat; shading interp;
%         %     colormap(blkwhtred)
%         colormap(ber)
%         %     caxis([-0.1 0.1])
%         axis tight equal
%         xlabel('x [m]')
%         ylabel('t [ns]')
    end 
end






end

