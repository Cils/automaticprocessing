function showmap(fname)
%

load(fname);

nx = length(mapdata(1,:));
ny = length(mapdata(:,1));

x = x1 + (0:nx-1)*h;
y = y1 + (0:ny-1)*h;
x = x/1000;
y = y/1000;
mapdata = flipud(mapdata);

image(x,y,mapdata);
axis xy;
axis equal;
axis tight;
colormap(cm);