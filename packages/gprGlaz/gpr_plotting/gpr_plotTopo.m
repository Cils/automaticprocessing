function gpr_plot2DTopo(indata, flipdir)

if nargin < 2
    flipdir = 0;
end

gprv  = 0.1 / 1e-9;
dt    = indata.sampling_rate * 1e-9;
dz    = dt * gprv / 0.5; % !! should be 2
Dz    = dz * indata.nr_of_samples;
z     = dz:dz:Dz;
ztopo = max(indata.z)-min(indata.z);
Z     = Dz + ztopo;
gprdat.z  = 0:dz:Z;

gprdat.dataz = zeros(length(gprdat.z), length(indata.x));
gprdat.dataz = gprdat.dataz/0;
for n = 1:length(indata.x)
    z1  = max(indata.z)-indata.z(n);
    dz1 = round(z1/dz)+1;
    dz2 = dz1 + size(indata.data,1)-1;
    gprdat.dataz(dz2:-1:dz1,n) = indata.data(:,n);
end

gprdat.z = gprdat.z + min(indata.z);

if flipdir
    indata.x = fliplr(indata.x);
end


figure
load rdwhitblu
pcolor(indata.x,gprdat.z,gprdat.dataz); colormap bone, shading flat; shading interp;
caxis([-1 1])
axis equal; axis tight
colormap(red_white_blue)