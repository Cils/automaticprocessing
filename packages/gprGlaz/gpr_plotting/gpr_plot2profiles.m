% work in progress = no garantee that it works properly
% plotting with adjustable color scale

% task = 0: Plot traces on x-axis, time on y-axis
% task = 1: Plot distance on x-axis, time on y-axis
% task = 3: Plot distance on x-axis, depth below surface on y axis
% adj = 0/1:  Without/with adjustment of colormap

%%
clear all
close all
clc

addpath(genpath('D:\GPR\Projects'));

%% load first data set: dataa

dataa_path = 'D:\GPR\Projects\Otemma_20141017\Otemma_20141017_proc\GROUND_EKKO\proc\';

cd(dataa_path);
dataa = load('gla_fu_12_proc.gprd','-mat');


a = length(dataa.gprdat);
% figure,imagesc(dataa.gprdat(a).data),load blkwhtred,colormap(blkwhtred)

%% load second data set: datab

datab_path = 'D:\GPR\Projects\Otemma_20141017\Otemma_20141017_proc\GROUND_EKKO\proc\';

cd(datab_path);
datab = load('DLINE_26_31.gprd','-mat');


b = length(datab.gprdat);
% figure,imagesc(datab.gprdat(b).data),load blkwhtred,colormap(blkwhtred)

%% parameters

task    = 3;
figtitle = '';
Titlea   = 'Ground Across glacier profile';
Titleb   = 'Ground Along glacier profile';
axislimit = [0,2000,0,4500];
%tti     = 0;
adj     = 1;            % adj = 0/1:  Without/with adjustment of colormap
vice    = 0.1689;       % Ice velocity
dri     = 0.5;          % Trace interpolation distance in migration
cax     = [-0.75 0.75];   %Lower and upper bounds of colorbar
t0_corr = 300;          % moving time in [ns]! gprdat.t0; % use time from header
maxdepth = 350;         % Maximum depth [m] for displaying data
tfun    = zeros(100,1); % exponential scaling factor for vertical axis

%% plot of dataa

a = length(dataa.gprdat);
b = length(datab.gprdat);



% ta = (0:4000-1)*dataa.gprdat(a).sampling_rate;
ta = (0:dataa.gprdat(a).nr_of_samples-1)*dataa.gprdat(a).sampling_rate;
%tb = (0:4000-1)*datab.gprdat(b).sampling_rate;
tb = (0:datab.gprdat(b).nr_of_samples-1)*datab.gprdat(b).sampling_rate;
%t = (0:nsamp-1)*sra;


dista = sqrt((dataa.gprdat(a).x-dataa.gprdat(a).x(1)).^2+(dataa.gprdat(a).y-dataa.gprdat(a).y(1)).^2);
distb = sqrt((datab.gprdat(b).x-datab.gprdat(b).x(1)).^2+(datab.gprdat(b).y-datab.gprdat(b).y(1)).^2);
%dist = (0:ntr-1)*procp.dri;

load zcm.mat
load blkwhtred.mat

%% 1st plot settings
%% x-axis number of number of traces, y-axis time
if (task == 0)
    xr = 1:dataa.gprdat(a).nr_of_traces;
    yr = ta;
    xla = 'Trace number';
    yla = 'Time [ns]';
    yd = 'reverse';
end

%% x-axis number distance, y-axis  time
if (task == 1)
    xr = dista;
    yr = ta;
    xla = 'Distance [m]';
    yla = 'Time [ns]';
    yd = 'reverse';
end





%% x-axis distance, y-axis depth in m
% Surface picking
[i n]= size(dataa.gprdat);
gprflw(n).name = 'pick_surface';
gprflw(n).para.tax  = 1200;          %Lower limitation of the time axis for picking

[tti] = gpr_SegyPickSurface(dataa.gprdat(n), gprflw(n).para, n);

if (task == 3)
   data2 = zeros(size(dataa.gprdat(a).data));
   for j = 1:dataa.gprdat(a).nr_of_traces
       if (tti(j) > 0)
           i = round(tti(j)/dataa.gprdat(a).sampling_rate);
           data2(1:dataa.gprdat(a).nr_of_samples-i+1,j) = dataa.gprdat(a).data(i:end,j);
       else
           data2(:,j) = dataa.gprdat(a).data(:,j);
       end;
   end;
   z = ta/2.0*vice;
   dataa.gprdat(a).data = data2;
   xr = dista;
   yr = z;
   yd = 'reverse';
   xla = 'Distance [m]';
   yla = 'Depth [m]';
end


%% 2nd plot settings
%% x-axis number of number of traces, y-axis time
if (task == 0)
    xb = 1:datab.gprdat(b).nr_of_traces;
    yb = tb;
    xlb = 'Trace number';
    ylb = 'Time [ns]';
    yd = 'reverse';
end

%% x-axis number distance, y-axis  time
if (task == 1)
    xb = distb;
    yb = tb;
    xlb = 'Distance [m]';
    ylb = 'Time [ns]';
    yd = 'reverse';
end

%% x-axis distance, y-axis depth in m

[i n]= size(datab.gprdat);
gprflw(n).name = 'pick_surface';
gprflw(n).para.tax  = 1200;          %Lower limitation of the time axis for picking

[ttj] = gpr_SegyPickSurface(datab.gprdat(n), gprflw(n).para, n);

if (task == 3)
   data2 = zeros(size(datab.gprdat(b).data));
   for j = 1:datab.gprdat(b).nr_of_traces
       if (ttj(j) > 0)
           i = round(ttj(j)/datab.gprdat(b).sampling_rate);
           data2(1:datab.gprdat(b).nr_of_samples-i+1,j) = datab.gprdat(b).data(i:end,j);
       else
           data2(:,j) = datab.gprdat(b).data(:,j);
       end;
   end;
   z = tb/2.0*vice;
   datab.gprdat(b).data = data2;
   xb = distb;
   yb = z;
   yd = 'reverse';
   xlb = 'Distance [m]';
   ylb = 'Depth [m]';
end


%% both plots with same scaling
% -------------------------------------------------------------------------
% ------------------- Plot and ADJUST COLORAXIS ------------------------------------
% -------------------------------------------------------------------------
% button = 0;
% dta = 5.0e-4;
% dcax = 1.1;
% dataao = dataa.gprdat(a).data;
% databo = datab.gprdat(b).data;
% if (adj == 1)
%     disp('Choose colormap with 1/2')
%     disp('Choose caxis with left/right')
%     disp('Choose vertical scaling with up/down')
% end;

% %% both graphs same parameterization
% 
% while(button ~= 27)
%     if (tfun(task+1) ~= 0)
%         for i = 1:length(dista)
%             dataa.gprdat(a).data(:,i) = dataao(:,i) .* exp(-(1:length(yr))*tfun(task+1))';
%         end;
%         for i = 1:length(distb)
%             datab.gprdat(b).data(:,i) = databo(:,i) .* exp(-(1:length(yb))*tfun(task+1))';
%         end;
%     end;
%     
%     subplot(2,1,1)
%     imagesc(xr,yr,dataa.gprdat(a).data);    
%     %axis(axislimit)
%     xlabel(xla,'fontsize',12);
%     ylabel(yla,'fontsize',12);
%     set(gca,'fontsize',12);
%     title(Title,'fontsize', 12,'interpreter','none');
%     colormap(blkwhtred);
%     caxis(cax);
%     freezeColors;
%     set(gca,'YDir',yd);
%     
%     subplot(2,1,2, 'align')
%     imagesc(xb,yb,datab.gprdat(b).data);
%     %axis(axislimit)
%     xlabel(xlb,'fontsize',12);
%     ylabel(ylb,'fontsize',12);
%     set(gca,'fontsize',12);
%     title(Title,'fontsize', 12,'interpreter','none');
%     colormap(blkwhtred);
%     caxis(cax);
%     freezeColors;
%     set(gca,'YDir',yd);
%     
%     %    if (task == 3)
%     %        axis([0 max(dist) 0 procp.maxdepth]);
%     %    end;
%     %    if ((task == 3) | (task == 4))
%     %        set(gca,'dataaspectratio',[procp.zexag 1 1]);
%     %    end;
%     %    if (task == 4)
%     %        hold on;
%     %        plot(dist,zeff,'k-','linewidth',1.5);
%     %        hold off;
%     %    end;
%     %
%     %    if (adj == 0), break; end;
%     %
%     [xx,yy,button] = ginput(1);
%     if (button == 28), cax = cax/dcax; end;
%     if (button == 29), cax = cax*dcax; end;
%     if (button == 49), cmap = zcm; end;
%     if (button == 50), cmap = blkwhtred; end;
%     if (button == 30), tfun(task+1) = tfun(task+1) + dta; end;
%     if (button == 31), tfun(task+1) = tfun(task+1) - dta; end;
% end;


%% 1st plot scaling

% annotation(figure,'textbox',...
%     [0.409374999999999 0.934183824854032 0.210416666666667 0.0568458783261113],...
%     'String',{figtitle},...
%     'HorizontalAlignment','center',...
%     'FontSize',16,...
%     'FontName','Tahoma',...
%     'FitBoxToText','off',...
%     'EdgeColor','none');

button = 0;
dta = 5.0e-4;
dcax = 1.1;
dataao = dataa.gprdat(a).data;
if (adj == 1)
    disp('Choose colormap with 1/2')
    disp('Choose caxis with left/right')
    disp('Choose vertical scaling with up/down')
end;

% figure setup

while(button ~= 27)
    if (tfun(task+1) ~= 0)
        for i = 1:length(dista)
            dataa.gprdat(a).data(:,i) = dataao(:,i) .* exp(-(1:length(yr))*tfun(task+1))';
        end;
    end;
    
    subplot(2,1,1)
    imagesc(xr,yr,dataa.gprdat(a).data);
%     axis(axislimit) 
%     YTick = [0 100 200 300];
%     YMinorTick = 'on';
    %set(gca,'TickDir','out')
    xlabel(xla,'fontsize',16,'FontName','Tahoma');
    ylabel(yla,'fontsize',16,'FontName','Tahoma');
    %set(gca,'fontsize',12);
    title(Titlea,'fontsize', 16,'interpreter','none','FontName','Tahoma');
    colormap(blkwhtred);
    caxis(cax);
    freezeColors;
    set(gca,'YDir',yd);
    set(gca,'YDir',yd);
    set(gcf, 'PaperUnits', 'centimeters');
    set(gcf, 'PaperType', 'A4');
    set(gcf, 'PaperOrientation', 'portrait');
    %set(gcf, 'PaperOrientation', 'landscape');
    
%    yend = max(dista)-170;
       if (task == 3)
%            axis([300 yend 0 maxdepth]);
           axis([0 max(dista) 0 maxdepth]);
       end;
%        if ((task == 3) | (task == 4))
    %        set(gca,'dataaspectratio',[procp.zexag 1 1]);
    %    end;
    %    if (task == 4)
    %        hold on;
    %        plot(dist,zeff,'k-','linewidth',1.5);
    %        hold off;
    %    end;
    %
    %    if (adj == 0), break; end;
    %
    [xx,yy,button] = ginput(1);
    if (button == 28), cax = cax/dcax; end;
    if (button == 29), cax = cax*dcax; end;
    if (button == 49), cmap = zcm; end;
    if (button == 50), cmap = blkwhtred; end;
    if (button == 30), tfun(task+1) = tfun(task+1) + dta; end;
    if (button == 31), tfun(task+1) = tfun(task+1) - dta; end;
end;

%% second plot scaling

button = 0;
dta = 5.0e-4;
dcax = 1.1;
databo = datab.gprdat(b).data;
if (adj == 1)
    disp('Choose colormap with 1/2')
    disp('Choose caxis with left/right')
    disp('Choose vertical scaling with up/down')
end;

while(button ~= 27)
    if (tfun(task+1) ~= 0)
        for i = 1:length(distb)
            datab.gprdat(b).data(:,i) = databo(:,i) .* exp(-(1:length(yb))*tfun(task+1))';
        end;
    end;
    
    subplot(2,1,2, 'align')
    imagesc(xb,yb,datab.gprdat(b).data);
    %axis(axislimit)
    xlabel(xlb,'fontsize',14,'FontName','Tahoma');
    ylabel(ylb,'fontsize',14,'FontName','Tahoma');
%     set(gca,'fontsize',12);
    title(Titleb,'fontsize',16,'interpreter','none','FontName','Tahoma');
    colormap(blkwhtred);
    caxis(cax);
    freezeColors;
    set(gca,'YDir',yd);
    
%      yend = max(distb)-300;
       if (task == 3)
%           axis([1700 yend 0 maxdepth]);
           axis([0 max(distb) 0 maxdepth]);
       end;
    %    if ((task == 3) | (task == 4))
    %        set(gca,'dataaspectratio',[procp.zexag 1 1]);
    %    end;
    %    if (task == 4)
    %        hold on;
    %        plot(dist,zeff,'k-','linewidth',1.5);
    %        hold off;
    %    end;
    %
    %    if (adj == 0), break; end;
    %
    [xx,yy,button] = ginput(1);
    if (button == 28), cax = cax/dcax; end;
    if (button == 29), cax = cax*dcax; end;
   
    if (button == 49), cmap = zcm; end;
    if (button == 50), cmap = blkwhtred; end;
    if (button == 30), tfun(task+1) = tfun(task+1) + dta; end;
    if (button == 31), tfun(task+1) = tfun(task+1) - dta; end;
end;

%print('-dtiffn','D:\GPR\Projects\Maps\Paper Fig\Ground_across.tiff')
 print('-dsvg','D:\GPR\Projects\Maps\Paper Fig\Heli_across_2.svg')


