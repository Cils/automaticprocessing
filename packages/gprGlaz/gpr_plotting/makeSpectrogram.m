function makeSpectrogram(data,tracenr,sra,fmin,fmax)
% S: Short time fft
% F: Frequencies in Hz where fft is analysed
% T: Vector of times at which spectrogram was computed
% P: Power spectral density of each segment
% [S F T P] = spectrogram(x,window,noverlap,F,sampling rate)


% ------------ SPECTROGRAM ------------
x = data(:,tracenr);
F = [fmin:10:fmax];
fs = 1/sra;
[S,F,T,P]=spectrogram(x,256,250,F,fs);

figure(3); clf
imagesc(T,F,log(abs(P)))
xlabel('Time [ns]','fontsize',12)
ylabel('Frequency [MHz]','fontsize',12)
h = colorbar;
set(get(h,'title'),'String','log(P)','fontsize',12);
title('Spectrogram', 'fontsize',12);
set(gca,'fontsize', 12);


figure(4); clf;
    subplot(4,1,1)
    plot(data(:,tracenr))
    xlabel('TWT [ns]')
    ylabel('Amplitude')
    title('Trace Spectrum')

    subplot(4,1,2:4)
    imagesc(T,F,log(abs(P)))
    xlabel('TWT [ns]')
    ylabel('Frequency [Hz]')
    h = colorbar;
    set(get(h,'title'),'String','log(P)')
    title(sprintf('Spectrogram trace %d',tracenr))

