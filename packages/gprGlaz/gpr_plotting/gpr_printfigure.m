function [] = gpr_printfigure( indata, para )
%GPR_PRINTFIGURE prints figure in desired format
% EXEMPLE   
% gprflw(n).name = 'export_figure';
% gprflw(n).para.papertype = 'A3'; % A4, A3
% gprflw(n).para.size = [42 29.7]; % A4 [29.7 21], A3[42 29.7] 
% gprflw(n).para.orientation = 'landscape'; % portrait / landscape
% gprflw(n).para.fontsize = 12;
% gprflw(n).para.xlabel = 'mMap x [m]';
% gprflw(n).para.ylabel = 'Elevation [m]';
% gprflw(n).para.title = '25 MHz GPR cross section';
% gprflw(n).para.resolution = 300; % dpi
% gprflw(n).para.type = 'pdf'; %pdf;jpg;...
% gprflw(n).para.fname = '25MHz_topo';
load rdwhitblu
dt    = indata.sampling_rate ;
ntr = indata.nr_of_traces;
ns = indata.nr_of_samples;
tt = (0:ns-1)*dt/1000;
xx = 1:ntr;

set(gcf, 'Units', 'centimeters',...
    'Position', [0 0 para.size],...
    'PaperPosition', [0 0 para.size],...
    'PaperUnits', 'centimeters',...
    'PaperOrientation',para.orientation,...
    'PaperType',para.papertype);
    
if isempty(indata.x) || isempty(indata.z)
    
    pcolor(xx,tt,indata.data)
    shading interp;
    colormap(red_white_blue)
    xlabel('trace nr')
    ylabel('t [ms]')
    axis tight ij
    set(gca,'layer','top')
    if isfield(para,'xlim')
        xlim(para.xlim)
    end
    if isfield(para,'ylim')
        ylim(para.ylim)
    end
elseif isempty(indata.dataz) || isempty(indata.z)
    
    pcolor(indata.x,tt,indata.data)
    shading interp;
    colormap(red_white_blue)
    axis tight ij
    xlabel('x [m]')
    ylabel('t [ms]')
    set(gca,'layer','top')
    if isfield(para,'xlim')
        xlim(para.xlim)
    end
    if isfield(para,'ylim')
        ylim(para.ylim)
    end
else
    pcolor(indata.x,indata.dataz,indata.data)
    shading interp;
    colormap(red_white_blue)
    axis tight
    xlabel('x [m]')
    ylabel('t [ms]')
    set(gca,'layer','top')
    if isfield(para,'xlim')
        xlim(para.xlim)
    end
    if isfield(para,'ylim')
        ylim(para.ylim)
    end
end
% set(h,'XAxisFontSize',para.fontsize)
set(gca, 'FontSize', para.fontsize)
xlabel(para.xlabel, 'FontSize',para.fontsize)
ylabel(para.ylabel, 'FontSize',para.fontsize)
title(para.title, 'FontSize',para.fontsize,'interpreter','none')
eval(['print(''-d',para.type,''', ''-r', num2str(para.resolution), ''', ''' para.fname,''')'])
end

