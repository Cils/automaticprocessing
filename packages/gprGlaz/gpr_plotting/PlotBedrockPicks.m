function PlotBedrockPicks

% -------------------------------------------------------------------------
% ---------------------- SET PATHS & FILEDATE & PARAMETERS ----------------
% -------------------------------------------------------------------------
pp = '/Users/celialucas/Desktop/Masterthesis/GSSI_Titlis_20131203/';
segyfolder = [pp 'SEGY/'];    %path to SEG-Y files
plotfolder = [pp 'PLOT/'];    %path to folder where jpg plots are stored
bedrPath =  [pp 'PICK/'];      %path to bedrock pick files
pp_cax = [pp 'matdata/'];        %path to folder to store caxis information

filedate = 'Titlis_20131203';  procp.dri = 0.75; procp.GlobalZshift = 5; procp.maxdepth = 200;

dd1 = dir(sprintf('%s%s_mig_PROFILE_*',segyfolder,filedate)); clear DD
for C = 1:length(dd1);
    DD(C) = sscanf(dd1(C,1).name,[filedate '_mig_PROFILE_%d']);
end

load zcm.mat
load blkwhtred.mat
procp.cmap = blkwhtred; % Colormap for displaying sections
procp.cax = [-1.0 1.0]; % Lower and upper bounds of colorbar
procp.vair = 0.299;     % Air velocity
procp.vice = 0.1689;    % Ice velocity

% -------------------------------------------------------------------------
% ---------------------- PLOT GPR DATA AND BEDROCK PICKS ------------------
% -------------------------------------------------------------------------
for a = DD
    disp(sprintf('Profile %d',a));
    Bedrock_file = sprintf('%s%s_PROFILE_%d_BEDROCK.txt',bedrPath,filedate,a);
    if exist(Bedrock_file) == 0; disp('No Picks'); continue; end
    
    % ------------------- READ SEGY FILE ----------------------------------
    segyname = sprintf('%s%s_mig_PROFILE_%03d',segyfolder,filedate,a);
    [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);
    
    % ------------------- LOAD CAXIS INFORMATION IF EXIST -----------------
    CAxisFile = sprintf('%s%s_CaxisProfile_%d.mat',pp_cax,filedate,a);
    if exist(CAxisFile) == 2;
        load(CAxisFile);
        procp.cax = Caxis;
    end
    
    % ------------------- PLOT GPR DATA -----------------------------------
    if sum(isnan(zdem)) == 0
        zeff = zeff - median(zeff-zdem);
    else
        zeff = zeff - procp.GlobalZshift;
    end;
    
    t = (0:nsamp-1)*sra;
    dist = (0:ntr-1)*procp.dri;
    dz = sra/2.0*procp.vice;
    
    maxalt = max(zeff) + 20;
    minalt = min(zeff) - procp.maxdepth;
    nz = round((maxalt - minalt)/dz);
    zz = linspace(-maxalt,-minalt,nz);
    
    dataz = zeros(nz,ntr);
    itopo = round(tti/sra);
    
    for A = 1:ntr
        [mv,mi] = min(abs(zz+zeff(A)));
        nins = nsamp - itopo(A);
        
        if (itopo(A) < 1), itopo(A) = 1; end;
        i3 = itopo(A);
        i4 = itopo(A)+nins - 1;
        i1 = mi;
        i2 = mi + nins - 1;
        if (i2 > nz)
            i4 = i4 - (i2-nz);
            i2 = nz;
        end;
        dataz(i1:i2,A) = data(i3:i4,A);
    end
    
    rsum = sum(abs(dataz),2);
    ii = find(rsum > 1.0e-3);
    zz = zz(ii(1):ii(length(ii)));
    dataz = dataz(ii(1):ii(length(ii)),:);
    
    fig = figure(1); clf;
    imagesc(dist,abs(zz),dataz); hold on;
    plot(dist,zeff,'k.','Markersize',3);
    xlabel('Distance [m]');
    ylabel('Altitude [masl]');
    set(gca,'dataaspectratio',[1 1 1]);
    set(gca,'YDir','normal')
    title(sprintf('Bedrock Picks: %s Profile %d',filedate,a),'fontsize',12,'interpreter','none');
    colormap(procp.cmap);
    caxis(procp.cax);
    
    
    % -------------- IMPORT BEDROCK PICKS FROM .TXT FILE ------------------
    Bedrock_file = sprintf('%s%s_PROFILE_%d_BEDROCK.txt',bedrPath,filedate,a);
    if exist(Bedrock_file) == 2;
        C = load(Bedrock_file);
        xbed =  C(:,1);
        ybed = C(:,2);
        zbed = C(:,3);
        zsurf = C(:,4);
        iweight = C(:,6);
        
        clear C dd i1 i2 zbed_p
        for ww = [0 1 2 3 10 11 12]
            clear xbedi zbedi
            i1 = find(iweight == ww);
            xbedi = xbed(i1);
            ybedi = ybed(i1);
            zbedi = zbed(i1);
            
            xybedCoord = sqrt(xbedi.^2 + ybedi.^2);
            pxpyCoord = sqrt(px.^2+py.^2);
            
            for dd = 1:ntr
                i2 = find(xybedCoord == pxpyCoord(dd));
                if length(i2)>1; i2 = []; end
                if isempty(i2) == 1;
                    zbed_p(dd) = NaN;
                else
                    zbed_p(dd) = zbedi(i2);
                end
            end
                        
            %plot(dist,zbed_p,'k--','linewidth',1); %Plot all the same way
            if ww == 0; plot(dist,zbed_p,'k-','linewidth',1.5); end
            if ww == 1; plot(dist,zbed_p,'k--','linewidth',1.5); end
            if ww == 2; plot(dist,zbed_p,'k--','linewidth',1); end
            if ww == 3; plot(dist,zbed_p,'k--','linewidth',1); end
            if ww == 10; plot(dist,zbed_p,'k-','linewidth',1.5); end
            if ww == 11; plot(dist,zbed_p,'k--','linewidth',1.5); end
            if ww == 12; plot(dist,zbed_p,'k--','linewidth',1); end
        end
    end
    
%     % ------------------- ADJUST COLORMAP FINAL FIGURE  -----------------
%     button = 0;
%     while(button ~= 27)
%         if (button == 28), procp.cax = procp.cax/1.1; end;
%         if (button == 29), procp.cax = procp.cax*1.1; end;
%         if (button == 49), procp.cmap = zcm; end;
%         if (button == 50), procp.cmap = blkwhtred; end;
%         colormap(procp.cmap);
%         caxis(procp.cax);
%         [xx,yy,button] = ginput(1);
%     end
%     
%     CAxisFile = sprintf('%s%s_CaxisProfile_%d.mat',pp_cax,filedate,a);
%     Caxis = procp.cax;
%     save(CAxisFile, 'Caxis')

%     % ------------------- PICK NEW BEDROCK  -----------------------------
%     title(sprintf('Pick New Bedrock: %s Profile %d',filedate,a),'interpreter','none');
%     xbed = [];
%     ybed = [];
%     zbed = [];
%     zsurf = [];
%     iweight = [];
%     xx = (0:ntr-1)*procp.dri;
%     
%     while(1)
%         [dbed,ztmp] = getline(gca);
%         
%         if (length(dbed) < 2),
%             break;
%         end;
%         
%         ftrace = ceil(min(dbed)/procp.dri);     % round towards positive infinity
%         ltrace = floor(max(dbed)/procp.dri);    % round towards negative infinity
%         zbedii = interp1(dbed,ztmp,xx(ftrace:ltrace),'linear'); % interpolate between picks
%         
%         w = inputdlg('Enter weight [0]');       % weight -> different colors
%         w = str2num(w{1});
%         
%         if (isempty(w) == 1), w = 0; end;
%         
%         % ----- Plot interpolated line between picks -----
%         if (w == 0), plot(xx(ftrace:ltrace),zbedii,'k-','linewidth',1.5); end;   % w=0: good
%         if (w == 1), plot(xx(ftrace:ltrace),zbedii,'k--','linewidth',1.5); end;  % w=1: not sure
%         if (w == 2), plot(xx(ftrace:ltrace),zbedii,'k--','linewidth',1); end;   % w=2: less sure
%         if (w == 3), plot(xx(ftrace:ltrace),zbedii,'b--','linewidth',1); end;   % w=3:
%         if (w == 10), plot(xx(ftrace:ltrace),zbedii,'k-','linewidth',1.5); end;  % w=10: second good bedrock
%         if (w == 11), plot(xx(ftrace:ltrace),zbedii,'k--','linewidth',1.5); end; % w=11: second not sure bedrock
%         if (w == 12), plot(xx(ftrace:ltrace),zbedii,'k--','linewidth',1); end;  % w=12:
%         
%         %plot(dbed,ztmp,'ko','linewidth',1.5);
%         
%         xbed = [xbed; px(ftrace:ltrace)']; % x coordinate of picked horizon
%         ybed = [ybed; py(ftrace:ltrace)']; % y coordinate of picked horizon
%         zbed = [zbed; zbedii'];            % z coordinate of picked horizon
%         zsurf = [zsurf; zeff(ftrace:ltrace)'];
%         iweight = [iweight; w*ones(size(zbedii'))];
%     end;
%     
%     if isempty(zbed) == 1; continue; end
%     
%     % -------------------------------------------------------------------------
%     % ------------------- SAVE PICKS IN .txt FILE -----------------------------
%     % -------------------------------------------------------------------------
%     %if isempty(answer) == 1; continue; end %*****
%     %choice = questdlg('Save Picks?','Save Picks','yes','no','yes');
%     %if strcmp(choice,'no') == 1; continue; end
%     Bedrock_file = sprintf('%s%s_PROFILE_%d_BEDROCK.txt',bedrPath,filedate,a);
%     %*****zbed = zbed - corrPick; %****
%     fid = fopen(Bedrock_file,'wt');
%     if isempty(zbed) == 0;
%         fid = fopen(Bedrock_file,'wt');
%         for DD = 1:length(xbed)
%             fprintf(fid,'%10.3f %10.3f %10.3f %10.3f %10.3f %d\n',xbed(DD),ybed(DD),zbed(DD),zsurf(DD),zsurf(DD)-zbed(DD),iweight(DD));
%         end;
%         fclose(fid);
%     end
    
    % -------------------------------------------------------------------------
    % ------------------- SAVE FIGURE IN .jpg FILE -----------------------------
    % -------------------------------------------------------------------------
    %choice = questdlg('Save Plot?','Save Plot','yes','no','yes');
    %if strcmp(choice,'no') == 1; continue; end
    
    plotfile = sprintf('%s%s_Bedrock_PROFILE_%03d.jpg',plotfolder,filedate,a);
    print(fig,'-djpeg99','-r600',plotfile);

end
