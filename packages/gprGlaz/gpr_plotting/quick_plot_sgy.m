
[fname,pname]=uigetfile({'*.sgy';'*.sgy'},'Choose Segy File to plot...','*.sgy');

[comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ...
     ReadSEGY([pname,fname]);
 
 procp = make_procp(comments);
 
 params = struct('fname',[],'pname',[],'comments',comments, ...
              'offset',offset,'ntr',ntr,'nsamp',nsamp,...
              'sra',sra,'px',px,'py',py,'pz',pz,...
              'data',data,'zdem',zdem,...
              'origpx',origpx,'origpy',origpy,...
              'tti',tti,'zeff',zeff,...
              'vel',[],'head',[],'azi',[],...
              'pitch',[],'roll',[],'hag',zeros(1,length(pz)),...
              'profnr',profnr,'date',date,...
              'info',info,'profinfo',profinfo,...
              'segyfolder','','plotfolder','',...
              'pickfolder','','matdata','',...
              'tag','','tag2','');
 
 procp = SegyPlotData(fname,procp,params,1,1);
 plotfile = sprintf('%s.jpg',fname); 
 print('-djpeg99','-r600',plotfile);
 

