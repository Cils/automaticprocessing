function SegyDisplayResults(Title,params,params2,procp,X,cmap,refmat,bbox)
%,X,cmap,refmat,bbox = output parameters from "geotiffread"

% load data
%[comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);

clf;

%extract "dept" and "zexag" from procp
maxdepth = procp.maxdepth;
zexag = procp.zexag;

if ~isempty(strfind(params.tag, '_ch2'))
    error([params.tag ' is not Channel 1'])
end

%create filename where 'procp' is saved and laod procp of ch1 if present
if exist(sprintf('%s%sprocp.mat',params.matdata,params.tag),'file'),
    procp_file = sprintf('%s%sprocp.mat',params.matdata,params.tag);
    load(procp_file);
end
%overwrite old "depth" "zexag" with new ones
procp.maxdepth = maxdepth;
procp.zexag = zexag;

% Plot data
if ~strcmp(params2.fname,'')
    subplot(221);
else
    subplot(211);
end
procp = SegyPlotData('Channel 1',procp,params,4,1);
% save procp to recall the best scaling for plotting at a later time  
procp_file = sprintf('%s%sprocp.mat',params.matdata,params.tag);
save(procp_file,'procp');        
freezeColors;

if ~strcmp(params2.fname,'')
    %create filename where 'procp' is saved and laod procp of ch2 if present
    if exist(sprintf('%s%sprocp.mat',params2.matdata,params2.tag),'file'),
        procp_file2 = sprintf('%s%sprocp.mat',params2.matdata,params2.tag);
        procp2=load(procp_file2);
        procp = procp2.procp;
    end

    %overwrite old "depth" "zexag" with new ones
    procp.maxdepth = maxdepth;
    procp.zexag = zexag;

    subplot(222);
    procp = SegyPlotData('Channel 2',procp,params2,4,1);
    % save procp to recall the best scaling for plotting at a later time   
    procp_file2 = sprintf('%s%sprocp.mat',params2.matdata,params2.tag);
    save(procp_file2,'procp');  
    freezeColors;
end

% Plot map
subplot(234);
if (isempty(X) == 0)
    axis equal
    xliminit=xlim;
    yliminit=ylim;
    %store map corner coordinates
    map_corner = [refmat(3,1),refmat(3,2)];
    % change to kilometer scale
    refmat(2,1) = refmat(2,1)/1000; refmat(1,2) = refmat(1,2)/1000;
    % set corner coordinates to zero
    refmat(3,1) = 0, refmat(3,2) = 0; 
    %display geotiff map
    mapshow(X,cmap,refmat);
    hold on;
    %reduce profile coordinates to local map and to km scale
    px = (params.px-map_corner(1))/1000;
    py = (params.py-map_corner(2))/1000;
    %plot profile on map
    plot(px,py,'k','LineWidth', 1.2);
    
    % Marker every 200 m
    xx = (0:params.ntr-1)*procp.dri;
    ticm = 0:200:max(xx);
    iitic = zeros(length(ticm),1);
    for b = 1:length(ticm)
        [~,iitic(b)] = min(abs(xx-ticm(b))); 
    end
    for bb = 1:length(iitic)
        plot(px(iitic(bb)),py(iitic(bb)),'b.','markersize',10); 
    end
    
    minx = min(px)-.5;
    maxx = max(px)+.5;
    miny = min(py)-.5;
    maxy = max(py)+.5;
    
    % Mark start point of the profile
    plot(px(1),py(1),'k.','markersize',15);
    hold off;
    xlabel('West-East [km]','fontsize',12);
    ylabel('North-South [km]','fontsize',12);
    xmag = (maxx - minx);
    ymag = (maxy - miny);
    prop=(yliminit(2)-yliminit(1))/(xliminit(2)-xliminit(1));
    if xmag*prop>ymag
        axis([(maxx+minx)/2+[-1 1]*xmag/2 (maxy+miny)/2+[-1 1]*xmag/2*prop]);
    else
        axis([(maxx+minx)/2+[-1 1]*ymag/2/prop (maxy+miny)/2+[-1 1]*ymag/2]);
    end
    set(gca,'XTickLabel','')
    set(gca,'YTickLabel','')
    %set(gca,'dataaspectratio',[maxx-minx maxy-miny 1]);
end;

% -------------- PLOT THE ALTITUDE --------------
subplot(235);
%plot(xx,params.zeff+ median(params.zdem - params.zeff),'r.',xx,params.zdem,'b.');
if (max(params.zdem) < 1),
   plot(xx,params.pz-params.hag,'r.',xx,params.pz);
   legend('Ground elevation (laser)','Antenna elevation (GPS)');
else
   plot(xx,params.pz-params.hag,'r.',xx,params.zdem,'b.'); 
   legend('Elevation from GPR','Digital Elevation Map');
end;   
title('Elevation Profile','fontsize',12);
xlabel('Distance [m]','fontsize',12);
ylabel('Altitude [m a.s.l.]','fontsize',12);
%legend('off')

subplot(236);
plot(xx,params.head-params.azi,'r.',xx,params.roll,'b.',xx,params.pitch,'g.')
legend('Yaw','Roll','Pitch');
hold off;
title('Antenna orientation','fontsize',12);
xlabel('Distance [m]','fontsize',12);
ylabel('Degrees [�]','fontsize',12);

 %text(-2500,280,Title)
 axes( 'Position', [0, 0.95, 1, 0.05] ) ;
 axis off
 
 Title=strrep(Title, '_ch1', ''); % delete '_ch1' if present
 Title=strrep(Title, '_', '\_'); % no subscript, but underlines
 text( 0.5, 0, Title, 'FontSize', 14', 'FontWeight', 'Bold', ...
      'HorizontalAlignment', 'Center', 'VerticalAlignment', 'Bottom' ) ;
