function [data] = SegyGPRMig(ntr,sra,data,tti,procp)

% -------------------------------------------------------------------------
% ------------------- BUILD VELOCITY MODEL --------------------------------
% -------------------------------------------------------------------------
% 2 layer velocity model: air - ice
vr = procp.vice * ones(size(data));
for a = 1:ntr
    isamp = round(tti(a)/sra);
    vr(1:isamp,a) = procp.vair;
end;



% -------------------------------------------------------------------------
% ------------------- RUN MIGRATION ---------------------------------------
% -------------------------------------------------------------------------
sprintf('start migration now')

params = nan*ones(12,1);
[data,tmig,xmig]=kirk_mig(data,vr,sra,procp.dri,params);
