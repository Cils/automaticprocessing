function [gprdat n] = gpr_2dmig(indat,para, n)
% coherency filter

gprdat = indat;

sra = indat.sampling_rate*1e-9;

x = indat.x;
z = indat.z;
v = para.velocity;

params = NaN(1,12);

% [outdat,tmig,xmig]=gpr_kirk_mig(indat.data,v,sra,x,params);

[outdat,xmig]=gpr_kirk_migz(indat.data,v,sra,x,z,params);

gprdat.data = outdat;
gprdat.x = xmig;

if nargin > 2
    n = n +1;
end
