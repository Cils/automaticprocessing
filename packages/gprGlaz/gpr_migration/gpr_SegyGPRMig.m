function [gprdat n] = gpr_SegyGPRMig(indat,para, n)

gprdat  = indat;
data    = indat.data;
ntr     = indat.nr_of_traces;
sra     = indat.sampling_rate;
tti     = para.tti;                              %tti = Picked two-way traveltime of the glacier surface
procp.vair  = para.vair;
procp.vice  = para.vice;
procp.dri   = para.dri;



% -------------------------------------------------------------------------
% ------------------- BUILD VELOCITY MODEL --------------------------------
% -------------------------------------------------------------------------
% 2 layer velocity model: air - ice
vr = procp.vice * ones(size(data));
for a = 1:ntr
    isamp = round(tti(a)/sra);
    vr(1:isamp,a) = procp.vair;
end;



% -------------------------------------------------------------------------
% ------------------- RUN MIGRATION ---------------------------------------
% -------------------------------------------------------------------------
sprintf('start migration now')

params = nan*ones(12,1);
[data,tmig,xmig]= gpr_kirk_mig(data,vr,sra,procp.dri,params);


gprdat.data = data;

if nargin > 2
    n = n +1;
end