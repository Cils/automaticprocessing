function [gprdat nn] = gpr_bin2D(indata,para, nn)

gprdat = indata;

geast = indata.x;
gnort = indata.y;
gheit = indata.z;

myfit = polyfit(geast, gnort, 1);
% c     = gnort+geast/myfit(1);
% xc    = (c -myfit(2))/(myfit(1)+1/myfit(1));
% yc    = myfit(1)*xc + myfit(2);
xtrace = zeros(size(geast));
ytrace = zeros(size(gnort));
% project points on line
for ii = 1:indata.nr_of_traces
    r = [0 myfit(2)]';
    u = [myfit(2)/myfit(1) myfit(2)]';
    tmp = r +dot([geast(ii) gnort(ii)]'-r,u)/dot(u,u)*u;
    xtrace(ii) = tmp(1);
    ytrace(ii) = tmp(2);
end

% [y0 yi] = min(yc);

dx = para.binsize.*cos(atan(myfit(1)));
dy = para.binsize.*sin(atan(myfit(1)));

xdir = sign(dx);
ydir = sign(dy);

dx = abs(dx);
dy = abs(dy);



xbin = (min(xtrace)):dx:(max(xtrace)+dx);
ybin = polyval(myfit,xbin);


if xdir>0 && ydir<0
%     ybin = fliplr(ybin);
    pdist = sqrt((xtrace-min(xtrace)).^2+(ytrace-max(ytrace)).^2); 
%     pbin = sqrt((xbin-max(xbin)).^2+(ybin-max(ybin)).^2);
elseif xdir<0 && ydir<0
    xbin = fliplr(xbin);
    ybin = polyval(myfit,xbin);
    pdist = sqrt((xtrace-max(xtrace)).^2+(ytrace-max(ytrace)).^2);
%     pbin = sqrt((xbin-max(xbin)).^2+(ybin-min(ybin)).^2);
elseif xdir<0 && ydir>0
    xbin = fliplr(xbin);
    ybin = polyval(myfit,xbin);
    pdist = sqrt((xtrace-max(xtrace)).^2+(ytrace-min(ytrace)).^2);
%     pbin = sqrt((xbin-min(xbin)).^2+(ybin-min(ybin)).^2);
elseif xdir > 0 && ydir >0 
    pdist = sqrt((xtrace-min(xtrace)).^2+(ytrace-min(ytrace)).^2);
%     pbin = sqrt((xbin-min(xbin)).^2+(ybin-min(ybin)).^2);
end


pbin = sqrt((xbin-xbin(1)).^2+(ybin-ybin(1)).^2);
% pdist = sqrt((xtrace-xtrace(1)).^2+(ytrace-ytrace(1)).^2);
gprdat.data = zeros(size(indata.data,1), length(pbin)-1);

zbinc = nan(length(xbin)-1,1);
xbinc = nan(length(xbin)-1,1);
ybinc = nan(length(ybin)-1,1);
pbinc = nan(length(pbin)-1,1);
[~,inbin] = histc(pdist,pbin);

for ii = 1:(length(pbin)-1)
    gprdat.data(:,ii)=mean(indata.data(:,inbin==ii),2);
    zbinc(ii) = mean(gheit(inbin==ii));
    xbinc(ii) = (xbin(ii)+xbin(ii+1))/2;
    ybinc(ii) = (ybin(ii)+ybin(ii+1))/2;
    pbinc(ii) = (pbin(ii)+pbin(ii))/2;
end
pbinc= pbinc-pbinc(1);
% gprdat.data          = gprdat.data-repmat(mean(gprdat.data,1),gprdat.nr_of_samples,1);
ebins = find(isnan(zbinc));
if ~isempty(ebins)
    disp(['Warning! ',num2str(length(ebins)),'empty bins'])
    ebins
    gprdat.data(:,isnan(zbinc))=0;
    zbinc = interp1(xbin(~isnan(zbinc)), zbinc(~isnan(zbinc)), xbinc, 'linear','extrap');
end


gprdat.nr_of_traces = size(gprdat.data,2);    
figure
subplot(211)
plot(geast,gnort,'b')
hold on
plot(xtrace,ytrace,'r.')


plot(xbin,ybin,'g+')
legend('original','projected','bins')

if ~isempty(ebins)
    plot(xbin(ebins),ybin(ebins),'k*')
    legend('original','projected','bins','empty')
end
xlabel('x')
ylabel('y')
subplot(212)
hold on
plot(pbinc,zbinc,'r')
plot(pdist,gheit,'b')

zbinc = smooth(zbinc,20);
plot(pbinc,zbinc,'g')
legend('original','binned','smoothed')
xlabel('x')
ylabel('z')


gprdat.x = xbinc;
gprdat.y = ybinc;
gprdat.z = zbinc;
% gprdat.pdist = pbinc;
gprdat.profdir = myfit(1);


if nargin > 2
    nn = nn +1;
end


end