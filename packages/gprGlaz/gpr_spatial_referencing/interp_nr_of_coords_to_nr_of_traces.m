function [coord_new] = interp_nr_of_coords_to_nr_of_traces(coords,data)

% in case less gps "coords" are given than traces exist in "data", it
% interpolate between elemtns of coords to create "length of data" number
% of coords
%
% prerequisite is, that beginning and end of "coords", represent
% the beginning and end of "data"

nr_given_samples = length(coords);
nr_wanted_samples = length(data);

ratio = nr_wanted_samples / nr_given_samples;

samp = linspace(1,length(data),length(coords));
samp = floor(samp);
samp2 = (1:1:length(data));

sys = frd(coords,samp);
isys = interp(sys,samp2);
[response, freq] = frdata(isys);
coord_new = squeeze(response);

