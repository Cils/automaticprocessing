function nprof = SegySplitProfiles(profno,nsplit,nprof,segyfolder,filedate,procp)



for a = 1:length(profno)
    segyname = sprintf('%s%s_proc2_PROFILE_%03d',segyfolder,filedate,profno(a));
    [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(segyname);

    ntr2 = ntr;
    px2 = px;
    py2 = py;
    pz2 = pz;
    origpx2 = origpx;
    origpy2 = origpy;
    data2 = data;
    tti2 = tti;
    zeff2 = zeff;
    zdem2 = zdem;
    
    b = 1;
    c = floor(ntr/nsplit(a));
    
    for d = 1:nsplit(a)
        n1 = b;
        n2 = b + c - 1;
        if (d == nsplit(a)),n2 = ntr2; end;
        b = b + c;
        px = px2(n1:n2);
        py = py2(n1:n2);
        pz = pz2(n1:n2);
        origpx = origpx2(n1:n2);
        origpy = origpy2(n1:n2);
        data = data2(:,n1:n2);
        tti = tti2(n1:n2);
        zeff = zeff2(n1:n2);
        zdem = zdem2(n1:n2);
        [nsamp ntr] = size(data);

        if (d == 1)
            nn = profno(a);
        else
            nprof = nprof + 1;
            nn = nprof;
        end;
        
        profnr = nn;
        segyname2 = sprintf('%s%s_proc2_PROFILE_%03d.sgy',segyfolder,filedate,nn);
        comments = make_comments(info.area,info.instr,profnr,info.freq,info.offset,info.dewsoft,date,ntr,sra,nsamp,procp,profinfo,1,1,1,0);
                               %(area,instr,profnr,freq,offset,dewsoft,date,ntr,sra,nsamp,procp,profinfo,proc1,picksurf,proc2,mig)    
        GPR_write_segy(segyname2,comments,offset,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date);
    end;
end;

     