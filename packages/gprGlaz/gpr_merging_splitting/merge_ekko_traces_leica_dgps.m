function merge_ekko_traces_leica_dgps(leica,pulsekko,csys)

%use uncorreced values as well when there is a gap in the corrected gps values?
%yes:all=1, no:all=0, only uncorrected:all=2
all=2;


% timeshift from ekko to leica in the format "hhmmss" as a float
% e.g. if you want to change from 14:33:12 (143312) to 16:33:12 (163312) this 
% number must be 20000
timeshift = 20000;

%coordsystem 2=wgs84, 1=swissgrid
csys=2;

%define location and name pulsekko file
ekko = struct('path','F:\SCCER\OTEMMA\GROUND_GPR\',...
              'file','LINE26.GPS');

%define location and name processed gps file
if csys==1,
    leica = struct('path','F:\SCCER\OTEMMA\PROCESSED_GPS\',...
               'file','1306-1641_ch.txt');     
elseif csys==2,           
    leica = struct('path','F:\SCCER\OTEMMA\PROCESSED_GPS\',...
                   'file','1306-1641_wgs84.txt');
end               
        


% write information from leica file into arrays
fid_l=fopen([leica.path leica.file],'r');
if csys==1,
    line = fgets(fid_l);
    ldata=textscan(fid_l,'%f %f %f %f %f %f','Delimiter','\t_ ');
    l_time = ldata{2};
    l_east = ldata{3};
    l_north = ldata{4};
    l_alt = ldata{5};
    l_qual = ldata{6};
elseif csys==2,
    line = fgets(fid_l);
    data=textscan(fid_l,'%s','Delimiter','\t');
    data = data{1};
    data = horzcat(data{:});
    timepos = strfind(data,'_');
    N_deg_pos = strfind(data,'N')-18;
    N_min_pos = strfind(data,'N')-16;
    N_sec_pos = strfind(data,'N')-11;
    E_deg_pos = strfind(data,'E')-18;
    E_min_pos = strfind(data,'E')-16;
    E_sec_pos = strfind(data,'E')-11;
    altpos = strfind(data,'E')+1;
    qualpos = strfind(data,'E')+10;
    l_time = repmat({'empty'}, length(timepos),1);
    N_deg  = repmat({'empty'}, length(timepos),1);
    N_min  = repmat({'empty'}, length(timepos),1);
    N_sec  = repmat({'empty'}, length(timepos),1);
    E_deg  = repmat({'empty'}, length(timepos),1);
    E_min  = repmat({'empty'}, length(timepos),1);
    E_sec  = repmat({'empty'}, length(timepos),1);
    l_alt  = repmat({'empty'}, length(timepos),1);
    l_qual = repmat({'empty'}, length(timepos),1);
   for i=1:length(timepos);   
    l_time{i} = data(timepos(i)+1:timepos(i)+8);
    N_deg{i}  = data(N_deg_pos(i):N_deg_pos(i)+1);
    N_min{i}  = data(N_min_pos(i)+1:N_min_pos(i)+3);
    N_sec{i}  = data(N_sec_pos(i)+1:N_sec_pos(i)+8);
    E_deg{i}  = data(E_deg_pos(i)+1:E_deg_pos(i)+1);
    E_min{i}  = data(E_min_pos(i)+1:E_min_pos(i)+3);
    E_sec{i}  = data(E_sec_pos(i)+1:E_sec_pos(i)+8);
    l_alt{i}  = data(altpos(i):altpos(i)+8);
    l_qual{i} = data(qualpos(i):qualpos(i)+5);    
   end 
   N_sec = cell2mat(N_sec);
   N_sec = str2num(N_sec);
   N_sec = N_sec/60;
   E_sec = cell2mat(E_sec);
   E_sec = str2num(E_sec);
   E_sec = E_sec/60;
   l_time=cell2mat(l_time);
   l_time =str2num(l_time);
else
    fprintf('No valid coordinate system key \n')
end    


% write trace number, time stamp and other string fragemnts
% from ekko gps file into cell arrays
fid_e=fopen([ekko.path ekko.file],'r');
edata=textscan(fid_e,'%s','delimiter','\t');
data = edata{1};
data = horzcat(data{:});
%define positions within string
tr1 = strfind(data,'Trace')+7;
tr2 = strfind(data,'Trace')+8;
tr3 = strfind(data,'Trace')+9;
tr4 = strfind(data,'Trace')+10;
tr5 = strfind(data,'Trace')+11;
gpgga1 = strfind(data,'GPGGA')+6;
gpgga2 = strfind(data,'GPGGA')+14;
gpgga_start = strfind(data,'GPGGA')-1;
gpgga_front = strfind(data,'GPGGA')+15;
gpgga_back = strfind(data,'GPGGA')+44;
gpgga_end = strfind(data,'GPGGA')+79;
lat_start = gpgga_front+1;
lat_end = gpgga_front+12;
lon_start = gpgga_front+16;
lon_end = gpgga_front+29;
%create empty arrays 
tr_num = repmat({'empty'}, length(gpgga1),1);
e_time = repmat({'empty'}, length(gpgga1),1);
tr_str = repmat({'empty'}, length(gpgga1),1);
frontstr = repmat({'empty'}, length(gpgga1),1);
backstr = repmat({'empty'}, length(gpgga1),1);
lat = repmat({'empty'}, length(gpgga1),1);
lon = repmat({'empty'}, length(gpgga1),1);
if length(tr1) == length(gpgga1),
 for i=1:length(gpgga1);
    e_time{i} = data(gpgga1(i):gpgga2(i));
    frontstr{i} = data(gpgga_start(i):gpgga_front(i));
    backstr{i}  = data(gpgga_back(i):gpgga_end(i));
    tr_str{i} = data(tr1(i)-7:tr1(i)+21);
    lat{i} = data(lat_start(i):lat_end(i));
    lon{i} = data(lon_start(i):lon_end(i));
     if i<10,
       tr_num{i} = data(tr1(i));
     elseif i>=10 && i<100,
       tr_num{i} = data(tr1(i):tr2(i));
     elseif i>=100 && i<1000
       tr_num{i} = data(tr1(i):tr3(i));
     elseif i>=1000 && i<10000  
       tr_num{i} = data(tr1(i):tr4(i));  
     elseif i>=10000 && i<100000  
       tr_num{i} = data(tr1(i):tr5(i));  
     end 
  end    
else
   fprintf('Different number of Traces and GPGGA entries \n')
end   
e_time = cell2mat(e_time);
e_time = str2num(e_time);
lon = str2num(cell2mat(lon));
lat = str2num(cell2mat(lat));


%convert ekko time to same as on leica
e_time = e_time+timeshift;
%bring ekko time data format to same number of digits
e_time = e_time*100;
%convert leica time into seconds
l_time = num2str(l_time);
l_hou=l_time(:,1:2);l_hou_sec=str2num(l_hou)*3600;
l_min=l_time(:,3:4);l_min_sec=str2num(l_min)*60;
l_sec=l_time(:,5:6);l_sec_sec=str2num(l_sec);
l_hun=l_time(:,7:8);l_hun_sec=str2num(l_hun)/100;
l_sec=l_hou_sec+l_min_sec+l_sec_sec+l_hun_sec;
%convert ekko time into seconds 
e_time = num2str(e_time);
e_hou=e_time(:,1:2);e_hou_sec=str2num(e_hou)*3600;
e_min=e_time(:,3:4);e_min_sec=str2num(e_min)*60;
e_sec=e_time(:,5:6);e_sec_sec=str2num(e_sec);
e_hun=e_time(:,7:8);e_hun_sec=str2num(e_hun)/100;
e_sec=e_hou_sec+e_min_sec+e_sec_sec+e_hun_sec;

l_time = l_sec;
e_time = e_sec;

% determine positions of ekko time stamps in leica data
l_time = rot90(l_time);
e_time = rot90(e_time);
pos = match(l_time,e_time,60);
% when zero positions exist (.i.e. Leica Data are missing), removing
% of according entries in the puls ekko string:
nonzero = find(pos);
if all==0,
 if length(nonzero)<length(pos),
  tr_str = tr_str(nonzero);
  frontstr = frontstr(nonzero);
  backstr = backstr(nonzero);
  %kill the zero entries in the leica data positioning array
  pos(pos==0) = [];
 end
elseif all==1,
% alternatively fill missing gps values with uncorrected 
% lat lon values from puls ekko string
 zero = find(pos==0);
end
  
%assign to each trace a north,east, alt value
if (csys==1 && all==0),
  north = l_north(pos);
  east = l_east(pos);
  alt = l_alt(pos);
elseif (csys==2 && all==2),
  north = lat;
  east = lon;
elseif (csys==2 && all==1),
  north = lat;
  east = lon;
  if ~isempty(nonzero),
  N_deg = str2num(cell2mat(N_deg(pos(nonzero))));
  N_min = str2num(cell2mat(N_min(pos(nonzero))))+N_sec(pos(nonzero));
  E_deg = str2num(cell2mat(E_deg(pos(nonzero))));
  E_min = str2num(cell2mat(E_min(pos(nonzero))))+E_sec(pos(nonzero));
  north_tmp = repmat({'empty'}, length(E_deg),1);
  east_tmp = repmat({'empty'}, length(E_deg),1);
  for j=1:length(E_deg);
   north_tmp{j}=sprintf('%i%10.7f',N_deg(j),N_min(j));
   east_tmp{j}=sprintf('%i%10.7f',E_deg(j),E_min(j));
  end
  end
elseif (csys==2 && all==0),
  if ~isempty(pos),  
    N_deg = str2num(cell2mat(N_deg(pos)));
    N_min = str2num(cell2mat(N_min(pos)))+N_sec(pos);
    E_deg = str2num(cell2mat(E_deg(pos)));
    E_min = str2num(cell2mat(E_min(pos)))+E_sec(pos);
    north = repmat({'empty'}, length(E_deg),1);
    east = repmat({'empty'}, length(E_deg),1);
    for j=1:length(E_deg);
     north{j}=sprintf('%i%10.7f',N_deg(j),N_min(j));
     east{j}=sprintf('%i%10.7f',E_deg(j),E_min(j));
    end
    north=str2num(cell2mat(north));
    east=str2num(cell2mat(east));
  end
end
    
   
%write output file
if csys==1,
    outfile=strcat(ekko.path,ekko.file,'.ch.corr');
elseif csys==2,
    if all==2,
    outfile=strcat(ekko.path,ekko.file,'.wgs.uncorr');
    else
    outfile=strcat(ekko.path,ekko.file,'.wgs.corr');
    end
end    
fid = fopen(sprintf('%s',outfile),'wt');
for i=1:length(tr_str);
    fprintf(fid,'%s\n',tr_str{i});
    if csys==1,
        fprintf(fid,'%s%8.2f%s%9.2f%s\n',frontstr{i},north(i),',N,',east(i),backstr{i});
    elseif csys==2,
        fprintf(fid,'%s%10.7f%s%10.7f%s\n',frontstr{i},north(i),',N,00',east(i),backstr{i});
    end    
end
fclose(fid);
fprintf('New file %s created!! \n',outfile)



