function [profidx,prof,nprof] = SelectAssignTime(nsamp,sra,ntr,data,px,py,hh,mm,ss)

%fname = 'D:\GLACIERS\OTEMMA\QUICK_DATA_HANDLING\20150410_Otemma_flight1\segy\20150408_Otemma_PulsEkko_flight1_profil-001_ch1_RAW.sgy';
%[fname,pname]=uigetfile({'*.sgy';'*.sgy'},'Choose Segy File to plot...','*.sgy');
%[comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ...
%     ReadSEGY([pname,fname]);

n = 0;
[nsamp,ntr] = size(data);
nprof = 0;
profidx = nan*ones(ntr,1);
button = 0;

global KEY_IS_PRESSED
KEY_IS_PRESSED = 0;

SegyPlotRaw(nsamp,sra,ntr,data,hh,mm,ss);
%gcf;
%set(gcf, 'KeyPressFcn', @myKeyPressFcn);


FS = stoploop({'Exit Assignment Procedure by clicking here and one more time in the plot window'}) ;

while (~FS.Stop())%~KEY_IS_PRESSED
    Title = 'Draw rectangle around data of interest'; 
    title(Title,'fontsize', 12,'interpreter','none');
    rect = getrect;
    t1 = int32(rect(1));
    t2 = t1+int32(rect(3));
    nprof = nprof + 1;
    ii = t1:t2;
    profidx(ii) = nprof;
    %Extract spatial coordinate information from assigned profile
    xx1 = px(t1);
    xx2 = px(t2);
    yy1 = py(t1);
    yy2 = py(t2);
    prof(nprof).x = [xx1,xx2]';
    prof(nprof).y = [yy1,yy2]';
    prof(nprof).n = 2;
    str = sprintf('Press Mouse button to continue with rectangle %d, or on the button in the external window to quit',nprof+1);
    title(str);
    drawnow;
    k = waitforbuttonpress;
    if k==0, continue; end;
end;

