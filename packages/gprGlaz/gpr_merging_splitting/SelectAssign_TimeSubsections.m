function [profidx,profinfo,nprof] = SelectAssign_TimeSubsections(fname,prx,pry,pxv,pyv,profidx,profinfo,nprof)

% assign for each time cut stright transects
% profidx = original vector with subprofil index
% profidx_tmp1 =same size as profidx, vector with final subprofile index for all time sections
% profidx_tmp2 = same size as ii, vector with subprofile numbers of specific time section
% (internal numbering)
profidx_tmp1 = NaN(length(profidx),1);
profinfo_tmp1 = struct('x',[],'y',[],'n',[]);
for i=1:max(profidx),
 close all,   
 ii = find(profidx == i);
 prx_cut = prx(ii);
 pry_cut = pry(ii);
 [profidx_tmp2,profinfo_tmp2,nprof_tmp2] = SelectAssignProfiles(fname,prx_cut,pry_cut,pxv,pyv);
 if i < 2,
   profinfo_tmp1 = profinfo_tmp2; 
   profidx_tmp1(ii) = profidx_tmp2;
   nprof_tmp1 = nprof_tmp2;
 else i > 1, 
   m = max(profidx_tmp1);  
   profidx_tmp1(ii) = profidx_tmp2+m;
   profinfo_tmp1 = [profinfo_tmp1,profinfo_tmp2]; 
   nprof_tmp1 = nprof_tmp1 + nprof_tmp2;
 end;
end;

profidx = profidx_tmp1;
profinfo = profinfo_tmp1;
nprof = nprof_tmp1;


