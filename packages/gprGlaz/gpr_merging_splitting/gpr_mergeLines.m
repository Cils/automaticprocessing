function [gprdat nn] = gpr_mergeLines(para, nn)

step = para.step;

for ii = 1:length(para.files)
    tmp(ii)= load([para.path para.files{ii} '.gprd'],'-mat');

end
gprdat = tmp(1).gprdat(step);

for ii = 2:length(para.files)
    if ((tmp(ii-1).gprdat(step).nr_of_samples == tmp(ii).gprdat(step).nr_of_samples) && ...
            (tmp(ii-1).gprdat(step).sampling_rate == tmp(ii).gprdat(step).sampling_rate))
        
        gprdat.nr_of_traces = gprdat.nr_of_traces + tmp(ii).gprdat(step).nr_of_traces;
        gprdat.data = [gprdat.data  tmp(ii).gprdat(step).data];
        gprdat.headers = [gprdat.headers  tmp(ii).gprdat(step).headers];
        gprdat.x = [gprdat.x  tmp(ii).gprdat(step).x];
        gprdat.y = [gprdat.y  tmp(ii).gprdat(step).y];
        gprdat.z = [gprdat.z  tmp(ii).gprdat(step).z];
        
        
    else
        disp('Data not merged, check datafiles / parameters')
    end
    
end
figure
hold on
col = {'r.','g.','b.','c.','y.','k.','r.','g.','b.','c.','y.','k.'};
for ii = 1:length(para.files)
    plot(tmp(ii).gprdat(step).x,tmp(ii).gprdat(step).y,col{ii})
end
legend(para.files)
if ~strcmp(para.outname,'')
    save([para.path para.outname '.gprd'], 'gprdat');
    disp(['data saved to: ', para.path, para.outname, '.gprd'])
end

if nargin > 1
    nn = nn +1;
end

end