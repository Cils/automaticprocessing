function [gprdat nn] = gpr_splitLines(indat,para, nn)
gprdat = indat;

gprdat.nr_of_traces = [];
gprdat.data = [];
gprdat.headers = [];
gprdat.nr_of_samples = [];
gprdat.time_window = [];
gprdat.t0 = [];
gprdat.frequency = [];
gprdat.separation = [];
gprdat.sampling_rate = []; 
gprdat.number_stacks = []; 
gprdat.dataz = []; 
gprdat.pos0 = []; 
gprdat.flip = []; 
gprdat.profdir = []; 
gprdat.x = [];
gprdat.y = [];
gprdat.z = [];
figure
hold on
plot(indat.x,indat.y,'b.');
axis equal
reply = 1;
while(reply)
    disp('select line (start / end)')
    [xl yl] = ginput(2);
    if min(xl)<min(indat.x);xl(xl==min(xl))=min(indat.x);end
    if min(yl)<min(indat.y);yl(yl==min(yl))=min(indat.y);end
    if max(xl)>max(indat.x);xl(xl==max(xl))=max(indat.x);end
    if max(yl)>max(indat.y);xl(yl==max(yl))=max(indat.y);end
    
    ind = find((indat.x>min(xl) & indat.x<max(xl)) & (indat.y>min(yl) & indat.y<max(yl)) );
    plot(indat.x(ind),indat.y(ind),'r.');
    fname = input('Input filename: ','s');
    
    gprdat.nr_of_traces = length(ind);
    gprdat.data = indat.data(:,ind);
    gprdat.nr_of_samples = indat.nr_of_samples;
    gprdat.t0 = indat.t0;
    gprdat.time_window = indat.time_window;
    gprdat.frequency = indat.frequency;
    gprdat.separation = indat.separation;
    gprdat.antenna_frequency = indat.antenna_frequency;
    gprdat.sampling_rate = indat.sampling_rate;
    gprdat.number_stacks = indat.number_stacks;
    gprdat.dataz = indat.dataz;
    gprdat.pos0 = indat.pos0;
    gprdat.flip = indat.flip;
    gprdat.profdir = indat.profdir;
    gprdat.headers = indat.headers(:,ind);
    gprdat.x = indat.x(ind);
    gprdat.y = indat.y(ind);
    gprdat.z = indat.z(ind);
    
    save([para.path fname '.gprd'], 'gprdat');
    disp(['line saved to: ', para.path, fname, '.gprd'])
    
    reply = input('Do you want more? Y/N [Y]: ', 's');
    if isempty(reply) || reply == 'y' ||reply == 'Y'
        reply = 1 ;
    else
        reply = 0;
    end
    
end


end