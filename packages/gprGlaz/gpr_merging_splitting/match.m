function [pos] = match(cal, data, tol)

%finds positions of "data" values in "cal" with tolerance "tol"
% IMPORTANT: Input 1D arrays must be horizontal
% cal is array pos is referring to
% data is array which values need to be matched with in prim
% tol is matching tolerance

% make matrices of the data and clibration vectors
data_m = repmat(data',size(cal));
cal_m = repmat(cal,size(data'));

% subtract the matrices
diff_m = cal_m - data_m;

% find the minimum of each column within the tolerance 
%r = find(abs(diff_m)<=tol);
[c r]=find(abs(diff_m)<=tol);
%ref_c = (1:length(r))
pos = zeros(1,length(data));
for i=1:length(data),
   test = find(i==c);
   if i==11080,
       i
   end    
   if isempty(test),
     pos(i)=0;
   elseif length(test)>1,
     pos(i)=min(r(test));
   else
     pos(i)=r(test);
   end
end
       

