
[fname,pname]=uigetfile({'*.sgy';'*.sgy'},'Choose Segy File to plot...','*.sgy');

[comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ...
     ReadSEGY([pname,fname]);
 
 procp = make_procp(comments);
 
 procp = SegyPlotData(fname,nsamp,sra,ntr,tti,data,zdem,zeff,procp,1,1);
 plotfile = sprintf('%s.jpg',fname); 
 print('-djpeg99','-r600',plotfile);
 

