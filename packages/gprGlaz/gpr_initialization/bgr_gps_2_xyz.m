function [] = bgr_gps_2_xyz(hd_pathfile,gps_xyz)

M = dlmread(char(strcat(hd_pathfile)),' ');
xx = M(:,3);
yy = M(:,4);
alt = M(:,7);
zone = repmat({'32 T'}, length(xx),1);
[lat,lon] = utm2deg(xx,yy,char(zone));
[y x h] = jwgs2sgr(lat,lon,alt);

dlmwrite(gps_xyz,[x y h],'-append','precision',8);