function [sdate,ntr,nsamp,sra,prx,pry,prz,freq,offset,data,tt,data2] = extract_content(content,content2,multi)  
% Input
% content   :structure as output from ReadPulsEkko.m
% content2  :structure as output from ReadPulsEkko.m
% multi=1:  data are multi component (detected by keyword scan in fname1)
% multi=0:  data are single component (detected by keyword scan in fname1) 
%
% Output:
% some components of "content" 

%delete last trace of content1 if ntr uneven, to make both datasets equal
%if mod(content.ntr,2)==1,
%    content.ntr = content.ntr-1; content.prx = content.prx-1;
%    content.pry = content.pry-1; content.prz = content.prz-1;
%    content.data = content.data(:,1:end-1);
%end   
%create arrays for cell elements of "content"
    sdate = content.sdate; ntr = content.ntr;  nsamp = content.nsamp;  sra = content.sra;
    prx = content.prx;  pry = content.pry;    prz = content.prz;
    freq = content.freq;  offset = content.offset;   data = content.data;
    tt = 0;
    if isfield(content,'tt'), tt = content.tt; end;
    data2=[]; 
    if multi>0, data2 = content2.data; 
        s1 = size(data); s2 = size(data2);
        if s1(2) > s2(2),
            data = data(:,1:end-1);ntr=ntr-1;
            prx=prx(1:end-1);prx=prz(1:end-1);prx=prz(1:end-1);
        end    
    end    