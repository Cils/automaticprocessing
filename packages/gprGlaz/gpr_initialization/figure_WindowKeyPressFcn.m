function figure_WindowKeyPressFcn(hObject, event_data)
    if strcmp(event_data.Key,'escape')
        ud=get(ancestor(hObject,'figure'),'UserData');
        ud{3}=1;
        set(ancestor(hObject,'figure'),'UserData',ud);
    end
 
    lastPlot=ud{2};
    if ~isempty(lastPlot)
        delete(lastPlot)
    end