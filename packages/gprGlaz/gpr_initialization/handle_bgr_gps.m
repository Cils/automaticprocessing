function [gps_xyz] = handle_bgr_gps(bgr_pathfile,hd_pathfile)
% Input
% bgr_pathfile:    complete path including filename of *.PAR file
% hd_pathfile:     complete path including filename of *.DST header file 

% Output:
% gps_xyz_1:    complete path including filename of (newly created) ch1 *.xyz
%               file with spatial coordinates

% In case of multi component measurements, split the two-channel GPS file in two separate ones
 

% Create 1 xyz gps ccordinate file
gps_xyz = [hd_pathfile(1:end-3),'XYZ'];
if exist(gps_xyz)==2, 
     fprintf('GPS File %s exists\n', gps_xyz);
   else  
     bgr_gps_2_xyz(hd_pathfile,gps_xyz);  
   end  
 end  


 
