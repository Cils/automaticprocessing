function plot_assignment(segfol,segfol2,prx,pry,prxv,pryv)
figure(1)
clf

choice = questdlg('Including background map in Plot for assigned profiles?','Plot Assigned Profiles','Yes','No','Yes');
switch choice
 case 'Yes'
   map = 1;
 case 'No'
   map = 0;
end
disp(['Including background map: ' choice ' was chosen'])

if map>0, 
    %chose and read background map file
    initPar = read_init();
    if isfield(initPar, 'lastBackgroundMap') && (min(initPar.lastBackgroundMap)~=0)
        defaultDir=initPar.lastBackgroundMap;
    else 
        defaultDir='*.tif';
    end
    
    [mapfile,mappath]=uigetfile({'*.tif';'*.tif'},'Choose geotiff for background map...',defaultDir);   
    [X, cmap, refmat, bbox]=geotiffread([mappath,mapfile]);
    write_init('lastBackgroundMap', [mappath '\' mapfile])
    if size(X,3)==4
        %get rid of X(:,:,4), otherwise mapshow will not work
        X=X(:,:,1:3);
    end
    
    mapshow(X,cmap,refmat);
    hold on;
end
if ~isempty(prxv)
    plot(prxv,pryv,'g.')
    hold on
    plot(prx,pry,'b.')
    legend('GPS of DVI','GPS of Geosat')
 else
    plot(prx,pry,'b.')
end
hold on;
title('Assigned profiles')
set(gca,'DataAspectRatio',[1 1 1]);
set(get(gcf, 'javaframe'),'Maximized',1);
axis([min(prx) max(prx) min(pry) max(pry)]);
yLim=ylim;
sift=(yLim(2)-yLim(1))/50;


wildcard = [segfol,'*profinfo.txt*'];
pick_list = dir(wildcard);
pick_list = {pick_list.name};
for b = 1:length(pick_list)
    pick_item = [segfol,pick_list{b}];
    profinfo = dlmread(pick_item);
    plot(profinfo(:,1),profinfo(:,2),'r-','linewidth',2);        
    [~,I]=min(profinfo([1,end],1));

    xs=profinfo(1,1);
    ys=profinfo(1,2);
    if I==1
        xshift=-sift;
    else
        xshift=0;
    end
    posProf=strfind(pick_list{b}, 'profil-');
    text(xs+xshift,ys,num2str(str2double(pick_list{b}(posProf+7:posProf+9))),'FontSize',14,'Color','m');
end;

disp('Save plot assigned Profiles...')
assignmentPlotFile = [segfol2 pick_list{b}(1:posProf-1) 'assignedProfiles.jpg'];
print('-djpeg99','-r600',assignmentPlotFile);