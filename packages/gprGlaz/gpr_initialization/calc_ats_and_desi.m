% Descirption:
%      calculate average trace spacing and difference in elevation of surface of intersections
%      plot the values and save it to root directory
% 
% Parameters:
%      segfol:   path to directory where segy files are stored
%      segfol2:  path to directory where figures will be saved to
function calc_ats_and_desi(segfol,pathSaveTo)

disp(['Calculate average trace spacing...'])
dd1 = dir(sprintf('%s*RAW*',segfol));
med = [];
low = [];
high = [];
pno = [];
allpx={};
allpy={};
allelev={};
for a = 1:length(dd1)
    if isempty(findstr(dd1(a).name,'_ch2'))
        [p,n,e] = fileparts(dd1(a).name);
        [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo,vel,head,azi,pitch,roll,hag] = ReadSEGY([segfol n]);
        posSegNr = strfind(dd1(a).name,'_profil-')+8;
        pno = [pno; str2double(dd1(a).name(posSegNr:posSegNr+2))];
        dx = diff(px);
        dy = diff(py);
        dh = sqrt(dx.^2 + dy.^2);
        med = [med;median(dh)];
        dh = sort(dh);
        low = [low; dh(round(0.25*length(dh)))];
        high = [high; dh(round(0.75*length(dh)))];
        allpx{end+1}=px;
        allpy{end+1}=py;
        allelev{end+1}=pz-hag;
    end
end;

figure(2)
clf;
plot(pno,med,'r*','linewidth',2);
hold on;
plot(pno,low,'k*','linewidth',2);
plot(pno,high,'k*','linewidth',2);
title('Average trace spacing')
xlabel('Profile number')
ylabel('Quantiles and mean distance')

disp('Save plot averageTraceSpacing...')
posSegNr = strfind(dd1(a).name,'profil-');
assignmentPlotFile = [pathSaveTo dd1(a).name(1:posSegNr-1) 'averageTraceSpacing.jpg'];
print('-djpeg99','-r600',assignmentPlotFile);




%calculate difference in elevation of surface of intersections
disp(['Calculate difference in elevation of surface of intersections...'])
diffArray=[];
ynames={};
for iprofile1=1:length(allpx)-1
    for iprofile2=iprofile1+1:length(allpx)
        if ~isempty(allpx{iprofile1}) && ~isempty(allpx{iprofile2})
            [x0,y0,i,j] = intersections(allpx{iprofile1},allpy{iprofile1},allpx{iprofile2},allpy{iprofile2},0);
            pos1 = floor(i); %cross pick positions in array which contains all other profiles
            pos2 = floor(j); %cross pick positions in actual array
            diffArray=[diffArray allelev{iprofile2}(pos2)-allelev{iprofile1}(pos1)];
            for i=1:length(pos2)
                ynames{end+1}=[num2str(iprofile2) '-' num2str(iprofile1)];
            end
        end
    end
end
if ~isempty(diffArray)
    figure(3)
    clf;
    barh(diffArray)
    ylim([0 length(ynames)+1])
    set(gca,'ytick',1:length(ynames))
    set(gca,'yticklabel',ynames)
    title('Difference in surface elevation of intersections')
    xlabel('Difference in surface elevation of intersections (m)')
    ylabel('profiles')

    disp('Save plot diffOfSurface...')
    posSegNr = strfind(dd1(a).name,'profil-');
    assignmentPlotFile = [pathSaveTo dd1(a).name(1:posSegNr-1) 'diffOfSurface.jpg'];
    print('-djpeg99','-r600',assignmentPlotFile);
end

%{
%calculate difference in elevation between GPS-laser and DEM
disp(['Calculate difference in elevation between GPS-laser and DEM...'])

DTMfol='C:\Users\LinoS\Documents\IceThicknessWorkingDirektory\GLACIERS\DTM\';

dd2 = dir([DTMfol '*.mat']);
n = length(dd2);
DTM={n};
for a = 1:n
    DTM{a}=load([DTMfol dd2(a).name]);
end

diffAll=[];
indProfile=zeros(length(allpx),1);
for iprofile=1:length(allpx)
    prx=allpx{iprofile};
    pry=allpy{iprofile};
    dif=ones(length(prx),1)*NaN;      
    for a=1:length(prx)
        for b=1:length(DTM)
            indx=find(abs(DTM{b}.x-prx(a))<=1,1);
            indy=find(abs(DTM{b}.y-pry(a))<=1,1);
            if ~isempty(indx) && ~isempty(indy) 
                dif(a)=allelev{iprofile}(a)-DTM{b}.z(indy,indx);
                continue
            end
        end
        if ~isnan(dif(a))
            continue
        end
        if a==length(prx)
            error('No DEM-Point within 2m')
        end
    end
    indProfile(iprofile)=length(diffAll)+1;
    diffAll=[diffAll; dif];
end


figure(4)
hold off
plot(diffAll)
hold on
plot((indProfile*[1 1])',ylim,'r:')
plot(xlim,[0 0],'k')
set(gca,'xtick',indProfile)
set(gca,'xticklabel',num2str(pno))
yLim=ylim;
if (yLim(1)<-100), ylim([-100 yLim(2)]); end
if (yLim(2)> 100), ylim([yLim(1) 100]); end
title('Difference in elevation between GPS-laser and DEM')
xlabel('Profile')
ylabel('GPS - laser - DEM (m)')

disp('Save plot diffGPS-DEM...')
posSegNr = strfind(dd1(1).name,'profil-');
assignmentPlotFile = [segfol2 dd1(1).name(1:posSegNr-1) 'diffGPS_DEM.jpg'];
print('-djpeg99','-r600',assignmentPlotFile);
%}