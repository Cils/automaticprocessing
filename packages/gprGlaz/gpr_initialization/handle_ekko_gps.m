function [gps_xyz_1] = handle_ekko_gps(gps_pathfile1,gps_pathfile2,content)
% Input
% ekko_pathfile1:    complete path including filename of ch1 *.dt1 file
% ekko_pathfile2:    complete path including filename of ch2 *.dt1 file
% gps_pathfile:      complete path including filename of *.gps file
% multi=1:  data are multi component (detected by keyword scan in fname1)
% multi=0:  data are single component (detected by keyword scan in fname1) 
%
% Output:
% gps_xyz_1:    complete path including filename of (newly created) ch1 *.xyz
%               file with spatial coordinates
% gps_xyz_2:    complete path including filename of (newly created) ch2 *.xyz
%               file with spatial coordinates

if strcmpi(gps_pathfile1(end-2:end),'GP2')
    posCh = strfind(gps_pathfile1,'-ch');
    gps_likeOldConsole = [gps_pathfile1(1:posCh-1),'.GPS'];
    gps_xyz_1 = [gps_pathfile1(1:posCh-1),'.xyz'];
    
    fid1 = fopen(sprintf('%s',gps_pathfile1),'rt');
    fid2 = fopen(sprintf('%s',gps_pathfile2),'rt');
    fid3 = fopen(sprintf('%s',gps_likeOldConsole),'wt');
    
    % find first line in File
    tline1 = '';
    status = 0;
    while ischar(tline1) && status == 0
        tline1 = fgetl(fid1); 
        tline2 = fgetl(fid2);
        if strcmp(tline1(1),';')
            continue;
        end
        posComma = strfind(tline1,',');
        [traceNum, status] = str2num(tline1(1:posComma(1)-1));
    end
       
    nextTraceNum = 1;
    for i=1:content.ntr
        if i>=nextTraceNum
            posString=tline1(posComma(2)+1:posComma(3)-1);
            gpsString=tline1(posComma(4)+2:end-1);
            tline1 = fgetl(fid1); 
            tline2 = fgetl(fid2);
            if ischar(tline1)
                if ~strcmp(tline1,tline2)
                    error([gps_pathfile1, ' and ', gps_pathfile1, ' are not the same. This code can not handle *.gp2 files that are the same.'])
                end
                posComma = strfind(tline1,',');
                nextTraceNum = str2double(tline1(1:posComma(1)-1));
            else
                nextTraceNum=inf;
            end
        end
        fprintf(fid3,'Trace #%i at position %s\n', i, posString);
        fprintf(fid3,'%s\n',gpsString);
    end   
    
    fclose(fid1);
    fclose(fid2);
    fclose(fid3);
    gps_pathfile1=gps_likeOldConsole;
else
    gps_xyz_1 = [gps_pathfile1(1:end-3),'xyz'];
end

if exist(gps_xyz_1,'file')==2, 
    fprintf('GPS File %s exists\n', gps_xyz_1);
else
    ekko_gps_2_xyz(gps_pathfile1,gps_xyz_1,1);
end