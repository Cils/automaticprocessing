function [fnames_out,pnames_out,Header,Tracefields,x,y,z] = open_gprmax()

% open *.out files
filename='*.out';
[fnames_out,pnames_out,index_out]=uigetfile({'*.out';'*.out'},'Choose gprmax outfiles...',filename,'MultiSelect','on');
% open *.geo file
%filename='*.geo';
%[fname_geo,pname_geo,index_geo]=uigetfile({'*.geo';'*.geo'},'Choose gprmax geofile...',filename);
% open reference out file
filename='*.out';
[fname_ref,pname_ref,index_ref]=uigetfile({'*.geo';'*.geo'},'Choose gprmax reference *.out file...',filename);
dummy = dir(char(strcat(pname_ref,fname_ref)));
dummysize = dummy.bytes;

if ischar(fnames_out), 
  fnames_out = cellstr(fnames_out);    
end

%loop over all chosen *.out files
for i = 1:length(fnames_out)
    
     testfile = dir(char(strcat(pnames_out,fnames_out(i))));
     testsize = testfile.bytes;
     
 %    strdigits = regexp(files(i).name,searchname,'tokens');
 %    TID(i,1) = str2double(strdigits{1});
     
    if testsize == dummysize
        [Header,Fields]=gprmax(char(strcat(pnames_out,fnames_out(i))));
        Header.rx;
    else  
        [Header,Fields]=gprmax_resize(char(strcat(pnames_out,fnames_out(i))));
    end
    
   % Tracefields.ex(:,i) = squeeze(Fields.ex);
   % Tracefields.ey(:,i) = squeeze(Fields.ey);
    Tracefields.ez(:,i) = squeeze(Fields.ez);
    Tracefields.hx(:,i) = squeeze(Fields.hx);
    Tracefields.hy(:,i) = squeeze(Fields.hy);
   % Tracefields.hz(:,i) = squeeze(Fields.hz);
   % Tracefields.ix(:,i) = squeeze(Fields.ix);
   % Tracefields.iy(:,i) = squeeze(Fields.iy);
   % Tracefields.iz(:,i) = squeeze(Fields.iz);
    
   % 2D
    x(1,i) = (Header.tx+(abs(Header.tx-Header.rx)/2))*Header.dx;                     %coordinates of middle between Tx and Rx
    y(1,i) = (Header.ty+(abs(Header.ty-Header.ry)/2))*Header.dy;
    z = 0;
    Header.NSteps = i;
  %  z(1,i) = (Header.tz+(abs(Header.tz-Header.rz)/2))*Header.dz;
   
    % 3D
%     x(1,i) = (Header.tx+(abs(Header.tx-Header.rx)/2))*Header.dx;                     %coordinates of middle between Tx and Rx
%     y(1,i) = (Header.ty+(abs(Header.ty-Header.ry)/2))*Header.dy;
%     z(1,i) = (Header.tz+(abs(Header.tz-Header.rz)/2))*Header.dz;
end