function [prx,pry,prz,prxv,pryv,przv,tti,zeff,hh,mm,ss,vel,head,azi,pitch,roll,hag,data,data2]...
                              = handle_spatial_information(gps_xyz_1,data, data2)
% Input
% gps_xyz_1:    complete path including filename of (newly created) ch1 *.xyz
%               file with spatial coordinates
% data:         the GPR data matrix 
%
% Output
% prx:          Easting coordinates of every trace in swiss grid as measured with GPS connected to
%               DVI console or geosat
% pry:          Northing coordinated of every trace in swuss grid as measured with GPS connected to
%               DVI console or geosat
% prz:          Elevation of every trace as measured with GPS connected to DVI console or geosat
% prxv:         Easting coordinates of every trace in swiss grid as measured with GPS connected to
%               DVI console or [] if no geosat-file available
% pryv:         Northing coordinated of every trace in swuss grid as measured with GPS connected to
%               DVI console or [] if no geosat-file available
% przv:         Elevation of every trace as measured with GPS connected to DVI console or [] if no geosat-file available
% tti:          Array with number of traces zero times (probably =0 at this
%               stage of processing)
% zeff:         elevation of ground point. Might be =0 if no information
%               avaiable such as a DEM or an laser altimeter number
% hh:           hours of every trace
% mm:           minutes of every trace
% ss:           seconds of every trace


coords = load(gps_xyz_1);
s1 = size(data);
if length(coords) > s1(2), coords = coords(1:end-1,:); end;
dim = size(coords);
dim_data1 = size(data);
if exist('data2','var') &&  ~isempty(data2) 
    dim_data2 = size(data2);
    len_data = [dim_data1(1),dim_data2(1)];
    col_data = [dim_data1(2),dim_data2(2)];
    if col_data(1) > col_data(2),
        data = data(:,1:end-1);
    elseif col_data(2) > col_data(1),
        data2 = data2(:,1:end-1);
    end    
    len_data = min(len_data);
    col_data = min(col_data);
else
    data2 = [];
    len_data = dim_data1(1);
    col_data = dim_data1(2);
end    

%take only every other coordinate in case gps file contains double the
%coordinates as traces in data (may happen in multi channel mode)
if dim(1)>col_data*2-10,
    coords = coords(2:2:end,:);
    dim_c = size(coords);
    if dim_c(1) > col_data,
        coords = coords(1:end-1,:);
    elseif dim_c(1) < col_data,
       data = data(:,1:end-1);
    end  
       
end    

ntr = length(coords);
prx = coords(:,1);
pry = coords(:,2);
prz = coords(:,3);
hh = coords(:,4);
mm = coords(:,5);
ss = coords(:,6);
    
% Handle to ask for GEOSAT file
choice = questdlg('Including Geosat File?','','Yes','No','No');
switch choice
 case 'Yes'
   disp([choice ' was chosen'])
   gsat = 1;
 case 'No'
   disp([choice ' was chosen'])
   gsat = 0;
end


if (gsat==1)
    %read in GEOSAT data
    prxv=prx; pryv=pry; przv=prz;
    defaultDir=fileparts(gps_xyz_1);
    [pry,prx,prz,zeff,vel,head,azi,pitch,roll,hag,keep] = read_geosat(hh,mm,ss,defaultDir);
    data = data(:,keep);
  
    if ~isempty(data2)
        data2 = data2(:,keep); 
    end
else
    vel=zeros(ntr,1); head=zeros(ntr,1); azi=zeros(ntr,1);
    pitch=zeros(ntr,1); roll=zeros(ntr,1); hag=zeros(ntr,1);
    zeff = zeros(ntr,1); % Ground elevation zeff = prz-(tti/2)*vair)
    prxv=[]; pryv=[]; przv=[];
    
    % eliminate coordinates that are equal to the previous one
    use = ones(ntr,1);
    dr = sqrt(diff(prx).^2 + diff(pry).^2);
    jj = find (dr < 1.0e-3);
    % kk = find(dr > 1); % find steps in coordinates to avoid interpolation to wrong side of step

    use(jj+1) = 0;
    use(ntr) = 1;
    jj0 = find(use == 0);
    jj1 = find(use == 1);

    %interpolate between deleted coordinates.
    prx(jj0) = interp1((jj1),prx(jj1),(jj0),'linear','extrap');

    pry(jj0) = interp1((jj1),pry(jj1),(jj0),'linear','extrap');

    prz(jj0) = interp1((jj1),prz(jj1),(jj0),'linear','extrap');

    %delete too weak traces
    ntr = length(prx);
    mean_tr=ones(1,ntr)*NaN;
    for i = 1:ntr
      mean_tr(1,i) = mean(abs(data(300:length(data(:,1)),i)),1);
    end
    kill_tr_1 = find(mean_tr <1);
    if ~isempty(kill_tr_1), 
      prx = removerows(prx,'ind',kill_tr_1);
      pry = removerows(pry,'ind',kill_tr_1);
      prz = removerows(prz,'ind',kill_tr_1);
    end; 
end

if max(zeff)==0, 
    zeff = prz;
end    

tti = zeros(ntr,1);  % Picked air traveltime
