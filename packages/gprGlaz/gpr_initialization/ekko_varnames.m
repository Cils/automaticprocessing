function [sarea,flight,sdate,tag,...
          ekko_pathfile1,ekko_pathtag1,ekko_tag1,ekko_file1,...
          ekko_pathfile2,ekko_pathtag2,ekko_tag2,ekko_file2,...
          gps_pathfile1,gps_file1,gps_pathfile2,gps_file2,...
          hd_pathfile1,hd_file1,hd_pathfile2,hd_file2]...
          = ekko_varnames(pname1,fname1,pname2,fname2,multi,demuxed);
         
                
% Input
% pname1:   Complete path to working directory with GPR files of channel 1
% fname1:   filename of channel 1 *.dt1 file 

%
% Output:
% sarea:            String including name of surveyed area
% flight:           String including flight number
% sdate:            String with survey date yyyy-mm-dd
% tag:              Name tag outputting *.seg files will contain
% ekko_pathfile1:   complete path including filename of ch1 *.dt1 file
% ekko_pathtag1:    same as ekko_pathfile1, but wihout file extension
% ekko_tag1:        ch1 *.dt1 file name without extension
% ekko_file1:       same as ekko_tag2 but with file extension
% ekko_pathfile2:   complete path including filename of ch2 *.dt1 file
% ekko_pathtag2:    same as ekko_pathfile2, but wihout file extension
% ekko_tag2:        ch2 *.dt1 file name without extension
% ekko_file2:       same as ekko_tag2 but with file extension
% gps_pathfile:     complete path including filename of *.gps file
% gps_file:         name of *.gps file without path
% hd_pathfile1:     complete path including filename of ch1 header file
% hd_file1:         name of ch1 header file without path
% hd_pathfile2:     complete path including filename of ch2 header file
% hd_file2:         name of ch2 header file without path

     
    
% extract variables tag, area, date from directory name
sarea_tmp = fliplr(pname1);
[token,remain] = strtok(sarea_tmp,filesep);
flight = fliplr(token);
[token,remain] = strtok(remain,filesep);
[token,remain] = strtok(remain,filesep);
tag = fliplr(token);
%tag = strtok(token,'\');
%tag = fliplr(token);
[sdate, sarea_tmp] = strtok(tag,'_'); 
sarea  = strtok(sarea_tmp,'_');
%flight = flight_tmp(2:end);



% define the hd and gps Ekko filenames and check if present
indPoints1=strfind(fname1, '.');
ekko_tag1 = fname1(1:indPoints1(end)-1);
ekko_file1 = fname1;
ekko_pathfile1 = [pname1,fname1];
ekko_pathtag1 = [pname1,ekko_tag1];
ekko_tag2 = '';
ekko_file2 = '';
ekko_pathfile2 = '';
ekko_pathtag2 = '';
% a differentiation has to be made, in case data where output by "demux" or not. 
if demuxed>0,
    indPoints2=strfind(fname1, '.');
    ekko_tag2 = fname2(1:indPoints2(end)-1);
    ekko_file2 = fname2;
    ekko_pathfile2 = [pname2,fname2];
    ekko_pathtag2 = [pname2,ekko_tag2];
    
    if exist([ekko_pathtag1,'.GPS'],'file')~=0
        gps_file1 = [ekko_tag1,'.GPS'];
        gps_file2 = '';
        gps_pathfile1 = [pname1,gps_file1];
        gps_pathfile2 = '';
    elseif exist([ekko_pathtag1,'.GP2'],'file')~=0 
        gps_file1 = [ekko_tag1,'.GP2']; 
        gps_file2 = [ekko_tag2,'.GP2']; 
        gps_pathfile1 = [pname1,gps_file1]; 
        gps_pathfile2 = [pname1,gps_file2];
    else
       error('GPS File not found\n');
    end
    
    hd_pathfile1 = [pname1,ekko_tag1,'.hd'];
    hd_file1 = [ekko_tag1,'.hd']; 
    hd_pathfile2 = [pname2,ekko_tag2,'.hd'];
    hd_file2 = [ekko_tag2,'.hd']; 
else
    gps_pathfile1 = [pname1,ekko_tag1,'.GPS'];
    gps_pathfile2 = '';
    gps_file1 = [ekko_tag1,'.GPS'];
    gps_file2 = '';
    hd_pathfile1 = [pname1,ekko_tag1,'.HD'];
    hd_file1 = [ekko_tag1,'.HD']; 
    hd_pathfile2 = '';
    hd_file2 = '';
end
if exist(gps_pathfile1,'file')~=2,
   fprintf('GPS File %s not found\n', gps_pathfile1);
   return
end
if exist(hd_pathfile1,'file')~=2,
    fprintf('Header File %s not found\n', hd_pathfile1);
    return
end
if multi>0 && exist(hd_pathfile2,'file')~=2,
    fprintf('Header File %s not found\n', hd_pathfile2);
    return
end
