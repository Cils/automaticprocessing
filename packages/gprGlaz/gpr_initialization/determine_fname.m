function [fname] = determine_fname(nr_files,fnames,k);
%creates the "fname" string out of reutrn cell array from "uigetfile"

%determine fname    
if nr_files < 2,
  fname = fnames;
else
  fname = char(fnames(k));
end  
