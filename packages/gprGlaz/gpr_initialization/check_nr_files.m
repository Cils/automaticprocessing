function [nr_files] = check_nr_files(fnames)

% check if multiple files were chosen
if iscell(fnames),
   nr_files = length(fnames);
else
   nr_files = 1;
end;   



