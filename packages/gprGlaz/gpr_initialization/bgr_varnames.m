function [sarea,flight,sdate,tag,...
          bgr_pathfile,bgr_pathtag,bgr_tag,bgr_file,...
          data_pathfile,data_file,hd_pathfile,hd_file]...
         = bgr_varnames(pname,fname);
% Input
% pname1:   Complete path to working directory with GPR files of channel 1
% fname1:   filename of channel 1 *.dt1 file 

%
% Output:
% sarea:            String including name of surveyed area
% flight:           String including flight number
% sdate:            String with survey date yyyy-mm-dd
% tag:              Name tag outputting *.seg files will contain
% bgr_pathfile:   complete path including filename of ch1 *.dt1 file
% bgr_pathtag:    same as bgr_pathfile1, but wihout file extension
% bgr_tag:        ch1 *.dt1 file name without extension
% bgr_file:       same as bgr_tag2 but with file extension
% data_pathfile:     complete path including filename of *.gps file
% data_file:         name of *.gps file without path
% hd_pathfile:     complete path including filename of ch1 header file
% hd_file:         name of ch1 header file without path


     
    
% extract variables tag, area, date from directory name
sarea_tmp = fliplr(pname);
[token,remain] = strtok(sarea_tmp,filesep);
flight = fliplr(token);
[token,remain] = strtok(remain,filesep);
[token,remain] = strtok(remain,filesep);
tag = fliplr(token);
[sdate, sarea_tmp] = strtok(tag,'_'); 
sarea  = strtok(sarea_tmp,'_');

% define the hd and gps bgr filenames and check if present
bgr_pathfile = [pname,fname];
bgr_pathtag = [pname,fname(1:end-4)];
bgr_tag = fname(1:end-4);
bgr_file = fname;
% a differentiation has to be made, in case data where output by "demux" or not. 
data_pathfile = [pname,fname(1:end-4),'.DAT'];
data_file = [fname(1:end-4),'.DAT'];
hd_pathfile = [pname,'Header',fname(1:6),fname(8:end-4),'.DST'];
hd_file = ['Header',fname(1:6),fname(8:end-4),'.DST']; 

if exist(data_pathfile)~=2,
   fprintf('*.DAT data File %s not found\n', par_pathfile);
   return
end
if exist(hd_pathfile)~=2,
   fprintf('*.DST Header File %s not found\n', hd_pathfile);
   return
end


