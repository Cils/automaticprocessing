function figure_WindowButtonMotionFcn(hObject, event_data, xk1, yk1, mpx, mpy, px, py)

    C = get (gca, 'CurrentPoint');
   
    xx=[xk1 C(1,1)];
    yy=[yk1 C(1,2)];
    xx = xx + mpx;
    yy = yy + mpy;
   
    [~,t1] = min((px-xx(1)).^2+(py-yy(1)).^2); %find first trace
    [~,t2] = min((px-xx(end)).^2+(py-yy(end)).^2);%find last trace   
    if (t1 < t2)
        ii = t1:t2;
    else
        ii = t2:t1;
    end
    
    p=plot(px(ii)-mpx,py(ii)-mpy,'c-','linewidth',2);
    
    ud=get(ancestor(hObject,'figure'),'UserData');
    lastPlot=ud{2};
    ud{2}=p;
    set(ancestor(hObject,'figure'),'UserData',ud);
    
    if ~isempty(lastPlot)
        delete(lastPlot)
    end
   
    
