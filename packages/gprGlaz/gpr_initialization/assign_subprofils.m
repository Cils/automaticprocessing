function [profidx,profinfo,nprof] = assign_subprofils(ekko_file1,nsamp,sra,ntr,data,prx,pry,prxv,pryv,hh,mm,ss)   

% this function creates by choice of user three arrays
% profidx = array as long as number of traces in 'data' assigning every
%           trace a subprofile number
% profinfo = spatial coordinates of segment points on profile, minimum
%            start and end point
% nprof = number of subprofiles

% Construct a questdlg with time and space option
choice = questdlg('Define subprofiles by time, by spatial coordinates or take the whole line?', ...
                 'Select Profile Assignment Mode',...
                 'Time', 'Spatial Coordinates','Line','Spatial Coordinates');
% Handle response
switch choice
case 'Time'
  time = 1;
  spat = 0;
case 'Spatial Coordinates'
  time = 0;
  spat = 1;
case 'Line'
  time = 0;
  spat = 0;
end
disp(['Select Profile Assignment Mode: ' choice ' was chosen'])

[p,n,e] = fileparts(ekko_file1);

if time==0 & spat==0,
 profidx = ones(length(prx),1);
 profinfo.x = [prx(1);prx(end)];
 profinfo.y = [pry(1);pry(end)];
 profinfo.n = 2;
 nprof = 1;
 
 clf 
 hold off;
 plot(prx,pry,'b.')
 hold on;
 set(gca,'DataAspectRatio',[1 1 1]);
 axis([min(prx) max(prx) min(pry) max(pry)]);
 for a = 1:nprof
     plot(profinfo.x,profinfo.y,'r-','linewidth',2);
 end;

elseif time==0,
  [profidx,profinfo,nprof] = SelectAssignProfiles(n,prx,pry,prxv,pryv);
else
  [profidx,profinfo,nprof] = SelectAssignTime(nsamp,sra,ntr,data,prx,pry,hh,mm,ss);  
  close all;
  [profidx,profinfo,nprof] = SelectAssign_TimeSubsections(n,prx,pry,prxv,pryv,profidx,profinfo,nprof);
end 