% Initialization Routine to convert gprmax output into sgy file

%read in gprmax files
[fnames_out,pnames_out,header,tracefields,x,y,z] = open_gprmax();
%[fnames_out,pnames_out,header,tracefields,x,y,z] = open_gprmax2();
fname_out = char(fnames_out(1));

%create data field
data = tracefields.ez;

% create missing folders
[segfol,segfol2,matfol] = create_gpr_subfolders(pnames_out);

% create names of variables and filenames
segyname = [segfol,'/',fname_out(1:end-4),'.sgy'];

% create comments array
ntr = header.NSteps; sra = header.dt*10^9; nsamp = header.iterations;
profinfo = struct('x',[min(x);max(x)],'y',[min(y);max(y)],'n',[2]); freq = 20; 
ddate = struct('y',0000,'m',00,'d',00,'dj',0000000,'hr',0,'min',0);
offset = x(2)-x(1); sarea = 'GPRmax'; zdem = zeros(1,ntr); tti = zeros(ntr,1);
zeff = zeros(ntr,1); b=1;
comments = make_comments(sarea,'GPRMax',b,freq,offset,'no dewow',ddate,...
                             ntr,sra,nsamp,profinfo,0,0,0);

% add white noise to data
% p = 0.0; b = mean(mean(data)) * p; a = -b;
% m = size(data);
% r = a + (b-a).*rand(m(1),m(2));
% data = data + r;

%create dummy orientation data vector for "GPR_write_segy" iput
vel=zeros(1,ntr); head=zeros(1,ntr); azi=zeros(1,ntr); 
pitch=zeros(1,ntr); roll=zeros(1,ntr); hag=zeros(1,ntr);

%rotate data matrix in dimension (nsamp, ntr)
%data=data';

% extend third coordinate
z = zeros(length(y));

%write segy file
GPR_write_segy(segyname, comments, offset, sra, x, z, y, data, zdem,...
               x, z, tti, zeff, b, ddate, vel, head, azi, pitch, roll, hag);