function [] = write_segy_subprofiles(segfol, data_file, data_file2, profidx, profinfo, nprof, multi, instr)

load(data_file);
if multi > 0, load(data_file2); end;  

prompt = {'Enter number of first profile'};
dlg_title = ' Define Number of first profile';
num_lines = 1;
defaultans = {'1'};
answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
fprof = str2num(answer{1});
profidx = profidx+fprof-1;

for b = fprof:nprof+fprof-1
    ii = find(profidx == b);
    data_cut = data(:,ii);
    if multi > 0, data_cut2 =  data2(:,ii); end 
    prx_cut = prx(ii);
    pry_cut = pry(ii);
    prz_cut = prz(ii);
    zeff_cut = zeff(ii);  
    vel_cut = vel(ii);
    head_cut = head(ii);
    azi_cut = azi(ii);
    pitch_cut = pitch(ii);
    roll_cut = roll(ii);
    hag_cut = hag(ii);
    
    profnr = b;          % Profile Number
    ntr = length(ii);    % Number of traces
    tti = zeros(ntr,1);  % Picked air traveltime
    zdem_cut = zeros(ntr,1);
    %zeff = zeros(ntr,1); % Ground elevation zeff = prz-(tti/2)*vair)
     
        
    % ----------------- Define sdate -----------------------------------
    ddate.y = str2num(sdate(1:4));                                  % Year
    ddate.m = str2num(sdate(6:7));                                  % Month
    ddate.d = str2num(sdate(9:10));                                % Day
    ddate.dj = juliandate(ddate.y,ddate.m,ddate.d);  % Day in Julian sdate
    ddate.hr = 0;                                 % Hour
    ddate.min = 0;                                % Minute
    sdate_short = strcat(sdate(1:4),sdate(6:7),sdate(9:10));
    
        
    % ----------------- PREPARE COMMENTS FOR TEXTUAL HEADER -----------
    comments = make_comments(sarea,instr,b,freq,offset,'dewsoft',ddate,...
                             ntr,sra,nsamp,profinfo,0,0,fprof);

    % ----------------- DEFINE FILE NAMES -----------------------------
    if b<10, prof = strcat('00',num2str(b)); 
    elseif 9<b & b<100, prof = strcat('0',num2str(b)); 
    elseif 99<b & b<1000, prof = num2str(b); 
    end
    profinfo_file = sprintf('%s%s%s_%s_PulsEKKO_%s_profil-%s_profinfo.txt',segfol,filesep,sdate_short,sarea,flight,prof);
    if multi > 0,           
       segyname =  sprintf('%s%s%s_%s_PulsEKKO_%s_profil-%s_ch1_RAW.sgy',segfol,filesep,sdate_short,sarea,flight,prof);
       segyname2 =  sprintf('%s%s%s_%s_PulsEKKO_%s_profil-%s_ch2_RAW.sgy',segfol,filesep,sdate_short,sarea,flight,prof);
    else
       segyname =  sprintf('%s%s%s_%s_PulsEKKO_%s_profil-%s_RAW.sgy',segfol,filesep,sdate_short,sarea,flight,prof); 
    end                       
    
    
    % -------- WRITE SEGMENT_POINT COORDINATES IN SEPARATE FILE -------  
    nr_seg = ones(profinfo(b-fprof+1).n,1) .* profinfo(b-fprof+1).n;
    M = [profinfo(b-fprof+1).x,profinfo(b-fprof+1).y,nr_seg];
    dlmwrite(profinfo_file,M,'precision','%.7f %.7f %d'); 
    
    
                         
    % ----------------- WRITE SEGY FILE -------------------------------
   
    %    ekko_file1(1:end-3));
        
    sprintf('%s%s_RAW_PROFILE.sgy',segfol,tag);
    GPR_write_segy(segyname, ...
                   comments, ...
                   offset, ...
                   sra, ...
                   prx_cut, pry_cut, prz_cut, ...
                   data_cut, zdem_cut, ...
                   prx_cut, pry_cut, tti, ...
                   zeff_cut, b, ddate,...
                   vel_cut, head_cut, azi_cut, ...
                   pitch_cut, roll_cut, hag_cut);
    sprintf('--- %s created ---',segyname)
        
    if multi > 0,
      GPR_write_segy(segyname2, ...
                     comments, ...
                     offset, ...
                     sra, ...
                     prx_cut, pry_cut, prz_cut, ...
                     data_cut2, zdem_cut, ...
                     prx_cut, pry_cut, tti, ...
                     zeff_cut, b, ddate,...
                     vel_cut, head_cut, azi_cut, ...
                     pitch_cut, roll_cut, hag_cut);
      sprintf('--- %s created ---',segyname2) 
    end 
        
      
       %j = j + 12; % time slot marker, one time entry has 6 digits, both start and end time entry 12 digits
    % loop over all digits of all time slots
 
end  