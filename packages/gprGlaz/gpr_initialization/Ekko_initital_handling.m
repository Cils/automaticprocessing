% Puls Ekko Initialization Routine
% Please name the directory with data files like the DATE and AREA you measured e.g. "20150410_Otemma_flight1",
% For usage of a background map, please have a swiss grid referenced
% geotiff file ready

% open *.dt1 file
[fname1,pname1,fname2,pname2,multi,demuxed] = open_dt1(); 
%[fname1,pname1,multi] = open_dt1(); 

% create missing folders
[segfol,segfol2,matfol,rootfol] = create_gpr_subfolders(pname1);

% create names of variables and filenames
    [sarea,flight,sdate,tag,...
    ekko_pathfile1,ekko_pathtag1,ekko_tag1,ekko_file1,...
    ekko_pathfile2,ekko_pathtag2,ekko_tag2,ekko_file2,...
    gps_pathfile1,gps_file1,gps_pathfile2,gps_file2,...
    hd_pathfile1,hd_file1,hd_pathfile2,hd_file2]...
    = ekko_varnames(pname1,fname1,pname2,fname2,multi,demuxed);

% Main read in process
content = ReadPulsEkko(ekko_pathtag1);
content2 = [];
if demuxed>0, content2 = ReadPulsEkko(ekko_pathtag2); end   

%demux multi channel data
if multi>0 && demuxed==0, 
   [content,content2] = ekko_demux(content);
elseif multi==0 && demuxed==1, 
   content2 = 0;
end   

% creation of *.xyz file with spatial coordinates
[gps_xyz_1] = handle_ekko_gps(gps_pathfile1,gps_pathfile2,content);


% create "content" cell array
[sdate,ntr,nsamp,sra,prx,pry,prz,freq,offset,data,tt,data2] = extract_content(content,content2,multi) ; 

% read in spatial coordinates from *.xyz file

[prx,pry,prz,prxv,pryv,przv,tti,zeff,hh,mm,ss,vel,head,azi,pitch,roll,hag,data,data2] = handle_spatial_information(gps_xyz_1,data,data2);

% define and create *.mat output files
[data_file,data_file2] = create_mat_files(matfol,ekko_file1,...
                                                      sdate,sarea,tag,flight,freq,offset,ntr,nsamp,sra,...
                                                      prx,pry,prz,data,data2,multi,zeff,...
                                                      vel,head,azi,pitch,roll,hag);
                                                     
% assign subprofiles either on a map (spatial) or on a radargram directly (time)
[profidx,profinfo,nprof] = assign_subprofils(ekko_file1,nsamp,sra,ntr,data,prx,pry,prxv,pryv,hh,mm,ss);


%loop over all subprofiles and write each subprofile into a segy file 
write_segy_subprofiles(segfol, data_file, data_file2, profidx, profinfo, nprof, multi, 'Ekko')

%plot assigned profiles
plot_assignment(segfol,rootfol,prx,pry,prxv, pryv)

%calculate average trace spacing and 
%difference in elevation of surface of intersections
calc_ats_and_desi(segfol,rootfol)

disp('Finished.')