function [segfol,segfol2,matfol,rootfol] = create_gpr_subfolders(pname1)
% Input
% pname1: Complete path to working directory with GPR files
%
% Output:
% segfol: path to directory where segy files will be stored
% segfol2: path to directory where output files will be stored
% matfol: path to directory where *.mat files will be stored
% rootfol: path to root directory

%create folders if necessary
segfol = strrep(pname1, 'L0', 'L1');
if ~exist(segfol,'dir'), mkdir(segfol), end;
segfol2 = strrep(pname1, 'L0', 'L2');
if ~exist(segfol2,'dir'), mkdir(segfol2), end;
matfol = [pname1,'matdata'];
matfol = strrep(matfol, 'L0', 'L1');
if ~exist(matfol,'dir'), mkdir(matfol), end;
rootfol = pname1(1:findstr(pname1, 'L0')-1);