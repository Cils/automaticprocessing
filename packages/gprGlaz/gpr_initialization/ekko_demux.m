function [content1,content2] = ekko_demux(content)

content1.data = content.data(:,1:2:end);
content2.data = content.data(:,2:2:end);
size1 = size(content1.data);
size2 = size(content2.data);

content1.sdate = content.sdate;
content2.sdate = content.sdate;

content1.ntr = size1(2);
content2.ntr = size2(2);

content1.nsamp = content.nsamp;
content2.nsamp = content.nsamp;

content1.sra = content.sra;
content2.sra = content.sra;

content1.prx = content.prx(1:2:end);
content2.prx = content.prx(2:2:end);
content1.pry = content.pry(1:2:end);
content2.pry = content.pry(2:2:end);
content1.prz = content.prz(1:2:end);
content2.prz = content.prz(2:2:end);

content1.freq = content.freq;
content2.freq = content.freq;

content1.offset = content.offset;
content2.offset = content.offset;

content1.multi = 1;
content2.multi = 1;

if isfield(content, 'tt')
    content1.tt = content.tt(1:2:end);
    content2.tt = content.tt(2:2:end);
end