% BGR (Refelx) Initialization Routine
% Please name the directory with data files like the DATE and AREA you measured e.g. "20150410_Otemma_flight1",
% For usage of a background map, please have a swiss grid referenced
% geotiff file ready

% open *.dt1 file
[fname,pname] = open_par(); 

% create missing folders
[segfol,segfol2,matfol,rootfol] = create_gpr_subfolders(pname);

% create names of variables and filenames
[sarea,flight,sdate,tag,...
 bgr_pathfile,bgr_pathtag,bgr_tag,bgr_file,...
 data_pathfile,data_file,hd_pathfile,hd_file]...
= bgr_varnames(pname,fname);

% creation of *.xyz file with spatial coordinates
[gps_xyz] = handle_bgr_gps(bgr_pathfile,hd_pathfile);


%%%%% to be continued from here

% Main read in process
content = ReadReflex(bgr_pathfile,data_pathfile);

%dummy variables
content2 = 0; multi = 0;

% create "content" cell array
[sdate,ntr,nsamp,sra,prx,pry,prz,freq,offset,data,tt,data2] = extract_content(content,content2,multi) ; 

% read in spatial coordinates from *.xyz file
[prx,pry,prz,prxv,pryv,przv,tti,zeff,hh,mm,ss,vel,head,azi,pitch,roll,hag] = handle_spatial_information(gps_xyz,data);

% define and create *.mat output files
[data_file,data_file2] = create_mat_files(matfol,bgr_file,...
                                                      sdate,sarea,tag,flight,freq,offset,ntr,nsamp,sra,...
                                                      prx,pry,prz,data,data2,multi,zeff,...
                                                      vel,head,azi,pitch,roll,hag);
                                                 
                                                     
% assign subprofiles either on a map (spatial) or on a radargram directly (time)
[profidx,profinfo,nprof] = assign_subprofils(bgr_file,nsamp,sra,ntr,data,prx,pry,prxv,pryv,hh,mm,ss);


%loop over all subprofiles and write each subprofile into a segy file 
write_segy_subprofiles(segfol, data_file, data_file2, profidx, profinfo, nprof, multi,'BGR')    
             
%plot assigned profiles
plot_assignment(segfol,rootfol,prx,pry,prxv, pryv)

%calculate average trace spacing and difference in elevation of surface of intersections
calc_average_trace_spacing(segfol,rootfol)

disp('Finished.')