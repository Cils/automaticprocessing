function [gnorth,geast,aelev,gelev,vel,head,azi,pitch,roll,hag,keep] = read_geosat(hh,mm,ss,defaultDir)
glat=[]; % LAtitude from goesat file
glon=[]; % Longitude from geosat fiule
aelev=[]; % Elevation of center of antenna frame
gelev=[]; % Elevation of center of antenna frame projected on ground
vel=[]; % Velocity above ground of antenna frame (m/s)
head=[]; % Heading of Helicopter
azi=[]; % HEading of Antenna frame
pitch=[]; % Pitch of antenna frame (negative nose downward)
roll=[]; % Roll fo antenna frame (negativ roll to the right)
hag=[]; % Height above ground as measured with the Laser altimeter
keep=[]; % positions to kill in the gpr data, as no geosat information available

if ~exist('filename','var'), filename='*.txt'; end
[fname,pname]=uigetfile({'*.txt';'*.txt'},'Choose GEOSAT ascii file ...', defaultDir );
fileID = fopen([pname,fname]);
fields = textscan(fileID,'%s %d32 %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %d','headerLines', 1);
fclose(fileID);

gtime = regexp(fields{1}, ':', 'split');
hh_geosat = ones(length(gtime),1)*NaN;
mm_geosat = ones(length(gtime),1)*NaN;
ss_geosat = ones(length(gtime),1)*NaN;
for i=1:length(gtime)
    hh_geosat(i) = str2double(gtime{i}(1));
    mm_geosat(i) = str2double(gtime{i}(2));
    ss_geosat(i) =str2double(gtime{i}(3));
end

tt=[];
for i=1:length(hh)
  h = find(hh_geosat==hh(i));
  m = find(mm_geosat==mm(i));
  s = find(ss_geosat==ss(i));
  kreuz1 = ismember(h,m);
  kreuz2 = ismember(h,s);
  matchpos = find(kreuz1 .* kreuz2);
  if isempty(matchpos), continue; end;
  matchpos = h(matchpos);
  keep = [keep,i];
  tt = [tt;hh(i)/24+mm(i)/24/60+ss(i)/24/60/60];
  glat = [glat;fields{3}(matchpos)];
  glon = [glon;fields{4}(matchpos)];
  aelev = [aelev;fields{5}(matchpos)];
  gelev = [gelev;fields{6}(matchpos)];
  vel = [vel;fields{7}(matchpos)];
  head  = [head;fields{8}(matchpos)];
  azi  = [azi;fields{9}(matchpos)];
  roll  = [roll;fields{10}(matchpos)];
  pitch  = [pitch;fields{11}(matchpos)];
  hag  = [hag;fields{15}(matchpos)];
end  

%smooth oversampled data
use = ones(length(tt),1);
dt = diff(tt);
jj = find (dt < 0.9/60/60/24);

use(jj+1) = 0;
use(end) = 1;
jj0 = find(use == 0);
jj1 = find(use == 1);

glat(jj0)  = interp1(jj1,glat(jj1) ,jj0,'linear','extrap');
glon(jj0)  = interp1(jj1,glon(jj1) ,jj0,'linear','extrap');
aelev(jj0) = interp1(jj1,aelev(jj1),jj0,'linear','extrap');
gelev(jj0) = interp1(jj1,gelev(jj1),jj0,'linear','extrap');
vel(jj0)   = interp1(jj1,vel(jj1)  ,jj0,'linear','extrap');
head(jj0)  = interp1(jj1,head(jj1) ,jj0,'linear','extrap');
azi(jj0)   = interp1(jj1,azi(jj1)  ,jj0,'linear','extrap');
roll(jj0)  = interp1(jj1,roll(jj1) ,jj0,'linear','extrap');
pitch(jj0) = interp1(jj1,pitch(jj1),jj0,'linear','extrap');
hag(jj0)   = interp1(jj1,hag(jj1)  ,jj0,'linear','extrap');

%conversion from wgs to siwssgrid
[x, y, aelev] = jwgs2sgr(glat,glon,aelev);
[gnorth, geast, gelev] = jwgs2sgr(glat,glon,gelev);

