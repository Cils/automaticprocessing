function [] = sgy_thead_mod(folder, old_string, new_string)

%takes all segy files in all subdirectories of "folder" 
%reads the text header, modifies it as defined with 
%"old_string" and "new_String" 
%it overwerites the old text header

% create a list of all segy files with in all subdirectories of "folder"
files_all = ReadFileNames(folder,{'sgy'});
hits=~cellfun(@isempty,regexp(files_all,'sgy'));
hit_indices = find(hits==1);
files = files_all([hit_indices]);


%loop over all files and write text header into array "thead"
%thead is then written into outfiles in same directory
k = 1;
m = 0;
n_coords=0;
coords = zeros(n_coords,3);
for i=1:length(files),
  segy_struct = segy_openFile(files{i},'r+','native','ascii');  
  thead = SEGY_ReadTextHeader(segy_struct,'ascii');
  thead = char(reshape(thead,40,80));
  %look for old string, if found: replace with new string
  s = size(thead);
  for j=1:s(1),
     hit = strfind(thead(j,:),old_string); 
     if j==s(1) && isempty(hit) && m~=1,
         sprintf('Original String not found in header!')
         break;
     end;
     if isempty(hit), continue; end;
     newstr = strrep(thead(j,:),old_string,new_string);  
     thead(j,:) = newstr(1:80); 
     thead=reshape(thead,3200,1);  
     SEGY_WriteTextHeader(segy_struct.FILE,thead,'ascii')  
     m = 1;
     fclose('all'); 
     sprintf('%s\n: changed header entry from %s to %s\n',...
         files{i}, old_string, new_string)
   end ; 
end; 

