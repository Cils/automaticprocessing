function data = tracenormalize(data)

[nsamp ntr] = size(data);

ii = find(isnan(data) == 1);
data(ii) = 0;
for a = 1:ntr
    dtmp = sort(abs(data(:,a)));
    scfac = dtmp(round(0.9*nsamp));
    data(:,a) = data(:,a)/scfac;
end;

