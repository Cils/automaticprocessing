function [xbed,ybed,zbed,zbed2,iweight,iweight2] = PickHorizon(px,py,params,params2,procp1,procp2)

% % define pick file name, and load if present, picks of actual profile
% if exist(sprintf('%s%sbedp.mat',params.matdata,params.tag)),
%   picks_file = sprintf('%s%sbedp.mat',params.matdata,params.tag);
%   load(picks_file);
% else
%   picks_file = sprintf('%s%sbedp.mat',params.matdata,params.tag);
% end;  

%load old picks, including all other profiles, to check for crosssections
wildcard = [params.pickfolder,'*bedrock.mat*'];
act_pickfile = sprintf('%s%sbedrock.mat',params.pickfolder,params.tag);
act_pickfile=strrep(act_pickfile,'ch1_','');act_pickfile=strrep(act_pickfile,'ch2_','');
pick_list = dir(wildcard);
pick_list = {pick_list.name};
xpick=[]; ypick=[]; zpick=[]; zpick2=[]; weightpick=[]; weightpick2=[]; 
indexNoIntersections=[];
nrOfLine=[]; nrOfLine2=[];
if ~strcmp(params2.fname,'')
    twoChannels=true;
else
    twoChannels=false;
end
%{
for b = 1:length(pick_list)
    pick_item = sprintf('%s',strcat(params.pickfolder,pick_list{b}));
    load(pick_item);
    load(pick_item);
    iweight(isnan(zbed))=0;
    zbed(isnan(zbed))=0;
    iweight2(isnan(zbed2))=0;
    zbed2(isnan(zbed2))=0;
    save(sprintf('%s',strcat(params.pickfolder,pick_list{b})),'xbed','ybed','zbed','zbed2','iweight','iweight2')
end
%}
for b = 1:length(pick_list)
    pick_item = sprintf('%s',strcat(params.pickfolder,pick_list{b}));
    load(pick_item);
    if strcmp(pick_item,act_pickfile)
        a_xbed = xbed; a_ybed = ybed; a_zbed = zbed; a_zbed2 = zbed2;
        a_iweight = iweight; a_iweight2 = iweight2;
    else
        xpick = [xpick,xbed]; ypick = [ypick,ybed]; zpick = [zpick,zbed]; zpick2 = [zpick2,zbed2];
        weightpick=[weightpick,iweight]; weightpick2=[weightpick2,iweight2];
        indexNoIntersections=[indexNoIntersections length(xpick)];
        nol = str2num(pick_list{b}(strfind(pick_list{b},'profil-')+[7:9]));
        nrOfLine=[nrOfLine ones(1,length(iweight))*nol];nrOfLine2=[nrOfLine2 ones(1,length(iweight2))*nol];
    end   
end
% set a_xbed etc to xbed
if (exist('a_xbed')), 
 xbed = a_xbed; ybed = a_ybed; zbed = a_zbed; zbed2 = a_zbed2;
 iweight = a_iweight; iweight2 = a_iweight2;
end;

xbed = px;
ybed = py;
  

if ~exist('a_xbed'),      % chek for old picks
    zbed = zeros(1,length(px));     % empty matrices 
    iweight = zeros(1,length(px));
    zbed2 = zeros(1,length(px));
    iweight2 = zeros(1,length(px));
end 


% Find intersections with previously picked lines and plot them on section
hold on;
if ~isempty(xpick),
    [x0,y0,i,j] = intersections(xbed,ybed,xpick,ypick,0);
    pos_act = floor(i); %cross pick positions in array which contains all other profiles
    pos_other = floor(j); %cross pick positions in actual array
    %remove intersections with connecting line between two profiles
    for i=1:length(indexNoIntersections)
        inToDelete = pos_other==indexNoIntersections(i);
        pos_act(inToDelete)=[];
        pos_other(inToDelete)=[];
    end
else
    pos_act=[];
    pos_other=[];
end

if twoChannels
    params3=params;
    params3.tag = strrep(params.tag, 'ch1', 'ch1+ch2');
    if exist(sprintf('%s%sprocp.mat',params3.matdata,params3.tag),'file'),
        procp_file3 = sprintf('%s%sprocp.mat',params3.matdata,params3.tag);
        procp3=load(procp_file3);
        procp3=procp3.procp;
    end
    % check for existense of procp3, if not present, copy procp1
    if ~exist('procp3','var'),
        procp3 = procp1;
    end
    procp3.maxdepth = procp1.maxdepth;
    procp3.zexag = procp1.zexag;

    if (max(abs(params.tti-params2.tti))>0) || ((abs(params.sra-params2.sra))>0) || (max(abs(params.pz-params2.pz))>0) || (max(abs(params.hag-params2.hag))>0)
        error('tti, sra, pz and hag of ch1 and ch2 have to be the same to be able to do the following step')
    end
    params3.nsamp=min(size(params.data,1),size(params2.data,1));
    params3.data = params.data(1:params3.nsamp,:)+params2.data(1:params3.nsamp,:);
end

bedrockPlots = {};
dist = (0:params.ntr-1)*procp1.dri;
button = 0; 
activeFigure = 0;
plotPick = true;
axisequal = true;
xLimDefault = [];
yLimDefault = [];
while(button ~= 27)                     % while not ESC
    
    if (button==0) || (button == 99)                                % if c pressed or first time
        % create 2 picking window2 with radargram section for channel 1 and
        % channel 2
        warning('off', 'MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
        Title = sprintf('Bedrock Picks: %s ',params.tag);
        adj = (button == 99); % adj if button c pressed
        if (activeFigure==0) || (activeFigure==1)
            h=figure(1);
            clf;
            procp1=SegyPlotData(Title,procp1,params,4,adj);
            if adj
                procp_file = sprintf('%s%sprocp.mat',params.matdata,params.tag);
                procp=procp1;
                save(procp_file,'procp'); 
            end
            drawnow;pause(0.1);
            set(get(handle(h), 'javaframe'),'Maximized',1);
            drawnow;
            xLimDefault = xlim;
            yLimDefault = ylim;
        end
        if twoChannels
            if (activeFigure==0) || (activeFigure==2)
                h=figure(2);
                clf;
                Title = sprintf('Bedrock Picks: %s ',params2.tag);
                procp2=SegyPlotData(Title,procp2,params2,4,adj);  
                if adj
                    procp_file2 = sprintf('%s%sprocp.mat',params2.matdata,params2.tag);
                    procp=procp2;
                    save(procp_file2,'procp'); 
                end
                drawnow;pause(0.1);
                set(get(h,'JavaFrame'),'Maximized',1);
                drawnow;
                xLimDefault = xlim;
                yLimDefault = ylim;
            end
            if (activeFigure==0) || (activeFigure==3)
                h=figure(3);
                clf;
                Title = sprintf('Bedrock Picks: %s ',params3.tag);
                procp3=SegyPlotData(Title,procp3,params3,4,adj);  
                if adj
                    procp_file3 = sprintf('%s%sprocp.mat',params3.matdata,params3.tag);
                    procp=procp3;
                    save(procp_file3,'procp'); 
                end
                drawnow;pause(0.1);
                set(get(h,'JavaFrame'),'Maximized',1);
                drawnow;
                xLimDefault = xlim;
                yLimDefault = ylim;
            end
        end
        if plotPick
            bedrockPlots = PlotBedrockPicks(bedrockPlots,dist,zbed,zbed2,iweight,iweight2,zpick,zpick2,weightpick,weightpick2,pos_act,pos_other,nrOfLine,nrOfLine2,twoChannels);
        end
            
        if (activeFigure==0) && twoChannels, activeFigure=3; end;
        if (activeFigure==0) && ~twoChannels, activeFigure=1; end;
        figure(1); if axisequal, axis equal; else axis normal; end; axis([xLimDefault yLimDefault]); 
        if twoChannels
            figure(2); if axisequal, axis equal; else axis normal; end; axis([xLimDefault yLimDefault]); 
            figure(3); if axisequal, axis equal; else axis normal; end; axis([xLimDefault yLimDefault]); 
        end
        figure(activeFigure);
    end
    
    [xx,yy,button] = ginput(1);
    if     (button == 49), activeFigure=1; figure(activeFigure);    % if 1 pressed
    elseif (button == 50 && twoChannels), activeFigure=2; figure(activeFigure);    % if 2 pressed
    elseif (button == 51 && twoChannels), activeFigure=3; figure(activeFigure);    % if 3 pressed
    elseif (button == 43), 
        figure(1);zoom(1.5);
        if twoChannels
            figure(2);zoom(1.5);
            figure(3);zoom(1.5);
        end
        bedrockPlots = PlotBedrockPicks(bedrockPlots,dist,zbed,zbed2,iweight,iweight2,zpick,zpick2,weightpick,weightpick2,pos_act,pos_other,nrOfLine,nrOfLine2,twoChannels);
        figure(activeFigure);                                       % if + pressed
    elseif (button == 45), 
        figure(1);zoom(1/1.5);
        if twoChannels
            figure(2);zoom(1/1.5);
            figure(3);zoom(1/1.5);
        end
        bedrockPlots = PlotBedrockPicks(bedrockPlots,dist,zbed,zbed2,iweight,iweight2,zpick,zpick2,weightpick,weightpick2,pos_act,pos_other,nrOfLine,nrOfLine2,twoChannels);
        figure(activeFigure);                                       % if - pressed
    elseif (button == 28)                                           % button left arrow
        xLim=xlim;
        vxlim=(xLim(2)-xLim(1))/3;
        figure(1);axis([xlim-vxlim ylim]);
        if twoChannels
            figure(2);axis([xlim-vxlim ylim]);
            figure(3);axis([xlim-vxlim ylim]);
        end
        figure(activeFigure);  
    elseif (button == 29)                                           % button right arrow
        xLim=xlim;
        vxlim=(xLim(2)-xLim(1))/3;
        figure(1);axis([xlim+vxlim ylim]);
        if twoChannels
            figure(2);axis([xlim+vxlim ylim]);
            figure(3);axis([xlim+vxlim ylim]);
        end
        figure(activeFigure);  
    elseif (button == 30)                                           % button arrow up
        yLim=ylim;
        vylim=(yLim(2)-yLim(1))/3;
        figure(1);axis([xlim ylim+vylim]);
        if twoChannels
            figure(2);axis([xlim ylim+vylim]);
            figure(3);axis([xlim ylim+vylim]);
        end
        bedrockPlots = PlotBedrockPicks(bedrockPlots,dist,zbed,zbed2,iweight,iweight2,zpick,zpick2,weightpick,weightpick2,pos_act,pos_other,nrOfLine,nrOfLine2,twoChannels);
        figure(activeFigure);  
    elseif (button == 31)                                           % button arrow down
        yLim=ylim;
        vylim=(yLim(2)-yLim(1))/3;
        figure(1);axis([xlim ylim-vylim]);
        if twoChannels
            figure(2);axis([xlim ylim-vylim]);
            figure(3);axis([xlim ylim-vylim]);
        end
        bedrockPlots = PlotBedrockPicks(bedrockPlots,dist,zbed,zbed2,iweight,iweight2,zpick,zpick2,weightpick,weightpick2,pos_act,pos_other,nrOfLine,nrOfLine2,twoChannels);
        figure(activeFigure);  
    elseif (button == 114)                                           % if r pressed
        figure(1);axis([xLimDefault yLimDefault]);
        if twoChannels
            figure(2);axis([xLimDefault yLimDefault]);
            figure(3);axis([xLimDefault yLimDefault]);
        end
        bedrockPlots = PlotBedrockPicks(bedrockPlots,dist,zbed,zbed2,iweight,iweight2,zpick,zpick2,weightpick,weightpick2,pos_act,pos_other,nrOfLine,nrOfLine2,twoChannels);
        figure(activeFigure);   
    elseif (button == 97)                                           % if a pressed
        axisequal = ~axisequal;
        figure(1); if axisequal, axis equal; else axis normal; end
        if twoChannels
            figure(2); if axisequal, axis equal; else axis normal; end
            figure(3); if axisequal, axis equal; else axis normal; end
        end
        figure(activeFigure);   
    elseif (button == 109)                                           % if m pressed
        for i=1:length(zbed)
            if  zbed(i)==0 && zbed2(i)~=0
                zbed(i)=zbed2(i);
                iweight(i)=iweight2(i);
            end
        end
        zbed2(1:end)=0;
        iweight2(1:end)=0;
        plotPick = true;
        bedrockPlots = PlotBedrockPicks(bedrockPlots,dist,zbed,zbed2,iweight,iweight2,zpick,zpick2,weightpick,weightpick2,pos_act,pos_other,nrOfLine,nrOfLine2,twoChannels);
        figure(activeFigure)
    elseif (button == 112)                                           % if p pressed
        plotPick = ~plotPick;
        if plotPick
            bedrockPlots = PlotBedrockPicks(bedrockPlots,dist,zbed,zbed2,iweight,iweight2,zpick,zpick2,weightpick,weightpick2,pos_act,pos_other,nrOfLine,nrOfLine2,twoChannels);    
        else
            for i=1:length(bedrockPlots)
                if ~isempty(bedrockPlots{i})
                    delete(bedrockPlots{i})
                end
            end
        end
        figure(activeFigure)
    elseif (button == 100)                                           % if d pressed
        cax = gca;
        waitforbuttonpress;
        point1 = cax.CurrentPoint;   
        rbbox;              
        point2 = cax.CurrentPoint;  
        point1 = point1(1,1:2); point2 = point2(1,1:2);
        p1 = min(point1,point2);
        p2 = max(point1,point2);
        
        ftrace = find(dist > min(p1(1)),1,'first');
        ltrace = find(dist < max(p2(1)),1,'last');
        for i=ftrace:ltrace
            if zbed(i)>p1(2) && zbed(i)<p2(2)
                zbed(i)=0;
                iweight(i)=0;
            end
            if zbed2(i)>p1(2) && zbed2(i)<p2(2)
                zbed2(i)=0;
                iweight2(i)=0;
            end
        end
        plotPick = true;
        bedrockPlots = PlotBedrockPicks(bedrockPlots,dist,zbed,zbed2,iweight,iweight2,zpick,zpick2,weightpick,weightpick2,pos_act,pos_other,nrOfLine,nrOfLine2,twoChannels);
        figure(activeFigure)
    elseif (button == 1)                                            % if left mouse buttons pressed
        [dbed,ztmp] = getline(gca);
        if (length(dbed) < 2)
            continue; 
        end; 
        
        %don't use last (right) klick
        dbed = dbed(1:end-1);
        ztmp = ztmp(1:end-1);
        
        ftrace = find(dist > min(dbed),1,'first');
        ltrace = find(dist < max(dbed),1,'last');
        zbedi = interp1(dbed,ztmp,dist(ftrace:ltrace),'linear');
        
        %plot in both open figures
        figure(1);
        hold on;
        h1a = plot(dist(ftrace:ltrace),zbedi,'k-','linewidth',1.5);
        h1b = plot(dbed,ztmp,'ko','linewidth',1.5);
        if twoChannels
            figure(2)
            hold on;
            h2a = plot(dist(ftrace:ltrace),zbedi,'k-','linewidth',1.5);
            h2b = plot(dbed,ztmp,'ko','linewidth',1.5); 
            figure(3)
            hold on;
            h3a = plot(dist(ftrace:ltrace),zbedi,'k-','linewidth',1.5);
            h3b = plot(dbed,ztmp,'ko','linewidth',1.5); 
        end
        bedrockPlots{end+1} = h1a;
        bedrockPlots{end+1} = h1b;
        if twoChannels
            bedrockPlots{end+1} = h2a;
            bedrockPlots{end+1} = h3a;
            bedrockPlots{end+1} = h2b;
            bedrockPlots{end+1} = h3b;
        end
        
        w = inputdlg('Enter weight [0]');    
        w = str2num(w{1});
        if (isempty(w) == 1)
          w = 1;
        end;  
%         n=0;
%         for i=ftrace:ltrace   
%           n=n+1;
%           if (zbed(1,i) == 0)
%               zbed(1,i) = zbedi(1,n);
%               iweight(1,i) = w;
%           else
%               zbed2(1,i) = zbedi(1,n);    % for mutliple bedrock options.
%               iweight2(1,i) = w;
%           end
        if(max(zbed(1,ftrace:ltrace))<=0)
            zbed(1,ftrace:ltrace) = zbedi;
            iweight(1,ftrace:ltrace) = w;
        else
            zbed2(1,ftrace:ltrace) = zbedi;
            iweight2(1,ftrace:ltrace) = w;
        end
        plotPick = true;
        bedrockPlots = PlotBedrockPicks(bedrockPlots,dist,zbed,zbed2,iweight,iweight2,zpick,zpick2,weightpick,weightpick2,pos_act,pos_other,nrOfLine,nrOfLine2,twoChannels);
        figure(activeFigure)
    elseif (button == 3),                                            % if right mouse buttons pressed
        if exist('h1a','var')
            clearvars 'h1a'
            % delete newely plotted bedrock
            if max(zbed2(ftrace:ltrace))>0, 
                zbed2(ftrace:ltrace)=0;
                iweight2(ftrace:ltrace)=0;
            else    
                zbed(ftrace:ltrace)=0;
                iweight(ftrace:ltrace)=0;
            end;
        elseif max(zbed2)>0
            zbed2 = zeros(1,length(px));
            iweight2 = zeros(1,length(px));
        else
            zbed = zeros(1,length(px));
            iweight = zeros(1,length(px));
        end;
        plotPick = true;
        bedrockPlots = PlotBedrockPicks(bedrockPlots,dist,zbed,zbed2,iweight,iweight2,zpick,zpick2,weightpick,weightpick2,pos_act,pos_other,nrOfLine,nrOfLine2,twoChannels);
        figure(activeFigure)
    end  
end

bedrockPlots = PlotBedrockPicks(bedrockPlots,dist,zbed,zbed2,iweight,iweight2,zpick,zpick2,weightpick,weightpick2,pos_act,pos_other,nrOfLine,nrOfLine2,twoChannels);