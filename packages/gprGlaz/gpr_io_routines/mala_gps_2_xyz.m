function [ ] = mala_gps_2_xyz(gps_file,outfile,sg)

% ekko_gps_2_xyz reads in a Puls Ekko *.GPS file 
% and writes xyz coordinates as three column ascii file to "outfile"  

% sg=1 conversion to Swiss grid
% sg=0 leave lat lon coordinates


if (nargin<3), sg=1; end;

fid = fopen(sprintf('%s',gps_file),'rt');
tt = textscan(fid,'%d%d-%d-%d%d:%d:%d%fN%fE%fM%d');
fclose(fid);

lat_decdeg = tt{8};
long_decdeg = tt{9};
alt = tt{10};
if (sg == 1),
  [x y h] = jwgs2sgr(lat_decdeg,long_decdeg,alt);
end;

%write gps coord file
fid = fopen(sprintf('%s',outfile),'wt');
%fprintf(fid,'Northing Easting Height \n');
for i=1:length(x);
    fprintf(fid,'%f %f %f\n',x(i),y(i),h(i));
end;   
fclose(fid);
fprintf('New file %s created!! \n',outfile)


 


