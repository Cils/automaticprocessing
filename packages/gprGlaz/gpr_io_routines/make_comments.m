function comments = make_comments(area,instr,profnr,freq,offset,dewsoft,ddate,ntr,sra,nsamp,profinfo,procp,proc_steps,fprof)
% proc1 = 1:    Intitial processing applied
% picsurf = 1;  Surface picking done
% proc2 = 1;    Second processing applied
% mig = 1;      Migration applied

%b = Profile number
%fprof = Number of first profile

C = 1;

% ------------------ GENERAL INFORMATION ----------------------------------
comments{C,1} = sprintf('C%02d Date: %02d/%02d/%04d\tLINE: %03d\tAREA: %s',...
                            C, ddate.m,ddate.d,ddate.y, profnr, area); C = C+1;
comments{C,1} = sprintf('C%02d INSTRUMENT: %s',...
                        C, instr); C = C+1;
comments{C,1} = sprintf('C%02d OFFSET: %02f\tFREQUENCY: %i',...
                        C, offset, freq); C = C+1;                  
comments{C,1} = sprintf('C%02d DATA TRACES: %d\tSAMPLE INTERVAL: %f\tNUMBER SAMPLES: %d',...
                        C, ntr, sra, nsamp); C = C+1;
comments{C,1} = sprintf('C%02d DEWOW SOFTWARE: %s',...
                        C, dewsoft); C = C+1;
comments{C,1} = sprintf('C%02d',C); C = C+1;    % empty


% ------------------ PROCESSING PARAMETERS --------------------------------
comments{C,1} = sprintf('C%02d PROCESSING PARAMETERS: (not necessarily applied)', C); C = C+1;

% fill processing section in comments with empty lines
if ~isfield(procp,'twin'),
  comments{C,1} = sprintf('C%02d',C); C = C+1; % empty
  comments{C,1} = sprintf('C%02d',C); C = C+1; % empty
  comments{C,1} = sprintf('C%02d',C); C = C+1; % empty
  comments{C,1} = sprintf('C%02d',C); C = C+1; % empty
  comments{C,1} = sprintf('C%02d',C); C = C+1; % empty
else
%or write lines with set processing parameter
% make sure that these keyswords are similar to the ones in make_procp.m
  comments{C,1} = sprintf('C%02d TIME WINDOW: %d\tMAXDEPTH: %d\tBANDPASS: %d %d %d',...
                            C, procp.twin, procp.maxdepth, procp.fl, procp.fh, procp.ford); C = C+1;
  comments{C,1} = sprintf('C%02d VAIR: %f\tVICE: %f',...
                            C, procp.vair, procp.vice); C = C+1;
  comments{C,1} = sprintf('C%02d HILBWIN START: %d\tHILBWIN LENGTH: %d\tDRI: %f',...
                           C, procp.hilbfirst, procp.hilbwin, procp.dri); C = C+1;
  comments{C,1} = sprintf('C%02d SVD LENGTH: %d\tFX-DECON LENGTH: %d\tGLOBALZSHIFT: %d',...
                            C, procp.svd_len, procp.decon_len, procp.GlobalZshift); C = C+1;
  comments{C,1} = sprintf('C%02d SET TIME ZERO: %f',C,procp.tzer); C = C+1;                      
end  
comments{C,1} = sprintf('C%02d',C); C = C+1; % empty

% ------------------ APPLIED PROCESSING STEPS -----------------------------
comments{C,1} = sprintf('C%02d PROCESSING APPLIED:', C); C = C+1;

if isfield(proc_steps,'twin'),
  if proc_steps.twind > 0, 
    comments{C,1} = sprintf('C%02d \tREDUCED TIME WINDOW',C); C = C+1;
  end

  if proc_steps.svdd > 0,
    comments{C,1} = sprintf('C%02d \tSVD FILTER',C); C = C+1;
  end
  if proc_steps.bgrd > 0, 
    comments{C,1} = sprintf('C%02d \tBACKGROUND REMOVAL',C); C = C+1;
  end

  if proc_steps.butterd > 0, 
    comments{C,1} = sprintf('C%02d \tBUTTERWORTH FILTER',C); C = C+1;
  end

  if proc_steps.surfd > 0, 
    comments{C,1} = sprintf('C%02d \tPICKED SURFACE REFLECTION',C); C = C+1;
  end

  if proc_steps.bind > 0, 
    comments{C,1} = sprintf('C%02d \tTRACE BINNING',C); C = C+1;
  end

  if proc_steps.hild > 0, 
    comments{C,1} = sprintf('C%02d \tHILBERT ENVELOPE GAIN',C); C = C+1;
  end

  if proc_steps.agcd > 0, 
    comments{C,1} = sprintf('C%02d \tAGC GAIN',C); C = C+1;
  end

  if proc_steps.mgaind > 0, 
    comments{C,1} = sprintf('C%02d \tMANUAL GAIN SETTINGS',C); C = C+1;
  end

  if proc_steps.fxdd > 0, 
    comments{C,1} = sprintf('C%02d \tFX DECONVOLUTION',C); C = C+1;
  end

  if proc_steps.kmigd > 0, 
    comments{C,1} = sprintf('C%02d \tKIRCHHOFF MIGRATION',C); C = C+1;
  end

  if proc_steps.mapd > 0, 
    comments{C,1} = sprintf('C%02d \tMAP CREATED',C); C = C+1;
  end

  if proc_steps.bedd > 0,
    comments{C,1} = sprintf('C%02d \tGLACIER BED PICKED',C); C = C+1;
  end

  if proc_steps.splitd > 0,
    comments{C,1} = sprintf('C%02d \tSPLIT TOO LONG PROFILES',C); C = C+1;
  end
  
  if proc_steps.tzerd > 0,
    comments{C,1} = sprintf('C%02d \tTIME ZERO SET',C); C = C+1;
  end
end;  

comments{C,1} = sprintf('C%02d END PROCESSING',C); C = C+1;
comments{C,1} = sprintf('C%02d',C); C = C+1; % empty


% ------------------ PROFINFO ---------------------------------------------
if exist('profinfo'),
%n_pnts = profinfo.n 
 b = profnr-fprof+1;
 if length(profinfo)<2, b = 1; end; %in case line number larger than size of profinfo (will happen when make_comments is called durinf processing
 %n_pnts = profinfo(b).n;
 n_pnts = 2;
 comments{C,1} = sprintf('C%02d START AND END POINT:',...
                 C); C = C+1;
% 
% for i = 1:n_pnts-1
 comments{C} = sprintf('C%02d %f %f;\t%f %f',...
                   C, profinfo(b).x(1,1), profinfo(b).y(1,1), profinfo(b).x(end,1), profinfo(b).y(end,1)); C = C+1;
% end
end;
 

comments{39,1} = sprintf('C39 SEG Y REV1');
comments{40,1} = sprintf('C40 END OF TEXTUAL HEADER');

if C >38
   sprintf('WARNING: Too many header lines')
end
