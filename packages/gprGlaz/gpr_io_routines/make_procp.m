function procp = make_procp(comments);

% -------------------------------------------------------------------------
% ------------------- DEFINE PROCP STRUCTURE WITH DEFAULT VALUES-----------
% -------------------------------------------------------------------------

load blkwhtred.mat
procp.cmap = blkwhtred;     % Colormap for displaying sections
procp.cax = [-1.0 1.0];     % Lower and upper bounds of colorbar
procp.tfun = zeros(100,1);  % exponential scaling factor for vertical axis
procp.zexag = 2;            % Vertical exaggeration for plotting
procp.vair = 0.299;         % Air velocity
procp.vice = 0.1689;        % Ice velocity
procp.fl =10;              % Low cut frequency for bandpass filter
procp.fh = 50;             % High cut frequency for bandpass filter
procp.ford = 4;             % Order of the Butterworth bandpass filter
procp.twin = 4000;          % Length of trace time window
procp.maxdepth = 400;       % Maximum depth [m] for displaying data
procp.hilbfirst = 0;      % Start time of Hilbert envelope window
procp.hilbwin = 500;        % Length of time window of Hilbert envelope [ns]
procp.dri = 1;              % Trace interpolation distance, used for binning
procp.svd_len = 50;          % Length of SVD filter (0 if no SVD)
procp.decon_len = 0;        % fx-deconvolution filter length (0 if no decon)
procp.agclen = 0;           % Length of AGC filter (0 if no agc)
procp.GlobalZshift = 0;     % z shift over all profiles
procp.t0_corr = 0;          % time-zero correction for PulseEkko Ground data
procp.hilbtask = 0;         % 0 = no adjust., 1 = manual, 2 = interactive
procp.hilb_incr = 0;        % increment to increase/decrease the hilbert window length 
procp.decon_mu = 0.02;        % fx deconvolution pre whitening factor
procp.tzer = 0.0;          % default time zero

[lines,columns]=size(comments);

for i=1:lines
 k = strfind(comments(i,:),'MAXDEPTH');
 if ~isempty(k), procp.maxdepth = str2num(comments(i,k+10:k+13)); k=[]; end 
 k = strfind(comments(i,:),'BANDPASS');
 if ~isempty(k), procp.fl = str2num(comments(i,k+10:k+12)); 
                 procp.fh = str2num(comments(i,k+13:k+15)); k=[]; end 
 k = strfind(comments(i,:),'VAIR');
 if ~isempty(k), procp.vair = str2num(comments(i,k+6:k+14)); k=[]; end 
 k = strfind(comments(i,:),'VICE');
 if ~isempty(k), procp.vice = str2num(comments(i,k+6:k+14)); k=[]; end   
 k = strfind(comments(i,:),'HILBWIN START');
 if ~isempty(k), procp.hilbfirst = str2num(comments(i,k+15:k+18)); k=[]; end    
 k = strfind(comments(i,:),'HILBWIN LENGTH');
 if ~isempty(k), procp.hilbwin = str2num(comments(i,k+16:k+20)); k=[]; end 
 k = strfind(comments(i,:),'DRI');
 if ~isempty(k), procp.dri = str2num(comments(i,k+5:k+13)); k=[]; end 
 k = strfind(comments(i,:),'SVD LENGTH');
 if ~isempty(k), procp.svd_len = str2num(comments(i,k+12:k+14)); k=[]; end
 k = strfind(comments(i,:),'FX-DECON LENGTH');
 if ~isempty(k), procp.decon_len = str2num(comments(i,k+16:k+19)); k=[]; end 
 k = strfind(comments(i,:),'GLOBALZSHIFT');
 if ~isempty(k), procp.GlobalZshift = str2num(comments(i,k+14:k+17)); k=[]; end 
 k = strfind(comments(i,:),'SET TIME ZERO');
 if ~isempty(k), procp.tzer = str2num(comments(i,k+15:k+20)); k=[]; end 
end

% save('matdata/procp.mat','procp')