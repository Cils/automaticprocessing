function SelectProfiles(data_file,profile_file,ax)
% Define 2D profiles
% data_file: 'name'_.mat file
% profile_file: .mat file containing profile trace information

load(data_file);
ccprof = [];
xyp = [];
nprof = 0;
first = 1;

while(1)
    clf;
    title('Press left button to pick profile or right button to quit');
     [xx,yy,button]=ginput(1);
    
    % button=0: left mouse click
    % button=3: right mouse click
    
     if ((button == 3) & (first == 0))
         break;
     end;
    
    first = 0;
    plot(px,py,'b.');
    title('SelectProfiles: Select Profile Line')
    hold on;
    set(gca,'DataAspectRatio',[1 1 1]);
    
    if (nargin > 2)
        axis(ax);
    else
        axis([min(px) max(px) min(py) max(py)]);
    end;
    
    for a = 1:nprof
        plot(prof(a).x,prof(a).y,'k-','linewidth',1.5);
    end;
    
    [xx,yy] = getline;
    nprof = nprof + 1;
    prof(nprof).x = xx;
    prof(nprof).y = yy;
    prof(nprof).n = length(xx);
    plot(prof(nprof).x,prof(nprof).y,'r-','linewidth',1.5);
    hold off;
    save(profile_file,'prof','nprof');
    title('Press any key to continue');
    [xx,yy,button]=ginput(1);
end;
