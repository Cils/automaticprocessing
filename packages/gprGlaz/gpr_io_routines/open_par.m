function [fname1,pname1] = open_par()
% Output
% fname1 = filename of channel 1 *.dt1 file 
% pname1 = complete path to location of fname1

initPar = read_init();
if isfield(initPar, 'lastParRawFileDir')
    defaultDir=initPar.lastParRawFileDir;
    if defaultDir==0
        defaultDir='';
    end
else 
    defaultDir='';
end

[fname1,pname1]=uigetfile({'*.PAR';'*.PAR'},'Choose BGR (Reflex) PAr File...',defaultDir);
if pname1==0
    error('Please choose a file')
end

write_init('lastParRawFileDir', pname1)

