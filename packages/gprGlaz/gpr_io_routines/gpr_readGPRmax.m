function [gprdat n] = gpr_readGPRmax(para, n)

% function for reading in gprmax.out files into processing routine
% [Header,Fields] = gprmax(name)necessary (additional scripts from GPRmax website)

fid = fopen([para.path para.datafile],'rb');
if(fid == -1)
    error('Binary input file (*.out) does not exist');
end

name = para.datafile;

[Header,Fields] = gprmax(name);                                                                         %function for reading in Gprmay binary output file

if Header.polarization == 120           %transverse
    gprdat.data = squeeze(Fields.ex(:,1,:));                                                            % always gives the strongest electromagnetic field component for the specific antenna orientation
elseif Header.polarization == 121       %axial
    gprdat.data = squeeze(Fields.ey(:,1,:));   
else 
    gprdat.data = squeeze(Fields.ez(:,1,:));
end

gprdat.nr_of_traces = Header.NSteps;
gprdat.nr_of_samples = Header.iterations;
gprdat.t0 = [];
gprdat.time_window = Header.removed/1e-9;

if Header.TxStepX ~= 0
    gprdat.separation = (Header.rx - Header.tx)*Header.dx;
elseif Header.TxStepY ~= 0
    gprdat.separation = (Header.ry - Header.ty)*Header.dy;   
else %Header.TxStepZ ~= 0
    gprdat.separation = (Header.rz - Header.tz)*Header.dz;
end
    
gprdat.frequency = (str2num(Header.title))/1e6;                                                     
gprdat.sampling_rate = Header.dt*1e9;
gprdat.number_stacks = []
gprdat.dataz   = [];

gprdat.x       = (Header.tx + (Header.TxStepX * (0:1:Header.NSteps-1)))*Header.dx;                     %coordinates of transmitter
gprdat.y       = (Header.ty + (Header.TxStepY * (0:1:Header.NSteps-1)))*Header.dy;
gprdat.z       = (Header.tz + (Header.TxStepZ * (0:1:Header.NSteps-1)))*Header.dz;
gprdat.pos0    = [];


if nargin > 1
    n = n +1;
end