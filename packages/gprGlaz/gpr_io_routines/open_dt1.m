function [fname1,pname1,fname2,pname2,multi,demuxed] = open_dt1()
% Output
% fname1 = filename of channel 1 *.dt1 file 
% pname1 = complete path to location of fname1

initPar = read_init();
if isfield(initPar, 'lastDt1RawFileDir')
    defaultDir=initPar.lastDt1RawFileDir;
    if defaultDir==0
        defaultDir='';
    end
else 
    defaultDir='';
end


[fname1,pname1]=uigetfile({'*.DT1';'*.DT1'},'Choose Puls-Ekko File with ch01...',defaultDir);
fname2 = '';
pname2 = '';
if pname1==0
    error('Please choose a file')
end

write_init('lastDt1RawFileDir', pname1)

%ask if Sensorts & Softeware demux was used
choice1 = questdlg('File was demuxed with Sensors & Software tool?','','Sensors','New Console','No demux','New Console');
switch choice1
case 'No demux'   
    % Handle to ask for Multi channel option
    choice2 = questdlg('Does the *.dt1 file containing two channel data?','','Yes','No','No');
    switch choice2
    case 'Yes'
        disp([choice2 ' was chosen'])
        multi = 1;
    case 'No'
        disp([choice2 ' was chosen'])
        multi = 0;
    end
    demuxed=0;
case 'Sensors'
    %old multi channel detector, when demux is done with Sensors&Software
    %EkkoDemux
    multi = findstr(fname1,'Ch');
    if length(multi)>0,
        multi=1;
    if ~exist('filename','var'), filename='*.DT1'; end
        [fname2,pname2]=uigetfile({'*.DT1';'*.DT1'},'Choose Puls-Ekko File with ch02...',pname1);   
    else
        multi=0;
    end
    demuxed=1;
case 'New Console'
    wildcard = [pname1,fname1(1:strfind(fname1,'-ch')+2),'*',fname1(strfind(fname1,'-ch')+4:end)];
    pick_list = dir(wildcard);
    pick_list = {pick_list.name};
    if length(pick_list) ~= 2
        error(['There are ' num2str(length(pick_list)) ' channels. This code works only for two channels.'])
    end
    for b = 1:length(pick_list)
        if ~strcmp(pick_list{b},fname1)
            pname2=pname1;
            fname2=pick_list{b};
            multi=1;
            demuxed=1;
        end
    end
    if strcmp(pname2,'')
        error(['Could not find another channel named ' wildcard])
    end
end    