function [t0 HP LP GainPoints] = ReadGssiGain(infile)

fid = fopen(infile,'r');

while(~feof(fid))
    line = fgetl(fid);
    
    if strfind(line,'Range Gain (dB)')
        GainPoints = sscanf(line,'%*s %*s %*s %f %f %f %f %f');
        if (length(GainPoints) == 3)
            line = fgetl(fid);
            GainPoints = [GainPoints;sscanf(line,'%f %f')];
        end;
    end
    
    
    if strfind(line,'Vert IIR LP N =1 F =')
        LP = cell2mat(textscan(line,'%*20c %f '));
    end
    if strfind(line,'Vert IIR HP N =1 F =')
        HP = cell2mat(textscan(line,'%*20c %f '));
    end
    if strfind(line,'Position Correction')
        t0 = cell2mat(textscan(line,'%*s %*s %f '));
    end
    
end

fclose(fid);