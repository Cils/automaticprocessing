function [gprdat n] = gpr_exportSEGY(indat, params,n)

%GPR_EXPORTSEGY exports data to segy file
%  SEGYMAT toolbox needed!!
gprdat = indat;
dummy = ones(1,indat.nr_of_traces);
WriteSegy(params.fname,indat.data,'dt',indat.sampling_rate);

if params.cflag ==1
    xx = (10^params.pprec).*linspace(indat.pos0(1,1),indat.pos0(2,1),indat.nr_of_traces);
    yy = (10^params.pprec).*linspace(indat.pos0(1,2),indat.pos0(2,2),indat.nr_of_traces);
    zz = (10^params.pprec).*indat.z;
elseif params.cflag ==2
    xx = (10^params.pprec).*indat.x;
    yy = (10^params.pprec).*zeros(size(xx));
    zz = (10^params.pprec).*indat.z;
end

WriteSegyTraceHeaderValue(params.fname,xx,'key','SourceX')
WriteSegyTraceHeaderValue(params.fname,yy,'key','SourceY')
WriteSegyTraceHeaderValue(params.fname,zz,'key','SourceSurfaceElevation')

WriteSegyTraceHeaderValue(params.fname,xx,'key','GroupX')
WriteSegyTraceHeaderValue(params.fname,yy,'key','GroupY')
WriteSegyTraceHeaderValue(params.fname,zz,'key','ReceiverGroupElevation')

WriteSegyTraceHeaderValue(params.fname,xx,'key','cdpX')
WriteSegyTraceHeaderValue(params.fname,yy,'key','cdpY')

WriteSegyTraceHeaderValue(params.fname,-(10^params.pprec),'key','ElevationScalar')
WriteSegyTraceHeaderValue(params.fname,-(10^params.pprec),'key','SourceGroupScalar')

WriteSegyTraceHeaderValue(params.fname,dummy.*indat.nr_of_samples,'key','ns')
WriteSegyTraceHeaderValue(params.fname,1:indat.nr_of_traces,'key','cdp')



if nargin > 2
    n = n +1;
end

end

