function [profidx,prof,nprof] = SelectAssignProfiles(fname,px,py,pxv,pyv)
if ~exist('pxv','var')
    pxv=[];
    pyv=[];
end

% Handle to ask for map 
choice = questdlg('Including background map?','','Yes','No','No');
switch choice
 case 'Yes'
   map = 1;
 case 'No'
   map = 0;
end
disp(['Including background map: ' choice ' was chosen'])

if map>0, 
    %chose and read background map file
    initPar = read_init();
    if isfield(initPar, 'lastBackgroundMap') && (min(initPar.lastBackgroundMap)~=0)
        defaultDir=initPar.lastBackgroundMap;
    else 
        defaultDir='*.tif';
    end
    
    [mapfile,mappath]=uigetfile({'*.tif';'*.tif'},'Choose geotiff for background map...',defaultDir);   
    [X, cmap, refmat, bbox]=geotiffread([mappath,mapfile]);
    write_init('lastBackgroundMap', [mappath '\' mapfile])
    if size(X,3)==4
        %get rid of X(:,:,4), otherwise mapshow will not work
        X=X(:,:,1:3);
    end
end

% Construct a questdlg with Type of projection
choice = questdlg('Do you want to draw yourself the profile and project the data to your profile or only choose start and endpoint and use the measured profile?', ...
    'Type of projection',...
    'Project', 'Start/Endpoint', 'Start/Endpoint');
% Handle response
switch choice
    case 'Project'
        projection = 1;
    case 'Start/Endpoint'
        projection = 0;
end
disp(['Type of projection: ' choice ' was chosen'])

ntr = length(px);
nprof = 0;
profidx = nan*ones(ntr,1);

if map>0
    mpx = 0;
    mpy = 0;
else
    mpx = min(px);
    mpy = min(py);
end

while(1)
    hold off;
    if map
        mapshow(X,cmap,refmat);
        hold on;
    end
    if ~isempty(pxv)
        plot(pxv-mpx,pyv-mpy,'g.')
        hold on
        plot(px-mpx,py-mpy,'b.')
        legend('GPS of DVI','GPS of Geosat')
    else
        plot(px-mpx,py-mpy,'b.')
        hold on;
    end
    set(gca,'DataAspectRatio',[1 1 1]);
    axis([min(px)-mpx max(px)-mpx min(py)-mpy max(py)-mpy]);
    for a = 1:nprof
        plot(prof(a).x-mpx,prof(a).y-mpy,'k-','linewidth',2);
    end;
    if projection==1
        title([fname ':Select Profile Line. !!!Use right button for final point on a line!!!)'],'interpreter','none');
        [xx,yy] = getline;
    else
        title([fname ':Select start- and endpoint of Profile Line.'],'interpreter','none');
        ud={{},{},1};
        while ~isempty(ud{3})
            [xk1,yk1] = ginput(1);
            plot(xk1,yk1,'c*');
            ud{3}={};
            set(gcf, 'UserData',ud)
            set(gcf, 'WindowKeyPressFcn',@figure_WindowKeyPressFcn)
            set(gcf, 'WindowButtonMotionFcn', {@figure_WindowButtonMotionFcn,  xk1, yk1, mpx, mpy, px, py})
            set(gcf, 'WindowButtonDownFcn',@figure_WindowButtonDownFcn)
            while isempty(ud{1}) && isempty(ud{3})
                waitfor(gcf,'UserData');
                ud=get(gcf,'UserData'); 
            end
            set(gcf, 'WindowButtonMotionFcn','')
            set(gcf, 'WindowButtonDownFcn','')
            set(gcf, 'WindowKeyPressFcn','')
        end
        posret=ud{1};
        xk2 = posret(1);
        yk2 = posret(2);
        plot(xk2,yk2,'c*');
        xx=[xk1 xk2];
        yy=[yk1 yk2];
    end
    xx = xx + mpx;
    yy = yy + mpy;
    if (length(xx) < 2), break; end;
    nprof = nprof + 1;

    % ----------- ASSIGN TRACES TO SELECTED PROFILE LINE ------------------
    [~,t1] = min((px-xx(1)).^2+(py-yy(1)).^2); %find first trace
    [~,t2] = min((px-xx(end)).^2+(py-yy(end)).^2);%find last trace   
    if (t1 < t2)
        ii = t1:t2;
    else
        ii = t2:t1;
    end;
    if projection==1
        prof(nprof).x = xx;
        prof(nprof).y = yy;
        prof(nprof).n = length(xx);
    else
        prof(nprof).x = px(ii);
        prof(nprof).y = py(ii);
        prof(nprof).n = length(ii);
    end
    profidx(ii) = nprof; %write profile number into profidx

    plot(prof(nprof).x-mpx,prof(nprof).y-mpy,'r-','linewidth',2);
    plot(px(t1)-mpx,py(t1)-mpy,'g*');
    plot(px(t2)-mpx,py(t2)-mpy,'g*'); 

    title('Press ESC to finish. Otherwise klick or press any key to continue');
    w = waitforbuttonpress;
    % if ESC pressed
    if w==1 && get(gcf,'CurrentCharacter') == 27
        break;
    end
 end;
 