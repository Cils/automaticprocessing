function comments = make_comments(b,area,instr,profnr,freq,offset,dewsoft,ddate,ntr,sra,nsamp,procp,profinfo,proc1,picksurf,proc2,mig)
% proc1 = 1:    Intitial processing applied
% picsurf = 1;  Surface picking done
% proc2 = 1;    Second processing applied
% mig = 1;      Migration applied

%b = Profile number


C = 1;

% ------------------ GENERAL INFORMATION ----------------------------------
comments{C,1} = sprintf('C%02d Date: %02d/%02d/%04d\tLINE: %03d\tAREA: %s',...
                            C, ddate.m,ddate.d,ddate.y, profnr, area); C = C+1;
comments{C,1} = sprintf('C%02d INSTRUMENT: %s',...
                        C, instr); C = C+1;
comments{C,1} = sprintf('C%02d OFFSET: %02f\tFREQUENCY: %s',...
                        C, offset, freq); C = C+1;                  
comments{C,1} = sprintf('C%02d DATA TRACES: %d\tSAMPLE INTERVAL: %f\tNUMBER SAMPLES: %d',...
                        C, ntr, sra, nsamp); C = C+1;
comments{C,1} = sprintf('C%02d DEWOW SOFTWARE: %s',...
                        C, dewsoft); C = C+1;
comments{C,1} = sprintf('C%02d',C); C = C+1;    % empty


% ------------------ PROCESSING PARAMETERS --------------------------------
comments{C,1} = sprintf('C%02d PROCESSING PARAMETERS:', C); C = C+1;

if proc1 == 0;
   comments{C,1} = sprintf('C%02d',C); C = C+1; % empty
   comments{C,1} = sprintf('C%02d',C); C = C+1; % empty
   comments{C,1} = sprintf('C%02d',C); C = C+1; % empty
   comments{C,1} = sprintf('C%02d',C); C = C+1; % empty
end

if proc1 == 1;
   comments{C,1} = sprintf('C%02d TIME WINDOW: %d\tMAXDEPTH: %d\tBANDPASS: %d %d %d',...
                            C, procp.twin, procp.maxdepth, procp.fl, procp.fh, procp.ford); C = C+1;
   comments{C,1} = sprintf('C%02d VAIR: %f\tVICE: %f',...
                            C, procp.vair, procp.vice); C = C+1;
   comments{C,1} = sprintf('C%02d HILBWIN START: %d\tHILBWIN LENGTH: %d\tDRI: %f',...
                            C, procp.hilbfirst, procp.hilbwin, procp.dri); C = C+1;
   comments{C,1} = sprintf('C%02d SVD LENGTH: %d\tFX-DECON LENGTH: %d\tGLOBALZSHIFT: %d',...
                            C, procp.svd_len, procp.decon_len, procp.GlobalZshift); C = C+1;
end                   
comments{C,1} = sprintf('C%02d',C); C = C+1; % empty

% ------------------ APPLIED PROCESSING STEPS -----------------------------
comments{C,1} = sprintf('C%02d PROCESSING APPLIED:', C); C = C+1;

if proc1 == 0 % Check if initial processing was done
   %comments{C,1} = sprintf('C%02d DEWOW\tDEFINE PROFILES',C); C = C+1;  % Dewow applied
   comments{C,1} = sprintf('C%02d   \tDEFINE PROFILES',C); C = C+1; % No Dewow applied
   comments{C,1} = sprintf('C%02d',C); C = C+1; % empty
end

if proc1 ==1 
   %comments{C,1} = sprintf('C%02d DEWOW\tDEFINE PROFILES\tREDUCE TIME WINDOW\tREMOVE CONSISTENT BACKGROUND NOISE',C); C = C+1;% Dewow applied
   comments{C,1} = sprintf('C%02d   \tDEFINE PROFILES\tREDUCE TIME WINDOW',C); C = C+1; % Now Dewow
    if picksurf == 0
       comments{C,1} = sprintf('C%02d SVD FILTER\tBUTTWERWORTH BANDPASS FILTER',C); C = C+1;
    end
    if picksurf ==1
       comments{C,1} = sprintf('C%02d SVD FILTER\tBUTTWERWORTH BANDPASS FILTER\tPICK SURFACE REFLECTION',C); C = C+1;
    end
end


if proc2 == 0 % Check if second processing was done
   comments{C,1} = sprintf('C%02d',C); C = C+1; % empty
end
if proc2 == 1
   comments{C,1} = sprintf('C%02d INCLUDE TOPOGRAPHY\tBINNING\tHILBERT ENVELOPE GAIN\tFX-DECON',C); C = C+1;
end


if mig == 0 % Check if migration was done
   comments{C,1} = sprintf('C%02d',C); C = C+1; % empty
end
if mig == 1
   comments{C,1} = sprintf('C%02d MIGRATION\tTRACE NORMALIZATION',C); C = C+1;
end

comments{C,1} = sprintf('C%02d END PROCESSING',C); C = C+1;
comments{C,1} = sprintf('C%02d',C); C = C+1; % empty


% ------------------ PROFINFO ---------------------------------------------
%n_pnts = profinfo.n 
if isfield(profinfo,'y'),
n_pnts = 2
comments{C,1} = sprintf('C%02d NUMBER OF SEGMENT POINTS: %02d\tStartpoint\tEndpoint',...
                 C, n_pnts); C = C+1;
% 
 for i = 1:n_pnts-1
 comments{C} = sprintf('C%02d %f %f;\t%f %f',...
                   C, profinfo.x(i,1), profinfo.y(i,1), profinfo.x(i+1,1), profinfo.y(i+1,1)); C = C+1;
 end
end

comments{39,1} = sprintf('C39 SEG Y REV1');
comments{40,1} = sprintf('C40 END OF TEXTUAL HEADER');

if C >38
   sprintf('WARNING: Too many header lines')
end
