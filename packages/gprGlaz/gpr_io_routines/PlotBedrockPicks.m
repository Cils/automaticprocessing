% plot bedrock picks and intersections, give back a cell array containing the
% handles to the plots.
% All plots in "plotToDeleteHandles" will be deleted.
function bedrockPlotHandle = PlotBedrockPicks(plotToDeleteHandles,dist,zbed,zbed2,iweight,iweight2,zpick,zpick2,weightpick,weightpick2,pos_act,pos_other,nrOfLine,nrOfLine2,twoChannels)
% delete all plots
for i=1:length(plotToDeleteHandles)
    if ~isempty(plotToDeleteHandles{i})
        delete(plotToDeleteHandles{i})
    end
end

% calculate indices of picks that are weighted 1 or not 1
posWeightOne   = find(zbed  & (iweight  == 1));
posWeightElse  = find(zbed  & (iweight  ~= 1));
posWeightOne2  = find(zbed2 & (iweight2 == 1));
posWeightElse2 = find(zbed2 & (iweight2 ~= 1));

% find pics of inetersections that are weighted 1 or not 1
i_otherWeightOne     = find(zpick(pos_other)  .* (weightpick(pos_other) ==1));
i_otherWeightElse    = find(zpick(pos_other)  .* (weightpick(pos_other) ~=1));
i_otherWeightOne2    = find(zpick2(pos_other) .* (weightpick2(pos_other) ==1));
i_otherWeightElse2   = find(zpick2(pos_other) .* (weightpick2(pos_other) ~=1));
%i_otherNopick        = find(~(zpick(pos_other)|zpick2(pos_other)));

bedrockPlotHandle={};
if twoChannels, allFigures = 1:3; else allFigures = 1; end
for plotChannel=allFigures
    figure(plotChannel);
    yLim=ylim;
    sift=(yLim(2)-yLim(1))/25;
    % plot picks and intersections
    
    hold on;
    if ~isempty(pos_act)
        bedrockPlotHandle{end+1} = plot([1;1]*dist(pos_act),ylim'*ones(1,length(pos_act)),'--','Color',0.4*[1 1 1]);
    end
    bedrockPlotHandle{end+1} = plot(dist(posWeightOne),zbed(posWeightOne),'g.','MarkerSize',5);
    bedrockPlotHandle{end+1} = plot(dist(posWeightElse),zbed(posWeightElse),'r.','MarkerSize',5);
    bedrockPlotHandle{end+1} = plot(dist(posWeightOne2),zbed2(posWeightOne2),'c.','MarkerSize',5);
    bedrockPlotHandle{end+1} = plot(dist(posWeightElse2),zbed2(posWeightElse2),'m.','MarkerSize',5);
    bedrockPlotHandle{end+1} = plot(dist(pos_act(i_otherWeightOne)),zpick(pos_other(i_otherWeightOne)),'g*','MarkerSize',12,'LineWidth',1.3);
    bedrockPlotHandle{end+1} = plot(dist(pos_act(i_otherWeightElse)),zpick(pos_other(i_otherWeightElse)),'r*','MarkerSize',12,'LineWidth',1.3);
    bedrockPlotHandle{end+1} = plot(dist(pos_act(i_otherWeightOne2)),zpick2(pos_other(i_otherWeightOne2)),'c*','MarkerSize',12,'LineWidth',1.3);
    bedrockPlotHandle{end+1} = plot(dist(pos_act(i_otherWeightElse2)),zpick2(pos_other(i_otherWeightElse2)),'m*','MarkerSize',12,'LineWidth',1.3);
   
    % plot annotations
    for i=1:length(pos_other)
        %{
        if weightpick(pos)==1 && zpick(pos)~=0
            bedrockPlotHandle{end+1} = text(dist(pos_act(i)),zpick(pos_other(i))-sift,num2str(nrOfLine(pos)),'FontSize',14);
        elseif weightpick(pos)~=1 && zpick(pos)~=0
            bedrockPlotHandle{end+1} = text(dist(pos_act(i)),zpick(pos_other(i))-sift,num2str(nrOfLine(pos)),'FontSize',14);
        end

        if weightpick2(pos)==1 && zpick2(pos)~=0
            bedrockPlotHandle{end+1} = text(dist(pos_act(i)),zpick2(pos_other(i))-sift,num2str(nrOfLine2(pos)),'FontSize',14);
        elseif weightpick2(pos)~=1 && zpick2(pos)~=0
            bedrockPlotHandle{end+1} = text(dist(pos_act(i)),zpick2(pos_other(i))-sift,num2str(nrOfLine2(pos)),'FontSize',14);
        end
        
        if ismember(i,i_otherNopick)
            bedrockPlotHandle{end+1} = text(dist(pos_act(i)),yLim(1)+sift,num2str(nrOfLine2(pos)),'FontSize',14);
        end
        %}
        bedrockPlotHandle{end+1} = text(dist(pos_act(i)),yLim(1)+sift,num2str(nrOfLine2(pos_other(i))),'FontSize',14,'Color',0.4*[1 1 1]);
    end

end
end