function fig = SegyPickHorizon(datafile,migfile,procp,outputfile);

[comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(migfile);
datam = data; % data after migration

[comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,origpx,origpy,tti,zeff,profnr,date,info,profinfo] = ReadSEGY(datafile);  


% -------------------------------------------------------------------------
% ------------------- TIME-TO-DEPTH CONVERSION ----------------------------
% -------------------------------------------------------------------------
dz = sra/2.0*procp.vice;

if (sum(isnan(zdem) == 0))
   zeff = zeff - median(zeff-zdem);
else
    zeff = zeff - procp.GlobalZshift;
end;

maxalt = max(zeff) + 20;
minalt = min(zeff) - procp.maxdepth;
nz = round((maxalt - minalt)/dz);
zz = linspace(-maxalt,-minalt,nz);
xx = (0:ntr-1)*procp.dri;
datamz = zeros(nz,ntr);
itopo = round(tti/sra);

for a = 1:ntr
    [mv,mi] = min(abs(zz+zeff(a)));
    nins = nsamp - itopo(a);
    if (itopo(a) < 1), itopo(a) = 1; end;
    i3 = itopo(a);
    i4 = itopo(a)+nins - 1;
    i1 = mi;
    i2 = mi + nins - 1;
    if (i2 > nz)
        i4 = i4 - (i2-nz);
        i2 = nz;
    end;
    datamz(i1:i2,a) = datam(i3:i4,a);
end;

rsum = sum(abs(datamz),2);
ii = find(rsum > 1.0e-3);
zz = zz(ii(1):ii(length(ii)));
datamz = datamz(ii(1):ii(length(ii)),:);



% -------------------------------------------------------------------------
% ------------------- PLOT THE GPR DATA -----------------------------------
% -------------------------------------------------------------------------
fig = figure(10); clf;
imagesc(xx,zz,datamz);
caxis(procp.cax);
axis tight;
colormap(procp.cmap);
freezeColors;
xlabel('Distance [m]');
ylabel('Altitude [-masl]');
title('Pick Horizon / Doubleclick when finish picking')
hold on; 
plot(xx,-zeff,'k-','linewidth',1.5); 


% -------------------------------------------------------------------------
% ------------------- PICK THE BEDROCK TOPOGRAPHY -------------------------
% -------------------------------------------------------------------------
xbed = [];
ybed = [];
zbed = [];
zsurf = [];
iweight = [];

while(1)
    [dbed,ztmp] = getline(gca);
    
    if (length(dbed) < 2),
        break;
    end;
    
    ftrace = ceil(min(dbed)/procp.dri);     % round towards positive infinity
    ltrace = floor(max(dbed)/procp.dri);    % round towards negative infinity
    zbedi = interp1(dbed,ztmp,xx(ftrace:ltrace),'linear'); % interpolate between picks
    
    w = inputdlg('Enter weight [0]');       % weight -> different colors
    w = str2num(w{1});
    
    if (isempty(w) == 1), w = 0;
    end;    
    
    % ----- Plot interpolated line between picks -----
    hold on; 
    if (w == 0), plot(xx(ftrace:ltrace),zbedi,'k-','linewidth',1.5); end;
    if (w == 1), plot(xx(ftrace:ltrace),zbedi,'k--','linewidth',1.5); end;
    if (w == 2), plot(xx(ftrace:ltrace),zbedi,'k--','linewidth',1); end;
    if (w == 10), plot(xx(ftrace:ltrace),zbedi,'k-','linewidth',1.5); end;
    if (w == 11), plot(xx(ftrace:ltrace),zbedi,'k--','linewidth',1.5); end;
    if (w == 12), plot(xx(ftrace:ltrace),zbedi,'k--','linewidth',1); end;
    
    plot(dbed,ztmp,'ko','linewidth',1.5);
    hold off;
    
    xbed = [xbed; px(ftrace:ltrace)']; % x coordinate of picked horizon
    ybed = [ybed; py(ftrace:ltrace)']; % y coordinate of picked horizon
    zbed = [zbed; -zbedi'];            % z coordinate of picked horizon
    zsurf = [zsurf; zeff(ftrace:ltrace)'];
    iweight = [iweight; w*ones(size(zbedi'))];
end;


% -------------------------------------------------------------------------
% ------------------- SAVE PICKS IN .txt FILE -------------------------
% -------------------------------------------------------------------------
fid = fopen(outputfile,'wt');
for a = 1:length(xbed)
    fprintf(fid,'%10.3f %10.3f %10.3f %10.3f %10.3f %d\n',xbed(a),ybed(a),zbed(a),zsurf(a),zsurf(a)-zbed(a),iweight(a));
end;
fclose(fid); 

