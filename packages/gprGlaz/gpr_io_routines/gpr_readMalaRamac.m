function [gprdat] = gpr_readMalaRamac(path, filename)

% read Mala Ramac *.rd3 files.
% if a parameter file *.rad exists in the same folder, it will also be read.

fid = fopen([path filename],'rb','ieee-le');
if(fid == -1)
    error('Binary input file (*.rd3) does not exist');
end
%determine the number of bytes
% fseek(fid,0,'eof');
% byte_end = ftell(fid);
% fseek(fid,0,'bof');
% open header file if it exists


if(exist([path filename(1:end-3) 'rad']))
    fh = fopen([path filename(1:end-3) 'rad'],'r');
    nsamp = fscanf(fh,'SAMPLES:%d\n');
    freq = fscanf(fh,'FREQUENCY:%f\n');
    sra = 1.0/freq*1000;
    for a = 1:8 , fgets(fh); end;
    dx = fscanf(fh,'DISTANCE INTERVAL:%g\n');
    for a = 1:3 , fgets(fh); end;
    afreq = fscanf(fh,'ANTENNAS:%g%*c\n');
    for a = 1:4 , fgets(fh); end;
    twin = fscanf(fh,'TIMEWINDOW:%g\n');
    if (twin == 0),twin = nsamp*sra;end;
    if (abs((twin-sra*nsamp)/twin) > 1e-3);
        sprintf('Error in file %s\n',filename);
    end;
    nstack = fscanf(fh,'STACKS:%g\n');
    for a = 1:2 , fgets(fh); end;
    ntr = fscanf(fh,'LAST TRACE:%g\n');
    gprdat.nr_of_traces = ntr; % number of traces
    gprdat.nr_of_samples = nsamp; % number of samples  
    gprdat.t0 = 0; % time zero
    gprdat.time_window = twin; % Time window
    fclose(fh);
else
    disp('Header file not found! Can not read data!')
    
end

fseek(fid,0,'eof');
NR_TR = ftell(fid)/(2*nsamp);
fseek(fid,0,'bof');
if (NR_TR ~= ntr)
   sprintf('Error in file %s\n',filename);
end;
data = fread(fid,[nsamp,ntr],'int16');

gprdat.data =zeros(gprdat.nr_of_samples, gprdat.nr_of_traces);
gprdat.headers=zeros(32, gprdat.nr_of_traces);

byte_pos = 0;

for tra = 1:ntr
    Header=zeros(32,1); %no trace header in ramac data
    Header(1) = tra; % trace number
    Header(2) = (tra-1)*dx; % position
    Header(3) = nsamp; % number of samples per trace
    Header(7) = twin; % time window
    gprdat.headers(:,tra)= Header;
    gprdat.data(:,tra) = data(:,tra);
    clear Header
end

gprdat.antenna_frequency = afreq;
gprdat.sampling_rate = sra;
gprdat.number_stacks = nstack;
gprdat.dataz   = [];
gprdat.x       = [];
gprdat.y       = [];
gprdat.z       = [];
gprdat.pos0    = [];
gprdat.flip    = [];
gprdat.profdir = [];

fclose(fid);

% if nargin > 1
%     n = n +1;
% end