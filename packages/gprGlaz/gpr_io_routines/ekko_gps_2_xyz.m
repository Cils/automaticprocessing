function [ ] = ekko_gps_2_xyz(gps_file,outfile,sg)

% ekko_gps_2_xyz reads in a Puls Ekko *.GPS file 
% and writes xyz coordinates as three column ascii file to "outfile"  

% sg=1 conversion to Swiss grid
% sg=0 leave lat lon coordinates


if (nargin<3), sg=1; end;
    tt = [];
    fid = fopen(sprintf('%s',gps_file),'rt');
    if fid == -1, error('%s does not exist. Make sure the *.GPS file has the same prefix as the *.DT1 file!',gps_file); end;
    
    tmp = textscan(fid,'%s','CommentStyle','Trace');
    hour=[]; min=[];sec=[];lat_deg=[];lat_min=[];
    lon_deg=[]; lon_min=[]; alt=[]; 
    x=[]; y=[]; h=[];

    % Read in Strings
    tmp = tmp{:};
    gga = ~cellfun(@isempty, strfind(tmp,'GPGGA'));
    tt = [tmp{find(gga)}];
    k = strfind(tt,'$GPGGA');
    check = 1;
    if length(k)<1, 
        llq = ~cellfun(@isempty, strfind(tmp,'GNLLQ'));
        tt = [tmp{find(llq)}];
        k = strfind(tt,'$GNLLQ');
        check = 2;
    end; 
    if length(k)<1,
        pllq = ~cellfun(@isempty, strfind(tmp,'GPLLQ'));
        tt = [tmp{find(pllq)}];
        k = strfind(tt,'$GPLLQ');
        check = 3;
    end;
    if length(k)<1,
        null = ~cellfun(@isempty, strfind(tmp,' '));
        k = [1,1]; %dummy k
        check = 4; 
    end;
    if length(k)<1, 
        error('!!!!! No line with $GPGGA, $GPLLQ or $GNLLQ found in GPS file!!!!');
    end;

% Write Strings into arrays
if check == 1, 
    lines = strsplit(tt,'$GPGGA');
    hour = ones(1,length(lines)-1)*NaN; min = hour; sec = hour;
    lat_deg = hour; lat_min = hour; lon_deg = hour; lon_min = hour;
    alt = hour;
%     if multi>0,
%         int = 2;
%     else
%         int = 1;
%     end 
    int=1;
    for i=2:int:length(lines)
        sepLine=strsplit(lines{i},',');
        hour(i-1)    = str2double(sepLine{2}(1:2));
        min(i-1)     = str2double(sepLine{2}(3:4));
        sec(i-1)     = str2double(sepLine{2}(5:end));
        try
            lat_deg(i-1) = str2double(sepLine{3}(1:2));
            lat_min(i-1) = str2double(sepLine{3}(3:end));
            lon_deg(i-1) = str2double(sepLine{5}(1:3));
            lon_min(i-1) = str2double(sepLine{5}(4:end));
            alt(i-1)     = str2double(sepLine{10});
        catch
        end
    end
%{
  alt_pos = 14;
  if multi>0,
    int = 2;
  else
    int = 1;
  end 
  
  %check for empty GPS values
  k2 = [k(2:end),0]; 
  kd = k2 - k; %character spacing between two $GPGGA strings, should be 84 for good GPS values
  pos_gaps = find(kd<81);
  
  for i=1:int:length(k),
      hour = [hour,str2num(tt(k(i)+7:k(i)+8))];
      min = [min,str2num(tt(k(i)+9:k(i)+10))];
      sec = [sec,str2num(tt(k(i)+11:k(i)+15))];
      if ~ismember(i,pos_gaps),   
        lat_deg = [lat_deg,str2num(tt(k(i)+17:k(i)+18))];
        lat_min = [lat_min,str2num(tt(k(i)+19:k(i)+28))];
        lon_deg = [lon_deg,str2num(tt(k(i)+32:k(i)+34))];
        lon_min = [lon_min,str2num(tt(k(i)+35:k(i)+44))];
        alt = [alt,str2num(tt(k(i)+57:k(i)+64))];
        if i==1 & length(alt)>1, %correct position of altitude values
           while length(alt)>1
             alt_pos = alt_pos-1;  
             alt = [];  
             alt = [alt,str2num(tt(k(i)+57-alt_pos:k(i)+64))];
           end 
        end   
       else
        lat_deg = [lat_deg,NaN];
        lat_min = [lat_min,NaN];
        lon_deg = [lon_deg,NaN];
        lon_min = [lon_min,NaN];
        alt = [alt,NaN];
    end    
  end;
  %}
  
%%   old version not checking for gaps
%   for i=1:int:length(k),
%     hour = [hour,str2num(tt(k(i)+7:k(i)+8))];
%     min = [min,str2num(tt(k(i)+9:k(i)+10))];
%     sec = [sec,str2num(tt(k(i)+11:k(i)+15))];
%     lat_deg = [lat_deg,str2num(tt(k(i)+17:k(i)+18))];
%     lat_min = [lat_min,str2num(tt(k(i)+19:k(i)+28))];
%     lon_deg = [lon_deg,str2num(tt(k(i)+pos_N+2:k(i)+pos_N+4))];
%     lon_min = [lon_min,str2num(tt(k(i)+pos_N+5:k(i)+pos_E-2))];
%     alt = [alt,str2num(tt(k(i)+pos_M-alt_pos:k(i)+pos_M-2))];
%     if i==1 & length(alt)>1, %correct position of altitude values
%        while length(alt)>1
%          alt_pos = alt_pos-1;  
%          alt = [];  
%          alt = [alt,str2num(tt(k(i)+pos_M-alt_pos:k(i)+pos_M-2))];
%        end 
%     end   
%   end;  
  
elseif check == 2,
  for i=1:length(k),
    hour = [hour,str2num(tt(k(i)+7:k(i)+8))];
    min = [min,str2num(tt(k(i)+9:k(i)+10))];
    sec = [sec,str2num(tt(k(i)+11:k(i)+15))];
    x = [x,str2num(tt(k(i)+25:k(i)+34))];%str2double(tline(25:34));
    y = [y,str2num(tt(k(i)+38:k(i)+46))];
    h = [h,str2num(tt(k(i)+61:k(i)+68))];
  end;  
elseif check == 3,
  for i=1:length(k),
    hour = [hour,str2num(tt(k(i)+7:k(i)+8))];
    min = [min,str2num(tt(k(i)+9:k(i)+10))];
    sec = [sec,str2num(tt(k(i)+11:k(i)+15))];
    x = [x,str2num(tt(k(i)+24:k(i)+34))];%str2double(tline(25:34));
    y = [y,str2num(tt(k(i)+37:k(i)+46))];
    h = [h,str2num(tt(k(i)+61:k(i)+68))];
  end;
elseif check == 4,
  for i=1:length(null),
    hour = [hour,0];
    min = [min,0];
    sec = [sec,0];
    x = [x,0];0
    y = [y,0];
    h = [h,0];
  end;
end;
fclose(fid);

%Interpolate missing data
lat_deg = fixgaps(lat_deg);
lat_min = fixgaps(lat_min);
lon_deg = fixgaps(lon_deg);
lon_min = fixgaps(lon_min);
alt = fixgaps(alt);

% hour = tt{1};
% min = tt{2};
% sec = tt{3};
% lat_deg = str2num(cell2mat(tt{4}));
% lat_min = str2num(cell2mat(tt{5}));
if check==1,
  lat_decdeg = lat_deg + lat_min*100/6000;
  % long_deg = str2num(cell2mat(tt{6}));
  % long_min = str2num(cell2mat(tt{7}));
  long_decdeg = lon_deg + lon_min*100/6000;
  % alt = str2num(cell2mat(tt{9}));
  if (sg == 1),
    [y x h] = jwgs2sgr(lat_decdeg,long_decdeg,alt);
  end;
end;  

%write gps coord file
fid = fopen(sprintf('%s',outfile),'wt');
%fprintf(fid,'Northing Easting Height \n');
for i=1:length(x);
    fprintf(fid,'%f %f %f %d %d %d\n',x(i),y(i),h(i),hour(i),min(i),sec(i));
end;   
fclose(fid);
fprintf('New file %s created!! \n',outfile)


 


