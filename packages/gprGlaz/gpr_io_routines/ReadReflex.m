function [content] = ReadReflex(headerfile,datafile)

% ---------------------------- READ HEADER FILE ---------------------------
fid = fopen(headerfile,'rb');
dummy = fread(fid,20*21,'char');
smallint = fread(fid,16,'short'); 
longint = fread(fid,2,'int');
singles = fread(fid,20,'float'); 
fclose(fid);

% ntr and nsamp will be determined from the data file
% ntr_par = longint(1);
nsamp_par = smallint(1);
if (smallint(9) == 2)
    ff = 'short'; 
    dsize = 2;
end;
if (smallint(9) == 3)
    ff = 'float'; 
    dsize = 4;
end;
sra = singles(2);


% ---------------------------- READ DATA FILE -----------------------------
thsize = 154;
fid = fopen(datafile,'rb');

% ---------------------------- DETERMINE FILE SIZE ------------------------
% Move to specified position in file (offset=0, origin=1 end of file, 
% origin=1 beginning of file)
fseek(fid,0,1); 
nbyte = ftell(fid); % Position in open file
fseek(fid,0,-1);

% Read nsamp of first record, assume that nsamp is identical for all traces
dummy = fread(fid,1,'int');
nsamp = fread(fid,1,'int')+1;
if (nsamp == 1), nsamp = nsamp_par+1; end; % take parameter file value if trace header is empty
fseek(fid,0,-1);
ntr =  nbyte/(thsize + nsamp*dsize);

% ---------------------------- ALLOCATE ARRAYS ----------------------------
srcx = zeros(ntr,1);
srcy = zeros(ntr,1);
srcz = zeros(ntr,1);
recx = zeros(ntr,1);
recy = zeros(ntr,1);
recz = zeros(ntr,1);
data = zeros(nsamp,ntr);    

for a = 1:ntr
    dummy = fread(fid,1,'int');
    dummy = fread(fid,1,'int');
    dummy = fread(fid,5,'int');
    dummy = fread(fid,1,'short');
    dummy = fread(fid,1,'int');
    dummy = fread(fid,1,'float');
    srcz(a) = fread(fid,1,'double');
    dummy = fread(fid,1,'double');
    srcx(a) = fread(fid,1,'double');
    srcy(a) = fread(fid,1,'double');
    recx(a) = fread(fid,1,'double');
    recy(a) = fread(fid,1,'double');
    dummy = fread(fid,2,'double');
    recz(a) = fread(fid,1,'double');
    dummy = fread(fid,1,'float');
    dummy = fread(fid,1,'double');
    dummy = fread(fid,8,'float');
    data(:,a) = fread(fid,[1,nsamp],ff);
end;
fclose(fid);

px = 0.5*(srcx+recx);
py = 0.5*(srcy+recy);
pz = 0.5*(srcz+recz);
% clf;
% %plot(srcx,srcy,'bo',recx,recy,'r.');
% imagesc(data);
% colorbar;
% axis equal;

content.sdate = strcat('20',headerfile(end-12:end-11),'-',headerfile(end-14:end-13),'-',headerfile(end-16:end-15));
content.ntr = ntr;
content.nsamp = nsamp;
content.sra = sra;
content.prx = px;
content.pry = py;
content.prz = pz;
content.freq = 0;
content.offset = 0;
content.data = data;
content.multi = 0;
%if (nargin > 1)
%content.tt = tt;
%end;
