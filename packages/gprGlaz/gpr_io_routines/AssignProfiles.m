function AssignProfiles(data_file,profile_file,assign_file,ax)

% data_file: initially generated data file
% profile_file: .mat file containing profile trace information

load(data_file);
load(profile_file);
profidx = nan*ones(ntr,1);

% ----------------- ASSIGN POINTS TO PROFILES -----------------------------
a = 0;
while(1)
    a = a + 1;
    
    if (a > nprof);
        break;
    end;
    
    clf;
    plot(px,py,'b.');
    set(gca,'DataAspectRatio',[1 1 1]);
    
    if (exist('ax') == 1)
        axis(ax);
    end;
    
    hold on;
    plot(prof(a).x,prof(a).y,'r-','linewidth',1.5);
    axis([min(prof(a).x)-100 max(prof(a).x)+100 min(prof(a).y)-100 max(prof(a).y)+100]);
    title('AssignProfiles: Pick polygon with points on profile in it')
    pause(.1)
    [xx,yy] = getline(gcf);
    
    if (length(xx) < 3)
        continue;
    end;
    
    ii = inpolygon(px,py,xx,yy);
    plot(px(ii),py(ii),'r.');
    title('Left: save file / Right: delete current selection / any other: continue without saving');
    [xx,yy,button]=ginput(1);
    if (button == 1)
       profidx(ii) = a;
       save(assign_file,'profidx');
    end;
    if (button == 3)
        a = a -1;
    end;
end;
close
