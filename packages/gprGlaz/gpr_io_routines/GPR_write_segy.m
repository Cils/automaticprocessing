function GPR_write_segy(segyname,comments,offset,sra,px,py,pz,data,zdem,...
                        origpx,origpy,tti,zeff,profnr,date,...
                        vel,head,azi,pitch,roll,hag)

% segyname: Filename of outputfile (name.segy)
% comments: Header information - if not empty, copied to header
% data:     Data matrix
% ntr:      Number of traces
% nsamp:    Number of samples per trace
% sra:      Sample rate in ns
% zdem:     Local topography
% offset:   Antennae separation
% xs:       x coordinate of Tx
% ys:       y coordinate of Tx
% zs:       z coordinate of Tx
% xr:       x coordinate of Rx
% yr:       y coordinate of Rx
% zr:       z coordinate of Rx
% origpx:   Original coordinates Tx
% origpy:   Original coordinates Tx
% tti:      Picked air travel time
% zeff:     Ground elevation
% profnr:   Profile number
% Create:   Contains the date: year, day, hour, minute of collected data


xs = px;    
ys = py;    
zs = pz;            
xr = px;             
yr = py;             
zr = pz;             
[nsamp ntr] = size(data);

NULL = 0;
ONE = 1;
FIVE = 5;


fid = fopen(segyname,'wb');

% -------------------------------------------------------------------------
% ----------------- WRITE 3200-byte TEXTUAL HEADER ------------------------
% -------------------------------------------------------------------------

tdummy = repmat(uint8(32),40,80);
if ~isempty(comments)
   cc=char(comments{:});
   tdummy(1:size(cc,1),1:size(cc,2))=cc;
end

fwrite(fid,tdummy,'char');


% -------------------------------------------------------------------------
% ----------------- WRITE 400-byte FILE HEADER ----------------------------
% -------------------------------------------------------------------------

fwrite(fid,ONE,'int32');        % 3201-3204 Job Id number
fwrite(fid,profnr,'int32');     % 3205-3208 Profile Line number
fwrite(fid,ONE,'int32');        % 3209-3212 Job Id numbers

fwrite(fid,ntr,'int16');        % 3213-3214 Number of data traces per ensemble
fwrite(fid,NULL,'int16');       % 3215-3216 Number of auxilary traces per ensemble

sra = round(sra*1000);          % GPR measures in ns
fwrite(fid,sra,'int16');        % 3217-3218 Sample interval [10^-9 s]
fwrite(fid,sra,'int16');        % 3219-3220 Sample interval [10^-9 s]
fwrite(fid,nsamp,'int16');      % 3221-3222 Number of samples per trace
fwrite(fid,nsamp,'int16');      % 3223-3224 Number of samples per trace
fwrite(fid,FIVE,'int16');       % 3225-3226 Data sample format code
                                %           (4-byte IEEE folating-point)

fwrite(fid,ONE,'int16');        % 3227-3228 Ensamble fold
fwrite(fid,NULL,'int16');       % 3229-3230 Trace sorting code
fwrite(fid,ONE,'int16');        % 3231-3232 Vertical sum code

dummy = zeros(11,1);            % 3233-3254
fwrite(fid,dummy,'int16');  

fwrite(fid,ONE,'int16');        % 3255-3256 Measurement system (1: meters)

dummy = zeros(2,1);             % 3257-3260
fwrite(fid,dummy,'int16');      
                           
dummy = zeros(120,1);
fwrite(fid,dummy,'int16');      % 3261-3500 

fwrite(fid,256,'int16');        % 3501-3502 SEG Y format revision number (Version 1.0)
fwrite(fid,ONE,'int16');        % 3503-3504 Fixed lenght trace flag
fwrite(fid,NULL,'int16');       % 3505-3506 Number of 3200-byte extended file header records



% ----- Unassigned -----
dummy = zeros(47,1);
fwrite(fid,dummy,'int16');      % 3507-3600 Unassigned
% ----------------------


%disp (['binary file header written - byte pos: ' num2str(ftell(fid))]);

% -------------------------------------------------------------------------
% ----------------- WRITE 240-byte TRACE HEADERS ----------------------------
% -------------------------------------------------------------------------

for a = 1:ntr  
 fwrite(fid,a,'int32');         %  1 -  4  Trace sequence number within line
 fwrite(fid,a,'int32');         %  5 -  8  Trace sequence number within file
 fwrite(fid,a,'int32');         %  9 - 12  Original field record number,FFID
 fwrite(fid,ONE,'int32');       % 13 - 16  Trace number in original field record
 fwrite(fid,a,'int32');         % 17 - 20  Energy source point number
 fwrite(fid,a,'int32');         % 21 - 24  CDP number
 fwrite(fid,ONE,'int32');       % 25 - 28  Trace number in CDP (1 trace per CDP)
 
 dummy = ones(4,1);
 fwrite(fid,dummy,'int16');     % 29 - 36  Trace id code, data use, ...
 
 offset = offset*100;           % write the offset in [m] 
 fwrite(fid,offset,'int32');    % 37 - 40  Tx-Rx offset [cm]
 
 % For resolution in cm
 escaler = 100;                 % Scaler on elevation, get elevation in [cm]
 kscaler = 100;                 % Scaler on coordinates, get coordinates in [cm]
 
 fwrite(fid,zr(a)*escaler,'int32');   % 41 - 44  Receiver elevation
 fwrite(fid,zs(a)*escaler,'int32');   % 45 - 48  Source elevation
 fwrite(fid,NULL,'int32');            % 49 - 52  source depth below surface
 fwrite(fid,NULL,'int32');            % 53 - 56  Datum elevation at receiver
 fwrite(fid,NULL,'int32');            % 57 - 60  Datum elevation at source
 
if length(zeff) == ntr,
 fwrite(fid,round(zeff(a)*escaler),'int32'); % 61 - 64  Ground elevation: zeff = pz-(tti/2)*vair)
 fwrite(fid,round(zeff(a)*escaler),'int32'); % 65 - 68  Ground elevation: zeff = pz-(tti/2)*vair)
else  
 fwrite(fid,round(zeff(1)*escaler),'int32'); % 61 - 64  Ground elevation: zeff = pz-(tti/2)*vair)
 fwrite(fid,round(zeff(1)*escaler),'int32'); % 65 - 68  Ground elevation: zeff = pz-(tti/2)*vair)
end

 % For resolution in cm
 fwrite(fid,-escaler,'int16');        % 69 - 70  Scalar applied to all elevations  %% CHECK
 fwrite(fid,-kscaler,'int16');        % 71 - 72  Scalar applied to all coordinates %% CHECK
 
 fwrite(fid,round(xs(a)*kscaler),'int32');   % 73 - 76  Source coordinate x
 fwrite(fid,round(ys(a)*kscaler),'int32');   % 77 - 80  Source coordinate y
 fwrite(fid,round(xr(a)*kscaler),'int32');   % 81 - 84  Receiver coordinate x
 fwrite(fid,round(yr(a)*kscaler),'int32');   % 85 - 88  Receiver coordinate y
 
 fwrite(fid,ONE,'int16');       % 89 - 90  Coordinate units (length,s,...)
 
 dummy = zeros(12,1);
 fwrite(fid,dummy,'int16');     % 91 - 114
 
 fwrite(fid,nsamp,'int16');     % 115 -116 Number of samples in this trce
 fwrite(fid,sra,'int16');       % 117 -118 Sample interval for this trace
 
 dummy = zeros(19,1);
 fwrite(fid,dummy,'int16');     % 119 -156 Gain type, sweep frequency, filers, ...
 
 fwrite(fid,date.y,'int16');    % 157 -158 Year data recorded (4-digits)
 fwrite(fid,date.dj,'int16');    % 159 -160 Day of yera (Julian day)
 fwrite(fid,date.hr,'int16');   % 161 -162 Hour of day
 fwrite(fid,date.min,'int16');  % 163 -164 Minute of hour
 fwrite(fid,NULL,'int16');      % 165 -166 Second of minute
 fwrite(fid,ONE,'int16');       % 167 -168 Time basis code: 1 = Local
 
 dummy = zeros(6,1);
 fwrite(fid,dummy,'int16');     % 169 -180
 
 fwrite(fid,round(origpx(a)*kscaler),'int32');   % 181 -184 Original x coordinate Tx
 fwrite(fid,round(origpy(a)*kscaler),'int32');   % 185 -188 Original y coordinate Tx
 
 % inlucde antenna orientation data
 fwrite(fid,vel(a),'float32');  % 189 -192 Velocity of antenna
 fwrite(fid,head(a),'float32');  % 193 -196 Heading of profile
 fwrite(fid,azi(a),'float32');  % 197 -200 Heading of antenna
 fwrite(fid,pitch(a),'float32');  % 201 -204 Pitch of antenna
 fwrite(fid,roll(a),'float32');  % 205 -208 Roll of antenna
 fwrite(fid,hag(a),'float32');  % 209 -212 Height above ground of antenna
 
 
 dummy = zeros(10,1);
 fwrite(fid,dummy,'int16');     % 213-232

 % ----- Unassigned -----
 if length(tti) == ntr,
 fwrite(fid,tti(a),'float32');  % 233-236 tti (Picked air traveltime [ns])
 fwrite(fid,zdem(a),'float32'); % 237-240 Digital elevation map zdem
 else
 fwrite(fid,tti(1),'float32');  % 233-236 tti (Picked air traveltime [ns])
 fwrite(fid,zdem(1),'float32'); % 237-240 Digital elevation map zdem
 end;
     
 % ----------------------
 
 
% -------------------------------------------------------------------------
% ----------------- WRITE TRACE DATA ----------------------------
% -------------------------------------------------------------------------
 fwrite(fid,data(:,a),'float32'); % Trace data: File for Matlab/Promax: 4-byte floating point value
end;
%disp (['binary file header written - byte pos: ' num2str(ftell(fid))]);


fclose(fid);
disp ([num2str(a) ' traces written to file ' segyname]);
disp ('use ''SEG-Y Input'' ,  Type ''MS-DOS IEEE floating-point'' to read data in ProMAX');
