function procp = SegyPlotData(Title,procp,params,task,adj)

% task = 0: Plot traces on x-axis, time on y-axis
% task = 1: Plot distance on x-axis, time on y-axis
% task = 2: Plot distance on x-axis, (tti) adjusted time on y axis
% task = 3: Plot distance on x-axis, depth below surface on y axis
% task = 4: Plot distance on x-axis, altitude on y axis
% task = 5: Plot traces on x-axis, (tti) adjusted time on y axis
% adj = 0/1:  Without/with adjustment of colormap
otitle = Title;

% define local variable names
nsamp = params.nsamp; sra = params.sra; ntr = params.ntr;
tti = params.tti; data = params.data;
zdem = params.zdem; zeff = params.pz-params.hag;
%zeff = params.zeff; % if params.pz-params.hag is erroneous 



% -------------------------------------------------------------------------
% ------------------- TRACE NORMALIZATION ---------------------------------
% -------------------------------------------------------------------------
%data = tracenormalize(data);
data = datanormalize(data);

% -------------------------------------------------------------------------
% ------------------- PLOT DATA WITH TIME AXIS ----------------------------
% -------------------------------------------------------------------------
t = (0:nsamp-1)*sra;
t = t - procp.t0_corr;
dist = (0:ntr-1)*procp.dri;
load zcm.mat
load blkwhtred.mat

if (task == 0)
    xr = 1:ntr;
    yr = t;
    xlab = 'Trace number';
    ylab = 'Time [ns]';
    yd = 'reverse';
end

if (task == 1)
    xr = dist;
    yr = t;
    xlab = 'Distance [m]';
    ylab = 'Time [ns]';
    yd = 'reverse';
end

if (task == 2)
    data2 = zeros(size(data));
    for a = 1:ntr
        if (tti(a) > 0)
            i1 = round(tti(a)/sra);
            data2(1:nsamp-i1+1,a) = data(i1:end,a);
        else
            data2(:,a) = data(:,a);
        end;
    end;
    data = data2;
    xr = dist;
    yr = t;
    xlab = 'Distance [m]';
    ylab = 'Time [ns]';
    yd = 'reverse';
end

if (task == 3)
    data2 = zeros(size(data));
    for a = 1:ntr
        if (tti(a) > 0)
            i1 = round(tti(a)/sra);
            data2(1:nsamp-i1+1,a) = data(i1:end,a);
        else
            data2(:,a) = data(:,a);
        end;
    end;
    z = t/2.0*procp.vice;
    data = data2;
    xr = dist;
    yr = z;
    yd = 'reverse';
    xlab = 'Distance [m]';
    ylab = 'Depth [m]';
end

if (task == 4) % altitude plot
    % Correct zeff using zdem or procp.GlobalZshift
   % if (sum(isnan(zdem)) == 0)
   %     zeff = zeff + median(zdem - zeff);
   % else
   %     zeff = zeff + procp.GlobalZshift;
   % end;
    
    % Define altitude range
    dz = sra/2.0*procp.vice;    
    maxalt = max(zeff) + 20;
    minalt = min(zeff) - procp.maxdepth;
    nz = round((maxalt - minalt)/dz);
    zz = linspace(-maxalt,-minalt,nz);
    
    dataz = zeros(nz,ntr);
    itopo = round(tti/sra);
    itopo(itopo < 1) = 1;
    surface_sample = []; %vector with sample number of the surface for each trace in dataz
    
    for a = 1:ntr
        [~,mi] = min(abs(zz+zeff(a))); % index of surface altitude
        nins = nsamp - itopo(a); % No. of samples to be transferred from data
        i3 = itopo(a);
        i4 = itopo(a)+nins - 1;
        i1 = mi;
        i2 = mi + nins - 1;
        
        if (i2 > nz)
            i4 = i4 - (i2-nz);
            i2 = nz;
        end;
        dataz(i1:i2,a) = data(i3:i4,a);
        surface_sample = [surface_sample,i1];
    end
    
    % Trim z range of data
    %rsum = sum(abs(dataz),2);
    %ii = find(rsum > 1.0e-3);
    %nsamp = nsamp - ii(1); %correct the number of "usable" samples in every trace
    %zz = zz(ii(1):ii(length(ii)));
    %dataz = dataz(ii(1):ii(length(ii)),:);
    
    data = dataz;
    %data = dataz(1:length(dataz)-max(surface_sample),:);
    u_range = int32(procp.maxdepth/dz)-1;
    [m,n] = size(data);
    for k=1:n
       data(surface_sample(k)+u_range:end,k)=0;
    end    
    xr = dist;
    yr = abs(zz);
    xlab = 'Distance [m]';
    ylab = 'Altitude [m.a.s.l.]';
    yd = 'normal';
end;

if (task == 5)
    data2 = zeros(size(data));
    for a = 1:ntr
        if (tti(a) > 0)
            i1 = round(tti(a)/sra);
            data2(1:nsamp-i1+1,a) = data(i1:end,a);
        else
            data2(:,a) = data(:,a);
        end;
    end;
    data = data2;
    xr = 1:ntr;
    yr = t;
    xlab = 'Traces';
    ylab = 'Time [ns]';
    yd = 'reverse';
end;

% -------------------------------------------------------------------------
% ------------------- Plot and ADJUST COLORAXIS ------------------------------------
% -------------------------------------------------------------------------
button = 0;
dta = 3.0e-3;
dcax = 1.5;
%data=ones(size(data)); %for Test
datao = data;
data_raw = data;
rcax = procp.cax;
rtfun = procp.tfun(task+1);
ragclen = procp.agclen;
%procp.tfun(task+1) = 9.0e-4;
%agc paramter
% if procp.agclen>0, 
%    fprintf('Applying AGC with found parameter');
%    datao = agc(data_raw,procp.agclen,sra); 
% end;
% if procp.agclen==0, procp.agclen = nsamp/5.; end;

if task < 4, surface_sample=0; end;
%u_range = length(data) - max(surface_sample); %usable data range 

if (adj == 1)
    Title = {'Choose colormap with 1/2','caxis with left/right | vertical scaling with up/down','agc with a/s/q | reset with r','Confirm with ESC'}; 
end;

while(button ~= 27)
    if (procp.tfun(task+1) ~= 0)
        for a = 1:length(dist)
            if surface_sample(a)+u_range<=size(data,1)
                bMin=surface_sample(a)+u_range;
            else
                bMin=surface_sample(a)+u_range-1;
            end
            data(surface_sample(a):bMin,a) = ...
                datao(surface_sample(a):bMin,a) ...
                .* exp(-(yr(surface_sample(a):bMin)-yr(surface_sample(a)))*procp.tfun(task+1))';
        end;
    else
        data=datao;
    end;

    imagesc(xr,yr,data);
    xlabel(xlab,'fontsize',12);
    ylabel(ylab,'fontsize',12);
    set(gca,'fontsize',12);
    title(Title,'fontsize', 12,'interpreter','none');
    colormap(procp.cmap);    
    caxis(procp.cax);
    freezeColors;
    set(gca,'YDir',yd);
    if (task == 3 )
        axis([0 max(dist) 0 procp.maxdepth]);
    end;
    if ((task == 3) | (task == 4))
        set(gca,'dataaspectratio',[procp.zexag 1 1]);
    end;
    if (task == 4)
        hold on;
        plot(dist,zeff,'k-','linewidth',1.5); 
        axis([0 max(dist) min(zeff)-procp.maxdepth max(zeff)])
        hold off;
    end;
    axis normal; %equal
    
    if (adj == 0), break; end;
    [xx,yy,button] = ginput(1);
    if (button == 28), procp.cax = procp.cax*dcax; end;         % button left arrow
    if (button == 29), procp.cax = procp.cax/dcax; end;         % button right arrow
    if (button == 49), procp.cmap = zcm; end;                   % button "1"
    if (button == 50), procp.cmap = blkwhtred; end;             % button "2"
    if (button == 30), procp.tfun(task+1) = procp.tfun(task+1) - dta; end; % button arrow up
    if (button == 31), procp.tfun(task+1) = procp.tfun(task+1) + dta; end; % button arrow down
    if (button == 113), datao = data_raw; end;                  % button "q"
    if (button == 97),                                          % button "a"
        procp.agclen = procp.agclen+500;
        datao = agc(data_raw,procp.agclen,sra);
    end;
    if (button == 115),                                         % button "s"
      procp.agclen = procp.agclen-500;
        datao = agc(data_raw,procp.agclen,sra);
    end;
    if (button == 114),                                         % button "r"
        procp.cax=rcax;
        procp.cmap = blkwhtred;
        procp.tfun(task+1)=rtfun;
        datao = data_raw;
        data = data_raw;
        procp.agclen = ragclen;
    end;
end;        

 title(otitle,'fontsize', 12,'interpreter','none');
 
% save params for faster restoring during subesquent plotting
 if ~isempty(params.matdata), 
   params.data = datao;  
   params_file = sprintf('%s%sparams.mat',params.matdata,params.tag);
   save(params_file,'params');  
 end;

