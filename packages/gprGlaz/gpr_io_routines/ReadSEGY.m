function [comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,...
          origpx,origpy,tti,zeff,profnr,date,info,profinfo,...
          vel,head,azi,pitch,roll,hag] = ReadSEGY(segyname)
          
% exclue .sgy in segyname
if findstr('.sgy',segyname)
   segyname = strtok(segyname,'.');
end   
segyfile = [segyname '.sgy'];
fid=fopen(segyfile,'r');

% -------------------------------------------------------------------------
% ------------------- READ 3200-byte TEXTUAL HEADER -----------------------
% -------------------------------------------------------------------------
comments = fread(fid,3200,'char'); % Comments from header


% -------------------------------------------------------------------------
% ------------------- READ 400-byte FILE HEADER ---------------------------
% -------------------------------------------------------------------------
hd.idnumber=fread(fid,1,'int32');       % 3201-3204 Job identification number
hd.profnumber=fread(fid,1,'int32');     % 3205-3208 Profile Line number
hd.rnumber=fread(fid,1,'int32');        % 3209-3212 Reel number
hd.ntr=fread(fid,1,'int16');            % 3213-3214 Number of data traces per ensemble
hd.auxntr=fread(fid,1,'int16');         % 3215-3216 Number of auxilary traces per ensemble
hd.sra=fread(fid,1,'int16');            % 3217-3218 Sample interval [10^-9 s]
hd.fsra=fread(fid,1,'int16');           % 3219-3220 Sample interval [10^-9 s] of original field data
hd.nsamp=fread(fid,1,'int16');          % 3221-3222 Number of samples per trace
hd.fnsamp=fread(fid,1,'int16');         % 3223-3224 Number of samples per trace of original field data
hd.format=fread(fid,1,'int16');         % 3225-3226 Data sample format code (4-byte IEEE folating-point)
hd.ensfold=fread(fid,1,'int16');        % 3227-3228 Ensamble fold (ntr per trace ensamble)
hd.trsort=fread(fid,1,'int16');         % 3229-3230 Trace sorting code (1 = As recorded)
hd.vsum=fread(fid,1,'int16');           % 3231-3232 Vertical sum code (1 = no sum)
hd.swfreqS=fread(fid,1,'int16');        % 3233-3234 Sweep frequency at Start
hd.swfreqE=fread(fid,1,'int16');        % 3235-3236 Sweep frequency at End
hd.sweeplength=fread(fid,1,'int16');    % 3237-3238 Sweep length
hd.sweepcode=fread(fid,1,'int16');      % 3239-3240 Sweep type code
hd.sweepntr=fread(fid,1,'int16');       % 3241-3242 Trace number of sweep channel
hd.sweeptrlengthS=fread(fid,1,'int16'); % 3243-3244 Sweep trace taper length at start
hd.sweeptrlengthE=fread(fid,1,'int16'); % 3245-3246 Sweep trace taper length at end
hd.tapertype=fread(fid,1,'int16');      % 3247-3248 Taper type
hd.corrtr=fread(fid,1,'int16');         % 3249-3250 Correlated data traces
hd.gain=fread(fid,1,'int16');           % 3251-3252 Binary gain recovered (yes or no)
hd.ampmethod=fread(fid,1,'int16');      % 3250-3254 Amplitude recovery method
hd.meas=fread(fid,1,'int16');           % 3255-3256 Measurement system (meters or feet)
hd.sigpolarity=fread(fid,1,'int16');    % 3257-3258 Impulse signal polarity
hd.vibpolarity=fread(fid,1,'int16');    % 3259-3260 Vibratory polarity code
hd.unas1=fread(fid,120,'int16');        % 3261-3500 Unsassigned
hd.segyformat=fread(fid,1,'int16');     % 3501-3502 SEG Y format revision number
hd.traceflag=fread(fid,1,'int16');      % 3503-3504 Fixed lenght trace flag
hd.extendheader=fread(fid,1,'int16');   % 3505-3506 Number of 3200-byte extended file header records
hd.unas2=fread(fid,47,'int16');         % 3507-3600 Unassigned


% -------------------------------------------------------------------------
% ------------------- READ 240-byte TRACE HEADERS -------------------------
% -------------------------------------------------------------------------
for a = 1:hd.ntr
    
    th.trnumberl(a)=fread(fid,1,'int32');       % 1-4   Trace sequence number within line
    th.trnumberf(a)=fread(fid,1,'int32');       % 5-8   Trace sequence number within file
    th.ffid(a)=fread(fid,1,'int32');            % 9-12  Original field record number, FFID
    th.trnumberorig(a)=fread(fid,1,'int32');    % 13-16 Trace number in original field record
    th.sourcenr(a)=fread(fid,1,'int32');        % 17-20 Energy source point number
    th.cdpnr(a)=fread(fid,1,'int32');           % 21-24 CDP number
    th.cdptr(a)=fread(fid,1,'int32');           % 25-28 Trace number in CDP (1 trace per CDP)
    th.idcode(a)=fread(fid,1,'int16');          % 29-30 Trace identification code
    th.vsumtr(a)=fread(fid,1,'int16');          % 31-32 Number of vertically summed traces yielding to this trace
    th.hsumtr(a)=fread(fid,1,'int16');          % 33-34 Number of horizontally summed traces yielding to this trace
    th.duse(a)=fread(fid,1,'int16');            % 35-36 Data use
    
    th.offset(a)=fread(fid,1,'int32');          % 37-40 Tx-Rx offset [cm]
    th.zr(a)=fread(fid,1,'int32');              % 41-44 Receiver elevation
    th.zs(a)=fread(fid,1,'int32');              % 45-48 Source elevation
    th.sdepth(a)=fread(fid,1,'int32');          % 49-52 Source depth below surface
    th.zdemr(a)=fread(fid,1,'int32');           % 53-56 Datum elevation at receiver
    th.zdems(a)=fread(fid,1,'int32');           % 57-60 Datum elevation at source
    th.zeffs(a)=fread(fid,1,'int32');           % 61-64 Tx Ground elevation: zeff = pz-(tti/2)*vair)
    th.zeffr(a)=fread(fid,1,'int32');           % 65-68 Rx Ground elevation: zeff = pz-(tti/2)*vair)
    
    th.escaler(a)=fread(fid,1,'int16');         % 69-70 Scalar applied to all elevations
    th.kscaler(a)=fread(fid,1,'int16');         % 71-72 Scalar applied to all coordinates
    
    th.xs(a)=fread(fid,1,'int32');              % 73-76 Source coordinate x
    th.ys(a)=fread(fid,1,'int32');              % 77-80 Source coordinate y
    th.xr(a)=fread(fid,1,'int32');              % 81-84 Receiver coordinate x
    th.yr(a)=fread(fid,1,'int32');              % 85-88 Receiver coordinate y
    
    th.unit(a)=fread(fid,1,'int16');            % 89-90 Coordinate units (length,s,...)
    
    fseek(fid,24,'cof');                        % 91-114  Weathering velocity, static correction, ...
    
    th.nsamp(a)=fread(fid,1,'int16');           % 115-116 Number of samples in this trce
    th.sra(a)=fread(fid,1,'int16');             % 117-118 Sample interval for this trace
    
    fseek(fid,38,'cof');                        % 119-156 Gain type, sweep frequency, filers, ...
    
    th.year(a)=fread(fid,1,'int16');            % 157-158 Year data recorded (4-digits)
    th.day(a)=fread(fid,1,'int16');             % 159-160 Day of yera (Julian day)
    th.hour(a)=fread(fid,1,'int16');            % 161-162 Hour of day
    th.minute(a)=fread(fid,1,'int16');          % 163-164 Minute of hour
    th.second(a)=fread(fid,1,'int16');          % 165-166 Second of minute
    th.timebase(a)=fread(fid,1,'int16');        % 167-168 Time basis code: 1 = Local
    
    fseek(fid,12,'cof');                        % 119-180 Trace weighting factor, Group number, ...
    
    th.origpx(a)=fread(fid,1,'int32');             % 181-184 Projected x coordinate on profile line
    th.origpy(a)=fread(fid,1,'int32');             % 185-188 Projected y coordinate on profile line
    
    th.vel(a)=fread(fid,1,'float32');           % 189-192 Velocity of antenna
    th.head(a)=fread(fid,1,'float32');           % 193-196 Heading of profile
    th.azi(a)=fread(fid,1,'float32');           % 197-200 Heading of antenna
    th.pitch(a)=fread(fid,1,'float32');           % 201-204 Pitch of antenna
    th.roll(a)=fread(fid,1,'float32');           % 205-208 Roll of antenna
    th.hag(a)=fread(fid,1,'float32');           % 209-212 Height above ground of antenna
    
    fseek(fid,20,'cof');                        % 213-232

    th.tti(a)=fread(fid,1,'float32');           % 233-236 tti (Picked air traveltime)
    th.zdem(a)=fread(fid,1,'float32');          % 237-240 Digital elevation map zdem
    
    nsamp = hd.nsamp;
     d(:,a)=fread(fid,nsamp,'float32');          % Data: File for Matlab/Promax: 4-byte floating point value

end

fclose(fid);

ntr = hd.ntr;
nsamp = hd.nsamp;
sra = hd.sra/1000;


offset = th.offset(1)/100; % Tx-Rx offset in [m]
pz = th.zr./abs(th.kscaler);
px = th.xr./abs(th.kscaler);
py = th.yr./abs(th.kscaler);
origpx = th.origpx./abs(th.kscaler);
origpy = th.origpy./abs(th.kscaler);
tti = th.tti;
data = d;
zdem = th.zdem;
zeff = th.zeffs./abs(th.escaler);
profnr = hd.profnumber;

date.y = th.year(1);
date.dj = th.day(1);
date.hr = th.hour(1);
date.min = th.minute(1);

vel = th.vel;
head = th.head;
azi = th.azi;
pitch = th.pitch;
roll = th.roll;
hag = th.hag;

% -------------------------------------------------------------------------
% ------------------- CONVERT COMMENTS FROM TEXTUAL HEADER ----------------
% -------------------------------------------------------------------------
comments = char(reshape(comments,40,80));

for j=1:40
  pos = strfind(comments(j,:),'Date');  
  if length(pos)>0, 
     date.d = str2num((comments(j,pos+9:pos+10))); % day
     date.m = str2num((comments(1,pos+6:pos+7))); % month
     date.y = str2num((comments(1,pos+12:pos+15))); % year
  end  
  pos = strfind(comments(j,:),'AREA');
  if length(pos)>0, info.area = strtrim(comments(j,pos+5:end));end % area
  pos = strfind(comments(j,:),'INSTRUMENT');  
  if length(pos)>0, info.instr = strtrim(comments(j,pos+12:end));end % instrument
  pos = strfind(comments(j,:),'OFFSET');  
  if length(pos)>0, info.offset = str2num(comments(j,pos+8:pos+15));end % offset
  pos = strfind(comments(j,:),'FREQUENCY');  
  if length(pos)>0, info.freq = str2num(comments(j,pos+11:pos+12));end % offset
  pos = strfind(comments(j,:),'DEWOW SOFTWARE');  
  if length(pos)>0, info.dewsoft = strtrim(comments(j,pos+16:end));end % instrument

% -----READ profinfo out of Textual file header -----
  pos = strfind(comments(j,:),'NUMBER OF SEGMENT POINTS');  
  if length(pos)>0,
    profinfo.n = str2num(comments(j,pos+26:pos+27)); % number of points
    for c = 1:profinfo.n-1
    xy = str2num(comments(j+c,4:80));
    profinfo.x(c,1) = xy(1,1);
    profinfo.y(c,1) = xy(1,2);
    profinfo.x(profinfo.n,1) = xy(2,1);
    profinfo.y(profinfo.n,1) = xy(2,2);
    end
  end
  
  % if header contains no formatted information as required for the heli
  % GPR data, create dummy values
  check = ~exist('profinfo');
  if check,
    profinfo.n = 1; profinfo.x = 0; profinfo.y = 0;
    info.area = 'unknown'; info.instr = 'unknwonw'; info.offset = 'unknown';
    info.freq = 'unknonw'; info.dewsoft = 'unknown';
  end  
    
end

