function [] = ekko_gpsfile_in_two(gps_file, outfile1, outfile2)

% ekko_gpsfile_in_two reads in a puls ekko *.GPS file
% and splits it into two separate files 
% It writes every other trace into the other file
% Typical application is a Puls_Ekko multi channel recording with two antennas
% as the Demux program does not split the *.GPS file, only the *.HD and *.DT1 files
% 


fid = fopen(sprintf('%s',gps_file),'rt');
nlines = 0;
while (fgets(fid) ~= -1),
  nlines = nlines+1;
end
frewind(fid); 
trace = textscan(fid,'%s','CommentStyle','$','Delimiter','');
fclose(fid);
% 
% gga = textscan(fid,'%s','CommentStyle',{'Trace','$GPGGA'})%,...
                       % 'CommentStyle','$GPGSA',...
                      % 'CommentStyle','$GPGGA');
                      

%gga = {}; j=1;
gga = [];
C = textread(gps_file, '%s','delimiter', '\n');
rowindx = 1;
while rowindx <= nlines
    match = findstr(cell2mat(C(rowindx)),'$GPGGA'); 
    if ~isempty(match) 
       gga = [gga; C(rowindx)];
    end
    rowindx = rowindx+1;
    if rowindx > nlines, break; end;
end    
%gga =cell2mat(gga{1});
%trace = cell2mat(trace{1});

%if gga still empry, fill it with zero values
if isempty(gga)
    gga = num2cell(zeros(nlines/2,1));
end    

%write 2 gps files, taking every other entry
fid1 = fopen(sprintf('%s',outfile1),'wt');
fid2 = fopen(sprintf('%s',outfile2),'wt');
for i=1:2:length(gga)-1;
    t = cell2mat(trace{1}(i));
    %g1 = cell2mat(gga{1}(i));
    g1 = gga{i};
    %g2 = cell2mat(gga{1}(i+1));
    g2 = gga{i+1};
    k1 = strfind(t,'#');
    k2 = strfind(t,'at');
    new_t = [t(1:k1) num2str(round(i/2)) ' ' t(k2:end)];
    fprintf(fid1,'%s\n',new_t);
    fprintf(fid1,'%s\n',g1);
    fprintf(fid2,'%s\n',new_t);
    fprintf(fid2,'%s\n',g2);
end;   

%in case number of gps points are even, add one more trace to ch01
%if mod(length(gga),2) == 1,
%  t = cell2mat(trace{1}(i+2));  
%  g1 = gga{i};
%  k1 = strfind(t,'#');
%  new_t = [t(1:k1) num2str(round(i/2)) ' ' t(k2:end)];
%  fprintf(fid1,'%s\n',new_t);
%  fprintf(fid1,'%s\n',g1);
%end  

fclose all;



fprintf('New file %s created!! \n',outfile1)
fprintf('New file %s created!! \n',outfile2)

'stop'