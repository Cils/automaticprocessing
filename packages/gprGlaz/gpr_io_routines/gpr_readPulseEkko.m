function [gprdat n] = gpr_readPulseEkko(para, n)
%function gprdat = gpr_readPulseEkko(para)
% read PulseEkko *.DT1 files.
% if a parameter file *.HD exists in the same folder, it will also be read.

fid = fopen([para.path para.datafile],'rb');
if(fid == -1)
    error('Binary input file (*.DT1) does not exist');
end
%determine the number of bytes
fseek(fid,0,'eof');
byte_end = ftell(fid);
fseek(fid,0,'bof');
% open header file if it exists
if(exist([para.path para.datafile(1:end-3) 'HD']))
    fh = fopen([para.path para.datafile(1:end-3) 'HD'],'r');
	fgetl(fh); fgetl(fh); fgetl(fh); fgetl(fh); fgetl(fh); fgetl(fh);
    line = fgetl(fh);
    gprdat.nr_of_traces = str2num(line(22:end)); fgetl(fh);
    line = fgetl(fh);
    gprdat.nr_of_samples = str2num(line(22:end)); fgetl(fh);
    line = fgetl(fh);    
    gprdat.t0 = str2num(line(22:end)); fgetl(fh);
    line = fgetl(fh);
    gprdat.time_window = str2num(line(22:end)); fgetl(fh);
    fgetl(fh);fgetl(fh);fgetl(fh);
    fgetl(fh);fgetl(fh);fgetl(fh);
    fgetl(fh);fgetl(fh);
    line = fgetl(fh);
    gprdat.frequency = str2num(line(22:end)); fgetl(fh);
    line = fgetl(fh);
    gprdat.separation = str2num(line(22:end)); fgetl(fh);
    fclose(fh);
elseif(exist([para.path para.datafile(1:end-3) 'hd']))
	fh = fopen([para.path para.datafile(1:end-3) 'hd'],'r');
    fgetl(fh); fgetl(fh); fgetl(fh); fgetl(fh); fgetl(fh); fgetl(fh);
    line = fgetl(fh);
    gprdat.nr_of_traces = str2num(line(22:end)); fgetl(fh);
    line = fgetl(fh);
    gprdat.nr_of_samples = str2num(line(22:end)); fgetl(fh);
    line = fgetl(fh);    
    gprdat.t0 = str2num(line(22:end)); fgetl(fh);
    line = fgetl(fh);
    gprdat.time_window = str2num(line(22:end)); fgetl(fh);
    fclose(fh);
else
    disp('Reading only binary file. Header file not found!')
    %determine number of samples and traces
    Header = fread(fid,32,'float32');
    gprdat.nr_of_samples = Header(3);
    gprdat.nr_of_traces = byte_end/(2*gprdat.nr_of_samples + 4*32);
    fseek(fid,0,'bof');
    clear Header
end

tra = 0;
gprdat.data =zeros(gprdat.nr_of_samples, gprdat.nr_of_traces);
gprdat.headers=zeros(32, gprdat.nr_of_traces);

byte_pos = 0;

while (byte_pos < byte_end)
    
   tra = tra + 1;
   %read trace header
   Header = fread(fid,32,'float32');
   byte_pos = byte_pos + 4*32;
   %read trace
   Record = fread(fid,Header(3),'int16');
   byte_pos = byte_pos + 2*Header(3);
   if 0 %(exist([s(1:end-3) 'HD']))
       Header(5) = t0;
       Header(9) = time_window;
   elseif(~Header(9))
       Header(9) = Header(3)*Header(7)/1000;
   end
   
   gprdat.headers(:,tra)= Header;
   gprdat.data(:,tra) = Record;
   
   clear Header Record;
  
end
gprdat.antenna_frequency = gprdat.frequency;
gprdat.sampling_rate = gprdat.time_window/gprdat.nr_of_samples;
gprdat.number_stacks = gprdat.headers(8,1);
gprdat.dataz   = [];
gprdat.x       = [];
gprdat.y       = [];
gprdat.z       = [];
gprdat.pos0    = [];
gprdat.profdir = [];

fclose(fid);

if nargin > 1
    n = n +1;
end