function write_init(parName, parValue)

% write initialization parameters to [prefdir '\GPRglaz_init.mat']
% parName:  name of parameter
% parValue:  value of parameter

initPar = read_init();

try
    initFile = [prefdir '\GPRglaz_init.mat'];
    initPar.(parName)=parValue;
    save(initFile, 'initPar');
catch ex
    rethrow(ex) %remove this line, if write_init or read_init causes troubles
end