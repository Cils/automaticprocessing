function [] = sgy_header2ascii(folder)

%takes all segy files in all subdirectories of "folder" 
%and creates ascii files with all ascii header information and places it
%in the same directory as the input file

%folder = 'B:\OLD_STUFF\Glacier_GPR\Testdata';

% create a list of all segy files with in all subdirectories of "folder"
files_all = ReadFileNames(folder,{'sgy'});
hits=~cellfun(@isempty,regexp(files_all,'sgy'));
hit_indices = find(hits==1);
files = files_all([hit_indices]);


%loop over all files and write text header into array "thead"
%thead is then written into outfiles in same directory
k = 1;
n_coords=0;
coords = zeros(n_coords,3);
for i=1:length(files),
  data = segy_openFile(files{i},'r','native','ascii');  
  thead = SEGY_ReadTextHeader(data,'ascii');
  thead = char(reshape(thead,40,80));
  length_prefix = find(files{i}=='.');
  outfile = strcat(substring(files{i},0,length_prefix-2),'_header.txt');
  dlmwrite(outfile, thead,'delimiter','');
  fprintf('New file %s created!! \n',outfile)
  fclose('all'); 
end 

