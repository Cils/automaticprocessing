function [] = gpr_exportPromSEGY(indat, params)

sgyname = params.sgyname;
% sgyname: filename of outputfile
% comments: header information - if not empty, copied to header
% data: traces
% sr: sample rate in ns
% offset: src-rec distance
% xs, ys: coordinates of source
% xr, yr: coordinates of receiver
% xc, yc: coordinates of cdp
% zdem: local topography

comments = [];
data = indat.data;
sr = indat.sampling_rate;
offset = zeros(size(indat.x));

xs = indat.x;
ys = indat.y;
zs = indat.z;

xr = indat.x;
yr = indat.y;
zr = indat.z;

xc = indat.x;
yc = indat.y;

zdem = indat.z;
 
[nsamp ntraces] = size(data);
data(isnan(data)) = 0;


%%% ntraces = 50    % for test purposes only

fid = fopen(sgyname,'wb');

NULL = 0;
ONE = 1;
MONE = -1;
FIVE = 5;

% write ASCII - Header
tdummy = repmat(uint8(32),80,40);
if ~isempty(comments)
   cc=char(comments().data);
   tdummy(1:size(cc,2),1:size(cc,1))=cc';
end
fwrite(fid,tdummy,'char');

%disp (['text file header written - byte pos: ' num2str(ftell(fid))]);
% disp ('text file header written');

% SEGY - Binary Reel Header
dummy = ones(3,1);
fwrite(fid,dummy,'int32');  % 3201-3212

% no of traces per record and no of aux. traces
ntr = 1;
fwrite(fid,ntr,'int16');  % 3213-3214
fwrite(fid,NULL,'int16'); % 3215-3216

% sampling rate and nsamp
sra = round(sr * 1000 );   %%%% CHECK %%%%
fwrite(fid,sra,'int16');  % 3217-3218
fwrite(fid,sra,'int16');  % 3219-3220
fwrite(fid,nsamp,'int16');  %3221-3222
fwrite(fid,nsamp,'int16');  %3223-3224

% floating point format    % special for Promax (ieee number format)
fwrite(fid,FIVE,'int16');  % 3225-3226

dummy = zeros(14,1);       % 3227-3254
fwrite(fid,dummy,'int16');

% meters
fwrite(fid,ONE,'int16');   % 3255-3256

dummy = zeros(122,1);
fwrite(fid,dummy,'int16');  % 3257-3500

fwrite(fid,1*256+0,'int16');  % 3501-3502  Version 1.0
fwrite(fid,ONE,'int16');      % 3503-3504  for V. 1.0 (1=fixed length traces)

dummy = zeros(48,1);
fwrite(fid,dummy,'int16');  % 3505-3600

% disp (['binary file header written - byte pos: ' num2str(ftell(fid))]);
% disp ('binary file header written');

ttraces=num2str(ntraces);
h=waitbar(0,[' traces processed: 0 / ' ttraces],'Name','...writing ProMAX SEGY file');

% write 240 byte trace headers
for a = 1:ntraces % dd.n
 fwrite(fid,a,'int32');     %  1 -  4  trace number in line
 fwrite(fid,a,'int32');     %  5 -  8  trace number in reel
 fwrite(fid,a,'int32');     %  9 - 12  FFID
 fwrite(fid,ONE,'int32');   % 13 - 16  trace number in record
 fwrite(fid,a,'int32');     % 17 - 20  source point number
 fwrite(fid,a,'int32');     % 21 - 24  cdp number
 fwrite(fid,ONE,'int32');   % 25 - 28  trace number in CDP
 dummy = ones(4,1);
 fwrite(fid,dummy,'int16');  % 29 - 36  
 %offset=
 fwrite(fid,offset(a),'int32'); % 37 - 40  src-rec offset

 escaler = 1000;   % scalar for elevations

 fwrite(fid,round(zr(a)*escaler),'int32');   % 41 - 44  receiver elevation
 fwrite(fid,round(zs(a)*escaler),'int32');   % 45 - 48  source elevation
 fwrite(fid,NULL*escaler,'int32');   % 49 - 52  source depth
 fwrite(fid,round(zdem(a)*escaler),'int32'); % 53-56  datum elev rec
 fwrite(fid,round(zdem(a)*escaler),'int32'); % 57-60 datum elev src
 dummy = zeros(2,1);
 fwrite(fid,dummy,'int32');  % 61 - 68   water depth rec/src

 if (escaler ~= 1); escaler=-escaler; end;
 kscaler = 1000;   % scaler for x-/y-coordinates
 
 fwrite(fid,escaler,'int16');   % 69 - 70  scaler for elevations   %%%% CHECK %%%%
 fwrite(fid,-kscaler,'int16');  % 71 - 72  scaler for coordinates  %%%% CHECK %%%%
 fwrite(fid,round(xs(a)*kscaler),'int32'); %  73 -  76  source x     %%%% CHECK %%%%
 fwrite(fid,round(ys(a)*kscaler),'int32'); %  77 -  80  source y     %%%% CHECK %%%%
 fwrite(fid,round(xr(a)*kscaler),'int32'); %  81 -  84  receiver x   %%%% CHECK %%%%
 fwrite(fid,round(yr(a)*kscaler),'int32'); %  85 -  88  receiver y   %%%% CHECK %%%%
 fwrite(fid,ONE,'int16');                  %  89 -  90  units
 dummy = zeros(12,1);
 fwrite(fid,dummy,'int16');                %  91 - 114  
 fwrite(fid,nsamp,'int16');                % 115 - 116  #samples this trace
 fwrite(fid,round(sr * 1000),'int16');     % 117 - 118 sample rate     %%%% CHECK %%%%
 dummy = zeros(31,1);
 fwrite(fid,dummy,'int16');                % 119 - 180

 %  These are for SEGY-REV 1 
 fwrite(fid,xc(a)*kscaler,'int32');        % 181 - 184   CDP x 
 fwrite(fid,yc(a)*kscaler,'int32');        % 185 - 188   CDP y
 
 dummy = zeros(26,1);
 fwrite(fid,dummy,'int16'); % 189 - 240
 
%  disp ([num2str(a) 'trace header written - byte pos: ' num2str(ftell(fid))]);


 fwrite(fid,data(:,a),'float');
 
%  disp ([num2str(a) ' trace data written - byte pos: ' num2str(ftell(fid))]);

 waitbar(a/ntraces,h,[' traces processed: ' num2str(a) ' / ' ttraces]);

end;
close(h);
fclose(fid);
disp ([num2str(a) ' traces written to file ' sgyname]);
disp ('use ''SEG-Y Input'' ,  Type ''MS-DOS'' to read data in ProMAX');






end

