function [] = gpr_coord2shape(indata, para)


myshp(1).Geometry = 'PolyLine';
myshp(1).BoundingBox = [min(indata.x) min(indata.y) max(indata.x) max(indata.y)];
myshp(1).X =indata.x;
myshp(1).Y =indata.y;
myshp(1).OBJECTID  =para.name;
myshp(1).OBJECTVAL = 1;

shapewrite(myshp(1), strcat(para.name,'.shp'))

if isfield(para,'projection')
    fid =fopen(strcat(para.name,'.prj'),'w');
    fprintf(fid,'%s\n',para.projection);
    fclose(fid);
else
    load('LV03LN02');
    fid =fopen(strcat(para.name,'.prj'),'w');
    fprintf(fid,'%s\n',LV03LN02);
    disp('default projection LV03LN02')
    fclose(fid);
end
    

end