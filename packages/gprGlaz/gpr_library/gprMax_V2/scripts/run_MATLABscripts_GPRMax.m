[Header, Fields] = gprmax( '../MacOs/rebar.sca' );

figure

it = Header.iterations;
l = -0.5;
u = 0.5;


subplot(311)
imagesc(squeeze(Fields.ex));
colormap('gray');
caxis([l u])
ylim([0 it])
title('Ex')

subplot(312)
imagesc(squeeze(Fields.ey));
colormap('gray');
caxis([l u])
ylim([0 it])
title('Ey')

subplot(313)
imagesc(squeeze(Fields.ez)); 
colormap('gray');
caxis([l u])
ylim([0 it])
title('Ez')



figure
plot(squeeze(Fields.hy(:,1,5)))
hold on
plot(squeeze(Fields.hy(:,1,35))*-1,'r')
legend('Pos. angle','Neg. angle')



x = 1:20:40;                                  %width                          
y = 1;                                  %dimension
z = 1;                                   %height
figure, 
cameratoolbar('SetCoordSys','z');
cameratoolbar('setmode','orbit');
slice(mesh,x,y,z);
axis tight;
xlabel('x');
ylabel('y');
zlabel('z');



x = 50;    %1:20:100;                                  %width                          
y = 1;                                  %dimension
z = 85;                                   %height
figure, 
cameratoolbar('SetCoordSys','z');
cameratoolbar('setmode','orbit');
colormap bone;
slice(Rx.ey,x,y,z);
axis tight;
xlabel('x');
ylabel('y');
zlabel('z');
set(findobj(gca,'Type','Surface'),'EdgeColor','none')