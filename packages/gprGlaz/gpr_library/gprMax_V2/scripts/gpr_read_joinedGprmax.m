function [gprdat n] = gpr_read_joinedGprmax(para, n)

% function for reading in several gprmax.out files and joining them to read them into processing routine

[Header,Fields]=gprmax(para.input1);

gla_1_fields = Fields;
gla_1_header = Header;

gla_1_fields.ex = squeeze(gla_1_fields.ex);
gla_1_fields.ey = squeeze(gla_1_fields.ey);
gla_1_fields.ez = squeeze(gla_1_fields.ez);
gla_1_fields.hx = squeeze(gla_1_fields.hx);
gla_1_fields.hy = squeeze(gla_1_fields.hy);
gla_1_fields.hz = squeeze(gla_1_fields.hz);
gla_1_fields.ix = squeeze(gla_1_fields.ix);
gla_1_fields.iy = squeeze(gla_1_fields.iy);
gla_1_fields.iz = squeeze(gla_1_fields.iz);

%%
[Header,Fields]=gprmax(para.input2);

gla_2_fields = Fields;

gla_2_fields.ex = squeeze(gla_2_fields.ex);
gla_2_fields.ey = squeeze(gla_2_fields.ey);
gla_2_fields.ez = squeeze(gla_2_fields.ez);
gla_2_fields.hx = squeeze(gla_2_fields.hx);
gla_2_fields.hy = squeeze(gla_2_fields.hy);
gla_2_fields.hz = squeeze(gla_2_fields.hz);
gla_2_fields.ix = squeeze(gla_2_fields.ix);
gla_2_fields.iy = squeeze(gla_2_fields.iy);
gla_2_fields.iz = squeeze(gla_2_fields.iz);

%%
[Header,Fields]=gprmax(para.input3);

gla_3_fields = Fields;

gla_3_fields.ex = squeeze(gla_3_fields.ex);
gla_3_fields.ey = squeeze(gla_3_fields.ey);
gla_3_fields.ez = squeeze(gla_3_fields.ez);
gla_3_fields.hx = squeeze(gla_3_fields.hx);
gla_3_fields.hy = squeeze(gla_3_fields.hy);
gla_3_fields.hz = squeeze(gla_3_fields.hz);
gla_3_fields.ix = squeeze(gla_3_fields.ix);
gla_3_fields.iy = squeeze(gla_3_fields.iy);
gla_3_fields.iz = squeeze(gla_3_fields.iz);

%%
[Header,Fields]=gprmax(para.input4);

gla_4_fields = Fields;

gla_4_fields.ex = squeeze(gla_4_fields.ex);
gla_4_fields.ey = squeeze(gla_4_fields.ey);
gla_4_fields.ez = squeeze(gla_4_fields.ez);
gla_4_fields.hx = squeeze(gla_4_fields.hx);
gla_4_fields.hy = squeeze(gla_4_fields.hy);
gla_4_fields.hz = squeeze(gla_4_fields.hz);
gla_4_fields.ix = squeeze(gla_4_fields.ix);
gla_4_fields.iy = squeeze(gla_4_fields.iy);
gla_4_fields.iz = squeeze(gla_4_fields.iz);

%%
[Header,Fields]=gprmax(para.input5);

gla_5_fields = Fields;

gla_5_fields.ex =  squeeze(gla_5_fields.ex);
gla_5_fields.ey =  squeeze(gla_5_fields.ey);
gla_5_fields.ez =  squeeze(gla_5_fields.ez);
gla_5_fields.hx =  squeeze(gla_5_fields.hx);
gla_5_fields.hy =  squeeze(gla_5_fields.hy);
gla_5_fields.hz =  squeeze(gla_5_fields.hz);
gla_5_fields.ix =  squeeze(gla_5_fields.ix);
gla_5_fields.iy =  squeeze(gla_5_fields.iy);
gla_5_fields.iz =  squeeze(gla_5_fields.iz);


%%
Fields.ex = horzcat(gla_1_fields.ex,gla_2_fields.ex,gla_3_fields.ex,gla_4_fields.ex,gla_5_fields.ex);
Fields.ey = horzcat(gla_1_fields.ey,gla_2_fields.ey,gla_3_fields.ey,gla_4_fields.ey,gla_5_fields.ey);
Fields.ez = horzcat(gla_1_fields.ez,gla_2_fields.ez,gla_3_fields.ez,gla_4_fields.ez,gla_5_fields.ez);
Fields.hx = horzcat(gla_1_fields.hx,gla_2_fields.hx,gla_3_fields.hx,gla_4_fields.hx,gla_5_fields.hx);
Fields.hy = horzcat(gla_1_fields.hy,gla_2_fields.hy,gla_3_fields.hy,gla_4_fields.hy,gla_5_fields.hy);
Fields.hz = horzcat(gla_1_fields.hz,gla_2_fields.hz,gla_3_fields.hz,gla_4_fields.hz,gla_5_fields.hz);
Fields.ix = horzcat(gla_1_fields.ix,gla_2_fields.ix,gla_3_fields.ix,gla_4_fields.ix,gla_5_fields.ix);
Fields.iy = horzcat(gla_1_fields.iy,gla_2_fields.iy,gla_3_fields.iy,gla_4_fields.iy,gla_5_fields.iy);
Fields.iz = horzcat(gla_1_fields.iz,gla_2_fields.iz,gla_3_fields.iz,gla_4_fields.iz,gla_5_fields.iz);

%%
figure

it = Header.iterations;
l = -0.01;
u = 0.01;


subplot(311)
imagesc(Fields.ex);
colormap('gray');
caxis([l u])
colorbar
ylim([0 it])
title('Ex')

subplot(312)
imagesc(Fields.ey);
colormap('gray');
caxis([l u])
colorbar
ylim([0 it])
title('Ey')

subplot(313)
imagesc(Fields.ez); 
colormap('gray');
caxis([l u])
colorbar
ylim([0 it])
title('Ez')

Header = gla_1_header;

if Header.polarization == 120           %transverse
    gprdat.data = Fields.ex;                                                            % always gives the strongest electromagnetic field component for the specific antenna orientation
elseif Header.polarization == 121       %axial
    gprdat.data = Fields.ey;   
else 
    gprdat.data = Fields.ez;
end

[a b] = size(Fields.ex);
gprdat.nr_of_traces = b;
gprdat.nr_of_samples = Header.iterations;
gprdat.t0 = [];
gprdat.time_window = Header.removed/1e-9;

if Header.TxStepX ~= 0
    gprdat.separation = (Header.rx - Header.tx)*Header.dx;
elseif Header.TxStepY ~= 0
    gprdat.separation = (Header.ry - Header.ty)*Header.dy;   
else %Header.TxStepZ ~= 0
    gprdat.separation = (Header.rz - Header.tz)*Header.dz;
end

gprdat.frequency = (str2num(Header.title))/1e6;                                                     
gprdat.sampling_rate = Header.dt*1e9;
gprdat.number_stacks = []
gprdat.dataz   = [];


gprdat.x       = (gla_1_header.tx + (gla_1_header.TxStepX * (0:1:b-1)))*gla_1_header.dx;                     %coordinates of transmitter
gprdat.y       = (gla_1_header.ty + (gla_1_header.TxStepY * (0:1:b-1)))*gla_1_header.dy;
gprdat.z       = (gla_1_header.tz + (gla_1_header.TxStepZ * (0:1:b-1)))*gla_1_header.dz;
gprdat.pos0    = [];


if nargin > 1
    n = n +1;
end