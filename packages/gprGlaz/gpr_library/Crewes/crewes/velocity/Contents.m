% CREWES velocity manipulation toolbox
%Tools to convert velocity functions from one form to another
%
% DRAWVINT ... draw interval velocity as a piecewise constant function
% VAVE2VINT ... convert average velocity to interval velocity
% VINT2T ... compute time given interval velocity versus depth
% VINT2VAVE ... convert interval velocity to average
% VINT2VRMS ... convert interval to rms velocity
% VRMS2VINT ... convert rms to interval velocity
% VRMS2VAVE ... convert rms to average velocity
% VZ2VRMS ... convert an interval velocity model in depth to vrms in time
% VelocityAnalysis ... interactive velocity analysis tool for shot gathers
% GetVelocities ... returns velocites from the analysis tool
%
% $Id: Contents.m,v 1.5 2009/05/25 21:00:55 cmhogan Exp $

