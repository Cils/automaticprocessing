function [mdatacc,mdatadec,illum]=pspi_mig(fdata,f,parms,pspi_parms,dx,dz,xshot,stab,tmax)
%[mdatacc,mdatadec,illum]=pspi_mig(fdata,f,parms,pspi_parms,dx,dz,xshot,stab,tmax)
%
%***prestack pspi depth migration***
%
%To see an example, type 'pspi_salt_psdm_script' in matlab.
%
%Please see 'Ferguson_Margrave_2005.pdf' or 'Ferguson and Margrave, 2005,
%Planned seismic imaging using explicit one-way operators, Geophysics, V70,
%NO5, S101 - S109' for details.
%
% Data prep: Your shot record and your velocity model should have exactly
% the same number of columns. Generally the shot has fewer columns than the
% model at start, so either pad the shot or reduce the model.
%
%mdatacc...depth migrated output using crosscorrelation imaging contition
%mdatadec...depth migrated output using stabilized deconvolution imaging contition
%fdata...f-kx spectrum of shot record 'fdata=ifft(fft(data),[],2);'.
%        NOTE f axis is band-limited and +ve only.
%        NOTE kx axis is uncentred.
%        NOTE, relative to mxn velocity model 'parms', it is assumed that
%        the source is located at n/2+1.
%f...frequency axis (Hz, must correspond to fdata).
%parms...velocity model.
%pspi_parms...blocky (in the lateral direction) velocity model.
%dx...trace spacing (m).
%dz...depth interval (m).
%xshot ... x coordinate of shot. Trace 1 is assumed to be zero.
%  ****** default is (round(size(fdata,2)/2)+1)*dx ******
%stab ... stability factor used in stabilized deconvolution imaging
%   condition. Must be contained in [0,1] .
%  ****** default is .0001 ******
%tmax ... maximum record time of signal in the input data. THis does not
%           include any zero pad. Programming this to a nonzero number will
%           cause the algorithm to re-zero the data at times greater than
%           this value every 10 dz steps. This adds some overhead but can 
%           prevent annoying wraparound. tmax=0 causes no re-zero action.
%  ****** default is 0 **********
%
%R. J. Ferguson, 2009
%
% NOTE: It is illegal for you to use this software for a purpose other
% than non-profit education or research UNLESS you are employed by a CREWES
% Project sponsor. By using this software, you are agreeing to the terms
% detailed in this software's Matlab source file.
 
% BEGIN TERMS OF USE LICENSE
%
% This SOFTWARE is maintained by the CREWES Project at the Department
% of Geology and Geophysics of the University of Calgary, Calgary,
% Alberta, Canada.  The copyright and ownership is jointly held by 
% its author (identified above) and the CREWES Project.  The CREWES 
% project may be contacted via email at:  crewesinfo@crewes.org
% 
% The term 'SOFTWARE' refers to the Matlab source code, translations to
% any other computer language, or object code
%
% Terms of use of this SOFTWARE
%
% 1) Use of this SOFTWARE by any for-profit commercial organization is
%    expressly forbidden unless said organization is a CREWES Project
%    Sponsor.
%
% 2) A CREWES Project sponsor may use this SOFTWARE under the terms of the 
%    CREWES Project Sponsorship agreement.
%
% 3) A student or employee of a non-profit educational institution may 
%    use this SOFTWARE subject to the following terms and conditions:
%    - this SOFTWARE is for teaching or research purposes only.
%    - this SOFTWARE may be distributed to other students or researchers 
%      provided that these license terms are included.
%    - reselling the SOFTWARE, or including it or any portion of it, in any
%      software that will be resold is expressly forbidden.
%    - transfering the SOFTWARE in any form to a commercial firm or any 
%      other for-profit organization is expressly forbidden.
%
% END TERMS OF USE LICENSE

%***get sizes of things***
[Nz,Nx]=size(parms);
[rd,cd]=size(fdata);
[rf,cf]=size(f(:));
%*************************

%***Err check***
if rf~=rd; error('freq axis wrong');end
if Nx~=cd; error('x axis wrong');end
%***************
if(nargin<9)
    tmax=0;
end
if(nargin==8)
    if(stab<0 || stab>1)
        error('stab must lie in [0,1]');
    end
else
    stab=.0001;
end

if(nargin<7)
    xshot=(round(cd/2)+1)*dx;
end

%***build the source***
%temp=zeros(rd,cd);
%temp(:,round(cd/2)+1)=1;
%fsou=ifft(temp,[],2);
%**********************

%***build the source***
temp=zeros(rd,cd);
for j=1:rf
	temp(j,:)=greenseed2(1,dx*[0:cd-1],xshot,f(j),f(end),parms(1,:),dz,1);
end
fsou=ifft(temp,[],2);
%**********************

mdatacc=zeros(Nz,cd);
mdatadec=zeros(Nz,cd);
illum=zeros(Nz,cd);
%ftemp=zeros(rd,cd);
%stemp=zeros(rd,cd);
%rtemp=zeros(rd,cd);
time1=clock;
timeused=0.0;
ievery=25;
for j=1:Nz-1
    if((rem(j,ievery)-2)==0)
        disp([' pspi prestack mig working on depth ',num2str(j),' of ',num2str(Nz),...
            ' time left ~ ' int2str(timeremaining) '(s)'])
    else
        disp([' pspi prestack mig working on depth ',num2str(j),' of ',num2str(Nz)])
    end
    if(rem(j,10)==0 & tmax~=0)
        fdata=ps_rezero(fdata,f,dx,tmax);
    end
	ftemp=pspi_ips(fdata,f,dx,parms(j,:),pspi_parms(j,:),dz);
	stemp=pspi_ips(fsou,f,dx,parms(j,:),pspi_parms(j,:),-dz);
	rcc=ftemp.*conj(stemp);%trivial reflectivity estimate
    illumination=stemp.*conj(stemp);
    rdec=rcc./(illumination+stab*max(abs(illumination(:))));
	mdatacc(j+1,:)=real(sum(rcc)+sum(rcc(1:rd-1,:)))/(2*rd-1)/2/pi;
    mdatadec(j+1,:)=real(sum(rdec)+sum(rdec(1:rd-1,:)))/(2*rd-1)/2/pi;
    illum(j+1,:)=real(sum(illumination)+sum(illumination(1:rd-1,:)))/(2*rd-1);
	fdata=ifft(ftemp,[],2);
	fsou=ifft(stemp,[],2);
    timenow=clock;
    timeused=etime(timenow,time1)+timeused;
    time1=timenow;
    timeremaining=(Nz-1)*timeused/j-timeused;
    %disp([' elapsed time ',int2str(timeused),' (s), estimated time remaining '...
    %    ,int2str(timeremaining),' (s)']);
end
disp(['shot migrated in ' int2str(timeused) '(s)'])
