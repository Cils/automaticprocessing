% CREWES migration toolbox
%
% Post Stack migration
%  FD15MIG: 15 degree finite-difference time migration (2D)
%  FKMIG: Stolt's fk migration (2D)
%  KIRK: simplified Kirchhoff time migration (2D)
%  KIRK_MIG: full-featured Kirchhoff time migration (2D)
%  KIRK_MIG3D: 3D post stack Kirchhoff time migration
%  PS_MIGT: time migration by Gazdag phase shift (2D)
%  PSNSPS_MIG: Exploding reflector depth migration by NSPS (2D)
%  SPLITSTEPF_MIG: Split-step Fourier depth migration (2D)
%  VZ_FKMIG: fk migration for v(z) (2D)
%  VZ_FKMOD: V(z) modelling by an fk technique (2D)
%
% Prestack migration
%  KIRK_SHOT: 2D prestack Kirchhoff migration of a single shot gather
%  KIRK_SHOT3D: 3D prestack Kirchhoff migration of a single shot gather
%  KIRK_SHOT3DFZ: 3D prestack Kirchhoff migration into depth planes. More
%       accurate but slower than KIRK_SHOT3D
%  PSPI_SHOT: 2D prestack PSPI migration of a single shot record
%  PSPI_SHOT_CWAVE: 2D prestack PSPI migration of a single shot record for PS events
%
% Ferguson
%  IPS: Isotropic phase shift extrapolation (stationary)
%  FX_IPS: Isotropic 80 degree f-x extrapolation
%  FX_MIG: prestack 80 degree fx depth migration
%  FX_ZERO_MIG: fx zero offset migration
%  GAZ_3C_MIG: prestack gazdag depth 3C migration
%  GAZ_MIG: prestack gazdag depth migration
%  GAZ_ZERO_MIG: generalized screen zero-offset migration
%  GS_IPS: Isotropic generalized phase extrapolation
%  GS_MIG: prestack generalized screen depth migration
%  GS_ZERO_MIG: generalized screen zero-offset migration
%  PSPI_3C_MIG: prestack pspi 3c depth migration
%  PSPI_IPS: Isotropic pspi extrapolation
%  PSPI_MIG: prestack pspi depth migration
%  PSPI_ZERO_MIG: pspi zero-offset migration
%  SS_3C_MIG: prestack splitestep 3c depth migration
%  SS_IPS: Isotropic split step extrapolation
%  SS_MIG: prestack splitestep depth migration
%  SS_ZERO_MIG: generalized screen zero-offset migration
%  UNIQUE_VELS: tba
%
% Utilities
%  BAGAINI: convert a velocity model to piecewise-constant (for PSPI)
%  CONV45: used by KIRK_MIG for 45 degree phase shift
%  COS_TAPER: used by KIRK_MIG
%  CLININT: Complex-valued linear interpolation (used by FKMIG)
%  CSINCI: complex valued sinc function interpolation
%  TIME2DEPTH: Convert a trace from time to depth by a simple stretch
%  DEPTH2TIME: Convert a trace from depth to time by a simple stretch
%  GAUSSIAN_SMOOTHER: smooth a velocity model by convolution with a gaussian
%  GREENSEED: calculate a 3D Helmholtz Green's function
%  GREENSEED2: calculate a 2D Helmholtz Green's function
%  GAINCC: gain correct a migrated shot after migration with the
%           cross-correlation imaging contition.
%  MIGSTACK: stack a line of migrated shots while applying a mute.
%  SLICE3D: extract a horizontal slice from a 3D volume using interpolation
%  PS_STEP: a friendly version if IPS
%  PS_REZERO: used by PSPI_SHOT to re-zero the temporal pad during migration
%
% Scripts (working demos)
%  VZ_MOD_SIM: script to demo vz_fkmod
%  VZ_FK_SIM: script to demo vz_fkmig
%  THRUST_TIME: demo time migration of the thrust model
%
% Scripts from RJ Ferguson  (working demos)
% Please see 'Ferguson_Margrave_2005.pdf' or 'Ferguson and Margrave, 2005,
% Planned seismic imaging using explicit one-way operators, Geophysics, V70
% regarding the following scripts:
%
% fx_salt_psdm_script    
% fx_salt_zero_script
% gs_salt_psdm_script
% gs_salt_zero_script
% gaz_salt_3c_psdm_script
% gaz_salt_psdm_script
% gaz_salt_zero_script
% pspi_salt_3c_psdm_script
% pspi_salt_psdm_script
% pspi_salt_zero_script
% ss_salt_3c_psdm_script   
% ss_salt_psdm_script             
% ss_salt_zero_script                 
%
