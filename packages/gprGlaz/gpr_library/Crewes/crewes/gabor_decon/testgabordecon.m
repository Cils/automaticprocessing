dt=.002;tmax=2;q=25;%f=define the synthetic to be deconvolved
twin=.1;tinc=.01;%defines the windows. tinc is not used for cosine windows

stab=0;phase=1;%decon stab factor and phase flag
iburg=0;%set to zero for Fourier algorithm
iwiener=1;%set to 1 for an AGC-WIENER-AGC comparison, leave this alone or risk trouble
iopt=1;%1 means analysis windows, 2 is synthesis, 3 is both
ibigfigs=0;% set to zero to turn of big figures

idemo=0;%demo forward and inverse gabor
igaborfact=0;% set to zero to turn off plotting of the factors of the input Gabor spectrum

%plotflags
plottraces=1; %plot time domain traces
plotfourier=0; %plot Fourier amplitude spectra of traces
gaborplot=1;% set to zero to turn off all ploting of Gabor spectra regardless of the next flags
plotgab_input=0; %plot Gabor spectrum of input (attenuated) signal
plotgab_wavelet=1; %plot Gabor spectrum of estimated wavelet
plotgab_refl=0; %plot Gabor spectrum of actual reflectivity
plotgab_refl_est=1; %plot Gabor spectrum of reflectivity estimate
plotgab_wiener=0;%plot Gabor spectrum of Wiener estimate of reflectivity

if(iburg)
    titalg=' Burg,';
else
    titalg=' Fourier,';
end
if(iopt==1)
    titwin=' analysis,';
elseif(iopt==2)
    titwin=' synthesis,';
elseif(iopt==3)
    titwin=' analysis and synthesis,';
end
if(~exist('rq'))
    [rq,t]=reflec(tmax,dt,.2);
    [w,tw]=wavemin(dt,20,tmax/10);
    qmat=qmatrix(q,t,w,tw);
    %s=convm(r,w);
    sq=qmat*rq;
end

tsmo=max(t);fsmo=5;%define deconvolution smoothers

if(idemo) %demo forward and inverse transforms
   
   [sqtvs,trow,fcol]=fgabor(sq,t,twin,tinc,1,0);

   sqq=igabor(sqtvs,fcol);
   sqq=sqq(1:length(sq));%unpad
   
   figure;
   inc=.8*max(sq);

   plot(t,sq,t,sqq+inc,t,sq-sqq+2*inc);
   xlabel('Time in seconds')
    title('signal reconstruction after forward and inverse gabor')
    nudge=-.1*inc;
    start=.5*tmax;
    text(start,nudge,'Attenuated signal');
    text(start,nudge+inc,'After forward+inverse Gabor');
    text(start,nudge+2*inc,'Difference');
    yoff
    
    if(ibigfigs)
        bigfig;whitefig;boldlines(gca,6);bigfont;
    end
end

if(~iburg)
   [r2,tvs_op]=gabordecon(sq,t,twin,tinc,tsmo,fsmo,stab,phase,iopt);
else
   [r2,tvs_op]=gabordeconb(sq,t,twin,tinc,tsmo,fsmo,10,stab,phase,iopt);
   %r2=gabordeconbq(sq,t,twin,tinc,tsmo,fsmo,10,stab,phase,q,iopt);
end


if(iwiener)
	% wiener for comparison
	izone=near(t,.3*tmax,.7*tmax);
	sqa=aec(sq,t,tmax/3);
	r3=deconw(sqa,sqa(izone),100);
    r3=aec(r3,t,tmax/3);
	r3=balans(r3,rq);
end

fnyq=1/(2*dt);
fmax=fnyq/2;
fwid=fnyq/20;
if(plottraces)

	r2=filtf(r2,t,[0 0],[fmax fwid]);
	r2=balans(r2,rq);
	rq2=filtf(rq,t,[0 0],[fmax fwid]);
	rq2=balans(rq2,rq);
	figure;
	%subplot(2,1,1)
	sq2=balans(sq,rq);
	plot(t,sq2,'b',t,r3+.1,'g',t,-r2+.2,'r',t,rq2+.3,'k')
	xlabel('Time in seconds')
	title([titalg titwin 'twin=' num2str(twin) ...
            ' tinc=' num2str(tinc) ' tsmo=' num2str(tsmo) ...
            ' fsmo=' num2str(fsmo) ' stab=' num2str(stab)])
	nudge=.05;
	start=.7*tmax;
	text(start,nudge,'Attenuated signal');
	text(start,nudge+.1,'After AGC+Wiener');
	text(start,nudge+.2,['After Gabor ' titalg(1:end-1)]);
	text(start,nudge+.3,'Bandlimited reflectivity');
	yoff
	
	if(ibigfigs)
            bigfig;whitefig;boldlines(gca,6);bigfont;
	end
end
%subplot(2,1,2)
if(plotfourier)
	figure
	[R,f]=fftrl(rq2,t);
	R2=fftrl(r2,t);
	R3=fftrl(r3,t);
	SQ=fftrl(sq2,t);
	plot(f,abs(SQ)/max(abs(SQ)),'b',f,abs(R3)/max(abs(R3))+1,'g',f,abs(R2)/max(abs(R2))+2,'r',f,abs(R)/max(abs(R))+3,'k')
	xlabel('Frequency in Hz')
	title([titalg titwin 'twin=' num2str(twin) ...
            ' tinc=' num2str(tinc) ' tsmo=' num2str(tsmo) ...
            ' fsmo=' num2str(fsmo) ' stab=' num2str(stab)])
	nudge=.2;
	start=.6*fnyq;
	text(start,nudge,'Attenuated signal');
	text(start,nudge+1,'After AGC+Wiener+AGC');
	text(start,nudge+2,['After Gabor ' titalg(1:end-1)]);
	text(start,nudge+3,'Bandlimited reflectivity');
	yoff
	
	if(ibigfigs)
            bigfig;whitefig;boldlines(gca,6);bigfont;
	end
end

%make some gabor plots
if(gaborplot==1)
   if(plotgab_input)
      
       [sqtvs,trow,fcol]=fgabor(sq,t,twin,tinc,1,0);
       plotimage(abs(sqtvs),trow,fcol);
       title('Attenutated signal');
       xlabel('Frequency in Hz');ylabel('Time in seconds')
       if(ibigfigs)
        bigfig;whitefig;boldlines;bigfont;
       end
   end
   if(plotgab_wavelet)
       if(~exist('trow'))
           [sqtvs,trow,fcol]=fgabor(sq,t,twin,tinc,1,0);
       end
       plotimage(1./abs(tvs_op),trow,fcol);
       title('Propagating wavelet');
       xlabel('Frequency in Hz');ylabel('Time in seconds')
       if(ibigfigs)
        bigfig;whitefig;boldlines;bigfont;
       end
   end
   
   if(plotgab_refl)

       [rqtvs,trow,fcol]=fgabor(rq,t,twin,tinc,1,0);
       plotimage(abs(rqtvs),trow,fcol);
       title('Reflectivity');
       xlabel('Frequency in Hz');ylabel('Time in seconds')
       if(ibigfigs)
        bigfig;whitefig;boldlines;bigfont;
       end
   end
   if(plotgab_refl_est)
       
       [r2tvs,trow,fcol]=fgabor(r2,t,twin,tinc,1,0);
       plotimage(abs(r2tvs),trow,fcol);
       if(iburg)
        title('After Gabor Decon (Burg)');
       else
        title('After Gabor Decon (Fourier)');
       end
       xlabel('Frequency in Hz');ylabel('Time in seconds')
       if(ibigfigs)
        bigfig;whitefig;boldlines;bigfont;
       end
   end
   
   if(plotgab_wiener)
       
       [r3tvs,trow,fcol]=fgabor(r3,t,twin,tinc,1,0);
       plotimage(abs(r3tvs),trow,fcol);
       title('After AGC+Wiener Decon');
       xlabel('Frequency in Hz');ylabel('Time in seconds')
       if(ibigfigs)
        bigfig;whitefig;boldlines;bigfont;
       end
   end
  
 end
 
 if(igaborfact)
   alpha=exp(-pi*trow(:)*fcol/q);
   plotimage(alpha,trow,fcol);
   title('Constant Q surface');
   xlabel('Frequency in Hz');ylabel('Time in seconds')
   if(ibigfigs)
    bigfig;whitefig;boldlines;bigfont;
   end
   [W,fw]=fftrl(pad(w,padpow2(sq)),t);
   plotimage(ones(length(trow),1)*abs(W'),trow,fw);
   title('Wavelet surface');
   xlabel('Frequency in Hz');ylabel('Time in seconds')
   if(ibigfigs)
    bigfig;whitefig;boldlines;bigfont;
   end

   plotimage((ones(length(trow),1)*abs(W')).*alpha.*abs(rqtvs),trow,fw);
   title('Gabor spectrum model');
   xlabel('Frequency in Hz');ylabel('Time in seconds')
   if(ibigfigs)
    bigfig;whitefig;boldlines;bigfont;
   end
 
end
