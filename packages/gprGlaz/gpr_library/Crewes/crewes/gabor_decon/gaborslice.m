function [slices,trow,tcol]=gaborslice(signal,t,twin,tinc,p)
% GABORSLICE: produces a set of Gabor slices using modified Gaussian windows
%
% [slices,trow,tcol]=gaborslice(signal,t,twin,tinc,p)
% 
% A Gabor slice is the result of multiplying a signal by a Gaussian whose
% center is at some particular time. A complete set of slices can be
% constructed such that they sum to recreated the signal. In order for the
% recreatiion to be exact, the Gaussians must be slightly modified (see
% gaussian_upou). The set of modified Gaussians sum exactly to one over the
% length of the input signal and are said to form a partition of unity
% (POU). An fft over the rows of the output gives a forward Gabor
% transform.
%
% signal= input trace 
% t= time coordinate vector for signal
% twin= width (seconds) of the Gaussian window
% tinc= temporal increment between windows
% p= exponent to define analysis windows. If g is a modified Gaussian
%   forming a POU, then the analysis window is g^p
%  ************ default p=1 **************
% slices= matrix of Gabor slice slices, one slice per column. The number of
%   rows is the same as the number of samples of signal, while the number
%   of columns is determined by the number of windows in the POU.
% gwin= Gaussian window. It is the same size as signal
%
% by G.F. Margrave, July 2009
%
% NOTE: It is illegal for you to use this software for a purpose other
% than non-profit education or research UNLESS you are employed by a CREWES
% Project sponsor. By using this software, you are agreeing to the terms
% detailed in this software's Matlab source file.
 
% BEGIN TERMS OF USE LICENSE
%
% This SOFTWARE is maintained by the CREWES Project at the Department
% of Geology and Geophysics of the University of Calgary, Calgary,
% Alberta, Canada.  The copyright and ownership is jointly held by 
% its author (identified above) and the CREWES Project.  The CREWES 
% project may be contacted via email at:  crewesinfo@crewes.org
% 
% The term 'SOFTWARE' refers to the Matlab source code, translations to
% any other computer language, or object code
%
% Terms of use of this SOFTWARE
%
% 1) Use of this SOFTWARE by any for-profit commercial organization is
%    expressly forbidden unless said organization is a CREWES Project
%    Sponsor.
%
% 2) A CREWES Project sponsor may use this SOFTWARE under the terms of the 
%    CREWES Project Sponsorship agreement.
%
% 3) A student or employee of a non-profit educational institution may 
%    use this SOFTWARE subject to the following terms and conditions:
%    - this SOFTWARE is for teaching or research purposes only.
%    - this SOFTWARE may be distributed to other students or researchers 
%      provided that these license terms are included.
%    - reselling the SOFTWARE, or including it or any portion of it, in any
%      software that will be resold is expressly forbidden.
%    - transfering the SOFTWARE in any form to a commercial firm or any 
%      other for-profit organization is expressly forbidden.
%
% END TERMS OF USE LICENSE
if(nargin<5)
    p=1;
end
tmin=t(1);
%make sure we have row vectors
[m,n]=size(signal);
if(n==1); signal=signal.'; end
[m,n]=size(t);
if(n==1); t=t'; end
%build first window and POU norm factor
if(p~=0)
    [gwin,norm_factor,tinc,nwin]=gaussian_upou(t,tmin,twin,tinc);
end
tcol=(0:nwin-1)*tinc+tmin;
trow=t;
%loop over windows
slices=zeros(length(signal),nwin);
for k=1:nwin
    if(p==0)
        gwin=ones(size(signal));
    else
        %build the gaussian
        tnot=(k-1)*tinc;
        gwin=gaussian_upou(t,tnot,twin,tinc,norm_factor);
        if(p~=1)
            gwin=gwin.^p;
        end
    end
    %window
    slices(:,k)=(gwin.*signal)';
end
