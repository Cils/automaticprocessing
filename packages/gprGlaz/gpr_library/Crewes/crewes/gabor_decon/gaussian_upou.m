function [gaus,norm_factor,xinc,nwin]=gaussian_upou(x,xnot,xwid,xinc,norm_factor)
% GAUSSIAN_UPOU: design a uniform partition of unity based on Gaussians.
%
% [gaus,normfactor,xinc,nwin]=gaussian_upou(x,xnot,xwid,xinc,norm_factor)
%
% This function designs a uniform partition of unity (POU) for a finite portion
% of the real line. The partition is based on the Gaussian window but the
% window is modified to ensure that the partition is exact. The
% modification amounts to creating an approximate POU using the exact
% Gaussians and then summing these Gaussians. The result of this summation
% is then used as a "normalization factor" in that each Gaussian is divided
% (pointwise) by the sum to create the modified Gaussian. The modified
% Gaussians then form an exact POU. This function is designed to return a
% single modified Gaussian each time it is called. On the first invocation,
% the normalization factor is computed and returned as well. Providing this
% factor on input to subsequent calls allows faster execution.
%
% x ... x coordinate vector describing the segment of the real line for
%   which the POU is to be designed.
% xnot ... x coordinate of the center of the desired modified Gaussian.
% xwid ... the Gaussian half-width. The Gaussian will have amplitude 1/e at
%   xnot+xwid and xnot-xwid.
% xinc ... increment between window centers. The POU will have a window
%   centered at min(x) and another at max(x) and intermediate windows
%   spaced at xinc. This means (max(x)-min(x))/xinc must be an integer. It
%   will be adjusted from the input value to the nearest value giving an
%   integer.
% norm_factor ... a vector the same length as x giving the normalization
%   factor required to make the POU exact. If omitted or specified as a
%   different size vector than x, it is computed and returned.
% gaus ... modified Gaussian window given by
%   gaus=exp(-((x-xnot)/xwid).^2)./norm_factor
% norm_factor ... Normalization factor
% xinc ... adjusted value of xinc to give an integer number of windows
% nwin ... number of windows required for the desired POU
%
% To see an example of the use of this function, examine fgabor and igabor.
%
% by G.F. Margrave, July 2009
%
% NOTE: It is illegal for you to use this software for a purpose other
% than non-profit education or research UNLESS you are employed by a CREWES
% Project sponsor. By using this software, you are agreeing to the terms
% detailed in this software's Matlab source file.
 
% BEGIN TERMS OF USE LICENSE
%
% This SOFTWARE is maintained by the CREWES Project at the Department
% of Geology and Geophysics of the University of Calgary, Calgary,
% Alberta, Canada.  The copyright and ownership is jointly held by 
% its author (identified above) and the CREWES Project.  The CREWES 
% project may be contacted via email at:  crewesinfo@crewes.org
% 
% The term 'SOFTWARE' refers to the Matlab source code, translations to
% any other computer language, or object code
%
% Terms of use of this SOFTWARE
%
% 1) Use of this SOFTWARE by any for-profit commercial organization is
%    expressly forbidden unless said organization is a CREWES Project
%    Sponsor.
%
% 2) A CREWES Project sponsor may use this SOFTWARE under the terms of the 
%    CREWES Project Sponsorship agreement.
%
% 3) A student or employee of a non-profit educational institution may 
%    use this SOFTWARE subject to the following terms and conditions:
%    - this SOFTWARE is for teaching or research purposes only.
%    - this SOFTWARE may be distributed to other students or researchers 
%      provided that these license terms are included.
%    - reselling the SOFTWARE, or including it or any portion of it, in any
%      software that will be resold is expressly forbidden.
%    - transfering the SOFTWARE in any form to a commercial firm or any 
%      other for-profit organization is expressly forbidden.
%
% END TERMS OF USE LICENSE


if(nargin<5)
    norm_factor=0;
end

%modify xinc if needed
nwin=round((max(x)-min(x))/xinc)+1;
xinc=(max(x)-min(x))/nwin;

nx=length(x);
%see if we need to create the normalization factor
if(length(norm_factor)~=nx || sum(abs(norm_factor(:)))==0)
    norm_factor=zeros(size(x));
    x0=min(x);
    for k=1:nwin
      norm_factor=norm_factor+make_gaussian(x,x0,xwid);
      x0=x0+xinc;
    end
end

if(~between(max(x),min(x),xnot,2))
    error('Gaussian origin not contained in x vector')
end

gaus=make_gaussian(x,xnot,xwid)./norm_factor;

function gaus=make_gaussian(x,xnot,xwid)
gaus=exp(-((x-xnot)/xwid).^2);
    