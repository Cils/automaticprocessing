function [tvs,tout,fout]=fgabor(trin,t,twin,tinc,p,padflag)
% FGABOR: forward Gabor transform with Gaussian analysis windowing
%
% [tvs,tout,fout]=fgabor(trin,t,twin,tinc,p,padflag)
% 
% FGABOR performs a forward Gabor transform of a seismic trace using a
% modified Gaussian analysis window. (See GAUSSIAN_UPOU for a discussion.)
% The transform is implemented by windowing the 
% trace multiple times with a temporally shifted Gaussian window. An 
% ordinary fft is performed over each window. The output is a 2D matrix,
% called tvs, with the row coordinate being the time of the center of each
% Gaussian window and the column coordinate being the Fourier frequency in
% Hz. This tvs, or time variant spectrum, is also called the Gabor spectrum.
% The Gabor spectrum may be inverted with either IGABOR or IGABOR_SYN.
%
% trin ... input trace 
% t ... time coordinate vector for trin
% twin ... half-width (seconds) of the Gaussian window
% tinc ... temporal shift (seconds) between windows
% p ... exponent used in analysis windowing. If g is the modified Gaussian
%   window then the analysis window actually used is g.^p . 
%   The value of p must lie in the interval [0,1].
% ************** default p=1 ************************
% NOTE: It is essential to use the same windowing parameters and p value in
%   IGABOR as in FGABOR.
% padflag ... if 0, the trace is transformed without padding. If 1,
%   it is padded with zeros to the next power of 2 (unless it already is
%   a power of 2)
% ************** default = 1 ***************
% tvs ... output time-variant spectrum (complex valued)
% tout ... column vector giving the row coordinate of tvs
% fout ... row vector giving the column coordinate of tvs
%
% by G.F. Margrave, May 2001, updated July 2009
%
% NOTE: It is illegal for you to use this software for a purpose other
% than non-profit education or research UNLESS you are employed by a CREWES
% Project sponsor. By using this software, you are agreeing to the terms
% detailed in this software's Matlab source file.
 
% BEGIN TERMS OF USE LICENSE
%
% This SOFTWARE is maintained by the CREWES Project at the Department
% of Geology and Geophysics of the University of Calgary, Calgary,
% Alberta, Canada.  The copyright and ownership is jointly held by 
% its author (identified above) and the CREWES Project.  The CREWES 
% project may be contacted via email at:  crewesinfo@crewes.org
% 
% The term 'SOFTWARE' refers to the Matlab source code, translations to
% any other computer language, or object code
%
% Terms of use of this SOFTWARE
%
% 1) Use of this SOFTWARE by any for-profit commercial organization is
%    expressly forbidden unless said organization is a CREWES Project
%    Sponsor.
%
% 2) A CREWES Project sponsor may use this SOFTWARE under the terms of the 
%    CREWES Project Sponsorship agreement.
%
% 3) A student or employee of a non-profit educational institution may 
%    use this SOFTWARE subject to the following terms and conditions:
%    - this SOFTWARE is for teaching or research purposes only.
%    - this SOFTWARE may be distributed to other students or researchers 
%      provided that these license terms are included.
%    - reselling the SOFTWARE, or including it or any portion of it, in any
%      software that will be resold is expressly forbidden.
%    - transfering the SOFTWARE in any form to a commercial firm or any 
%      other for-profit organization is expressly forbidden.
%
% END TERMS OF USE LICENSE
if(nargin<5); p=1; end
if(nargin<6); padflag=1; end
if(~between(1,0,p,2))
    error('p must lie in the interval [0,1]')
end
tmin=t(1);
%make sure we have row vectors
[m,n]=size(trin);
if(n==1); trin=trin.'; end
[m,n]=size(t);
if(n==1); t=t'; end
%pad trace
if(padflag)
    trin=padpow2(trin);
    t=(0:length(trin)-1)*(t(2)-t(1));
end
%build first window and POU norm factor
[gwin,norm_factor,tinc,nwin]=gaussian_upou(t,tmin,twin,tinc);
tout=(0:nwin-1)*tinc+tmin;
%loop over windows
for k=1:nwin
    %build the gaussian
    tnot=(k-1)*tinc;
    gwin=gaussian_upou(t,tnot,twin,tinc,norm_factor);
    if(p<1)
        if(p==0)
            gwin=ones(size(trin));
        else
            gwin=gwin.^p;
        end
    end
    %window and fft
    if(k==1)
        [tmp,fout]=fftrl(gwin.*trin,t);
        tvs=zeros(nwin,length(tmp));
        tvs(k,:)=tmp;
    else
        tvs(k,:)=fftrl(gwin.*trin,t);
    end
end
