% Gabor Deconvolution Toolbox
%
% NOTE: to make a simple constant Q synthetic with a minimum phase source
% wavelet, try:
% >> [wm,tw]=wavemin(.002,20,.3); %wavemin is in SYNTRACES toolbox
% >> [r,t]=reflec(1,.002); %wavemin is in SYNTRACES toolbox
% >> qmat=qmatrix(50,t,wm,tw); %wavemin is in SYNTRACES toolbox
% >> s=qmat*r; %matrix vector multiply installs Q
% >> figure;plot(t,r,t,s+.1,'r');legend('reflectivity','Seismogram with attenuation')
% >> plotimage(qmat,t,t);title('The Q matrix') %view with 'Max scaling'
%
% Transforms
%  FGABOR: forward Gabor transform with modified Gaussian analysis windowing
%  IGABOR: inverse Gabor transform with modified Gaussian synthesis windowing
%  GABORSLICE: produce a single Gabor slice of a signal using Gaussian windowing
%
% Deconvolution
%  GABORDECON: seismic deconvolution using the Gabor transform with Fourier operator design.
%  GABORDECONB: seismic deconvolution using the Gabor transform with Burg operator design.
%
% Utilities
%  GAUSSIAN_UPOU: Design a uniform partition of unity (POU) using modified Gaussians.
%  FILTHYP: Bandpass filtering with filter parameters following time-frequency hyperbolae.
%  HYPERSMOOTH: smooth a time variant spectrum along hyperbolic contours.
% 
