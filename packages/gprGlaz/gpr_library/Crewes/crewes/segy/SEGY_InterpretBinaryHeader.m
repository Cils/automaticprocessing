function bheadtxt = SEGY_InterpretBinaryHeader(segy)

% bheadtxt = SEGY_InterpretBinaryHeader(segy)
%
% Return cell array containing human readable text from 
% switchable binary header values
%
% Kevin Hall, 2009
%
bhead = segy.bhead;
bheadtxt{30}= '';

% 1. bhead.jobid  = fread(FILE,1,   'int');
% 2. bhead.lino  = fread(FILE,1,    'int'); 
% 3. bhead.reno= fread(FILE,  1,    'int');  
% 4. bhead.ntrpr = fread(FILE,  1,   'short');
% 5. bhead.nart = fread(FILE,  1,    'short');
% 6. bhead.hdt = fread(FILE,   1,    'ushort'); 
% 7. bhead.dto = fread(FILE,   1,    'ushort');
% 8. bhead.hns = fread(FILE, 1,      'ushort');
% 9. bhead.nso = fread(FILE, 1,      'ushort');
% 10. bhead.format = fread(FILE, 1,   'short');
switch bhead.format
    case {1}
        bheadtxt{10}='4-byte IBM floating point';
    case {2}
        bheadtxt{10}='4-byte, two''s complement integer';
    case {3}
        bheadtxt{10}='2-byte, two''s complement integer';
    case {4}
        bheadtxt{10}='4-byte, fixed point with gain (obsolete)';
    case {5}
        bheadtxt{10}='4-byte IEEE floating point';
    case {8}
        bheadtxt{10}='1-byte, two''s complement integer';
    otherwise
        bheadtxt{10}='Unknown';
end
% 11. bhead.fold = fread(FILE, 1,     'short');
% 12. bhead.tsort = fread(FILE, 1,    'short');
switch bhead.tsort
    case {-1}
        bheadtxt{12}='Other';
    case {1}
        bheadtxt{12}='As recorded (no sorting)';
    case {2}
        bheadtxt{12}='CDP ensemble';
    case {3}
        bheadtxt{12}='Single fold continuous profile';
    case {4}
        bheadtxt{12}='Horizontally stacked';
    case {5}
        bheadtxt{12}='Common source point';
    case {6}
        bheadtxt{12}='Common receiver point';
    case {7}
        bheadtxt{12}='Common offset point';
    case {8}
        bheadtxt{12}='Common mid-point';
    case {9}
        bheadtxt{12}='Common conversion point';
    otherwise
        bheadtxt{12}='Unknown';
end
% 13. bhead.vscode = fread(FILE, 1,   'short');
switch bhead.tsort
    case {1}
        bheadtxt{13}='No sum';
    case {2}
        bheadtxt{13}='Two sum';
    otherwise
        bheadtxt{13}='N = M-1 sum';
end
% 14. bhead.hsfs = fread(FILE, 1,     'short');
% 15. bhead.hsfe = fread(FILE, 1,     'short');
% 16. bhead.hslen = fread(FILE, 1,    'short');
% 17. bhead.hstyp = fread(FILE, 1,    'short');
switch bhead.hstyp
    case {1}
        bheadtxt{17}='Linear';
    case {2}
        bheadtxt{17}='Parabolic';
    case {3}
        bheadtxt{17}='Exponential';        
    otherwise
        bheadtxt{17}='Other';
end

% 18. bhead.schn = fread(FILE, 1,     'short');
% 19. bhead.hstas = fread(FILE,1,     'short');
% 20. bhead.hstae = fread(FILE, 1,    'short');
% 21. bhead.htatyp = fread(FILE, 1,   'short');
switch bhead.htatyp
    case {1}
        bheadtxt{21}='Linear';
    case {2}
        bheadtxt{21}='Cos^2';      
    otherwise
        bheadtxt{21}='Other';
end
% 22. bhead.hcorr = fread(FILE, 1,    'short');
switch bhead.htatyp
    case {1}
        bheadtxt{22}='No';
    case {2}
        bheadtxt{22}='Yes';      
    otherwise
        bheadtxt{22}='Unknown';
end
% 23. bhead.bgrcv = fread(FILE, 1,    'short');
switch bhead.bgrcv
    case {1}
        bheadtxt{23}='Yes';
    case {2}
        bheadtxt{23}='No';      
    otherwise
        bheadtxt{23}='Unknown';
end
% 24. bhead.rcvm = fread(FILE, 1,     'short');
switch bhead.rcvm
    case {1}
        bheadtxt{24}='None';
    case {2}
        bheadtxt{24}='Spherical Divergence';      
    case {3}
        bheadtxt{24}='AGC';
    case {4}
        bheadtxt{24}='Other';      
    otherwise
        bheadtxt{24}='Unknown';
end


% 25. bhead.mfeet = fread(FILE, 1,    'short');
switch bhead.mfeet
    case {1}
        bheadtxt{25}='Meters';
    case {2}
        bheadtxt{25}='Feet';      
    otherwise
        bheadtxt{25}='Unknown';
end
% 26. bhead.polyt = fread(FILE, 1,    'short');
switch bhead.polyt
    case {1}
        bheadtxt{26}='Pressure increase or geophone case moves up = (-)';
    case {2}
        bheadtxt{26}='Pressure increase or geophone case moves up = (+)';      
    otherwise
        bheadtxt{26}='Unknown';
end
% 27. bhead.vpol = fread(FILE, 1,     'short');
switch bhead.vpol
    case {1}
        bheadtxt{27}='337.5 to 22.5 degrees';
    case {2}
        bheadtxt{27}='22.5 to 67.5 degrees';
    case {3}
        bheadtxt{27}='67.5 to 112.5 degrees';
    case {4}
        bheadtxt{27}='112.5 to 157.5 degrees';
    case {5}
        bheadtxt{27}='157.5 to 202.5 degrees';
    case {6}
        bheadtxt{27}='202.5 to 247.5 degrees';
    case {7}
        bheadtxt{27}='247.5 to 292.5 degrees';
    case {8}
        bheadtxt{27}='292.5 to 337.5 degrees';
    otherwise
        bheadtxt{27}='Unknown';
end
% 28. bhead.rev = fread(FILE, 1,      'short');
a = typecast(swapbytes(int16(bhead.rev)),'uint8');
bheadtxt{28} = ['SEG-Y format revision number: ' num2str(a(1)+a(2)/100)];

% 29. bhead.trfix = fread(FILE, 1,    'short');
switch bhead.trfix
    case {1}
        bheadtxt{29}='Fixed trace length';
    case {0}
        bheadtxt{29}='Variable trace length';      
    otherwise
        bheadtxt{29}='Unknown';
end
% 30. bhead.nthdr = fread(FILE, 1,    'short');
% 31. bhead.tracebytelen = (1920 + (bhead.hns * 32)) / 8;
bheadtxt{31}='';
