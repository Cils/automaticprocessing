function [machinefmt,textfmt] = SEGY_AnalyzeFile(segyfilename)

% [machinecode,txtfmt] = SEGY_AnalyzeFile(filename)
%
% Examine a SEG-Y file and try to determine binary and text formats
%
% machinefmt = ieee-be (default guess), ieee-le (test for this)
% txtfmt     = ascii (default guess), ebcdic (test for this)
%
% Kevin Hall, 2009
%
% Todo: Error checking of fseek and fread
%

%Initialize variables to defaults
textfmt    = 'ascii';   % default guess (not segy rev 0 standard)
machinefmt = 'ieee-be'; % default guess (segy standard)

%open file for reading
fid = fopen(segyfilename, 'r', machinefmt);
if(fid == -1)
    error('Error: fopen() failed in SEGY_AnalyzeFile()'); 
end

%Textual header format
%   EBCDIC chars can have values > 127
%   Only change textfmt if the text header can be proven to be EBCDIC
textheader = char(fread(fid, 3200, 'uchar'))';
if ~isempty(find(textheader > 127))
  textfmt = 'ebcdic';
end

%Binary data byte order
%    SEG-Y data format code will be out of range (1-8) if read in the 
%    wrong byte order. Max value = 8 in current standard

fseek(fid,3224,'bof');
if(fread(fid, 1, 'int16', 0, 'ieee-le') <255)
    machinefmt = 'ieee-le';
end
    
fclose(fid);

