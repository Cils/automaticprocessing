function varargout = SegyInspector(varargin)
% SEGYINSPECTOR M-file for SegyInspector.fig
%      SEGYINSPECTOR, by itself, creates a new SEGYINSPECTOR or raises the existing
%      singleton*.
%
%      H = SEGYINSPECTOR returns the handle to a new SEGYINSPECTOR or the handle to
%      the existing singleton*.
%
%      SEGYINSPECTOR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SEGYINSPECTOR.M with the given input arguments.
%
%      SEGYINSPECTOR('Property','Value',...) creates a new SEGYINSPECTOR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SegyInspector_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SegyInspector_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SegyInspector

% Last Modified by GUIDE v2.5 24-Jul-2009 09:33:55

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SegyInspector_OpeningFcn, ...
                   'gui_OutputFcn',  @SegyInspector_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SegyInspector is made visible.
function SegyInspector_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SegyInspector (see VARARGIN)

% Choose default command line output for SegyInspector
handles.output = hObject;

% Add segy file structure to guidata
handles.segy=[];
handles.matbinfmt='';     %matlab machine format to use with fopen
handles.txtfmt='';        %textual header is ascii or ebcdic
handles.fmtcode=0;        %segy format code (integer 1-8)
handles.fname='';         %last file that was chosen by user
handles.pname='';         %last path that was chosen by user

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes SegyInspector wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = SegyInspector_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function File_Callback(hObject, eventdata, handles)
% hObject    handle to File (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function FileOpen_Callback(hObject, eventdata, handles)
% hObject    handle to FileOpen (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Use UIGETFILE to select data file
[handles.fname, handles.pname] = uigetfile( ...
    {'*.sgy;*.segy;','SEGY Files (*.sgy,*.segy)';
    '*.*','All Files (*.*)'}, ...
    'Select SEG-Y data file',handles.pname);
% If "Cancel" is selected then return
if isequal([handles.fname,handles.pname],[0,0])
    return
    % Otherwise construct the fullfilename and Check and load the file.
else
    %Enable the radio buttons
    set(handles.ieeele_rb,'Enable','on');
    set(handles.ieeebe_rb,'Enable','on');
    set(handles.ascii_rb,'Enable','on');
    set(handles.ebcdic_rb,'Enable','on');
    
    %Get file name from user
    File = fullfile(handles.pname,handles.fname);
    set(handles.filename_txtb,'String',File) %display full filename
    
    %Analyze segy and open file for reading
    [handles.matbinfmt,handles.txtfmt] = SEGY_AnalyzeFile(File);
    handles.segy = SEGY_OpenFile(File,'r',handles.matbinfmt,handles.txtfmt);

    %Set radio button values
    switch lower(handles.matbinfmt)
        case{'ieee-le'}
            set(handles.ieeele_rb,'Value',1.0);
        case{'ieee-be'}
            set(handles.ieeebe_rb,'Value',1.0);
        otherwise
    end
    switch lower(handles.txtfmt)
        case{'ascii'}
            set(handles.ascii_rb,'Value',1.0);
        case{'ebcdic'}
            set(handles.ebcdic_rb,'Value',1.0);
        otherwise
    end
    
    %Display Text and Binary headers and save data
    display_headers(hObject, handles)
    guidata(hObject, handles);   
end


% --------------------------------------------------------------------
function FileClose_Callback(hObject, eventdata, handles)
% hObject    handle to FileClose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%FileClose file
SEGY_ReleaseFile(handles.segy);
set(handles.filename_txtb,'String','')
%Disable radio buttons
set(handles.ieeele_rb,'Value',0.0);      
set(handles.ieeebe_rb,'Value',0.0);
set(handles.ascii_rb,'Value',0.0);
set(handles.ebcdic_rb,'Value',0.0);
set(handles.ieeele_rb,'Enable','off');
set(handles.ieeebe_rb,'Enable','off');
set(handles.ascii_rb,'Enable','off');
set(handles.ebcdic_rb,'Enable','off');
%Clear displayed header values
set(handles.bheadvalue_lb,'String','');
set(handles.bheadinterp_lb,'String','');
set(handles.texthdr_lb,'String','');

% --------------------------------------------------------------------
function FileExit_Callback(hObject, eventdata, handles)
% hObject    handle to FileExit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fclose('all');
close all hidden

% --- Executes on button press in ieeele_rb.
function ieeele_rb_Callback(hObject, eventdata, handles)
% hObject    handle to ieeele_rb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ieeele_rb
%Re-read file
SEGY_ReleaseFile(handles.segy);
File = get (handles.filename_txtb,'String');
handles.segy = SEGY_OpenFile(File,'r','ieee-le',handles.txtfmt);
display_headers(hObject, handles)


% --- Executes on button press in ieeele_rb.
function ieeebe_rb_Callback(hObject, eventdata, handles)
% hObject    handle to ieeele_rb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ieeele_rb
%Re-read file
SEGY_ReleaseFile(handles.segy)
File = get (handles.filename_txtb,'String');
handles.segy = SEGY_OpenFile(File,'r','ieee-be',handles.txtfmt);
display_headers(hObject, handles)

% --- Executes on button press in ebcdic_rb.
function ebcdic_rb_Callback(hObject, eventdata, handles)
% hObject    handle to ebcdic_rb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ebcdic_rb

% Read file header from disk assuming it's EBCDIC
handles.txtfmt = 'ebcdic';
handles.segy.thead = SEGY_ReadTextHeader(handles.segy,handles.txtfmt);
set(handles.texthdr_lb,'String',reshape(handles.segy.thead,80,40)');

% --- Executes on button press in ascii_rb.
function ascii_rb_Callback(hObject, eventdata, handles)
% hObject    handle to ascii_rb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ascii_rb

% Read file header from disk assuming it's ASCII
handles.txtfmt = 'ascii';
handles.segy.thead = SEGY_ReadTextHeader(handles.segy,handles.txtfmt);
set(handles.texthdr_lb,'String',reshape(handles.segy.thead,80,40)');

% ---Displays text and binary header data
function display_headers(hObject, handles)
set(handles.bheadvalue_lb,'String',struct2cell(handles.segy.bhead));
set(handles.bheadinterp_lb,'String',SEGY_InterpretBinaryHeader(handles.segy));
set(handles.texthdr_lb,'String',reshape(handles.segy.thead,80,40)');

% --- Executes on selection change in bheadvalue_lb.
function bheadvalue_lb_Callback(hObject, eventdata, handles)
% hObject    handle to bheadvalue_lb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns bheadvalue_lb contents as cell array
%        contents{get(hObject,'Value')} returns selected item from bheadvalue_lb
index = get(handles.bheadvalue_lb,'Value');
set(handles.bheaddesc_lb,'Value',index)
set(handles.bheadinterp_lb,'Value',index)

% --- Executes on selection change in bheaddesc_lb.
function bheaddesc_lb_Callback(hObject, eventdata, handles)
% hObject    handle to bheaddesc_lb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns bheaddesc_lb contents as cell array
%        contents{get(hObject,'Value')} returns selected item from bheaddesc_lb
index = get(handles.bheaddesc_lb,'Value');
set(handles.bheadvalue_lb,'Value',index)
set(handles.bheadinterp_lb,'Value',index)

% --- Executes on selection change in bheadinterp_lb.
function bheadinterp_lb_Callback(hObject, eventdata, handles)
% hObject    handle to bheadinterp_lb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns bheadinterp_lb contents as cell array
%        contents{get(hObject,'Value')} returns selected item from bheadinterp_lb
index = get(handles.bheadinterp_lb,'Value')
set(handles.bheadvalue_lb,'Value',index)
set(handles.bheaddesc_lb,'Value',index)


% --------------------------------------------------------------------
function View_Callback(hObject, eventdata, handles)
% hObject    handle to View (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
