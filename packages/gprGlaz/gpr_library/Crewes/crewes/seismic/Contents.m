% CREWES Seismic toolbox
% Seismic processing tools 
%
%
% Transforms, Spectra
%  FFTRL: forward Fourier transform for real vectors.
%  IFFTRL: inverse Fourier transform for real time series
%  IFKTRAN: inverse fk transform
%  FKTRAN_MC: forward fk transform using less memory than FKTRAN
%  FKTRAN: Forward fk transform
%  IFKTRAN_MC: memory conserving inverse fk transform
%  BURG: COmpute the 1D Burg spectrum (maximum entropy)
%
% Filters, convolution
%  CONVM: convolution followed by truncation for min phase filters
%  CONVZ: convolution then truncation for non-min phase filters
%  FILTF: apply a bandass filter to a trace
%  FILTSPEC: designs the filter spectrum for FILTF
%  SECTCONV: runs convz on each trace in a matrix
%  SECTFILT: runs FILTF on each trace in a section
%  TVSW: time variant spectral whitening
%
% Amplitude adjustment
%  AEC: automatic envelope correction, a better AGC.
%  BALANS: match the rms power of one trace to another
%  CLIP: clips the amplitudes on a trace
%
% Q attenuation
%  QMATIRX: (in the syntraces toolbox) compute a matric to apply Q to a trace.
%  EINAR: (in SYNTRACES) compute a Q impulse response wavelet
%  SPRAT: Estimate Q by the spectral ratio method
%
% Interpolation, resampling
%  RESAMP: resample a signal using sinc function interpolation
%  SINC: sinc function evaluation
%  SINCI: sinc function interpolation for time series without nan's
%  INTERPSINC: identical to SINCI except for the order of the input arguments.
%  SINCINAN: sinc function interpolation for signals with embedded nan's 
%  SINQUE: sinc function evaluation
%  SECTRESAMP: runs resamp on each trace in a seismic section
%
% Attributes
%  PICKER: make picks in a seismic matrix
%  IPICK: interactive interface to PICKER
%  FIND_ZERO_CROSSINGS: as the name says
%  INS_PHASE: Compute the instantaneous phase useing complex trace theory
%  INS_AMP: Compute the magnitude of the complex trace.
%  INS_FREQ: Compute the instantaneous frequency useing complex trace theory
%
% Deconvolution, inversion
%  LEVREC: solve Tx=b using Levinson's recursion
%  DECONF: frequency domain stationary spiking deconvolution
%  DECONW: time domain stationary spiking decon (Wiener)
%  DECONB: time domain stationary spiking decon using Burg spectra
%  (See also gabordecon in the gabor_decon toolbox)
%
% Utilities
%  CONSTPHASE: estimate constant phase rotation between two signals by lsq
%  CONSTPHASE2: estimate cons. phs. rotation by systematic search over angles
%  TVCONSTPHASE: time variant version of constphase
%  TODB: convert to decibels
%  TOMIN: compute the minimum phase equivalent of a signal or wavelet
%  TOINV: compute the causal (min. phs.) inverse of a signal
%  TOINVF: compute freq. domain non-causal inverse of a signal
%  TOALL: compute the all pass equivalent of a signal
%  TOZERO: compute the zero phase equivalent of a signal
%  
% Auto and cross correlation
%  AUTO: single-sided autocorrelation
%  AUTO2: returns the two-sided autocorrelation 
%  MAXCORR: given two signals, find max crosscorrelation coef. and its lag
%
% Traveltime adjustment
%  STAT: static shift a trace

