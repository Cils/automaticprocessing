function phi=constphase(s1,s2)
%
% phi=constphase(s1,s2)
%
% Given two signals s1 and s2, this function finds the constant phase
% rotation which, when applied to s1, minimizes the L2 norm of the
% difference: s2-phase_rotated(s1) .
%
% s1 ... input time series to be rotated
% s2 ... input time series to match to rotated s1
% phi ... best constant phase angle in degrees. To rotate s1 to look like
%           s2, use s1r=phsrot(s1,phi)
%

%compute hilbert transform of s1
s1p=imag(hilbert(s1));

%loop from -180 to 180 in 10 degree increments
angles=-180:10:180;
err=zeros(size(angles));
for k=1:length(angles)
    theta=angles(k)*pi/180;
    %s1r=cos(theta)*s1-sin(theta)*s1p;
    s1r=phsrot(s1,angles(k));
    err(k)=norm(s2-s1r);
end
ind=find(err==min(err));
if(ind>1 & ind<length(angles))
    ang=angles(ind-1):1:angles(ind+1);
else
    ang=[-170:-1:-179 180:-1:170];
end
err=zeros(size(ang));
for k=1:length(ang)
    theta=ang(k)*pi/180;
    %s1r=cos(theta)*s1-sin(theta)*s1p;
    s1r=phsrot(s1,ang(k));
    err(k)=norm(s2-s1r);
end
ind=find(err==min(err));
phi=ang(ind);
x=1;