function [ix1,iz1,nxr,ix1m,iz1m,pvel,svel,density,nx,nxf,nxplot,nz,nzplot,.....
    Rho,Lp2m,Mu,LKrat,LKratF,vdtdx,vsdtdx].....
    = fdInitPad(pvel,svel,density,nx,nxplot,nz,nzplot,Dt,Dxz,iRrigid)
%Allow for padding around model borders to reduce edge effects
%
% P.M. Manning, Oct 2008
%
% NOTE: It is illegal for you to use this software for a purpose other
% than non-profit education or research UNLESS you are employed by a CREWES
% Project sponsor. By using this software, you are agreeing to the terms
% detailed in this software's Matlab source file.

% BEGIN TERMS OF USE LICENSE
%
% This SOFTWARE is maintained by the CREWES Project at the Department
% of Geology and Geophysics of the University of Calgary, Calgary,
% Alberta, Canada.  The copyright and ownership is jointly held by
% its author (identified above) and the CREWES Project.  The CREWES
% project may be contacted via email at:  crewesinfo@crewes.org
%
% The term 'SOFTWARE' refers to the Matlab source code, translations to
% any other computer language, or object code
%
% Terms of use of this SOFTWARE
%
% 1) Use of this SOFTWARE by any for-profit commercial organization is
%    expressly forbidden unless said organization is a CREWES Project
%    Sponsor.
%
% 2) A CREWES Project sponsor may use this SOFTWARE under the terms of the
%    CREWES Project Sponsorship agreement.
%
% 3) A student or employee of a non-profit educational institution may
%    use this SOFTWARE subject to the following terms and conditions:
%    - this SOFTWARE is for teaching or research purposes only.
%    - this SOFTWARE may be distributed to other students or researchers
%      provided that these license terms are included.
%    - reselling the SOFTWARE, or including it or any portion of it, in any
%      software that will be resold is expressly forbidden.
%    - transfering the SOFTWARE in any form to a commercial firm or any
%      other for-profit organization is expressly forbidden.
%
% END TERMS OF USE LICENSE

ix1 = 6;    %1;    %9;            %Note new start of model at ix1, iz1
iz1 = 2;    %6;
nxr = 1;    %Number of extra columns on right
%nxf = nx+nxr;
ix1m = ix1-1;
iz1m = iz1-1;
%Shift right
pvel(ix1:nx+ix1m,:) = pvel(1:nx,:);         %Shifting - this works
svel(ix1:nx+ix1m,:) = svel(1:nx,:);
density(ix1:nx+ix1m,:) = density(1:nx,:);
%Pad left
xpvect = pvel(1,:);
xsvect = svel(1,:);
xdvect = density(1,:);
zvect = ones(ix1m,1);
pvel(1:ix1m,:) = zvect*xpvect;
svel(1:ix1m,:) = zvect*xsvect;
density(1:ix1m,:) = zvect*xdvect;
nx = nx+ix1m;

%Pad right
zvect = ones(nxr,1);
xpvect = pvel(nx,:);
xsvect = svel(nx,:);
xdvect = density(nx,:);
pvel(nx+1:nx+nxr,:) = zvect*xpvect;
svel(nx+1:nx+nxr,:) = zvect*xsvect;
density(nx+1:nx+nxr,:) = zvect*xdvect;
nxf = nx+nxr;

%Shift down
pvel(:,iz1:nz+iz1m) = pvel(:,1:nz);
svel(:,iz1:nz+iz1m) = svel(:,1:nz);
density(:,iz1:nz+iz1m) = density(:,1:nz);
%Pad top
xvect = ones(1,iz1m);
zpvect = pvel(:,1);
zsvect = svel(:,1);
zdvect = density(:,1);
pvel(:,1:iz1m) = zpvect*xvect;
svel(:,1:iz1m) = zsvect*xvect;
density(:,1:iz1m) = zdvect*xvect;
%disp(size(zpvect));disp(size(xvect))
%disp(size(density))

nz = nz+iz1m;
nxplot = nxplot+ix1m;
nzplot = nzplot+iz1m;
% nzm = nz-1;
% if iBrigid<=0
%     nzm = nz;
% end
%nzm2 = nz-2;

%Basic velocity and density values
Rho = density;
%disp(size(pvel));disp(size(density));
Lp2m = density.*pvel.^2;
Mu = density.*svel.^2;

LKrat = (Lp2m-2*Mu)./Lp2m;
LKratF = LKrat(:,1);
%if ibrigid<=0
    Lp2m(:,nz+1) = Lp2m(:,nz);
    Mu(:,nz+1) = Mu(:,nz);
    %density(:,nz+1) = density(:,nz);
    LKrat(:,nz+1) = LKrat(:,nz);
    vdtdx = pvel(round(nx/2),nz)*Dt/Dxz;
    vsdtdx = svel(round(nx/2),nz)*Dt/Dxz;
%end
if iRrigid<=0
    vdtdx = pvel(nxf,round(nz/2))*Dt/Dxz;
    vsdtdx = svel(nxf,round(nz/2))*Dt/Dxz;
end
