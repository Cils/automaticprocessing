%fdModDown - Geological model building by outlining the tops of formations
    %Script ****Down**** version
%Lay out the Tops matrix as follows - each formation described with one line
    %whose parameters apply everywhere below it (until the next line is added)
    %Below each succesive line, its parameters replace those from above
    %The matrix is a rectangle filled out with zeros if necessary
%
% P.M. Manning, Oct 2008
%
% NOTE: It is illegal for you to use this software for a purpose other
% than non-profit education or research UNLESS you are employed by a CREWES
% Project sponsor. By using this software, you are agreeing to the terms
% detailed in this software's Matlab source file.

% BEGIN TERMS OF USE LICENSE
%
% This SOFTWARE is maintained by the CREWES Project at the Department
% of Geology and Geophysics of the University of Calgary, Calgary,
% Alberta, Canada.  The copyright and ownership is jointly held by
% its author (identified above) and the CREWES Project.  The CREWES
% project may be contacted via email at:  crewesinfo@crewes.org
%
% The term 'SOFTWARE' refers to the Matlab source code, translations to
% any other computer language, or object code
%
% Terms of use of this SOFTWARE
%
% 1) Use of this SOFTWARE by any for-profit commercial organization is
%    expressly forbidden unless said organization is a CREWES Project
%    Sponsor.
%
% 2) A CREWES Project sponsor may use this SOFTWARE under the terms of the
%    CREWES Project Sponsorship agreement.
%
% 3) A student or employee of a non-profit educational institution may
%    use this SOFTWARE subject to the following terms and conditions:
%    - this SOFTWARE is for teaching or research purposes only.
%    - this SOFTWARE may be distributed to other students or researchers
%      provided that these license terms are included.
%    - reselling the SOFTWARE, or including it or any portion of it, in any
%      software that will be resold is expressly forbidden.
%    - transfering the SOFTWARE in any form to a commercial firm or any
%      other for-profit organization is expressly forbidden.
%
% END TERMS OF USE LICENSE

clear

First= [ 1500,  750,  2.0];
%          Vp,   Vs,  rho,   X1,  Z1,  X2,  Z2,
Tops = [ 1700,  850,  2.2,    0,  35,   0,   0;....
         2000, 1000,  2.4,    0,  75,   0,   0;....
         2500, 1200,  2.7,    0, 100,   0,   0;....
         3000, 1400,  2.9,    0, 200, 400,  20;....
         3500, 1600,  2.9,    0, 250, 400,  70];

xMinG = -100; xMaxG = 1000;     %Left and right limits to which the model will be extendsd
modName = 'Down bed cutoff';    %A plot title
UpDown = 'Down';                %Code defining type of model

fdModUD(First,Tops,xMinG,xMaxG,modName,UpDown)
