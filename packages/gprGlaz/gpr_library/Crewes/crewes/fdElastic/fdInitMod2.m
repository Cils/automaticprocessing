function [pvel,svel,density,oldFile] = fdInitMod2(oldFile,Dxz,nx,nz,xMin)
%function [pvel,svel,density,oldFile] = fdInitMod2(oldFile,Dxz,nx,nz,xMin)
%Get the geological model from disc - fill in f-d grid
%
% P.M. Manning, Oct 2008
%
% NOTE: It is illegal for you to use this software for a purpose other
% than non-profit education or research UNLESS you are employed by a CREWES
% Project sponsor. By using this software, you are agreeing to the terms
% detailed in this software's Matlab source file.

% BEGIN TERMS OF USE LICENSE
%
% This SOFTWARE is maintained by the CREWES Project at the Department
% of Geology and Geophysics of the University of Calgary, Calgary,
% Alberta, Canada.  The copyright and ownership is jointly held by
% its author (identified above) and the CREWES Project.  The CREWES
% project may be contacted via email at:  crewesinfo@crewes.org
%
% The term 'SOFTWARE' refers to the Matlab source code, translations to
% any other computer language, or object code
%
% Terms of use of this SOFTWARE
%
% 1) Use of this SOFTWARE by any for-profit commercial organization is
%    expressly forbidden unless said organization is a CREWES Project
%    Sponsor.
%
% 2) A CREWES Project sponsor may use this SOFTWARE under the terms of the
%    CREWES Project Sponsorship agreement.
%
% 3) A student or employee of a non-profit educational institution may
%    use this SOFTWARE subject to the following terms and conditions:
%    - this SOFTWARE is for teaching or research purposes only.
%    - this SOFTWARE may be distributed to other students or researchers
%      provided that these license terms are included.
%    - reselling the SOFTWARE, or including it or any portion of it, in any
%      software that will be resold is expressly forbidden.
%    - transfering the SOFTWARE in any form to a commercial firm or any
%      other for-profit organization is expressly forbidden.
%
% END TERMS OF USE LICENSE

if isempty(oldFile)
    disp('The geology definition files for F-D input are ')
    ls *.gfd
    [oldFile] = fdLoadModel;
end
disp(oldFile)
load(oldFile,'-mat')
%disp(size(Xtops))
%disp(Xtops(1,:))
%disp(Ztops(1,:))
hold off
plot(Xtops',Ztops')
axis ij
title(modName)
%figure
pvel = ones(nx,nz)*First(1);
svel = ones(nx,nz)*First(2);
density = ones(nx,nz)*First(3);
Wons = ones(1,nz);
[nTops,nEntMax] = size(Xtops);
for iT=1:nTops
    xT = xMin;
    for ix=1:nx
        ig = find(Xtops(iT,:)>xT);
        zTp = Ztops(iT,ig(1))+(xT-Xtops(iT,ig(1)))*(Ztops(iT,ig(1))-Ztops(iT,ig(1)-1)).....
            /(Xtops(iT,ig(1))-Xtops(iT,ig(1)-1));
        iTz = fix(zTp/Dxz);
        %disp(iTz)
        %stop
        if strcmpi(UpDown,'Up')
            if iTz>1
                if iTz>nz; iTz = nz; end
                pvel(ix,1:iTz) = Vp(iT)*Wons(1:iTz);

                svel(ix,1:iTz) = Vs(iT)*Wons(1:iTz);
                density(ix,1:iTz) = rho(iT)*Wons(1:iTz);
            end
        else
            iTz = iTz+1;
            if iTz<nz
                if iTz<1; iTz = 1; end
                pvel(ix,iTz:nz) = Vp(iT)*Wons(iTz:nz);

                svel(ix,iTz:nz) = Vs(iT)*Wons(iTz:nz);
                density(ix,iTz:nz) = rho(iT)*Wons(iTz:nz);
            end
        end
        xT = xT+Dxz;
    end
end
%disp('HERE')
