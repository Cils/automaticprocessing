function [M,Ux,Uxt0,Uz,Uzt0,isurf,surfUz,surfUx,jfr,gfdFile,...
        ix1,nxplot,iz1,nzplot,ixp,izp] = .....
    fdLayDoc(Dt,Dxz,nx,nz,nstep,nframes,shotDepth,xMin,wave,tw,....
        iBrigid,iLedge,iRrigid,wncvar,traceFile,traceZlevel,mvTif,mvTinit,...
        mvZtop,mvXmax,mvZmax,mvPlot,mvAmp,mvClip,initStep,actiF,...
        M,Ux,Uxt0,Uz,Uzt0,isurf,surfUz,surfUx,jfr,gfdFile)
% function [M,Ux,Uxt0,Uz,Uzt0,isurf,surfUz,surfUx,jfr,gfdFile,...
%         ix1,nxplot,iz1,nzplot,ixp,izp] = .....
%     fdLayDoc(Dt,Dxz,nx,nz,nstep,nframes,shotDepth,xMin,wave,tw,....
%         iBrigid,iLedge,iRrigid,wncvar,traceFile,traceZlevel,mvTif,mvTinit,...
%         mvZtop,mvXmax,mvZmax,mvPlot,mvAmp,mvClip,initStep,actiF,...
%         M,Ux,Uxt0,Uz,Uzt0,isurf,surfUz,surfUx,jfr,gfdFile)
%
%fdLayDoc is a finite-difference elastic modelling program in 2 dimensions
%These are the input variables
%Dt  .....  Time sample rate in seconds
%Dxz  ....  Spatial sample rate (assume metres)
%nx  .....  Number of spatial samples in the X direction
%nz  .....  Number of spatial samples in the Z direction
%nstep  ..  Number of time steps
%nframes    Number of movie frames (for a Matlab movie or one built from .tif files)
%shotDepth  Source depth in metres
%xMin ....  %Starting point in X within the disc model
%wave  ...  %A source wavelet in time.
%tw  .....  %The time vector of the source wavelet.
%
%iBrigid .  %Bottom boundary key: use 9 (for a rigid boundary).
%iLedge  .  %Left boundary key: use 3 (for a mirror surface).
%iRrigid .  %Right boundary key: use 0 for a tranmitting edge, >0 for rigid.
%wncvar  .  %Correction '.mat' file, use '' if no file to be used.
%traceFile  %Unique part of a trace file name, output in SEGY, '' for no file
%traceZlevel %Z-level where displacements are recorded, usually 0.
%mvTif ....  %Name of .tif file movie frames, use '' if not required.
%mvTinit ..  %Number of first movie frame to be recorded, usually 1.
%
%mvZtop ...  %Z-level at top of each movie frame, often 0.
%mvXmax  ..  %X-distance to right boundary of the movie frame, often = lengthX.
%mvZmax  ..  %Z-level at bottom of each movie frame, often = lengthZ.
%mvPlot  ..  %Format key for movie snapshot displays: 0 to 9.
                %0-Uz (displacement) in wiggle trace.
                %1-Ux (displacement) in wiggle trace.
                %2-colour, with the shade showing displacement direction.
                    %mvClip reduces high amplitudes as fraction of maximum.
                %3-p/t colour, with the shade distinguishing P and S energy:
                    %red/green shades showing compressional energy,
                    %blue/yellow shades showing twisted energy.
                    %mvClip reduces high amplitudes as fraction of maximum.
                %4-absolute amplitude controled version of mvPlot = 2.
                    %mvAmp translates displacement to colour intensity (100 to 300?)
                    %This plot is useful for movies because the scaling is consistent
                    % between frames.
                %9-Matlab quiver plot. Displacements shown as arrows.
                    %Default quiver scaling used.
%mvAmp  ...  %Applied only to mvPlot = 4, translates displacement to colour intensity.
                %(100 to 300?), result is clipped to 1 (most intense colours).
%mvClip  ..  %Multiplier applied in mvPlot = 2 or 3 (0 < mvClip <= 1).
                %Reduces amplitudes higher than mvClip to mvClip level.
%initStep .  %When starting = 1, will be set suitably when continuing
%actiF  ...  %Figure controller for plots
%
%The following variables define the state of the modelling computations
    %They are outputs, which can then be input to continue model calculations
%M  .......  %Matlab movie file, may be played 5 times with 'movie(M,5)'
%Ux .......  %X displacements in an X/Y grid
%Uxt0 .....  %X displacements in an X/Y grid, previous time step
%Uz .......  %Z displacements in an X/Y grid
%Uzt0 .....  %Z displacements in an X/Y grid, previous time step
%isurf ....  %The sample number in Z direction of surf arrays (1 for surface)
%surfUz ...  %Z displacements at the model surface - over X and time
%surfUx ...  %X displacements at the model surface - over X and time
%jfr  .....  %The index of the next frame (for movies)
%gfdFile ..  %The name of the .gfd file defining the geological model
%
%The following variables specify how to plot snapshots
%ix1 ......  %X co-ordinate of start of model in arrays
%nxplot ...  %X co-ordinate of end of model in arrays
%iz1 ......  %Z co-ordinate of start of model in arrays
%nzplot ...  %Z co-ordinate of end of model in arrays
%ixp  .....  %X index of a vertical dicontinuity
%izp  .....  %Z index of a vertical dicontinuity - to plot boundaries
%
%     textNo = num2str(nShot);
%
% P.M. Manning, Oct 2008
%
% NOTE: It is illegal for you to use this software for a purpose other
% than non-profit education or research UNLESS you are employed by a CREWES
% Project sponsor. By using this software, you are agreeing to the terms
% detailed in this software's Matlab source file.

% BEGIN TERMS OF USE LICENSE
%
% This SOFTWARE is maintained by the CREWES Project at the Department
% of Geology and Geophysics of the University of Calgary, Calgary,
% Alberta, Canada.  The copyright and ownership is jointly held by
% its author (identified above) and the CREWES Project.  The CREWES
% project may be contacted via email at:  crewesinfo@crewes.org
%
% The term 'SOFTWARE' refers to the Matlab source code, translations to
% any other computer language, or object code
%
% Terms of use of this SOFTWARE
%
% 1) Use of this SOFTWARE by any for-profit commercial organization is
%    expressly forbidden unless said organization is a CREWES Project
%    Sponsor.
%
% 2) A CREWES Project sponsor may use this SOFTWARE under the terms of the
%    CREWES Project Sponsorship agreement.
%
% 3) A student or employee of a non-profit educational institution may
%    use this SOFTWARE subject to the following terms and conditions:
%    - this SOFTWARE is for teaching or research purposes only.
%    - this SOFTWARE may be distributed to other students or researchers
%      provided that these license terms are included.
%    - reselling the SOFTWARE, or including it or any portion of it, in any
%      software that will be resold is expressly forbidden.
%    - transfering the SOFTWARE in any form to a commercial firm or any
%      other for-profit organization is expressly forbidden.
%
% END TERMS OF USE LICENSE


%global NTS,Tarr
%global NQS
%disp(NQS)
%Load bottom correction file if it is not empty (= '')
%clear wnc
%Initialize general conditions ******************************************
        [nxplot,initzp,nzplot,icorr,wncU,supp,small,wnc,mint,thrat] =.....
    fdInitGen(mvXmax,mvZtop,mvZmax,mvPlot,nstep,nframes,Dt,Dxz,wncvar);
%Get the geological model from disc *************************************
        [pvel,svel,density,gfdFile] = .....
    fdInitMod2(gfdFile,Dxz,nx,nz,xMin);
%Pad around borders and reset indicies **********************************
        [ix1,iz1,nxr,ix1m,iz1m,pvel,svel,density,nx,nxf,nxplot,nz,nzplot,.....
        Rho,Lp2m,Mu,LKrat,LKratF,vdtdx,vsdtdx] = .....
    fdInitPad(pvel,svel,density,nx,nxplot,nz,nzplot,Dt,Dxz,iRrigid);
%Do first time step initialization
if initStep==1
        %Set up arrays for recording (surface) traces *******************
            [isurf,surfUx,surfUz,jfr,surfT,surfS,surfX,surfZ] =.....
        fdInitTrace(traceFile,traceZlevel,Dxz,nxf,nstep,shotDepth);
        %Iinitilize fd time step state arrays ***************************
            [Ux,Uxt0,Uz,Uzt0] =.....
        fdInitArray1(nxf,nz);
end
%Set out space for working arrays ***************************************
        [Ux2,Uz2,DUxDx,DUxDz,DUzDx,DUzDz,D2UxDx,D2UxDz,.....
        D2UzDxz,D2UzDz,D2UzDx,D2UxDxz,h1,h1a,h2,h2a,h3,h3a,h4,h4a,h5,h5a] =.....
    fdInitArray(nxf,nz);
%Add space for 'mirror' boundary on left ********************************
        [ixm,ixm2,izm,izm2,izb,ivxi,ivxo,nExtra,iExtrai,iExtrao,iExtra,zFill] =.....
    fdInitBound(nz,iBrigid,nxf,ix1,wnc);
%Set up array where initializing forces will be applied *****************
        [ixz,ampfx,ampfz,ixx,forcex,nfxmax,ixxp,ixzp,forcez,nfzmax,izx,izz,izxp,izzp] =.....
    fdInitForce(shotDepth,Dxz,ix1,wave);

for nts=initStep:nstep
    %disp(size(Ux))
    NTS = nts;
  %Add source over time component
  if(nts<=nfxmax)
    Ux(ixx:ixxp,ixz:ixzp) = forcex(nts).*ampfx....		%displacement
	+Ux(ixx:ixxp,ixz:ixzp);				% +addition
  end
  if(nts<=nfzmax)
    Uz(izx:izxp,izz:izzp) = forcez(nts).*ampfz....		%displacement
	+Uz(izx:izxp,izz:izzp);				% +addition
  end
  iBrigid = 9;  %For now
  if iBrigid<=0
    nPr = 100; %510; %450;  %20000; %500;
    nComp = 200;
    UHx = 1:nComp;
%     if nts>nPr
%         %disp(Uz(1:24,izb-1));
%         UzH1 = Uz(UHx,izb-1);
%         UxH1 = Ux(UHx,izb-1);
%     end
    %[Ux(ix1:nx,izb),Uz(ix1:nx,izb)] = padbottom7(Ux,Uz,Uxt0,Uzt0,izb,ix1,nx,nts,vdtdx,vsdtdx,wncU);
    %[Ux(ix1:nx,izb),Uzv(ix1:nx)] = padbottom7(Ux,Uz,Uxt0,Uzt0,izb,ix1,nx,nts,vdtdx,vsdtdx,wncU);
    %[Uxv(ix1:nx),Uzv(ix1:nx),nPq,nTq] = padbottom7(Ux,Uz,Uxt0,Uzt0,izb,ix1,nx,nts,vdtdx,vsdtdx,wncU);
        Uzv(ix1+iExtrao) = Uzv(ix1+iExtrai);     %Symmetric edge
        Uxv(ix1+iExtrao) = Uxv(ix1+iExtrai-1);   %Symmetric edge
        Uzv(nx+iExtra+1) = zFill;               %Rigid edge
        Uxv(nx+iExtra+1) = zFill;               %Rigid edge
        %disp(size(wnc));
        %disp(size(Uzv));disp(size(UHx));
        %stop
        Uzh = conv(wnc,Uzv);                    %Filter
        Uxh = conv(wnc,Uxv);
        Uz(1:nx,izb) = Uzh(nExtra+1:nExtra+nx); %Shift into main array
        Ux(1:nx,izb) = Uxh(nExtra+1:nExtra+nx);
        Uz(ix1,izb) = Uz(ix1+1,izb);
    if nts>nPr
        %disp(Uz(1:24,izb-1));
        UzH2 = Uz(UHx,izb-1);
        %UxH2 = Ux(UHx,izb-1);
        UzvH = Uzv(UHx);
        %figure
        plot(UHx,UzvH,'b',UHx,UzH2,'g')
        %plot(UHx,UzH1,'b',UHx,UzH2,'g')
        %plot(UHx,UzH1,'b',UHx,UxH1,'g')
        %plot(UHx,UxH1,UHx,UxH2)
        grid on
        title(num2str(nts))
        disp([nPq nTq])
        pause %stop
        %clf
    end
    if NTS>400
        %disp(Uz(1:5,izb));disp(Uz(1:5,izb-1));
        %disp(Ux(5,80:izb));
        %disp(nz)
        %stop
    end
  end
  if iLedge>0
      Ux(ivxo,:) = -Ux(ivxi,:);         % Left edge
      Uz(ivxo,:) = Uz(ivxi+1,:);
  end     

  if (iLedge>1)&&(iz1>1)                 % Free surface
      for izf=iz1-1:-1:1
          Ux(ixm,izf) = Ux(ixm,izf+1)+Uz(ixm+1,izf+1)-Uz(ixm,izf+1);
          Uz(ixm+1,izf) = Uz(ixm+1,izf+1)+LKratF(ixm).*(Ux(ixm+1,izf)-Ux(ixm,izf));
          Uz(1,izf) = Uz(ix1*2-1,izf);
      end
  end
 
  %if iLedge>2
  if nxr>0
      ixr = nx+1;   %Use only for non corrected, one column (row)
      if iRrigid>0          %Should be >0 for this module (just pad bottom)
          Ux(ixr,:) = 0;
          Uz(ixr,:) = 0;
      else
        [Ux(ixr,1:nz),Uz(ixr,1:nz)] = padright6(Ux,Uz,Uxt0,Uzt0,ixr,iz1,nz,small,vdtdx,vsdtdx);
      end
%       if nts>200
%           ntss = nts;
%       end
  end
 
  Ux(nxf-3:nxf,1:4) = supp.*Ux(nxf-3:nxf,1:4);  %Squash data in corner
  Uz(nxf-3:nxf,1:4) = supp.*Uz(nxf-3:nxf,1:4);
 
  %Calculate derivatives of Ux with respect to x
  %disp(size(Ux));disp(size(Lp2m));disp(size(DUxDx));
  DUxDx(ixm+1,:) = (Ux(ixm+1,:)-Ux(ixm,:)).*Lp2m(ixm+1,:);
  D2UxDx(ixm2,:) = DUxDx(ixm2+1,:)-DUxDx(ixm2,:);
  %Calculate derivatives of Ux with respect to z
  DUxDz(:,izm+1) = (Ux(:,izm+1)-Ux(:,izm)).*Mu(:,izm+1);
  D2UxDz(:,izm) = DUxDz(:,izm+1)-DUxDz(:,izm);
  if iBrigid>0
    D2UxDz(:,nz) = 0;                             %Rigid basement
  end
  %Calculate derivatives of Uz with respect to z
  DUzDz(:,izm) = (Uz(:,izm+1)-Uz(:,izm)).*Lp2m(:,izm);
  if iBrigid>0
    D2UzDz(:,nz) = 0;                             %Rigid basement
  end
  D2UzDz(:,izm+1) = DUzDz(:,izm+1)-DUzDz(:,izm);
  %Calculate derivatives of Uz with respect to x
  DUzDx(ixm,:) = (Uz(ixm+1,:)-Uz(ixm,:)).*Mu(ixm,:);
  D2UzDx(ixm+1,:) = DUzDx(ixm+1,:)-DUzDx(ixm,:);

  %Calculate Ux cross terms using first derivatives
  D2UxDxz(ixm2,izm+1) = (DUxDz(ixm2,izm+1)-DUxDz(ixm2-1,izm+1)).....
      +(DUxDx(ixm2,izm+1)-DUxDx(ixm2,izm)).*LKrat(ixm2,izm+1);
  %Calculate Uz cross terms using first derivatives
  D2UzDxz(ixm,izm) = (DUzDx(ixm,izm+1)-DUzDx(ixm,izm)).....
      +(DUzDz(ixm+1,izm)-DUzDz(ixm,izm)).*LKrat(ixm,izm);
  D2UzDxz(ixm,nz) = (DUzDz(ixm+1,nz)-DUzDz(ixm,nz)).*LKrat(ixm,nz);
 
  if icorr>0
    h1(ixm2,izm2) = conv2(D2UxDx(ixm2,izm2),wncmat(:,:,1,1),'same');
    h1a(ixm2,izm2) = conv2(D2UxDx(ixm2,izm2),wncmat(:,:,1,2),'same');
    h3(ixm2,izm2) = conv2(D2UxDz(ixm2,izm2),wncmat(:,:,3,1),'same');
    h3a(ixm2,izm2) = conv2(D2UxDz(ixm2,izm2),wncmat(:,:,3,2),'same');
    h2(ixm2,izm2) = conv2(D2UzDxz(ixm2,izm2),wncmat(:,:,2,1),'same');
    h2a(ixm2,izm2) = conv2(D2UzDxz(ixm2,izm2),wncmat(:,:,2,2),'same');
    %spacex = h1+h3+h2;
    spacex = (h1+h3+h2).*mask1+(h1a+h3a+h2a).*mask2;
  else
    spacex = D2UxDx+D2UxDz+D2UzDxz;
  end
  Ux2(ixm2,izm2)=Ux(ixm2,izm2)*2-Uxt0(ixm2,izm2)+thrat*spacex(ixm2,izm2)./Rho(ixm2,izm2);
 
  if icorr>0
    h4(ixm2,izm2) = conv2(D2UzDz(ixm2,izm2),wncmat(:,:,4,1),'same');
    h4a(ixm2,izm2) = conv2(D2UzDz(ixm2,izm2),wncmat(:,:,4,2),'same');
    h5(ixm2,izm2) = conv2(D2UzDx(ixm2,izm2),wncmat(:,:,5,1),'same');
    h5a(ixm2,izm2) = conv2(D2UzDx(ixm2,izm2),wncmat(:,:,5,2),'same');
    h2(ixm2,izm2) = conv2(D2UxDxz(ixm2,izm2),wncmat(:,:,2,1),'same');
    h2a(ixm2,izm2) = conv2(D2UxDxz(ixm2,izm2),wncmat(:,:,2,2),'same');
    %spacez = h4+h5+h2;
    spacez = (h4+h5+h2).*mask1+(h4a+h5a+h2a).*mask2;
  else
    spacez = D2UzDz+D2UzDx+D2UxDxz;
  end
  Uz2(ixm2,izm2)=Uz(ixm2,izm2)*2-Uzt0(ixm2,izm2)+thrat*spacez(ixm2,izm2)./Rho(ixm2,izm2);

  %centrz(nts,:) = Dzt0(nxhaf,:);
  if isurf>0
    %surfx(:,nts) = Uxt0(:,1);
    %surfz(:,nts) = Uzt0(:,1);
    surfUx(:,nts) = Ux(:,isurf);
    surfUz(:,nts) = Uz(:,isurf);
    surfT(nts) = (nts-1)*Dt;
  end
  %nxxxx = nts;

  Uxt0 = Ux;
  Ux  = Ux2;
  Uzt0 = Uz;
  Uz  = Uz2;
  if(rem(nts-1,mint)==0)&&(nts>=mvTinit);
    jfr = jfr+1;
    plotPack(actiF,mvPlot,mvClip,mvAmp,Dxz,Ux,Uz,ix1,xMin,nxplot,iz1,nzplot)
    if (mvPlot>1)&&(mvPlot<5)
      set(gca,'xlim',[xMin (nxplot-ix1m)*Dxz+xMin],'ylim',[initzp*Dxz (nzplot-iz1m)*Dxz]);
    elseif mvPlot==0
      set(gca,'xlim',[0 nxplot-ix1m],'ylim',[initzp nzplot-iz1m]);
    end
    title(['Time =' int2str(nts)])
    ylabel('Depth')
    xlabel('Offset')
    if mvPlot>=0
        axis equal
        %axis off
    end
    M(:,jfr) = getframe;
    if ~isempty(mvTif)
      eval(['print -dtiff -r150 ' mvTif int2str(jfr) '.tif'])
      %Ajfr(jfr) = nts*Dt;
      %eval(['print -dtiff -r150 ' mvTif Ajfr(jfr) '.tif'])
    end
  end
%  nxxxx = nts;
end
%disp(xv(1:5))
hold on
[ixp,izp] = zbounds(actiF,pvel,ix1,iz1,Dxz,xMin);
%disp(ixp')
figure
%surface(pvel(1:2:200,1:2:150),jet)
%contourf(pvel(1:2:200,1:2:150),10)
mesh(pvel(1:4:200,1:4:150))
view(-80,30)
colormap jet
