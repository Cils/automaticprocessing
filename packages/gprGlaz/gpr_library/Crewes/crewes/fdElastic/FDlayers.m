%FDlayers.m - Generates a model with an emphasis on surface waves
%Finite-difference elastic modelling code
%Most of the coding is done in this script, with the main parameters in
%section 3
%Control is from a menu set up here
%
% P.M. Manning, Oct 2008
%
% NOTE: It is illegal for you to use this software for a purpose other
% than non-profit education or research UNLESS you are employed by a CREWES
% Project sponsor. By using this software, you are agreeing to the terms
% detailed in this software's Matlab source file.

% BEGIN TERMS OF USE LICENSE
%
% This SOFTWARE is maintained by the CREWES Project at the Department
% of Geology and Geophysics of the University of Calgary, Calgary,
% Alberta, Canada.  The copyright and ownership is jointly held by
% its author (identified above) and the CREWES Project.  The CREWES
% project may be contacted via email at:  crewesinfo@crewes.org
%
% The term 'SOFTWARE' refers to the Matlab source code, translations to
% any other computer language, or object code
%
% Terms of use of this SOFTWARE
%
% 1) Use of this SOFTWARE by any for-profit commercial organization is
%    expressly forbidden unless said organization is a CREWES Project
%    Sponsor.
%
% 2) A CREWES Project sponsor may use this SOFTWARE under the terms of the
%    CREWES Project Sponsorship agreement.
%
% 3) A student or employee of a non-profit educational institution may
%    use this SOFTWARE subject to the following terms and conditions:
%    - this SOFTWARE is for teaching or research purposes only.
%    - this SOFTWARE may be distributed to other students or researchers
%      provided that these license terms are included.
%    - reselling the SOFTWARE, or including it or any portion of it, in any
%      software that will be resold is expressly forbidden.
%    - transfering the SOFTWARE in any form to a commercial firm or any
%      other for-profit organization is expressly forbidden.
%
% END TERMS OF USE LICENSE

try Sa = saved;
catch saved = 'Yes';
end
if strcmpi(saved,'No')
    kOptn = menu('Choose computation','Save computations','Load prior',.....
        'Start afresh','Continue from previous','Plot last frame (K 5)',.....
        'Plot seismic (K 6)','Play movie','Output SEGY (K 8)');
else
    kO = menu('Choose computation','Load prior','Start afresh',.....
        'Continue from previous','Plot last frame (K 5)',.....
        'Plot seismic (K 6)','Play movie','Output SEGY (K 8)');
    kOptn = kO+1;
end
%Key code 1 - Save the model calculations to disc *****************************
if kOptn==1
    disp('The existing finite-difference computed models are ')
    ls *.fdc
    fdcFile = input('Type file name within single-quotes (.fdc will be appended)');
    %eval(['save ',fdcFile,'.fdc'])
    kOptn = 5;
    save([fdcFile,'.fdc'])
    saved = 'Yes';
end
%Key code 2 - Load previous model calculations ********************************
if kOptn==2;
    clear
    kOptn = 2;
    [oldFile] = fdLoadPrior;
    disp(oldFile)
    load(oldFile,'-mat')
    saved = 'Yes';
    %whos
end
%Key code 3 - Start model from time zero **************************************
if kOptn==3;
    clear
    kOptn = 3;
    %Main input parameters
        Dt = .0002; Dxz = 1;        %Sample rates
        %Dt = 2;                    %Test bad value
        lengthX = 400;              %Length of X axis in (metres)
        lengthZ = 200; %80; %100;              %Length of Z axis in (metres)
        initStep = 1;               %Always 1 at this point
        nstep = 351;%601;                %Number of time steps (of Dt)
        gfdFile = ''; %'downCutoff.gfd'; %'';   %Geological model ('' if choosing later)
        xMin = 200; %100; %50; %220; %15;
        nShot = 3; %2; %1;
    %Monitor display and/or tif files
        nframes = 100;      %Number of frames in the movie (a fraction of nstep)
    %Source  shotDepth in metres, frequency in Hz
        shotDepth = 4; centerFreq = 30;
    %Edges
        iLedge = 3;     %3 for a mirror surface left edge
        iBrigid = 9;    %0 for a transmitting bottom, >0 for rigid
        iRrigid = 0;    %0 for a transmitting right edge, >0 for rigid
    %Correction file (if available), '' if none
        wncvar = ''; %'wncUnity'

    %Various ASCII files are written to folder tracePath.
    %Their names are suffixed version of the traceFile variable.
    %See fdSEGY2.m, see the man pages to ascii2segy_t on how to add more.
    %Output in seismic trace format - .txt file, '' if none
    %suffix files.
        tracePath = 'c:\cygwin\tmp\a2s\';            % path to ASCII repository.
        traceFile = 'testF';

        traceZlevel = 0;            %In metres
    %Output .tif files for a movie, '' if none
        mvTif = ''; %'mvTest'; %'testT';		
        %Movie size restrictions (to save space)
            nx = round(lengthX/Dxz)+1;
            nz = round(lengthZ/Dxz)+1;
            mvTinit = 1;                                         %Index
            %mvXmax = lengthX; mvZtop = 0; mvZmax = lengthZ;  %In metres
            mvZtop = 0; %In metres - currently not used
            mvXmax = 200; mvZmax = 110;  %In metres    120
                if mvXmax>lengthX; mvXmax = lengthX; end
                if mvZmax>lengthZ; mvZmax = lengthZ; end
        %Movie display parameters
            mvPlot = 3;	%0-Uz; 1-Ux; 2-colour; 3-p/tcolour; 4-abs.amp;  9-quiver
            mvAmp = 300; %200; %350;     %(for mvPlot=4)
            mvClip = .25; %1; %.25; %.5; %.95;

    if Dt>.004
        error('Dt is coded in seconds, the maximum allowed is now .004')
    end
    [wave,tw]=ricker(Dt,centerFreq,.06);
    %[M,Ux,Uz,ix1,nxplot,iz1,nzplot,surfUz,surfUx] = fdLayDoc2.....
    Ux = [];Uxt0 = [];Uz = [];Uzt0 = [];isurf = [];surfUz = [];surfUx = [];jfr = [];
    actiF = figure;
    M = getframe;
        [M,Ux,Uxt0,Uz,Uzt0,isurf,surfUz,surfUx,jfr,gfdFile,...
        ix1,nxplot,iz1,nzplot,ixp,izp] = .....
    fdLayDoc(Dt,Dxz,nx,nz,nstep,nframes,shotDepth,xMin,wave,tw,....
        iBrigid,iLedge,iRrigid,wncvar,traceFile,traceZlevel,mvTif,mvTinit,...
        mvZtop,mvXmax,mvZmax,mvPlot,mvAmp,mvClip,initStep,actiF,...
        M,Ux,Uxt0,Uz,Uzt0,isurf,surfUz,surfUx,jfr,gfdFile);
    saved = 'No';
   
end
%Key code 4 - Continue modelling **********************************************
if kOptn==4;
    %Some carefully selected parameters may be changed here
    initStep = nstep+1;
    nstep = 800; %500; %1000;           %Update to new total number of time steps
            %gfdFile = 'downCutoff.gfd'; %This usually commented out
        [M,Ux,Uxt0,Uz,Uzt0,isurf,surfUz,surfUx,jfr,gfdFile,...
        ix1,nxplot,iz1,nzplot,ixp,izp] = .....
    fdLayDoc(Dt,Dxz,nx,nz,nstep,nframes,shotDepth,xMin,wave,tw,....
        iBrigid,iLedge,iRrigid,wncvar,traceFile,traceZlevel,mvTif,mvTinit,...
        mvZtop,mvXmax,mvZmax,mvPlot,mvAmp,mvClip,initStep,actiF,...
        M,Ux,Uxt0,Uz,Uzt0,isurf,surfUz,surfUx,jfr,gfdFile);
    saved = 'No';

end
%Key code 5 - Plot final snapshot *********************************************
if kOptn==5
    %Change plotting parameters here, for final frame
    mvPlot = 1; %4; %2; %Controls type of plot
    mvAmp = 300;    %Absolute amplitude for mvPlot = 4;
    h = figure;
    plotPack(h,mvPlot,mvClip,mvAmp,Dxz,Ux,Uz,ix1,xMin,nxplot,iz1,nzplot)
    hold on
    %Plot all the line segments separating cells with vertical Vp contrasts
    npl = length(ixp);
    for ipl = 1:npl
        xpl = ixp(ipl)*Dxz+xMin;
        zpl = (izp(ipl)-iz1)*Dxz+Dxz*0.5;
        plot([xpl-Dxz*0.5,xpl+Dxz*0.5],[zpl,zpl])
    end
    %print -depsc -r150 snapshot.eps
    xlabel('X co-ordinate')
    ylabel('Depth')
    %boldlines
    bigfont(gca,1.5,2,1)
    whitefig
    grid off
    %print -dtiff -r150 Dup350.tif %PTdown350.tif  %snapshot.tif
end
%Key code 6 - Plot traces *****************************************************
if kOptn==6
    component = 0; %Plots Uz (0) or Ux
    plotTraces(component,Dxz,Dt,nx,xMin,nstep,surfUx,surfUz)
    print -dtiff -r150 real800seis.tif
    xlabel('X-coordinate')
    ylabel('Time in ms')
    %boldlines
    bigfont(gca,1.5,2,1)
    whitefig
    grid off
end
%Key code 7 - Run Matlab movie ************************************************
if kOptn==7
    %figure
    nRepeats = 5;       %Number of times the movie is repeated, once loaded
    movie(M,nRepeats)
end
%Key code 8 - Prepare for SEGY output (intermediate text files) ***************
if kOptn==8
    %traceFile = 'testSEGY';
    %nShot = 3; %2; %1;
    fdSEGY2(tracePath,traceFile,Dt,Dxz,nShot,xMin,surfUx,surfUz,shotDepth)
end
