function fdModUD(First,Tops,xMinG,xMaxG,modName,UpDown)
%fdModUD(First,Tops,xMinG,xMaxG,modName,UpDown)
%Rearrange Tops into property vectors and into X and Z tops matrices
    % and save on disc
%Input parameters
    %First .... Gives Vp, Vs, rho for first layer
    %Tops ..... Matrix for all layers - Vp, Vs, rho, and X/Z coorinates
    %xMinG .... Start point for all layers
    %xMaxG .... End point for all layers
    %modName .. Model name for a title
    %UpDown ... 'Up' or 'Down' - the design of the model
%Ouput parameters (to disc)
    %First .... same
    %Vp ....... Vector of Vp's
    %Vs ....... Vector of Vs's
    %rho ...... Vector of densities
    %Xtops .... X matrix
    %Ztops .... Z matrix
    %nEnt ..... Vector - number of significant entries in matrix layers
    %modName .. same
    %UpDown ... same
%
% P.M. Manning, Oct 2008
%
% NOTE: It is illegal for you to use this software for a purpose other
% than non-profit education or research UNLESS you are employed by a CREWES
% Project sponsor. By using this software, you are agreeing to the terms
% detailed in this software's Matlab source file.

% BEGIN TERMS OF USE LICENSE
%
% This SOFTWARE is maintained by the CREWES Project at the Department
% of Geology and Geophysics of the University of Calgary, Calgary,
% Alberta, Canada.  The copyright and ownership is jointly held by
% its author (identified above) and the CREWES Project.  The CREWES
% project may be contacted via email at:  crewesinfo@crewes.org
%
% The term 'SOFTWARE' refers to the Matlab source code, translations to
% any other computer language, or object code
%
% Terms of use of this SOFTWARE
%
% 1) Use of this SOFTWARE by any for-profit commercial organization is
%    expressly forbidden unless said organization is a CREWES Project
%    Sponsor.
%
% 2) A CREWES Project sponsor may use this SOFTWARE under the terms of the
%    CREWES Project Sponsorship agreement.
%
% 3) A student or employee of a non-profit educational institution may
%    use this SOFTWARE subject to the following terms and conditions:
%    - this SOFTWARE is for teaching or research purposes only.
%    - this SOFTWARE may be distributed to other students or researchers
%      provided that these license terms are included.
%    - reselling the SOFTWARE, or including it or any portion of it, in any
%      software that will be resold is expressly forbidden.
%    - transfering the SOFTWARE in any form to a commercial firm or any
%      other for-profit organization is expressly forbidden.
%
% END TERMS OF USE LICENSE

[nTops,nEntMax] = size(Tops);
Vp = Tops(:,1);
Vs = Tops(:,2);
rho = Tops(:,3);
Xtops = Tops(:,4:2:nEntMax);
Ztops = Tops(:,5:2:nEntMax);
nEnt = zeros(nTops,1);
%Determine the number of definition points per horizon
for iT = 1:nTops
    nEnt(iT) = find(Ztops(iT,:),1,'last');
end
%disp(nEnt)
%Add extra points for calculation purposes
for iT = 1:nTops
    nE = nEnt(iT);
    %Add a point on left of each layer
    if Xtops(iT,1)>xMinG
        Xtops(iT,2:nE+1) = Xtops(iT,1:nE);
        Xtops(iT,1) = xMinG;
        Ztops(iT,2:nE+1) = Ztops(iT,1:nE);
        Ztops(iT,1) = Ztops(iT,2);
    end
    %Add a point on right of each layer
    nEnt(iT) = nE+1;
    nE = nEnt(iT);
    if Xtops(iT,nE)<xMaxG
        Xtops(iT,nE+1) = xMaxG;
        Ztops(iT,nE+1) = Ztops(iT,nE);
    end
    nEnt(iT) = nE+1;
end
%Fill out X & Z matrix rows to the same length
for iT = 1:nTops
    nE = nEnt(iT);
    if nE<nEntMax
        for ix = nE+1:nEntMax
            Xtops(iT,ix) = Xtops(iT,nE);
            Ztops(iT,ix) = Ztops(iT,nE);
        end
    end
end
disp(modName)   
%Plot for quality control
    plot(Xtops',Ztops')
    axis ij
    title(modName)
    xlabel('X co-ordinate')
    ylabel('Depth')
    %boldlines
    bigfont(gca,1.5,2,1)
    whitefig
    grid off
    %print -dtiff -r150 Gmodel.tif
%Prompt for a name - and save the model
disp('The existing geology definition files for F-D input are ')
ls *.gfd
gfdFile = input('Type file name within single-quotes (.gfd will be appended)');
%whos
save([gfdFile,'.gfd'],'First','Vp','Vs','rho','Xtops','Ztops','nEnt','modName','UpDown')

