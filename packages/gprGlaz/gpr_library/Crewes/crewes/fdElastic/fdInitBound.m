function [ixm,ixm2,izm,izm2,izb,ivxi,ivxo,nExtra,iExtrai,iExtrao,iExtra,zFill].....
    = fdInitBound(nz,iBrigid,nxf,ix1,wnc)
%Allow for displacements on the left which simulate a 'mirrored surface'
%
% P.M. Manning, Oct 2008
%
% NOTE: It is illegal for you to use this software for a purpose other
% than non-profit education or research UNLESS you are employed by a CREWES
% Project sponsor. By using this software, you are agreeing to the terms
% detailed in this software's Matlab source file.

% BEGIN TERMS OF USE LICENSE
%
% This SOFTWARE is maintained by the CREWES Project at the Department
% of Geology and Geophysics of the University of Calgary, Calgary,
% Alberta, Canada.  The copyright and ownership is jointly held by
% its author (identified above) and the CREWES Project.  The CREWES
% project may be contacted via email at:  crewesinfo@crewes.org
%
% The term 'SOFTWARE' refers to the Matlab source code, translations to
% any other computer language, or object code
%
% Terms of use of this SOFTWARE
%
% 1) Use of this SOFTWARE by any for-profit commercial organization is
%    expressly forbidden unless said organization is a CREWES Project
%    Sponsor.
%
% 2) A CREWES Project sponsor may use this SOFTWARE under the terms of the
%    CREWES Project Sponsorship agreement.
%
% 3) A student or employee of a non-profit educational institution may
%    use this SOFTWARE subject to the following terms and conditions:
%    - this SOFTWARE is for teaching or research purposes only.
%    - this SOFTWARE may be distributed to other students or researchers
%      provided that these license terms are included.
%    - reselling the SOFTWARE, or including it or any portion of it, in any
%      software that will be resold is expressly forbidden.
%    - transfering the SOFTWARE in any form to a commercial firm or any
%      other for-profit organization is expressly forbidden.
%
% END TERMS OF USE LICENSE

nzm = nz-1;
if iBrigid<=0
    nzm = nz;
end
ixm = 1:nxf-1;
ixm2 = 2:nxf-1;
izm = 1:nzm;
izm2 = 2:nzm;
izb = nz+1;
ivxi = ix1:ix1*2-2;     %Indicies to fill in symmetric displacements on left
ivxo = ix1-1:-1:1;

%Allow for the extra displacement points needed for correction filtering
%disp(wnc)
nExtra = (length(wnc)-1)/2;
iExtrai = 1:nExtra+1;
iExtrao = -1:-1:-(nExtra+1);
iExtra = 1:nExtra;
zFill = zeros(1,nExtra);
%Will be used as follows
    %Uz(ix1+iExtrao) = Uz(ix1+iExtrai);     %Symmetric edge
    %Ux(ix1+iExtrao) = Ux(ix1+iExtrai-1);   %Symmetric edge
    %Uz(nx+iExtra+1) = zFill;               %Rigid edge
    %Ux(nx+iExtra+1) = zFill;               %Rigid edge

