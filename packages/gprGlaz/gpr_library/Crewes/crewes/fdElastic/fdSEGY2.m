function fdSEGY2(tracePath,traceFile,Dt,Dxz,nShot,xMin,surfUx,surfUz,shotDepth)
%function fdSEGY2(tracePath,traceFile,Dt,Dxz,nShot,xMin,surfUx,surfUz,shotDepth)
%
%Build ascii files - pass to UNIX a2s (ascii2segy_t) for standard SEGY output.
%This function writes ASCII files, including a parameter file for a2s with
%a suffix .para; it will not execute ascii2segy_t.
%
% P.M. Manning, Oct 2008
%
% NOTE: It is illegal for you to use this software for a purpose other
% than non-profit education or research UNLESS you are employed by a CREWES
% Project sponsor. By using this software, you are agreeing to the terms
% detailed in this software's Matlab source file.

% BEGIN TERMS OF USE LICENSE
%
% This SOFTWARE is maintained by the CREWES Project at the Department
% of Geology and Geophysics of the University of Calgary, Calgary,
% Alberta, Canada.  The copyright and ownership is jointly held by
% its author (identified above) and the CREWES Project.  The CREWES
% project may be contacted via email at:  crewesinfo@crewes.org
%
% The term 'SOFTWARE' refers to the Matlab source code, translations to
% any other computer language, or object code
%
% Terms of use of this SOFTWARE
%
% 1) Use of this SOFTWARE by any for-profit commercial organization is
%    expressly forbidden unless said organization is a CREWES Project
%    Sponsor.
%
% 2) A CREWES Project sponsor may use this SOFTWARE under the terms of the
%    CREWES Project Sponsorship agreement.
%
% 3) A student or employee of a non-profit educational institution may
%    use this SOFTWARE subject to the following terms and conditions:
%    - this SOFTWARE is for teaching or research purposes only.
%    - this SOFTWARE may be distributed to other students or researchers
%      provided that these license terms are included.
%    - reselling the SOFTWARE, or including it or any portion of it, in any
%      software that will be resold is expressly forbidden.
%    - transfering the SOFTWARE in any form to a commercial firm or any
%      other for-profit organization is expressly forbidden.
%
% END TERMS OF USE LICENSE


%Create .Ux .Uz .GRP_X .GRP_Z files
    [nxf,nStep] = size(surfUx);
    horz = 10;
    Axs = ((1:nxf)-1)*Dxz*horz+xMin;
    AxsInt = round(Axs);
    Azs = zeros(1,nxf);
    textNo = num2str(nShot);
    eval(['save ' tracePath traceFile textNo '.Ux  surfUx -ascii'])  %*****
    eval(['save ' tracePath traceFile textNo '.Uz  surfUz -ascii'])
    %eval(['save ' tracePath traceFile textNo '.GRP_X  AxsInt -ascii'])
    eval(['save ' tracePath traceFile textNo '.GRP_Z  Azs -ascii'])
%Create .GRP_X file
    st = ['fid = fopen(''' tracePath traceFile textNo '.GRP_X'',' '''wt'');'];
    eval(st)
    fprintf(fid, '%6d',AxsInt);
    fclose(fid);
%Create .parm file
    st2 = ['fid = fopen(''' tracePath traceFile textNo '.para'',' '''wt'');'];
    %disp(st)
    eval(st2)
    fprintf (fid, ['ASCIIDataInfile ', traceFile, textNo ,'.Uz\n']);
    fprintf (fid, ['SEGYOutfile  ', traceFile, textNo ,'_Uz.sgy\n']);
    fprintf (fid, ['FileNameRoot ', traceFile, textNo,'\n']);
    fprintf (fid, ['TxtHdrFile ', traceFile, textNo ,'.c80\n']); % text header
    fprintf (fid, 'FMT\t\tR\n');
    fprintf (fid, 'GatherType\t5\n');
    fprintf (fid, 'NtrPerGather\t%d\n', nxf);
    fprintf (fid, 'Nsamp\t\t%d\n', nStep);
    DtInt = round(Dt*1000000);
    fprintf (fid, 'dt\t\t%d\n', DtInt);
    fprintf (fid, 'unit\t\t1\n');
    fprintf (fid, 'Scaling\t\t1\nvertMultiplier\t1\n');
    fprintf (fid, 'horzMultiplier\t-%d\n',horz);
    fprintf (fid, 'Stn1\t%d\n', 0);
    fprintf (fid, 'dStn1\t%d\n', 0);
    fprintf (fid, 'dStn\t%d\n', 0);
    fprintf (fid, 'X1\t%d\n', 0);
    fprintf (fid, 'dX1\t%d\n', 1);
    fprintf (fid, 'dx\t%d\n', Dxz);
    fprintf (fid, 'FFID\t%d\n', 0);
    fprintf (fid, 'dFFID\t%d\n', 0);
    fprintf (fid, 'SP\t%d\n', nShot);
    fprintf (fid, 'dSP\t%d\n', 0);
    fprintf (fid, 'SP_x\t%d\n', xMin);
    fprintf (fid, 'dSP_x\t%d\n', 0);
    fprintf (fid, 'THV_SRC_DEPTH\t%d\n', shotDepth);
    fprintf (fid, 'THV_SRC_X\t%d\n', xMin);
    fprintf (fid, 'THV_SRC_Z\t%d\n', 0);
    fprintf (fid, 'CDP\t%d\n', 0);
    fprintf (fid, 'dCDP\t%d\n', 0);
    fprintf (fid, 'CDP_x\t%d\n', 0);
    fprintf (fid, 'dCDP_x\t%d\n', 0);
    fclose(fid);
    %Create .c80 file (SEGY text header)
    %Build and output edcdic header,
    % directly from Matlab input parameter file
    fidin = fopen('FDlayers.m', 'r');
    st = ['fid = fopen(''' tracePath traceFile textNo '.c80'',' '''wt'');'];
    eval(st)
    str = date;
    fprintf (fid, '%s\n', str);
    for iCard = 1:37
        card = fgets(fidin,80);
        if iCard==1; disp(card); end
        %fprintf (fid, '%s80\n', card);
        fprintf (fid, '%s', card);
    end
    fclose(fidin);
    fclose(fid);
