function [Ux2,Uz2,DUxDx,DUxDz,DUzDx,DUzDz,D2UxDx,D2UxDz,.....
    D2UzDxz,D2UzDz,D2UzDx,D2UxDxz,h1,h1a,h2,h2a,h3,h3a,h4,h4a,h5,h5a].....
    = fdInitArray(nxf,nz)
%Declare space for working arrays
%
% P.M. Manning, Oct 2008
%
% NOTE: It is illegal for you to use this software for a purpose other
% than non-profit education or research UNLESS you are employed by a CREWES
% Project sponsor. By using this software, you are agreeing to the terms
% detailed in this software's Matlab source file.

% BEGIN TERMS OF USE LICENSE
%
% This SOFTWARE is maintained by the CREWES Project at the Department
% of Geology and Geophysics of the University of Calgary, Calgary,
% Alberta, Canada.  The copyright and ownership is jointly held by
% its author (identified above) and the CREWES Project.  The CREWES
% project may be contacted via email at:  crewesinfo@crewes.org
%
% The term 'SOFTWARE' refers to the Matlab source code, translations to
% any other computer language, or object code
%
% Terms of use of this SOFTWARE
%
% 1) Use of this SOFTWARE by any for-profit commercial organization is
%    expressly forbidden unless said organization is a CREWES Project
%    Sponsor.
%
% 2) A CREWES Project sponsor may use this SOFTWARE under the terms of the
%    CREWES Project Sponsorship agreement.
%
% 3) A student or employee of a non-profit educational institution may
%    use this SOFTWARE subject to the following terms and conditions:
%    - this SOFTWARE is for teaching or research purposes only.
%    - this SOFTWARE may be distributed to other students or researchers
%      provided that these license terms are included.
%    - reselling the SOFTWARE, or including it or any portion of it, in any
%      software that will be resold is expressly forbidden.
%    - transfering the SOFTWARE in any form to a commercial firm or any
%      other for-profit organization is expressly forbidden.
%
% END TERMS OF USE LICENSE

nzp = nz+1;
Ux2 = zeros(nxf,nzp);
Uz2 = zeros(nxf,nzp);
DUxDx = zeros(nxf,nzp);
DUxDz = zeros(nxf,nzp);
DUzDx = zeros(nxf,nzp);
DUzDz = zeros(nxf,nzp);
D2UxDx = zeros(nxf,nzp);
D2UxDz = zeros(nxf,nzp);
D2UzDxz = zeros(nxf,nzp);
D2UzDz = zeros(nxf,nzp);
D2UzDx = zeros(nxf,nzp);
D2UxDxz = zeros(nxf,nzp);
h1 = zeros(nxf,nzp);
h1a = zeros(nxf,nzp);
h2 = zeros(nxf,nzp);
h2a = zeros(nxf,nzp);
h3 = zeros(nxf,nzp);
h3a = zeros(nxf,nzp);
h4 = zeros(nxf,nzp);
h4a = zeros(nxf,nzp);
h5 = zeros(nxf,nzp);
h5a = zeros(nxf,nzp);
