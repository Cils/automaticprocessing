function [supp] = suppress
%Design sheet to suppress high amplitudes in the corner
nx = 4;
nz = 4;
supp = ones(nx,nz);
for ix=1:nx
    for iz=1:nz
        dist = sqrt((ix-nx)^2+(iz-1)^2);
        if dist<nx
            supp(ix,iz) = dist/nx;
        end
    end
end
disp('Use suppress in the UR corner')
