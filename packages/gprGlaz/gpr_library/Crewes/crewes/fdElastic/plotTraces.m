function plotTraces(component,Dxz,Dt,nx,xMin,nstep,surfUx,surfUz)
%function plotTraces(component,Dxz,Dt,nx,xMin,nstep,surfUx,surfUz)
%Translate f-d model parms for use with plotseis routine
%
% P.M. Manning, Oct 2008
%
% NOTE: It is illegal for you to use this software for a purpose other
% than non-profit education or research UNLESS you are employed by a CREWES
% Project sponsor. By using this software, you are agreeing to the terms
% detailed in this software's Matlab source file.

% BEGIN TERMS OF USE LICENSE
%
% This SOFTWARE is maintained by the CREWES Project at the Department
% of Geology and Geophysics of the University of Calgary, Calgary,
% Alberta, Canada.  The copyright and ownership is jointly held by
% its author (identified above) and the CREWES Project.  The CREWES
% project may be contacted via email at:  crewesinfo@crewes.org
%
% The term 'SOFTWARE' refers to the Matlab source code, translations to
% any other computer language, or object code
%
% Terms of use of this SOFTWARE
%
% 1) Use of this SOFTWARE by any for-profit commercial organization is
%    expressly forbidden unless said organization is a CREWES Project
%    Sponsor.
%
% 2) A CREWES Project sponsor may use this SOFTWARE under the terms of the
%    CREWES Project Sponsorship agreement.
%
% 3) A student or employee of a non-profit educational institution may
%    use this SOFTWARE subject to the following terms and conditions:
%    - this SOFTWARE is for teaching or research purposes only.
%    - this SOFTWARE may be distributed to other students or researchers
%      provided that these license terms are included.
%    - reselling the SOFTWARE, or including it or any portion of it, in any
%      software that will be resold is expressly forbidden.
%    - transfering the SOFTWARE in any form to a commercial firm or any
%      other for-profit organization is expressly forbidden.
%
% END TERMS OF USE LICENSE

td = Dt*1000;
intplx = round(nx/50);
figure
tSAx = td:td:nstep*td;
%xSAx = Dxz:intplx*Dxz:nx*Dxz;
xSAx = xMin:intplx*Dxz:nx*Dxz+xMin;
if component==0
    plotseis(surfUz(1:intplx:nx,:)',tSAx,xSAx,1,10,1,1,'k');
    title('Vertical data collected at surface')
else
    plotseis(surfUx(1:intplx:nx,:)',tSAx,xSAx,1,10,1,1,'k');
    title('Horizontal data collected at surface')
end
%plotseis(matout(:,1:intplx:nx-49,:),.2:.2:nts*.2,50:intplx:nx,1,2,1,'k')
xlabel('X-coordinate')
ylabel('Time in ms')
%boldlines
bigfont(gca,1.5,2,1)
whitefig
grid off
