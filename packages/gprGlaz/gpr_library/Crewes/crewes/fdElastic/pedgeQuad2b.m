function [Pplus,nelem,Pnew] = pedgeQuad2b(Pt,Pminus,Pleft,Pright,Pold,C)
%Project a value for Pplus that will make the boundary transmitting
% C = Dx/(vDt) generally >1
%global NTS ImArray
%global MPR ImArray
%sizeArr = size(Pt)
% P.M. Manning, Oct 2008
%
% NOTE: It is illegal for you to use this software for a purpose other
% than non-profit education or research UNLESS you are employed by a CREWES
% Project sponsor. By using this software, you are agreeing to the terms
% detailed in this software's Matlab source file.

% BEGIN TERMS OF USE LICENSE
%
% This SOFTWARE is maintained by the CREWES Project at the Department
% of Geology and Geophysics of the University of Calgary, Calgary,
% Alberta, Canada.  The copyright and ownership is jointly held by
% its author (identified above) and the CREWES Project.  The CREWES
% project may be contacted via email at:  crewesinfo@crewes.org
%
% The term 'SOFTWARE' refers to the Matlab source code, translations to
% any other computer language, or object code
%
% Terms of use of this SOFTWARE
%
% 1) Use of this SOFTWARE by any for-profit commercial organization is
%    expressly forbidden unless said organization is a CREWES Project
%    Sponsor.
%
% 2) A CREWES Project sponsor may use this SOFTWARE under the terms of the
%    CREWES Project Sponsorship agreement.
%
% 3) A student or employee of a non-profit educational institution may
%    use this SOFTWARE subject to the following terms and conditions:
%    - this SOFTWARE is for teaching or research purposes only.
%    - this SOFTWARE may be distributed to other students or researchers
%      provided that these license terms are included.
%    - reselling the SOFTWARE, or including it or any portion of it, in any
%      software that will be resold is expressly forbidden.
%    - transfering the SOFTWARE in any form to a commercial firm or any
%      other for-profit organization is expressly forbidden.
%
% END TERMS OF USE LICENSE

%stop
%clear
%who
global iTest
iTest = iTest+1;
%NTS = NTS+1;
%MPR = MPR+1;
%disp(MPR)
C2 = C.*C;
D = 2*C2.*(Pt-Pold)+Pminus+Pleft+Pright-4*Pt;
R2 = (Pleft-Pright).^2;
mom = 2*(Pt-Pold);
pSy = (4*Pt-(2*Pminus+Pright+Pleft));
halfB = (C2.*Pminus+D)./(C2-1);
halfDisc = halfB.^2-(C2.*Pminus.^2+C2.*R2-D.^2)./(C2-1);
if iTest==60
    halfDisc(50) = -1;
end
% ii = find((halfDisc<0)&(halfDisc>-0.000000000001));
% halfDisc(ii) = 0;
%disp(size(halfDisc))
ii = find(halfDisc'<0);
% if ~isempty(ii)
%     disp(ii)
% end
%sz = size(ii);
nelem = length(ii);
%ImArray = [ImArray sz(2)];
if ii~=0
    %disp('disp output here')
%     %disp(size(ImArray))
%     disp(size(ii))
%     disp(size(ImArray(MPR,:)))
%     disp(ii)
    %disp(MPR)
    %disp(iTest)
end
%ImArray(iTest,1:nelem) = ii(1:nelem,1)';
%if ~isempty(ii)
%    'imaginary'
%end
halfDisc(ii) = 0;
%Set up no-solution values
%     Pplus = halfB;
%     Pnew = (2*Pt-Pold)+(Pplus+Pminus+Pleft+Pright-4*Pt)./C2;
	Pmr = pSy/2-C2.*mom/2;
	Pnr = (mom-pSy./C2)/2;
    %Pmr = 0;
    %Pnr = mom+(Pmr-pSy)/C2;
    Pplus = Pminus+Pmr;
	Pnew = Pold+Pnr;
%ImArray(iTest,:) = Pplus';
%Solution values
PpOne = halfB-sqrt(halfDisc);
PpTwo = halfB+sqrt(halfDisc);
PnOne = (2*Pt-Pold)+(PpOne+Pminus+Pleft+Pright-4*Pt)./C2;
PnTwo = (2*Pt-Pold)+(PpTwo+Pminus+Pleft+Pright-4*Pt)./C2;
Select = (PpOne-Pminus).*(PnOne-Pold);
    ij = find(Select<0);
    Pplus(ij) = PpOne(ij);
    Pnew(ij) = PnOne(ij);
Select = (PpTwo-Pminus).*(PnTwo-Pold);
    ik = find(Select<0);
    Pplus(ik) = PpTwo(ik);
    Pnew(ik) = PnTwo(ik);
% if MPR >= 187
%     nnts = MPR;
% end
