function [Uxr,Uzr] = padright6(Ux,Uz,Ux0,Uz0,ixr,iz1,nz,small,vdtdx,vsdtdx)
%Pad right edge of model with a 'no boundary' boundary, one row (physical column) at a time
%function [Uxr,Uzr] = padright6(Ux,Uz,Ux0,Uz0,ixr,iz1,nz,small,vdtdx,vsdtdx)
%
% P.M. Manning, Oct 2008
%
% NOTE: It is illegal for you to use this software for a purpose other
% than non-profit education or research UNLESS you are employed by a CREWES
% Project sponsor. By using this software, you are agreeing to the terms
% detailed in this software's Matlab source file.

% BEGIN TERMS OF USE LICENSE
%
% This SOFTWARE is maintained by the CREWES Project at the Department
% of Geology and Geophysics of the University of Calgary, Calgary,
% Alberta, Canada.  The copyright and ownership is jointly held by
% its author (identified above) and the CREWES Project.  The CREWES
% project may be contacted via email at:  crewesinfo@crewes.org
%
% The term 'SOFTWARE' refers to the Matlab source code, translations to
% any other computer language, or object code
%
% Terms of use of this SOFTWARE
%
% 1) Use of this SOFTWARE by any for-profit commercial organization is
%    expressly forbidden unless said organization is a CREWES Project
%    Sponsor.
%
% 2) A CREWES Project sponsor may use this SOFTWARE under the terms of the
%    CREWES Project Sponsorship agreement.
%
% 3) A student or employee of a non-profit educational institution may
%    use this SOFTWARE subject to the following terms and conditions:
%    - this SOFTWARE is for teaching or research purposes only.
%    - this SOFTWARE may be distributed to other students or researchers
%      provided that these license terms are included.
%    - reselling the SOFTWARE, or including it or any portion of it, in any
%      software that will be resold is expressly forbidden.
%    - transfering the SOFTWARE in any form to a commercial firm or any
%      other for-profit organization is expressly forbidden.
%
% END TERMS OF USE LICENSE


%global NTS
%small = 0.0000001;   %0.00001;
fudge = 0.995;  %0.98;   %1.0;    %0.99 to .995 seems best
nxm = ixr-1;
nzm = nz-1;
%ivzm = 1:nzm;
%ivzm2 = 2:nzm;
ivzm = iz1:nzm;
ivzm2 = iz1+1:nzm;
%Calculate pressure
Pressm(ivzm) = (Ux(nxm,ivzm)-Ux(nxm-1,ivzm))+(Uz(nxm,ivzm+1)-Uz(nxm,ivzm));
Pressmm(ivzm) = (Ux(nxm-1,ivzm)-Ux(nxm-2,ivzm))+(Uz(nxm-1,ivzm+1)-Uz(nxm-1,ivzm));
Pressm(nz) = Pressm(nzm);
Pressm0(ivzm) = (Ux0(nxm,ivzm)-Ux0(nxm-1,ivzm))+(Uz0(nxm,ivzm+1)-Uz0(nxm,ivzm));
%Project pressure
% cosdenp(ivzm2) = Pressm(ivzm2)-Pressm0(ivzm2);
% costh = ones(1,nzm);
% ii = find(abs(cosdenp)>small);
% cosnump(ivzm2) = Pressm(ivzm2+1)-Pressm(ivzm2-1);
% costh(ii) = 0.5*vdtdx*cosnump(ii)./cosdenp(ii);
%   if max(abs(Uz(nxm,:)))>0.0001
%       nxxxx = nxm;
%   end
% ii = find(costh>1);
% costh(ii) = ones(1,length(ii));
% ii = find(costh<-1);
% costh(ii) = -ones(1,length(ii));
% sinth = sqrt(1-costh.^2);
% Press(ivzm) = Pressm(ivzm)-sinth(ivzm).*(Pressm(ivzm)-Pressm0(ivzm))/vdtdx;
%function Pplus = pedgeSolve(Pt,Pminus,Pleft,Pright,Pold,C)
Press(ivzm2) = pedgeQuad2b(Pressm(ivzm2),Pressmm(ivzm2),Pressm(ivzm2-1),.....
                Pressm(ivzm2+1),Pressm0(ivzm2),1/vdtdx);
%Calculate shearing
Shearm(ivzm) = (Uz(nxm,ivzm+1)-Uz(nxm-1,ivzm+1))-(Ux(nxm-1,ivzm+1)-Ux(nxm-1,ivzm));
Shearm(nz) = Shearm(nzm);
Shearm0(ivzm) = (Uz0(nxm,ivzm+1)-Uz0(nxm-1,ivzm+1))-(Ux0(nxm-1,ivzm+1)-Ux0(nxm-1,ivzm));
%Project shearing
cosdens(ivzm2) = Shearm(ivzm2)-Shearm0(ivzm2);
cosphi = ones(1,nzm);
ii = find(abs(cosdens)>small);
cosnums(ivzm2) = Shearm(ivzm2+1)-Shearm(ivzm2-1);
cosphi(ii) = 0.5*vsdtdx*cosnums(ii)./cosdens(ii);
ii = find(cosphi>1);
cosphi(ii) = ones(1,length(ii));
ii = find(cosphi<-1);
cosphi(ii) = -ones(1,length(ii));
sinphi = sqrt(1-cosphi.^2);
Shear(ivzm) = Shearm(ivzm)-sinphi(ivzm).*(Shearm(ivzm)-Shearm0(ivzm))/vsdtdx;
Uzr = zeros(1,nz);
Uxr = Uzr;
%Project Z displacement using shear
Uzr(ivzm+1) = Shear(ivzm)+Uz(nxm,ivzm+1)+Ux(nxm,ivzm+1)-Ux(nxm,ivzm);
Uxr(ivzm2) = Press(ivzm2)+Ux(nxm,ivzm2)-fudge*(Uzr(ivzm2+1)-Uzr(ivzm2));
  dum = Ux(1,1);
  if max(abs(Uz(nxm,:)))>0.0001
  %if max(abs(shear))>1   %0.1
  %if max(abs(comp))>0.001   %0.1
  %if max(Press2(:))>.0001
      nxxxx = nxm;
  end
