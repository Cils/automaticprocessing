%Test fdCREWES routines
FDlayers
    %fdLayDoc
%FDlayersTopo    %topoDrive2
                     %genArr2 colourColumn2 topoFreeSurf2 plotStGr
    %fdLayTopo
    %disp(size(M))
    %plot(M(:,99))
%fdModUp
%fdModDown
%F2delem2   %Contour spectrum of limited size spatial correction filters in 2D
%FDcentral  %Design optimum 1D first derivative central difference correction filter
%help quiver
% clear
