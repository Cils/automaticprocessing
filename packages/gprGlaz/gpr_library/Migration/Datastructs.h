/****************************************************************************************/
/*	COPYWRITE:		Urs Böniger				   02.09.98 	*/
/****************************************************************************************/
/*--------------------------------------------------------------------------------------*/
/*	Header-Files:	Bzag_dataform.h							*/
/*--------------------------------------------------------------------------------------*/
/*	Funktion:		Definition of ZAG - dataformat				*/
/*											*/
/*											*/
/*--------------------------------------------------------------------------------------*/
/*	Remarks:		---							*/
/*											*/
/*											*/
/****************************************************************************************/

#ifndef _ZAG_DATAFORM_H_
#define _ZAG_DATAFORM_H_

/*.........................................................................................................................*/
/*      defines bineray header												   */
/*.........................................................................................................................*/

typedef struct{
long  line_num, BB1, no_tr, no_pts, BB2, no_bin_x, no_bin_y, BB3, BB4, timezero_pt, no_stacks, BB5, BB6, BB7, BB8, BB9, BB10, no_bytes, length_binhead, length_tracehead;float sample_rate, step_x, step_y, x_fix, y_fix, z_fix, angle_fft, timewindow, step_size, frequency, antenna_sep, voltage;
} BINARY_HEADER;


/*.........................................................................................................................*/
/*      defines trace header and set pointer to trace samples					 										   */
/*.........................................................................................................................*/

typedef struct{
long  num_seq, num_x, num_y, TT1, no_pts, no_stacks, TT2, TT3, zero_flag, TT4, com_flag; float pos_x, pos_y, x, y, z, normx, normy, normz, timewindow, toposhift, timezero, timezero_adj, time_ms, time_day;
} TRACE_HEADER;


#endif
