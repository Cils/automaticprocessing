function [datam,FileHeaderm,TraceHeadersm] = topomig_wrap(data,Pos_in,Pos_cross,Pos_Z,dt,dx,dy,velocity,apperture,cosexp,radexp,freq,offset,in)
% wrapper function for topomig_3D mex function 
% example: 
%    [datam,FileHeaderm,TraceHeadersm] =...
%        topomig_wrap_3D(grid_line,xvec',yvec',zvec,dt,dx,dy,velocity,apperture,cosexp,radexp,freq,offset,in);
%% simple check for consitent input parameters:
% coordinate check
data_size = size(data);
if numel(size(data)) ~=2;
    disp('Please use 2D matrix [time,traces] as input')
    return
end
if data_size(2) ~= size(Pos_in,2);
    disp(['Problem with X coordinates: Nr of traces = ' num2str(data_size(2)) ' , but nr of coordinates : ' num2str(size(Pos_in,2))])
    return
end
if data_size(2) ~= size(Pos_cross,2);
    disp(['Problem with Y coordinates: Nr of traces = ' num2str(data_size(2)) ' , but nr of coordinates : ' num2str(size(Pos_cross,2))])
    return
end
if data_size(2) ~= size(Pos_Z,2);
    disp(['Problem with Z coordinates: Nr of traces = ' num2str(data_size(2)) ' , but nr of coordinates : ' num2str(size(Pos_Z,2))])
    return
end
if (data_size(1) ~= size(velocity,1)) || (data_size(2) ~= size(velocity,2))
    disp('Problem with velocity matrix, size not equal data matrix')
    return
end
if min(Pos_Z)<=0
    disp('negative z coordinates found, they must be > 0')
    return
end

%% create header structure
datum = clock;

FileHeader.StartingPosition=1;
FileHeader.NumberOfStacks = 1;
FileHeader.ZeroTimeSample=1;
FileHeader.SurveyMode = 'Reflection ';
FileHeader.PositionUnits = 'm';

FileHeader.TracesPerEnsemble = 1;

FileHeader.TotalTimeWindow = size(data,1).*dt;
FileHeader.NumberOfSamples = size(data,1);
FileHeader.StepSizeUsed = dx;
FileHeader.NumberOfTraces = size(data,2);

FileHeader.StartingPosition=1;
FileHeader.NominalFrequency = freq;
FileHeader.AntennaSeparation = offset;

FileHeader.RecMonth = datum(2);
FileHeader.RecDay = datum(3);
FileHeader.RecYear = datum(1);
FileHeader.PulserVoltage = 12;

in_lim=min(Pos_in):dx:max(Pos_in);
cross_lim=min(Pos_cross):dy:max(Pos_cross);

FileHeader.NumberOfTracesInLine=size(in_lim,2);
FileHeader.NumberOfTracesCrossLine=size(cross_lim,2);

inbin=1:(FileHeader.NumberOfTracesInLine);
inbin=repmat(inbin,FileHeader.NumberOfTracesCrossLine,1);

crossbin = 1:FileHeader.NumberOfTracesCrossLine;
crossbin = repmat(crossbin',1,FileHeader.NumberOfTracesInLine);

% 1 or 0 if 1 trace is ignored in migration and set to 0 in the results
CommentFlag = ones(size(Pos_in));
CommentFlag(in) = 0;

% % calculating surface normals by assuming a plane surface perpendicular to
% % the profile direction
if unique(crossbin)==1
    [Surfnx,Surfny,Surfnz]=surfnorm([Pos_in;Pos_in;Pos_in],[Pos_cross;Pos_cross+1;Pos_cross+2],[Pos_Z;Pos_Z;Pos_Z]);
    Surfnx_vec = (Surfnx(1,:));
    Surfny_vec = (Surfny(1,:));
    Surfnz_vec = (Surfnz(1,:));
    if sum(Pos_cross)~=0
        disp('Y Coordinates have to be zero')
        return
    end
else
    Pos_Z_grid = reshape(Pos_Z,[FileHeader.NumberOfTracesCrossLine, FileHeader.NumberOfTracesInLine]);
    Pos_in_grid = reshape(Pos_in,[FileHeader.NumberOfTracesCrossLine, FileHeader.NumberOfTracesInLine]);
    Pos_cross_grid = reshape(Pos_cross,[FileHeader.NumberOfTracesCrossLine, FileHeader.NumberOfTracesInLine]);
    [Surfnx,Surfny,Surfnz] = surfnorm(Pos_in_grid,Pos_cross_grid,Pos_Z_grid);
    
    %    Surfnx = Surfnx';
    %    Surfny = Surfny';
    %    Surfnz = Surfnz';
    
    Surfnx_vec = (Surfnx(:));
    Surfny_vec = (Surfny(:));
    Surfnz_vec = (Surfnz(:));
end
%% sort data in Y direction
nr_traces = FileHeader.NumberOfTraces ;

TraceHeaders.TraceNumbers = 1:nr_traces;
TraceHeaders.TimeOfDay = repmat(now,1,nr_traces);
TraceHeaders.NumberOfPointsPerTrace = repmat(FileHeader.NumberOfSamples,1,nr_traces);
TraceHeaders.NumberOfStacks  = repmat(FileHeader.NumberOfStacks,1,nr_traces);
TraceHeaders.TimeWindow = repmat(FileHeader.TotalTimeWindow,1,nr_traces);
TraceHeaders.CommentFlag = CommentFlag; % 1 or 0 if 1 trace is ignored in migration and set to 0 in the results
TraceHeaders.TimeZeroAdjustment = zeros(1,nr_traces);
TraceHeaders.Topography = Pos_Z;
TraceHeaders.ZeroFlag = zeros(1,nr_traces);
TraceHeaders.Surfnx =Surfnx_vec' ;
TraceHeaders.Surfny =Surfny_vec' ;
TraceHeaders.Surfnz =Surfnz_vec' ;
TraceHeaders.GPSX1 =Pos_in ;
TraceHeaders.GPSY1 =Pos_cross ;
TraceHeaders.GPSZ1 =Pos_Z ;
TraceHeaders.InBin  = inbin(:)' ;
TraceHeaders.CrossBin  = crossbin(:)' ;
TraceHeaders.Pos_in  = Pos_in ;
TraceHeaders.Pos_cross  = Pos_cross ;
TraceHeaders.Pos_Z  = Pos_Z ;


%% set migration parameters (not all are used due to several code edits)
%topographic migration (0=no, 1=yes)	<- standard migration possible  %
topomig.topoind = 1;
%initial scaling (0=no, 1=yes):		<- time scaling if input data unscaled	%
topomig.iscaling = 0;
%final scaling (0=no, 1=yes):		<- time scaling	of output data  %
topomig.fscaling = 0;
%cosinusexponent:					<- Exponent for the obliquity factor %
topomig.cosexp = cosexp;
%exponent of radius:					<- Exponent for the radial term	%
topomig.radexp = radexp;
% /*time step (ns, 0=input sample rate)
topomig.dt = FileHeader.TotalTimeWindow/ FileHeader.NumberOfSamples;
% define cut-off frequency in GHz % TOT???
topomig.cfreq = 1;
% sort_trace_index  <- Trace assortment of output traces by the
% positions in the grid %
topomig.traceass = 0;
% min.x-value:					<- minimum x position of output traces
topomig.minx =  min(TraceHeaders.Pos_in);
% max.x-value:  						<- maximum x position of output traces
topomig.maxx = max(TraceHeaders.Pos_in);
% x-gridsize-interval:				<- distance between adjacent output traces in x- direction
topomig.dx = dx;
% min.y-value: 						<- minimum y position of output traces
topomig.miny = min(TraceHeaders.Pos_cross);
% max.y-value: 						<- maximum y position of output traces
topomig.maxy = max(TraceHeaders.Pos_cross);
% y-gridsize-interval:				<- distance between adjacent output traces in y- direction
topomig.dy = dy;
% start_trace:						<- first trace to be migrated
topomig.itrace=1;
% end_trace:							<- last trace to be migrated
topomig.ftrace=FileHeader.NumberOfTraces;
% max.-migration time (ns):           <- max. migration time
topomig.maxt = FileHeader.TotalTimeWindow;
% time step (ns, 0=input sample rate):<- sample rate of the migrated output %
topomig.timestep = 0;
% max. offset for which data will be considered in migration
topomig.maxapert = apperture; %
% Kind of summation (0=stacking, 1=norm. crosscorrelation, 2=semblance coefficient)
topomig.summethod = 0;
% size of median-filter in (ns) to remove low-frequency effect onto the semblance %
topomig.medfilt = 100;
% Size of max.-timetube in ns:        (If the value is <=0.0 the inverse of the center frequency will be used as the size of the time tube)*/
topomig.ttube = 0;
% dist.-interval (m,0=complete range)
topomig.tracess = 0;
% nr.of traces/interval(0=complete range)
topomig.nrtraces = 0;
% offset estimate (m):                <- Parameter governing the size of the time tube for shallow depths*/
topomig.offset = 0;
% wanted resolution (ns): ( resolution (ns) is estimated )		<- Parameter governing the size of the time tube for shallow depths*/
topomig.res = 0;

%% call mex function
[datam,FileHeaderm]=topo_mig_opt(data,FileHeader,TraceHeaders,topomig,velocity);


%% write output structure
TraceHeadersm = TraceHeaders;
TraceHeadersm.NumberOfSamples = repmat(size(datam,1),1,size(datam,2));
TraceHeadersm.Topography = TraceHeaders.Pos_Z;


FileHeaderm.NumberOfSamples = size(datam,1);
FileHeaderm.ZeroTimeSample = 1;
FileHeaderm.TotalTimeWindow = FileHeader.TotalTimeWindow;
FileHeaderm.AntennaSeparation = offset;
FileHeaderm.PositionUnits=FileHeader.PositionUnits;
FileHeaderm.StartingPosition=0;
FileHeaderm.NumberOfStacks = 1;
FileHeaderm.ZeroTimeSample=0;
FileHeaderm.SurveyMode = 'Reflection ';
FileHeaderm.PositionUnits = 'm';
FileHeaderm.TracesPerEnsemble = 1;
end
