function [fname, pname, comments,offset,ntr,nsamp,sra,...
          px,py,pz,data,zdem,origpx,origpy,tti,zeff,...
          vel,head,azi,pitch,roll,hag,...
          profnr,date,info,profinfo,pnr,proc_steps]...
         = read_in_sgy(fname,pname)

%determine p (number of processing sequences already applied)
pos = strfind(fname,'proc');
if isempty(pos),
 pnr = 0;
else 
 pnr = str2num(fname(pos+4));
end 



[comments,offset,ntr,nsamp,sra,px,py,pz,data,zdem,...
 origpx,origpy,tti,zeff,profnr,date,info,profinfo,...
 vel,head,azi,pitch,roll,hag] = ReadSEGY([pname,fname]);
%disp(comments);

%create proc_steps and determine processing steps applied
% proc_steps=0 not applied
% proc_stesp=2 applied
% in "chose_proc_steps.m" steps can be set to 1,
%                i.e. to be executed in this run
if ~exist('proc_steps'),
proc_steps = struct('twin',0,'svd',0,'bgr',0,'butter',0,'surf',0,...
                    'bin',0,'hil',0,'agc',0,'mgain',0,'fxd',0,'kmig',0,...
                    'map',0,'bed',0,'split',0,'rep',0);
end
                
for i=1:40
    if strfind(comments(i,:),'PROCESSING APPLIED:')>0,
       for j=i+1:40
         if strfind(comments(j,:),'REDUCED TIME WINDOW')>0, proc_steps.twind=1;end;  
         if strfind(comments(j,:),'SVD FILTER')>0, proc_steps.svdd=1;end;     
         if strfind(comments(j,:),'BACKGROUND REMOVAL')>0, proc_steps.bgrd=1;end; 
         if strfind(comments(j,:),'BUTTERWORTH FILTER')>0, proc_steps.butterd=1;end; 
         if strfind(comments(j,:),'PICKED SURFACE REFLECTION')>0, proc_steps.surfd=1;end; 
         if strfind(comments(j,:),'TRACE BINNING')>0, proc_steps.bind=1;end; 
         if strfind(comments(j,:),'HILBERT ENVELOPE GAIN')>0, proc_steps.hild=1;end; 
         if strfind(comments(j,:),'AGC GAIN')>0, proc_steps.agcd=1;end;     
         if strfind(comments(j,:),'MANUAL GAIN SETTINGS')>0, proc_steps.mand=2;end; 
         if strfind(comments(j,:),'FX DECONVOLUTION')>0, proc_steps.fxdd=2;end; 
         if strfind(comments(j,:),'KIRCHHOFF MIGRATION')>0, proc_steps.kmigd=2;end; 
         if strfind(comments(j,:),'MAP CREATED')>0, proc_steps.mapd=2;end; 
         if strfind(comments(j,:),'GLACIER BED PICKED')>0, proc_steps.bedd=2;end;
         if strfind(comments(j,:),'SPLIT TOO LONG PROFILES')>0, proc_steps.splitd=2;end;     
       end;
    end;
end;
   
