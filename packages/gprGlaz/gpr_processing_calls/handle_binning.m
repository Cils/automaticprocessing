function [p,params,procp,proc_steps] = handle_binning(procp,proc_steps,params,p)

%set butter paramter
%input processing paramter
if (proc_steps.bin > 0), 
 prompt = {'Enter binning interval (m):'};
 dlg_title = 'Desired Binning Interval ';
 num_lines = 1;
 defaultans = {sprintf('%s',num2str(procp.dri))};
 answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
 procp.dri = str2num(answer{1});
end    


datat = params.data;
nsampt = params.nsamp;
t2 = (0:nsampt-1)*params.sra;

% -------------------------------------------------------------------------
% ------------------- DETERMINE BINNING CENTERS ---------------------------
% -------------------------------------------------------------------------

maxp = 1000000;
xi = zeros(maxp,1);
yi = zeros(maxp,1);
pseg = zeros(maxp,1);
n = 0;
rest = 0;

% dist: distance of segment
% azi:  azimuth of segment
% dri:  distance between binning centres
for a = 1:params.profinfo.n-1
    dist = sqrt((params.profinfo.x(a+1)-params.profinfo.x(a))^2 + ...
        (params.profinfo.y(a+1)-params.profinfo.y(a))^2);
    azi = atan2((params.profinfo.y(a+1)-params.profinfo.y(a)),...
                (params.profinfo.x(a+1)-params.profinfo.x(a)));
    if dist>rest
        n = n + 1;
        xi(n) = params.profinfo.x(a) + rest*cos(azi);
        yi(n) = params.profinfo.y(a) + rest*sin(azi);
        pseg(n) = a;
        
        nrBinsPerSeg=1;
        while(1)
            rest = rest + procp.dri;
            if (rest >= dist)
                rest = rest - dist;
                break;
            else
                n = n + 1;
                xi(n) = params.profinfo.x(a) + rest * cos(azi);
                yi(n) = params.profinfo.y(a) + rest * sin(azi);
                pseg(n) = a;
            end;
        end;
    else
        rest = rest - dist;
    end
    
    
end;

xi = xi(1:n);
yi = yi(1:n);
pseg = pseg(1:n);


% -------------------------------------------------------------------------
% ------------------- MAKE SURE PROFILE RUNS FROM WEST TO EAST ------------
% -------------------------------------------------------------------------
% if profile does not run from west to east: flip profile
if (xi(end) < xi(1))
    xi = flipud(xi);
    yi = flipud(yi);
    pseg = flipud(pseg);
end

%{
% % -------------------------------------------------------------------------
% % ------------------- PROJECT PX AND PY ON THE INDIVIDUAL SEGMENTS --------
% % -------------------------------------------------------------------------
xxp = zeros(length(params.px),params.profinfo.n-1);
yyp = zeros(length(params.py),params.profinfo.n-1);

for a = 1:(params.profinfo.n-1);
    x = params.profinfo.x(a:a+1); % Start and end points of segments
    y = params.profinfo.y(a:a+1); % Start and end points of segments
    dx = abs(diff(x));     % dx length of sement
    dy = abs(diff(y));     % dy length of segment
    
    % Find a straight line through the start and end points of the segments
    % Project xp, yp on this line
    if (dx > dy)
        cc = polyfit(x,y,1);
        [xxp(:,a),yyp(:,a)] = project_p(params.px,params.py,cc(1),cc(2));
    else
        cc = polyfit(y,x,1);
        [yyp(:,a),xxp(:,a)] = project_p(params.py,params.px,cc(1),cc(2));
    end;
end;

% % -------------------------------------------------------------------------
% ----------IDENTIFY FIRST AND LAST INTERPOLATION POINTS ON SEGMENT -------
% -------------------------------------------------------------------------

% Check start point
for a = 1:length(xi)
    dist = sqrt((xxp(:,pseg(a))-xi(a)).^2 + (yyp(:,pseg(a))-yi(a)).^2);
    if (isempty(find(dist < procp.dri)) == 0)
        break;
    end;
end;

first = a;          % First point projected on line
last = length(xi);  % last point projected on line

% Check last point
while(1)
    dist = sqrt((xxp(:,pseg(last))-xi(last)).^2 + (yyp(:,pseg(last))-yi(last)).^2);
    if (isempty(find(dist < procp.dri)) == 0)
        break;
    else
        last = last - 1;
    end;
end;

xi = xi(first:last);
yi = yi(first:last);
%}

figure(1); clf;
%mix = min(params.px);
%miy = min(params.py);
%plot(params.profinfo.x-mix,params.profinfo.y-miy, 'b-',xi-mix,yi-miy,'r.',...
%     params.px-mix,params.py-miy,'m.', xxp-mix,yyp-miy,'k.');
plot(params.profinfo.x,params.profinfo.y, 'm*',xi,yi,'gv',...
     params.px,params.py,'k^');
xlabel('Easting');
ylabel('Northing');
legend('picked segment','binning centres','measured points')
title('Projected Points Profile');
axis equal


% -------------------------------------------------------------------------
% ------------------- BINNING - TAKE ONLY CLOSEST TRACE -------------------
% -------------------------------------------------------------------------
[~,index1] = min(sqrt((params.px-xi(1)).^2 + (params.py-yi(1)).^2));
[~,index2] = min(sqrt((params.px-xi(end)).^2 + (params.py-yi(end)).^2));
indexMin=min(index1,index2);
indexMax=max(index1,index2);

ntri = length(xi);
datait = zeros(nsampt,ntri);
zi = zeros(ntri,1);
zeffi = zeros(ntri,1);
zdemi = zeros(ntri,1);
ttii = zeros(ntri,1);
fsi = zeros(ntri,1);
origpxi = zeros(ntri,1);
origpyi = zeros(ntri,1);
veli = zeros(ntri,1);
headi = zeros(ntri,1);
azii = zeros(ntri,1);
pitchi = zeros(ntri,1);
rolli = zeros(ntri,1);
hagi = zeros(ntri,1);
iivect=zeros(ntri,1);
for a = 1:ntri
    [~,ii] = min(sqrt((params.px(indexMin:indexMax)-xi(a)).^2 + (params.py(indexMin:indexMax)-yi(a)).^2));
    
    iivect(a)=(ii);
    datait(:,a) = datat(:,ii);
    zi(a) =  params.pz(ii);
    zeffi(a) = params.zeff(ii);
    zdemi(a) = params.zdem(ii);
    ttii(a) = params.tti(ii);
    origpxi(a) = params.origpx(ii);
    origpyi(a) = params.origpy(ii);
    veli(a) = params.vel(ii);
    headi(a) = params.head(ii);
    azii(a) = params.azi(ii);
    pitchi(a) = params.pitch(ii);
    rolli(a) = params.roll(ii);
    hagi(a) = params.hag(ii);

    % -------------- EXTRAPOLATE EMPTY TRACES --------------
    % If no data in trace, take data from trace before and write in this empty trace
    % With if (a > 1) allow the first trace to be empty (tti is zero for all traces in Ground based data)
    if (a > 1)
        if (zi(a) == 0), zi(a) = zi(a-1); end;
        if (zeffi(a) == 0), zeffi(a) = zeffi(a-1); end;
        if (zdemi(a) == 0), zdemi(a) = zdemi(a-1); end;
        if (veli(a) == 0), veli(a) = veli(a-1); end
        if (headi(a) == 0), headi(a) = headi(a-1); end
        if (azii(a) == 0), azii(a) = azii(a-1); end
        if (pitchi(a) == 0), pitchi(a) = pitchi(a-1); end
        if (rolli(a) == 0), rolli(a) = rolli(a-1); end
        if (hagi(a) == 0), hagi(a) = hagi(a-1); end
        if (ttii(a) == 0), ttii(a) = ttii(a-1); end;
        if ((fsi(a) == 0) & (a > 1)), fsi(a) = fsi(a-1); end;
        fsi = round(fsi);
        if (fsi(a) == 0), fsi(a) = 1; end;
    end

    if ((sum(abs(datait(:,a))) == 0) & (a ~=1))
        datait(:,a) = datait(:,a-1);     end;
end;

params.data = datait; params.px = xi; params.py = yi; params.pz = zi;
params.ntr = ntri; params.zdem = zdemi; params.tti = ttii;
params.origpx = xi; params.origpy = yi; params.zeff = zeffi;
params.vel = veli; params.head = headi; params.azi = azii;
params.pitch = pitchi; params.roll = rolli; params.hag = hagi;

proc_steps.bind = 1;
% optional sava as sgy file and/or plot radargram after binning
if (proc_steps.bins == 1), p = p+1; end; 
if (proc_steps.bins > 0), handle_segy_write(p,params,procp,proc_steps); end;
if (proc_steps.binp > 0), 
    fig = figure(2);
    [procp,params]=handle_segy_plot(proc_steps,p,params,procp,'After Binning',1,0,'binned');
end;



