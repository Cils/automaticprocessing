function [proc_steps,repeat] = chose_proc_steps(params)

repeat = 0;
proc_steps = struct('twin',0,'twinp',0,'twinv',0,'twind',0,'twins',0,...
                      'tzer',0,'tzerp',0,'tzerv',0,'tzerd',0,'tzers',0,...
                      'svd',0,'svdp',0,'svdv',0,'svdd',0,'svds',0,...
                      'bgr',0,'bgrp',0,'bgrv',0,'bgrd',0,'bgrs',0,...
                      'butter',0,'butterp',0,'butterv',0,'butterd',0,'butters',0,...
                      'surf',0,'surfp',0,'surfv',0,'surfd',0,'surfs',0,...
                      'bin',0,'binp',0,'binv',0,'bind',0,'bins',0,...
                      'hil',0,'hilp',0,'hilv',0,'hild',0,'hils',0,...
                      'agc',0,'agcp',0,'agcv',0,'agcd',0,'agcs',0,...
                      'mgain',0,'mgainp',0,'mgainv',0,'mgaind',0,'mgains',0,...
                      'fxd',0,'fxdp',0,'fxdv',0,'fxdd',0,'fxds',0,...
                      'kmig',0,'kmigp',0,'kmigv',0,'kmigd',0,'kmigs',0,...
                      'map',0,'mapp',0,'mapv',0,'mapd',0,'maps',0,...
                      'bed',0,'bedp',0,'bedv',0,'bedd',0,'beds',0,...
                      'split',0,'splitp',0,'splitv',0,'splitd',0,'splits',0,...
                      'raw_plot',0,'sgy',0,'rep',0);
                
                  
              

             
% read proc_steps from comments
[lines,columns]=size(params.comments);
for i=1:lines
 k = strfind(params.comments(i,:),'REDUCED TIME WINDOW');
 if ~isempty(k), proc_steps.twind = 1; end 
 k = strfind(params.comments(i,:),'TIME ZERO SET');
 if ~isempty(k), proc_steps.tzerd = 1; end 
 k = strfind(params.comments(i,:),'SVD FILTER');
 if ~isempty(k), proc_steps.svdd = 1; end 
 k = strfind(params.comments(i,:),'BACKGROUND REMOVAL');
 if ~isempty(k), proc_steps.bgrd = 1; end 
 k = strfind(params.comments(i,:),'BUTTERWORTH FILTER');
 if ~isempty(k), proc_steps.butterd = 1; end 
  k = strfind(params.comments(i,:),'PICKED SURFACE REFLECTION');
 if ~isempty(k), proc_steps.surfd = 1; end 
 k = strfind(params.comments(i,:),'TRACE BINNING');
 if ~isempty(k), proc_steps.bind = 1; end 
  k = strfind(params.comments(i,:),'HILBERT ENVELOPE GAIN');
 if ~isempty(k), proc_steps.hild = 1; end 
 k = strfind(params.comments(i,:),'AGC GAIN');
 if ~isempty(k), proc_steps.agcd = 1; end 
  k = strfind(params.comments(i,:),'MANUAL GAIN SETTINGS');
 if ~isempty(k), proc_steps.mgaind = 1; end 
 k = strfind(params.comments(i,:),'FX DECONVOLUTION');
 if ~isempty(k), proc_steps.fxdd = 1; end 
 k = strfind(params.comments(i,:),'KIRCHHOFF MIGRATION');
 if ~isempty(k), proc_steps.kmigd = 1; end 
 k = strfind(params.comments(i,:),'MAP CREATED');
 if ~isempty(k), proc_steps.mapd = 1; end 
 k = strfind(params.comments(i,:),'GLACIER BED PICKED');
 if ~isempty(k), proc_steps.bedd = 1; end 
 k = strfind(params.comments(i,:),'SPLIT TOO LONG PROFILES');
 if ~isempty(k), proc_steps.splitd = 1; end 
end  


% button1 = ' ';
% if exist('matdata\proc_steps.mat'),
%    button1 = questdlg('Found old proc_steps.mat file in matdata folder. Use it?')
%    if button1(1)=='Y'
%      button2 = questdlg('Repeat steps in procs_steps with all sgy filed in directory?')
%      repeat=1
%      uiresume;
%      close all; 
%     end     
%    uiresume;
%    close all; 
% end    
% 
% if button1(1) ~= 'Y',



%create GUI using "uicontrol" functions
ybase = 550;
xincr = 150;
yincr = 25;

figure('Position',[200 200 440 600]);

text1 = uicontrol( 'Style','text','Position', [5 ybase 300 30],'String','Processing Step',...
                   'FontWeight','bold','HorizontalAlignment','left'); 
text2 = uicontrol( 'Style','text','Position', [205 ybase 100 30],'String','Set',...
                   'FontWeight','bold');
text2b = uicontrol( 'Style','text','Position', [205 ybase-yincr 100 30],'String','Parameter',...
                   'FontWeight','bold');
text3 = uicontrol( 'Style','text','Position', [305 ybase 100 30],'String','Output',...
                   'FontWeight','bold'); 
text4 = uicontrol( 'Style','text','Position', [305 ybase-yincr 500 30],'String','*.sgy',...
                   'FontWeight','bold','HorizontalAlignment','left'); 
text5 = uicontrol( 'Style','text','Position', [355 ybase-yincr 50 30],'String','*.jpg',...
                   'FontWeight','bold','HorizontalAlignment','left'); 

if proc_steps.twind < 1,
twin = uicontrol( 'Style','checkbox','Position', [10 ybase-2*yincr 200 30],'String','Time Window Cut',...
                 'Callback',@(src,evnt)buttonCallback('twin') ); 
twinv = uicontrol( 'Style','checkbox','Position', [10+2*xincr-60 ybase-2*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('twinv') );             
twins = uicontrol( 'Style','checkbox','Position', [10+3*xincr-150 ybase-2*yincr 205 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('twins') ); 
twinp = uicontrol( 'Style','checkbox','Position', [10+4*xincr-250 ybase-2*yincr 205 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('twinp') );              
else    
twin = uicontrol( 'Style','text','Position', [10 ybase-2*yincr-6 310 30],'String','Time Window Cut already applied (following sgy text header)'); 
end 


if proc_steps.bgrd < 1,
bgr = uicontrol( 'Style','checkbox','Position', [10 ybase-3*yincr 200 30],'String','Background Removal',...
                 'Callback',@(src,evnt)buttonCallback('bgr') ); 
bgrv = uicontrol( 'Style','checkbox','Position', [10+2*xincr-60 ybase-3*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('bgrv') ); 
bgrs = uicontrol( 'Style','checkbox','Position', [10+3*xincr-150 ybase-3*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('bgrs') );                  
bgrp = uicontrol( 'Style','checkbox','Position', [10+4*xincr-250 ybase-3*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('bgrp') ); 
         
else    
bgr = uicontrol( 'Style','text','Position', [10 ybase-6-3*yincr 440 30],'String','Background removal already applied (following sgy text header)',...
                 'HorizontalAlignment','left'); 
end 

if proc_steps.svdd < 1,
svd = uicontrol( 'Style','checkbox','Position', [10 ybase-4*yincr 200 30],'String','SVD Filter',...
                 'Callback',@(src,evnt)buttonCallback('svd') ); 
svdv = uicontrol( 'Style','checkbox','Position', [10+2*xincr-60 ybase-4*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('svdv') );  
svds = uicontrol( 'Style','checkbox','Position', [10+3*xincr-150 ybase-4*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('svds') );               
svdp = uicontrol( 'Style','checkbox','Position', [10+4*xincr-250 ybase-4*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('svdp') );              
else    
svd = uicontrol( 'Style','text','Position', [26 ybase-10-4*yincr 440 30],'String','SVD Filter already applied (following sgy text header)',...
                 'HorizontalAlignment','left'); 
end

if proc_steps.butterd < 1,
butter = uicontrol( 'Style','checkbox','Position', [10 ybase-5*yincr 200 30],'String','Butterworth Bandpass',...
                 'Callback',@(src,evnt)buttonCallback('butter') ); 
butterv = uicontrol( 'Style','checkbox','Position', [10+2*xincr-60 ybase-5*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('butterv') );           
butters = uicontrol( 'Style','checkbox','Position', [10+3*xincr-150 ybase-5*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('butters') );                        
butterp = uicontrol( 'Style','checkbox','Position', [10+4*xincr-250 ybase-5*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('butterp') ); 
else    
butter = uicontrol( 'Style','text','Position', [26 ybase-10-5*yincr 440 30],'String','Butterworth Bandpass already applied (following sgy text header)',...
                    'HorizontalAlignment','left'); 
end

if proc_steps.surfd < 1,
surf = uicontrol( 'Style','checkbox','Position', [10 ybase-6*yincr 200 30],'String','Pick Surface Reflection',...
                 'Callback',@(src,evnt)buttonCallback('surf') ); 
surfv = uicontrol( 'Style','checkbox','Position', [10+2*xincr-60 ybase-6*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('surfv') );           
surfs = uicontrol( 'Style','checkbox','Position', [10+3*xincr-150 ybase-6*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('surfs') );                        
surfp = uicontrol( 'Style','checkbox','Position', [10+4*xincr-250 ybase-6*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('surfp') );     
else    
surf = uicontrol( 'Style','text','Position', [26 ybase-10-6*yincr 440 30],'String','Surface reflection already picked (following sgy text header)',...
                 'HorizontalAlignment','left'); 
end 

if proc_steps.bind < 1,
bin = uicontrol( 'Style','checkbox','Position', [10 ybase-7*yincr 200 30],'String','Trace binning',...
                 'Callback',@(src,evnt)buttonCallback('bin') ); 
binv = uicontrol( 'Style','checkbox','Position', [10+2*xincr-60 ybase-7*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('binv') );           
bins = uicontrol( 'Style','checkbox','Position', [10+3*xincr-150 ybase-7*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('bins') );                        
binp = uicontrol( 'Style','checkbox','Position', [10+4*xincr-250 ybase-7*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('binp') );     
else    
bin = uicontrol( 'Style','text','Position', [26 ybase-10-7*yincr 440 30],'String','Trace binning already applied (following sgy text header)',...
                 'HorizontalAlignment','left'); 
end 

if proc_steps.hild < 1,
hil = uicontrol( 'Style','checkbox','Position', [10 ybase-8*yincr 200 30],'String','Hilbert Gain Fucntion',...
                 'Callback',@(src,evnt)buttonCallback('hil') ); 
hilv = uicontrol( 'Style','checkbox','Position', [10+2*xincr-60 ybase-8*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('hilv') );           
hils = uicontrol( 'Style','checkbox','Position', [10+3*xincr-150 ybase-8*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('hils') );                        
hilp = uicontrol( 'Style','checkbox','Position', [10+4*xincr-250 ybase-8*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('hilp') );     
else    
hil = uicontrol( 'Style','text','Position', [26 ybase-10-8*yincr 440 30],'String','Hilbert Gain function already applied (following sgy text header)',...
                 'HorizontalAlignment','left');  
end 

if proc_steps.agcd < 1,
agc = uicontrol( 'Style','checkbox','Position', [10 ybase-9*yincr 200 30],'String','Automatic Gain Function',...
                 'Callback',@(src,evnt)buttonCallback('agc') ); 
agcv = uicontrol( 'Style','checkbox','Position', [10+2*xincr-60 ybase-9*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('agcv') );           
agcs = uicontrol( 'Style','checkbox','Position', [10+3*xincr-150 ybase-9*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('agcs') );                        
agcp = uicontrol( 'Style','checkbox','Position', [10+4*xincr-250 ybase-9*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('agcp') );     
else    
agc = uicontrol( 'Style','text','Position', [26 ybase-10-9*yincr 440 30],'String','AGC already applied (following sgy text header)',...
                 'HorizontalAlignment','left'); 
end 

if proc_steps.mgaind < 1,
mgain = uicontrol( 'Style','checkbox','Position', [10 ybase-10*yincr 200 30],'String','Manual Gain Setting',...
                 'Callback',@(src,evnt)buttonCallback('mgain') ); 
mgainv = uicontrol( 'Style','checkbox','Position', [10+2*xincr-60 ybase-10*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('mgainv') );           
mgains = uicontrol( 'Style','checkbox','Position', [10+3*xincr-150 ybase-10*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('mgains') );                        
mgainp = uicontrol( 'Style','checkbox','Position', [10+4*xincr-250 ybase-10*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('mgainp') );     
else    
agc = uicontrol( 'Style','text','Position', [26 ybase-10-10*yincr 440 30],'String','Manual Gain already applied (following sgy text header)',...
                 'HorizontalAlignment','left'); 
end 

if proc_steps.fxdd < 1,
fxd = uicontrol( 'Style','checkbox','Position', [10 ybase-11*yincr 200 30],'String','FX Deconvolution',...
                 'Callback',@(src,evnt)buttonCallback('fxd') ); 
fxdv = uicontrol( 'Style','checkbox','Position', [10+2*xincr-60 ybase-11*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('fxdv') );           
fxds = uicontrol( 'Style','checkbox','Position', [10+3*xincr-150 ybase-11*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('fxds') );                        
fxdp = uicontrol( 'Style','checkbox','Position', [10+4*xincr-250 ybase-11*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('fxdp') );     
else    
fxd = uicontrol( 'Style','text','Position', [26 ybase-10-11*yincr 440 30],'String','FX Decon already applied (following sgy text header)',...
                 'HorizontalAlignment','left');  
end 

if proc_steps.kmigd < 1,
kmig = uicontrol( 'Style','checkbox','Position', [10 ybase-12*yincr 200 30],'String','Kirchhoff Migration',...
                 'Callback',@(src,evnt)buttonCallback('kmig') ); 
kmigv = uicontrol( 'Style','checkbox','Position', [10+2*xincr-60 ybase-12*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('kmigv') );           
kmigs = uicontrol( 'Style','checkbox','Position', [10+3*xincr-150 ybase-12*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('kmigs') );                        
kmigp = uicontrol( 'Style','checkbox','Position', [10+4*xincr-250 ybase-12*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('kmigp') );     
else    
kmig = uicontrol( 'Style','text','Position', [26 ybase-10-12*yincr 440 30],'String','Kirchhoff migration already applied (following sgy text header)',...
                 'HorizontalAlignment','left');  
end 

if proc_steps.mapd < 1,
map = uicontrol( 'Style','checkbox','Position', [10 ybase-13*yincr 200 30],'String','Display Results with Map',...
                 'Callback',@(src,evnt)buttonCallback('map') ); 
mapv = uicontrol( 'Style','checkbox','Position', [10+2*xincr-60 ybase-13*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('mapv') );
mapp = uicontrol( 'Style','checkbox','Position', [10+4*xincr-250 ybase-13*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('mapp') );     
else    
map = uicontrol( 'Style','text','Position', [26 ybase-10-13*yincr 440 30],'String','Map Plot already produced (following sgy text header)',...
                 'HorizontalAlignment','left');  
end 

if proc_steps.bedd < 1,
bed = uicontrol( 'Style','checkbox','Position', [10 ybase-14*yincr 200 30],'String','Pick Glacier Bed',...
                 'Callback',@(src,evnt)buttonCallback('bed') ); 
bedv = uicontrol( 'Style','checkbox','Position', [10+2*xincr-60 ybase-14*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('bedv') );           
beds = uicontrol( 'Style','checkbox','Position', [10+3*xincr-150 ybase-14*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('beds') );                        
bedp = uicontrol( 'Style','checkbox','Position', [10+4*xincr-250 ybase-14*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('bedp') );     
else    
bed = uicontrol( 'Style','text','Position', [26 ybase-10-14*yincr 440 30],'String','Glacier bed already picked (following sgy text header)',...
                 'HorizontalAlignment','left'); 
end 

if proc_steps.splitd < 1,
split = uicontrol( 'Style','checkbox','Position', [10 ybase-15*yincr 200 30],'String','Split Long Profile',...
                 'Callback',@(src,evnt)buttonCallback('split') ); 
splitv = uicontrol( 'Style','checkbox','Position', [10+2*xincr-60 ybase-15*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('splitv') );           
splits = uicontrol( 'Style','checkbox','Position', [10+3*xincr-150 ybase-15*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('splits') );                        
splitp = uicontrol( 'Style','checkbox','Position', [10+4*xincr-250 ybase-15*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('splitp') );     
else    
split = uicontrol( 'Style','text','Position', [26 ybase-10-15*yincr 440 30],'String','Profile already split (following sgy text header)',...
                 'HorizontalAlignment','left');  
end 

if proc_steps.tzerd < 1,
tzer = uicontrol( 'Style','checkbox','Position', [10 ybase-16*yincr 200 30],'String','Set Time Zero',...
                 'Callback',@(src,evnt)buttonCallback('tzer') ); 
tzerv = uicontrol( 'Style','checkbox','Position', [10+2*xincr-60 ybase-16*yincr 200 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('tzerv') );             
tzers = uicontrol( 'Style','checkbox','Position', [10+3*xincr-150 ybase-16*yincr 205 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('tzers') ); 
tzerp = uicontrol( 'Style','checkbox','Position', [10+4*xincr-250 ybase-16*yincr 205 30],'String','','HorizontalAlignment','left',...
                 'Callback',@(src,evnt)buttonCallback('tzerp') );              
else    
tzer = uicontrol( 'Style','text','Position', [10 ybase-16*yincr-6 310 30],'String','Time zero already set (following sgy text header)'); 
end 

%ADD NEW IF BLOCK FOR NEW PROCESSING STEP Before THIS LINE AND INCREASE nr_check by 1
nr_check = 16;

if proc_steps.raw_plot < 1,
raw = uicontrol( 'Style','checkbox','Position', [10 ybase-(nr_check+1.5)*yincr 600 30],'String','Raw Data Plot',...
                 'Callback',@(src,evnt)buttonCallback('raw') );
end             

%sgy = uicontrol( 'Style','checkbox','Position', [10+xincr ybase-15*yincr 200 30],...
%                    'String','Write *.sgy file for this flow',...
%                    'Callback',@(src,evnt)buttonCallback('sgy') );              
             
rep = uicontrol( 'Style','checkbox','Position', [10 ybase-(nr_check+2.5)*yincr 600 30],...
                    'String','Repeat this flow for all selected segy files?',...
                    'Callback',@(src,evnt)buttonCallback('rep') );    
                
          
quit = uicontrol( 'Style','pushbutton','Position', [10 ybase-(nr_check+4.1)*yincr 400 30],...
                    'String','Continue with Processing!',...
                    'Callback',@(src,evnt)exit_menu('quit') );    
                
old = uicontrol( 'Style','pushbutton','Position', [10 ybase-(nr_check+5.5)*yincr 400 30],...
                    'String','Load proc_steps mat file!',...
                    'Callback',@(src,evnt)load_file('old'));                 
                
            
uiwait;

%----------------------------------------------------------------------------
%----------------------------------------------------------------------------

function buttonCallback(button_string)
   if  strcmp(button_string,'twin'), proc_steps.twin=twin.Value; end
   if  strcmp(button_string,'bgr'), proc_steps.bgr=bgr.Value; end
   if  strcmp(button_string,'svd'), proc_steps.svd=svd.Value; end
   if  strcmp(button_string,'butter'), proc_steps.butter=butter.Value; end
   if  strcmp(button_string,'surf'), proc_steps.surf=surf.Value; end
   if  strcmp(button_string,'bin'), proc_steps.bin=bin.Value; end
   if  strcmp(button_string,'hil'), proc_steps.hil=hil.Value; end
   if  strcmp(button_string,'agc'), proc_steps.agc=agc.Value; end
   if  strcmp(button_string,'mgain'), proc_steps.mgain=mgain.Value; end
   if  strcmp(button_string,'fxd'), proc_steps.fxd=fxd.Value; end
   if  strcmp(button_string,'kmig'), proc_steps.kmig=kmig.Value; end
   if  strcmp(button_string,'map'), proc_steps.map=map.Value; end
   if  strcmp(button_string,'bed'), proc_steps.bed=bed.Value; end
   if  strcmp(button_string,'split'), proc_steps.split=split.Value; end
   if  strcmp(button_string,'raw'), proc_steps.raw_plot=raw.Value; end
   if  strcmp(button_string,'tzer'), proc_steps.tzer=tzer.Value; end
   
   if  strcmp(button_string,'twins'), proc_steps.twins=twins.Value; end
   if  strcmp(button_string,'bgrs'), proc_steps.bgrs=bgrs.Value; end
   if  strcmp(button_string,'svds'), proc_steps.svds=svds.Value; end
   if  strcmp(button_string,'butters'), proc_steps.butters=butters.Value; end
   if  strcmp(button_string,'surfs'), proc_steps.surfs=surfs.Value; end
   if  strcmp(button_string,'bins'), proc_steps.bins=bins.Value; end
   if  strcmp(button_string,'hils'), proc_steps.hils=hils.Value; end
   if  strcmp(button_string,'agcs'), proc_steps.agcs=agcs.Value; end
   if  strcmp(button_string,'mgains'), proc_steps.mgains=mgains.Value; end
   if  strcmp(button_string,'fxds'), proc_steps.fxds=fxds.Value; end
   if  strcmp(button_string,'kmigs'), proc_steps.kmigs=kmigs.Value; end
   if  strcmp(button_string,'maps'), proc_steps.maps=maps.Value; end
   if  strcmp(button_string,'beds'), proc_steps.beds=beds.Value; end
   if  strcmp(button_string,'splits'), proc_steps.splits=splits.Value; end
   if  strcmp(button_string,'tzers'), proc_steps.tzers=tzers.Value; end
   
   if  strcmp(button_string,'twinp'), proc_steps.twinp=twinp.Value; end
   if  strcmp(button_string,'bgrp'), proc_steps.bgrp=bgrp.Value; end
   if  strcmp(button_string,'svdp'), proc_steps.svdp=svdp.Value; end
   if  strcmp(button_string,'butterp'), proc_steps.butterp=butterp.Value; end
   if  strcmp(button_string,'surfp'), proc_steps.surfp=surfp.Value; end
   if  strcmp(button_string,'binp'), proc_steps.binp=binp.Value; end
   if  strcmp(button_string,'hilp'), proc_steps.hilp=hilp.Value; end
   if  strcmp(button_string,'agcp'), proc_steps.agcp=agcp.Value; end
   if  strcmp(button_string,'mgainp'), proc_steps.mgainp=mgainp.Value; end
   if  strcmp(button_string,'fxdp'), proc_steps.fxdp=fxdp.Value; end
   if  strcmp(button_string,'kmigp'), proc_steps.kmigp=kmigp.Value; end
   if  strcmp(button_string,'mapp'), proc_steps.mapp=mapp.Value; end
   if  strcmp(button_string,'bedp'), proc_steps.bedp=bedp.Value; end
   if  strcmp(button_string,'splitp'), proc_steps.splitp=splitp.Value; end
   if  strcmp(button_string,'tzerp'), proc_steps.tzerp=tzerp.Value; end
   
   if  strcmp(button_string,'twinv'), proc_steps.twinv=twinv.Value; end
   if  strcmp(button_string,'bgrv'), proc_steps.bgrv=bgrv.Value; end
   if  strcmp(button_string,'svdv'), proc_steps.svdv=svdv.Value; end
   if  strcmp(button_string,'butterv'), proc_steps.butterv=butterv.Value; end
   if  strcmp(button_string,'surfv'), proc_steps.surfv=surfv.Value; end
   if  strcmp(button_string,'binv'), proc_steps.binv=binv.Value; end
   if  strcmp(button_string,'hilv'), proc_steps.hilv=hilv.Value; end
   if  strcmp(button_string,'agcv'), proc_steps.agcv=agcv.Value; end
   if  strcmp(button_string,'mgainv'), proc_steps.mgainv=mgainv.Value; end
   if  strcmp(button_string,'fxdv'), proc_steps.fxdv=fxdv.Value; end
   if  strcmp(button_string,'kmigv'), proc_steps.kmigv=kmigv.Value; end
   if  strcmp(button_string,'mapv'), proc_steps.mapv=mapv.Value; end
   if  strcmp(button_string,'bedv'), proc_steps.bedv=bedv.Value; end
   if  strcmp(button_string,'splitv'), proc_steps.splitv=splitv.Value; end
   if  strcmp(button_string,'tzerv'), proc_steps.tzerv=tzerv.Value; end
% save('matdata\\proc_steps.mat','proc_steps');  
 save([params.matdata,'proc_steps.mat'],'proc_steps');
   if  strcmp(button_string,'rep'), proc_steps.rep=rep.Value; end
  % if  strcmp(button_string,'sgy'), proc_steps.sgy=sgy.Value; end
end

function load_file(button_string)
   [fname,pname]=uigetfile({'*.mat';'*.mat'},'Choose mat file with proc_steps structure ...','*.mat')
   S = load([pname,fname]);
   proc_steps = S.proc_steps;
   clearvars S;
   close all;
   uiresume;
end  

function exit_menu(button_string)
   uiresume;
   close all; 
end  


end