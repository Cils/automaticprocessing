function [p,params,procp,proc_steps] = reduce_twin(procp,proc_steps,params,p)


% graphical interface to enter processing parameter
if (proc_steps.twinv > 0), 
 prompt = {'Enter maximal time (ns):'};
 dlg_title = 'Input paramter for Time Window Filter';
 num_lines = 1;
 defaultans = {sprintf('%s',num2str(procp.twin))};
 answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
 procp.twin = str2num(answer{1});
end    

params.nsamp = round(procp.twin/params.sra);
if (params.nsamp > length(params.data(:,1)))
    params.nsamp = length(params.data(:,1));
end;
params.data = params.data(1:params.nsamp,:);

proc_steps.twind = 1;

if (proc_steps.twins == 1), p = p+1; end; 
if (proc_steps.twins > 0), handle_segy_write(p,params,procp,proc_steps); end;
if (proc_steps.twinp > 0), [procp,params]=handle_segy_plot(proc_steps,p,params,procp,'Plot after Reduction of Time Window',1,0,'twin');end;
