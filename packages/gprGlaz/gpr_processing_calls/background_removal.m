function [p,params,procp,proc_steps] = background_removal(procp,proc_steps,params,p)

%Actual BGR
sumtr = median(params.data,2);
for b = 1:params.ntr
    params.data(:,b) = params.data(:,b) - sumtr; 
    params.data(:,b) = params.data(:,b) - median(params.data(:,b));
end;

proc_steps.bgrd = 1;
%Optional sgy save and plot, if so, inrease p counter by one
if (proc_steps.bgrs == 1), p = p+1; end; 
if (proc_steps.bgrs > 0), handle_segy_write(p,params,procp,proc_steps); end;
if (proc_steps.bgrp > 0), [procp,params]=handle_segy_plot(proc_steps,p,params,procp,'After Background Removal',1,0,'bgr');end;
