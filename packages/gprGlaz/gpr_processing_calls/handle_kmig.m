function [p,params,procp,proc_steps]=handle_kmig(procp,proc_steps,params,p);


%set kmig paramter
if (proc_steps.kmigv > 0), 
 prompt = {'Enter Kirchhoff Migration Parameter)'};
 dlg_title = 'Ice Velocity (m/s)';
 num_lines = 1;
 defaultans = {sprintf('%s',num2str(procp.vice))};
 answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
 procp.vice = str2num(answer{1});
end    

% ------------------- BUILD VELOCITY MODEL --------------------------------
% 2 layer velocity model: air - ice
vr = procp.vice * ones(size(params.data));
for a = 1:params.ntr
    isamp = round(params.tti(a)/params.sra);
    vr(1:isamp,a) = procp.vair;
end;

% ------------------- RUN MIGRATION ---------------------------------------
sprintf('start migration now')
migparams = nan*ones(12,1);
%migparams(12)=1;
[params.data,tmig,xmig]=kirk_mig(params.data,vr,params.sra,procp.dri,migparams);



proc_steps.kmigd = 1;
if (proc_steps.kmigs == 1), p = p+1; end; 
if (proc_steps.kmigs > 0), handle_segy_write(p,params,procp,proc_steps); end;
if (proc_steps.kmigp > 0), 
    fig = figure(2);
    [procp,params]=handle_segy_plot(proc_steps,p,params,procp,'After Kirchhoff Migration',0,0,'kmig');
end;
