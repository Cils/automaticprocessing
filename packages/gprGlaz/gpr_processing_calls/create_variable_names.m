function [segyfolder,plotfolder,pickfolder,matdata,tag,tag2] = create_variable_names(params)

%pp = 'F:\GLACIERS\OTEMMA\APRIL_2015\Heli_GPR_Arolla\';  % enter individual path
segyfolder = [params.pname];  % path to folder for storing SEGY files
plotfolder = strrep(segyfolder,'L1','L2') % path to folder for storing plotfiles
pickfolder = [segyfolder,'picks' filesep];  % path to folder for storing bedrock picks
matdata = [segyfolder,'matdata' filesep];  % path to folder for storing *.mat files
pos = strfind(params.fname,'profil');
ch = strfind(params.fname,'ch');
if isempty(ch),
  tag = params.fname(1:pos+10);
else
  tag = params.fname(1:pos+14);
end 
if isempty(tag), tag = params.fname(1:end-4), end;
if exist(plotfolder)~=7, mkdir(plotfolder), end;
if exist(pickfolder)~=7, mkdir(pickfolder), end;
tag2 = '';
if ~isempty(tag), tag2 = strrep(tag, 'ch1', 'ch2'); end;
