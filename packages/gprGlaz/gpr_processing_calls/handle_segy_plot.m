function [procp,params] = handle_segy_plot(proc_steps,p,params,procp,Title,time,adj,suffix)
% p     : number of processing step
% params: Stucture with most parameters and data from the
%         segy_gpr_processing.m main procedure
% procp:  Structure with processing parameters
% Title:  String with plot title
% time:   integer, see SegyPlotData.m for details
% adj:    integer, see SegyPlotData.m for details
% suffix: abbreviation inidcating last applied proc step

procp = SegyPlotData(Title,procp,params,time,adj);

plotfile = sprintf('%s\\%s%s.jpg',params.plotfolder,params.tag,suffix);        
print('-djpeg99','-r600',plotfile);
params.comments = make_comments(params.info.area,params.info.instr,...
                         params.profnr,params.info.freq,...
                         params.info.offset,params.info.dewsoft,...
                         params.date,params.ntr,params.sra,params.nsamp,...
                         params.profinfo,procp,proc_steps,0);
                     
comments2txt([plotfile(1:end-4),'.txt'],params.comments);
%save(procp_file,'procp');