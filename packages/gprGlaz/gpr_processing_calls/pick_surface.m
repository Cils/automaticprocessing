function [p,params,procp,proc_steps] = pick_surface(procp,proc_steps,params,p)

pick_file = sprintf('%s%ssurface.mat',params.matdata,params.tag);
 pick_ch2 = strrep(pick_file, 'ch2', 'ch1');
 pick_ch1 = strrep(pick_file, 'ch1', 'ch2');

%check if a surface pick file exists for this profile (ch1 or ch2)
if exist(pick_ch2, 'file') == 2 || exist(pick_ch1, 'file'),
    choice = questdlg('Surface pick file already exists. Keep old file','','Yes','No','Yes');
switch choice
 case 'Yes'
     disp([choice ' was chosen'])
     if exist(pick_ch2, 'file') == 2,
        load(pick_ch2);
        params.tti = tti;
     elseif exist(pick_ch1, 'file') == 2,
        load(pick_ch1);
        params.tti = tti
     end;
     save(pick_file,'-struct','params', 'tti', 'px', 'py', 'pz');
     proc_steps.surfd = 1;
     if (proc_steps.surfs == 1), p = p+1; end; 
     if (proc_steps.surfs > 0), handle_segy_write(p,params,procp,proc_steps); end;
     close all
     return;
 case 'No'
     disp([choice ' was chosen'])
end
end    
    
tax = 1500;
% graphical interface to enter processing parameter
if (proc_steps.surfv > 0), 
 prompt = {'Lower limit for time axis (only display)(ns):'};
 dlg_title = 'Input paramter for Surface Pick Window';
 num_lines = 1;
 defaultans = {sprintf('%s',num2str(tax))};
 answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
 tax = str2num(answer{1});
end    

%determine travel time to ground elevation point
tt_ground = 2*(params.pz - params.zeff) / 0.3;
tt_ground_hag = 2*params.hag / 0.3;


%determine time
t = (0:params.nsamp-1)*params.sra;

%why not just data/max(data)?
params.data = datanormalize(params.data);

clf;
imagesc(1:params.ntr,t,params.data);
%caxis(procp.cax);
caxis([-5 5])
colormap(procp.cmap);
v = axis;
axis([v(1) v(2) 0 tax]);
title('Pick Surface Reflection');
hold on
plot(1:params.ntr,tt_ground,'r')
hold on
plot(1:params.ntr,tt_ground_hag,'b');

% Handle to ask to keep calculated surface reflection
choice = 'Blue';
choice = questdlg('Surface reflection from laser (blue), the corrected surace topo point (red) or manual picking?','','Blue','Red','Manual','Blue');
switch choice
 case 'Blue'
   disp([choice ' was chosen'])
   keep = 1;
 case 'Red'
   disp([choice ' was chosen'])
   keep = 2;
 case 'Manual'
   disp([choice ' was chosen'])
   keep = 0; 
end

if (keep == 0),
 [nn,tt] = getline(gca); % select a polyline in the figure
 [nn,nidx] = sort(nn);
 tt = tt(nidx);
 nidx = find(diff(nn) > 1.0e-4);
 nn = nn(nidx);
 tt = tt(nidx);
 set(gcf,'WindowButtonMotionFcn','');
 params.tti = interp1(nn,tt,1:params.ntr,'linear','extrap');
 hold on;
 plot(1:params.ntr,params.tti,'k'); 
elseif (keep == 1)
 params.tti = tt_ground_hag;
else 
 params.tti = tt_ground;
end 

%determine surface height from picks and pz
%heli_height = (params.tti * 0.3) / 2;
%params.zeff = params.pz-heli_height;
   

save(pick_file,'-struct','params', 'tti', 'px', 'py', 'pz');

proc_steps.surfd = 1;
if (proc_steps.surfs == 1), p = p+1; end; 
if (proc_steps.surfs > 0), handle_segy_write(p,params,procp,proc_steps); end;

close all

  
   
       
  
        
        


