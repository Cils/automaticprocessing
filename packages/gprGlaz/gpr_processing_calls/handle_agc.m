function [p,params,procp,proc_steps]=handle_agc(procp,proc_steps,params,p);

%set agc paramter
if (proc_steps.agc > 0), 
 prompt = {'Enter AGC length (samples)'};
 dlg_title = 'AGC Length (samples)';
 num_lines = 1;
 defaultans = {sprintf('%s',num2str(procp.agclen))};
 answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
 procp.agclen = str2num(answer{1});
end    

if (procp.agclen > 0)
    params.data = agc(params.data,procp.agclen,params.sra);
end;

proc_steps.agcd = 1;
if (proc_steps.agcs == 1), p = p+1; end; 
if (proc_steps.agcs > 0), handle_segy_write(p,params,procp,proc_steps); end;
if (proc_steps.agcp > 0), 
    fig = figure(2);
    [procp,params]=handle_segy_plot(proc_steps,p,params,procp,'After AGC gain',0,0,'agc');
end;