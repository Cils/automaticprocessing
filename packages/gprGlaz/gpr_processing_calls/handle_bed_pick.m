    function [p,params,procp,proc_steps]=handle_bed_pick(procp,proc_steps,params,params2,p)
% if ch2 picked
if strfind(params.fname,'_ch2_')
    choice=questdlg({['You chose "' params.fname '".'] ,'It is recommended to choose only _CH1_. Do you want to continue wit _CH2_?'},'Warning','Continue','Skip','Skip');
    if strcmp(choice,'Skip')
        return;
    end
end

%set paramter for radargram display 
if (proc_steps.bedv > 0), 
 prompt = {'Enter max depth (m)','Vertical exxageration factor'};
 dlg_title = ' Display Paramater';
 num_lines = 1;
 defaultans = {sprintf('%s',num2str(procp.maxdepth)),sprintf('%s',num2str(procp.zexag))};
 answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
 procp.maxdepth = str2num(answer{1});
 procp.zexag = str2num(answer{2});
end
maxdepth = procp.maxdepth;
zexag = procp.zexag;

tagWithoutCh=strrep(params.tag, 'ch1_', '');tagWithoutCh=strrep(tagWithoutCh, 'ch2_', '');
outputfile=[params.plotfolder,tagWithoutCh,'bedrock.txt'];
pickfile=[params.pickfolder,tagWithoutCh,'bedrock.mat'];

%check if file with 'procp' structrure exists and if yes, load (e.g. for
%scaling values for plotting)
if exist(sprintf('%s%sprocp.mat',params.matdata,params.tag),'file'),
    procp_file = sprintf('%s%sprocp.mat',params.matdata,params.tag);
    load(procp_file);
end
procp.maxdepth = maxdepth;
procp.zexag = zexag;

if ~strcmp(params2.fname,'') 
    if exist(sprintf('%s%sprocp.mat',params2.matdata,params2.tag),'file')
        procp_file2 = sprintf('%s%sprocp.mat',params2.matdata,params2.tag);
        procp2=load(procp_file2);
        procp2=procp2.procp;
    end
    % check for existense of procp2, if not present, copy procp1
    if ~exist('procp2')
        procp2 = procp;
    end
    procp2.maxdepth = maxdepth;
    procp2.zexag = zexag;
else
    procp2=[];
end

%Actual picking routine
[xbed,ybed,zbed,zbed2,iweight,iweight2] = PickHorizon(params.px,params.py,params,params2,procp,procp2);       

% Determine zeff for ascii file output
if (sum(isnan(params.zdem)) == 0)
   params.zeff = params.zeff;
else
    params.zeff = params.zeff + procp.GlobalZshift;
end;

%determine glacier thickness and set to zero positions without picks
zbed(isnan(zbed))=0;
zbed2(isnan(zbed2))=0;
gt1 = (params.pz-params.hag)-zbed;
gt1(zbed==0) = 0;
gt2 = (params.pz-params.hag)-zbed2;
gt2(zbed2==0) = 0;

%save picks into *.mat file
save(pickfile,'xbed','ybed','zbed','zbed2','iweight','iweight2'); 

fid = fopen(outputfile,'wt');
for b = 1:length(xbed)
    [~,mi] = min(sqrt((params.px-xbed(b)).^2 + (params.py-ybed(b)).^2));
    fprintf(fid,'%s_%s %10.3f %10.3f %10.3f %10.3f %10.3f %d %10.3f %10.3f %d\n',...
                 params.tag(1:8),params.tag(end-7:end-5),...
                 xbed(b),ybed(b),(params.pz(mi)-params.hag(mi)),...
                 zbed(b),gt1(b),iweight(b),...
                 zbed2(b),gt2(b),iweight2(b));
end;
fclose(fid);    

if proc_steps.bedp == 1,
    figure(1);
    plotfile = [params.plotfolder,params.fname(1:end-9),'bedrock'];
    print('-djpeg99','-r600',plotfile);
    if ~strcmp(params2.fname,'')
        figure(2);
        plotfile = [params2.plotfolder,params2.fname(1:end-9),'bedrock'];
        print('-djpeg99','-r600',plotfile);
        figure(3);
        fname3=strrep(params.fname(1:end-9),'ch1','ch1+ch2');
        plotfile = [params.plotfolder,fname3,'bedrock'];
        print('-djpeg99','-r600',plotfile);
    end
end   

end