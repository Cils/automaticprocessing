function [p,params,procp,proc_steps] = singular_value_decomposition(procp,proc_steps,params,p)

%input processing paramter
if (proc_steps.svdv > 0), 
 prompt = {'Enter SVD length (traces):'};
 dlg_title = 'Input paramter for SVD Filter';
 num_lines = 1;
 defaultans = {sprintf('%s',num2str(procp.svd_len))};
 answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
 procp.svd_len = str2num(answer{1});
end    

%svd
d = params.data;
n = length(d(1,:));
%dv = zeros(size(d));
a = 1;
done = 0;
while(done == 0)
    n2 = a;
    n3 = a+procp.svd_len-1;
    n1 = n2 - procp.svd_len;
    n4 = n3 + procp.svd_len;
    if (n1 < 1), n1 = 1; end;
    if (n3 >= n), n3 = n; done = 1; end;
    if (n4 > n), n4 = n; end;
    nneffidx = n3 - n2;
    dtmp = d(:,n1:n4);
   [u,s,v] = svd(dtmp);
   s(1,1) = 0;
%   s(2,2) = 0;
   dtmp = u*s*v';
   params.data(:,n2:n3) = dtmp(:,(n2-n1+1):(n2-n1+1)+nneffidx);
   a = a + procp.svd_len;
end;

proc_steps.svdd = 1;
if (proc_steps.svds == 1), p = p+1; end; 
if (proc_steps.svds > 0), handle_segy_write(p,params,procp,proc_steps); end;
if (proc_steps.svdp == 1), [procp,params]=handle_segy_plot(proc_steps,p,params,procp,'After SVD',1,0,'svd');end;

