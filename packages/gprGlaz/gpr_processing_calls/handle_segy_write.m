function handle_segy_write(p,params,procp,proc_steps)

 segyname = sprintf('%s%sproc%i.sgy',params.segyfolder,params.tag,p);
 params.comments = make_comments(params.info.area,params.info.instr,...
                          params.profnr,params.info.freq,...
                          params.info.offset,params.info.dewsoft,...
                          params.date,params.ntr,params.sra,params.nsamp,...
                          params.profinfo,procp,proc_steps,0);
 GPR_write_segy(segyname,params.comments,params.info.offset,params.sra,...
     params.px,params.py,params.pz,params.data,params.zdem,...
     params.origpx,params.origpy,params.tti,params.zeff,...
     params.profnr,params.date,...
     params.vel,params.head,params.azi,params.pitch,params.roll,params.hag);
 comments2txt([segyname(1:end-4),'.txt'],params.comments);
 sprintf('--- Write file %s after %i processing round ---',segyname,p) 
