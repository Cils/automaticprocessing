function [p,params,procp,proc_steps]=handle_fxd(procp,proc_steps,params,p);

%set agc paramter
if (proc_steps.fxdv > 0), 
 prompt = {'Enter Decon Filter length (samples)','prewhitening factor','minimum frequency in data','maximum frequency in data'};
 dlg_title = 'Decon Length (samples)';
 num_lines = 1;
 defaultans = {sprintf('%s',num2str(procp.decon_len)),sprintf('%s',num2str(procp.decon_mu))...
               sprintf('%s',num2str(procp.fl)),sprintf('%s',num2str(procp.fh))};
 answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
 procp.decon_len = str2num(answer{1});
 procp.decon_mu = str2num(answer{2});
 procp.fl = str2num(answer{3});
 procp.fh = str2num(answer{4});
end    

if (procp.decon_len > 0)
    params.data = fx_decon(params.data,params.sra/1000,procp.decon_len,procp.decon_mu,procp.fl,procp.fh);
end;

proc_steps.fxdd = 1;
if (proc_steps.fxds == 1), p = p+1; end; 
if (proc_steps.fxds > 0), handle_segy_write(p,params,procp,proc_steps); end;
if (proc_steps.fxdp > 0), 
    fig = figure(2);
    [procp,params]=handle_segy_plot(proc_steps,p,params,procp,'After FX Deconvolution',0,0,'fxd');
end;