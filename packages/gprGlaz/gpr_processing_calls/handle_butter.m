function [p,params,procp,proc_steps] = handle_butter(procp,proc_steps,params,p);

%set butter paramter
%input processing paramter
if (proc_steps.butterv > 0), 
 prompt = {'Enter lower frequency bound (Hz):','Enter upper frequency bound (Hz):'};
 dlg_title = 'Frequency Range for Butterworth Filter';
 num_lines = 1;
 defaultans = {sprintf('%s',num2str(procp.fl)),sprintf('%s',num2str(procp.fh))};
 answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
 procp.fl = str2num(answer{1});
 procp.fh = str2num(answer{2});
end    

%calc spectrum
fdata = sum(abs(fft(params.data))');
f = linspace(0,1/(2*params.sra),params.nsamp/2) * 1000;
nf = length(f);

%spectrum plot
fig = figure(1); clf;
plot(f,fdata(1:nf),'b');
hold on;
xlabel('Frequency [MHz]','fontsize',12);
ylabel('Amplitude','fontsize',12);
title('Amplitude Spectrum','fontsize',12);

%apply butterworth filter
nyq = 0.5 / params.sra * 1000;
fl = procp.fl/nyq;
fh = procp.fh/nyq;
[bb,aa] = butter(procp.ford,[fl fh]);
params.data = filter(bb,aa,params.data);
fdata = sum(abs(fft(params.data))');

%continue spectrum plot
plot(f,fdata(1:nf),'r');
legend('Before Butterworth Bandpass Filter','After Butterworth Bandpass Filter');
hold off

proc_steps.butterd = 1;
if (proc_steps.butters == 1), p = p+1; end; 
if (proc_steps.butters > 0), handle_segy_write(p,params,procp,proc_steps); end;
if (proc_steps.butterp > 0), 
    fig = figure(2);
    [procp,params]=handle_segy_plot(proc_steps,p,params,procp,'After Butterworth Bandpass',0,0,'butter');
end;

