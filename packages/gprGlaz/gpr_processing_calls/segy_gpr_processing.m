% try to quess which is the next file the user wants to open
try
    initPar = read_init();
    if isfield(initPar, 'lastSgyFile')
        lastSgyFile=initPar.lastSgyFile;
        if strcmp(lastSgyFile(end-6:end),'RAW.sgy')
            possibleFile = [lastSgyFile(1:end-7) 'proc1.sgy'];
            if exist(possibleFile, 'file') == 2
                nextSgyFile = possibleFile;
            else
                nextSgyFile = lastSgyFile;
            end
        elseif strcmp(lastSgyFile(end-8:end-5),'proc')
            possibleFile = [lastSgyFile(1:end-5) num2str(str2double(lastSgyFile(end-4))+1) '.sgy'];
            if exist(possibleFile, 'file') == 2
                nextSgyFile = possibleFile;
            else
                nextSgyFile = lastSgyFile;
            end
        elseif strcmp(lastSgyFile(end-9:end-6),'proc')
            possibleFile = [lastSgyFile(1:end-6) num2str(str2double(lastSgyFile(end-5:end-4))+1) '.sgy'];
            if exist(possibleFile, 'file') == 2
                nextSgyFile = possibleFile;
            else
                nextSgyFile = lastSgyFile;
            end
        else
            nextSgyFile=lastSgyFile;
        end
    else 
        nextSgyFile='';
    end
catch
    nextSgyFile='';
end

% open filenames
[fnames,pnames,index]=uigetfile({'*.sgy';'*.sgy'},'Choose sgy file to proces, ...',nextSgyFile,'MultiSelect','on');
nr_files = check_nr_files(fnames) ;
rep = 0; %initialize varaible to check for repetition

fname = determine_fname(nr_files,fnames,1);
write_init('lastSgyFile', [pnames '\' fname]);

%loop over all chosen *.out files
for k = 1:nr_files,

    %determin "fname" string    
    fname = determine_fname(nr_files,fnames,k);

    %create params structure
    % params = struct('fname',[],'pname',[],'comments',[], ...
    %               'offset',[],'ntr',[],'nsamp',[],...
    %               'sra',[],'px',[],'py',[],'pz',[],...
    %               'data',[],'zdem',[],...
    %               'origpx',[],'origpy',[],...
    %               'tti',[],'zeff',[],...
    %               'vel',[],'head',[],'azi',[],...
    %               'pitch',[],'roll',[],'hag',[],...
    %               'profnr',[],'date',[],...
    %               'info',[],'profinfo',[],...
    %               'segyfolder','','plotfolder','',...
    %               'pickfolder','','matdata','',...
    %               'tag','','tag2','');

    % Read in segy file
    fprintf('Reading sgy File ....\n');
    [params.fname, params.pname, params.comments,params.offset,...
     params.ntr,params.nsamp,params.sra,...
     params.px,params.py,params.pz,params.data,params.zdem,...
     params.origpx,params.origpy,params.tti,params.zeff,...
     params.vel,params.head,params.azi,params.pitch,params.roll,params.hag,...
     params.profnr,params.date,params.info,params.profinfo,...
     p,proc_steps] = read_in_sgy(fname,pnames);

    % Read in ch2 segy file
    if any(strfind(fname,'ch1'))
        fname_ch2 = strrep(fname, 'ch1', 'ch2');
        [params2.fname, params2.pname, params2.comments,params2.offset,...
            params2.ntr,params2.nsamp,params2.sra,...
            params2.px,params2.py,params2.pz,params2.data,params2.zdem,...
            params2.origpx,params2.origpy,params2.tti,params2.zeff,...
            params2.vel,params2.head,params2.azi,params2.pitch,params2.roll,params2.hag,...
            params2.profnr,params2.date,params2.info,params2.profinfo,...
            p,proc_steps] = read_in_sgy(fname_ch2,pnames);
    else
        fname_ch2 = '';
        params2.fname = '';
        params2.pname = '';
    end


    % create procp structure which is determine processing status of segy file
    if rep == 0,  % check if this is a repetition after first sgy file
     procp = make_procp(params.comments);
    end 

    % Determine variable names and missing folders
    [params.segyfolder,params.plotfolder,params.pickfolder,...
     params.matdata,params.tag,params.tag2] = create_variable_names(params);
    if ~strcmp(fname_ch2,'')
        [params2.segyfolder,params2.plotfolder,params2.pickfolder,...
        params2.matdata,params2.tag,params2.tag2] = create_variable_names(params2);
    end

    % read in profinfo file
    if strcmp(params.tag(end-3:end-2),'ch')
        profinfo_file = strcat(params.segyfolder,params.tag(1:end-4),'profinfo.txt');
    else
        profinfo_file = strcat(params.segyfolder,params.tag,'profinfo.txt');
    end
    if exist(profinfo_file, 'file') == 2,
        M=dlmread(profinfo_file);
        params.profinfo.x = M(:,1);
        params.profinfo.y = M(:,2);
        params.profinfo.n = M(1,3);
        if ~strcmp(fname_ch2,'')
         params2.profinfo.x = M(:,1);
         params2.profinfo.y = M(:,2);
         params2.profinfo.n = M(1,3); 
        end 
    end

    % Determine processing to do
    if rep == 0, % check if this is a repetition after first sgy file
      [proc_steps,repeat] = chose_proc_steps(params);
      if proc_steps.rep > 0,
        save([pnames,'/matdata/','proc_steps.mat'],'proc_steps');
        rep = 1;
      end 
    else
      load([pnames,'/matdata/','proc_steps.mat']);  
      proc_steps = val_flag_zero(proc_steps);
    end;


    % Processing calls: one if statement per call 
    if (proc_steps.raw_plot == 1),
       fprintf('Plotting data with no processing applied ...\n');
       [procp,params]= handle_segy_plot(proc_steps,p,params,procp,'No further processing applied',0,0,'raw');
    end

    if (proc_steps.tzer == 1),
       fprintf('Set time zero ...\n');
       [p,params,procp,proc_steps] = set_tzero(procp,proc_steps,params,p);
    end

    if (proc_steps.twin == 1),
       fprintf('Reducing time window length ...\n');
       [p,params,procp,proc_steps] = reduce_twin(procp,proc_steps,params,p);
    end

    if (proc_steps.surf == 1),
        fprintf('Picking surface ....\n');
       [p,params,procp,proc_steps] = pick_surface(procp,proc_steps,params,p);
    end

    if (proc_steps.bgr == 1),
        fprintf('Removing Background ....\n')
       [p,params,procp,proc_steps] = background_removal(procp,proc_steps,params,p);
    end

    if (proc_steps.svd == 1),
       fprintf('Performing SVD Filter ...\n');
       [p,params,procp,proc_steps] = singular_value_decomposition(procp,proc_steps,params,p);
    end

    if (proc_steps.butter == 1),
        fprintf('Butterwoth Frequency Bandpass filtering ...\n')
       [p,params,procp,proc_steps] = handle_butter(procp,proc_steps,params,p);
    end

    if (proc_steps.bin == 1),
       fprintf('Spatial binning of data ...\n')
       [p,params,procp,proc_steps] = handle_binning(procp,proc_steps,params,p);
    end

    if (proc_steps.agc == 1),
       fprintf('Applying AGC gain ...\n')
       [p,params,procp,proc_steps] = handle_agc(procp,proc_steps,params,p);
    end

    if (proc_steps.fxd == 1),
       fprintf('Applying FX Deconvolution ...\n')
       [p,params,procp,proc_steps] = handle_fxd(procp,proc_steps,params,p);
    end

    if (proc_steps.kmig == 1),
       fprintf('Applying Kirchhoff migration ...\n')
       [p,params,procp,proc_steps] = handle_kmig(procp,proc_steps,params,p);
    end

    
    if (proc_steps.map == 1),
       fprintf('Display results with map ...\n')
       [p,params,procp,proc_steps] = handle_map(procp,proc_steps,params,params2,p);
    end

    if (proc_steps.bed == 1),
       fprintf('Picking bedrock ...\n')
       [p,params,procp,proc_steps] = handle_bed_pick(procp,proc_steps,params,params2,p);
    end

    if (proc_steps.sgy > 0), 
        p = p+1;
        handle_segy_write(p,params,procp,proc_steps);
    end;
    
end; 

disp('Finished.')