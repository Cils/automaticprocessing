function [proc_steps] = val_flag_zero(proc_steps)

% set in proc_steps structure all value change flags to zero

proc_steps.twinv = 0;
proc_steps.svdv = 0;
proc_steps.bgrv = 0;
proc_steps.butterv = 0;
proc_steps.surfv = 0;
proc_steps.binv = 0;
proc_steps.hilv = 0;
proc_steps.agvv = 0;
proc_steps.mgainv = 0;
proc_steps.fxdv = 0;
proc_steps.kmigv = 0;
proc_steps.mapv = 0;
proc_steps.bedv = 0;
proc_steps.splitv = 0;


