function [trout,t]=igabor(tvs,trow,fcol,twin,tinc,p)
% IGABOR: inverse Gabor transform
%
% [trout,t]=igabor(tvs,trow,fcol,twin,tinc,p)
% 
% IGABOR performs an inverse Gabor transform of a Gabor spectrum. This is
% implemented with a modified Gaussian synthesis window of unity. This is
% the inverse of fgabor.
%
% tvs ... input time variant spectrum or Gabor spectrum. This is typically
%      created by FGABOR.
% trow ... time coordinate vector for the rows of tvs
% fcol ... frequency coordinate vector for the columns of tvs
% twin ... half-width (seconds) of the Gaussian window
% tinc ... temporal shift (seconds) between windows
% p ... exponent used in analysis windowing. If g is the modified Gaussian
%   window then the synthesis window actually used is g.^(1-p) . 
%   The value of p must lie in the interval [0,1]. 
% trout ... output time series
% t ... time coordinate vector for trout
%
% by G.F. Margrave, May 2001 updated July 2009
%
% NOTE: It is illegal for you to use this software for a purpose other
% than non-profit education or research UNLESS you are employed by a CREWES
% Project sponsor. By using this software, you are agreeing to the terms
% detailed in this software's Matlab source file.
 
% BEGIN TERMS OF USE LICENSE
%
% This SOFTWARE is maintained by the CREWES Project at the Department
% of Geology and Geophysics of the University of Calgary, Calgary,
% Alberta, Canada.  The copyright and ownership is jointly held by 
% its author (identified above) and the CREWES Project.  The CREWES 
% project may be contacted via email at:  crewesinfo@crewes.org
% 
% The term 'SOFTWARE' refers to the Matlab source code, translations to
% any other computer language, or object code
%
% Terms of use of this SOFTWARE
%
% 1) Use of this SOFTWARE by any for-profit commercial organization is
%    expressly forbidden unless said organization is a CREWES Project
%    Sponsor.
%
% 2) A CREWES Project sponsor may use this SOFTWARE under the terms of the 
%    CREWES Project Sponsorship agreement.
%
% 3) A student or employee of a non-profit educational institution may 
%    use this SOFTWARE subject to the following terms and conditions:
%    - this SOFTWARE is for teaching or research purposes only.
%    - this SOFTWARE may be distributed to other students or researchers 
%      provided that these license terms are included.
%    - reselling the SOFTWARE, or including it or any portion of it, in any
%      software that will be resold is expressly forbidden.
%    - transfering the SOFTWARE in any form to a commercial firm or any 
%      other for-profit organization is expressly forbidden.
%
% END TERMS OF USE LICENSE

[nwin,nf]=size(tvs);

q=1-p;

if(p==1)
    %no synthesis windowing
    [trout,t]=ifftrl(sum(tvs),fcol); 
else
    %inverse transform the first row of tvs
    [tmp,t]=ifftrl(tvs(1,:),fcol);
    %build first window and POU norm factor
    [gwin,norm_factor]=gaussian_upou(t,trow(1),twin,tinc);
    trout=tmp.*(gwin.^q);
    for k=2:nwin
        [gwin,norm_factor]=gaussian_upou(t,trow(k),twin,tinc,norm_factor);
        tmp=ifftrl(tvs(k,:),fcol);
        trout=trout+tmp.*(gwin.^q);
    end
end

%trout=trout(:)/nwin;
trout=trout(:);
