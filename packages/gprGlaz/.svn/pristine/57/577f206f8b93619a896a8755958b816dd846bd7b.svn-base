/*
 * =============================================================
 * Topo_mig_opt.c
 * This code compiles to a Matlab mex file for the topographic migration 
 * using rms velocities (Allroggen 2014). It uses a Matlab script to 
 * calculate surface normals and set the necessary headers. The code has
 * been tested used with Matlab 2013a at Linux(Arch).
 * The code is a continues development based on the work of Lehmann 2002,
 * reworked by Björn Heincke, optimised by Rita Streich, transfered to a 
 * mex file by Urs Böniger.
 * It has been reported to also run at MAC/Windows, but is not tested there.
 * Furthermore, it has to be regarded to be experimental and requires 
 * futher testing, optimisation and possibly cleaning by an professional 
 * programmer. Anyhow, feel free to contact me about code questions I 
 * will try to answer (search for Niklas Allroggen, at moment 
 * Niklas.Allroggen@geo.uni.potsdam.de).
 * 
 * Use at your own risk! I´m not responsible if you break your system!
 * 
 * This is a MEX-file for MATLAB.
 * Copyright (c) 1984-2014 The MathWorks, Inc.
 * =============================================================
 */
#include <stdlib.h>
#include <stddef.h>
#include <ctype.h>
#include <mex.h>
#include <string.h>
#include <time.h>
#include <stdio.h>
#include <math.h>

#include "Datastructs.h"
#include "nrutil.h"

#define MAXCHARS 80   /* max length of string contained in each field */
#define   LIGHT 0.3

/* Always add the gateway routine.  */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
  const char **fnamesFh;       /* pointers to field names */
  mxArray    *value;
  int        nfieldsFh, ifieldFh;
  int 	     drows , dcols, j;
  /* Initializing existing variables */
  int offset_index;
  int topo_index, initial_index, final_index, sort_trace_index;
  float phi_scal, R_scal, low_pass_high_cut_freq;
  long i,l,k,m,nr_of_samples, nr_of_active_traces, *i_act_tr;
  long time_sample, t_deriv_index, trace_number_grid;
  long start_trace, end_trace, x_index, y_index;
  long timezerohelp, timezerohelp_out, no_pts_help;
  /* Binary header variables */ 
      
  /* Read in the data values */
  double *data1;
  /* Read velocity: By NA */
  double *velocity1;
  float **velocity_a;
  
  /* Output data pointer */
  double *migdata;
  /* Set the trace header pointers */
  double *pnum_seq, *pnum_x, *pnum_y, *pno_pts, *pno_stacks, *pzero_flag;
  double *pcom_flag, *px, *py, *pz, *pnormx, *pnormy, *pnormz, *ptimewindow, *ptoposhift;
  double *ptimezero_adj, *ptime_day;
  
  float *spur, *z_coor, *z_coor1, offset,time_resol, max_time, max_appert;
  float **data;
  float **data_sum;
  float scaling, time_help, /*time0,*/ _time, time_sample_float, time_sample_residual;
  float *time0vector, *time0sep, *time0_2, *scalvector, *tmpx, *tmpy, *tmpz;
  float vel2, vel24, *help1, t21help;
  float zmin, zmax;
  float x_grid_min, x_grid_max, y_grid_min, y_grid_max, x_grid_int, y_grid_int;
  float time_step;

  double scal_help, weighting, zeit_angabe_sekunden;
  double tmp_dist;
  double vel05, scal_exp, t1, t2, weighthelp, partsum, *zparthelp;
  
  time_t time_start, time_relative;
  BINARY_HEADER  bi_head, bi_head_out;
  TRACE_HEADER  *tr_head, *tr_head_grid;
    
  /* Check proper input and output, nhrs corresponds to the number of 
  right hand sides */
   if (nrhs != 5) 
    mexErrMsgTxt("The fields Data, FileHeader, TraceHeaders, MigParams and a RMS velocity field are required!");
  else if (nlhs != 2)
    mexErrMsgTxt("Two output parameters (Data and File header) expected.");
  else if (!mxIsDouble(prhs[0]))
    mexErrMsgTxt("The Data input is defined by a double array.");
  else if (!mxIsStruct(prhs[1]))
    mexErrMsgTxt("The FileHeader input must be a structure.");
  else if (!mxIsStruct(prhs[2]))
    mexErrMsgTxt("The TraceHeaders input must be a structure.");
  else if (!mxIsStruct(prhs[3]))
    mexErrMsgTxt("The MigParams input must be a structure.");

/* Get and print the size of the input data*/
drows = mxGetM(prhs[0]);
dcols = mxGetN(prhs[0]);	

/* Get the number of fields of the File Header */
nfieldsFh = mxGetNumberOfFields(prhs[1]);
fnamesFh = mxCalloc(nfieldsFh, sizeof(*fnamesFh));

for (ifieldFh = 0; ifieldFh < nfieldsFh; ifieldFh++) {
    fnamesFh[ifieldFh] = mxGetFieldNameByNumber(prhs[1],ifieldFh);
  }
plhs[1] = mxCreateStructMatrix(1, 1, nfieldsFh, fnamesFh);

/* Read the input parameters from the Lehmann code modified after B. Heincke */

topo_index = (int) *mxGetPr(mxGetField(prhs[3],0,"topoind"));
initial_index = (int) *mxGetPr(mxGetField(prhs[3],0,"iscaling"));
final_index = (int) *mxGetPr(mxGetField(prhs[3],0,"fscaling"));
phi_scal = (float) *mxGetPr(mxGetField(prhs[3],0,"cosexp"));
R_scal = (float) *mxGetPr(mxGetField(prhs[3],0,"radexp"));
t_deriv_index = (long) *mxGetPr(mxGetField(prhs[3],0,"dt"));
low_pass_high_cut_freq = (float) *mxGetPr(mxGetField(prhs[3],0,"cfreq"));
sort_trace_index = (int) *mxGetPr(mxGetField(prhs[3],0,"traceass"));
x_grid_min = (double) *mxGetPr(mxGetField(prhs[3],0,"minx"));
x_grid_max = (double) *mxGetPr(mxGetField(prhs[3],0,"maxx"));
x_grid_int = (double) *mxGetPr(mxGetField(prhs[3],0,"dx"));
y_grid_min = (double) *mxGetPr(mxGetField(prhs[3],0,"miny"));
y_grid_max = (double) *mxGetPr(mxGetField(prhs[3],0,"maxy"));
y_grid_int = (double) *mxGetPr(mxGetField(prhs[3],0,"dy"));
start_trace = (long) *mxGetPr(mxGetField(prhs[3],0,"itrace"));
end_trace = (long) *mxGetPr(mxGetField(prhs[3],0,"ftrace"));
max_time = (float) *mxGetPr(mxGetField(prhs[3],0,"maxt"));
time_step = (float) *mxGetPr(mxGetField(prhs[3],0,"timestep"));
max_appert = (float) *mxGetPr(mxGetField(prhs[3],0,"maxapert"));
offset = (float) *mxGetPr(mxGetField(prhs[3],0,"offset"));
time_resol = (float) *mxGetPr(mxGetField(prhs[3],0,"res"));


/*----------------------- Various output stuff similar to Björns stuff -----------------------*/
/* Start timer: */
time(&time_start);

mexPrintf("------------------Migration Parameters:------------------------\n");
/* mexPrintf("Migration velocity [m/ns]:            %f\n", velocity);
 */
if(topo_index == 1) 
	mexPrintf("Topographic migration:        	      yes\n");
else
	mexPrintf("Topographic migration:        	      no\n");

if(initial_index == 1) 
	mexPrintf("Apply scaling prior to migration:    yes\n");
else
	mexPrintf("Apply scaling prior to migration:     no\n");

if(final_index == 1) 
	mexPrintf("Apply scaling after migration:        yes\n");
else
	mexPrintf("Apply scaling after migration:        no\n");

mexPrintf("Applied cos-exponent:                 %f\n", phi_scal);
mexPrintf("Applied (1/r)-exponent:               %f\n", R_scal);

if(t_deriv_index == 0)
	mexPrintf("Trace modifications:                  Raw traces\n");
else if(t_deriv_index < 0)
{
	mexPrintf("Trace modifications:                  %d-order integration of traces\n",(-1)*t_deriv_index);
	mexPrintf("Low-cut filter:                %f GHz\n", low_pass_high_cut_freq);
}
else if(t_deriv_index > 0)
{
	mexPrintf("Kind of data:                 %d-order derivative of traces\n", t_deriv_index);
	mexPrintf("Filter applied at:               %f GHz\n\n", low_pass_high_cut_freq);
}

if(sort_trace_index == 1)
{
	mexPrintf("Trace assortment of output traces by the positions in the grid:\n");
	mexPrintf("Minimum x-position:            %f\n", x_grid_min);
	mexPrintf("Maximum x-position:            %f\n", x_grid_max);
	mexPrintf("Grid-interval in x-direction:  %f\n\n", x_grid_int);
	mexPrintf("Minimum y-position:            %f\n", y_grid_min);
	mexPrintf("Maximum y-position:            %f\n", y_grid_max);
	mexPrintf("Grid-interval in y-direction:  %f\n\n", y_grid_int);
}
else
{	
	mexPrintf("Trace assortment of output traces by input trace numbers:\n");
	mexPrintf("Start trace:                  	      %ld\n", start_trace);
	mexPrintf("End trace:                    	      %ld\n\n", end_trace);
}

if(max_time <= 0.0)
	mexPrintf("The trace-length in the migrated section is the same like the input section\n");
else
{
	mexPrintf("The trace-length is %fns (or the trace length [ns])!\n",max_time);
}


if(time_step > 0.)
	mexPrintf("Time step in migrated data: %f ns\n\n", time_step);
else if(time_step == 0.)
	mexPrintf("The output time sampling is equal to the input sampling!\n\n");
else
{
	mexPrintf("Selected time sampling invalid!\n\n");
	return;
}

if(max_appert <= 0.0)
	mexPrintf("The migration aperture is set to infinity\n\n");
else
	mexPrintf("The migration aperture is set to: %f\n\n", max_appert);


	mexPrintf("Stacking method:               Summing along diffraction hyperbolas\n");

	mexPrintf("Maximum estimated offset:      %f\n",offset);
	mexPrintf("The wanted resolution:         %f\n",time_resol);

mexPrintf("----------------------------------------------------------------\n\n");

/*----------------------- End printing stuff similar to Björns stuff -----------------------*/

/* Read the File Header in */

bi_head.no_tr = (long) *mxGetPr(mxGetField(prhs[1],0,"NumberOfTraces"));
bi_head.no_pts = (long) *mxGetPr(mxGetField(prhs[1],0,"NumberOfSamples"));
bi_head.timewindow = (float) *mxGetPr(mxGetField(prhs[1],0,"TotalTimeWindow"));
bi_head.frequency =  (float) *mxGetPr(mxGetField(prhs[1],0,"NominalFrequency"));
bi_head.antenna_sep = (float) *mxGetPr(mxGetField(prhs[1],0,"AntennaSeparation"));
bi_head.no_stacks = (long) *mxGetPr(mxGetField(prhs[1],0,"NumberOfStacks"));
bi_head.timezero_pt = (long) *mxGetPr(mxGetField(prhs[1],0,"ZeroTimeSample"));
bi_head.no_bin_x = (long) *mxGetPr(mxGetField(prhs[1],0,"NumberOfTracesInLine"));
bi_head.no_bin_y = (long) *mxGetPr(mxGetField(prhs[1],0,"NumberOfTracesCrossLine"));
bi_head.voltage = (float) *mxGetPr(mxGetField(prhs[1],0,"PulserVoltage"));
bi_head.sample_rate = bi_head.timewindow/bi_head.no_pts;

    if (bi_head.timezero_pt < 1)
		bi_head.timezero_pt = 1;
  
    if((end_trace-start_trace+1)%bi_head.no_bin_x != 0 && sort_trace_index != 1)
    {
        mexPrintf("\nWARNING: number of traces to be migrated is not a multiple of number of traces per line.\n");
        mexPrintf("Problems might occur in data conversion later.\n");
        mexPrintf("Performing migration anyway...\n\n");
    }
    else if(((start_trace-1)%bi_head.no_bin_x != 0 || (end_trace%bi_head.no_bin_x != 0)) && sort_trace_index !=1)
    {
        mexPrintf("\nWARNING: data set is split in the middle of a line.\n");
        mexPrintf("Problems might occur in data conversion later.\n");
        mexPrintf("Performing migration anyway...\n\n");
    }

	if (bi_head.no_bin_x == 1 || bi_head.no_bin_y == 1)
		mexPrintf("\n2D Kirchhof Migration\n--------------------------\n\n");
	else
		mexPrintf("\n3D Kirchhof Migration\n--------------------------\n\n");

    if (topo_index == 1)
		mexPrintf("\ncounting for topography\n");
	else
		mexPrintf("\n!! IGNORES TOPOGRAPHY !!\n");

    /* create output binary header */
    bi_head_out = bi_head;
    if( time_step > 0)
        bi_head_out.sample_rate = time_step;

    /*: Set the trace length of the output traces:*/
    	max_time = max_time/bi_head_out.sample_rate; /* max_time becomes output sample nr of max. time */
	if((max_time > 0) && (max_time < bi_head.no_pts))
		nr_of_samples = (long) max_time + 1;
	else
		nr_of_samples = (long) bi_head.no_pts;

	/* (De-)activate the maximum aperture option*/
	if(max_appert <= 0.0)
		offset_index = 0;
	else
		offset_index = 1;

	bi_head_out.no_pts = nr_of_samples;
	bi_head_out.timewindow = (float)(bi_head_out.sample_rate * (bi_head_out.no_pts -1));
    	bi_head_out.timezero_pt = (bi_head.timezero_pt-1) * (long)(bi_head.sample_rate/bi_head_out.sample_rate) + 1;

/*....................................................................................*/
/*      allocate memory					 				  						  */
/*....................................................................................*/

	if(sort_trace_index ==1)
	{
		trace_number_grid = ((int)((x_grid_max-x_grid_min)/x_grid_int)+1)*((int)((y_grid_max-y_grid_min)/y_grid_int)+1);
		mexPrintf("\n........................................\n");
		mexPrintf("total bytes of memory to allocate (max.): %d\n",(bi_head.no_tr+1)*bi_head.no_pts+trace_number_grid*bi_head_out.no_pts+(trace_number_grid+bi_head.no_tr)*(sizeof(TRACE_HEADER)+4)+sizeof(BINARY_HEADER)+2*sizeof(FILE)+1024*2);
		mexPrintf("........................................\n");
	}
	else
	{
		trace_number_grid = (end_trace-start_trace+1);
		mexPrintf("\n........................................\n");
		mexPrintf("total bytes of memory to allocate (max.): %d\n",(bi_head.no_tr+1)*bi_head.no_pts+trace_number_grid*bi_head.no_pts+(trace_number_grid+bi_head.no_tr)*(sizeof(TRACE_HEADER)+4)+sizeof(BINARY_HEADER)+2*sizeof(FILE)+1024*2);
		mexPrintf("........................................\n");
	}
    /* allocate memory for input data volume */
    /*---------------------------------*/
    /* allocate pointers to rows */    
	data=(float **) mxCalloc(bi_head.no_tr,sizeof(float*));
      if (!data)
	  {
		  mexPrintf("allocation failure 1 data\n\n");
		  return;	
	  }
    /* added allocation for velocity matrix; modified by NA */ 
	velocity_a=(float **) mxCalloc(bi_head.no_tr,sizeof(float*));
      if (!velocity_a)
	  {
		  mexPrintf("allocation failure 1 velocity_a\n\n");
		  return;	
	  } 
    /* allocate rows and set pointers to them */
    for (l=0;l<bi_head.no_tr;l++)
	{
		data[l]=(float *) mxCalloc(bi_head.no_pts,sizeof(float));
		velocity_a[l]=(float *) mxCalloc(bi_head.no_pts,sizeof(float));
	
		if (!data[l]) 
	    {
	      mexPrintf("allocation failure 2 in data\n\n");
	      return;
	    }
		
		if (!velocity_a[l]) 
	    {
	      mexPrintf("allocation failure 2 in velocity_a\n\n");
	      return;
	    }
	 }


/* allocate memory for data_sum volume */
/*-------------------------------------*/

		/* allocate pointers to rows */   
		data_sum=(float **) mxCalloc(trace_number_grid,sizeof(float*));
		if (!data_sum)
		{
			mexPrintf("allocation failure 1 data_sum\n\n");
			  return;	
		}
      
		/* allocate rows and set pointers to them */
		for (l=0;l<trace_number_grid;l++)
		{
			data_sum[l]=(float *) mxCalloc(nr_of_samples,sizeof(float));
			if (!data_sum[l]) 
			{
			mexPrintf("allocation failure 2 in data_sum\n\n");
			return;
			}
		}	


/* allocate memory for trace_header, migration mask, trace, and z-coordinate */
/*---------------------------------------------------------------------------*/

    spur = (float *)mxCalloc(bi_head.no_pts,sizeof(float));
    if (!spur)
    {
		mexPrintf("\n\nkann das memory fuer 'spur' nicht alluzieren\n\n");
        return;
    }

    tr_head = (TRACE_HEADER *)mxCalloc(bi_head.no_tr,sizeof(TRACE_HEADER));
    if (!tr_head)
    {
		mexPrintf("\n\nkann das memory fuer 'tr_head' nicht alluzieren\n\n");
        return;
    }

	/*output-daten*/
	tr_head_grid = (TRACE_HEADER *)mxCalloc(trace_number_grid,sizeof(TRACE_HEADER));
    if (!tr_head_grid)
    {
		mexPrintf("\n\nkann das memory fuer 'tr_head_grid' nicht alluzieren\n\n");
        return;
    }

	z_coor = (float *)mxCalloc(bi_head.no_tr,sizeof(float));
    if (!z_coor)
    {
		mexPrintf("\n\nkann das memory fuer 'z_coor' nicht alluzieren\n\n");
        return;
    }


	z_coor1 = (float *)mxCalloc(trace_number_grid,sizeof(float));
    if (!z_coor1)
    {
		mexPrintf("\n\nkann das memory fuer 'z_coor1' nicht alluzieren\n\n");
        return;
    }

/*........................................................................................*/
/*      reads data into memory, apply initial scaling, and calculates the time derivative */
/*........................................................................................*/

mexPrintf("\n...................................\n");
mexPrintf("data input & preprocessing");
mexPrintf("\n...................................\n\n");

/* Read the Trace Headers in */
/*for (i=0;i<10;i++)
{
tr_head[i].num_seq= (long) mxGetPr(mxGetField(prhs[0],0,"TraceNumbers"));
}
*/

/* Readin the data and assign it to the data matrix */
data1 = mxGetPr(prhs[0]); /*Read in the data as one long vector (1,1-2,1-3,1-4,1....n,m */
velocity1 = mxGetPr(prhs[4]);
for (l=0; l<dcols;l++)
{
	for (i=0; i<drows;i++)
	{
		data[l][i]=data1[i+l*drows];
		velocity_a[l][i]=velocity1[i+l*drows];
	}
}


/* Read the traces and trace headers and assign them to the structure */
zmin = (float)9999;
zmax = (float)-9999;

pnum_seq = mxGetPr(mxGetField(prhs[2],0,"TraceNumbers"));
pno_pts = mxGetPr(mxGetField(prhs[2],0,"NumberOfPointsPerTrace"));
pno_stacks = mxGetPr(mxGetField(prhs[2],0,"NumberOfStacks"));
ptimewindow = mxGetPr(mxGetField(prhs[2],0,"TimeWindow"));
pcom_flag = mxGetPr(mxGetField(prhs[2],0,"CommentFlag"));
ptime_day = mxGetPr(mxGetField(prhs[2],0,"TimeOfDay"));
ptimezero_adj = mxGetPr(mxGetField(prhs[2],0,"TimeZeroAdjustment"));
ptoposhift = mxGetPr(mxGetField(prhs[2],0,"Topography"));
pzero_flag = mxGetPr(mxGetField(prhs[2],0,"ZeroFlag"));
pnum_x = mxGetPr(mxGetField(prhs[2],0,"InBin"));
pnum_y = mxGetPr(mxGetField(prhs[2],0,"CrossBin"));
pnormx = mxGetPr(mxGetField(prhs[2],0,"Surfnx"));
pnormy = mxGetPr(mxGetField(prhs[2],0,"Surfny"));
pnormz = mxGetPr(mxGetField(prhs[2],0,"Surfnz"));
px = mxGetPr(mxGetField(prhs[2],0,"PosX"));
py = mxGetPr(mxGetField(prhs[2],0,"PosY"));
pz = mxGetPr(mxGetField(prhs[2],0,"PosZ"));

/* Maybe I still have to add the following entries */
/*ppos_x =
ppos_y =
ptimezero =
ptime_ms =
*/

  /*      mexPrintf("bi_head.no_tr %ld bi_head.no_pts %ld \n\n", bi_head.no_tr,bi_head.no_pts);
        mexEvalString("drawnow;");
        return;*/
for (j=0;j<bi_head.no_tr;j++)
{
	tr_head[j].num_seq=(long) pnum_seq[j];
	tr_head[j].no_pts=(long) pno_pts[j];
	tr_head[j].no_stacks=(long) pno_stacks[j];
	tr_head[j].timewindow=(float) ptimewindow[j];
	tr_head[j].com_flag=(long) pcom_flag[j];
	tr_head[j].time_day=(long) ptime_day[j];
	tr_head[j].timezero_adj=(float) ptimezero_adj[j];
	tr_head[j].toposhift=(float) ptoposhift[j];
	tr_head[j].zero_flag=(long) pzero_flag[j];
	tr_head[j].normx=(float) pnormx[j];
	tr_head[j].normy=(float) pnormy[j];
	tr_head[j].normz=(float) pnormz[j];
	tr_head[j].num_x=(long) pnum_x[j];
	tr_head[j].num_y=(long) pnum_y[j];
	tr_head[j].x=(float) px[j];
	tr_head[j].y=(float) py[j];
	tr_head[j].z=(float) pz[j];

	/* Topography implemented */
	if (tr_head[j].zero_flag != 1)
	{
		if(tr_head[j].z < zmin)
			zmin = tr_head[j].z;
		if(tr_head[j].z > zmax)
			zmax = tr_head[j].z;
	}

	z_coor[j] = tr_head[j].z;
	if (topo_index != 1)
		tr_head[j].z = (float)0;

	
	/* Initial scaling implemented */
	scaling = (float)1;
	
	for(i=0;i<bi_head.no_pts;i++)
	{
		if (initial_index == 1)
			scaling = (float)(fabs((float)(i-bi_head.timezero_pt+1))*bi_head.sample_rate*velocity_a[j][i]/2.0);
		
		data[j][i]  = data[j][i]*scaling;
	}

}

/*........................................................................................*/
/*      Arrange and determine the output traces                                           */
/*........................................................................................*/
for(l=0; l<(trace_number_grid); l++)
{
		for(i=0; i<nr_of_samples; i++)
		{
			data_sum[l][i] = (float)0;
		}
}

/* Rearranging output header for a regular grid Still makes some Problems */
/* !!!! TO BE MODIFIED !!!*/
if(sort_trace_index ==1)
{		
	k=0;
	for(x_index=0; x_index < ((int)((x_grid_max-x_grid_min)/x_grid_int)+1); x_index++)
		{
		for(y_index=0; y_index < ((int)((y_grid_max-y_grid_min)/y_grid_int)+1); y_index++)
		{
		for(m=0; m< bi_head.no_tr ; m++)
			{		
			if(tr_head[m].x == (x_grid_min + ((float)x_index*x_grid_int)) && tr_head[m].y == (y_grid_min + ((float)y_index*y_grid_int)))
				{
				tr_head_grid[k] = tr_head[m];
				z_coor1[k] = z_coor[m];
				mexPrintf("%d of %d gridpoint coordinates are assigned\n",k+1,trace_number_grid);
				goto next_trace;
				}
			}
		mexPrintf("NO input trace exists at this grid point! This case is NOT implemented yet !!!");
		return;

		next_trace:;
		k++;
		}
		}
}
else
{	
	for(k=0; k<trace_number_grid; k++)
	{
		tr_head_grid[k] = tr_head[start_trace-1+k];	
		z_coor1[k] = z_coor[start_trace-1+k];
	}
}

/*........................................................................................*/
/*      Setup the Matlab outputs							  */	
/*........................................................................................*/

plhs[0] = mxCreateDoubleMatrix(bi_head_out.no_pts,trace_number_grid, mxREAL);
/* Create a pointer to the output data */
migdata = mxGetPr(plhs[0]);

/*........................................................................................*/
/*      calculates migration mask and							  */	
/*........................................................................................*/
/*........................................................................................*/
/*      2D or 3D Kirchhoff migration							  */	
/*........................................................................................*/

mexPrintf("\n...................................\n");
mexPrintf("calculates migration mask followed by migration");
mexPrintf("\n...................................\n\n");

/* preparation in attempt to accelerate program */
time0vector = (float *)mxCalloc(bi_head_out.no_pts,sizeof(float));
if (!time0vector)
{
	mexPrintf("allocation failure time0vector\n\n");
      return;	
  	}

  time0sep = (float *)mxCalloc(bi_head_out.no_pts,sizeof(float));
if (!time0sep)
{
	mexPrintf("allocation failure time0sep\n\n");
      return;	
  	}	
  
  time0_2 = (float *)mxCalloc(bi_head_out.no_pts,sizeof(float));
if (!time0_2)
{
	mexPrintf("allocation failure time0_2\n\n");
      return;	
  	}

  scalvector = (float *)mxCalloc(bi_head_out.no_pts,sizeof(float));
if (!scalvector)
{
	mexPrintf("allocation failure scalvector\n\n");
      return;	
  	}

  help1 = (float *)mxCalloc(bi_head_out.no_pts,sizeof(float));
if (!help1)
{
	mexPrintf("allocation failure help1\n\n");
      return;	
  	}

  zparthelp = (double *)mxCalloc(bi_head_out.no_pts,sizeof(double));
 	if (!zparthelp)
{
	mexPrintf("allocation failure zparthelp\n\n");
      return;	
  	}

/*allocate memory for the trace distances*/
/*-------------------------------------*/
  tmpx=(float *) mxCalloc(bi_head.no_tr,sizeof(float));
  	if (!tmpx)
{
    mexPrintf("allocation failure in tmpx\n\n");
	return;	
}

  tmpy=(float *) mxCalloc(bi_head.no_tr,sizeof(float));
 	if (!tmpy)
{
    mexPrintf("allocation failure in tmpy\n\n");
	return;	
}

  tmpz=(float *) mxCalloc(bi_head.no_tr,sizeof(float));
  	if (!tmpz)
{
    mexPrintf("allocation failure in tmpz\n\n");
	return;	
}

/*allocate memory for the index specifying,*/ 
/* if the trace is located within the aperture*/
/*-------------------------------------*/
	i_act_tr = (long *) mxCalloc(bi_head.no_tr,sizeof(long));
    if (!i_act_tr)
	{
	    mexPrintf("allocation failure in i_act_tr\n\n");
		return;	
	}

	
	
/*calculate constant parameters (not velocity dependend) */ 
    	scal_exp = (double)(R_scal + phi_scal);
	t1 = bi_head.antenna_sep/LIGHT;
	timezerohelp = bi_head.timezero_pt - 1;
    	timezerohelp_out = bi_head_out.timezero_pt - 1;	
	no_pts_help = bi_head.no_pts - 1;
	
	if (final_index != 1)
	{
		for(i=timezerohelp_out; i<bi_head_out.no_pts; i++)
			scalvector[i] = (float)1.;
	}
/*........................................................................................*/
/*       Common diffraction stack                                                         */
/*........................................................................................*/
    mexPrintf("Survey boundaries X min: %f X max %f Y min: %f Y max: %f \n\n", x_grid_min, x_grid_max,y_grid_min,y_grid_max);
    mexPrintf("trace_number_grid: %d \n\n", trace_number_grid);
    mexEvalString("drawnow;");

for (k = 0; k < trace_number_grid; k++)
{
        if( k % 1000 == 0 )
        {
			mexPrintf("At trace: %ld of %ld \n\n", k, trace_number_grid );
			mexEvalString("drawnow;");
        }
		if(tr_head[k].com_flag == 1.)
		{
			for(i=timezerohelp_out; i<bi_head_out.no_pts; i++) 
			{
				migdata[i+k*bi_head_out.no_pts]=-1;
			} 
		}
		else
		{		
			nr_of_active_traces = 0;
			/*Calculate the distances*/
					/*mexPrintf("At trace: %ld of %ld \n\n", k, trace_number_grid);
					mexEvalString("drawnow;");*/
			for(l = 0; l < bi_head.no_tr; l++)
			{
				tmpx[l] = (tr_head[l].x - tr_head_grid[k].x);
				tmpy[l] = (tr_head[l].y - tr_head_grid[k].y);
				tmpz[l] = (tr_head[l].z - tr_head_grid[k].z);
				
				/*Specify, if the trace lies within the migration aperture:*/
				tmp_dist = sqrt(tmpx[l]*tmpx[l] + tmpy[l]*tmpy[l] + tmpz[l]*tmpz[l]);
			
				if((offset_index == 1 && tmp_dist < max_appert) && tr_head[l].com_flag == 0.)
				{
					i_act_tr[nr_of_active_traces] = l; /*associated trace will be considered during migration*/
					nr_of_active_traces = nr_of_active_traces+1;
				}	
			}

			if(offset_index == 0)
				nr_of_active_traces = bi_head.no_tr;
				/*if( k % 100 == 0 )
					{	
					mexPrintf("time_min: %ld time max %ld active traces %d \n\n", timezerohelp_out, nr_of_samples,nr_of_active_traces );
					mexEvalString("drawnow;");
					}*/
			for (i=timezerohelp_out; i<nr_of_samples; i++) /* loop over the samples */
			{
				time0vector[i] = (float)(i - bi_head.timezero_pt*bi_head.sample_rate/bi_head_out.sample_rate + 1)*bi_head_out.sample_rate;
				time0_2[i] = time0vector[i]*time0vector[i];

				for(l = 0; l < nr_of_active_traces; l++) /* loop through input traces */
				{
					help1[i] = (float)4.0*time0vector[i]/velocity_a[i_act_tr[l]][i];
					vel2 = velocity_a[i_act_tr[l]][i] * velocity_a[i_act_tr[l]][i];
					vel24 = (float)4.0 / vel2;
					vel05 = 0.5 * (double)velocity_a[i_act_tr[l]][i];
				
					t2 = bi_head.antenna_sep*bi_head.antenna_sep/vel2;		
					t21help = (float)(sqrt(t2)-t1);
					time0sep[i] = (float)(sqrt(time0vector[i]*time0vector[i]+t2)-t1);
					zparthelp[i] = 0.5*(double)time0sep[i]*(double)velocity_a[i_act_tr[l]][i];
					weighthelp = pow((double)velocity_a[i_act_tr[l]][i],(double) R_scal);

					if (final_index == 1)
						scalvector[i] = (float)(fabs((float)(i-timezerohelp_out))*bi_head_out.sample_rate*vel05);
					
					time_help = time0_2[i] + vel24*(tmpx[i_act_tr[l]]*tmpx[i_act_tr[l]] + tmpy[i_act_tr[l]]*tmpy[i_act_tr[l]]+ tmpz[i_act_tr[l]]*tmpz[i_act_tr[l]])				+help1[i]*tmpz[i_act_tr[l]];

					_time = time_help >= 0. ? (float)(sqrt(time_help+t2)-t1) : t21help;

					/* Interpolation between the timesamples to improve the accuracy of the migration-stack*/
					time_sample_float = (_time/bi_head.sample_rate) + timezerohelp; /* sample nr in input data */
					time_sample = (long) time_sample_float;
					time_sample_residual = time_sample_float - (float)time_sample;
					
					if (time_sample < no_pts_help)
					{
						if (_time == 0.0)
							weighting = (float)1;
						else
						{
							/* Including topography effect and variable exponent for the radiation-cos*/
							scal_help = pow(vel05*(double)_time, scal_exp);
							partsum = (double)(tr_head[i_act_tr[l]].normx * tmpx[i_act_tr[l]])+(double)(tr_head[i_act_tr[l]].normy * tmpy[i_act_tr[l]])+(double)(zparthelp[i] * tr_head[i_act_tr[l]].normz);
							weighting = partsum >= 0 ? pow(partsum, (double)phi_scal )/(weighthelp * scal_help) : 0.;
						}

						/* Applying the interpolate version of the migration-stack */
						data_sum[k][i] += (float)(((data[i_act_tr[l]][time_sample] * (1.0 - time_sample_residual)) +  data[i_act_tr[l]][time_sample + 1] * time_sample_residual) * weighting);
					}
					

				}/* end loop through input traces */
				

					/* final scaling   */
				data_sum[k][i] *= scalvector[i];		
				/* Pass the data back into Matlab */
				migdata[i+k*bi_head_out.no_pts]=(float) data_sum[k][i];		
			}
		}
        /* Set the trace header values */
        tr_head_grid[k].z = z_coor1[k];
		tr_head_grid[k].no_pts = bi_head_out.no_pts;
        tr_head_grid[k].timewindow = bi_head_out.timewindow;
	}
/*..................... FREE THE ALLOCATED MEMORY ............................*/
mxFree(spur);
mxFree(tr_head);
mxFree(tr_head_grid);
mxFree(z_coor);
mxFree((char*) (data[0]));
mxFree((char*) (data));
mxFree((char*) (data_sum[0]));
mxFree((char*) (data_sum));
mxFree((char*) (velocity_a[0]));
mxFree((char*) (velocity_a));

mxFree(tmpx);
mxFree(tmpy);
mxFree(tmpz);
mxFree(i_act_tr);
mxFree(time0vector);
mxFree(time0sep);
mxFree(time0_2);
mxFree(scalvector);
mxFree(help1);
mxFree(zparthelp);

   /*timer */
time(&time_relative);
zeit_angabe_sekunden = difftime(time_relative, time_start);
mexPrintf("------------------Comments:------------------------\n");
mexPrintf("Elapsed time: %f h\n",zeit_angabe_sekunden/3600);
mexPrintf("You should apply a topographic correction at this point!\n");
mexPrintf("------------------Migration terminated-------------------------\n");

/* Write the file header values */
value = mxCreateDoubleScalar((long) *mxGetPr(mxGetField(prhs[1],0,"RecMonth")));
mxSetField(plhs[1], 0,"RecMonth",value);
value = mxCreateDoubleScalar((long) *mxGetPr(mxGetField(prhs[1],0,"RecDay")));
mxSetField(plhs[1], 0,"RecDay",value);
value = mxCreateDoubleScalar((long) *mxGetPr(mxGetField(prhs[1],0,"RecYear")));
mxSetField(plhs[1], 0,"RecYear",value);
value = mxCreateDoubleScalar((long) *mxGetPr(mxGetField(prhs[1],0,"StepSizeUsed")));
mxSetField(plhs[1], 0,"StepSizeUsed",value);
value = mxCreateDoubleScalar((long) *mxGetPr(mxGetField(prhs[1],0,"NominalFrequency")));
mxSetField(plhs[1], 0,"NominalFrequency",value);
value = mxCreateDoubleScalar((long) *mxGetPr(mxGetField(prhs[1],0,"AntennaSeparation")));
mxSetField(plhs[1], 0,"AntennaSeparation",value);
value = mxCreateDoubleScalar((long) *mxGetPr(mxGetField(prhs[1],0,"NumberOfStacks")));
mxSetField(plhs[1], 0,"NumberOfStacks",value);
value = mxCreateDoubleScalar((long) *mxGetPr(mxGetField(prhs[1],0,"NumberOfTracesInLine")));
mxSetField(plhs[1], 0,"NumberOfTracesInLine",value);
value = mxCreateDoubleScalar((long) *mxGetPr(mxGetField(prhs[1],0,"NumberOfTracesCrossLine")));
mxSetField(plhs[1], 0,"NumberOfTracesCrossLine",value);
value = mxCreateDoubleScalar(bi_head_out.no_tr);
mxSetField(plhs[1], 0,"NumberOfTraces",value);
value = mxCreateDoubleScalar(bi_head_out.no_pts);
mxSetField(plhs[1], 0,"NumberOfSamples",value);
value = mxCreateDoubleScalar(bi_head_out.timezero_pt);
mxSetField(plhs[1], 0,"ZeroTimeSample",value);
value = mxCreateDoubleScalar(bi_head_out.timewindow);
mxSetField(plhs[1], 0,"TotalTimeWindow",value);
value = mxCreateDoubleScalar(bi_head_out.voltage);
mxSetField(plhs[1], 0,"PulserVoltage",value);


return;
}
