function [content] = ReadPulsEkko(ekko_file,gps_ind)
% ntr,nsamp,sra,prx,py,pz,freq,offset,data,tt
% Function to read in Puls Ekko Files
% Example: ReadPulsEkko('DLINE55')
%gps_ind is either 
% 1: data are in the PulsEkko gps file
% 2: data are in a time stamp sorted ascii file

% file name definititons
head_file=[ekko_file '.HD'];
dat_file=[ekko_file '.DT1'];
%if Ekko GPS file used (i.e. gps_ind = 1) overwrite variable gps_file
if nargin > 1 
    if (gps_ind == 1) 
      gps_file=[ekko_file '.GPS'];
    else 
    % if GPS ascii file is used, choose the appropriate file ....    
      gps_fname='*';  
      [gps_fname,gps_pname]=uigetfile({'*';'*'},'Choose GPS ascii file ...',gps_fname);  
      gps_file = [gps_pname,gps_fname];
    end;  

    %to get time stamp in seconds of the day
    fid = fopen(sprintf('%s',gps_file),'rt');
    tt = textscan(fid,'$GPGGA,%2d%2d%f%s','CommentStyle','Trace');
    fclose(fid);
    %conversion in seconds of the day
    tt = double(tt{1}) * 3600 + double(tt{2}) * 60 + double(tt{3});
end;

%Do not include file extension (e.g. ekko_file = 'D_LINE08')
%sdate is the date that the data were collected
%The 21 header words in *.hd file are stored in the structure called 'h'
%For example, h(7).data gives the step size used.
%Additional comments in the *.hd file are stored in the 'comments'
%structure
%header is a structure containing header information  (32 header words) for each trace
%For example, header(65).data(2) gives the position of the 65th trace in the
%file
%section is the time sample x trace matrix of GPR data
%x contains the profile positions
%z contains the corresponding elevtaion coordinate (if available).

% -------- Read the header file and extract fields --------
fid_h=fopen(head_file,'r');

if fid_h==-1
    fprintf('ERROR: Could not read header file\n')
    return
end

%check header lines
while ~feof(fid_h)
    hline=fgets(fid_h)
    if strfind(hline,'PRO') continue, end;
    date_ind=strfind(hline,'/');
    if isempty(date_ind), date_ind=strfind(hline,'-'); end;
    if isempty(date_ind)~=1
        sdate=sscanf(hline, '%s');
        break
    end
end

%bring sdate into format yyyy-mm-dd
check = ~isempty(strfind(sdate,'/'));
if check>0, sdate = [sdate(7:10),'-',sdate(1:2),'-',sdate(4:5)]; end;
posLine = strfind(sdate,'-');
month=sdate(posLine(1)+1:posLine(2)-1);
[~, status] = str2num(month);
if status==0
    switch month
        case 'Jan'
            month='01';
        case 'Feb'
            month='02';
        case 'Mar'
            month='03';
        case 'Apr'
            month='04';
        case 'May'
            month='05';
        case 'Jun'
            month='06';
        case 'Jul'
            month='07';
        case 'Aug'
            month='08';
        case 'Sep'
            month='09';
        case 'Oct'
            month='10';
        case 'Nov'
            month='11';
        case 'Dec'
            month='12';
        otherwise
            error(['Month ' month ' unknown.']);
    end
    sdate=[sdate(1:posLine(1)) month sdate(posLine(2):end)];
end

%read rest of header file and check for keywords for multi channel
%recording
i=0;k=0;
comments = '';
multi = 0;
while ~feof(fid_h)
    line=fgets(fid_h);
    eq_pos=strfind(line,'='); %Find the header data fields (to the right of the = sign)
    if isempty(eq_pos)==0
        i=i+1;
        h(i).data=line(eq_pos+1:length(line)); %Put all header data fields into a structure
    else
       check = findstr(line, 'ulti'); 
       if length(check)>0, multi=1; end 
       if length(line)>2; %Comments are attached
        k=k+1;
        comments(k).data=line;
       end 
    end 
end


fclose(fid_h);

fprintf('Input Ekko Filename: %s\n', ekko_file);
fprintf('Number of traces in file: %g\n', str2num(h(1).data));
fprintf('Sample interval: %g ns\n', str2num(h(4).data)/str2num(h(2).data));
fprintf('Number of samples: %g \n', str2num(h(2).data));
fprintf('Trace window : %g ns\n', str2num(h(4).data));
fprintf('Nominal antenna frequency : %g MHz\n', str2num(h(9).data));
fprintf('Antenna separation: %g m\n', str2num(h(10).data));
fprintf('Step size: %g m\n', str2num(h(7).data));
if multi>0,
  fprintf('Multi Component Measurement: Yes\n\n');
else
  fprintf('Multi Component Measurement: No\n\n');
end  




% -------- Read the trace headers and traces --------
fid_d=fopen(dat_file,'rb');

if fid_d==-1
    fprintf('ERROR: Could not read dt1 file\n')
    return
end
section=zeros(str2num(h(2).data),str2num(h(1).data)); % matrix(number of samples, number of traces)
x=zeros(1,str2num(h(1).data));
z=zeros(1,str2num(h(1).data));
tra=0;
while tra<str2num(h(1).data)
    tra=tra+1;
    header(tra).data=fread(fid_d,[1,32],'float32');
    x(tra)=header(tra).data(2);
    z(tra)=header(tra).data(4);
    record=fread(fid_d,[1,str2num(h(2).data)],'int16');
    section(1:str2num(h(2).data),tra)=record';
end


ntr = str2num(h(1).data);
nsamp = str2num(h(2).data);
sra = str2num(h(4).data)/nsamp;
prx = (0:ntr-1)';
pry = zeros(size(prx));
prz = zeros(size(prx));
freq = str2num(h(9).data);
offset = str2num(h(10).data);
data = section;

content.sdate = sdate;
content.ntr = ntr;
content.nsamp = nsamp;
content.sra = sra;
content.prx = prx;
content.pry = pry;
content.prz = prz;
content.freq = freq;
content.offset = offset;
content.data = data;
content.multi = multi;
if (nargin > 1)
    content.tt = tt;
end;

fclose(fid_d);



