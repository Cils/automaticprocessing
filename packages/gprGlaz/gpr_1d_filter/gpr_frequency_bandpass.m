function [gprdat n] = gpr_frequency_bandpass(indata, para, n)
%function gprdat = gpr_frequency_bandpass(indata,para)
% frequency bandpass filter with corner frequencies para.corner_frequencies

if(~isfield(para,'ntaper')), para.ntaper = 11; end
if(~isfield(para,'output')), para.output = 0; end

[nsamp ntrace]=size(indata.data);
nf=2^ceil(log2(nsamp));
fdatag=zeros(nf,ntrace);
fdatag(1:nsamp,:)=indata.data;
fdatag=fft(fdatag,nf);
df=1/(nf*indata.sampling_rate*1e-9);
f=(0:(nf-1))*df;

bfs=round(para.corner_frequencies(1)*1e6/df)+1;
efs=round(para.corner_frequencies(2)*1e6/df)+1;
ntap2=(para.ntaper-1)/2;
if bfs<ntap2-1
    error(['Taper is too long, should be less then ' num2str(2*bfs-1)])
end


taper='cosine';
if taper=='cosine' ;
    tap=(pi).*(0:(para.ntaper-1))/(para.ntaper-1);
    itap=transpose((cos(tap).'+1)/2);
    tap=fliplr(itap);
elseif taper=='linear';
    ntap2=(para.ntaper-1)/2;
    tap=(0:para.ntaper-1)/8;
    itap=fliplr(tap);
end
fdatagf=fdatag;
for itrace=1:ntrace
    fdatagf(bfs-ntap2:bfs+ntap2,itrace)=transpose(tap).*fdatag(bfs-ntap2:bfs+ntap2,itrace);
    fdatagf(nf-bfs-ntap2:nf-bfs+ntap2,itrace)=transpose(itap).*fdatag(nf-bfs-ntap2:nf-bfs+ntap2,itrace);
end
fdatagf(1:bfs-ntap2,:)=0;
fdatagf(efs+ntap2:nf-efs-ntap2,:)=0;
fdatagf(nf-bfs+ntap2:nf,:)=0;
for itrace=1:ntrace
    fdatagf(efs-ntap2:efs+ntap2,itrace)=transpose(itap).*fdatag(efs-ntap2:efs+ntap2,itrace);
    fdatagf(nf-efs-ntap2:nf-efs+ntap2,itrace)=transpose(tap).*fdatag(nf-efs-ntap2:nf-efs+ntap2,itrace);
end
idatagf=real(ifft(fdatagf,nf));

gprdat = indata;
gprdat.data = idatagf(1:nsamp,:);

if para.output==1
    
    mfdatagf = mean(abs(fdatagf(:,min(~isnan(fdatagf)))),2);
    mfdatag  = mean(abs(fdatag(:,min(~isnan(fdatagf)))),2);
    figure;
    hold on
    plot(f(1:end/2)/1000000, mfdatag(1:end/2));
    plot(f(1:end/2)/1000000, mfdatagf(1:end/2),'r');
    xlabel('Frequency (MHz)')
    
end

if nargin > 2
    n = n +1;
end
  