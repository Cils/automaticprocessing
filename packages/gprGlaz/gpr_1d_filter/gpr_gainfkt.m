function [gprdat n] = gpr_gainfkt(indata, para, n)

gprdat = indata;

a = para.lin_gain; %linear gain
b = para.exp_gain; %exponent
% v = 0.1;
start_time = para.timewindow(1); %in time samples
end_time   = para.timewindow(2); %in time samples
% t = start_time:size(data_shift,1);
t = start_time:end_time;
for ntraces = 1:size(indata.data,2)
    gf(1:start_time-1,1)       = a*(start_time)+exp(b*(start_time));
    gf(start_time:end_time,1)  = a*(t)+exp(b*(t));
    gf(end_time:indata.nr_of_samples,1)         = a*(end_time)+exp(b*(end_time));
    gprdat.data(:,ntraces)= gf.*indata.data(:,ntraces);
end


if nargin > 2
    n = n +1;
end

end