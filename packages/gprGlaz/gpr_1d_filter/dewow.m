function dewow(maxfn,minfn,pfolder,outfolder,prefix)
% Program to dewow many files
% Use: dewow(maxfn,[minfn],[pfolder],[outfolder],[prefix])
% maxfn: number of last file
% minfn (optional): number of first file. If not given, minfn=0
% pfolder (optional): temporary processing folder (must be DOS compatible)
% outfolder (optional): relative to input folder or absolut
% prefix (optional): name trunk of the files (default: "LINE")
% - Files to be processed should be named e.g. "LINE00.DT1"
% - The output files will be called e.g. "DLINE00.DT1"
% - The program DEWOW.EXE (part of the EKKO_TOOLS) should be in the same
%   folder as this script
% - To execute this script, you either need to
%       - have writing permissions to "c:\ekko42\raw", or
%       - give a third argument which is a folder you have writing
%         permissions on. This folder name must be DOS compatible (no
%         folder names longer than 8 characters, no spaces, ...)
% written by Joseph Doetsch, 20090203


if(nargin<1), error('Sorry, no input parameters'); end;
if(nargin<2), minfn = 0; end;
if(nargin<3), pfolder = 'c:\ekko42\raw\'; end;
if(nargin<4), outfolder = pwd; end
if(nargin<5), prefix='LINE'; end
if(pfolder(end)~='\'), pfolder(end+1)='\'; end
if(~exist(pfolder,'file')), mkdir(pfolder); end;
if(~exist(outfolder,'file')), mkdir(outfolder); end;
if(~mkdir([pfolder 'test']))
    error('Sorry, you do not have writing permissions in the processing folder.\n Please give a different folder as third argument to the function!');
else
    rmdir([pfolder 'test']);
end
    

dew = which('dewow');
dew(end:end+2) = 'exe';
if(~exist(dew,'file'))
    error('Sorry, the program DEWOW.EXE cannot be located.');
end
ffolder = pwd;


for fn = minfn:maxfn
    disp(sprintf('Processing file %i, last file is nr %i.',fn,maxfn));
    if(fn<10)
        fname = sprintf('%s0%i.DT1',prefix,fn);
    else
        fname = sprintf('%s%i.DT1',prefix,fn);
    end
    copyfile([fname(1:end-3) '*'],pfolder);
    cd(pfolder)
    unix(sprintf('%s %s>tmp',dew,fname));        
    cd(outfolder)
    copyfile([pfolder fname],sprintf('D%s',fname))
    copyfile([pfolder fname(1:end-3) 'HD'],sprintf('D%sHD',fname(1:end-3)))
    delete([pfolder fname(1:end-3) '*']);
    cd(ffolder)
end


