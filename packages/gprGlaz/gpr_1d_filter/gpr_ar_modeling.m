function [yf,yb] = gpr_ar_modeling(x,lf,mu);
%AR_MODELING: autoregressive modeling of 1D spatial data
%
%  IN    x:   data 
%        lf:  length of the operator
%        mu:  pre-whitening in %
%      
%  OUT   yf:  prediction of the data using forward AR modeling
%        yb:  prediction of the data using backward AR modeling
% 
%  Copyright (C) 2008, Signal Analysis and Imaging Group
%  For more information: http://www-geo.phys.ualberta.ca/saig/SeismicLab
%  Author: M.D.Sacchi
%
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published
%  by the Free Software Foundation, either version 3 of the License, or
%  any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details: http://www.gnu.org/licenses/
%



   nx = length(x);

% backward ar-modeling

   y  = x(1:nx-lf,1);
   C  = x(2:nx-lf+1,1);
   R  = x(nx-lf+1:nx,1);
   M = hankel(C,R);

   B = M'*M;  beta = B(1,1)*mu/100;
   ab = (B + beta*eye(lf))\M'*y;
   temp = M*ab;
   temp = [temp;zeros(lf,1)];
   yb = temp;


   y  = x(lf+1:nx,1);
   C  = x(lf:nx-1,1);
   R = flipud(x(1:lf,1));
   M = toeplitz(C,R);


   B = M'*M;  beta = B(1,1)*mu/100;

   af = (B + beta*eye(lf))\M'*y;
   temp = M*af;
   temp = [zeros(lf,1);temp];
   yf = temp;
   
return