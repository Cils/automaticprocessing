function [gprdat nn] = gpr_spikeRemove(indat,para, nn)


% spike edit
% uses a sliding gate to measure the average amplitude around each sample,
% the sample is excluded from the measurement. The ratio of the sample
% absolute amplitude to the average absolute in the gate is computed. If
% this ratio is larger than the specified threshold, the sample is
% clasified as spike and is linearly interpolated from the adjacent
% samples.
%
%sra: sampling rate
%fl:  filter length [time]
%thr: threshold
% optional: t0: spike editing starts after t0

% call:
% gprflw(n).name = 'spike_removal';
% gprflw(n).para.filter_length = 100;
% gprflw(n).para.threshold = 4.5;
% gprflw(n).para.t0 = 0;
% gprflw(n).para.output = 0; % 1 for graphical output
% 
% [gprdat(n) n] = gpr_gpr_spikeRemove(gprdat(n-1), gprflw(n).para, n);

gprdat = indat;

sra = indat.sampling_rate;
fl = para.filter_length;
thr = para.threshold;
if isfield(para,'t0')
    t0 = para.t0;
else
    t0 = 0;
end

ntr = indat.nr_of_traces;
ns =  indat.nr_of_samples;

nst0 = round(t0/sra);
nsf = round(fl/sra);
data= indat.data;

if mod(nsf,2)==0; nsf = nsf-1; end %uneven filter length
nsf2 = (nsf-1)/2;
h = waitbar(0,'removing spikes...');
nrem=0;
for kk = 1:ntr
    for ii = 3:nsf2
        if ii<nst0;
            continue;
        else
            meanamp = mean(abs([data(1:ii-1,kk);data(ii+1:ii+nsf2,kk)]));
            if (abs(data(ii,kk))/meanamp) > thr;
                data(ii,kk) = (data(ii-1,kk)+data(ii+1,kk))/2;
                nrem = nrem +1;
                spike(nrem,:)=[ii kk];
            end
        end
    end
    
    for ii = nsf2+1:ns-nsf2
        meanamp = mean(abs([data(ii-nsf2:ii-1,kk);data(ii+1:ii+nsf2,kk)]));
        if (abs(data(ii,kk))/meanamp) > thr;
            data(ii,kk) = (data(ii-1,kk)+data(ii+1,kk))/2;
            nrem = nrem +1;
            spike(nrem,:)=[ii kk];
        end
    end
    
    for ii = ns-nsf2+1:ns-1
        meanamp = mean(abs([data(ii-nsf2:ii-1,kk);data(ii+1:ns,kk)]));
        if (abs(data(ii,kk))/meanamp) > thr;
            data(ii,kk) = (data(ii-1,kk)+data(ii+1,kk))/2;
            nrem = nrem +1;
            spike(nrem,:)=[ii kk];
        end
    end
    waitbar(kk/ntr)
    
end

fprintf('# of removed spikes: %d\n',nrem)

if para.output == 1
    load rdwhitblu
    figure
    subplot(121)
    imagesc(1:ntr,1:ns,indat.data)
    colormap(red_white_blue)
%     shading flat
    hold on
    xlabel('trace no')
    ylabel('sample no')
    plot(spike(:,2),spike(:,1),'ro')
    subplot(122)
    imagesc(1:ntr,1:ns,data)
    colormap(red_white_blue)
%     shading flat
    hold on
    xlabel('trace no')
    ylabel('sample no')
    plot(spike(:,2),spike(:,1),'ro')
close(h)

gprdat.data = data;

if nargin > 2
    nn = nn +1;
end
end