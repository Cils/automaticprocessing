function [gprdat n] = gpr_cohfilt(indat,para, n)
% coherency filter

gprdat = indat;

ntr = indat.nr_of_traces;
dc = indat.data;
for a = 2:ntr-1
   dc(:,a) = 0.5 * (0.5*indat.data(:,a-1) + indat.data(:,a) + 0.5*indat.data(:,a+1));
end;

maxval      = max(abs(dc(1:end,:)));
gprdat.data = dc./repmat(maxval, size(dc, 1), 1);

if nargin > 2
    n = n +1;
end
