function [data,HP,LP,sra,t0,ns0,GainPoints] = rmGSSIgain(infile)
% gets gain function from file
% linear interpolation between gain points to get gain function
% removes median from data
% divide data by gain function 

[t0 HP LP GainPoints] = ReadGssiGain([infile,'.TXT']);

[rh,data]=Readgssi2([infile,'.DZT']);

gloc = linspace(0,rh.nsamp,length(GainPoints));
sra = rh.range/rh.nsamp;
gainfkt = interp1(gloc,10.^(GainPoints./20),1:rh.nsamp,'linear','extrap');
ns0 = round(-t0/sra);


gainmat = repmat(gainfkt',1,size(data,2));
datamedian = median(data,1);
data = data- repmat(datamedian,rh.nsamp,1); 
datasc = data./gainmat;


load('blkwhtred.mat')

figure
subplot(211)
hold on
plot(data./max(max(abs(data))),'b')
plot(gainfkt./max(gainfkt),'r-')
title('original data (normalised)')
xlim([1 rh.nsamp])
ylim([-1.1 1.1])
% legend('mean trace (normalised)','gain fct (normalised)')
% plot(1:rh.nsamp,gainfkt,'k-')

subplot(212)

imagesc(data)
colormap(blkwhtred)

figure
hold on
plot(gloc,10.^(GainPoints./20),'ro')
plot(1:rh.nsamp,gainfkt,'b-')
legend('Gain Points',...
    '10^{Gain Points/20}','location','best')


figure
title('scaled data')
subplot(211)
hold on
plot(datasc./max(mean(abs(datasc),2)),'b')
% legend('mean trace (normalised)','gain fct (normalised)')
xlim([1 rh.nsamp])
ylim([-1.1 1.1])
plot(ns0,0,'ko')

subplot(212)
imagesc(datasc)
colormap(blkwhtred)

figure
datasvd = msvdfilt(data,100);
imagesc(datasvd)
colormap(blkwhtred)
