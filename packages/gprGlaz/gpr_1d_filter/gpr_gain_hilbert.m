function data_new = gpr_gain_hilbert(data,filter_length,max_scale)

data_env = zeros(size(data));
for i=1:size(data,2);
    data_env(:,i) = abs(hilbert(data(:,i)));
    data_env(:,i) = smooth(data_env(:,i),filter_length);
end
scale_f     = max(max(data_env));
env_norm    = data_env./scale_f;
a = env_norm<(1/max_scale);
env_norm(a) = 1/max_scale;
data_new    = data./env_norm;

