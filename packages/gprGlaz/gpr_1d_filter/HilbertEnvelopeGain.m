function [datait,procp] = HilbertEnvelopeGain(datait,ntri,ttii,t2,sra,nsampt,procp)
% Choose the start poin and length of the hilbert window with the arrow
% keys. Press enter when finished.

dist = (0:ntri-1)*procp.dri;
figure(1); clf

button = 0;
while(button ~= 27) %button 27 = escape
    if (button == 28); procp.hilbwin = procp.hilbwin-200; end %left arrow
    if (button == 29); procp.hilbwin = procp.hilbwin+200; end %right arrow
    if (button == 30); procp.hilbfirst = procp.hilbfirst-200; end %down
    if (button == 31); procp.hilbfirst = procp.hilbfirst+200; end %up
    if (button == 1); [xx,yy,button] = ginput(1); end
    
    nsamph = round(procp.hilbwin/sra);
    ifirst = round(procp.hilbfirst/sra);
    itti = round(ttii/sra);
    env = zeros(nsamph,1);
    for a = 1:ntri
        hd = hilbert(datait(itti(a)+1+ifirst:itti(a)+nsamph+ifirst,a));
        env = env + sqrt(hd.*conj(hd));
    end
    env = env/ntri;
    th = (0:nsamph-1)*sra;
    cc = polyfit(th,log(env)',1);
    
    clear dataitH
    for a = 1:nsampt
        dataitH(a,:) = datait(a,:)/exp(polyval(cc,t2(a)));
    end

    imagesc(dist,t2,dataitH); hold on
    plot(dist,ttii,'k');
    plot(dist,ttii+procp.hilbfirst,'b');
    plot(dist,ttii+procp.hilbfirst+procp.hilbwin,'b'); hold off
    colormap(procp.cmap);
    cc = mean(mean(abs(dataitH)));
    caxis([-cc*2 cc*2]);
    xlabel('Distance [m]');
    ylabel('TWT [ns]');
    
    disp('--------- Choose ---------');
    disp('Later Hilbfirst:      Down');
    disp('Earlier Hilbfirst:	Up');
    disp('Longer Twin:          Right');
    disp('Shorter Twin:         Left');
    disp('---------------------------')
    [xx,yy,button] = ginput(1);
end
disp('--------- Hilbert Parameters ---------');
disp(sprintf('Hilbfirst:\t%d',procp.hilbfirst))
disp(sprintf('Hilbwin:\t%d',procp.hilbwin))

datait = dataitH;
end