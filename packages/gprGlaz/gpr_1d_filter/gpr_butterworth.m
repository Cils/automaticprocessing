function [gprdat n] = gpr_butterworth(indata, para, n)

procp = para;

fdata = sum(abs(fft(indata.data))');
f = linspace(0,1/(2*indata.sampling_rate),indata.nr_of_samples/2) * 1000;
nf = length(f);
load blkwhtred

if(isfield(indata,'fig'))
    if(indata.fig==1)
        fig = figure; clf;
        plot(f,fdata(1:nf),'b');
        hold on;
        xlim([0 500])
        xlabel('Frequency [MHz]','fontsize',12);
        ylabel('Amplitude','fontsize',12);
        title('Amplitude Spectrum','fontsize',12);
    end
end
 
t = (0:indata.nr_of_samples-1)*indata.sampling_rate;

if (procp.fl & procp.fh > 0)
    nyq = 0.5 / indata.sampling_rate * 1000;
    fl = procp.fl/nyq;
    fh = procp.fh/nyq;
    [bb,aa] = butter(procp.ford,[fl fh]);
    data = filter(bb,aa,indata.data);
    fdata = sum(abs(fft(data))');

if(isfield(indata,'fig'))
    if(indata.fig==1)
        plot(f,fdata(1:nf),'r');
        legend('Before Butterworth Bandpass Filter','After Butterworth Bandpass Filter');
        hold off
    end
end
% fig = figure; clf;
%       imagesc(1:indata.nr_of_traces,t,data);
%       colormap(blkwhtred)
%       xlabel('Trace number','fontsize',12);
%       ylabel('Time [ns]','fontsize',12);
%       set(gca,'fontsize',12);
%       title('After Butterworth Bandpass Filter');
%       xlim([0 500])
end   

gprdat = indata;
gprdat.data = data;

if nargin > 2
    n = n +1;
end