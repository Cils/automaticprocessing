function [gprdat n] = gpr_cutTime(indat,para, n)
% function gprdat = gpr_cutTime(indat,para)
% cut timeseries 

maxsamples           = round(para.timecut/indat.sampling_rate);
gprdat               = indat;
gprdat.data          = indat.data(1:maxsamples,:);
gprdat.nr_of_samples = size(gprdat.data,1);
gprdat.time_window   = gprdat.nr_of_samples*gprdat.sampling_rate;

if nargin > 2
    n = n +1;
end