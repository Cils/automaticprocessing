function [gprdat n] = gpr_subaverage(indata, para, n)

gprdat = indata;
timerange = para.timewindow(1):para.timewindow(2);
for ntrace = 1:indata.nr_of_traces
    tracerange = max(1,ntrace-para.tracewindow/2)+1:min(size(indata.data,2), ntrace+para.tracewindow/2);
    tracerange(max(isnan(indata.data(:,tracerange)))) = [];
    gprdat.data(timerange,ntrace) = gprdat.data(timerange,ntrace) - mean(indata.data(timerange,tracerange),2);
end

if nargin > 2
    n = n +1;
end