function [gprdat n] = gpr_gaingagc( indata, params ,n)
gprdat = indata;
%
%   GAGC  : Apply Automatic Gain Control to the traces of the GPR section
%          "d" by scaling the amplitude of the data at the centre of a
%          Gaussian bell tapered sliding window with respect to the RMS
%          amplitude of the window. 
%
%   Usage : dagc  = gaingagc( d, dt )
%
%  Inputs :   dt,  sampling interval 
%              d,  the input GPR section
%
%  Output : dagc,  the aplified section
%
% Credits : This function was transcoded and modified from the SU function
%           do_gagc appearing in program sugain. Credits to CWP, Jack K.
%           Cohen, Brian Sumner, and Dave Hale 
%
% Author  : Andreas Tzanis,
%           Department of Geophysics, 
%           University of Athens
%           atzanis@geol.uoa.gr
%           (C) 2005, by Andreas Tzanis, all right reserved.
%
% modified by hrm: No interactive input error is estimated over the initial
% noiselen milliseconds
% modified by Kaspar Merz to be used with GPRmatlab

[ns, ntr] = size(indata.data);
dagc     = zeros(ns,ntr);

indata.data(isnan(indata.data))=0;
% ask       = inputdlg({'Define noise level (e.g. 5.0e-7)' ...
%     'Give AGC window length in ns'},...
%     'Gaussian taper AGC ',1);
% if isempty(ask), 
%     dagc = [];
%     return; 
% end;
% EPS = sqrt(abs(log(str2num(ask{1}))));
% if ~EPS,
%     EPS   = 3.8090232;
% end
% wagc      = str2num(ask{2});                       % agc window in ns
% if ~wagc,
%     errordlg('Wrong data specification','GAGC : ERROR');
%     uiwait
%     dagc = [];
%     return
% end

iwagc     = floor(params.agcwindow/indata.sampling_rate);                        % agc window in samples
EPS = 3.809;
% Compute Gaussian window weights 
w   = zeros(iwagc,1);
u   = EPS/iwagc;
u2  = u*u;
for i=1:iwagc
    w(i) = exp(-(u2*i*i));
end
d2 = zeros(ns,1);                    % Initialize sum of squares 
s  = zeros(ns,1);                    % Initialize weighted sum of squares


h = waitbar(0,'Gain is applied ...');
% loop over all traces
for itr = 1:ntr
    tr = indata.data(:,itr);                    % Current trace to process
    agcdata = zeros(ns,1);            % work array for agc'ed data 
% agc itr'th trace
    for i = 1:ns
        val     = tr(i);
        d2(i) = val * val;
        s(i)  = d2(i);
    end
    for j = 1:iwagc-1;
        for i = j:ns
            s(i) = s(i) +( w(j)*d2(i-j+1));
        end
        k = ns - j;
        for i = 1:k
            s(i) = s(i) +( w(j)*d2(i+j));
        end
    end
    for i = 1:ns
        if ~s(i),
            agcdata(i) = 0.0;
        else
            agcdata(i) = tr(i)/sqrt(s(i));
        end
    end
    gprdat.data(:,itr) = agcdata;
    waitbar(itr/ntr,h);
end                                    % itr loop over traces
close(h);

if nargin > 2
    n = n +1;
end 
