function [p,params,procp,proc_steps] = set_tzero(procp,proc_steps,params,p)

tax = 500;
% graphical interface to enter processing parameter
if (proc_steps.tzerv > 0), 
 prompt = {'Lower limit for time axis (only display)(ns):'};
 dlg_title = 'Input paramter for Time Zero Pickwindow';
 num_lines = 1;
 defaultans = {sprintf('%s',num2str(tax))};
 answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
 tax = str2num(answer{1});
end    

%name of time zero pick file
pick_file = sprintf('%s%szero.mat',params.matdata,params.tag);

%determine time
t = (0:params.nsamp-1)*params.sra;

%why not just data/max(data)?
params.data = datanormalize(params.data);

clf;
imagesc(1:params.ntr,t,params.data);
%caxis(procp.cax);
caxis([-5 5])
colormap(procp.cmap);
v = axis;
axis([v(1) v(2) 0 tax]);
title('Set Time Zero');

[nn,tt] = getline(gca); % select a polyline in the figure
[nn,nidx] = sort(nn);
tt = tt(nidx);
nidx = find(diff(nn) > 1.0e-4);
nn = nn([nidx;nidx(end)+1]);
tt = tt([nidx;nidx(end)+1]);
params.tti = interp1(nn,tt,1:params.ntr,'linear','extrap');
hold on;
plot(1:params.ntr,params.tti,'k'); 

%number of samples to cut at the beginning
cut_samp = floor(max(params.tti)/params.sra);
params.nsamp = params.nsamp - cut_samp;
params.data = params.data(cut_samp:end,:);

%save(pick_file,'-struct','params', 'tti', 'px', 'py', 'pz');

proc_steps.tzerd = 1;
if (proc_steps.tzers == 1), p = p+1; end; 
if (proc_steps.tzers > 0), handle_segy_write(p,params,procp,proc_steps); end;

close all
