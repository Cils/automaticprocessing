function [gprdat nn] = gpr_svdfilt(indata, para, nn)
% single value decomposition filter
% para.number of eigenvalues that is set to zero

gprdat = indata;




    [u,s,v] = svd(indata.data);
dv = zeros(size(indata.data));

if para.output==1
    plot(diag(s))
end

if ~isfield(para,'trwin') || para.trwin==0

    for ii = 1:length(para.n)
        s(ii,ii)=0;
    end
    dv = u*s*v';
else
    trwin2 = floor(para.trwin/2);
    h = waitbar(0,'moving SVD filter') ;
    for ii = 1:para.trwin
        
        [u,s,v] = svd(indata.data(:,1:ii+trwin2));
        s(1:para.n,1:para.n) = 0;
        k = u*s*v';
        dv(:,ii) = k(:,ii);
        waitbar(ii/indata.nr_of_traces)
    end;
    for ii = trwin2+1:indata.nr_of_traces-trwin2
        [u,s,v] = svd(indata.data(:,ii-trwin2:ii+trwin2));
        s(1:para.n,1:para.n) = 0;
        k = u*s*v';
        dv(:,ii) = k(:,trwin2+1);
        waitbar(ii/indata.nr_of_traces)
    end
    for ii = indata.nr_of_traces-trwin2+1:indata.nr_of_traces
        
        [u,s,v] = svd(indata.data(:,ii-trwin2:indata.nr_of_traces));
        s(1:para.n,1:para.n) = 0;
        k = u*s*v';
        dv(:,ii) = k(:,(ii-(indata.nr_of_traces-trwin2-1)));
        waitbar(ii/indata.nr_of_traces)
    end
    close(h)
end

maxval      = max(abs(dv(1:end,:)));
gprdat.data = dv./repmat(maxval, size(dv, 1), 1);

if nargin > 2
    nn = nn +1;
end