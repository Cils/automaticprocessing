function [gprdat n] = gpr_muteDirectWave(indata, para, n)

gprdat = indata;

dt    = indata.sampling_rate ;
ntr = indata.nr_of_traces;
ns = indata.nr_of_samples;
tt = (0:ns-1)*dt/1000;
xx = 1:ntr;
load blkwhtred
indata.x = sort(indata.x);

figure
imagesc(indata.x,tt,indata.data)
shading flat; shading interp;
colormap(blkwhtred)
axis tight ij
xlabel('x [m]')
ylabel('t [s]')

[xp, tp] = ginput(2);

mutemin = round(tp(1)/(dt/1000));
if mutemin<1; mutemin=1;end
mutemax = round(tp(2)/(dt/1000));

gprdat.data(mutemin:mutemax,:) = 0;
imagesc(indata.x,tt,gprdat.data)
if nargin > 2
    n = n +1;
end

end