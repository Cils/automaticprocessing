function dv = msvdfilt(d,nn)
%
n = length(d(1,:));
dv = zeros(size(d));
a = 1;
done = 0;
while(done == 0)
    n2 = a;
    n3 = a+nn-1;
    n1 = n2 - nn;
    n4 = n3 + nn;
    if (n1 < 1), n1 = 1; end;
    if (n3 >= n), n3 = n; done = 1; end;
    if (n4 > n), n4 = n; end;
    nneffidx = n3 - n2;
    dtmp = d(:,n1:n4);
   [u,s,v] = svd(dtmp);
   s(1,1) = 0;
%   s(2,2) = 0;
   dtmp = u*s*v';
    dv(:,n2:n3) = dtmp(:,(n2-n1+1):(n2-n1+1)+nneffidx);
    a = a + nn;
end;

