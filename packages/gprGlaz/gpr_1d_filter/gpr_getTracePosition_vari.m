
function [gprdat nn] = gpr_getTracePosition_vari(indata,para, nn)
% gpstypes: 
% 1: PE swiss / utm84 (lat/long) coordinates from .GPS file
% 2: PE swiss / utm84 (lat/long) coordinates - interpolation on reference gps-data
% 3: GSSI swiss coordinates from GEOSAT file
% 4: GPRMax coordinates
% 5: Ramac rtk_positions in .cor file, 
% 6: Ramac interpolation
% 7 :get position from header
gprdat = indata;

%% 1: PE swiss / utm84 (lat/long) coordinates from .GPS file

% open file, if type = 1
if para.gpstype == 1
    gpsdata=fopen([para.path para.gpsfile],'r');
    count1=1;
    
    %check for GPS-string
    line = fgets(gpsdata);
    line = fgets(gpsdata);
    a    = sscanf(line, '%s');
    frewind(gpsdata);
    
    % test if first 7 entities contain predefined string then use this routine
    if strcmp(a(1:6), '$GPGGA')
        
        % get line and scan line until end of file is reached
        while ~feof(gpsdata)
            line=fgetl(gpsdata);
            a = sscanf(line, '%s');
            
            % test if first 7 entities contain predefined string then write out coordinates
            if strcmp(a(1:6), '$GPGGA')
                
                %extact time stamp
                extract = (line(8:16));
                hour    = str2num(extract(1:2))+ para.timesft;
                minute  = str2num(extract(3:4));
                second  = str2num(extract(5:9));
                if isempty(line)==1
                    break
                end
                
                % is not empty, get swiss coordinates out of line (a)
                if (isempty(second) ||isempty(minute)||isempty(hour))
                    gtime(count1) = NaN;
                else
                    if length(line)>50
                        
                        pick = (line(18:65));
                                              % Swiss coordinates
                        %                     geast(count1) = str2double(pick(1:12));
                        %                     gnort(count1) = str2double(pick(16:28));
                        %                     gheit(count1) = str2double(pick(41:48));
                        
                        % UTM84 (lat/long coordinates)
                        lat_degree(count1) = (str2double(pick(1:2)));                   %Degrees
                        lat_minute(count1) = (str2double(pick(3:12)))/60;               %Decimal minutes
                        lat(count1) = lat_degree(count1) + lat_minute(count1);
                        
                        long_degree(count1) = (str2double(pick(16:18)));
                        long_minute(count1) = (str2double(pick(19:28)))/60;
                        long(count1) = long_degree(count1) + long_minute(count1);
                        
                        gheit(count1) = str2double(pick(41:48));
                        [gnort,geast] = jwgs2sgr(lat,long,gheit);
                        
                        for n = (1:length(geast))
                            if geast(n)<0
                                geast(n) = NaN;
                            end
                        end
                        for n = (1:length(gnort))
                            if gnort(n)<0
                                gnort(n) = NaN;
                            end
                        end
                    end
                end
                count1 = count1 + 1;
            end
        end
        
        figure;
        plot(geast, gnort, 'b.')
        legend('current line')
        
        
        % if other string contains the right position
    elseif strcmp(a(1:6), '$GNLLQ')
        
        % get line and scan line until end of file is reached
        while ~feof(gpsdata)
            line=fgets(gpsdata);
            a=sscanf(line, '%s');
            
            % get Trace number
            if strfind(a,'Trace')>=1 %| strcmp(a(1:5),'at_pos')
                gtrno = str2double(a(strfind(a,'#')+1:strfind(a,'at')-1));
                tline=fgets(gpsdata);
                if length(tline)<=50
                    count1 = count1 + 1;
                else
                    geast(count1) = str2double(tline(25:34));
                    gnort(count1) = str2double(tline(38:46));
                    gheit(count1) = str2double(tline(61:68));
                    gind(count1)  = gtrno;
                    
                    %                 % UTM84 (lat/long coordinates)
                    %                 pick = (line(18:65));                                           % has to ba adjusted to lat longstring!!!
                    %                 lat_degree(count1) = (str2double(pick(1:2)));                   %Degrees
                    %                 lat_minute(count1) = (str2double(pick(3:12)))/60;               %Decimal minutes
                    %                 lat(count1) = lat_degree(count1) + lat_minute(count1);
                    %
                    %                 long_degree(count1) = (str2double(pick(16:18)));
                    %                 long_minute(count1) = (str2double(pick(19:28)))/60;
                    %                 long(count1) = long_degree(count1) + long_minute(count1);
                    %
                    %                 gheit(count1) = str2double(pick(41:48));
                    %                 [gnort,geast] = jwgs2sgr(lat,long,gheit);
                    
                    count1 = count1 + 1;
                end
            end
        end
        geast = interp1(gind(find(geast)),geast(geast>0),1:gprdat.nr_of_traces,'linear','extrap');
        gnort = interp1(gind(find(gnort)),gnort(gnort>0),1:gprdat.nr_of_traces,'linear','extrap');
        gheit = interp1(gind(find(gheit)),gheit(gheit>0),1:gprdat.nr_of_traces,'linear','extrap');
        gheit = (smooth(gheit,0.1,'rloess'))';
        
        figure;
        plot(geast, gnort, 'b.')
        legend('current line')
    end




%% 2: PE swiss / utm84 (lat/long) coordinates - interpolation on reference gps-data

% open file, if type = 2
elseif para.gpstype == 2
    gpsdata=fopen([para.path para.gpsfile],'r');
    count1=1;
    
    %check for GPS-string
    line = fgets(gpsdata);
    line = fgets(gpsdata);
    a    = sscanf(line, '%s');
    frewind(gpsdata);
    
    % test if first 7 entities contain predefined string
    if strcmp(a(1:6),'$GPGGA')
        
        % get line and scan line until end of file is reached + count with trace number
        while ~feof(gpsdata)
            line=fgets(gpsdata);
            st = strfind(line,'#');
            en = strfind(line,'at');
            a=sscanf(line, '%s');
            count1 = str2double(line(st+1:en-1));
            
            extract = (line(8:16));
            hour    = str2num(extract(1:2))+ para.timesft;
            minute  = str2num(extract(3:4));
            second  = str2num(extract(5:9));
            if isempty(line)==1
                break
                
            else
                gtime(count1) = (hour*3600)+(minute*60)+second;%str2double(tline(8:16));
                geast(count1) = str2double(line(25:34));
                gnort(count1) = str2double(line(38:47));
                gheit(count1) = str2double(line(62:69));
                
                %                 % UTM84 (lat/long coordinates)
                %                 pick = (line(18:65));                                           % has to ba adjusted to lat longstring!!!
                %                 lat_degree(count1) = (str2double(pick(1:2)));                   %Degrees
                %                 lat_minute(count1) = (str2double(pick(3:12)))/60;               %Decimal minutes
                %                 lat(count1) = lat_degree(count1) + lat_minute(count1);
                %
                %                 long_degree(count1) = (str2double(pick(16:18)));
                %                 long_minute(count1) = (str2double(pick(19:28)))/60;
                %                 long(count1) = long_degree(count1) + long_minute(count1);
                %
                %                 gheit(count1) = str2double(pick(41:48));
                %                 [gnort,geast] = jwgs2sgr(lat,long,gheit);
                
                
            end
        end
        
        gtime = interp1(find(unique(gtime(isfinite(gtime)))),unique(gtime(isfinite(gtime)&gtime>0)),1:count1,'linear','extrap');
        
        % open referenced GPS-file to interpolate coordinates
        fid = fopen ([para.pospath para.posfile],'r');
        pdata = textscan(fid,'%f %f %f %s %s');
        %         pdata = textscan(fid, '%s %s %s %f %f %f %f', 'HeaderLInes', 1);
        %         pdata = textscan(fid,'%f %f %f %s %s', 'HeaderLines',1);
        fclose(fid);
        
        ptstring = cell2mat(pdata{:,5});
        for n = 1:size(ptstring,1)
            phour      = str2double(ptstring(n,1:2));
            pminute    = str2double(ptstring(n,4:5));
            psecond    = str2double(ptstring(n,7:8));
            ptime(n)  = (phour*3600)+(pminute*60)+psecond;
        end
        peast = pdata{:,1};
        pnort = pdata{:,2};
        pheit = pdata{:,3};
        
        geast = interp1(ptime, peast, gtime,'spline');
        gnort = interp1(ptime, pnort, gtime,'spline');
        gheit = interp1(ptime, pheit, gtime,'spline');
        
        figure;
        plot(peast, pnort, 'r.');
        hold on;
        plot(geast, gnort, 'b.')
        legend('all positions','current line')
        
    elseif strcmp(a(1:6), '$GNLLQ')
        while ~feof(gpsdata)
            line=fgets(gpsdata);
            a=sscanf(line, '%s');
            if strfind(a,'Trace')>=1 %| strcmp(a(1:5),'at_pos')
                gtrno = str2double(a(strfind(a,'#')+1:strfind(a,'at')-1));
                tline=fgets(gpsdata);
                if length(tline)<=50
                    count1 = count1 + 1;
                else
                    gtime(count1) = (str2double(tline(8:9))+ para.timesft)*3600 + str2double(tline(10:11))*60 + str2double(tline(12:16));
                    geast(count1) = str2double(tline(25:34));
                    gnort(count1) = str2double(tline(38:46));
                    gheit(count1) = str2double(tline(61:68));
                    gind(count1)  = gtrno;
                    
                    %                 % UTM84 (lat/long coordinates)
                    %                 pick = (line(18:65));                                           % has to ba adjusted to lat longstring!!!
                    %                 lat_degree(count1) = (str2double(pick(1:2)));                   %Degrees
                    %                 lat_minute(count1) = (str2double(pick(3:12)))/60;               %Decimal minutes
                    %                 lat(count1) = lat_degree(count1) + lat_minute(count1);
                    %
                    %                 long_degree(count1) = (str2double(pick(16:18)));
                    %                 long_minute(count1) = (str2double(pick(19:28)))/60;
                    %                 long(count1) = long_degree(count1) + long_minute(count1);
                    %
                    %                 gheit(count1) = str2double(pick(41:48));
                    %                 [gnort,geast] = jwgs2sgr(lat,long,gheit);
                    
                    count1 = count1 + 1;
                end
            end
        end
        geast = interp1(gind(find(geast)),geast(geast>0),1:gprdat.nr_of_traces,'linear','extrap');
        gnort = interp1(gind(find(gnort)),gnort(gnort>0),1:gprdat.nr_of_traces,'linear','extrap');
        gheit = interp1(gind(find(gheit)),gheit(gheit>0),1:gprdat.nr_of_traces,'linear','extrap');
        gheit = (smooth(gheit,0.1,'rloess'))';
    
    % open referenced GPS-file to interpolate coordinates
    fid = fopen ([para.pospath para.posfile],'r');
    pdata = textscan(fid,'%f %f %f %s %s');
    %         pdata = textscan(fid, '%s %s %s %f %f %f %f', 'HeaderLInes', 1);
    %         pdata = textscan(fid,'%f %f %f %s %s', 'HeaderLines',1);
    fclose(fid);
    
    ptstring = cell2mat(pdata{:,5});
    for n = 1:size(ptstring,1)
        phour      = str2double(ptstring(n,1:2));
        pminute    = str2double(ptstring(n,4:5));
        psecond    = str2double(ptstring(n,7:8));
        ptime(n)  = (phour*3600)+(pminute*60)+psecond;
    end
    peast = pdata{:,1};
    pnort = pdata{:,2};
    pheit = pdata{:,3};
    
    geast = interp1(ptime, peast, gtime,'spline');
    gnort = interp1(ptime, pnort, gtime,'spline');
    gheit = interp1(ptime, pheit, gtime,'spline');
    
    figure;
    plot(peast, pnort, 'r.');
    hold on;
    plot(geast, gnort, 'b.')
    legend('all positions','current line')

    end
%%  3: GSSI swiss coordinates from GEOSAT file (preprocessed, removal of trace number)

elseif para.gpstype == 3
    gpsdata=fopen([para.path para.gpsfile],'r');
    count1=1;
    
    while ~feof(gpsdata)
        line=fgetl(gpsdata);
        
        if isempty(line)==1
            break
        end
        
        geast(count1) = str2double(line(1:10));
        gnort(count1) = str2double(line(12:20));
        gheit(count1) = str2double(line(22:29));
        
        %           UTM84 (lat/long coordinates
        %         lat(count1) = (str2double(line(1:12)));
        %         long(count1) = (str2double(line(14:24)));
        %         gheit(count1) = str2double(line(26:33));
        %         [gnort,geast] = jwgs2sgr(lat,long,gheit);
        
        count1 = count1 + 1;
    end
    
    figure;
    plot(geast, gnort, 'b.')
    legend('current line')


%% 4: Read in GPR Max coordinates for model

elseif para.gpstype == 4
    geast = gprdat.x;
    gnort = gprdat.y;
    gheit = gprdat.z;
    
    figure;
    plot(geast, gnort, 'b.')
    legend('current line')
    

%% Ramac data
elseif para.gpstype == 5
    fid = fopen([para.path para.gpsfile],'r');
    C = textscan(fid,'%f %*s %s %f %*s %f %*s %f %*s %*f');
    fclose(fid);
    ind    = C{1};
    gtime   = C{2};
    glat    = C{3};
    glon    = C{4};
    gheith = C{5};
    
    tt = datevec(gtime);
    tt(:,1:3) = zeros(size(tt,1),3);
    tt = datenum(tt);
    atime = interp1(ind,tt,1:gprdat.nr_of_traces,'linear','extrap')';
    
    geast = interp1(tt,glat,atime);
    gnort = interp1(tt,glon,atime);
    gheit = interp1(tt,gheith,atime);
    

elseif para.gpstype == 6
    fid = fopen([para.path para.gpsfile],'r');
    C = textscan(fid,'%f %*s %s %f %*s %f %*s %f %*s %*f');
    fclose(fid);
    gind    = C{1};
    gtime   = C{2};
    clear C
    
    fid2 = fopen([para.pospath para.posfile],'r');
    C= textscan(fid, '%f, %f, %f, %f\n');
    fclose(fid2);
    ptime = C{1};
    peast = C{2};
    pnort = C{3};
    pheit = C{4};
    clear C
    

    tt = datevec(gtime);
    tt(:,1:3) = zeros(size(tt,1),3);
    tt = datenum(tt);
    atime = interp1(gind,tt,1:gprdat.nr_of_traces,'linear','extrap')';
    
    tind = find((ptime >= min(atime)) & (ptime <= max(atime)));
    
    geast = interp1(ptime(tind),peast(tind),atime,'linear','extrap')';
    gnort = interp1(ptime(tind),pnort(tind),atime,'linear','extrap')';
    gheit = interp1(ptime(tind),pheit(tind),atime,'linear','extrap')';
    figure;
    plot(peast, pnort, 'r.');
    hold on;
    plot(geast, gnort, 'b.')
    legend('all positions','current line')
      
elseif para.gpstype == 7
    geast = gprdat.headers(2,:);
    gnort = zeros(size(geast));
    gheit = zeros(size(geast));
else
    disp('Positions could not be determined, dummy coordinates inserted')
    geast = 1:gprdat.nr_of_traces;
    gnort = zeros(size(geast));
    gheit = zeros(size(geast));
end

%% output

gprdat.x = geast;
gprdat.y = gnort;
gprdat.z = gheit;



if nargin > 2
    nn = nn +1;
end

end