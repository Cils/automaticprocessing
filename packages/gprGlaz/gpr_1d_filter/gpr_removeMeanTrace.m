function [gprdat n] = gpr_removeMeanTrace(indat,para, n)
% subtract mean trace 

gprdat               = indat;
data = indat.data;
data(isnan(data)) =0;


% %gprdat.data          = indat.data-repmat(mean(data,2),1,indat.nr_of_traces);
% gprdat.data          = indat.data-repmat(median(data,2),1,indat.nr_of_traces);

%% takes the mean/median of every row(?!)
ntr = indat.nr_of_traces;
sumtr = median(data,2);
for b = 1:ntr
%     data(:,b) = data(:,b) - sumtr; 
    data(:,b) = data(:,b) - median(data(:,b));
end;

gprdat.data = data;

%%
if nargin > 2
    n = n +1;
end