function [tti] = gpr_SegyPickSurface(indat,para, n)

gprdat  = indat;
ntr     = indat.nr_of_traces;
sra     = indat.sampling_rate;
nsamp   = indat.nr_of_samples;
data    = indat.data;
tax     = para.tax;

load ('rdwhitblu.mat')
procp.cmap = red_white_blue;


t = (0:nsamp-1)*sra;
data = gpr_datanormalize(data);

clf;
imagesc(1:ntr,t,data);
%caxis(procp.cax);
caxis([-5 5])
colormap(procp.cmap);
v = axis;
axis([v(1) v(2) 0 tax]);
title('Pick Surface Reflection');

[nn,tt] = getline(gca); % select a polyline in the figure
[nn,nidx] = sort(nn);
tt = tt(nidx);
nidx = find(diff(nn) > 1.0e-4);
nn = nn(nidx);
tt = tt(nidx);
set(gcf,'WindowButtonMotionFcn','');
tti = interp1(nn,tt,1:ntr,'linear','extrap');
hold on;
plot(1:ntr,tti,'k');

%gprdat.tti = tti;

if nargin > 2
    n = n +1;
end