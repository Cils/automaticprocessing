function [gprdat n] = gpr_histeq(indat,para, n)
% coherency filter

gprdat = indat;

ntr = indat.nr_of_traces;
ns = indat.nr_of_samples;

nbin = para.n;
dc = floor((indat.data+1)*(nbin-1)/2);

np = ntr*ns;
linpix = reshape(dc,np,1);
nb = hist(linpix,nbin);
nb = nb/np;
csum = cumsum(nb);
dataeq = zeros(ns,ntr);
for ii = 1:ns
    for jj = 1:ntr
        dataeq(ii,jj) = (nbin-1)*csum(dc(ii,jj)+1);
    end
end
gprdat.data = dataeq/(nbin-1)*2-1;

if nargin > 2
    n = n +1;
end
