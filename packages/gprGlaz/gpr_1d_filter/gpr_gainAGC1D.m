function [gprdat n] = gpr_gainAGC1D(indata, para, n)

gprdat = indata;

h =  waitbar(0,'AGC is applied...');

for ii = 1:indata.nr_of_traces
    
    jj = para.timewindow(1);
    samplerange = max(jj-para.agcwindow/2, 1):min(jj+para.agcwindow/2,indata.nr_of_samples);
    for jj = 1:para.timewindow(1)
        gprdat.data(jj,ii) = indata.data(jj,ii)/mean(abs(indata.data(samplerange,ii)));
    end
    
    for jj = para.timewindow(1):min(para.timewindow(2), indata.nr_of_samples)
        samplerange = max(jj-para.agcwindow/2, 1):min(jj+para.agcwindow/2,indata.nr_of_samples);
        gprdat.data(jj,ii) = indata.data(jj,ii)/mean(abs(indata.data(samplerange,ii)));
    end
    
    waitbar(ii/indata.nr_of_traces)
%     disp(ii)
end

close(h)
if nargin > 2
    n = n +1;
end

end