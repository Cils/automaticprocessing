function [gprdat n] = gpr_moveZeroTime(indat,para, n)
% function gprdat = gpr_setZeroTime(indat,para)
% move zero time by para.timeshift

shift_samples = round(para.timeshift/indat.sampling_rate);
gprdat = indat;
gprdat.data = indat.data(shift_samples:end,:);
gprdat.nr_of_samples = size(gprdat.data,1);
gprdat.time_window = gprdat.nr_of_samples*gprdat.sampling_rate;

if nargin > 2
    n = n +1;
end