function [gprdat n] = gpr_gainInvEnv2D(indata, para, n)

gprdat = indata;
sra = indata.sampling_rate;
t = (0:indata.nr_of_samples-1)*sra;
s0 = round(para.timewindow(1)/sra);
s1 = round(para.timewindow(2)/sra);
switch 1
    case 1
%         figure
        for ii = 1:indata.nr_of_traces
            
            tracerange = max(1,ii-para.tracewindow/2)+1:min(size(indata.data,2), ii+para.tracewindow/2);
            tracerange(max(isnan(indata.data(:,tracerange)))) = [];
            hd         = hilbert(indata.data(:,tracerange));
            env        = sum(sqrt(hd.*conj(hd)), 2)./length(tracerange);
            
            cc = polyfit(t(s0:s1), log(env(s0:s1))', 1);
            
            samplerange = 1:s0-1;
            gprdat.data(samplerange, ii) = indata.data(samplerange, ii)./exp(polyval(cc, t(samplerange(end))));
            
            samplerange = s0+1:s1;
            gprdat.data(samplerange, ii) = indata.data(samplerange, ii)./exp(polyval(cc, t(samplerange)))';
            
            samplerange = s1+1:size(indata.data,1);
            gprdat.data(samplerange, ii) = indata.data(samplerange, ii)./exp(polyval(cc, t(s1)));
%             plot(t,log(env),'b',t(s0:s1),polyval(cc, t(s0:s1)),'r',...
%                 t(s0:s1),1./polyval(cc, t(s0:s1)),'g',t,gprdat.data(:,ii),'k')
        end
        
%     case 2
%         tracerange = 1:indata.nr_of_traces;
%         hd         = hilbert(indata.data(:,tracerange));
%         env        = sum(sqrt(hd.*conj(hd)), 2)./length(tracerange);
%         
%         cc = polyfit(para.timewindow(1):para.timewindow(2), log(env(para.timewindow(1):para.timewindow(2)))', 1);
%         
%         for ii = 1:indata.nr_of_traces
%         samplerange = 1:para.timewindow(1)7st;
%         gprdat.data(samplerange, ii) = indata.data(samplerange, ii)*0.2./exp(polyval(cc, samplerange(end)*indata.sampling_rate)).';
%         
%         samplerange = para.timewindow(1)+1:para.timewindow(2);
%         gprdat.data(samplerange, ii) = indata.data(samplerange, ii)./exp(polyval(cc, samplerange*indata.sampling_rate)).';
%         
%         samplerange = para.timewindow(2)+1:size(indata.data,1);
%         gprdat.data(samplerange, ii) = indata.data(samplerange, ii)./exp(polyval(cc, samplerange(1)*indata.sampling_rate)).';
%         end
%         
%     case 3
%         for ii = 1:indata.nr_of_traces
%             hd         = hilbert(indata.data(:,ii));
%             env        = sum(sqrt(hd.*conj(hd)), 2);
%             gprdat.data(:,ii) = indata.data(:,ii)./env;
%         end
end
% trnumber = round(linspace(1,ntr,floor(ntr/trint)+1));
% figure
% hold on
% hd = hilbert(data);
% %env = zeros(ns,1);
% for ii = 2:length(trnumber)
%     env = sum(sqrt(hd(:,trnumber(ii-1)+1:trnumber(ii)).*...
%         conj(hd(:,trnumber(ii-1)+1:trnumber(ii)))),2)./(trnumber(ii)-trnumber(ii-1)+1);
%     % for a = 1:ntr
%     %     env = env + sqrt(hd(:,a).*conj(hd(:,a)));
%     % end;
%     % env = env/ntr;
%
%     cc = polyfit(t(s1:s2),log(env(s1:s2))',1);
%
%     for a = 1:s2
%         data(a,trnumber(ii-1)+1:trnumber(ii)) = data(a,trnumber(ii-1)+1:trnumber(ii))/exp(polyval(cc,t(a)));
%     end;
%
%     for a = s2+1:ns
%         data(a,trnumber(ii-1)+1:trnumber(ii)) = data(a,trnumber(ii-1)+1:trnumber(ii))/exp(polyval(cc,t(s2)));
%     end
%
%
%     plot(1:ns,log(env'),'b',s1:s2,polyval(cc,t(s1:s2)),'g',s1:s2,1./polyval(cc,t(s1:s2)),'r')
% end
if nargin > 2
    n = n +1;
end


end