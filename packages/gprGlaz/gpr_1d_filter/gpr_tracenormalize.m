function [gprdat nn] = gpr_tracenormalize(indata, nn)

gprdat      = indata;
maxval      = max(abs(indata.data(1:end,:)));
gprdat.data = indata.data./repmat(maxval, size(indata.data, 1), 1);
gprdat.data(isnan(gprdat.data)) = 0;
if nargin > 1
    nn = nn +1;
end