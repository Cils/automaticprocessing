function da = agc(d,agclen,sra);

% d = input data
% da = output data
% agclen = length of the time window over which the gain is computed
% sra = sampling interval
%
% Author: Achille Capelli, SLF
% % email: capelli@slf.ch 
% November 2019; Last revision: 

agclen = round(agclen/sra);
da = d;
nsamp = length(d(:,1));
agc2 = round(0.5*agclen);

for a = 1:agc2
   agcf = mean(abs(d(1:a+agc2,:))) + 0.0001;
   da(a,:) = da(a,:) ./ agcf;
end;

for a = agc2+1:nsamp-agc2
   agcf = mean(abs(d(a-agc2:a+agc2,:))) + 0.0001;
   da(a,:) = da(a,:) ./ agcf;
end;

for a = nsamp-agc2+1:nsamp
   agcf = mean(abs(d(a-agc2:end,:))) + 0.0001;
   da(a,:) = da(a,:) ./ agcf;
end;
