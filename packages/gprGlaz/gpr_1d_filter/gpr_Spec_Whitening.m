function [gprdat nn ] = gpr_Spec_Whitening( indata, para, nn )
%Spectral whitening of radargram
%   USAGE
%   para.alpha: whitening coefficient 0<=alpha<=1
%   para.bandpass : bandpass filtering after whitening
gprdat = indata;
alpha = para.alpha;
[ns ntr] = size(indata.data);
sra = indata.sampling_rate*1e-9;
data = indata.data;
% original spectrum
nyq = 1/2/sra;
f = linspace(0,nyq,ns/2);
nf = length(f);
forig = sum(abs (fft(data)),2);

if alpha < 0 || alpha > 1
    error('Whitening parameter must be between 0 and 1')
end

% loop over traces to whiten spectrum
% whitened spectrum
for ii = 1:ntr
    ftr = fft(data(:,ii));
    fwt(ftr~=0) = ftr(ftr~=0)./abs(ftr(ftr~=0)).^alpha;
    tmp = ifft(fwt);
    data(:,ii) = real(tmp(1:ns));
end

fwtall = sum(abs (fft(data)),2);

figure
plot(f,forig(1:nf)./max(forig),'b'); hold on;
plot(f,fwtall(1:nf)./max(fwtall),'g');
legend('original','whitened')
xlabel('frequency [Hz]');
ylabel('amplitude')
% bandpass filter + bp spectrum
if isfield(para,'bandpass')
    if length(para.bandpass)<2
        disp('bandpass filter needs min / max freq')
    end
    
    
    fl = para.bandpass(1)/nyq;
    fh = para.bandpass(2)/nyq;
    [bb,aa] = butter(4,[fl fh]);
    data = filter(bb,aa,data);
    fbp = sum(abs (fft(data)),2);
    
    plot(f,fbp(1:nf)./max(fbp),'r');
    legend('original (nomalised)','whitened','after bandpass')
end

end

