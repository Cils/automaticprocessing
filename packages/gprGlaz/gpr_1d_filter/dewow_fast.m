function [DataProc,medMat] = dewow_fast(DataProc,dcrem,dewow,highf)
% [DataProc,medMat] = cs_dewow_fast(DataProc,dcrem,dewow,highf)
%
% Remove (1) Constant DC shift based on median value of pre-first-arrival
% sampels and/or (2) remove "wow" by subtracting a running median filter 
% (de-wow follwing Gerlitz et al., 1993).
%
% DataProc      Input/output data structure (Needed: DataProc.Data, DataProc.dt)
% dcrem         if 1: remove DC shift
% dewow         if 1: de-wow (OBS: needs highf)
% highf         Highest wow frequency (for dewow == 1)
%
% CS-2012-05-07
%
% OBS: Non-robust: Memory requirement around 2*data size!!!



%% === Parameters & Checks ===

nfirst = size(DataProc.Data,1);

if dewow == 1 && nargin < 4
    error('highf neede for dewow == 1!')
end



%% === Compute length of filter (Box-car) ===

if dewow == 1

nfilt = 2*(1/highf)*(1/DataProc.dt);
nfilt = (floor(nfilt/2)*2)+1;     % Ensure odd number
nfiltHalf = (nfilt-1)/2;


% - Check: too long filter doesn't make sense -
if nfilt >= nfirst
    disp(['Filter length is: ',num2str(nfilt)])
    disp(['Data length is: ',num2str(nfirst)])
    error('Filter in samples must be shorter than data!')
end


end


%% === Filter starting from lowest picked zero-time sample ===

if isfield(DataProc,'FATime'),
samplLow = min(DataProc.FATime);
else
samplLow = 1;
end;



%% === Remove DC based on pre-FB sample median ===

if dcrem == 1
    
    disp('Will remove DC component ...')
    
    dccomp = median(DataProc.Data(1:samplLow,:));   % Compute DC component
    dccomp = ones(nfirst,1)*dccomp;                 % Span matrix of data size
    DataProc.Data = DataProc.Data - dccomp;         % Subtract
    
    clear dccomp                                    % Delete to free memory
    
end



%% === De-wow ===


if dewow == 1
    
    disp(['De-wow: Filter length is: ',num2str(nfilt),' samples'])
    
    % --- Pad with symmetric part at beginning and end ---
    % --- As medfilt2 option 'symmetric' ---
    
    A = DataProc.Data(samplLow:end,:);          % Extract data part to filter
    nA = size(A,1);                             % Size of part to filter
    
    medMat = A*0;                               % Allocate memory for median
    
    Atop = A(1:nfiltHalf,:);                    % Flipped top
    Abot = A(end-nfiltHalf+1:end,:);            % Flipped bottom
    
    A = [flipud(Atop); A; flipud(Abot)];        % Put together
    
    clear Atop Abot
    
    
    %% === Filter ===
    
    
    % --- (1) Compute running median ---
    
    % Loop: All signal samples...
    for tt = nfiltHalf+1:1:nA+nfiltHalf
        
        % Compute for each block the median value
        medMat(tt-nfiltHalf,:) = median(A(tt-nfiltHalf:tt+nfiltHalf,:));
        
    end
    
    
    
    % --- (2) Subtract from data ---
    
    DataProc.Data(samplLow:end,:) = DataProc.Data(samplLow:end,:) - medMat;
    
    medMat = [zeros(samplLow-1,size(medMat,2)); medMat];
    
    
end




return




