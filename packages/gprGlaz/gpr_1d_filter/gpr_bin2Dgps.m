function [gprdat nn] = gpr_bin2Dgps(indata, para, nn)

gprdat = indata;

if para.gpstype == 1 || para.gpstype ==2
    gpsdata=fopen([para.path para.gpsfile],'r');
    
    count1=1;
    count2=0;
    
    %check for GPS-string
    line = fgets(gpsdata);
    line = fgets(gpsdata);
    a    = sscanf(line, '%s');
    frewind(gpsdata);
    
    if strcmp(a(1:6), '$GPGGA')
        while ~feof(gpsdata)
            line=fgets(gpsdata);
            a=sscanf(line, '%s');
            %     a=[a, zeros(1,100)];
            if strfind(a,'Trace')>=1 %| strcmp(a(1:5),'at_pos')
                tline=fgets(gpsdata);
                if isempty(tline)==1
                    break
                end
                extract = (tline(8:16));
                hour    = str2num(extract(1:2))+ para.timesft;
                minute  = str2num(extract(3:4));
                second  = str2num(extract(5:9));
                if (isempty(second) ||isempty(minute)||isempty(hour))
                    gtime(count1) = NaN;
                else
                    gtime(count1) = (hour*3600)+(minute*60)+second;%str2double(tline(8:16));
                %             geast(count1) = str2double(tline(25:34));
                %             gnort(count1) = str2double(tline(38:47));
                %             gheit(count1) = str2double(tline(62:69));
                %             gprec(count1) = str2double(tline(56:60));
                end
                count1 = count1 + 1;
            end
        end
        gtime = interp1(find(unique(gtime(isfinite(gtime)))),unique(gtime(isfinite(gtime))),1:count1-1,'linear','extrap');
        
    elseif strcmp(a(1:6), '$GNLLQ')
        while ~feof(gpsdata)
            line=fgets(gpsdata);
            a=sscanf(line, '%s');
            %     a=[a, zeros(1,100)];
            if strfind(a,'Trace')>=1 %| strcmp(a(1:5),'at_pos')
                tline=fgets(gpsdata);
                if isempty(tline)==1
                    break
                end
                gtime(count1) = str2double(tline(8:9))*3600 + str2double(tline(10:11))*60 + str2double(tline(12:16));
                geast(count1) = str2double(tline(25:34));
                gnort(count1) = str2double(tline(38:47));
                gheit(count1) = str2double(tline(62:69));
                gprec(count1) = str2double(tline(56:60));
                
                count1 = count1 + 1;
            end
        end
    end
    
    if para.gpstype == 2
        fid = fopen ([para.path para.posfile],'r');
%         pdata = textscan(fid, '%s %s %s %f %f %f %f', 'HeaderLInes', 1);
        pdata = textscan(fid,'%f %f %f %s %s', 'HeaderLines',1);
        fclose(fid);
        ptstring = cell2mat(pdata{:,5});
        for n = 1:size(ptstring,1)
            hour      = str2double(ptstring(n,1:2));
            minute    = str2double(ptstring(n,4:5));
            second    = str2double(ptstring(n,7:8));
            ptime(n)  = (hour*3600)+(minute*60)+second;
        end
        peast = pdata{:,1};
        pnort = pdata{:,2};
        pheit = pdata{:,3};
%         pprec = pdata{:,7};
        geast = interp1(ptime, peast, gtime,'spline');
        gnort = interp1(ptime, pnort, gtime,'spline');
        gheit = interp1(ptime, pheit, gtime,'spline');
        %         gprec = interp1(ptime, pprec, gtime,'spline');
        figure; plot(peast, pnort, 'r.'); hold on; plot(geast, gnort, 'b.')
    end

end

% Ramac data
if para.gpstype == 3
    fid = fopen([para.path para.gpsfile],'r');
    C = textscan(fid,'%f %*s %s %f %*s %f %*s %f %*s %*f');
    fclose(fid);
    ind    = C{1};
    gtime   = C{2};
    glat    = C{3};
    glon    = C{4};
    gheith = C{5};
    
    tt = datevec(gtime);
    tt(:,1:3) = zeros(size(tt,1),3);
    tt = datenum(tt);
    atime = interp1(ind,tt,1:gprdat.nr_of_traces,'linear','extrap')';
    
    geast = interp1(tt,glat,atime);
    gnort = interp1(tt,glon,atime);
    gheit = interp1(tt,gheith,atime);
    
end

if para.gpstype == 4
    fid = fopen([para.path para.gpsfile],'r');
    C = textscan(fid,'%f %*s %s %f %*s %f %*s %f %*s %*f');
    fclose(fid);
    gind    = C{1};
    gtime   = C{2};
    clear C
    
    fid2 = fopen([para.path para.posfile],'r');
    C= textscan(fid, '%f, %f, %f, %f\n');
    fclose(fid2);
    ptime = C{1};
    peast = C{2};
    pnort = C{3};
    pheit = C{4};
    clear C
    

    tt = datevec(gtime);
    tt(:,1:3) = zeros(size(tt,1),3);
    tt = datenum(tt);
    atime = interp1(gind,tt,1:gprdat.nr_of_traces,'linear','extrap')';
    
    tind = find((ptime >= min(atime)) & (ptime <= max(atime)));
    
    geast = interp1(ptime(tind),peast(tind),atime,'linear','extrap');
    gnort = interp1(ptime(tind),pnort(tind),atime,'linear','extrap');
    gheit = interp1(ptime(tind),pheit(tind),atime,'linear','extrap');
    
    
  
end
%% delete outliers
% kickout = find(abs(gprec)>0.1);
% gtime(kickout) = [];
% geast(kickout) = [];
% gnort(kickout) = [];
% gheit(kickout) = [];
% indata.data(:,kickout) = [];

myfit = polyfit(geast, gnort, 1);
c     = gnort+geast/myfit(1);
xc    = (c -myfit(2))/(myfit(1)+1/myfit(1));
yc    = myfit(1)*xc + myfit(2);

% [y0 yi] = min(yc);

d    = sqrt((xc-xc(1)).^2 + (yc-yc(1)).^2);
d    = d - min(d);

gprdat.pos0    = [xc(1) yc(1); xc(end) yc(end)];
gprdat.profdir = atan(myfit(1));

disp(['min_d = ' num2str(min(d))])
disp(['max_d = ' num2str(max(d))])

dd = diff(d);
disp(['min_dd = ' num2str(min(dd))])
disp(['max_dd = ' num2str(max(dd))])

xbin0    = min(d):para.binsize:max(d);
[n,bin]  = histc(d, xbin0);
gprdat.x = xbin0(1:end-1);
gprdat.nr_of_traces = length(gprdat.x);

gprdat.data = zeros(size(indata.data,1), length(gprdat.x));
for n = 1:length(gprdat.x)
    inbin = find(bin==n);
    gprdat.data(:,n) = mean(indata.data(:,inbin(inbin<=indata.nr_of_traces)),2);
    tmpz(n)      = mean(gheit(inbin));
end

gprdat.z = interp1(gprdat.x(~isnan(tmpz)), tmpz(~isnan(tmpz)), gprdat.x, 'spline');

if nargin > 2
    nn = nn +1;
end