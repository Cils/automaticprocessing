% Pre- an Processing upGPR Data
% Wrapper file
% workflow to read in all raw data, process and save it
% Dylan Longridge, 5.4.2017
% Modified by A. Capelli, 2019

%% Tabula Rasa
clear all
close all
clc



% add project to path
addpath(genpath('N:\lawprae\LBI\Projects\101_upGPR\postprocessing-scripts\Matlab\automaticProcessing'))

%% Processing %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%88886
plotraw     = 0; % PLOT RAW DATA
staticcorr  = 0; % MANUAL STATIC CORRECTION
background  = 10; % BACKGROUND REMOVAL median = 1, svd gerade gerade = 2, svd steigung steigung = 3, svd gerade steigung = 4, svd steigung gerade = 5, svd steigung alles = 6, svd gerade alles = 7
dewow       = 0; % SUBTRACT MEAN (DEWOW)
freqspectra = 0; % PLOT FREQUENCY SPECTRA
butterbp1   = 1; % BUTTERWORTH BANDPASS FILTER
staticauto  = 1; % AUTOMATIC STATIC CORRECTION
butterbp2   = 0; % BUTTERWORTH BANDPASS FILTER
agcgain     = 0; % AGC GAIN FUNCTION
stacktraces = 1; % STACK TRACES
averagexy   = 0; % AVERAGE XY-FILTER

% -------------------------------------------------------------------------
% ------------------------- INPUT PARAMETERS ------------------------------
% -------------------------------------------------------------------------

meastime             = 40;   % Measurement time for nsamp [ns]
vlen                 = 10;   % Displaying time (max 40 ns)
picksurf             = 0;    % Use static correction picks (1) or not (0)

% -------------------------------------------------------------------------
% ------------------- DEFINE PROCESSING PARAMETERS ------------------------
% -------------------------------------------------------------------------
% colorbar
load zcm.mat                % blue-red colormap
load blkwhtred.mat          % black-white-red colormap
procp.cmap = blkwhtred;     % Colormap for displaying sections
procp.cax = [-1.0 1.0];     % Lower and upper bounds of colorbar

% electromagnetic wave speed in air,snow and ice 
procp.vair = 0.299;         % Air velocity
procp.vice = 0.1689;        % Ice velocity
procp.vsnow = 0.23;         % Snow velocity


procp.nmax=3640;             % Maximum number of data to use

procp.fl = 290;             % Low cut frequency for bandpass filter %210
procp.fh = 2500;            % High cut frequency for bandpass filter %2450
procp.threshold_amp = 0.07; % Threshold for the amplitude
procp.ford = 6;             % Order of the Butterworth bandpass filter

% static correction settings ()
procp.startTwt  = 4.8;      % Phase follower starting time [ns], staticauto  must be set 1. % 5.8 for svd3
startDate = 1;              % starting trace (set to 1)
%procp.twin = 3000;         % Length of trace time window
%procp.maxdepth = 250;      % Maximum depth [m] for displaying data

% SVD filter settings
procp.svd_len = 51;        % Length of SVD filter
procp.SVD_WinNumb=1.;        % Used to determine SVD window based on ram lenght.  svd_len=ramp_length/SVD_WinNumb
procp.svd_dWin=50;             % Running windows spacing in SVD filter

procp.bound_SVD = [20 200 400 700 1620 1645 1840 2130 3010 procp.nmax]; % Boundaries, where the upGPR moves up and down, used in option background=3
% bounds changes during season
procp.SVD_changebound=1;        % 1 if bounds changes during season
procp.SVD_bounds_dates=[181204.1730,190801];  % end of bounds period. Format:YYMMDD.hhmm
procp.SVD_bounds={[20 251 540 920 2000 2020 2220 2640 3630 procp.nmax]
                   procp.bound_SVD};
                                % bounds for different periods


%procp.decon_len = 50;      % fx-deconvolution filter length
procp.agclen = 10;          % Length of AGC filter (0 if no agc) (max 40ns)
procp.tord = 0;             % y-axis time (0) or depth (1)
procp.tax = 10;             % Lower limitation of the time axis for picking


% check settings parameters
procp.cheksettings=1;       % use function for checking bonds and startTwt
procp.justnew=1;            % check just new data  
static_check=1;
bkgr_filter=1;


% -------------------------------------------------------------------------
% ------------------- DEFINE data locations  ------------------------
% -------------------------------------------------------------------------

% project name
GPRproject_prjname = sprintf('wholeseason_SVD_bakgr%d_%d%d%d%d%d%d%d%d%d%d%d',background,plotraw,staticcorr,background,dewow,freqspectra,butterbp1,staticauto,butterbp2, agcgain, stacktraces,averagexy);
GPRproject_prjname_old = sprintf('wholeseason_driftrightandnewnewstaticcorr_background%d_%d%d%d%d%d%d%d%d%d%d%d',background,plotraw,staticcorr,background,dewow,freqspectra,butterbp1,staticauto,butterbp2, agcgain, stacktraces,averagexy);
%GPRproject_prjpath = 'N:\lawprae\LBI\Projects\101_upGPR\data\scan_info\2015-16\upGPR STR\missions\';
GPRproject_prjpath = 'X:\Radar\Radardata\2018-19\upGPR WFJ\missions\';
%GPRproject_prjpath = 'C:\Users\longridg\Desktop\testordner\';

% get list of missions
listingmissions = dir([GPRproject_prjpath '*.MIS']);
% use just part of missions
%  listingmissions =listingmissions([1,18,20,45,61,95]);
% listingmissions =listingmissions([1,95]);

% save path
GPRproject_save    = 'N:\lawprae\LBI\Projects\101_upGPR\postprocessing-scripts\Matlab\automaticProcessing\processed_data\upGPR_19\';
% save mat files
matdata = 'N:\lawprae\LBI\Projects\101_upGPR\postprocessing-scripts\Matlab\automaticProcessing\processed_data\upGPR_19\matdata\';


% list of missions to skip
exceptions={'181204AA.ZON', '181204_1756.ZON', '181204_1800.ZON'};

% process just part of season
partofseason=1;      % if =1 process just one part of season and add it to previously processed data
partofseason_bounds=[190505, 190615];  % start and end of processing period. Format:YYMMDD.hhmm


%% check processing settings for every mission

%load cell arrays containing information for single scan_info or create
%empty array
try 
    load([GPRproject_save 'scan_info.mat'])
    r=length(scan_info);
catch ME
    disp(ME.identifier)
    disp('Creating new scan_info!!')
    disp('Preprocessing without scan_info!!!!!!!!!')
    scan_info.names=[]; % scan name
%     scan_info.bound_SVD={}; % scan bound
%     scan_info.exception=[]; % scan r is not to be considered if scan_info(r).exception=1
%     scan_info.startTwt=[];
    r=0;  % index for scan_info
end

for aaa = 1:length(listingmissions)  % loop through days
       
    daypath = fullfile(GPRproject_prjpath, listingmissions(aaa).name,'\');
    listingzon = dir([daypath '*.ZON']);
%     disp(daypath(47:52));
    
    % loop through missions of one day 
%     for bbb = 1:length(listingzon)
    for bbb = 1:1  % check just first scan of day
        %         disp(['       ' listingzon(bbb).name])
        
        
        % check if season have different boundaries and set right bound
        if procp.SVD_changebound
            dati=str2double([listingzon(bbb).name(1:6) '.' listingzon(bbb).name(8:11)]); %
            for i=1:length(procp.SVD_bounds_dates)
                if dati< procp.SVD_bounds_dates(i)
                    bound_SVD=procp.SVD_bounds{i};
                    break
                end
            end
        end
              
        
        sessionname = listingzon(bbb).name(1:11);
        sessionpath = fullfile(daypath, listingzon(bbb).name, '\');
        upGPRsession = dir([sessionpath '*.dt']);
        
        %------------------------------------------------------------------
        % load information from scan_info and make a new one if not found
        %------------------------------------------------------------------
        try
            idx = find(ismember({scan_info.names}, listingzon(bbb).name(1:end-4)));
        catch
            idx=[];
        end
            
        if length(idx)==1   % scan is already in scan_info
            r=idx;
            bound_SVD=scan_info(r).bound_SVD;
            exc=scan_info(r).exception;
            procp.startTwt = scan_info(r).startTwt;
            scan_info(r).new=0;
            
        else                % scan isn't in scan_info. Creating new entry.
            if r>0
                r=length(scan_info)+1;
            else
                r=1
            end
            scan_info(r).names=listingzon(bbb).name(1:end-4);
            scan_info(r).bound_SVD= bound_SVD;
            scan_info(r).exception=0;
            scan_info(r).startTwt=procp.startTwt;
            scan_info(r).new=1;
        end
        
        % check if scan can be skipped
        cheksettings=procp.cheksettings;
        if (procp.justnew && ~scan_info(r).new)  
            cheksettings=0;
            disp(['skip:', scan_info(r).names])
        end
        
        if cheksettings
            % import gpr-data
            %----------------

            gprflw.para.path     = sessionpath;
            gprflw.para.datafile = upGPRsession;
            gprflw.name = 'import_IDS';
            gprdat.data = ReadIDS([gprflw(1).para.path gprflw(1).para.datafile.name]);
            gprdat.date =listingzon(bbb).name(1:end-4);
            % -------------------------------------------------------------------------
            % ------------------- Calculated Input Variables --------------------------
            % -------------------------------------------------------------------------
            [gprdat,yend,deltat,ylab,f,nf,viewlen,stepsize]=calsInpVar(gprdat,1,procp,meastime,vlen);        


            %------------------------------------------------------------------
            % call function plotting raw data and change settings if necessary
            %------------------------------------------------------------------
            [bound_SVD,exc,startTwt]=find_bounds(gprdat,procp,bound_SVD,bkgr_filter,static_check,1);


            %------------------------------------------------------------------
            % save settings for this scan
            %------------------------------------------------------------------
            %             r=r+1             
%             scan_info(r).names=listingzon(bbb).name(1:end-4);
            scan_info(r).bound_SVD= bound_SVD;
            scan_info(r).exception=exc;
            scan_info(r).startTwt=startTwt;
            procp.startTwt=startTwt;
        end
    end
end

% save data
save([GPRproject_save 'scan_info.mat'], 'scan_info')
disp('end')





%% -------------------------------------------
%%---- process data---------------------------
%%--------------------------------------------

tic
times=zeros(1,length(listingmissions));

% load previusly processed data if intend to reprocess just part of data
if partofseason
    load([GPRproject_save GPRproject_prjname '.mat']);
end

% counting = 1;

%load cell arrays containing information for single scan_info or create
%empty array
try 
    load([GPRproject_save 'scan_info.mat'])
    r=length(scan_info);
catch ME
    disp(ME.identifier)
    disp('Creating new scan_info!!')
    disp('Preprocessing without scan_info!!!!!!!!!')
    scan_info.names=[]; % scan name
%     scan_info.bound_SVD={}; % scan bound
%     scan_info.exception=[]; % scan r is not to be considered if scan_info(r).exception=1
%     scan_info.startTwt=[];
    r=0;  % index for scan_info
end


for aaa = 1:length(listingmissions)  % loop through days
    
    tStart = tic;
    
    daypath = fullfile(GPRproject_prjpath, listingmissions(aaa).name,'\');
    listingzon = dir([daypath '*.ZON']);
    disp(daypath(47:52))
    
    if length(listingzon) > 48 % measurement on day 121 and 128 (year 15/16) have more than 48 measurements. Kill too many traces.
        ccc = 48;
        disp([daypath,' had more than 48 measurements. The ones too many were killed.'])
    else
        ccc = length(listingzon);
    end
    
    % loop through missions of one day 
    for bbb = 1:ccc  

                
        % if intend to process just part of season check if mission is in period to process
        if partofseason
            dati=str2double([listingzon(bbb).name(1:6) '.' listingzon(bbb).name(8:11)]); %
            if not( (partofseason_bounds(1)< dati) & (dati< partofseason_bounds(2)) )
                disp([string(dati) 'was skipped!'])
                continue
            end
        end
        
        
        % check if season have different boundaries and set right bound
        if procp.SVD_changebound
            dati=str2double([listingzon(bbb).name(1:6) '.' listingzon(bbb).name(8:11)]); %
            for i=1:length(procp.SVD_bounds_dates)
                if dati< procp.SVD_bounds_dates(i)
                    procp.bound_SVD=procp.SVD_bounds{i};
                    break
                end
            end
        end
        
        
        
        disp(['       ' listingzon(bbb).name])
        
        sessionname = listingzon(bbb).name(1:11);
        sessionpath = fullfile(daypath, listingzon(bbb).name, '\');
        upGPRsession = dir([sessionpath '*.dt']);
        
        %------------------------------------------------------------------
        % load information from scan_info and make a new one if not found
        %------------------------------------------------------------------
        try
            idx = find(ismember({scan_info.names}, listingzon(bbb).name(1:end-4)));
        catch
            idx=[];
        end
            
        if length(idx)==1   % scan is already in scan_info
            r=idx;
            bound_SVD=scan_info(r).bound_SVD;
            procp.bound_SVD=bound_SVD;
            exc=scan_info(r).exception;
            procp.startTwt = scan_info(r).startTwt;
            
        else                % scan isn't in scan_info. Creating new entry.
            if r>0
                r=length(scan_info)+1;
            else
                r=1;
            end
            scan_info(r).names=listingzon(bbb).name(1:end-4);
            scan_info(r).bound_SVD= bound_SVD;
            scan_info(r).exception=0;
            scan_info(r).startTwt=procp.startTwt;
            scan_info(r).new=1;
        end
        
        % check if mission is in exceptionslist and skip if the case
        if ismember(listingzon(bbb).name,exceptions) | scan_info(r).exception
            disp(['       ' listingzon(bbb).name 'was skipped!'])
            continue;
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        filedate = sessionname;
        
        %% import gpr-data
        
        gprflw(1).para.path     = sessionpath;
        gprflw(1).para.datafile = upGPRsession;
        gprflw(1).name = 'import_IDS';
        gprdat(1).data = ReadIDS([gprflw(1).para.path gprflw(1).para.datafile.name]);
        
        n = 1; % counting pics
        k = 1; % counting data
        
        % cut data if to long
        if length(gprdat(k).data(1,:))>procp.nmax  % if too many samples are recorded, kill those traces
            l=length(gprdat(k).data(1,:));
            redtraces = gprdat(k).data(:,1:procp.nmax);
            gprdat(k).data = redtraces;
            disp(['Mission ' listingzon(bbb).name sprintf(' has %d traces. Traces higher than %d were killed.', l, procp.nmax)])
        end
        

        
        % -------------------------------------------------------------------------
        % ------------------- Calculated Input Variables --------------------------
        % -------------------------------------------------------------------------
        [gprdat,yend,deltat,ylab,f,nf,viewlen,stepsize]=calsInpVar(gprdat,1,procp,meastime,vlen);    
        
        k = k+1;
        
        %%
        % -------------------------------------------------------------------------
        % ------------------- BACKGROUND REMOVAL ----------------------------------
        % -------------------------------------------------------------------------
        if background > 0
            [ gprdat, k ] = backgroundrem( gprdat, k, background, procp );
        end
        
        %%
        % -------------------------------------------------------------------------
        % ------------------- BUTTERWORTH BANDPASS FILTER -------------------------
        % -------------------------------------------------------------------------
        
        if butterbp1 == 1
            if (procp.fl & procp.fh > 0)
                nyq = 0.5 / gprdat(k-1).sampling_rate * 1000;
                fl = procp.fl/nyq;
                fh = procp.fh/nyq;
                [bb,aa] = butter(procp.ford,[fl fh]);
                gprdat(k).data = filter(bb,aa,gprdat(k-1).data);
                gprdat = renamedata(gprdat,k);
                gprdat(k).fdata = sum(abs(fft(gprdat(k).data))');
                
                k = k+1;
            end
        end
        
        % -------------------------------------------------------------------------
        % ------------------- AUTOMATIC STATIC CORRECTION -------------------------
        % -------------------------------------------------------------------------
        
        if staticauto == 1 % follow the phase after a pick %
            a=1;
            
            if picksurf == 1    % If picks  already exist as .mat file
                
                pick_file = sprintf('%sSURFPICK_PROFILE_%d.mat', matdata,background);
                %pick_file = sprintf('%s%s_SURFPICK_PROFILE_%03d.mat',matdata,filedate,a); %let each session pick his his own surface..
                load(pick_file,'tti')
                
                
            else                % pick tti and save picks
                dates     = linspace(1,gprdat(k-1).nr_of_traces,gprdat(k-1).nr_of_traces);
                samples   = linspace(1,gprdat(k-1).nr_of_samples,gprdat(k-1).nr_of_samples);
                startTwt  = round(procp.startTwt/gprdat(k-1).sampling_rate);
                %newPickTwt = manualPick_FollowPhase2(gprdat(k-1).data, dates, samples, startDate, startTwt);
                [newPickTwt, newPickTwt_zero] = manualPick_FollowPhase2(gprdat(k-1).data, dates, samples, startDate, startTwt);
                
                newPickTwt_zero = newPickTwt_zero';
                lennewpick=size(newPickTwt,1);
                newPickTwt = newPickTwt_zero;
                
                
                
                
                
                %%%%111%%%% the starting date can be changed from one, if this is applied!
                for i = size(newPickTwt,1)+1:lennewpick
                    newPickTwt(i) = 0;
                end
                
                for i = 1:lennewpick
                    if newPickTwt(i) == 0% if any value is zero, take the last value
                        newPickTwt(i) = newPickTwt(i-1);
                    end
                end
                
                if startDate > 1
                    for i = 1:startDate-1
                        % add values at the same starting height as the first pick at
                        % the first few positions
                        newPickTwt2(i) = newPickTwt(1);
                    end
                    newPickTwt2(startDate:gprdat(k-1).nr_of_traces) = newPickTwt(:);
                else
                    newPickTwt2 = newPickTwt;
                end
                newPickTwt =newPickTwt2;
                %%%%111%%%%
                
                [tti_2] =(newPickTwt*gprdat(k-1).sampling_rate);
                [tti] = [tti_2]';
                %pick_file = sprintf('%sSURFPICK_PROFILE_2.mat', matdata);
                %pick_file = sprintf('%s%s_SURFPICK_PROFILE_%03d.mat',matdata,filedate,a);
                %save(pick_file);
            end
            
            %Drifting
            %tti = tti+driftavg_zero_TWT(counting); % Reason why a plus see Figure + and -
%             counting = counting +1;
            
            
            
            % cut traces at surface pick
            for a = 1:gprdat(k-1).nr_of_traces
                if (tti(a) > 0)
                    i1 = round(tti(a)/gprdat(k-1).sampling_rate);
                    gprdat(k).data(1:gprdat(k-1).nr_of_samples-i1+1,a) = gprdat(k-1).data(i1:end,a);
                else
                    gprdat(k).data(:,a) = gprdat(k-1).data(:,a);
                end
            end
            
            gprdat = renamedata(gprdat,k);
            gprdat(k).nr_of_samples = length(gprdat(k).data(:,1));
            viewlen = length(gprdat(k).data(:,1));
            
            gprdat(k).fdata = sum(abs(fft(gprdat(k).data))');
            
            k = k+1;
        end
        
        
        % -------------------------------------------------------------------------
        % ------------------- BUTTERWORTH BANDPASS FILTER -------------------------
        % -------------------------------------------------------------------------
        
        if butterbp2 == 1
            if (procp.fl & procp.fh > 0)
                nyq = 0.5 / gprdat(k-1).sampling_rate * 1000;
                fl = procp.fl/nyq;
                fh = procp.fh/nyq;
                [bb,aa] = butter(procp.ford,[fl fh]);
                gprdat(k).data = filter(bb,aa,gprdat(k-1).data);
                gprdat = renamedata(gprdat,k);
                gprdat(k).fdata = sum(abs(fft(gprdat(k).data))');
                
                
                k = k+1;
            end
        end
        
        % -------------------------------------------------------------------------
        % ---------------------- STACK TRACES -------------------------------------
        % -------------------------------------------------------------------------
        
        if stacktraces == 1
            for i = 1:gprdat(k-1).nr_of_samples
                stack(i) = sum(gprdat(k-1).data(i,:));
            end
            gprdat(k).data = stack';
            gprdat = renamedata(gprdat,k);
            gprdat(k).date = filedate;
            
            
            k = k+1;
        end 
        
        gprdatsession(aaa,bbb) = gprdat(k-1);
        
        
    end
    
    time=toc(tStart);
    disp(['Time for processing ',daypath(47:52),': ', time, ' s'])
    time
    times(aaa)=time;
end

save([GPRproject_save GPRproject_prjname '.mat'], 'gprdatsession');


toc
