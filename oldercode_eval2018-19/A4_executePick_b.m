
%% loads and constants
clear all
clc
tic

addpath(genpath('N:\lawprae\LBI\Projects\101_upGPR\postprocessing-scripts\Matlab\automaticProcessing'))

%% legend
% % HSCompl.values  = HS Complete
% % Radar.HS        = HS Manual pick
% % Surface.HS      = HS Autopick
%%
%choose colormap;
load blkwhtred.mat
%load zcm.mat
%load('R:\postprocessing-scripts\Matlab\data\general\colRamp_BlueYellow')
colRamp = blkwhtred;



waveSpeed=0.23; % (m/ns) 0.23
pick =  8; % choose pick
% 1 = pickOberflaecheMaxDiff
% 2 = pickOberflaecheMaxDiffIncremental
% 3 = pickOberflaecheMaxDiffIncrementalWithTaTssHs
% 4 = pickOberflaecheMaxDiffIncrementalWithTaTssHsVW
% 5 = pickOberflaecheMovingWindow
% 6 = pickOberflaechePhaseFollower
% 7 = pickOberflaecheManuell

% set datum of first pick
start_pick='05.12.2018 00:00' ;


% Settings of preprocessing (used to select right data file) 
background  = 10; % BACKGROUND REMOVAL
agcgain     = 1; % AGC GAIN FUNCTION
averagexy   = 0; % AVERAGE XY-FILTER

%% Get project

namenonumbers = 'wholeseason_SVD_background'; % Name of the project without numbers behind
% namenonumbers = 'wholeseason_driftrightandnewnewstaticcorr_background'; % Name of the project without numbers behind


GPRproject_prjname = sprintf('%s%d_%d%d%d',namenonumbers,background,background,agcgain,averagexy);
GPRproject_location = ['N:\lawprae\LBI\Projects\101_upGPR\postprocessing-scripts\Matlab\automaticProcessing\processed_data\upGPR_19\' GPRproject_prjname,'.mat'];



GPRproject_save = 'N:\lawprae\LBI\Projects\101_upGPR\postprocessing-scripts\Matlab\automaticProcessing\processed_data\upGPR_19\';


Sufacename_save = GPRproject_save;
Surfacename = 'Surface_autopick';

load(GPRproject_location) % load data from preprocessing

% load old surface pick
try
    load([Sufacename_save Surfacename '.mat']);
    Surface_old=Surface;
    Surface_old.label='old Pick';
    Surface_old.LS='b--';
catch ME
    Surface_old=[];
end


% load HS WFJ

WFJ_path='P:\SLdata\19WFJ_MST_flux.dat';
WFJ_path='P:\SLdata\19WFJ_TP.dat';
% WFJ_path='N:\lawprae\LBI\Projects\101_upGPR\postprocessing-scripts\Matlab\automaticProcessing\19WFJ_MST_flux.dat';

try
%     WFJ_surface=load_WFJ(WFJ_path,'MST_flux');
    WFJ_surface=load_WFJ(WFJ_path,'TP');
    WFJ_surface.label='HS WFJ';
    WFJ_surface.twt=Surface.HS/waveSpeed*2;
    WFJ_surface.LS='k';
catch ME
    WFJ_surface=[];
end



%% cut data to right height:
% Radar_old = Radar;   % Radar is the variable loaded from preprocessing
% clear Radar
% [ Radar ] = datacutter( Radar_old,37,length(Radar_old.samples(:,1)) )

%% algorithms
sampleTime=Radar.samples(end)/(length( Radar.samples)-1);

%Lino
[bla,iStart] = findNearest(Radar.dates,datenum( start_pick ,'dd.mm.yyyy HH:MM'));
start=1;
Radar.dates=Radar.dates(iStart:end);
Radar.radargramC1=Radar.radargramC1(:,iStart:end);

if pick == 1
[Surface.dates,Surface.twt]=pickOberflaecheMaxDiff(Radar.dates,Radar.radargramC1,sampleTime);
elseif pick == 2
[Surface.dates,Surface.twt]=pickOberflaecheMaxDiffIncremental(Radar.dates,Radar.radargramC1, sampleTime, 0.4);
elseif pick == 3
[Surface.dates,Surface.twt]=pickOberflaecheMaxDiffIncrementalWithTaTssHs(Radar.dates,Radar.radargramC1, sampleTime, TA, TSS, HS, start);
elseif pick == 4
[Surface.dates,Surface.twt]=pickOberflaecheMaxDiffIncrementalWithTaTssHsVw(Radar.dates,Radar.radargramC1, sampleTime, TA, TSS, HS, VW, 0.4);
elseif pick == 5
[Surface.dates,Surface.twt]=pickOberflaecheMovingWindow(Radar.dates,Radar.radargramC1, sampleTime);
elseif pick == 6
[Surface.dates,Surface.twt]=pickOberflaechePhaseFollower(Radar.dates,Radar.radargramC1, sampleTime);
elseif pick == 7
[Surface.dates,Surface.twt]=pickOberflaecheManuell(Radar, Radara, colRamp, WAN6, WFJ);
elseif pick == 8
%     WFJ.HS = HS;
%     WFJ.date= date_num;s
%     Surface_WFJ1617.date =Surface_WFJ1617.dates';
%     Surface_WFJ1617.HS = Surface_WFJ1617.HS*100;
%     [Surface.dates,Surface.twt]=pickOberflaecheManuell_AC(Radar, colRamp, waveSpeed);   %,Surface_WFJ1617,); 
    Radar.radargram = Radar.radargramC1;
    Radar.TWT = 1;
    Surface.twt = manualPick_PickReflector_AC(Radar,colRamp,waveSpeed,WFJ_surface,Surface_old);
    Surface.dates = Radar.dates;
end

% calculations
Surface.HS=Surface.twt*waveSpeed/2;
Surface.label=Surfacename;



%% plot figures

figure()
hold on
plot(Surface.dates,Surface.HS,'m')
plot(WFJ_surface.dates,WFJ_surface.HS,'k')
title('HS Autopick')
ylim([0,3.5])
xlabel('date')
ylabel('Depth [m]')
datetick('x','dd.mm.yy')
legend('Pick upGPR','HS WFJ','Location','southeast')
dynamicDateTicks() % Dylan
xlim([Surface.dates(1),Surface.dates(end)])

% save .jpg figure
plotfile = sprintf('%sHS_pick.jpg',Sufacename_save);
print('-djpeg99','-r600',plotfile);


step=1/8;
% interpolate, if the next measurement is temporally closer than interpolTime (in hours). Fill up with zeros if not.
interpolTime=3;

figure()
plotRadargram_dl('05.12.2018 0:00:00','28.01.2019 00:00:00',step,interpolTime,...
    Radar.radargramC1,Radar.dates,Radar.samples, 'e', colRamp, [-1,1],...
    WFJ_surface.dates,WFJ_surface.HS,'.g','HS WFJ',...
    Surface.dates,Surface.HS,'m','Pick upGPR')
plotfile = sprintf('%sradargramm_HS_pick.jpg',Sufacename_save);
print('-djpeg99','-r600',plotfile);

% for older code see Dyllan scripts at: N:\lawprae\LBI\Projects\101_upGPR\Master_Longridge

%% want to save??
save([Sufacename_save Surfacename '.mat'], 'Surface');



% % % % % % % GPRproject_prjname = sprintf('%s_pick_%d',GPRproject_prjname,pick)
% % % % % % % save([GPRproject_save GPRproject_prjname '.mat'], 'Radar','Surface');
toc

