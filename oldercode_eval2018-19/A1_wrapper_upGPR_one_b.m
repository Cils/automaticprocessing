% Pre- an Processing upGPR Data
% Wrapper file
% workflow to read in raw data, process and save it
% Dylan Longridge
% 28.3.2017

%% Tabula Rasa
clear all
close all
clc

%% Preprocessing %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% add project to path
addpath(genpath('N:\lawprae\LBI\Projects\101_upGPR\postprocessing-scripts\Matlab\automaticProcessing'))

% initialize project
Date='191127';
Time='1200';
season='2019-20';
disp([Date,'_',Time]);

% project name
GPRproject_prjname = 'upGPR_19';
% raw data path
GPRproject_prjpath = ['X:\Radar\Radardata\',season,'\upGPR WFJ\missions\',Date,'.MIS\',Date,'_',Time,'.ZON\LID10001.dt']; % set path/file manually
% raw data file
GPRproject_datfile = dir([GPRproject_prjpath '*.dt']);
% save path
GPRproject_save    = 'N:\lawprae\LBI\Projects\101_upGPR\postprocessing-scripts\Matlab\automaticProcessing\processed_data';

% save mat files
matdata = 'N:\lawprae\LBI\Projects\101_upGPR\postprocessing-scripts\Matlab\automaticProcessing\processed_data\matdata\';
filedate = '20190501';
plotfolder = [matdata filedate '_Plots/'];

% import gpr-data

gprflw(1).para.path     = GPRproject_prjpath;
gprflw(1).para.datafile = GPRproject_datfile;
gprflw(1).name = 'import_IDS';
gprdat(1).data = ReadIDS([gprflw(1).para.path gprflw(1).para.datafile.name]);

% Processing settings %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 0 for no
% 1 for yes
plotraw     = 1; % PLOT RAW DATA
staticcorr  = 0; % MANUAL STATIC CORRECTION
background  = 10; % BACKGROUND REMOVAL median = 1, svd gerade gerade = 2, svd steigung steigung = 3, svd gerade steigung = 4, svd steigung gerade = 5, svd steigung alles = 6, svd gerade alles = 7
dewow       = 0; % SUBTRACT MEAN (DEWOW)
freqspectra = 0; % PLOT FREQUENCY SPECTRA
butterbp1   = 1; % BUTTERWORTH BANDPASS FILTER
staticauto  = 1; % AUTOMATIC STATIC CORRECTION
butterbp2   = 0; % BUTTERWORTH BANDPASS FILTER
agcgain     = 0; % AGC GAIN FUNCTION
stacktraces = 1; % STACK TRACES
averagexy   = 0; % AVERAGE XY-FILTER

% -------------------------------------------------------------------------
% ------------------------- INPUT PARAMETERS ------------------------------
% -------------------------------------------------------------------------

meastime             = 40;   % Measurement time for nsamp [ns]
vlen                 = 20;   % Displaying time (max 40 ns)
picksurf             = 0;    % Use static correction picks (1) or not (0)

% -------------------------------------------------------------------------
% ------------------- DEFINE PROCESSING PARAMETERS ------------------------
% -------------------------------------------------------------------------
% propcp: structure containing all processing parameters

% colorbar
load zcm.mat                % blue-red colormap
load blkwhtred.mat          % black-white-red colormap
procp.cmap = blkwhtred;     % Colormap for displaying sections
procp.cax = [-1.0 1.0];     % Lower and upper bounds of colorbar

% electromagnetic wave speed in air,snow and ice 
procp.vair = 0.299;         % Air velocity
procp.vice = 0.1689;        % Ice velocity
procp.vsnow = 0.23;         % Snow velocity


procp.nmax=3640;             % Maximum number of data to use

procp.fl = 290;             % Low cut frequency for bandpass filter %210
procp.fh = 2500;            % High cut frequency for bandpass filter %2450
procp.threshold_amp = 0.07; % Threshold for the amplitude
procp.ford = 6;             % Order of the Butterworth bandpass filter
procp.startTwt  = 4.8;        % Phase follower starting time [ns], staticauto  must be set 1. % 5.8 for svd3
startDate = 1;             % starting trace (set to 1)
%procp.twin = 3000;         % Length of trace time window
%procp.maxdepth = 250;      % Maximum depth [m] for displaying data

% SVD filter settings
procp.svd_len = 201;        % Length of SVD filter
procp.bound_SVD = [1 251 540 900 2000 2005 2220 2640 3620 procp.nmax]; % Boundaries, where the upGPR moves up and down, used in option background=3
procp.bound_SVD= [20 200 400 700 1620 1645 1840 2130 3010 procp.nmax];
procp.SVD_WinNumb=1.;        % Used to determine SVD window based on ram lenght.  svd_len=ramp_length/SVD_WinNumb
procp.svd_dWin=20;             % Running windows spacing in SVD filter

%procp.decon_len = 50;      % fx-deconvolution filter length
procp.agclen = 10;          % Length of AGC filter (0 if no agc) (max 40ns)
procp.tord = 0;             % y-axis time (0) or depth (1)
procp.tax = 10;             % Lower limitation of the time axis for picking

%% -------------------------------------------------------------------------
% ------------------- Calculated Input Variables --------------------------
% -------------------------------------------------------------------------


if length(gprdat.data(1,:))>procp.nmax  % if too many samples are recorded, kill those traces
    redtraces = gprdat.data(:,1:procp.nmax);
    gprdat.data = redtraces
    disp(sprintf('More than %d traces in this measurement. Traces higher than %d were killed.', procp.nmax, procp.nmax))
end


gprdat.nr_of_traces  = length(gprdat.data(1,:)); % Number of traces (around 1444)
gprdat.nr_of_samples = length(gprdat.data(:,1)); % Number of samples (around 1024)

gprdat.sampling_rate = meastime / gprdat.nr_of_samples; % Sampling Interval [ns]
f = linspace(0,1/(2*gprdat.sampling_rate),gprdat.nr_of_samples/2)* 1000;
nf = length(f);

n = 1; % counting pics, indices for figures
k = 1; % counting data, indices for identifing processed data

viewlen = vlen/gprdat.sampling_rate;
stepsize = gprdat.nr_of_samples/4; % for time: 4  %%for depth: 92 for vsnow, 59.8 for v air

if procp.tord == 1 % y-axis: depth
    yend = procp.vsnow*meastime/2;
    deltat = yend*stepsize/gprdat.nr_of_samples;
    ylab = 'Depth [m]';
else % y-axis: time
    yend = meastime;
    deltat = yend*stepsize/gprdat.nr_of_samples;
    ylab = 'Time [ns]';
end

% -------------------------------------------------------------------------
% ------------------- Calculated Input Variables --------------------------
% -------------------------------------------------------------------------
[gprdat,yend,deltat,ylab,f,nf,viewlen,stepsize]=calsInpVar(gprdat,1,procp,meastime,vlen);

gprdat(k).fdata =1; % Initialize
gprdat(k).normfdata =1;





% -------------------------------------------------------------------------
% ------------------- PLOT RAW DATA ---------------------------------------
% -------------------------------------------------------------------------

if plotraw == 1
    fig = figure(n);clf;
    imagesc(gprdat(k).data(1:viewlen,:))
    set(gca,'YTick',0:stepsize:gprdat(k).nr_of_samples)
    set(gca,'YTickLabel',0:deltat:yend)
    colormap(procp.cmap)
    xlabel('Trace','fontsize',12);
    ylabel(ylab,'fontsize',12);
    set(gca,'fontsize',12);
    title('Raw Data')
    %         hold on
    %         line([230 250],[300 300], 'color','r')
    %         hold on
    %         line([240 240],[0 300], 'color','r')
    
    hold on
    for i =1:length( procp.bound_SVD)
        plot( [procp.bound_SVD(i) procp.bound_SVD(i) ],[0 viewlen], 'color','g' )
    end
    startTwt  = round(procp.startTwt/gprdat(k).sampling_rate);
    plot([0;length(gprdat(k).data(1,:))],[startTwt ;startTwt],'--k');
    n = n+1;
    k = k+1;
else
    k = k+1;
end


%% ------------------------------------------------------------------
% call function plotting raw data and change settings if necessary
%------------------------------------------------------------------

[bound_SVD,exc,startTwt]=find_bounds(gprdat,procp,procp.bound_SVD,1,1,1);
procp.bound_SVD=bound_SVD;
procp.startTwt=startTwt;


% -------------------------------------------------------------------------
% ------------------- BACKGROUND REMOVAL ----------------------------------
% -------------------------------------------------------------------------
%%

if background >0
    [ gprdat, k ] = backgroundrem( gprdat, k, background, procp );
    %%% for thesis plots (before, after median, after svd) insert
    %%% bgrcomparison.m
    
    fig = figure(n); clf;
    subplot(1,2,1)
    imagesc(gprdat(k-2).data(1:viewlen,:))
    set(gca,'YTick',0:stepsize:gprdat(k-2).nr_of_samples)
    set(gca,'YTickLabel',0:deltat:yend)
    colormap(procp.cmap)
    xlabel('Trace','fontsize',12);
    ylabel(ylab,'fontsize',12);
    set(gca,'fontsize',12);
    title('Before Background Removal')
    subplot(1,2,2)
    imagesc(gprdat(k-1).data(1:viewlen,:))
    set(gca,'YTick',0:stepsize:gprdat(k-1).nr_of_samples)
    set(gca,'YTickLabel',0:deltat:yend)
    colormap(procp.cmap)
    xlabel('Trace','fontsize',12);
    ylabel(ylab,'fontsize',12);
    set(gca,'fontsize',12);
    title('After Background Removal')
    n = n+1;
end


gprdat(k-1).fdata = sum(abs(fft(gprdat(k-1).data))'); % calculate for further steps

%% -------------------------------------------------------------------------
% ------------------- BUTTERWORTH BANDPASS FILTER -------------------------
% -------------------------------------------------------------------------

if butterbp1 == 1
    if (procp.fl & procp.fh > 0)
        fig = figure(n); clf;
        plot(f,gprdat(k-1).fdata(1:nf),'b')
        hold on
        nyq = 0.5 / gprdat(k-1).sampling_rate * 1000;
        fl = procp.fl/nyq;
        fh = procp.fh/nyq;
        [bb,aa] = butter(procp.ford,[fl fh]);
        gprdat(k).data = filter(bb,aa,gprdat(k-1).data);
        gprdat = renamedata(gprdat,k);
        gprdat(k).fdata = sum(abs(fft(gprdat(k).data))');
        plot(f,gprdat(k).fdata(1:nf),'r');
        xlabel('Frequency [MHz]','fontsize',12);
        ylabel('Amplitude [1]','fontsize',12);
        title('Amplitude Spectrum');
        legend('Before Butterworth Bandpass Filter','After Butterworth Bandpass Filter');
        xlim([0 6000])
        hold off
        n = n+1;
        
        fig = figure(n); clf;
        imagesc(gprdat(k).data(1:viewlen,:))
        set(gca,'YTick',0:stepsize:gprdat(k).nr_of_samples)
        set(gca,'YTickLabel',0:deltat:yend)
        colormap(procp.cmap)
        xlabel('Trace','fontsize',12);
        ylabel(ylab,'fontsize',12);
        set(gca,'fontsize',12);
        title('Static Correction');
        n = n+1;
        k = k+1;
    end
end

% %% AGC
% sra = 40/1024;
% sam=gprdat(k-1).nr_of_samples;
% agcgain = 1;
%
% if agcgain == 1
%     if procp.agclen > 0 && procp.agclen < sra*sam
%         gprdat(k).data = agc(gprdat(k-1).data,procp.agclen,sra);
%     elseif procp.agclen >=   sra*sam
%         disp(['AGC length (procp.agclen) is too long. Type in value lower than ', num2str(sra*sam),' ns!'])
%     elseif procp.agclen == 0
%         disp('No AGC gain function applied (procp.agclen = 0)')
%     else
%         disp(['AGC length (procp.agclen) has to be a positive number smaller than ',num2str(sra*sam),' ns.'])
%     end
% end
% gprdat = renamedata(gprdat,k);
% k = k+1;
%% -------------------------------------------------------------------------
% ------------------- AUTOMATIC STATIC CORRECTION -------------------------
% -------------------------------------------------------------------------

if staticauto == 1 % follow the phase after a pick %
    a=1;
    
    if picksurf == 1    % If picks  already exist as .mat file
        
        pick_file = sprintf('%sSURFPICK_PROFILE_2.mat', matdata);
        %pick_file = sprintf('%s%s_SURFPICK_PROFILE_%03d.mat',matdata,filedate,a); %let each session pick his his own surface..
        load(pick_file,'tti')
        
        
    else                % pick tti and save picks into .mat file
        dates     = linspace(1,gprdat(k-1).nr_of_traces,gprdat(k-1).nr_of_traces);
        samples   = linspace(1,gprdat(k-1).nr_of_samples,gprdat(k-1).nr_of_samples);
        
        startTwt  = round(procp.startTwt/gprdat(k-1).sampling_rate);
        [newPickTwt, newPickTwt_zero] = manualPick_FollowPhase2(gprdat(k-1).data, dates, samples, startDate, startTwt);
        
        newPickTwt_zero = newPickTwt_zero';
        lennewpick=size(newPickTwt,1);
        newPickTwt = newPickTwt_zero;
        %%%%111%%%% the starting date can be changed from one, if this is applied!
        for i = size(newPickTwt,2)+1:lennewpick
            newPickTwt(i) = 0;
        end
        
        for i = 1:lennewpick
            if newPickTwt(i) == 0% if any value is zero, take the last value
                newPickTwt(i) = newPickTwt(i-1);
            end
        end
        if startDate > 1
            for i = 1:startDate-1
                % add values at the same starting height as the first pick at
                % the first few positions
                newPickTwt2(i) = newPickTwt(1);
            end
            newPickTwt2(startDate:gprdat(k-1).nr_of_traces) = newPickTwt(:);
        else
            newPickTwt2 = newPickTwt;
        end
        newPickTwt =newPickTwt2;
        %%%%111%%%%
        hold on
        plot(newPickTwt2)
        if staticauto ~= 3
            %%% Dylan 15.5, Korrektur
            %newPickTwt = newPickTwt - 23;
            %%% Dylan 12.7, Korrektur
            newPickTwt = newPickTwt - 19;
        end
        
        [tti_2] =(newPickTwt*gprdat(k-1).sampling_rate);
        [tti] = [tti_2]';
        pick_file = sprintf('%sSURFPICK_PROFILE_%d.mat', matdata,background);
        %pick_file = sprintf('%s%s_SURFPICK_PROFILE_%03d.mat',matdata,filedate,a);
    end
    
    
    % figure plots
    hold on
    plot(newPickTwt)
    legend('Static Correction','Static Correction shifted to Surface Level')
    for a = 1:gprdat(k-1).nr_of_traces
        if (tti(a) > 0)
            i1 = round(tti(a)/gprdat(k-1).sampling_rate);
            gprdat(k).data(1:gprdat(k-1).nr_of_samples-i1+1,a) = gprdat(k-1).data(i1:end,a);
        else
            gprdat(k).data(:,a) = gprdat(k-1).data(:,a);
        end;
    end;
    gprdat = renamedata(gprdat,k);
    gprdat(k).nr_of_samples = length(gprdat(k).data(:,1));
    %%% fuer Thesis: Plot von static correction
    %%% hier staticcorrectionplot.m ausf�hren
    
%     viewlen = length(gprdat(k).data(:,1));
    fig = figure(n); clf;
    subplot(2,1,1)
    imagesc(gprdat(k-1).data(1:viewlen,:))
    hold on
    plot(dates,newPickTwt,'LineWidth',2,'Color','m')
    set(gca,'YTick',0:stepsize:gprdat(k-1).nr_of_samples)
    set(gca,'YTickLabel',0:deltat:yend)
    colormap(procp.cmap)
    xlabel('Trace','fontsize',12);
    ylabel(ylab,'fontsize',12);
    set(gca,'fontsize',12);
    title('Static correction line')
    
    subplot(2,1,2)
    imagesc(gprdat(k).data(1:viewlen,:))
    set(gca,'YTick',0:stepsize:gprdat(k).nr_of_samples)
    set(gca,'YTickLabel',0:deltat:yend)
    colormap(procp.cmap)
    xlabel('Trace','fontsize',12);
    ylabel(ylab,'fontsize',12);
    set(gca,'fontsize',12);
    title('Static correction')
    
    gprdat(k).fdata = sum(abs(fft(gprdat(k).data))');
%    save(pick_file); % to save the picks!!!
    n = n+1;
    k = k+1;
end



%% -------------------------------------------------------------------------
% ------------------- PLOT FREQUENCY SPECTRA ------------------------------
% -------------------------------------------------------------------------

if freqspectra == 1
    fig = figure(n); clf;
    plot(f,gprdat(k-1).fdata(1:nf),'b');
    hold on;
    xlabel('Frequency [MHz]','fontsize',12);
    ylabel('Amplitude','fontsize',12);
    title('Amplitude Spectrum','fontsize',12);
    n = n+1;
    % ---------- NORMALIZED LOG PLOT ----------
    MaxAmp = max(gprdat(k-1).fdata);
    for a = 1:length(gprdat(k-1).fdata);
        gprdat(k-1).normfdata(a) = gprdat(k-1).fdata(a)/MaxAmp;
    end
    fig = figure(n);clf;
    semilogy(f,gprdat(k-1).normfdata(1:nf),'b');
    hold on
    procp.threshold_amp = ones(length(f),1)*procp.threshold_amp;
    plot(f,procp.threshold_amp,'g--')
    xlabel('Frequency [MHz]','fontsize',12);
    ylabel('Normalized Log Amplitude','fontsize',12);
    title('Normalized Amplitude Spectrum','fontsize',12);
    n = n+1;
end

%% -------------------------------------------------------------------------
% ------------------- BUTTERWORTH BANDPASS FILTER -------------------------
% -------------------------------------------------------------------------

if butterbp2 == 1
    if (procp.fl & procp.fh > 0)
        fig = figure(n); clf;
        plot(f,gprdat(k-1).fdata(1:nf),'b')
        hold on
        nyq = 0.5 / gprdat(k-1).sampling_rate * 1000;
        fl = procp.fl/nyq;
        fh = procp.fh/nyq;
        [bb,aa] = butter(procp.ford,[fl fh]);
        gprdat(k).data = filter(bb,aa,gprdat(k-1).data);
        gprdat = renamedata(gprdat,k);
        gprdat(k).fdata = sum(abs(fft(gprdat(k).data))');
        plot(f,gprdat(k).fdata(1:nf),'r');
        legend('Before Butterworth Bandpass Filter','After Butterworth Bandpass Filter');
        hold off
        n = n+1;
        
        fig = figure(n); clf;
        imagesc(gprdat(k).data(1:viewlen,:))
        set(gca,'YTick',0:stepsize:gprdat(k).nr_of_samples)
        set(gca,'YTickLabel',0:deltat:yend)
        
        colormap(procp.cmap)
        xlabel('Trace','fontsize',12);
        ylabel(ylab,'fontsize',12);
        set(gca,'fontsize',12);
        title('After Butterworth Bandpass Filter');
        n = n+1;
        k = k+1;
    end
end


%% -------------------------------------------------------------------------
% ------------------- AGC GAIN FUNCTION -----------------------------------
% -------------------------------------------------------------------------

if agcgain == 1
    if procp.agclen > 0 && procp.agclen < 41
        gprdat(k).data = agc(gprdat(k-1).data,procp.agclen,gprdat(k-1).sampling_rate);
        gprdat = renamedata(gprdat,k);
        fig = figure(n); clf;
        imagesc(gprdat(k).data(1:viewlen,:))
        set(gca,'YTick',0:stepsize:gprdat(k).nr_of_samples)
        set(gca,'YTickLabel',0:deltat:yend)
        colormap(procp.cmap)
        xlabel('Trace','fontsize',12);
        ylabel(ylab,'fontsize',12);
        set(gca,'fontsize',12);
        title('After AGC Gain function')
    elseif procp.agclen >= 41
        disp('AGC length (procp.agclen) is too long. Type in value lower than 41!')
    elseif procp.agclen == 0
        disp('No AGC gain function applied (procp.agclen = 0)')
    else
        disp('AGC length (procp.agclen) has to be a positive number smaller than 41.')
    end
    n = n+1;
    k = k+1;
end


%% -------------------------------------------------------------------------
% ---------------------- STACK TRACES -------------------------------------
% -------------------------------------------------------------------------

if stacktraces == 1
    for i = 1:gprdat(k-1).nr_of_samples
        stack(i) = sum(gprdat(k-1).data(i,:));
    end
    gprdat(k).data = stack';
    gprdat = renamedata(gprdat,k);
    fig = figure(n); clf;
    
    y= zeros(gprdat(k).nr_of_samples,1);
    plot(gprdat(k).data)
    hold on
    plot(y,'Color','b')
    
    set(gca,'XTick',0:stepsize:gprdat(k).nr_of_samples)
    set(gca,'XTickLabel',0:deltat:yend)
    %colormap(procp.cmap)
    ylabel('Amplitude [1]','fontsize',12);
    xlabel(ylab,'fontsize',12);
    set(gca,'fontsize',12);
    title('After Stacking')
    n = n+1;
    k = k+1;
end

%%

%% save project to save path
% flow and data files
%
% save([GPRproject_save GPRproject_prjname '.gprf'], 'gprflw');
% save([GPRproject_save GPRproject_prjname '.gprd'], 'gprdat');


%%%%% TO DO %%%%%

%     -dewow ersetzen durch subtract mean?
%     -und soltte Background removal und subtract mean (dewow) nicht nach
%     der static correction sein?
%     -zwei Wege f�r die Static correction:
%           1. Genauen Bewegungsverlauf kennen und dann so programmieren
%           2. Static correction mit picking 'surface' machen
