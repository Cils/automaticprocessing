% Pre- an Processing upGPR Data
% Wrapper file
% workflow to see all the data beside each other
% Dylan Longridge
% 5.4.2017

%% Tabula Rasa
clear all
close all
clc


%% Input data
plotraw     = 0; % PLOT RAW DATA
staticcorr  = 0; % MANUAL STATIC CORRECTION
background  = 10; % BACKGROUND REMOVAL
dewow       = 0; % SUBTRACT MEAN (DEWOW)
freqspectra = 0; % PLOT FREQUENCY SPECTRA
butterbp1   = 1; % BUTTERWORTH BANDPASS FILTER
staticauto  = 1; % AUTOMATIC STATIC CORRECTION
butterbp2   = 0; % BUTTERWORTH BANDPASS FILTER
agcgain     = 0; % AGC GAIN FUNCTION
stacktraces = 1; % STACK TRACES
averagexy   = 0; % AVERAGE XY-FILTER

% Get project
addpath(genpath('N:\lawprae\LBI\Projects\101_upGPR\postprocessing-scripts\Matlab\automaticProcessing'))

namenonumbers = 'wholeseason_SVD'; % Name of the project without numbers behind
GPRproject_prjname = sprintf('%s_bakgr%d_%d%d%d%d%d%d%d%d%d%d%d',namenonumbers,background,plotraw,staticcorr,background,dewow,freqspectra,butterbp1,staticauto,butterbp2, agcgain, stacktraces,averagexy)
GPRproject_save = 'N:\lawprae\LBI\Projects\101_upGPR\postprocessing-scripts\Matlab\automaticProcessing\processed_data\upGPR_19\';
cd(GPRproject_save)
plotfolder = 'N:\lawprae\LBI\Projects\101_upGPR\postprocessing-scripts\Matlab\automaticProcessing\processed_data\upGPR_19\';
GPRproject_location = ['N:\lawprae\LBI\Projects\101_upGPR\postprocessing-scripts\Matlab\automaticProcessing\processed_data\upGPR_19\' GPRproject_prjname,'.mat']

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set options for output data
agcgain     = 1;    % AGC GAIN FUNCTION
averagexy   = 0;    % AVERAGE XY-FILTER

task = 1;           % task = 0: Plot date on x-axis, time on y-axis. task = 1: Plot date on x-axis, depth on y-axis
adj = 1;            % adj = 0/1:  Without/with adjustment of colormap
vsnow = 0.23;       % Snow velocity [m/ns]
procp.vsnow=vsnow;
procp.agclen =  7;  % AGC length [ns]
dta = 5.0e-4;       % exponential increase for adjusting the plot
dcax = 1.05;        % increase factor of the coloraxis for adjusting the plot
cutlength = 900;    % cut data to a certain trace length

% SVD filter
%------------
procp.svd_len = 160; % 8 Messungen pro Tag
procp.decon_len = 500; % FX-Deconvolution filter length
procp.fl = 200;
procp.fh = 2500;

sra = 40/1024;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Input calculations
load(GPRproject_location)
[n m] = size(gprdatsession);
tra = n*m;  % traces


% Create data array
b=0;
for a = 1:tra
    c = ceil(a/m); % row _-_-
    d = a-((c-1)*m); % column ||||
    if isempty(gprdatsession(c,d).data) == 1
    else
        b=b+1;
        data(:,b) = gprdatsession(c,d).data(1:cutlength); % Dylan (:)
        date(b) = datenum(gprdatsession(c,d).date, 'yymmdd_HHMM');
    end
end
nr_samples = length(data(:,1)); % samples

% plot raw data

[ax,fig] = plotRadargram_raw(agc(data,procp.agclen,sra),date,procp,task,sra,'Raw data');
data_raw = data;


%% fit drift and apply correcture drift 


sections = datenum(['181204_2100';'190220_0000'], 'yymmdd_HHMM');
% Picks(1).str= [0.02,0.07,0.14,0.25];
% Picks(2).str= [0.02,0.07,0.19,0.25];
% Picks(3).str= [0.02,0.07,0.15,0.2];
Picks(1).str= [0.25];
Picks(2).str= [0.19];
Picks(3).str= [0.02];

colors=['g';'m';'b';'c'];

sect_ind(1)=1;
for i=1:length(sections)
    sect_ind(i+1) = find(date> sections(i),1);
end
sect_ind(i+2)= length(date); 

for i=1:length(Picks)
    t = (0:nr_samples-1)*sra*procp.vsnow/2;
%   samples   = linspace(1,length(data(:,1)),length(data(:,1)));
%   Xs=linspace(1,sect_ind(i+1)-sect_ind(i),sect_ind(i+1)-sect_ind(i));
    Picks(i).d=zeros(sect_ind(i+1)-sect_ind(i)+1,length(Picks));
    
    % create figure
    fig_drift=figure();
    hold on
    tt=[datestr(date(sect_ind(i)), 'dd/mm/YYYY'),'-',datestr(date(sect_ind(i+1)),'dd/mm/YYYY')];
    title(tt)
    legend()
   
    % loop through starting points
    for j=1:length(Picks(i).str)
        
        [Picks(i).d(:,j),Picks(i).iy(:,j)] = FollowPhase(data(:,sect_ind(i):sect_ind(i+1)),...
                                              t,...
                                              Picks(i).str(j) );
                                          
%         [~,Picks(i).d(:,j)] = manualPick_FollowPhase3(data(:,sect_ind(i):sect_ind(i+1)),...
%                                       Xs,...
%                                       t,...
%                                       1,...
%                                       Picks(i).str(j) );
        % plot picked drifts
        
        % on radargram
        figure(fig)
        set(fig,'CurrentAxes',ax)
        hold on
        plot(date(sect_ind(i):sect_ind(i+1)),...
             Picks(i).d(:,j),...
             'Color',colors(j),...
             'LineWidth',2);
        
        % difference between picking lines
        figure(fig_drift)
        plot(date(sect_ind(i):sect_ind(i+1)),Picks(i).d(:,j)-Picks(i).d(1,j),...
             'Color',colors(j),...
             'DisplayName',string( Picks(i).str(j) ) )
        
    end

    
end

% shift data
data2=data;
for i=1:length(Picks)
    
    for j=1:length(Picks(i).iy(:,1))
        k=sect_ind(i)+j-1;
        iy=Picks(i).iy(j,1)-Picks(i).iy(1,1);
        
        if iy > 0
            data2(1:nr_samples-iy ,k)= data(1+iy:nr_samples ,k);
            if j>1
                data2(nr_samples-iy+1:nr_samples ,k)= data2(nr_samples-iy+1:nr_samples ,k-1);
            end
            
        elseif iy < 0
            data2(-iy+1:nr_samples ,k)= data(1:nr_samples+iy ,k);
            if j>1
                data2(1:-iy ,k)= data2(1:-iy ,k-1);
            end
        end
    end 
end

[ax2,fig2] = plotRadargram_raw(agc(data2,procp.agclen,sra),date,procp,task,sra,'After shifting drift');

data=data2;


%% Moving window SVD Filtering dr�ber hauen

for i=1:length(Picks)
    
    [ gprdat ] = bckgamma( data(:,sect_ind(i):sect_ind(i+1)-1),procp.svd_len,sra,procp.decon_len,procp.fl,procp.fh );

    data(:,sect_ind(i):sect_ind(i+1)-1) = gprdat(3).data;
end
% plot data
[ax3,fig3]= plotRadargram_raw(data,date,procp,task,sra,'SVD');



% older version without dividing season in sections

% [ gprdat ] = bckgamma( data,procp.svd_len,sra,procp.decon_len,procp.fl,procp.fh );
% data = gprdat(3).data;
% 
% % plot data
% [ax3,fig3]= plotRadargram_raw(data,date,procp,task,sra,'SVD');



%% further processing
% -------------------------------------------------------------------------
% ------------------- AGC GAIN FUNCTION -----------------------------------
% -------------------------------------------------------------------------
if agcgain == 1
    if procp.agclen > 0 && procp.agclen < sra*nr_samples
        data = agc(data,procp.agclen,sra); % f�r Plots zur AGC function muss hier folgendes eingef�gt werden: data = agc2(data,procp.agclen,sra);
    elseif procp.agclen >=   sra*nr_samples
        disp(['AGC length (procp.agclen) is too long. Type in value lower than ', num2str(sra*nr_samples),' ns!'])
    elseif procp.agclen == 0
        disp('No AGC gain function applied (procp.agclen = 0)')
    else
        disp(['AGC length (procp.agclen) has to be a positive number smaller than ',num2str(sra*nr_samples),' ns.'])
    end
end

% plot data
[ax2,fig2] = plotRadargram_raw(data,date,procp,task,sra,'AGC');




%% Plot and ADJUST COLORAXIS

t = (0:nr_samples-1)*sra;
date = date'; %datestr(date);
load blkwhtred.mat
load zcm.mat
procp.cmap = blkwhtred;

if (task == 0)
    xr = date;
    yr = t;
    xlab = 'Date';
    ylab = 'Time [ns]';
    yd = 'normal';
end

if (task == 1)
    xr = date;
    yr = t*vsnow/2;
    xlab = 'Date'
    ylab = 'Depth [m]';
    yd = 'normal';
end


button = 0;
datao = data;
procp.tfun = zeros(100,1);  % exponential scaling factor for vertical axis
procp.cmap = blkwhtred;
procp.cax = [-1.0 1.0];     % Lower and upper bounds of colorbar
Title = 'Season';
if (adj == 1)
    disp('Choose colormap with 1/2')
    disp('Choose caxis with left/right')
    disp('Choose vertical scaling with up/down')
end;

while(button ~= 27)
    if (procp.tfun(task+1) ~= 0)
        for a = 1:length(date)
            data(:,a) = datao(:,a) .* exp(-(1:length(yr))*procp.tfun(task+1))';
        end;
    end;
    imagesc(xr,yr,data);
    xlabel(xlab,'fontsize',12);
    ylabel(ylab,'fontsize',12);
    set(gca,'fontsize',12);
    title(Title,'fontsize', 12,'interpreter','none');
    colormap(procp.cmap);    
    caxis(procp.cax); %caxis('auto');% caxis(procp.cax); 
    freezeColors;
    set(gca,'YDir',yd);
    %colorbar
    datetick('x','dd.mm.yy')
    
    if (adj == 0), break; end;
        
    [xx,yy,button] = ginput(1);
    if (button == 28), procp.cax = procp.cax/dcax; end;
    if (button == 29), procp.cax = procp.cax*dcax; end;
    if (button == 49), procp.cmap = zcm; end;
    if (button == 50), procp.cmap = blkwhtred; end;                
    if (button == 30), procp.tfun(task+1) = procp.tfun(task+1) + dta; end;
    if (button == 31), procp.tfun(task+1) = procp.tfun(task+1) - dta; end;
end;        

Radar.dates = date;
Radar.radargramC1 = data;
Radar.samples = linspace(0,nr_samples*sra,nr_samples)';

%% save 
GPRproject_prjname2 = sprintf('%s_background%d_%d%d%d',namenonumbers,background,background,agcgain,averagexy);
save([GPRproject_save GPRproject_prjname2 '.mat'], 'Radar');
save([GPRproject_save GPRproject_prjname2 '_' datestr(today,'yymmdd') '.mat'], 'Radar');


% save .jpg figure
plotfile = sprintf('%s%s.jpg',plotfolder,GPRproject_prjname2)
print('-djpeg99','-r600',plotfile)
plotfile = sprintf('%s%s_%s.jpg',plotfolder,GPRproject_prjname2,datestr(today,'yymmdd'))
print('-djpeg99','-r600',plotfile)
%% save as matlab figure
h=gcf;
saveas(h,plotfile(1:end-4),'fig')